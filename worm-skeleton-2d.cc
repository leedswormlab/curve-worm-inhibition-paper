
// HACK, apparently used in other headers
#undef NCAMS
#define NCAMS 1
#undef WORLD_DIM
#define WORLD_DIM 2

#include "config.hh"

#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>
#include <unistd.h>

#define USE_FREAK

// #include <opencv2/imgproc.hpp>

// input parameters
#include "lib/parameter.hh"

// mesh and time helpers
#include "lib/mesh.hh"
#include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"
#include "lib/image/io.hh"

#include "lib/trace.hh"
#include "lib/timer.hh"

// graph-based midline finder backend
#include "lib/graph/frames.hh"
#include "lib/image/known_points.hh"
#include "lib/graph_pointfinder.hh"

static const std::string default_dilate_silhouette="0";
static const bool _displaystuff=false;

//static constexpr unsigned NCAMS=2;
//static constexpr unsigned WORLD_DIM=2;

// why these?
static const unsigned int dim = WORLD_DIM;
static const unsigned int nCams = NCAMS;

CameraProjectionOperator< 2, 2 > X(17,17); // test



typedef draft::MidlineFinderA<ANNOTATION> kpf_default_type;

template<class K>
void simpleMidlineFinder( const cv::Mat& background,
			  const cv::Mat& raw,
			  const cv::Mat& rawPrev,
			  K& points )
{
  /*
   #  hat tip to http://felix.abecassis.me/2011/09/opencv-morphological-skeleton/

    img = img.copy() # don't clobber original
    skel = img.copy()

    skel[:,:] = 0
    kernel = cv2.getStructuringElement(cv2.MORPH_CROSS, (3,3))

    while True:
        eroded = cv2.morphologyEx(img, cv2.MORPH_ERODE, kernel)
        temp = cv2.morphologyEx(eroded, cv2.MORPH_DILATE, kernel)
        temp  = cv2.subtract(img, temp)
        skel = cv2.bitwise_or(skel, temp)
        img[:,:] = eroded[:,:]
        if cv2.countNonZero(img) == 0:
            break
*/
  cv::namedWindow("simple", cv::WINDOW_NORMAL );
  cv::resizeWindow("simple", 1024, 1024);

  // background subtraction
  // cv::Mat subtracted;
  cv::Mat subtracted1 = ( background - raw );
  cv::Mat subtracted2 = ( raw - background );
  cv::Mat subtracted = subtracted1 + 3 * subtracted2;

  double max, min;
  cv::minMaxLoc( subtracted, &min, &max );

  if( max > 0 )
    {
      subtracted *= 255/max;
    }

  // blur
  cv::Mat blurred;
  cv::GaussianBlur( subtracted, blurred, cv::Size(3,3), 3 );

  // threshold
  cv::Mat thresholded;
  cv::threshold( blurred, thresholded, 0, 255, cv::THRESH_BINARY|cv::THRESH_OTSU );

  // find contour for plotting
  std::vector<std::vector<cv::Point> > contours;
  std::vector<cv::Vec4i> hierarchy;

  // create kernel
  const cv::Mat kernel = cv::getStructuringElement( cv::MORPH_CROSS, cv::Size( 3, 3 ) );
  for( unsigned int j = 0; j < 2; ++j )
    cv::morphologyEx( thresholded, thresholded, cv::MORPH_ERODE, kernel );
  for( unsigned int j = 0; j < 7; ++j )
    cv::morphologyEx( thresholded, thresholded, cv::MORPH_DILATE, kernel );
  for( unsigned int j = 0; j < 5; ++j )
    cv::morphologyEx( thresholded, thresholded, cv::MORPH_ERODE, kernel );

  /// Find contours
  cv::findContours( thresholded, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE );

  // create skeleton
  cv::Mat skeleton = cv::Mat::zeros( thresholded.size(), thresholded.type() );
  cv::Mat eroded, temp;
  while( cv::countNonZero( thresholded ) )
    {
      cv::morphologyEx( thresholded, eroded, cv::MORPH_ERODE, kernel );
      cv::morphologyEx( eroded, temp, cv::MORPH_DILATE, kernel );
      temp = thresholded - temp;
      cv::bitwise_or( skeleton, temp, skeleton );
      thresholded = eroded.clone();
    }

  // find a connected contour of points
  cv::Mat contourMat = cv::Mat::zeros( raw.size(), raw.type() );
  drawContours( contourMat, contours, -1, cv::Scalar(255), 1, 8, hierarchy, 0, cv::Point() );

  // assuming your matrix is CV_8U, and black is 0
  std::vector<cv::Point> blackPixels;
  unsigned char *p = skeleton.ptr<unsigned char>();
  for(int i = 0; i < skeleton.rows * skeleton.cols; ++i, ++p)
    {
      if(*p != uchar(0))
	{
	  const int x = i % skeleton.cols;
	  const int y = i / skeleton.cols;
	  const cv::Point pt( x, y );

	  bool inContour = ( contourMat.at<uchar>(pt) == (uchar)255 );
	  if( not inContour )
	    points.emplace( pt, 0 );
	}
    }

  cv::Mat rawCopy;
  cv::cvtColor( raw, rawCopy, CV_GRAY2BGR );

  for( const auto& contour : contours )
    for( const auto& pt : contour )
      {
	rawCopy.at< cv::Vec3b >( pt )[0] = (uchar)0;
	rawCopy.at< cv::Vec3b >( pt )[1] = (uchar)255;
	rawCopy.at< cv::Vec3b >( pt )[2] = (uchar)0;
      }

  int minx = raw.cols, miny = raw.rows, maxx = 0, maxy = 0;
  for( const auto& p : points )
    {
      const auto pt = p.first;
      rawCopy.at< cv::Vec3b >( pt )[0] = (uchar)0;
      rawCopy.at< cv::Vec3b >( pt )[1] = (uchar)0;
      rawCopy.at< cv::Vec3b >( pt )[2] = (uchar)255;

      int i = pt.x;
      int j = pt.y;
      maxx = std::max( maxx, i+15 );
      minx = std::min( minx, i-15 );
      maxy = std::max( maxy, j+15 );
      miny = std::min( miny, j-15 );
    }

  cv::imshow( "simple", rawCopy.colRange( minx, maxx ).rowRange( miny, maxy ) );
  cv::waitKey(10);
}

void skeleton( ConfigParameters const& parameters, const Mesh &mesh )
{
  // time from start of run
  std::clock_t startOfRun;
  startOfRun = std::clock();

  cv::namedWindow("output", cv::WINDOW_NORMAL );
  cv::resizeWindow("output", 1024, 1024);

  untested();
  bool use_graph=get(parameters, "use-graph", false);
  untested();
  double deltat=parameters.get<double>("midline-deltat");
  double similarity_sigma=parameters.get<double>("similarity-sigma");
  unsigned thresh_edge=parameters.get<unsigned>("thresh-edge");
  unsigned thresh_collapse=parameters.get<unsigned>("thresh-collapse");
  unsigned dilate_sil_radius=parameters.get<unsigned>("dilate-sil-radius");
  int initialguess_known=parameters.get<int>("initial-known");
  int initialguess_unknown=parameters.get<int>("initial-unknown");
  bool restrict_sil=parameters.get<bool>("restrict-sil");

  known_points_finder<ANNOTATION>* kpf=NULL;

  if(!use_graph){ untested();
    kpf=new kpf_default_type();
  }else{

    typedef draft::neigh_similarity1 weights_type;
    typedef draft::edgeweighted_graph_type1 G;
  //  typedef draft::framesgraph<G, weights_type> fg_type;
    typedef draft::framesgraph_pointfinder<G, weights_type> fgpf_type;

    std::string loadfile=parameters.get<std::string>("loadfile");
    kpf = new fgpf_type(loadfile, deltat, initialguess_known, initialguess_unknown,
	thresh_collapse, thresh_edge, similarity_sigma, dilate_sil_radius, restrict_sil);

#if 0 //no.
  if(loadfile!=""){ untested();
    s_timer tmr("file load");
    std::ifstream myfile;
    myfile.open (loadfile);
//    fg_type a(myfile); <= this is needed.
//    kpf=a;
  }else{ untested();
    std::cerr << "need loadfile\n";
    exit(1);
  }
#endif
  }

  // open video streams
  std::array< cv::VideoCapture, nCams > caps;
  for( unsigned int cam = 0; cam < nCams; ++cam )
    {
      // select view
      auto& cap = caps.at(cam);

      // open from filename
      const std::string fn = parameters.getCamString( "video.filename", cam );
      cap.open( fn );

      if(!cap.isOpened())  // check if we succeeded
	{ untested();
	  throw "unable to open video feed " + fn;
	}
    }

  // get starting frame number
  const unsigned int videoStartFrame = parameters.get<unsigned int>("video.startframe");

  // get background frame
  std::array< cv::Mat, nCams > backgrounds;
  for( unsigned int cam = 0; cam < nCams; ++cam )
    {
      const std::string backgroundFn = parameters.getCamString( "video.background", cam );
      std::cout << "using background image " << backgroundFn << std::endl;
      getBackgroundImage( backgroundFn, backgrounds.at(cam) );
    }

  // read calibration
  using BaseProjectionOperatorType = CameraProjectionOperator< dim, 2 /* hmm */ >;
  std::vector< BaseProjectionOperatorType > p;
  {
    const double cameraScaling = parameters.get<double>("video.calibration.scaling");
    const double cameraCentre = parameters.get<double>("video.calibration.centre");
    BaseProjectionOperatorType pp( cameraScaling, cameraCentre );
    p.push_back( pp );
    // readCalibrationParameters( filename, p );
  }

  // set energy difference tolerance
  const double energyDiffTol = parameters.get< double >( "term.energydiff" );

  // initial time provider
  TimeProvider imageTimeProvider;

  // initialise output
  VTUWriter writer( parameters, imageTimeProvider, "skeletons_" );
  std::vector< VTUWriter > projectedWriter;
  for( unsigned int cam = 0; cam < nCams; ++cam )
    {
      std::stringstream ss;
      ss << "projected_skeletons_cam" << cam;
      projectedWriter.push_back( VTUWriter( parameters, imageTimeProvider, ss.str() ) );
    }

  // persistent containers for position and gamma
  Vector persistentPositionData( dim * mesh.N() );
  PiecewiseLinearFunction<dim> persistentPosition( mesh, SubArray< Vector >( persistentPositionData.begin(), persistentPositionData.end() ) );
  bool persistentCreated = false;
  double persistentGamma = parameters.get<double>("model.gamma");

  std::array< cv::Mat, nCams > rawPrev;
  for( ; ; imageTimeProvider.next( 1.0 / 25.0 ) )
    {
      // skip if before start frame
      if( imageTimeProvider.iteration() < videoStartFrame )
	{ untested();
	  for( auto& cap : caps )
	    cap.grab();
	  continue;
	}

      trace0("main loop");
      // time the loop
      std::clock_t start;
      start = std::clock();

      // get this frame
      std::array< cv::Mat, nCams > frames;
      std::array< cv::Mat, nCams > raw;
      for( unsigned int cam = 0; cam < nCams; ++cam )
	{
	  // get a new frame from camera
	  cv::Mat frame;
	  caps.at(cam) >> frame;
	  if( frame.empty() )
	    { untested();
	      std::stringstream ss;
	      ss << "unable to open video feed frame no. " << imageTimeProvider.iteration();
	      throw ss.str();
	    }

	  // convert to b&w and store
	  cv::cvtColor(frame, frames.at(cam), CV_BGR2GRAY);
	  frames.at(cam).copyTo( raw.at(cam) );
	}

      message(bTRACE, "bg sub\n");
      for( unsigned int cam = 0; cam < nCams; ++cam ) { untested();
          frames[cam] = backgrounds[cam] - frames[cam];
      }
      // silhouette
      // silhoutteImage( gray, backgrounds.at(cam), srcs.at(cam), 10 );
      std::array< cv::Mat, nCams > srcs;
      silhouetteImages( p, frames, srcs, 10 );

      // do background subtraction of frame
      for( unsigned int cam = 0; cam < nCams; ++cam )
	{
	  // cv::subtract(cv::Scalar::all(255),frames.at(cam),frames.at(cam));
	  // cv::Mat bgCol;
	  cv::cvtColor( backgrounds.at(cam) - frames.at(cam), frames.at(cam), CV_GRAY2BGR );
	}

      // time holder
      TimeProvider timeProvider;
      const double endTime = parameters.get< double >( "time.end" );
      double deltaT = parameters.get< double >( "time.deltaT" );
      timeProvider.setDeltaT( deltaT );

      using BaseDistanceFunctionType = VectorDistanceFunction;
      std::vector< BaseDistanceFunctionType > d;

      for( unsigned int cam = 0; cam < nCams; ++cam )
	{
// no more trimming.
//	  const auto pHead = p.at(cam)( head );
//	  const auto pTail = p.at(cam)( tail );

	  {
	    ANNOTATION known;
	    cv::Mat raw2;
	    cv::cvtColor(frames[cam], raw2, CV_BGR2GRAY );
	    {
	      s_timer tmr("setting up midline finder");
	      // kpf->find_known_points(srcs.at(cam), raw2, known);
	      simpleMidlineFinder( backgrounds.at(cam), raw.at(cam), rawPrev.at(cam), known );
	    }
	    assert(known.size());

	    if(_displaystuff) { untested();
	      display_pointset_on_frame("known", raw2, known);
	    }

	    // print out how many points found
	    std::cout << "cam " << cam << " image has " << known.size() << " points" << std::endl;

	    d.emplace_back(known.begin(), known.end());
	  }

	}
      if(get<bool>(parameters, "known", false)){ untested();
	break;
      }else{ untested();
      }

      for( unsigned int cam = 0; cam < nCams; ++cam )
	rawPrev.at(cam) = raw.at(cam).clone();

      using DistanceFunctionType = ProjectedDistanceFunction< BaseDistanceFunctionType, BaseProjectionOperatorType, nCams >;
      DistanceFunctionType distanceFunction( d, p );

      // find model
      using ModelType = DistanceModel< dim, nCams >;
      const ModelType model( parameters, distanceFunction,
			     timeProvider, imageTimeProvider, persistentGamma );

      // initialise scheme
      using FemSchemeType = FemScheme< ModelType, WORLD_DIM >;
      FemSchemeType scheme( mesh, model );

      // initialse scheme
      if( not persistentCreated )
	scheme.initialiseXY( distanceFunction.middle() );
      else
	scheme.initialiseXY( persistentPosition );

      // io time helpers
      double nextWriteTime = 0;
      const double nextWriteTimeStep = parameters.get< double >( "io.timestep" );
      const bool doInnerWrite = parameters.get< bool >( "io.innerwrite" );

      std::stringstream innerWriterSs;
      innerWriterSs << "skeletons_frame_" << imageTimeProvider.iteration() << "_";
      VTUWriter innerWriter( parameters, timeProvider, innerWriterSs.str() );
      std::vector< VTUWriter > projectedInnerWriter;
      for( unsigned int cam = 0; cam < nCams; ++cam )
	{ untested();
	  std::stringstream ss;
	  ss << "projected_skeleton_frame_" << imageTimeProvider.iteration() << "_" << "cam" << cam << "_";
	  projectedInnerWriter.push_back( VTUWriter( parameters, timeProvider, ss.str() ) );
	}

      if( parameters.get< bool >( "io.midpointswrite" ) )
	{ untested();
	  for( unsigned int cam = 0; cam < nCams; ++cam )
	    { untested();
	      std::stringstream ss2;
	      ss2 << parameters.getString( "io.prefix" )
		  << "/distance_cam" << cam << "_frame_" << std::setfill('0') << std::setw(2) << imageTimeProvider.iteration() << ".csv";
	      std::ofstream file;
	      file.open( ss2.str().c_str() );
	      d.at(cam).printKnown( file );
	      file.close();
	    }
	}

      if( parameters.get< bool >( "io.imagewrite" ) )
	{ untested();
	  for( unsigned int cam = 0; cam < nCams; ++cam )
	    { untested();
	      std::stringstream ss3;
	      ss3 << parameters.getString( "io.prefix" )
		  << "/cam" << cam << "_" << "frame_" << std::setfill('0') << std::setw(2) << imageTimeProvider.iteration() << ".png";
	      cv::imwrite( ss3.str(), frames.at(cam) );
	    }
	}

      std::ofstream energyFile;
      { untested();
	const std::string prefix = parameters.get< std::string >( "io.prefix" );
	std::stringstream energy_ss;
	energy_ss << prefix << "/energy_frame_" << imageTimeProvider.iteration() << ".txt";
	energyFile.open( energy_ss.str().c_str() );
	if( energyFile.is_open() )
	  std::cout << "energy file opened: " << energy_ss.str() << std::endl;
	else
	  throw "unable to open file " + energy_ss.str();
      }

      if( not persistentCreated && get<bool>( parameters, "findgamma.use", false ) )
	{
	  // tolerance
	  const double gssTol = parameters.get<double>("findgamma.tolerance");

	  // optimisation functional
	  const auto f = [&]( const double gammaIn ) -> double
	    {
	      // initialise parameters
	      scheme.initialiseXY( distanceFunction.middle() );
	      persistentGamma = gammaIn;
	      timeProvider.reset();
	      double oldEnergy = scheme.energy();
	      timeProvider.next( deltaT );

	      std::cout << "running with gamma = " << persistentGamma << std::endl;

	      // time loop
	      for( ; timeProvider.time() < endTime ; timeProvider.next( deltaT ) )
		{
		  // prepare
		  scheme.prepare();

		  // solve
		  scheme.solve();

		  // compute energy
		  const double energy = scheme.energy( energyFile );

		  // check stopping criteria
		  const double criteria = scheme.updateSize();
		  if( criteria < energyDiffTol )
		    {
		      return energy;
		    }

		  // update old energy
		  oldEnergy = energy;

		  if( timeProvider.iteration() % 100 == 0 )
		    std::cout << timeProvider.iteration() << ": " << criteria << " / " << energyDiffTol << std::endl;
		}

	      return oldEnergy;
	    };

	  // Golden section search
	  double a = parameters.get<double>("findgamma.min",0.8);
	  double b = parameters.get<double>("findgamma.max",1.2);
	  double c, d;
	  const double gr = ( std::sqrt(5.0) + 1.0 ) / 2.0;

	  c = b - ( b - a ) / gr;
	  d = a + ( b - a ) / gr;

	  double fc = f( c );
	  double fd = f( d );

	  while( std::abs( c - d ) > gssTol )
	    {
	      if( fc < fd )
		b = d;
	      else
		a = c;

	      c = b - ( b - a ) / gr;
	      d = a + ( b - a ) / gr;
	      fc = f( c );
	      fd = f( d );

	      std::cout << "a " << a << ", b " << b << ", c " << c << ", d " << d
			<< " f(c) " << fc << " " << " f(d) " << fd << std::endl;
	    }

	  persistentGamma = ( a + b ) / 2.0;
	}

      double oldEnergy = scheme.energy();

      // compute energy and output important data
      if( timeProvider.time() >= nextWriteTime and doInnerWrite )
	{ untested();
	  innerWriter.writeVtu( scheme.position(), scheme.curvature(), scheme.pressure() );
	  for( unsigned int cam = 0; cam < nCams; ++cam )
	    { untested();
	      Vector pXdata( dim * mesh.N() );
	      PiecewiseLinearFunction< dim > pX( mesh, SubArray< Vector >( pXdata.begin(), pXdata.end() ) );

	      p.at( cam ).projectSolution( scheme.position(), pX );
	      projectedInnerWriter.at( cam ).writeVtu( pX, scheme.curvature(), scheme.pressure() );
	    }

	  std::cout << "time: " << timeProvider.time() << std::endl;
	  std::cout << "energy: " << oldEnergy << std::endl;

	  nextWriteTime += nextWriteTimeStep;
	}
      timeProvider.next( deltaT );

      for( ; timeProvider.time() < endTime ; timeProvider.next( deltaT ) )
	{ itested();
	  // prepare
	  scheme.prepare();

	  // solve
	  scheme.solve();

	  // compute energy
	  const double energy = scheme.energy( energyFile );

	  // compute energy and output important data
	  if( timeProvider.time() >= nextWriteTime and doInnerWrite )
	    { untested();
	      innerWriter.writeVtu( scheme.position(), scheme.curvature(), scheme.pressure() );
	      for( unsigned int cam = 0; cam < nCams; ++cam )
		{ untested();
		  Vector pXdata( dim * mesh.N() );
		  PiecewiseLinearFunction< dim > pX( mesh, SubArray< Vector >( pXdata.begin(), pXdata.end() ) );

		  p.at( cam ).projectSolution( scheme.position(), pX );
		  projectedInnerWriter.at( cam ).writeVtu( pX, scheme.curvature(), scheme.pressure() );
		  // do online plotting
		  {
		    cv::Mat rawCopy;
		    cv::cvtColor( raw.at(cam), rawCopy, CV_GRAY2BGR );
		    // cv::drawContours( rawCopy, contours, -1, cv::Scalar(255,0,255), 1 );

		    int minx = raw.at(cam).cols, miny = raw.at(cam).rows, maxx = 0, maxy = 0;
		    for( const auto& pt : d.at(cam).retKnown() )
		      {
			rawCopy.at< cv::Vec3b >( pt )[2] = (uchar)255;

			int i = pt.x;
			int j = pt.y;
			maxx = std::max( maxx, i+15 );
			minx = std::min( minx, i-15 );
			maxy = std::max( maxy, j+15 );
			miny = std::min( miny, j-15 );
		      }

		    for( unsigned int i = 0; i < pX.N(); ++i )
		      {
			const cv::Point2d pt = pX.evaluate( i );
			rawCopy.at< cv::Vec3b >( pt )[0] = (uchar)255 * ( pX.N() - i ) / pX.N();
			rawCopy.at< cv::Vec3b >( pt )[1] = (uchar)255 * ( i ) / pX.N();
		      }

		    cv::imshow( "output", rawCopy.colRange( minx, maxx ).rowRange( miny, maxy ) );
		    cv::waitKey(10);
		  }

		  double outofplane = 0.0;
		  double meanZ = 0.0;
		  for( unsigned int i = 0; i < scheme.position().N(); ++ i )
		    {
		      const auto xi = scheme.position().evaluate( i );
		      meanZ += xi[2];
		    }
		  meanZ /= static_cast<double>( scheme.position().N() );

		  for( unsigned int i = 0; i < scheme.position().N(); ++ i )
		    {
		      const auto xi = scheme.position().evaluate( i );
		      outofplane += std::abs( xi[2] - meanZ );
		    }
		  outofplane /= static_cast<double>( scheme.position().N() );

		  std::cout << "out of plane: " << outofplane << " meanZ: " << meanZ << std::endl;
		}

	      std::cout << "time: " << timeProvider.time() << std::endl;
	      std::cout << "energy: " << energy
			<< " (change: " << ( oldEnergy - energy )
			<< " / " << scheme.updateSize()
			<< ")"<< std::endl;

	      nextWriteTime += nextWriteTimeStep;
	    }

	  // check stopping criteria
	  const double criteria = scheme.updateSize();
	  if( criteria < energyDiffTol )
	    { untested();
	      std::cout << "breaking: change = " << criteria << "/ " << energyDiffTol << std::endl;
	      break;
	    }

	  // update old energy
	  oldEnergy = energy;
	}

      // output image and known points
      writer.writeVtu( scheme.position(), scheme.curvature(), scheme.pressure() );
      { untested();
	for( unsigned int cam = 0; cam < nCams; ++cam )
	  { untested();
	    Vector pXdata( dim * mesh.N() );
	    PiecewiseLinearFunction< dim > pX( mesh, SubArray< Vector >( pXdata.begin(), pXdata.end() ) );

	    p.at( cam ).projectSolution( scheme.position(), pX );

	    std::stringstream ss;
	    ss << "projected_skeletons_cam" << cam << "_worm_"
	       << std::setfill('0') << std::setw(6) <<  imageTimeProvider.iteration() << ".vtu";
	    projectedWriter.at( cam ).writeVtu( pX, scheme.curvature(), scheme.pressure(), ss.str() );

	    // do online plotting
	    {
	      cv::Mat rawCopy;
	      cv::cvtColor( raw.at(cam), rawCopy, CV_GRAY2BGR );
	      // cv::drawContours( rawCopy, contours, -1, cv::Scalar(255,0,255), 1 );

	      int minx = raw.at(cam).cols, miny = raw.at(cam).rows, maxx = 0, maxy = 0;
	      for( const auto& pt : d.at(cam).retKnown() )
		{
		  rawCopy.at< cv::Vec3b >( pt )[2] = (uchar)255;

		  int i = pt.x;
		  int j = pt.y;
		  maxx = std::max( maxx, i+15 );
		  minx = std::min( minx, i-15 );
		  maxy = std::max( maxy, j+15 );
		  miny = std::min( miny, j-15 );
		}

	      for( unsigned int i = 0; i < pX.N(); ++i )
		{
		  const cv::Point2d pt = pX.evaluate( i );
		  rawCopy.at< cv::Vec3b >( pt )[0] = (uchar)255 * ( pX.N() - i ) / pX.N();
		  rawCopy.at< cv::Vec3b >( pt )[1] = (uchar)255 * ( i ) / pX.N();
		}

	      cv::imshow( "output", rawCopy.colRange( minx, maxx ).rowRange( miny, maxy ) );
	      cv::waitKey(1000);
	    }
	  }
      }

      double finalEnergy = scheme.energy( energyFile, true );

      energyFile.close();

      const double totalTime = (std::clock() - start) / (double)(CLOCKS_PER_SEC);
      const double totalTimeStartOfRun = (std::clock() - startOfRun) / (double)(CLOCKS_PER_SEC);

      std::cout << "** frame " << imageTimeProvider.iteration() << std::endl;
      std::cout << "total time " << totalTime << " sec." << std::endl;
      std::cout << "final energy: " << finalEnergy << std::endl;
      std::cout << "simulation time: " << timeProvider.time() << std::endl;
      std::cout << "simulation iter: " << timeProvider.iteration() << std::endl;
      std::cout << "time since start of run: " << totalTimeStartOfRun << std::endl;

      // copy to persistent container
      if( finalEnergy < parameters.get<double>("term.foundwormtol") )
	{ untested();
	  persistentPosition.assign( scheme.position() );
	  persistentCreated = true;
	}
      else
	{ untested();
	  throw "unable to detect single worm";
	}

      if(get<bool>(parameters, "firstframe_only", false)){ untested();
	break;
      }else{ untested();
      }

      if( imageTimeProvider.iteration() > 10 ){
	error(bDANGER, "#warning breaking after ten\n");
	error(bDANGER, "#warning breaking after ten\n");
	error(bDANGER, "#warning breaking after ten\n");
	error(bDANGER, "#warning breaking after ten\n");
	break;
      }
    }
}

int main( int argc, char **argv )
{
  int i=1;

  std::string dilate_silhouette="";

  ConfigParameters parameters;

  // midline finder setup
  parameters.set("restrict-sil", "1"); // start with known points instead of
                                       // whole worm silhouette
  parameters.set("dilate-sil-radius", "1"); // make worm silhouette a bit bigger
                                            // also applies to restricted sil

  // graph parameters
  parameters.set("similarity-sigma", "4");

  // heat equation solver params
  parameters.set("midline-deltat", "0.001");

  // freakstuff.
  // pixel range (on image) for feature extraction.
  parameters.set("pattern-scale", "15");
  // pixels (in graph) as close as this will be collapsed
  parameters.set("thresh-collapse", "80");
  // no edge between vertices further than this.
  parameters.set("thresh-edge", "30000");

  // heat equation stuff
  parameters.set("initial-known", "0");
  parameters.set("initial-unknown", "0");

  // wormlength
  parameters.set("findgamma.tolerance", "0.01");

  std::cerr << "====================================================\n";
  std::cerr << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n";
  std::cerr << "====================================================\n";
  std::cerr << "synopsis has changed in a backwards incompatible way\n";
  std::cerr << "(sorry)\n";
  std::cerr << "=======old==========================================\n";
  std::cerr << argv[0] << " [<paramfile>] [<parameter> ..]\n";
  std::cerr << " paramfile   keyvaluepairs are initialized here\n";
  std::cerr << " parameter   <KEY>:<VALUE>. set default/override (?)\n";
  std::cerr << "=======new==========================================\n";
  std::cerr << argv[0] << " [<paramfile> | <parameter> ... ]\n";
  std::cerr << " in any order (rightmost wins)\n";
  std::cerr << " parameter   --<KEY>:<VALUE> | --<KEY>=<VALUE>\n";
  std::cerr << " paramfile   merge params from file\n";
  std::cerr << "=======thank you====================================\n";

  // poor mans getopt. yes it's limited, but it works.
  for(; i<argc; ++i){
    trace1("main", argv[i][0]);
    if(argv[i][0]=='-' && argv[i][1]!='-'){
      switch(argv[i][1]){ untested();
	case 'G':
	case 'l':
	  ++i;
	  parameters.set("use-graph", "true");
	  parameters.set("loadfile", argv[i]);
	  break;
	case 't':
	  ++i;
	  parameters.set("midline-deltat", argv[i]);
	  break;
	case 'm': untested();
	  incomplete();
	  // should merge file...
	  break;
	case 's':
	  incomplete();
	///  skip frames...
	  break;
	case 'k': untested();
	  // only compute known points. no curve fitting.
	  parameters.set("known-only", "true");
	  break;
	case 'd':
	  ++i;
	  dilate_silhouette = atoi(argv[i]);
	  break;
	case 'f':
	  parameters.set("firstframe_only", "true");
	  break;
	default:
	  break;
      }
    }else if(argv[i][0]=='-' && argv[i][1]=='-'){
      trace1("got", &argv[i][2]);
      try{
	parse(parameters, &argv[i][2]);
	// ok, must have been : or =
	trace1("parsed", argv[i]);
      }catch(exception_nomatch){
	incomplete(); // BUG, no feedback
	// should only allow override existing params.
	if(i+1<argc){
	  parameters.set( &argv[i][2], argv[i+1]);
	  ++i;
	}else{
	  std::cerr << "something wrong with '" << argv[i] << "'\n";
	  exit(1);
	}
      }
    }else{
      trace1("nodash", argv[i]);
      try{
	parameters.merge_file(argv[i]);
      }catch( ...) { // BUG
	std::cout << "something wrong w/params\n";
	return 1;
      }
    }
  }


  try{
    std::cout << "compile time variables:" << std::endl;
    std::cout << " - WORLD_DIM: " << WORLD_DIM << std::endl;
    std::cout << " - NCAMS: " << NCAMS << std::endl;
    std::cout << " - USE_TIME_ADAPTIVE: " << USE_TIME_ADAPTIVE << std::endl;
    std::cout << " - USE_INTERSECTION_FORCE: " << USE_INTERSECTION_FORCE << std::endl;

    // print variables
    std::cout << "other parameters" << std::endl;
    parameters.dump( std::cout, " - ", " = " );
    std::cout << std::endl;

    // make mesh
    const unsigned int N = parameters.get< unsigned int >( "mesh.N" );
    const double a = parameters.get< double >( "mesh.a" );
    const double b = parameters.get< double >( "mesh.b" );
    Mesh mesh( a, b, N );

    //
    // run algorithm
    skeleton( parameters, mesh );
  }
  catch( const std::string& e ) { untested();
    std::cout << "Exception: " << e << std::endl;
    return 1;
  }
  catch( const char* e ) { untested();
    std::cout << "Exception: " << e << std::endl;
    return 1;
  }
  catch( std::exception& e ) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }
}
// vim:ts=8:sw=2
