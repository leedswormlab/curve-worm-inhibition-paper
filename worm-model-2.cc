// tmp. need to split. later
#undef WORLD_DIM
#define WORLD_DIM 2

#include "config.hh"

#include <iostream>
#include <functional>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>
#include <limits>
#include <getopt.h>
#include <chrono>
#include <thread>

// input parameters
#include "lib/parameter.hh"

// mesh and time helpers
#include "lib/mesh.hh"
#include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/model_lambda.hh"
#include "lib/model_driven.hh"
#include "lib/model_value_driven.hh"
#include "lib/model_neuroMuscular.hh"
#include "lib/worm-moments.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"
#include "lib/options.hh"
#include "lib/writer.hh"
#include "lib/probe.hh"

static TimeProvider getTimeProvider(const ConfigParameters &parameters) {
  const double endTime = parameters.get<double>("time.end");
  const double startTime = parameters.get<double>("time.start");
  const double deltaT = parameters.get<double>("time.deltaT");

  TimeProvider timeProvider(startTime, endTime);
  timeProvider.setDeltaT(deltaT);
  // TODO: check time scale in worm to fix units

  return timeProvider;
}

template<unsigned int dim>
static WriterManager getWriterManager(const ConfigParameters &parameters, const TimeProvider &timeProvider, worm_<dim> &myworm) {
  const double writeTimeStep = parameters.get<double>("io.timestep");
  const std::string outputFolder = parameters.getString("io.prefix");
  WriterManager writerManager(writeTimeStep, outputFolder);

  const std::vector<std::string> &probes = parameters.getStringVector("io.probes");
  for (std::string probe_ : probes) {
    try {
      const std::string filename = parameters.getString("io.filename." + probe_, "/" + probe_ + ".txt");
      writerManager.add(probe_, filename, myworm.new_probe(probe_));
    } catch (std::exception e) {
      message(bWARNING, "specified probe '%s' does not exist\n", probe_.c_str());
    }
  }

  /*
   * TODO: make parameterizable
   * something like this:
   *
   * io.probes:2
   * io.probes.1:energy,gamma
   * io.probes.1.filename:foo.txt
   * io.probes.2:curvature
   * #uses default filename curvature.txt
   *
   * - start with 0 or 1
   * - what to do with vtu, maybe io.probes.vtu:energy,gamma
   *   but what to do with only vtu files without probe data
   */

//  std::vector<probe *> v;
//  v.push_back(myworm.new_probe("gamma"));
//  v.push_back(myworm.new_probe("energy"));
//  writerManager.add("test", "/test.txt", v);

  writerManager.addVTU(outputFolder + "vtu/vtu.txt", timeProvider, myworm); //TODO: implement addVTU

  return writerManager;
}

template<unsigned int dim>
void initScheme(worm_<dim> &myworm, FemScheme<dim> &scheme) {
  scheme.init();
  myworm.initialiseXY(); // TODO: move call to scheme.init()?

  // TODO: clean up, move to scheme.init() ?
  myworm.tr_begin();
  scheme.advance_time();
}

template<unsigned int dim>
void algorithm(ConfigParameters &parameters) {
  TimeProvider timeProvider = getTimeProvider(parameters);

  /*
   * TODO: make worm selectable from parameter file
   *  - move common methods to meshobject
   */
  worm_<dim> myworm(parameters);
  const unsigned int N = parameters.get<unsigned int>("mesh.N");
  myworm.setN(N);

#ifdef WIP_MODEL_SWITCH
  const std::string &modelParameter = parameters.getString("worm.model");
  if (modelParameter == "value") {
    ValueDrivenMuscleModel<dim> model(parameters);
    myworm.setModel(&model);
  } else if (modelParameter == "lambda") {
    LambdaMuscleModel<dim> model(parameters);
    myworm.setModel(&model);
  } else if (modelParameter == "driven") {
    DrivenMuscleModel<dim> model(parameters);
    myworm.setModel(&model);
  } else if (modelParameter == "neuron") {
    NeuroMuscularModel<dim> model(parameters);
    myworm.setModel(&model);
  } else {
    message(bDANGER, "Specified model (%s) not found!\n", modelParameter.c_str());
    exit(1);
  }
#else

#define USE_VALUE_MODEL
#ifdef USE_LAMBDA_MODEL
  using ModelType = LambdaMuscleModel<dim>;
  LambdaMuscleModel<dim> model(parameters);
  std::function<typename ModelType::RangeVectorType(double)> X0;
  std::function<double(double, double)> beta;

  X0 = X0::driven<typename ModelType::RangeVectorType>();

  const double beta0 = parameters.get<double>("model.beta.beta0");
  const double beta1 = parameters.get<double>("model.beta.beta1");
  const double lambda = parameters.get<double>("model.beta.lambda");
  const double omega = parameters.get<double>("model.beta.omega");

  beta = [beta0, beta1, lambda, omega](const double u, const double t) -> double {
    return (beta0 * (1 - u) + beta1 * u) * sin(2.0 * M_PI * u / lambda - 2.0 * M_PI * omega * t);
  };
  model.setX0(&X0);
  model.setBeta(&beta);
  myworm.setModel(&model);
#elif defined(USE_VALUE_MODEL)
  ValueDrivenMuscleModel<dim> model(parameters);
  myworm.setModel(&model);
#endif
#endif

  FemScheme<dim> scheme(timeProvider);
  scheme.push_back(myworm);

  WriterManager writerManager = getWriterManager<dim>(parameters, timeProvider, myworm);

  timeProvider.reset(); // TODO: why reset?
  initScheme(myworm, scheme); // TODO: make part of scheme

  writerManager.write(timeProvider.time());

  message(bDEBUG, "============ ENTERING LOOP ============\n");

  Entity *ent = &myworm;
  while (timeProvider.next()) {
    message(bDEBUG, "------- loop iteration %i -------\n", timeProvider.time() / timeProvider.deltaT());

    scheme.prepare();
    scheme.solve();
    ent->tr_review(); // TODO: clean up, move to scheme.accept() ?
    ent->tr_accept(); // TODO: clean up, move to scheme.accept() ?

    writerManager.write(timeProvider.time());
  }

  message(bDEBUG, "================= END =================\n");
  writerManager.close();
}

static void printParameters(const ConfigParameters &parameters) {
  std::cout << "compile time variables:" << std::endl;
  std::cout << " - WORLD_DIM: " << WORLD_DIM << std::endl;
  std::cout << "other parameters" << std::endl;
  parameters.dump(std::cout, " - ", " = ");
  std::cout << std::endl;
}

static ConfigParameters getParameters(int argc, char **argv, int i) {
  ConfigParameters parameters;
#include "worm_defaults.hh" // TODO: move default values to worm model

  try {
    parameters.merge_file(argv[i]);
  } catch (std::exception &e) {
    message(bDANGER, "exception caught: %s\n", e.what());
    std::exit(1);
  }

  ++i;
  for (; i < argc; ++i) {
    untested();
    try {
      parse(parameters, &argv[i][2]);
    } catch (std::exception &e) {
      message(bDANGER, "exception caught: %s\n", e.what());
      std::exit(1);
    }
  }
  return parameters;
}

static int parseArgumentsToOptions(int argc, char **argv) {
  int option = 0;
  while (option != -1) {
    option = getopt(argc, argv, "DTL");
    switch (option) {
    case 'D':options::set_errorlevel(bDEBUG);
      break;
    case 'T':options::set_errorlevel(bTRACE);
      break;
    case 'L':options::set_errorlevel(bLOG);
      break;
    case '?': {
      message(bWARNING, "usage: %s -[DTL] {parameter file} [parameter overrides]\n", argv[0]);
      std::exit(1);
    }
    default: break;
    }
  }

  return optind;
}

int main(int argc, char **argv) {
  int i = parseArgumentsToOptions(argc, argv);
  ConfigParameters parameters = getParameters(argc, argv, i);
  printParameters(parameters);

  try {
    algorithm<WORLD_DIM>(parameters);
  } catch (std::exception &e) {
    message(bDANGER, "exception caught: %s\n", e.what());
    return 1;
  }

  return 0;
}
//  vim: set cinoptions=>4,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1
//  vim: set autoindent expandtab shiftwidth=2 softtabstop=2 tabstop=8:
