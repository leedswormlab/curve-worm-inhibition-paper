# CURVE-WORM (0.01)

## Installation

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Ubuntu Steps

1. sudo apt-get update
2. git clone <<repository URL>>
3. sudo apt-get install cmake
4. sudo apt-get install libatlas-base-dev
5. Shared SuiteSparse:
    * sudo add-apt-repository ppa:bzindovic/suitesparse-bugfix-1319687
    * sudo apt-get update
    * sudo apt-get install libsuitesparse-dev
6. Static SuiteSparse:
    * sudo apt-get install libsuitesparse-dev
7. sudo apt-get install libopencv-dev python-opencv
8. http://faculty.cse.tamu.edu/davis/suitesparse.html
9. tar -xf archive.tar -C /target/directory
10. sudo apt-get install gfortran
11. Create 'output' folder and 'output-fixed-K' (Capital K) (Name specified in data/model-parameters)
12. Set permissions on 'build' folder (chmod -R 777) (optional)
13. sudo apt-get install paraview

### Mac steps (Incomplete)

###### TODO: Should use MacPorts or Brew ideally (instead of both)...
1. git clone <<repository URL>>
2. brew install cmake
3. chmod -R 777 python folder (TODO: Hack - this shouldn't be necessary)
4. brew install gcc
5. remove rt from CMakeLists.txt
6. sudo port -v install SuiteSparse
7. sudo port install opencv

## Arc3 set up

1. module add mkl suitesparse
2. set -DSuiteSparse_ROOT=/apps/developers/libraries/suitesparse/5.2.0/1/intel-17.0.1/ in config.sh
3. add this as a cmake flag
4. run ./config.sh

## Running

1. paraview &
2. Open output/worm...vtu
3. Play

## Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

## worm-graph ##

extract a dataset from head_clips
./fetch_head_clips.sh # <= gets headclips from a non-backup location on toms machine
cd build
./worms-graph [args] -d dumpfile.fg0 # reads ../head_clips/index. dumps into dumpfile

extra args
-D <radius>    dilate the silhouette by radius (integer). default 0==no-op
-S <radius>    specify FREAK scaling factor. default 15.

## worm-skeleton ##

cd build
./worm-skeleton -G dumpfile.fg0 # uses the data from dumpfile.fg0 to find midpoints
                                # uses scaling factor from dumpfile.fg0

## EXAMPLES ##

$ ./worm-skeleton -G test15.fg0 --kp-only=true \
   ../test-data/worm/parameters -T --dump-kp=1 --io.midpointswrite=1 --end=3
$ ./view-ann -t 1 -T -p 2 -B ../test-data/worm/Cam2.png \
   ../output/test-worm/distance_cam0_frame_09.csv

read known points from csv
$ ./worm-skeleton -G test15.fg0 --kp-only=false    ../test-data/worm/parameters -D --dump-kp=1 --io.midpointswrite=1 --frames-end=10 --annotation.path=../output/test-worm

use annotated firstframe
$ ./worm-skeleton-33 -G 6_21.fg0 --kp-only=false    ../tests/skeleton-test -L --dump-kp=1 --io.midpointswrite=1 --frames-end=10 --annotation.path=../output/test-worm --annotation.cam0=../tests/20160427_trial01_cam1_frame_0.csv --annotation.cam1=../tests/20160427_trial01_cam2_frame_0.csv --annotation.cam2=../tests/20160427_trial01_cam3_frame_0.csv -T |&less
