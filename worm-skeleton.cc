#ifndef WORLD_DIM
#define WORLD_DIM 3
#define NCAMS 3
#endif

#include "config.hh"

#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>
#include <unistd.h>

#define USE_FREAK
#define USE_NESTED_MAP
#define OpenCV_FOUND

// #include <opencv2/imgproc.hpp>

// input parameters
#include "lib/parameter.hh"
#include "lib/model_distance.hh"

// mesh and time helpers
#include "lib/mesh.hh"
#include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/forcefunction_distance.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"
#include "lib/image/io.hh"

#include "lib/trace.hh"
#include "lib/timer.hh"
#include "lib/options.hh"

// graph-based midline finder backend
#include "lib/graph_pointfinder.hh"

// the worm
#include "lib/worm.hh"

// BUG.
typedef worm_<WORLD_DIM> WORM_HERE;

static const std::string default_dilate_silhouette="0";
static const bool _displaystuff=false; // --display...

static const unsigned int dim = WORLD_DIM;
static const unsigned int nCams = NCAMS;

static const bool hack_always_accept=1;

typedef draft::MidlineFinderA<ANNOTATION> kpf_default_type;
void skeleton( ConfigParameters const& parameters, const Mesh &mesh );

int main( int argc, char **argv )
{
  int i=1;

  std::string dilate_silhouette="";

  ConfigParameters parameters;

#include "worm_defaults.hh"

  // midline finder setup
  parameters.set("restrict-sil", "1"); // start with known points instead of
  // whole worm silhouette
  parameters.set("dilate-sil-radius", "1"); // make worm silhouette a bit bigger
  // also applies to restricted sil

  parameters.set("dump-sil", "0");
  parameters.set("sil-only", "0");

  parameters.set("dump-kp", "0");
  parameters.set("kp-only", "0");

  parameters.set("annotation.path", "");
  // BUG. need annotation file pattern
  parameters.set("annotation.cam0", "");
  parameters.set("annotation.cam1", "");
  parameters.set("annotation.cam2", "");
  parameters.set("frames-begin", "0");
  parameters.set("frames-end", "-1");

  for(; i<argc; ++i){
      trace1("main", argv[i][0]);
      if(argv[i][0]=='-' && argv[i][1]!='-'){
          switch(argv[i][1]){ untested();
            case 'G':
            case 'l':
              ++i;
              parameters.set("use-graph", "true");
              parameters.set("loadfile", argv[i]);
              break;
            case 't':
              ++i;
              parameters.set("midline-deltat", argv[i]);
              break;
            case 'm': untested();
                      incomplete();
                      // should merge file...
                      break;
            case 's':
                      incomplete();
                      ///  skip frames...
                      break;
            case 'k': untested();
                      // only compute known points. no curve fitting.
                      parameters.set("known-only", "true");
                      break;
            case 'b': // framenumber begin
                      ++i;
                      parameters.set("frames-begin", argv[i]);
                      break;
            case 'e': // framenumber end
                      ++i;
                      parameters.set("frames-end", argv[i]);
                      break;
            case 'd':
                      ++i;
                      dilate_silhouette = atoi(argv[i]);
                      break;
            case 'f':
                      parameters.set("frames-begin", "0");
                      parameters.set("frames-end", "1");
                      break;
            default:
                      break;
            case 'P':
                      options::set_errorlevel(bPICKY);
                      break;
            case 'L':
                      options::set_errorlevel(bLOG);
                      break;
            case 'D':
                      options::set_errorlevel(bDEBUG);
                      break;
            case 'T':
                      options::set_errorlevel(bTRACE);
                      break;
          }
      }else if(argv[i][0]=='-' && argv[i][1]=='-'){
          message(bTRACE, "got %c\n", argv[i][2]);
          try{
              parse(parameters, &argv[i][2]);
              // ok, must have been : or =
              message(bTRACE, "parsed %s\n", argv[i]);
          }catch(exception_nomatch){ untested();
              incomplete(); // BUG, no feedback
              // should only allow override existing params.
              if(i+1<argc){ untested();
                  parameters.set( &argv[i][2], argv[i+1]);
                  ++i;
              }else{ untested();
                  std::cerr << "something wrong with '" << argv[i] << "'\n";
                  exit(1);
              }
          }
      }else{
          trace1("nodash", argv[i]);
          try{
              parameters.merge_file(argv[i]);
          }catch( ...) { // BUG
              std::cout << "something wrong w/params\n";
              return 1;
          }
      }
  }

  try{
      std::cout << "compile time variables:" << std::endl;
      std::cout << " - WORLD_DIM: " << WORLD_DIM << std::endl;
      std::cout << " - NCAMS: " << NCAMS << std::endl;
//      std::cout << " - USE_TIME_ADAPTIVE: " << USE_TIME_ADAPTIVE << std::endl;
//      std::cout << " - USE_INTERSECTION_FORCE: " << USE_INTERSECTION_FORCE << std::endl;

      // print variables
      std::cout << "other parameters" << std::endl;
      parameters.dump( std::cout, " - ", " = " );
      std::cout << std::endl;

      // make mesh
      const unsigned int N = parameters.get< unsigned int >( "mesh.N" );
      const double a = parameters.get< double >( "mesh.a" );
      const double b = parameters.get< double >( "mesh.b" );
      Mesh mesh( a, b, N );

      //
      // run algorithm
      skeleton( parameters, mesh );
  }
  catch( const std::string& e ) { untested();
      std::cout << "Exception: " << e << std::endl;
      return 1;
  }
  catch( const char* e ) { untested();
      if( std::string(e).compare("done for now") == 0 )
        return 0;

      std::cout << "Exception: " << e << std::endl;
      return 1;
  }
  // catch( std::exception& e ) { untested();
  //     std::cerr << "exception caught: " << e.what() << std::endl;
  //     return 1;
  //}
}

template<class F>
inline double gss(double a, double b, double gssTol, F f)
{ untested();
  // Golden section search
  double c, d;
  const double gr = ( std::sqrt(5.0) + 1.0 ) / 2.0;

  c = b - ( b - a ) / gr;
  d = a + ( b - a ) / gr;

  double fc = f( c );
  double fd = f( d );

  while( std::abs( c - d ) > gssTol ) { untested();
      if( fc < fd ){ untested();
          b = d;
      }else{ untested();
          a = c;
      }

      c = b - ( b - a ) / gr;
      d = a + ( b - a ) / gr;
      fc = f( c );
      fd = f( d );

      message(bDEBUG, "a %e, b %e, c %e, d %e, f(c), %e, f(d) %e\n",
          a, b, c, d, fc, fd);
  }

  return ( a + b ) / 2.0;
}

typedef std::array< cv::Mat, nCams > FramesType;
typedef std::array< cv::VideoCapture, nCams > caps_type;
static void get_frame_from_camera(caps_type& caps, FramesType& frames)
{
  for( unsigned int cam = 0; cam < nCams; ++cam ) {
      // get a new frame from camera
      cv::Mat frame;
      caps.at(cam) >> frame;
      if( frame.empty() ) { untested();
          //< imageTimeProvider.iteration();
          throw exception_corrupt("unable to open feed");
      }

      // convert to b&w and store
      cv::cvtColor(frame, frames.at(cam), CV_BGR2GRAY);
  }
}

static void open_video_streams(caps_type& caps, ConfigParameters const& parameters)
{
    for( unsigned int cam = 0; cam < nCams; ++cam ) {
        // select view
        auto& cap = caps.at(cam);

        // open from filename
        const std::string fn = parameters.getCamString( "video.filename", cam );
        cap.open( fn );

        if(!cap.isOpened()) { untested();
            throw "unable to open video feed " + fn;
        }else{
        }
    }
}

using BaseDistanceFunctionType = VectorDistanceFunction;

#define SKELETON_IMAGES_block3 \
              for( unsigned int cam = 0; cam < nCams; ++cam ) \
                { untested(); \
                  Vector pXdata( dim * mesh.N() ); \
                  PiecewiseLinearFunction< dim-1 > pX( mesh, SubArray< Vector >( pXdata.begin(), pXdata.end() ) ); \
 \
                  p.at( cam ).projectSolution( _scheme.position(), pX ); \
 \
                  /* do online plotting */ \
                    { untested(); \
                      cv::Mat rawCopy;/*cv::Mat::zeros( 2024, 2048, CV_8UC3 );*/ \
                      cv::cvtColor( _frames.at(cam), rawCopy, CV_GRAY2BGR ); \
                      int minx = rawCopy.cols, \
                          miny = rawCopy.rows, maxx = 0, maxy = 0; \
                      for( const auto& pt : d.at(cam).retKnown() ) \
                        { untested(); \
                          rawCopy.at< cv::Vec3b >( pt )[2] = (uchar)255; \
 \
                          int i = pt.x; \
                          int j = pt.y; \
                          maxx = std::max( maxx, i+15 ); \
                          minx = std::min( minx, i-15 ); \
                          maxy = std::max( maxy, j+15 ); \
                          miny = std::min( miny, j-15 ); \
                        } \
 \
                      if( minx < 0 ) minx = 0; \
                      if( maxx > rawCopy.cols-1 ) maxx = rawCopy.cols-1; \
                      if( miny < 0 ) miny = 0; \
                      if( maxy > rawCopy.rows-1 ) maxy = rawCopy.rows-1; \
 \
                      for( unsigned int i = 0; i < pX.N(); ++i ) \
                        { untested(); \
                          const cv::Point2d pt = pX.evaluate( i ); \
                          rawCopy.at< cv::Vec3b >( pt )[0] = (uchar)255 * ( pX.N() - i ) / pX.N(); \
                          rawCopy.at< cv::Vec3b >( pt )[1] = (uchar)255 * ( i ) / pX.N(); \
                        } \
 \
                      cv::imshow( "initial" + std::to_string(cam), rawCopy.colRange( minx, maxx ).rowRange( miny, maxy ) ); \
                    } \
                } \
              cv::waitKey(10);

template<class CONTAINER>
static void write_midpoints(unsigned cam,
    CONTAINER const& distf,
    ConfigParameters const& parameters,
    unsigned frameno )
{
  std::stringstream ss2;
  ss2 << parameters.getString( "io.prefix" )
    << "/distance_cam" << cam << "_frame_" << std::setfill('0')
    << std::setw(0) << frameno << ".csv";
  std::ofstream file;
  message(bLOG, "writing %d known points to %s\n", distf.numKnown(),
      ss2.str().c_str());
  file.open( ss2.str() );
  file << "# video file name: "
    << parameters.getString("video.filename.cam" + std::to_string(cam))
    << "\n"
    << "# frame number: " << frameno+1 /*sic*/ << "\n"
    << "# number of points: " << distf.numKnown() << "\n"
    // << "# unordered: " << distf.ordered()?"true":"false" << "\n"
    << "# unordered: true\n"
    << "#";
  distf.printKnown( file );
  file.close();
}

using BaseProjectionOperatorType = CameraProjectionOperator< dim, 2 >;
using DistanceFunctionType = ProjectedDistanceFunction< BaseDistanceFunctionType,
                                                        BaseProjectionOperatorType,
                                                        nCams >;
typedef DistanceModel< dim, nCams > WormModel;
using FemSchemeType = FemScheme<WORLD_DIM>;

class optimisation_functional {
private:
  // optimisation_functional(){ unreachable(); }
  // optimisation_functional(const optimisation_functional&){ unreachable(); }
public: // construct
  optimisation_functional(
      ConfigParameters const& parm,
      const DistanceFunctionType& df,
      FemSchemeType& fs,
      FramesType const& frms,
      TimeProvider& tp, double endt,
      WORM_HERE& w)
    : _parameters(parm),
      _distanceFunction(df),
      _scheme(fs),
      _frames(frms),
      _timeProvider(tp),
      _endTime(endt),
      _energyDiffTol(parm.get< double >( "term.energydiff" )),
      _worm(w)
    { untested();
    }

private:
  bool next(){ untested();
      _timeProvider.next();
      return _timeProvider.time() < _endTime;
  }
  void advance_time(){ untested();
      incomplete();
  }
public:
  // findgamma stuff.
  double operator()(double persistentGamma){ untested();

      // constructor?
#if WORLD_DIM==3
      _worm.initialiseXY( _distanceFunction );
#else
      _worm.initialiseXY();
#endif

      // initialise parameters
      _timeProvider.reset();
      double oldEnergy = _worm.energy();
      _timeProvider.next();
      double oldCriteria = 1.0;

      std::cout << "running with gamma = " << persistentGamma << std::endl;

      double deltaT_in = _parameters.get< double >( "time.deltaT" );
      _timeProvider.setDeltaT( deltaT_in/100. ); // ...

      while(next()) { untested();
          _scheme.advance_time();
          // _myworm.eval(); // really?
          _scheme.prepare();
          _scheme.solve();
      _timeProvider.setDeltaT( deltaT_in ); // ...

          double energy=_worm.energy();

          // check stopping criteria
          const double criteria = _worm.updateSize();
          if( criteria < _energyDiffTol )
            { untested();
              return energy;
            }

          // update old energy
          oldEnergy = energy;

          if( criteria < oldCriteria * 0.1 ) { untested();
              // message?
              message(bLOG, "%d: %e / %e\n", criteria, _timeProvider.iteration(),
                  _energyDiffTol);
              oldCriteria = criteria;

              // SKELETON_IMAGES_block3
          }else{ untested();
          }
        }

      return oldEnergy;
  };
private:
  ConfigParameters _parameters;
  DistanceFunctionType const& _distanceFunction;
  FemSchemeType& _scheme;
  FramesType const& _frames;
  TimeProvider& _timeProvider;
  double _endTime; // timeprovider ?
  const double _energyDiffTol;
  WORM_HERE& _worm;
}; //optimisation_functional

// TODO: cleanup
#define SOME_IMAGEWRITE_CODE \
  if( parameters.get< bool >( "io.imagewrite" ) ) \
    { untested(); \
      for( unsigned int cam = 0; cam < nCams; ++cam ) \
        { untested(); \
          std::stringstream ss3; \
          ss3 << parameters.getString( "io.prefix" ) \
            << "/cam" << cam << "_" << "frame_" \
            << std::setfill('0') << std::setw(2) \
            << framenumber << ".png"; \
          cv::imwrite( ss3.str(), frames.at(cam) ); \
        } \
    }

#define SOME_ENERGY_CODE \
    { untested(); \
      const std::string prefix = parameters.get< std::string >( "io.prefix" ); \
      std::stringstream energy_ss; \
      energy_ss << prefix << "/energy_frame_" << framenumber << ".txt"; \
      message(bDEBUG, "opening energyfile %s\n", energy_ss.str().c_str() ); \
      energyFile.open( energy_ss.str().c_str() ); \
      if( energyFile.is_open() ){ untested(); \
          message(bLOG, "energy file opened: %s\n", energy_ss.str().c_str()); \
      } else{ untested(); \
          throw "unable to open file " + energy_ss.str(); \
      } \
    }

// TODO: turn into function.
#define SKELETON_SHOW_IMAGES_codeblock1 \
                { untested(); \
                  cv::Mat rawCopy = cv::Mat::zeros( 2024, 2048, CV_8UC3 ); \
                  int minx = rawCopy.cols, \
                      miny = rawCopy.rows, maxx = 0, maxy = 0; \
                  for( const auto& pt : d.at(cam).retKnown() ) \
                    { untested(); \
                      rawCopy.at< cv::Vec3b >( pt )[2] = (uchar)255; \
 \
                      int i = pt.x; \
                      int j = pt.y; \
                      maxx = std::max( maxx, i+15 ); \
                      minx = std::min( minx, i-15 ); \
                      maxy = std::max( maxy, j+15 ); \
                      miny = std::min( miny, j-15 ); \
                    } \
 \
                  if( minx < 0 ) minx = 0; \
                  if( maxx > rawCopy.cols-1 ) maxx = rawCopy.cols-1; \
                  if( miny < 0 ) miny = 0; \
                  if( maxy > rawCopy.rows-1 ) maxy = rawCopy.rows-1; \
 \
                  for( unsigned int i = 0; i < pX.N(); ++i ) \
                    { untested(); \
                      const cv::Point2d pt = pX.evaluate( i ); \
                      rawCopy.at< cv::Vec3b >( pt )[0] = (uchar)255 * ( pX.N() - i ) / pX.N(); \
                      rawCopy.at< cv::Vec3b >( pt )[1] = (uchar)255 * ( i ) / pX.N(); \
                    } \
 \
                  cv::imshow( "initial" + std::to_string(cam), rawCopy.colRange( minx, maxx ).rowRange( miny, maxy ) ); \
                  const std::string camOut = parameters.getString("io.prefix") + "/start_cam" + std::to_string(cam+1) + ".png"; \
                  std::cout << "saving initial image to " << camOut << std::endl; \
                  cv::imwrite( camOut, rawCopy.colRange( minx, maxx ).rowRange( miny, maxy ) ); \
                }

#define SKELETON_SHOW_IMAGES_codeblock2 \
                    { untested(); \
                      cv::Mat rawCopy; /*cv::Mat::zeros( 2024, 2048, CV_8UC3 ); */ \
                      cv::cvtColor( frames.at(cam), rawCopy, CV_GRAY2BGR );\
                      int minx = rawCopy.cols, \
                          miny = rawCopy.rows, maxx = 0, maxy = 0; \
                      for( const auto& pt : d.at(cam).retKnown() ) \
                        { untested(); \
                          rawCopy.at< cv::Vec3b >( pt )[2] = (uchar)255; \
 \
                          int i = pt.x; \
                          int j = pt.y; \
                          maxx = std::max( maxx, i+15 ); \
                          minx = std::min( minx, i-15 ); \
                          maxy = std::max( maxy, j+15 ); \
                          miny = std::min( miny, j-15 ); \
                        } \
 \
                      if( minx < 0 ) minx = 0; \
                      if( maxx > rawCopy.cols-1 ) maxx = rawCopy.cols-1; \
                      if( miny < 0 ) miny = 0; \
                      if( maxy > rawCopy.rows-1 ) maxy = rawCopy.rows-1; \
 \
                      for( unsigned int i = 0; i < pX.N(); ++i ) \
                        { untested(); \
                          const cv::Point2d pt = pX.evaluate( i ); \
                          rawCopy.at< cv::Vec3b >( pt )[0] = (uchar)255 * ( pX.N() - i ) / pX.N(); \
                          rawCopy.at< cv::Vec3b >( pt )[1] = (uchar)255 * ( i ) / pX.N(); \
                        } \
 \
                      cv::imshow( "initial" + std::to_string(cam), rawCopy.colRange( minx, maxx ).rowRange( miny, maxy ) ); \
                    } \
                  cv::waitKey(30); \

// FIXME: use ANNOTATION annotation parser?
template<class F, class K, class S>
void parse_known_points(F& frames, // BUG
    ConfigParameters const& parameters, unsigned cam, unsigned framenumber,
    ANNOTATION& known, cv::Mat& src, cv::Mat& raw2, K&, float, S& fs)
{

  std::string avifilename, dir;
  float ppmm=0;

  try{
      parse_ifstream(fs, ppmm, known,
          avifilename,
          dir, framenumber, &src);

//      known.prune(src);

  }catch (exception_corrupt& e){
      std::cerr << "????" << avifilename << "\n";
      message(bDANGER, "while reading %s...\n", avifilename);
      throw e;
  }

  if(known.size()){
  }else{ untested();
      throw exception_invalid("no points\n");
  }
} // parse_known_points

template<class F, class K, class L>
void fetch_known_points(F& frames, // BUG
    ConfigParameters const& parameters, unsigned cam, unsigned framenumber,
    ANNOTATION& known, cv::Mat& src, cv::Mat& raw2, K& k, float f, L& l,
    std::string path="")
{
  std::string filename;
  if(path!=""){ untested();
    std::string filepattern="distance_cam\%1_frame_\%2.csv";
    size_t p1=filepattern.find("\%1");
    if(p1==std::string::npos){ untested();
        throw exception_invalid(filepattern);
    }else{ untested();
        filepattern.replace(p1,2,std::to_string(cam));
    }
    size_t p2=filepattern.find("\%2");
    if(p2==std::string::npos){ untested();
        throw exception_invalid(filepattern);
    }else{ untested();
        filepattern.replace(p2,2,std::to_string(framenumber));
    }

//    sprintf(buf, filepattern, cam, framenumber);
    message(bDEBUG, "looking for %s in %s\n", filepattern.c_str(), path.c_str());

    filename = findfile(filepattern, path, R_OK);
    message(bDEBUG, "found %s\n", filename.c_str());

  }else{
      filename = parameters.getCamString( "annotation", cam );
  }

  std::ifstream s(filename.c_str());
  if(!s.is_open()){ untested();
      message(bDEBUG, "failed to open %s\n", filename.c_str());
      throw exception_cantfind(filename);
  }else{
  }


  message(bLOG, "reading from %s\n", filename.c_str());
  try{
      parse_known_points(frames, parameters, cam, framenumber,
          known, src, raw2, k, f, s);
  }catch (exception_corrupt& e){
      std::cerr << "ERROR " << filename << "\n";
      throw e;
  }

}

template<class F, class K, class L>
void detect_known_points(F& frames, // BUG
    ConfigParameters const& parameters, unsigned cam, unsigned framenumber,
    ANNOTATION& known, cv::Mat& src, cv::Mat& raw2, K& kpf, float ppmm, L& kpflog)
{ untested();
//    KnownListType known;
    raw2=frames[cam].clone();
    //display_grayscale_frame("bla", src);
    //display_grayscale_frame("bla", raw2);
      { untested();
        // kluge: always compute 3D silhouette, also if not needed.
        // recheck: does it use the silhouette?
        kpf->set_frame(framenumber, cam);
        kpf->find_known_points(src, raw2, known, ppmm);
        kpflog << framenumber << " " << cam << " ";
        kpf->dump_stats(kpflog);
        kpflog << std::flush << "\n";
      }
    // ANNOTATION A;
    // try{ ...
    // }
    // annotated_frame myframe( myView, annotated_frame::_CSV );
    // known = myframe.annotation();
} // detect_known_points

typedef std::vector< BaseProjectionOperatorType >  SceneSetup;

/// later: move to lib/solver.hh
template<unsigned dim>
class skeleton_solver{
  public:
    struct quality_t{
      enum qt{
          q_NONE=0,
          q_SIL=10,   // found a silhouette
          q_SID=11,   // known silhouette component
          q_KP=20,    // have keypoints
          q_OKP=21,   // have ordered keypoints. should imply _orient.
          q_TR=22,    // have triangulation from ordered keypoints
          q_SKEL=30  // have a midline
      };
    public:
      quality_t() : _q(q_NONE) {}
      quality_t(const quality_t& q) = default;
      void reset(){
          _q = q_NONE;
          _orient = false;
      }
      void increase(qt x){ untested();
          if(x>_q){ untested();
              _q = x;
          }else{ untested();
          }
      }
      operator std::string() const{ untested();
          return std::to_string(_q)+(_orient?"o":""); // for now.
      }

      qt _q;
      bool _orient; // know the orientation.
    };
    // using q_SID=quality_t::q_SID;
  private:
    skeleton_solver() = delete;
#if 0
      : _persistentPositionData(0)
      { unreachable(); }
    skeleton_solver(skeleton_solver const&): _persistentPositionData(0)  { unreachable(); }
#endif
  public:
    typedef PiecewiseLinearFunction<dim> midline_type;
  public: // construct
    skeleton_solver( const Mesh& mesh, double const& gamma, double const& time_end,
                     std::string ioprefix, WORM_HERE& w, CAMERA_SETUP<WORLD_DIM> const& cs)
      : _persistentPositionData ( dim * mesh.N() ),
        _persistentPosition(
        mesh, SubArray< Vector >( _persistentPositionData.begin(),
                                  _persistentPositionData.end() ) ),
        _persistentCreated(false),
        _timeProvider(0, time_end),
        _mesh(mesh),
        _persistentGamma(gamma),
        _framenumber(0),
        _iter(0), // timeprovider?!
        _prefix(ioprefix), _worm(w),
        _dfs(NULL), _p(NULL),
        _cs(cs)
    {

#ifdef DO_PROJECTED_WRITES
      for( unsigned int cam = 0; cam < nCams; ++cam ) {
          std::stringstream ss;
          ss << "projected_skeleton_frame_" << _framenumber << "_" << "cam" << cam << "_";
          _projectedInnerWriter.push_back( VTUWriter( _prefix, _timeProvider, ss.str(),
                bDEBUG ) );
      }
#endif
      std::stringstream innerWriterSs;
      _innerWriter=NULL;
      _energyprobe = _worm.new_probe("energy");
      _distsprobe = _worm.new_probe("dists");
      _rkpsprobe = _worm.new_probe("rkps");
    }

  midline_type& persistentPosition(){ return _persistentPosition;}

  void reset_quality(){
      // something went wrong...
      _quality.reset();
  }
  void advance_frame(){
      _quality1 = _quality;
      reset_quality();
  }

  // cleanup, later.
 void findMidline(
    FemSchemeType& scheme,
    DistanceFunctionType const& distanceFunction,
    FramesType const& frames,
    ConfigParameters const& parameters,
    unsigned framenumber,
    SceneSetup const& p,
    bool have_ann_ord,
    WORM_HERE& /* all things? */ worm);

 // BUG: why did we delete it?
 void assign(midline_type const& x){ untested();
   _persistentPosition.assign(x);
   // BUG: need to check if x is good enough.
   _quality.increase(quality_t::q_SKEL);
   _persistentCreated = true;
 }
 using RangeVectorType = RangeVector<WORLD_DIM>;
 void assign(RangeVectorType x){ untested();
     message(bLOG, "initial worm from center. was: %s\n", std::string(_quality).c_str());
     _quality.increase(quality_t::q_SID);
     _middle = x;
 }

 // YUCK. need pp and pp1...
 RangeVectorType const& center1() const{ untested();
     if(_persistentCreated){ untested();
       unsigned N = _persistentPosition.N();
       _middle = _persistentPosition.evaluate( N/2 );
     }else{ untested();
     }
     return _middle;
 }

 void outdata( FemSchemeType const& scheme, VTUWriter& writer,
#ifdef DO_PROJECTED_WRITES
    std::vector< VTUWriter >& projectedWriter,
#endif
    SceneSetup const& p, unsigned framenumber, WORM_HERE const& myworm);

private:
  void inner_outdata(FemSchemeType const& scheme,
      ConfigParameters const&, SceneSetup const&);


  void solvescheme(FemSchemeType&);
public: // const access
   bool haveCenter() const{ untested();
     return _quality._q >= quality_t::q_SID;
   }
   bool haveCenter1() const{
     return _quality1._q >= quality_t::q_SID;
   }
   bool persistentCreated() const{
     return _persistentCreated;
   }
   double const& gamma() const{
     return _persistentGamma;
   }
  unsigned iter() const{ untested();
      return _iter;
  }
  bool next(){ untested();
      return _timeProvider.next();
//      return _timeProvider.time() < _timeProvider.endTime();
  }
  TimeProvider /* const */& timeProvider() /*const*/{
      return _timeProvider;
  }

private:
  Vector _persistentPositionData;
  midline_type _persistentPosition;
  mutable RangeVectorType _middle;
  bool _persistentCreated; // obsolete.
  TimeProvider _timeProvider;
  Mesh const& _mesh;
  double _persistentGamma;
  quality_t _quality;
  quality_t _quality1;
  unsigned _framenumber;
  unsigned _iter;
  unsigned _inner_iter; // not yet
#ifdef DO_PROJECTED_WRITES
  std::vector< VTUWriter > _projectedInnerWriter;
#endif
  VTUWriter* _innerWriter; // yuck
  const std::string _prefix;
  WORM_HERE& _worm;
  probe* _energyprobe;
  probe* _distsprobe;
  probe* _rkpsprobe;

public:
  void set_dfs( ForceFunctionBase<2> const** x){
      _dfs = x;
  }
  void set_prj( CameraProjectionOperator<WORLD_DIM,2>* x){
      _p = x;
  }
  void set_recal_damp(double x){
      _recal_damp = x;
  }
private: // force_stuff
  ForceFunctionBase<2> const** _dfs;
  CameraProjectionOperator<WORLD_DIM, 2>* _p;
  CAMERA_SETUP<WORLD_DIM> const& _cs;
  double _recal_damp;


  // move to camera setup?
  // _cs.recalibrate(_dfs);

         void recalibrate_camera_offsets(){
             if(!_dfs){
                 return;
             }
              // message(bTRACE,"RECAL %E %E %E %E %E %E\n",
              //     _p[0].ooffset()[0],
              //     _p[0].ooffset()[1],
              //     _p[1].ooffset()[0],
              //     _p[1].ooffset()[1],
              //     _p[2].ooffset()[0],
              //     _p[2].ooffset()[1]);

             for(unsigned i=0; i<3; ++i){
                 auto tf=_dfs[i]->total_force();
                 RangeVector<2> d = _cs.cdir(i);
                 d *= scalar_product(d, tf);
                 message(bTRACE, "offset %E %E\n", d[0], d[1]);
                 if(_framenumber){ untested();
                   _p[i].shift_ooffset(_recal_damp * d);
                 }else{ untested();
                   _p[i].shift_ooffset(.1 * d);
                 }
             }
         }

}; // skeleton_solver

template<unsigned dim>
void skeleton_solver<dim>::inner_outdata(
    FemSchemeType const& scheme,
    ConfigParameters const& parameters /*bug*/,
    SceneSetup const& p /* bug */)
{

  if(!_innerWriter){
      incomplete();
      return;
  }

  double energy=_worm.energy();
  _innerWriter->writeVtu( _worm.position(), _worm.curvature(),
      _worm.pressure(), energy, _timeProvider.iteration());

#ifdef DO_PROJECTED_WRITES
  for( unsigned int cam = 0; cam < nCams; ++cam ){
      Vector pXdata( dim * _mesh.N() );
      PiecewiseLinearFunction< dim-1 > pX( _mesh, SubArray< Vector >( pXdata.begin(), pXdata.end() ) );

      p[cam].projectSolution( _worm.position(), pX );
      _projectedInnerWriter[cam].writeVtu( pX, _worm.curvature(), _worm.pressure() );

      // SKELETON_SHOW_IMAGES_codeblock2
  }
#endif

  std::cout << "time: " << _timeProvider.time() << std::endl;
  std::cout << "energy: " << energy
    << " (change: " << ( /*oldEnergy - */energy )
    << " or " << _worm.updateSize()
    << ")"<< std::endl;

  message(bLOG, "time: %f\n", _timeProvider.time());
  message(bLOG, "energy %f\n", energy);

}
/* ============================================================================ */
// femscheme::solve()?!. that is already taken..
// maybe femscheme::iterate?
#if 1
template<unsigned dim>
void skeleton_solver<dim>::solvescheme(FemSchemeType& scheme)
{ untested();
  scheme.prepare(0);
  _inner_iter = 0;
  bool converged = false;
  unsigned maxiter=10; // for now.
  while(_inner_iter<maxiter and !converged){ untested();
    ++_inner_iter;
    // update rhs according to integration method?
    // rhs = (oldRhs + some_operator*solutiontuple)
    scheme.solve();
    converged = scheme.converged();
    break; // incomplete
  }
  if(_iter==maxiter){ untested();
//      incomplete();
  }else{ untested();
  }
}
#endif
/* ============================================================================ */

template<unsigned dim>
void skeleton_solver<dim>::findMidline(
    FemSchemeType& scheme,
    DistanceFunctionType const& distanceFunction,
    FramesType const& frames,
    ConfigParameters const& parameters,
    unsigned framenumber,
    SceneSetup const& p,
    bool have_annotation_and_ordered,
    WORM_HERE& myworm)
{
  _timeProvider.reset();
  bool doInnerWrite = parameters.get< bool >( "io.innerwrite" ); // BUG.
  std::string ioprefix = parameters.getString("io.prefix"); // BUG
  double nextWriteTimeStep = parameters.get< double >( "io.timestep" ); // BUG

  assert(!_innerWriter);
  _framenumber = framenumber;

  if(doInnerWrite){
      assert(_framenumber<99999999);
      _innerWriter = new VTUWriter( ioprefix, _timeProvider, "skeletons_frame_" +
           std::to_string(_framenumber));
      _innerWriter->push_probe(_energyprobe);
      _innerWriter->push_probe(_rkpsprobe);
      _innerWriter->push_probe(_distsprobe);
  }else{
  }

  message(bDEBUG, "findMidline persistentGamma %f, ordann %d fn %d iter %d\n",
    _persistentGamma, have_annotation_and_ordered, _framenumber, _iter);
//    double& persistentGamma = scheme.model().gamma();
  double deltaT_in = parameters.get< double >( "time.deltaT" );
  double deltaT = deltaT_in;
  const double energyDiffTol = parameters.get< double >( "term.energydiff" );

  if(have_annotation_and_ordered){
#if WORLD_DIM==3
    myworm.initialiseXY(distanceFunction);
#else
    // BUG
    myworm.initialiseXY( distanceFunction.middle() );
#endif
    deltaT /= 100.;
    // deltaT *= parameters.get< double >( "worm.time_scale");
  }else if( not _persistentCreated ){ untested();
      // BUG use worm quality
    myworm.initialiseXY( distanceFunction.middle() );
  }else{ untested();
      // BUG: (why) is this necessary?
    myworm.initialiseXY( _persistentPosition );
  }
  _timeProvider.setDeltaT( deltaT ); // ...

  scheme.advance_time();
  assert(_worm.X1().evaluate(0)!=_worm.X1().evaluate(1));

  // io time helpers
  double nextWriteTime=0.;
  if(!doInnerWrite){ untested();
      nextWriteTime = 1e99;
  }else{
  }

  SOME_IMAGEWRITE_CODE


  if( persistentCreated() ){ untested();
      //persistentCreated == something like !initial
      //
      // a curve has been found somehow.
      // curve finding requires ordered annotations? but where?
  } else if (get<bool>( parameters, "findgamma.use", false ) ) { untested();
      // tolerance
      //
      // find_length() ...
      // { untested();
      const double gssTol = parameters.get<double>("findgamma.tolerance");

      _timeProvider.setDeltaT(deltaT);
      const optimisation_functional f(parameters, distanceFunction,
          scheme, frames, _timeProvider, deltaT*100, myworm);

      double a = parameters.get<double>("findgamma.min",0.8);
      double b = parameters.get<double>("findgamma.max",1.2);
      _persistentGamma = gss(a, b, gssTol, f);
      message(bDEBUG, "findMidline raw persistentGamma %f\n", _persistentGamma);

#if WORLD_DIM==3
      myworm.initialiseXY(distanceFunction);
#else
      //BUG
      myworm.initialiseXY( distanceFunction.middle() );
#endif
      _timeProvider.reset();
      _timeProvider.setDeltaT( deltaT_in/100. ); // ...

      double s = parameters.get<double>("findgamma.scaling");
      _persistentGamma *= s;
      // }
  }else{
  }

  assert(_timeProvider.deltaT());

  double oldEnergy = -1.; // not used?
  (void) oldEnergy;

  // findMidline loop
  // compute energy and output important data

  if( _timeProvider.time() >= nextWriteTime ) {
      inner_outdata(scheme, parameters, p);
      nextWriteTime += nextWriteTimeStep;
  } else {
  }
  scheme.reset_counters();

  Entity& e(myworm);

  e.tr_begin();
  scheme.advance_time(); // corrensponding to "uic_now" in s_tr_swp.cc
  e.tr_accept();

  // bug: initial step missing.
  // solvescheme(scheme);

//  assert(deltaT == parameters.get< double >( "time.deltaT" ) );
  message(bDEBUG, "find midline loop timestep %E\n", _timeProvider.deltaT());
  assert(_timeProvider.deltaT());
  message(bTRACE, "find midline loop entering %E\n", myworm.time());
  message(bTRACE, "find midline loop %E\n", myworm.deltat());

  _iter = 0;
  // inner iteration
  while(_timeProvider.next()){
      ++_iter;
//      scheme.advance_time();
      message(bDEBUG, "find midline step %d, at time %E, worm dt %E\n",
          _iter, _timeProvider.time(), myworm.deltat());

//hmmm scheme.solve_iteratively??
      message(bDEBUG, "advanced to %E %E\n", myworm.time(), myworm.deltat());
      scheme.prepare(); // advances time
      scheme.solve();
      e.tr_review();
      e.tr_accept();

      message(bDEBUG, "solved %f\n", myworm.time(), myworm.deltat());

      deltaT = deltaT_in; // HACK.
      _timeProvider.setDeltaT( deltaT );

      // compute energy and output important data
      if( _timeProvider.time() >= nextWriteTime and doInnerWrite ) {
          inner_outdata(scheme, parameters, p);
          nextWriteTime += nextWriteTimeStep;
      }else{ untested();
      }

      double criteria = _worm.updateSize();
      if( criteria < energyDiffTol ) {
          message(bDEBUG, "done %f / %f\n", criteria, energyDiffTol);
      //    break; for now.
      }else{ untested();
//        oldEnergy = energy; ?
      }


      recalibrate_camera_offsets();
    } // timeProvider loop

  if(_innerWriter){
      delete _innerWriter;
      _innerWriter=NULL;
  }else{
  }
} // findMidline

#if 0
static void do_online_plotting...
{ untested();
  cv::Mat rawCopy = cv::Mat::zeros( 2024, 2048, CV_8UC3 );
  int minx = rawCopy.cols,
      miny = rawCopy.rows, maxx = 0, maxy = 0;
  for( const auto& pt : d.at(cam).retKnown() )
    { untested();
      rawCopy.at< cv::Vec3b >( pt )[2] = (uchar)255;

      int i = pt.x;
      int j = pt.y;
      maxx = std::max( maxx, i+15 );
      minx = std::min( minx, i-15 );
      maxy = std::max( maxy, j+15 );
      miny = std::min( miny, j-15 );
    }

  if( minx < 0 ) minx = 0;
  if( maxx > rawCopy.cols-1 ) maxx = rawCopy.cols-1;
  if( miny < 0 ) miny = 0;
  if( maxy > rawCopy.rows-1 ) maxy = rawCopy.rows-1;

  for( unsigned int i = 0; i < pX.N(); ++i )
    { untested();
      const cv::Point2d pt = pX.evaluate( i );
      rawCopy.at< cv::Vec3b >( pt )[0] = (uchar)255 * ( pX.N() - i ) / pX.N();
      rawCopy.at< cv::Vec3b >( pt )[1] = (uchar)255 * ( i ) / pX.N();
    }

#ifdef SKELETON_SHOW_IMAGES
  cv::imshow( "initial" + std::to_string(cam), rawCopy.colRange(
        minx, maxx ).rowRange( miny, maxy ) );
#endif
  const std::string camOut = parameters.getString("io.prefix") + "/final_cam" + std::to_string(cam+1) + ".png";
  std::cout << "saving final image to " << camOut << std::endl;
  cv::imwrite( camOut, rawCopy.colRange( minx, maxx ).rowRange( miny, maxy ) );
} // online plotting
#endif

template<unsigned dim>
void skeleton_solver<dim>::outdata( FemSchemeType const& scheme, VTUWriter& writer,
#ifdef DO_PROJECTED_WRITES
    std::vector< VTUWriter >& projectedWriter,
#endif
    SceneSetup const& p, unsigned framenumber, WORM_HERE const& worm)
{ untested();
  assert(framenumber!=-1u);
  // FIXME: use worm, not scheme.
  writer.writeVtu( _worm.position(), _worm.curvature(), _worm.pressure(),
      _worm.energy(), framenumber, scheme.iterations());

  for( unsigned int cam = 0; cam < nCams; ++cam ) { untested();
      Vector pXdata( dim * _mesh.N() );
      PiecewiseLinearFunction< dim-1 > pX( _mesh, SubArray< Vector >( pXdata.begin(), pXdata.end() ) );

#ifdef DO_PROJECTED_WRITES
      p[cam].projectSolution(_worm.position(), pX );

      std::stringstream ss;
      ss << "projected_skeletons_cam" << cam << "_worm_"
        << std::setfill('0') << std::setw(6)
        << framenumber << ".vtu";
      projectedWriter.at( cam ).writeVtu( pX, _worm.curvature(), _worm.pressure(), ss.str() );
#endif

      if(0){ incomplete();
          // do_online_plotting()
      }else{ untested();
      }

    }
}


void skeleton( ConfigParameters const& parameters, const Mesh &mesh )
{
  // time from start of run
  std::clock_t startOfRun;
  startOfRun = std::clock();

#if 0
  for( unsigned int cam = 0; cam < nCams; ++cam )
    { untested();
      cv::namedWindow("output" + std::to_string(cam), cv::WINDOW_NORMAL );
      cv::resizeWindow("output" + std::to_string(cam), 800, 800);
      cv::moveWindow("output" + std::to_string(cam), 800*cam, 0);
    }
#endif

  bool use_graph=get(parameters, "use-graph", false);
  double deltat=parameters.get<double>("midline-deltat");
  double distance_scale=parameters.get<double>("distance-scale");
  unsigned thresh_edge=parameters.get<unsigned>("thresh-edge");
  unsigned thresh_collapse=parameters.get<unsigned>("thresh-collapse");
  unsigned dilate_sil_radius=parameters.get<unsigned>("dilate-sil-radius");
  int initialguess_known=parameters.get<int>("initial-known");
  int initialguess_unknown=parameters.get<int>("initial-unknown");
  bool restrict_tgt_sil=parameters.get<bool>("restrict-tgt-sil");

  bool dump_kp=parameters.get<bool>("dump-kp");
  bool kp_only=get<bool>(parameters, "kp-only");

  bool dump_sil=parameters.get<bool>("dump-sil");
  bool sil_only=get<bool>(parameters, "sil-only");

  double eucl_base=get<double>(parameters, "graph.eucl-base");

//  double gravity_radius=parameters.get<double>("force.gravity-radius");

//  unsigned frames_begin=0; // not yet

  known_points_finder<ANNOTATION>* kpf=NULL;


  double time_end=parameters.get< double >( "time.end" );

  std::string loadfile=parameters.get<std::string>("loadfile");
  if(!use_graph){ untested();
      message(bLOG, "no graph\n");
      kpf=new kpf_default_type();
  }else{
      message(bLOG, "graph from %s\n", loadfile.c_str());

      typedef draft::neigh_similarity1 weights_type;
      typedef draft::edgeweighted_graph_type1 G;
      //  typedef draft::framesgraph<G, weights_type> fg_type;
      typedef draft::framesgraph_pointfinder<G, weights_type> fgpf_type;

      // bug: pass parameter slice (not implemented yet).
      kpf = new fgpf_type(loadfile, deltat, initialguess_known, initialguess_unknown,
          thresh_collapse, thresh_edge, distance_scale, dilate_sil_radius, restrict_tgt_sil);
      kpf->set_eucl_base(eucl_base);

  }

  caps_type caps;
  open_video_streams(caps, parameters);

  std::string ioprefix = parameters.getString("io.prefix");
  std::string kpflogfilename = ioprefix + "/kpf.log";
  message(bLOG, "logging kpf to %s\n", kpflogfilename.c_str());
  std::ofstream kpflog(kpflogfilename);
  kpflog << "frame cam data" << std::endl;

  // get background frame
  std::array< cv::Mat, nCams > backgrounds;
  for( unsigned int cam = 0; cam < nCams; ++cam )
    {
      const std::string backgroundFn = parameters.getCamString( "video.background", cam );
      message(bLOG, "using background image " + backgroundFn + "\n");
      getBackgroundImage( backgroundFn, backgrounds.at(cam) );
    }

  // read calibration
    SceneSetup p;
    {
      const std::string filename = parameters.getString( "video.calibration" );
      readCalibrationParameters( filename, p );
    }

  for(unsigned c=0; c<3; ++c){
      message(bLOG, "calib cam %d pp/mm %f\n", c, p[c].pixelPerMM());
  }

  // initial time provider
  TimeProvider imageTimeProvider(0, 1e99);

  // initialise output
  message(bDEBUG, "init output\n");
  VTUWriter writer( parameters, imageTimeProvider, "skeletons_" );

#ifdef DO_PROJECTED_WRITES
  std::vector< VTUWriter > projectedWriter;
  for( unsigned int cam = 0; cam < nCams; ++cam ) {
      std::stringstream ss;
      ss << "projected_skeletons_cam" << cam;
      projectedWriter.push_back( VTUWriter( parameters, imageTimeProvider, ss.str() ) );
  }
#endif

  message(bDEBUG, "done init output\n");
  std::cout << std::flush;

  double pGamma = parameters.get<double>("model.gamma");
  std::string const& io_prefix=parameters.getString( "io.prefix" );
  WORM_HERE myworm(parameters /*.cat(worm)*/ );
  myworm.set_time_end(time_end); // hack, controlling forces.
  CAMERA_SETUP<WORLD_DIM> cs(p);
  writer.push_probe(cs.new_probe("offset"));
  ForceFunctionBase<2>* dfs[3];
  ForceFunctionBase<WORLD_DIM>* pdfs[3];
  writer.push_probe(myworm.new_probe("dists"));
  writer.push_probe(myworm.new_probe("rkps"));
  writer.push_probe(myworm.new_probe("gamma"));

  double time_scale = parameters.get<double>( "worm.time_scale" );
  myworm.set_time_scale(time_scale);

  skeleton_solver<WORLD_DIM> S(mesh, pGamma, time_end, ioprefix, myworm, cs);
  S.set_prj(&p.front());
  S.set_dfs(const_cast< const ForceFunctionBase<2>** >( dfs ));
  double recal_damp = parameters.get<double>("recal_damp");
  S.set_recal_damp(recal_damp);

  TimeProvider & timeProvider=S.timeProvider();// bug. const?
  double const& persistentGamma=S.gamma();
  writer.push_probe(myworm.new_probe("energy"));

  unsigned frames_begin = parameters.get<unsigned>("frames-begin");
  unsigned frames_end = parameters.get<unsigned>("frames-end"); // NOT IMPLEMENTED

  unsigned pp_num_ed = parameters.get<unsigned>("pp.num-ed");
  unsigned pp_thinning = parameters.get<unsigned>("pp.thinning");

  bool use_firstframe_ann[3];
  use_firstframe_ann[0]=""!=parameters.getCamString("annotation", 0);
  use_firstframe_ann[1]=""!=parameters.getCamString("annotation", 1);
  use_firstframe_ann[2]=""!=parameters.getCamString("annotation", 2);
  std::string annotation_path=parameters.get<std::string>("annotation.path");

#if 0
  double s = parameters.get<double>("findgamma.scaling");
  persistentGamma *= s;
#endif
  std::vector< BaseDistanceFunctionType > d;
//  std::vector< BaseForceType > f;
  std::vector< cv::Point > unordered_points[3];
  for( unsigned int cam = 0; cam < nCams; ++cam ) {
      d.emplace_back(unordered_points[cam]);
  }

  DistanceFunctionType distanceFunction( d, p );

  // HERE:
  // get inverse image of 3 x origin
  //

  for( unsigned i=0; i<nCams; ++i ) {
    message(bTRACE, "csetup %d %E %E\n", cs._cal_cam[i], cs._cal_dir[i][0], cs._cal_dir[i][1]);
  }


  WormModel model_rw( parameters, distanceFunction,
      timeProvider, imageTimeProvider, persistentGamma );
  auto const& model(model_rw);

  myworm.setModel(&model);
  FemSchemeType scheme( timeProvider );
  scheme.push_back(myworm); // HACK

  const unsigned int N = parameters.get< unsigned int >( "mesh.N" );//?

  myworm.setN(N);

  scheme.init(); // expands.

  // put together worm.
  // BUG: memory leak, df, gf. attach to worm
  for( unsigned int cam = 0; cam < nCams; ++cam ) {
    dfs[cam] = new DistanceForceFunction(unordered_points[cam], myworm.diststrength());
    dfs[cam]->set_label("df"+std::to_string(cam));
    pdfs[cam] = new_ProjectedForceFunction (*dfs[cam], p[cam]);
    myworm._extra_forces.push_back(pdfs[cam]);
    myworm._extra_forces.back()->set_label("pdf"+std::to_string(cam));
  }

  // BUG: memory leak, df, gf. attach to worm
  for( unsigned int cam = 0; cam < nCams; ++cam ) {
    auto rf=new ReverseForceFunction(unordered_points[cam], myworm.rkpstrength());
    rf->set_label("rf"+std::to_string(cam));
    myworm._extra_forces.push_back(new_ProjectedForceFunction (*rf, p[cam]));
    myworm._extra_forces.back()->set_label("prf"+std::to_string(cam));
  }

    // auto gf=new GravityForceFunction(unordered_points[cam], myworm.gravstrength(), gravity_radius);
    // GravityForceFunction df(unordered_points[cam]); maybe?
  myworm.expand(scheme); // HACK

  // myworm.eval(); no. begin() or init() maybe?
  // timeProvider.next( deltaT );??
  message(bTRACE, "================================ entering outer loop\n");
  unsigned skipped=0;

  for( ; ; imageTimeProvider.next( 1./25. ) ) {

      unsigned framenumber=imageTimeProvider.iteration();
      assert(framenumber<9999999);
      message(bTRACE, "================================ outer loop %d\n", framenumber);
      assert(framenumber!=-1u);
      // fixme: part of loop invariant.
      if(framenumber>=frames_end){
          break;
      }else if(framenumber<frames_begin){
          continue;
          for( auto& cap : caps ) { untested();
              cap.grab();
          }
          ++skipped;
          continue; // BUG. seek before main loop
      }else{
      }

      // shift quality, center, persistent
      message(bDEBUG, "skel main loop %d skipped %d\n", imageTimeProvider.iteration(), skipped);

      S.advance_frame();

      // time the loop
      std::clock_t start;
      start = std::clock();

      // get this frame
      FramesType frames;
#if 1
      try{
          get_frame_from_camera(caps, frames);
      }catch(exception_corrupt& e){
          message(bDANGER, "error grabbing stuff in" + std::to_string(imageTimeProvider.iteration()));
          message(bDANGER, e.what());

          return;
          throw e; // not yet?
      }
#endif

#if 1
      // silhouette
      // silhoutteImage( gray, backgrounds.at(cam), srcs.at(cam), 10 );
      std::array< cv::Mat, nCams > srcs;

      // do background subtraction of frame
#endif

      message(bTRACE, "bg sub\n");
      for( unsigned int cam = 0; cam < nCams; ++cam ) {
          frames[cam] = backgrounds[cam] - frames[cam];
      }

      bool silhouette_problem=false;
      // try{ untested();
      if(S.haveCenter1()){ untested();
        message(bLOG, "calling sil with center\n");
        auto middle=S.center1();
        silhouetteImages( p, frames, srcs, 10, &middle);
      }else{
        message(bLOG, "calling sil without center\n");
        silhouetteImages( p, frames, srcs, 10, ( RangeVector<WORLD_DIM> const* ) NULL);
      }
      if(dump_sil) { untested();
          for( unsigned cam=0; cam<nCams; ++cam ) { untested();
              std::string filename=
                ioprefix+"/sil_"+std::to_string(cam)
                +"_"+std::to_string(framenumber)+".png";
              message(bLOG, "dumping sil to %s\n", filename.c_str());
              dump_frame(filename, srcs[cam]);
          }
      }else{
      }

      if(sil_only){ untested();
      // todo: assign center of silhouette if applicable
          incomplete();
          // continue. TODO: cleanup
      }else{
          // ...
      }

      // } catch(exception_corrupt e){ untested();
          //silhouette_problem=true;
      //}

      message(bDEBUG, "done silhouettes\n");

      timeProvider.reset();
//      const double endTime = parameters.get< double >( "time.end" );
      double deltaT = parameters.get< double >( "time.deltaT" );
      timeProvider.setDeltaT( deltaT );
      unsigned have_annotation_and_ordered=0;

      // find keypoints and setup associated distancefunctions
      // FIXME: different place.
      for( unsigned cam=0; cam<nCams; ++cam ) {
              // known_points_in_3D

              ANNOTATION known;
              cv::Mat raw2;
              bool needs_postp=false;
              bool found_kp=false;

              if(!framenumber && use_firstframe_ann[cam]) {
                  message(bLOG, "first annotation from file\n");
                  try{
                      fetch_known_points(frames, parameters, cam,
                      framenumber, known, srcs[cam], raw2, kpf,
                      p[cam].pixelPerMM(), kpflog);
                  }catch(exception_cantfind) { untested();
                      message(bDANGER, "firstframe specified, but not there\n");
                      throw;
                  }
                  raw2=srcs[cam];
                  found_kp = true;
                  have_annotation_and_ordered += known.is_ordered();

                  message(bLOG, "got first from file, %d kp\n", known.size());
                  assert(have_annotation_and_ordered); // for now
                  // if !ordered
                  // known->postprocess(2,1);
              }else if(annotation_path!=""){ untested();
                  message(bDEBUG, "annotation from path\n");
                  try{ untested();
                      known.set_mask(srcs[cam]);
                    fetch_known_points(frames, parameters, cam,
                        framenumber, known, srcs[cam], raw2, kpf,
                        p[cam].pixelPerMM(), kpflog, annotation_path);
                    assert(known.size());
                    found_kp = true;
                    needs_postp = true; // for now.
                    raw2 = srcs[cam];
                    have_annotation_and_ordered = known.is_ordered();
                    
                    if(have_annotation_and_ordered){
                        // incomplete(); // perhaps the data is not tagged right?!
                    }
                  }catch(exception_cantfind) { untested();
                      message(bDEBUG, "no file there.\n");
                  }catch(exception_invalid) { untested();
                      message(bDEBUG, "no points there...?!\n");
                  }
              }else{untested();
              }

              if(found_kp){
              }else if(framenumber){
                  message(bLOG, "kp detect known\n");
                  detect_known_points(frames, parameters, cam,
                      framenumber, known, srcs[cam], raw2, kpf,
                      p[cam].pixelPerMM(), kpflog);

                  needs_postp=true;
              }else if (silhouette_problem){ untested();
                  message(bDANGER, "something went wrong with silhouette\n");
              }else{ untested();
                  message(bLOG, "kp detect first\n");
                  detect_known_points(frames, parameters, cam,
                      framenumber, known, srcs[cam], raw2, kpf,
                      p[cam].pixelPerMM(), kpflog);

                  needs_postp=true;
              }
              if(!known.size()){
                  unreachable();
                  message(bDANGER, "did not find keypoints frame %d cam %d\n", framenumber, cam);
              } else if( parameters.get< bool >( "io.midpointswrite" ) ) {
                  // dump the distance_cam* files.
                  write_midpoints(cam, known, parameters, framenumber);
              }else{ untested();
              }


//              display_pointset_on_frame("known", raw2, known);
              if(needs_postp){ untested();
                  message(bTRACE, "PP\n");
                  known.postprocess(pp_num_ed, pp_thinning);
//                  display_pointset_on_frame("pp'ed", raw2, known);
              }else{
              }

              message(bLOG, "cam %d image has %d points\n", cam, known.size());

              // d.emplace_back(known);

              if(!known.size()){ untested();
                  message(bDANGER, "could not find any known points in %d %d\n", framenumber, cam);
              }else{
                  if(dump_kp) {
                      std::string filename=
                        ioprefix+"/kp_"+std::to_string(cam)
                        +"_"+std::to_string(framenumber)+".png";
                      message(bLOG, "dumping redpoints to %s\n", filename.c_str());
                      dump_pointset_on_frame(filename, raw2, known);
                  }else{ untested();
                  }
              }

// BUG: proper type for d.
#if USE_GRAVITY
              d.emplace_back(known.begin(), known.end(), gravity_scale, gravity_radius);
#else
//              d.emplace_back(known.begin(), known.end());
//
              unordered_points[cam].assign(known.begin(), known.end());

#endif
      } // keypoint setup

      if(have_annotation_and_ordered<3){ untested();
        have_annotation_and_ordered=0;
      }else{
          // will triangulate in findMidline
      }

      if(kp_only){ untested();
      }else{
          // do the actual work.
          //
            S.findMidline(scheme, distanceFunction,
                frames, parameters, framenumber, p,
                have_annotation_and_ordered, myworm);

          double finalEnergy = myworm.energy();
          S.outdata(scheme, writer, 
#ifdef DO_PROJECTED_WRITES
              projectedWriter,
#endif
              p, framenumber, myworm);

          double totalTime = (std::clock() - start) / (double)(CLOCKS_PER_SEC);
          double totalTimeStartOfRun = (std::clock() - startOfRun) / (double)(CLOCKS_PER_SEC);

          message(bLOG,"** frame %d\n", framenumber);
          message(bLOG,"total time %E seconds\n", totalTime);
          message(bLOG,"final energy: %E\n", finalEnergy);
          message(bLOG,"simulation time: %E\n", timeProvider.time());
          message(bLOG,"iterations %d\n", S.iter());
          message(bLOG,"time since start of run: %E\n", totalTimeStartOfRun);
          for(unsigned i=0; i<3; ++i){
            message(bDEBUG,"force %d %E, %E\n", i, p[i].ooffset()[0], p[i].ooffset()[1]);
          }

          // copy to persistent container
          if(hack_always_accept){
              S.assign( myworm.position() ); // why this?!
          }else if( finalEnergy < parameters.get<double>("term.foundwormtol") ) { itested();
              S.assign( myworm.position() );
              // persistentCreated = true;
          } else { untested();
              S.reset_quality();
              throw "unable to detect single worm";
          }
      }

  } // frames loop

  delete kpf;
} // skeleton

//  vim: set cinoptions=>4,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1
//  vim: set autoindent expandtab shiftwidth=2 softtabstop=2 tabstop=8:
