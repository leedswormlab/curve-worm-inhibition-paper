

parameters.set("io.timestep", "1");
// weight parameters
parameters.set("similarity-sigma", "4");
parameters.set("distance-scale", ".01154700394041675210");
#ifdef USE_FREAK
parameters.set("pattern-scale", "20");
// pixels as close as this will be collapsed
parameters.set("thresh-collapse", "80");
// no edge between vertices further than this.
parameters.set("thresh-edge", "30000");
#else // perhaps obsolete.
	parameters.set("thresh-collapse", "40");
	parameters.set("thresh-edge", "250");
#endif

// silhouette flags for annotated/learning frames
parameters.set("restrict-sil", "0");
parameters.set("dilate-sil", "0");

// frame annotation. look here.
parameters.set("annotation.path", "");
// parameters.set("annotation.pattern", "..%d %d ...");

// ... when targetting unknown worms
parameters.set("restrict-tgt-sil", "1");
parameters.set("dilate-tgt-sil", "5");

// heat equation solver params (used?)
parameters.set("midline-deltat", "0.001");

// freakstuff.
// pixel range (on image) for feature extraction.
parameters.set("pattern-scale", "15");
// pixels (in graph) as close as this will be collapsed
// heat equation stuff
parameters.set("initial-known", "0");
parameters.set("initial-unknown", "0");
parameters.set("graph.eucl-base", ".0000003059023205018");

// wormlength
parameters.set("findgamma.tolerance", "0.01");
parameters.set("findgamma.scaling", ".9");
parameters.set("gamma.scaling", ".9");
parameters.set("gamma.contract-tau", "1."); // 1e-2?
parameters.set("gamma.contract-scale", "0."); //1?

parameters.set("loadfile", "");

// frame postprocessing
parameters.set("pp.num-ed", "0");
parameters.set("pp.thinning", "0");

// model
parameters.set("model.e0", "1.");
parameters.set("model.e2", "0.");
parameters.set("model.et", "0.");
parameters.set("model.diriclet-curv", "true");

// force models
parameters.set("force.dist0", "1.");
parameters.set("force.dist1", "0.");
parameters.set("force.dist2", "0.");
parameters.set("force.rkp0", "1.");
parameters.set("force.rkp1", "0.");
parameters.set("force.rkp2", "0.");
parameters.set("force.gravity0", "0.");
parameters.set("force.gravity1", "0.");
parameters.set("force.gravity2", "0.");
parameters.set("force.gravity-radius", "1.");


//video
parameters.set("video.filename.cam0", "");
parameters.set("video.filename.cam1", "");
parameters.set("video.filename.cam2", "");
parameters.set("video.background.cam0", "");
parameters.set("video.background.cam1", "");
parameters.set("video.background.cam2", "");
parameters.set("video.calibration", "");

parameters.set("recal_damp", ".1");

parameters.set("worm.time_scale", "1.");
