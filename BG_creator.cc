#ifdef OpenCV_FOUND
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/imgcodecs.hpp"
#endif

#include <cassert>
#include <ctime>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <utility>
#include <vector>
#include <string>

#include "lib/parameter.hh"

struct CommandLineArgumentParser
{
  CommandLineArgumentParser( int argc, char** argv )
  {
    if( not ( argc >= 3 ) )
      {
	usage( argv );
	abort();
      }

    inputVideo = argv[1];
    outputImage = argv[2];

    // set defaults
    method = "mean";
    methodCase = 0;
    maxFrame = -1;
    frameStep = -1;

    // over write from parameters
    for( int i = 3; i < argc; ++i )
      {
	std::istringstream is_line( argv[i] );
	std::string key;
	if( std::getline( is_line, key, ':' ) )
	  {
	    std::string value;
	    if( std::getline( is_line, value ) )
	      {
		if( key.compare( "method" ) == 0 )
		  {
		    if( value.compare( "mean" ) == 0 )
		      {
			method = "mean";
			methodCase = 0;
		      }
		    else if( value.compare( "max" ) == 0 )
		      {
			method = "max";
			methodCase = 1;
		      }
		    else if( value.compare( "min" ) == 0 )
		      {
			method = "min";
			methodCase = 2;
		      }
		    else
		      {
			std::cerr << "unknown method type. choose from:\n"
				  << " mean, max, min" << std::endl;
			abort();
		      }
		  }
		else if( key.compare( "maxframe" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> maxFrame;
		  }
		else if( key.compare( "framestep" ) == 0 )
		  {
		    std::stringstream value_ss( value );
		    value_ss >> frameStep;
		  }
		else
		  {
		    std::cerr << "unknown parameter key. choose from:\n"
			      << " method, maxframe, framestep" << std::endl;
		    abort();
		  }
	      }
	  }
      }

    std::cout << "- input video: " << inputVideo << "\n"
	      << "- output image: " << outputImage << "\n"
	      << "- method: " << method << " (" << methodCase << ")" << "\n"
	      << "- max frame: " << maxFrame << "\n"
	      << "- frame step: " << frameStep << std::endl;
  }

  void usage( char** argv ) const
  {
    std::cout << "usage: " << std::endl;
    std::cout << " " << argv[0] << " {input video} {output background image} {..options..}" << std::endl;
  }

  std::string inputVideo;
  std::string outputImage;

  std::string method;
  int methodCase;
  int maxFrame;
  int frameStep;
};

#ifdef OpenCV_FOUND
void silhoutteImage( const cv::Mat& input, const cv::Mat& background, cv::Mat& output, const double minValue )
{
  // subtract background image
  output = background - input;

  // set values less than minValue to 0
  threshold( output, output, minValue, 255, THRESH_TOZERO );

  // find threshold to binary using Otsu's method
  threshold( output, output, 0, 255, CV_THRESH_BINARY | CV_THRESH_OTSU );
}

void invert( cv::Mat& mat )
{
  // invert each pixel in mat
  for( int i = 0; i < mat.rows; ++i)
    {
      uchar* p = mat.ptr<uchar>(i);

      for ( int j = 0; j < mat.cols; ++j)
	{
	  const int pj = 255-(int) p[j];
	  p[j] = (uchar) pj;
	}
    }
}

static void BG_create( const std::string videoFn, const std::string BG_image,
		const int methodCase, const int maxFrame, const int frameStep )
{
  // extract file name info
  std::cout << "using video stream " << videoFn << std::endl;

  // open video stream
  VideoCapture cap( videoFn );

  // check if we succeeded
  if(!cap.isOpened())
    {
      throw "video capture not opened";
    }

  std::cout << videoFn << " opened successfully." << std::endl;

  // frame loop
  std::clock_t start;
  start = std::clock();

  int nextRead = 0;

  cv::Mat acc;
  int count;
  for( count = 1; ( maxFrame >= 0 ) ? ( count < maxFrame ) : true; ++count )
    {
      if( count % 100 == 0 )
	std::cout << "frame: " << count << std::endl;

      // read frame
      cv::Mat frame;
      if( count > nextRead )
	{
	  cap >> frame;
	  nextRead += frameStep;
	}
      else
	{
	  cap.grab();
	}

	//	int rows = 0, cols = 0;

      if( frame.empty() )
	{
	  std::cout << "empty frame" << std::endl;
	  break;
	}

      // convert to grayscale
      cv::Mat gray;
      cvtColor( frame, gray, CV_BGR2GRAY);

      if( count == 1 )
      	{
      	  // extract rows and cols
      	  // rows = gray.rows;
      	  // cols = gray.cols;

      	  // create copy
	  if( methodCase == 0 )
	    gray.convertTo( acc, CV_64F );
	  else
	    gray.copyTo( acc );
      	}
      else
      	{
	  switch( methodCase )
	    {
	    case 0: // mean
	      accumulate( gray, acc );
	      break;
	    case 1: // max
	      max( gray, acc, acc );
	      break;
	    case 2: // min
	      min( gray, acc, acc );
	      break;
	    }
      	}
    }

  // convert and write out
  cv::Mat ttt = ( methodCase == 0 ) ? ( acc / count ) : acc;
  ttt.convertTo( ttt, CV_8U );
  imwrite( BG_image, ttt );

  std::cout << "background image written to " << BG_image << std::endl;

  // print run time
  const double totalTime = (std::clock() - start) / (double)(CLOCKS_PER_SEC);
  std::cout << "total time " << totalTime << " sec." << std::endl;
}
#else
void algorithm( const std::string videoFn, const std::string BG_image,
		const int methodCase, const int maxFrame, const int frameStep )
{
   std::cerr <<"need opencv\n";
}
#endif

int main(int argc, char** argv)
{
  CommandLineArgumentParser parser( argc, argv );

#ifdef OpenCV_FOUND
  try {
    BG_create( parser.inputVideo, parser.outputImage, parser.methodCase, parser.maxFrame, parser.frameStep );
    return 0;
  }
  catch( const char* msg ) {
    std::cerr << "Exception: " << msg << std::endl;
    return 1;
  }
#endif
}


// vim:ts=8:
