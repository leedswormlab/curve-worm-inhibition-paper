#!/bin/bash -x

# fix later.
top_srcdir=../..
srcdir=$top_srcdir/tests

framecvs=${top_srcdir}/head_clips/20160427_trial02_cam3_frame_0.csv
graph=$srcdir/test20.fg0


../worm-graph -T -l "${graph}" \
	--eval-csv="$framecvs" \
	--verbose --display=0 | grep -v took
