#include <iostream>
#include <ctime>
#include "../lib/mesh.hh"

int main() {

  double c0 = 0;
  double c1 = 0;
  double c2 = 0;
  double c3 = 0;

  for( int i = 0; i < 1000; ++i ) {
    double t = (double)i/ 1000;
    const unsigned int N = 128;
    Mesh mesh( t, t+1.0, N );

    std::clock_t start = std::clock();

    {
      double sum = 0;
      for( const auto& vertex : mesh.vertices() ) {
	const auto u = vertex.u();
	sum += u;
      }

      double sum_h = 0;
      for( const auto& element : mesh.elements() ) {
	const auto h = element.h();
	sum_h += h;
      }

      std::cout << sum << " " << sum_h <<std::endl;
    }

    const double duration =  ( std::clock() - start ) / (double)( CLOCKS_PER_SEC );

    std::clock_t start_0 = std::clock();

    {
      double sum = 0;
      for( auto&& vertex : mesh.vertices() ) {
	const auto u = vertex.u();
	sum += u;
      }

      double sum_h = 0;
      for( auto&& element : mesh.elements() ) {
	const auto h = element.h();
	sum_h += h;
      }

      std::cout << sum << " " << sum_h <<std::endl;
    }

    const double duration_0 =  ( std::clock() - start_0 ) / (double)( CLOCKS_PER_SEC );

    std::clock_t start_2 = std::clock();

    {

      double sum_h = 0;
      const double h = 1.0 / ( N - 1.0 );
      for( unsigned int e = 0; e < N-1; ++e ) {
	sum_h += h;
      }
      double sum = 0;
      double u = 0;
      for( unsigned int i = 0; i < N; ++i ) {
	sum += u;
	u += h;
      }

      std::cout << sum << " " << sum_h <<std::endl;
    }

    const double duration2 = ( std::clock() - start_2 ) / (double)( CLOCKS_PER_SEC );

    std::clock_t start_3 = std::clock();

    {
      double sum = 0;
      const auto end = mesh.vertices().end();
      for( auto it = mesh.vertices().begin(); it != end;
	   ++it ) {
	const auto &v = *it;
	sum += v.u();
      }

      double sum_h = 0;
      const auto eend = mesh.elements().end();
      for( auto it = mesh.elements().begin(); it != eend;
	   ++it ) {
	const auto &e = *it;
	sum_h += e.h();
      }

      std::cout << sum << " " << sum_h <<std::endl;
    }

    const double duration_3 =  ( std::clock() - start_3 ) / (double)( CLOCKS_PER_SEC );

    c0 += duration;
    c1 += duration_0;
    c2 += duration2;
    c3 += duration_3;
  }

  std::cout << c0 << " / " << c1 << " / " << c2 << "/" << c3 << std::endl;
  std::cout << c0/c1 << " / " << "X" << "/" << c2/c1 << "/" << c3/c1 << std::endl;
  std::cout << c0/c2 << " / " << c1/c2 << " / " << "X" << c3/c2 << std::endl;
  std::cout << c0/c3 << " / " << c1/c3 << " / " << c2/c3 << "X" << std::endl;

  return 0;
}
