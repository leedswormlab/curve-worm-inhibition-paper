#include "../lib/entity/entity.hh"
#include "../lib/entity/world-entity.hh"

class Loud_entity : public Entity_base {
public:
  Loud_entity( const std::string name )
    : Entity_base( name ) {}

  // called at the start of the simulation before any others
  virtual void load() {
    std::cout << long_label() << " load" << std::endl;
  }
  // called once per simulation before the time loop
  virtual void tr_begin() { untested();
    std::cout << long_label() << " tr_begin" << std::endl;
  }
  // called once per simulation after tr_begin
  virtual void tr_begin_accept() { untested();
    std::cout << long_label() << " tr_begin_accept" << std::endl;
  }
  // called once per time step
  virtual void tr_advance() { untested();
    std::cout << long_label() << " tr_advance" << std::endl;
  }
  // called once per solver iteration - does the assembly
  virtual void tr_load() { untested();
    std::cout << long_label() << " tr_load" << std::endl;
  }
  // called once per solver iteration - does the solve
  virtual void eval() { untested();
    std::cout << long_label() << " eval" << std::endl;
  }
  // called after the solver iteration
  virtual double tr_review() {
    std::cout << long_label() << " tr_review" << std::endl;
    return 1e99; }
  // called at the end of the timestep
  virtual void tr_accept() {
    std::cout << long_label() << " tr_accept" << std::endl;
  }

};

int main() {
  try {
    World_entity world;
    using pEntity_base = typename World_entity :: pEntity_base;

    pEntity_base e0 = std::make_shared< Loud_entity >( "e0" );
    world.add_sub_entity( e0 );
    pEntity_base e1 = std::make_shared< Loud_entity >( "e1" );
    world.add_sub_entity( e1 );
    pEntity_base e2 = std::make_shared< Loud_entity >( "e2" );
    world.add_sub_entity( e2 );

    std::cout << "testing " << world.long_label() << std::endl;

    world.load();
    world.tr_begin();
    world.tr_begin_accept();

    { // within time loop
      world.tr_advance();

      { // within solver loop
	world.tr_load();
	world.eval();
      }

      world.tr_review();
      world.tr_accept();
    }

    return 0;
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }
}
