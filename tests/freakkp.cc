//
// (c) 2017 scs.leeds.ac.uk
// Author: Felix Salfelder 2017
//
// a worm-graph thing

#include "config.hh"
#include <boost/graph/properties.hpp>
#include <boost/functional/hash.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <opencv2/features2d.hpp>
//#include <opencv2/features2d_manual.hpp>

#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>

// #include <opencv2/opencv.hpp> // c++ headers
#include "trace.hh"
#include "io_misc.hh"

#include "lib/parameter.hh"
#include "lib/matrix_graph.hh"
#include "lib/error.hh"
#include "lib/image/util.hh"

// mesh and time helpers
#include "lib/mesh.hh"
#include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"
#include "lib/image/freak.hh" // FIXME: use FREAKMAP

#include "lib/error.hh"
#include "lib/options.hh"

using cv::xfeatures2d::FREAK;

int main( int argc, char **argv )
{ itested();
	ConfigParameters parameters;
  parameters.set("imgfile", "bg.png");
  parameters.set("bgname", "");

  unsigned x=0;
  unsigned y=0;
	int i=1;
	// poor mans getopt. yes it's limited, but it works.
	unsigned framenumber=0;
	float patternScale=22.0f;
	for(; i<argc; ++i){ itested();
		trace1("main", argv[i][0]);
		if(argv[i][0]=='-' && argv[i][1]!='-'){ itested();
			switch(argv[i][1]){ untested();
				case 'x':
					++i;
					x = atoi(argv[i]);
					break;
				case 'y':
					++i;
					y = atoi(argv[i]);
					break;
				case 's': untested();
					++i;
					patternScale = atof(argv[i]);
					break;
				case 'T': untested();
					options::errorlevel = bTRACE;
					break;
			}
		}else if(argv[i][0]=='-' && argv[i][1]=='-'){ untested();
			try{ untested();
				parse(parameters, &argv[i][2]); // only works with "="
			}catch(exception_nomatch){ untested();
				incomplete();
			  std::cerr << "something wrong with '" << argv[i] << "'\n";
			  exit(1);
			}

		}else{ itested();
			break;
		}
	}
	if(i!=argc-1){ untested();
		std::cerr << "usage: \n";
		std::cerr << argv[0] << "[options] <image_filename>\n";
		exit(1);
	}else{ itested();
		parameters.set("imgfile", argv[i]);
	}
	std::string imgfile=parameters.get<std::string>("imgfile");

	cv::Mat M;
	// get_grayscale_image(imgfile, M);
	//
	message(bTRACE, "image %s frame %d\n", imgfile.c_str(), framenumber);

	getframefromfile(imgfile, M, framenumber);

	std::string bgname=parameters.get<std::string>("bgname");
	if(bgname!=""){ untested();
		cv::Mat B;
		getframefromfile(bgname, B, framenumber);
		M = M - B;
	}else{ untested();
	}

//	display_grayscale_frame("1", M);

	std::vector<cv::KeyPoint> keypoints;
	bool orientationNormalized=true;// Enable orientation normalization.
	bool scaleNormalized=false; // Enable scale normalization.
	unsigned nOctaves=1; // Number of octaves covered by the detected keypoints.

	auto _backend=FREAK::create( orientationNormalized, scaleNormalized,
			patternScale, nOctaves);

//	cv::SurfFeatureDetector detector(2000,4);
	auto detector = cv::FastFeatureDetector::create();
//	auto detector = cv::StarDetector::create();
//	auto detector = cv::SimpleBlobDetector::create();

	message(bTRACE, "keypoint %d, %d\n", x, y);

	detector->detect(M, keypoints);

	for(auto const& k: keypoints){
		std::cout << k.pt.x << " " << k.pt.y << "\n";
	}

	display_pointset_on_frame("kp", M, keypoints, 255, 900, 300, 1100, 450);

}
