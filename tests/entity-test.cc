#include "../lib/entity/entity.hh"

int main() {
  try {
    Entity_base my_entity("my_entity");

    std::cout << "testing " << my_entity.long_label() << std::endl;

    my_entity.load();
    my_entity.tr_begin();
    my_entity.tr_begin_accept();

    { // within time loop
      my_entity.tr_advance();

      { // within solver loop
	my_entity.tr_load();
	my_entity.eval();
      }

      my_entity.tr_review();
      my_entity.tr_accept();
    }

    return 0;
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }
}
