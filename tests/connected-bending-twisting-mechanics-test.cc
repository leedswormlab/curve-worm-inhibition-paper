#define CONSTANT_RADIUS 1

#include "../lib/mesh.hh"
#include "../lib/timeprovider.hh"
#include "../lib/entity/world-entity.hh"
#include "../lib/entity/control/constant-control.hh"
#include "../lib/entity/mechanics/bending-twisting-mechanics-entity.hh"
#include "../lib/entity/mechanics/problem.hh"
#include "../lib/parameter.hh"

int main() {
  try {
    Parameters::set( "control.constant.test_case", "1" );
    Parameters::set( "control.constant.beta_0", "10.0" );
    Parameters::set( "control.constant.beta_1", "6.0" );
    Parameters::set( "control.constant.gamma0", "0.65" );
    Parameters::set( "mechanics.problem.K", "7.0" );
    Parameters::set( "mechanics.problem.K_rot", "1.0" );
    Parameters::set( "mechanics.problem.I2_eps", "1.0e-4" );
    Parameters::set( "mechanics.problem.e", "1.0" );
    Parameters::set( "mechanics.problem.g", "1.0" );
    Parameters::set( "mechanics.problem.mu", "1.0" );
    Parameters::set( "mechanics.problem.zeta", "1.0" );
    Parameters::set( "mechanics.problem.gamma", "1.0" );
    Parameters::set( "mechanics.problem.initial_posture", "line" );
    Parameters::set( "mechanics.problem.frame_update", "double_rotation" );
    Parameters::set( "mechanics.problem.rotation_cutoff", "1.0e-12" );
    Parameters::set( "mechanics.problem.constant_radius", "1" );

    Mesh mesh(0.0, 1.0, 21);
    TimeProvider time_provider( 0.0, 3.0e-4 );
    time_provider.setDeltaT( 1.0e-4 );

    World_entity world;
    using pEntity_base = typename World_entity :: pEntity_base;

    pEntity_base control = std::make_shared<Constant_control_entity>( "control",
									mesh,
									time_provider );
    world.add_sub_entity(control);

    pEntity_base mechanics = std::make_shared<Bending_twisting_mechanics_entity>("mechanics", mesh, time_provider );
    world.add_sub_entity(mechanics);

    mechanics->connect_probe( control->get_probe( "control.beta" ) );
    mechanics->connect_probe( control->get_probe( "control.alpha0" ) );

    using ComProbe = typename Bending_twisting_mechanics_entity :: Centre_of_mass_probe;
    auto com_probe_ptr = std::dynamic_pointer_cast< const ComProbe >( mechanics->get_probe( "mechanics.centre_of_mass" ) );
    assert( com_probe_ptr );

    world.load();
    world.tr_begin();

    for( ; time_provider.time() < time_provider.endTime(); time_provider.next() ){
      // within time loop
      world.tr_advance();

      { // within solver loop
	world.tr_load();
	world.eval();
      }

      world.tr_review();
      world.tr_accept();

      std::cout << com_probe_ptr->evaluate() << std::endl;
    }

    return 0;
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }
}
