#include <cassert>
#include <array>
#include <cmath>
#include <iostream>

#include "../lib/vector.hh"
#include "../lib/solver/bicgstab.hh"

struct Matrix {
  template< class V >
  void matvec( const V& x, V& y ) const {
    assert( x.size() == y.size() );
   for( unsigned j = 0; j < x.size(); ++j ) {
      const double a = static_cast<double>(j+1);
      y.at(j) = a * x.at(j);
    }
  }
};

struct Preconditioner {
  template< class V >
  void inv_matvec( const V& x, V& y ) const {
    assert( x.size() == y.size() );
    // for( unsigned j = 0; j < x.size(); ++j ) {
    //   const double a = static_cast<double>(j+1)
    //   * ( 1.0 + 1.0e-6*std::pow(-1,j) );
    //   y.at(j) = (1.0/a) * x.at(j);
    // }
    y = x;
  }
};

int main(int argc, char** argv) {
  const unsigned N = (argc > 1) ? std::atoi(argv[1]) : 100;
  Vector x(N), b(N);
  for( unsigned j = 0; j < N; ++j ) {
    b[j] = static_cast<double>(j+1);
  }

  Matrix A;
  Preconditioner M;

  const double tol = 1.0e-14;
  const int max_iter = 1000;
  int it = bicgstab( A, M, b, x, tol, max_iter, true );

  if( N < 1000 ) {
    std::cout << "solution:   " << x << "\n";
  }
  std::cout << "iterations: " << it << "\n";

  assert( it < max_iter );
  return 0;
}
