#define FLAT_WORM 1
#define CONSTANT_RADIUS 1

#ifndef WORLDDIM
#define WORLDDIM 3
#endif

#include "../lib/mesh.hh"
#include "../lib/timeprovider.hh"
#include "../lib/entity/mechanics/bending-twisting-mechanics-entity.hh"
#include "../lib/entity/mechanics/problem.hh"
#include "../lib/parameter.hh"

int main() {
  try {
    Parameters::set( "mechanics.problem.K", "7.0" );
    Parameters::set( "mechanics.problem.K_rot", "1.0" );
    Parameters::set( "mechanics.problem.I2_eps", "1.0e-4" );
    Parameters::set( "mechanics.problem.e", "1.0" );
    Parameters::set( "mechanics.problem.g", "1.0" );
    Parameters::set( "mechanics.problem.mu", "1.0" );
    Parameters::set( "mechanics.problem.zeta", "1.0" );
    Parameters::set( "mechanics.problem.gamma", "1.0" );
    Parameters::set( "mechanics.problem.initial_posture", "line" );
    Parameters::set( "mechanics.problem.frame_update", "double_rotation" );
    Parameters::set( "mechanics.problem.rotation_cutoff", "1.0e-12" );
    Parameters::set( "mechanics.problem.constant_radius", "1" );

    Mesh mesh(0.0, 1.0, 21);
    TimeProvider time_provider( 0.0, 3.0e-3 );
    time_provider.setDeltaT( 1.0e-4 );

    Bending_twisting_mechanics_entity my_entity("mechanics", mesh, time_provider);
    using Position_probe = typename Bending_twisting_mechanics_entity :: Position_probe;
    using Moment_probe = typename Bending_twisting_mechanics_entity :: Bending_moment_probe;
    using Curvature_probe = typename Bending_twisting_mechanics_entity :: Curvature_probe;
    using Pressure_probe = typename Bending_twisting_mechanics_entity :: Pressure_probe;
    using Twist_probe = typename Bending_twisting_mechanics_entity :: Twisting_moment_probe;
    using AngularVelocity_probe = typename Bending_twisting_mechanics_entity :: Tangent_angular_velocity_probe;
    using Twist_moment_probe = typename Bending_twisting_mechanics_entity :: Twisting_moment_probe;

    using Centre_of_mass_probe = typename Bending_twisting_mechanics_entity :: Centre_of_mass_probe;
    using Head_probe = typename Bending_twisting_mechanics_entity :: Head_probe;
    using Tail_probe = typename Bending_twisting_mechanics_entity :: Tail_probe;
    using Length_probe = typename Bending_twisting_mechanics_entity :: Length_probe;
    using Energy_probe = typename Bending_twisting_mechanics_entity :: Energy_probe;
    using Frame_probe = typename Bending_twisting_mechanics_entity :: Frame_probe;

    auto position_probe_ptr = std::dynamic_pointer_cast< const Position_probe >( my_entity.get_probe( "mechanics.position" ) );
    auto moment_probe_ptr = std::dynamic_pointer_cast< const Moment_probe >( my_entity.get_probe( "mechanics.bending_moment" ) );
    auto curvature_probe_ptr = std::dynamic_pointer_cast< const Curvature_probe >( my_entity.get_probe( "mechanics.curvature" ) );
    auto pressure_probe_ptr = std::dynamic_pointer_cast< const Pressure_probe >( my_entity.get_probe( "mechanics.pressure" ) );
    auto twist_probe_ptr = std::dynamic_pointer_cast< const Twist_probe >( my_entity.get_probe( "mechanics.twist" ) );
    auto angular_velocity_probe_ptr = std::dynamic_pointer_cast< const AngularVelocity_probe >( my_entity.get_probe( "mechanics.tangent_angular_velocity" ) );
    auto twist_moment_probe_ptr = std::dynamic_pointer_cast< const Twist_moment_probe >( my_entity.get_probe( "mechanics.twisting_moment" ) );

    auto frame0_probe_ptr = std::dynamic_pointer_cast < const Frame_probe >( my_entity.get_probe( "mechanics.frame0" ) );
    auto frame1_probe_ptr = std::dynamic_pointer_cast < const Frame_probe >( my_entity.get_probe( "mechanics.frame1" ) );
    auto frame2_probe_ptr = std::dynamic_pointer_cast < const Frame_probe >( my_entity.get_probe( "mechanics.frame2" ) );

    auto centre_of_mass_probe_ptr = std::dynamic_pointer_cast< const Centre_of_mass_probe >( my_entity.get_probe( "mechanics.centre_of_mass" ) );
    auto head_probe_ptr = std::dynamic_pointer_cast< const Head_probe >( my_entity.get_probe( "mechanics.head" ) );
    auto tail_probe_ptr = std::dynamic_pointer_cast< const Tail_probe >( my_entity.get_probe( "mechanics.tail" ) );
    auto length_probe_ptr = std::dynamic_pointer_cast< const Length_probe >( my_entity.get_probe( "mechanics.length" ) );
    auto energy_probe_ptr = std::dynamic_pointer_cast< const Energy_probe >( my_entity.get_probe( "mechanics.energy" ) );

    std::cout << "testing " << my_entity.long_label() << std::endl;

    my_entity.load();
    my_entity.tr_begin();

    for( ; time_provider.time() < time_provider.endTime(); time_provider.next() ){
      std::cout << "new time step: " << time_provider.time() << std::endl;
      // within time loop
      my_entity.tr_advance();

      { // within solver loop
	my_entity.tr_load();
	my_entity.eval();
      }

      my_entity.tr_review();
      my_entity.tr_accept();

      // test that probes are usable
      std::cout << position_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< moment_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< curvature_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< pressure_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< twist_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< angular_velocity_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< twist_moment_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< frame0_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< frame1_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< frame2_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< centre_of_mass_probe_ptr->evaluate() << " "
		<< head_probe_ptr->evaluate() << " "
		<< tail_probe_ptr->evaluate() << " "
		<< length_probe_ptr->evaluate() << " "
		<< energy_probe_ptr->evaluate() << "\n";
    }

    return 0;
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }
}
