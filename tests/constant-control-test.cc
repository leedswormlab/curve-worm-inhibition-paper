#ifndef WORLDDIM
#define WORLDDIM 3
#endif

#include "../lib/mesh.hh"
#include "../lib/timeprovider.hh"
#include "../lib/entity/control/constant-control.hh"
#include "../lib/parameter.hh"

int main() {
  try {
    Parameters::set( "control.constant.test_case", "1" );
    Parameters::set( "control.constant.beta_0", "10.0" );
    Parameters::set( "control.constant.beta_1", "6.0" );
    Parameters::set( "control.constant.gamma0", "0.65" );

    TimeProvider time_provider( 0.0, 3.0e-4 );
    time_provider.setDeltaT( 1.0e-4 );

    Mesh mesh( 0.0, 1.0, 5 );

    Constant_control_entity my_entity("precribed_control", mesh, time_provider );

    // access beta probe
    using BetaProbe = typename Constant_control_entity :: BetaProbe;
    auto beta_probe_ptr = std::dynamic_pointer_cast< const BetaProbe >( my_entity.get_probe( "control.beta" ) );
    assert( beta_probe_ptr );
    const auto& beta = beta_probe_ptr->evaluate();

    // access alpha0 probe
    using Gamma0Probe = typename Constant_control_entity :: Gamma0Probe;
    auto gamma0_probe_ptr = std::dynamic_pointer_cast< const Gamma0Probe >( my_entity.get_probe( "control.gamma0" ) );
    assert( gamma0_probe_ptr );
    const auto& gamma0 = gamma0_probe_ptr->evaluate();

    std::cout << "testing " << my_entity.long_label() << std::endl;

    my_entity.load();
    my_entity.tr_begin();

    for( ; time_provider.time() < time_provider.endTime(); time_provider.next() ){
      std::cout << "new time step: " << time_provider.time() << std::endl;
      // within time loop
      my_entity.tr_advance();

      { // within solver loop
	my_entity.tr_load();
	my_entity.eval();
      }

      my_entity.tr_review();
      my_entity.tr_accept();

      for( auto&& vertex : mesh.vertices() ) {
	std::cout << " " << beta.evaluate( vertex.index() );
      }
      std::cout << '\n';
      for( auto&& element : mesh.elements() ) {
	std::cout << " " << gamma0.evaluate( element.index() );
      }
      std::cout << '\n';    }

    return 0;
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }

  return 0;
}
