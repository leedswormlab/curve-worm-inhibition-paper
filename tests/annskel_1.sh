#!/bin/bash

echo ${top_srcdir}

set -e
set -u

# ---------- input parameters ----------

strain="N2"
media="2_00pc"
date="20160615"
run="trial03"

VIDEO0="${top_srcdir}/test-data/worm/Cam1_firstsec.avi"
VIDEO1="${top_srcdir}/test-data/worm/Cam2_firstsec.avi"
VIDEO2="${top_srcdir}/test-data/worm/Cam3_firstsec.avi"

BGIMAGE0="${top_srcdir}/test-data/worm/Cam1.png"
BGIMAGE1="${top_srcdir}/test-data/worm/Cam2.png"
BGIMAGE2="${top_srcdir}/test-data/worm/Cam3.png"

CALIBRATION_FILE="../test-data/worm/calibration-3d.xml"

PARAMETERFILE="${top_srcdir}/test-data/worm/parameters"

# ---------- output parameters ----------

OUTPUT_PREFIX="output/great-first-frame"

# ---------- ensure output directories exist  ----------

mkdir -p ${OUTPUT_PREFIX}

# ---------- display header information ----------

out_fn="run.sh.out"
err_fn="run.sh.err"
exec  > >(tee ${out_fn}) 2> >(tee ${err_fn} >&2)

echo "# ------------------------------------------------------------"
echo "# by $(whoami)"
echo "# in $(pwd -P)"
# version information
# CMAKE_SRC_DIR=$(cat CMakeCache.txt | grep -E '.*_SOURCE_DIR:STATIC=.*' | grep -E -o '/.*')
# GITLOG=$(cd ${CMAKE_SRC_DIR} && git rev-parse --short HEAD)
# echo "# most recent commit: ${GITLOG}"
# run time parameters
echo "# video0: ${VIDEO0}"
echo "# video1: ${VIDEO1}"
echo "# video2: ${VIDEO2}"
echo "# ------------------------------------------------------------"
echo


#csvlist=./../head_clips/*_cam1_frame_0.csv
csvlist=../../head_clips/20160427_trial01_cam1_frame_0.csv

for line in $csvlist;
do

echo main loop, ${line}
basedir=$(dirname ${line})
    basename=$(basename ${line})
    IFS='_' read -ra params <<< "$basename"

    date=${params[0]}
    trial=${params[1]}
    cam=${params[2]}
rest="${params[3]}_${params[4]}"

VIDEO0="${top_srcdir}/head_clips/${date}_${trial}_cam1_frames_0-25.avi"
VIDEO1="${top_srcdir}/head_clips/${date}_${trial}_cam2_frames_0-25.avi"
VIDEO2="${top_srcdir}/head_clips/${date}_${trial}_cam3_frames_0-25.avi"

    echo "# date: ${date}"
    echo "# trial: ${trial}"
    echo "# cam: ${cam}"
    echo "# rest: ${rest}"

    if [[ ( $date == *"20170515"* ) ]]; then
    	echo "skipping latest containing 20160711"
    	continue
    fi

    annotation1="${basedir}/${date}_${trial}_cam1_${rest}"
    annotation2="${basedir}/${date}_${trial}_cam2_${rest}"
    annotation3="${basedir}/${date}_${trial}_cam3_${rest}"
    calibdir="${basedir}/calibration/${date}/new-calibration-3d.xml"

    if [ ! -f ${annotation1} ]; then
	echo "${annotation1} not found!"
	break
    fi
    if [ ! -f ${annotation2} ]; then
	echo "${annotation2} not found!"
	break
    fi
    if [ ! -f ${annotation3} ]; then
	echo "${annotation3} not found!"
	break
    fi
    if [ ! -f ${calibdir} ]; then
	echo "${calibdir} not found!"
	break
    fi
    if [ ! -f ${VIDEO1} ]; then
	echo "${VIDEO1} not found!"
	break
    fi
    if [ ! -f ${VIDEO2} ]; then
	echo "${VIDEO2} not found!"
	break
    fi
    if [ ! -f ${VIDEO0} ]; then
	echo "${VIDEO0} not found!"
	break
    fi


    myioprefix="output/first-frame-test_findgamma/${date}_${trial}/"
    mkdir -p ${myioprefix}

	 echo annotation1: ${annotation1}

cmd="../worm-skeleton -D -f ${PARAMETERFILE} \
	--annotation.cam0:${annotation1} --annotation.cam1:${annotation2} --annotation.cam2:${annotation3} \
	--video.calibration:${calibdir} --video.background.cam0:${BGIMAGE0} --video.filename.cam0:${VIDEO0} --video.background.cam1:${BGIMAGE1} --video.filename.cam1:${VIDEO1} --video.background.cam2:${BGIMAGE2} --video.filename.cam2:${VIDEO2} --io.prefix:${myioprefix} --io.innerwrite:1 --findgamma.use:false --io.timestep:1.0e-2 --model.e.firststep.lastframewithrelaxation:0 --model.e.firststep.maxpenality:-1 --model.gamma:0.9 --findgamma.use:true"

# echo "$cmd"
$cmd

# --findgamma.use:true
done

