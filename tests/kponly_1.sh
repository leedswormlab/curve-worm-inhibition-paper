#!/bin/bash -x

top_srcdir=${top_srcdir-../../}

../worm-skeleton -D -G ${top_srcdir}/tests/kponly.fg0 --kp-only=true \
	   ${top_srcdir}/test-data/worm/parameters -T --dump-kp=1 --io.midpointswrite=1
