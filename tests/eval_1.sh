#!/bin/bash -x

srcdir=$top_srcdir/tests

framecvs=${top_srcdir}/head_clips/20160427_trial02_cam3_frame_0.csv
graph=$srcdir/test15_20.fg0

../worm-evolve -l ${graph} -T \
	--eval-csv="$framecvs" | grep -v took
