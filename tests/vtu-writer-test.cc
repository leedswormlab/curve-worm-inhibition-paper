#include "../lib/mesh.hh"
#include "../lib/timeprovider.hh"
#include "../lib/entity/world-entity.hh"
#include "../lib/entity/mechanics/elastic-mechanics-entity.hh"
#include "../lib/io/vtuwriter.hh"
#include "../lib/parameter.hh"

int main() {
  try {
    Parameters::set( "mechanics.problem.K", "7.0" );
    Parameters::set( "mechanics.problem.e", "1.0" );
    Parameters::set( "mechanics.problem.mu", "0.0" );
    Parameters::set( "mechanics.problem.I2_eps", "1.0e-4" );
    Parameters::set( "mechanics.problem.gamma", "1.0" );
    Parameters::set( "mechanics.problem.constant_radius", "1" );
    Parameters::set( "mechanics.problem.initial_posture", "line" );
    Parameters::set( "mechanics.problem.muscle_tension", "0" );
    Parameters::set( "mechanics.problem.dyn_function", "else" );
    Parameters::set( "mechanics.problem.e_max", "0.0" );
    Parameters::set( "control.beta_amplitude", "1.0" );
    Parameters::set( "mechanics.problem.e_cut", "0.0" );
    Parameters::set( "control.beta_amplitude", "1.0" );
    Parameters::set( "mechanics.problem.beta_grad", "0.0" );

    Parameters::set( "control.driven.alpha", "3.0" );

    Parameters::set( "control.feedback.clamp_t0", "0.0" );
    Parameters::set( "control.feedback.clamp_t1", "0.0" );
    Parameters::set( "control.feedback.beta_clampA", "0.0" );
    Parameters::set( "control.feedback.beta_clampB", "0.0" );
    Parameters::set( "mechanics.physical_clampA", "0.0" );
    Parameters::set( "mechanics.physical_clampB", "0.0" );

    Parameters::set( "output.vtu.essential_ports", "mechanics.position,mechanics.curvature,mechanics.pressure,mechanics.centre_of_mass" );

    Mesh mesh(0.0, 1.0, 21);
    TimeProvider time_provider( 0.0, 3.0e-4 );
    time_provider.setDeltaT( 1.0e-4 );

    World_entity world;
    using pEntity_base = typename World_entity::pEntity_base;

    pEntity_base mechanics
      = std::make_shared< Elastic_mechanics_entity >("mechanics",
						     mesh,
						     time_provider);
    world.add_sub_entity(mechanics);

    pEntity_base writer
      = std::make_shared< VTUWriter_entity >( mesh,
				       time_provider,
				       "output",
				       0.0 );
    world.add_sub_entity( writer );

    world.load();
    world.tr_begin();
    world.tr_begin_accept();

    for( ; time_provider.time() < time_provider.endTime(); time_provider.next() ){
      // std::cout << "new time step: " << time_provider.time() << std::endl;
      // within time loop
      world.tr_advance();

      { // within solver loop
	world.tr_load();
	world.eval();
      }

      world.tr_review();
      world.tr_accept();
    }

    return 0;
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }
}
