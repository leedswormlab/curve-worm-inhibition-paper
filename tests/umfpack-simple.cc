#include <stdio.h>
#include <assert.h>
#include <suitesparse/umfpack.h>

#include <cmath>

int n = 5 ;
// column storage
int Ap [ ] = {0, 2, 5, 9, 10, 12} ;
// the columns
int Ai [ ] =    { 0,  1,  0,  2,  4,  1,  2,  3,  4,  2,  1, 4} ;
double Ax [ ] = { 2., 3., 2.,-1., 4., 4.,-3., 1., 2., 2., 6., 1.} ;

/* [ 2  2          ] [ 1 ]   [ 6 ]
 * [ 3     4     6 ] [ 2 ]   [ 45]
 * [   -1 -3  2    ] [ 3 ] = [-3 ]
 * [       1       ] [ 4 ]   [ 3 ]
 * [    4  2     1 ] [ 5 ]   [ 19]
 */
double b [ ] = {6., 45., -3., 3., 19.} ;
double x [5] ;
int main (void)
{
	double *null = (double *) NULL ;
	int i ;
	void *Symbolic, *Numeric ;


	(void) umfpack_di_symbolic (n, n, Ap, Ai, Ax, &Symbolic, null, null) ;
	(void) umfpack_di_numeric (Ap, Ai, Ax, Symbolic, &Numeric, null, null) ;
	(void) umfpack_di_solve (UMFPACK_A, Ap, Ai, Ax, x, b, Numeric, null, null) ;

	for (i = 0 ; i < n ; i++){
		printf ("x [%d] = %g\n", i, x[i]) ;
	}
	for( i = 0; i < n; ++i ) {
		assert( std::abs( x[i] - (i+1) ) < 1.0e-8 );
	}
	printf("still same\n");
	for (i = 0 ; i < 12 ; i++){
		printf ("Ax [%d] = %g\n", i, Ax[i]) ;
	}

	Ax[9]=1.;

	(void) umfpack_di_numeric (Ap, Ai, Ax, Symbolic, &Numeric, null, null);

	// allocates some memory. use wsolve.
   // (void) umfpack_di_solve (UMFPACK_A, Ap, Ai, Ax, x, b, Numeric, null, null) ;

	double W[5*n]; // need 5*n. for some reason, iterative refinement is active
	               // is that the default?!
	int Wi[n];
	(void) umfpack_di_wsolve (UMFPACK_A, Ap, Ai, Ax, x, b, Numeric, null, null, Wi, W);

	for (i = 0 ; i < n ; i++){
		printf ("x [%d] = %g\n", i, x[i]) ;
	}

	umfpack_di_free_symbolic (&Symbolic) ;
	umfpack_di_free_numeric (&Numeric) ;

	return (0) ;
}
