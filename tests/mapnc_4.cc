#define NC 1
#include "../lib/bits/map.hh"

using nest::nested_map;

int main()
{
	typedef nested_map<uchar, std::string, 4u> map_4;
	typedef map_4::key_type key_type;

	map_4 M;
	{
		auto b=M.begin();
		assert(b.is_end());
	}

	{
		auto x=nest::make_range(M);
		assert(x.first.is_end());
		assert(x.second.is_end());
		assert(x.first.is_end());
		assert(x.first==x.second);
	}

	key_type a={ 0, 0, 0, 0 };

	std::map<key_type, std::string> m;

	for(unsigned i=0; i<2; ++i){
		for(unsigned j=i; j<i+2; ++j){
			for(unsigned k=0; k<2; ++k){
				for(unsigned l=k; l<k+2; ++l){
					a[0]=i; a[1]=j; a[2]=k; a[3]=l;
					M[a] = std::to_string(i)+","+std::to_string(j)+","+std::to_string(k)
						+","+std::to_string(l);
					std::cout << M[a] << "\n";
				}
			}
		}
	}

	auto p=nest::make_cn_range(M, a, 2);
	std::cout << (*p.first).second << "\n";

	std::cout << "center\n";
	std::cout << int(a[0]) << int(a[1]) << int(a[2]) << int(a[3]) << "\n";
	std::cout << "loop\n";

	for(;p.first!=p.second; ++p.first){
		std::cout << (*p.first).second << " - "
		          << (*p.first).first << "\n";
	}

	M.erase(a);
	p=nest::make_cn_range(M, a, 2);
	std::cout << "without center\n";
	for(;p.first!=p.second; ++p.first){
		std::cout << (*p.first).second << " - "
		          << (*p.first).first << "\n";
	}

	std::cout << "without first 10\n";
	for(unsigned i=0; i<10; ++i){
	map_4::const_iterator ii=M.begin();
	M.erase(ii);
	}
	p=nest::make_cn_range(M, a, 1000);
	for(;p.first!=p.second; ++p.first){
		std::cout << (*p.first).second << " - "
		          << (*p.first).first << "\n";
	}

}
