#define FLAT_WORM 1
#define CONSTANT_RADIUS 1

#include "../lib/mesh.hh"
#include "../lib/timeprovider.hh"
#include "../lib/entity/mechanics/elastic-mechanics-entity.hh"
#include "../lib/entity/mechanics/problem.hh"
#include "../lib/parameter.hh"

int main() {
  try {
    static const unsigned int dim = 2;

    Parameters::set( "mechanics.problem.K", "7.0" );
    Parameters::set( "mechanics.problem.e", "1.0" );
    Parameters::set( "mechanics.problem.mu", "0.0" );
    Parameters::set( "mechanics.problem.I2_eps", "1.0e-4" );
    Parameters::set( "mechanics.problem.gamma", "1.0" );
    Parameters::set( "mechanics.problem.constant_radius", "1" );
    Parameters::set( "mechanics.problem.initial_posture", "line" );
    Parameters::set( "mechanics.problem.muscle_tension", "0" );
    Parameters::set( "mechanics.problem.dyn_function", "else" );
    Parameters::set( "mechanics.problem.e_max", "0.0" );
    Parameters::set( "control.beta_amplitude", "1.0" );
    Parameters::set( "mechanics.problem.e_cut", "0.0" );
    Parameters::set( "control.beta_amplitude", "1.0" );
    Parameters::set( "mechanics.problem.beta_grad", "0.0" );

    Parameters::set( "control.feedback.clamp_t0", "0.0" );
    Parameters::set( "control.feedback.clamp_t1", "0.0" );
    Parameters::set( "control.feedback.beta_clampA", "0.0" );
    Parameters::set( "control.feedback.beta_clampB", "0.0" );
    Parameters::set( "mechanics.physical_clampA", "0.0" );
    Parameters::set( "mechanics.physical_clampB", "0.0" );

    Mesh mesh(0.0, 1.0, 21);
    TimeProvider time_provider( 0.0, 3.0e-3 );
    time_provider.setDeltaT( 1.0e-4 );

    Elastic_mechanics_entity my_entity("elastic_mechanics", mesh, time_provider);
    using Position_probe = typename Elastic_mechanics_entity :: Position_probe;
    using Curvature_probe = typename Elastic_mechanics_entity :: Curvature_probe;
    using Pressure_probe = typename Elastic_mechanics_entity :: Pressure_probe;

    auto position_probe_ptr = std::dynamic_pointer_cast< const Position_probe >( my_entity.get_probe( "mechanics.position" ) );
    auto curvature_probe_ptr = std::dynamic_pointer_cast< const Curvature_probe >( my_entity.get_probe( "mechanics.curvature" ) );
    auto pressure_probe_ptr = std::dynamic_pointer_cast< const Pressure_probe >( my_entity.get_probe( "mechanics.pressure" ) );

    std::cout << "testing " << my_entity.long_label() << std::endl;

    my_entity.load();
    my_entity.tr_begin();

    for( ; time_provider.time() < time_provider.endTime(); time_provider.next() ){
      std::cout << "new time step: " << time_provider.time() << std::endl;
      // within time loop
      my_entity.tr_advance();

      { // within solver loop
	my_entity.tr_load();
	my_entity.eval();
      }

      my_entity.tr_review();
      my_entity.tr_accept();

      // test that probes are usable
      std::cout << position_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< curvature_probe_ptr->evaluate().evaluate( 4 ) << " "
		<< pressure_probe_ptr->evaluate().evaluate( 4 ) << std::endl;
    }

    return 0;
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }
}
