#ifndef VTUWRITER_HH
#define VTUWRITER_HH

#include <iomanip>
#include <sstream>
#include <limits>
#include <list>

#include "parameter.hh"
#include "mesh.hh"
#include "timeprovider.hh"
#include "probe/probe.hh"
#include "error.hh"

unsigned iteration(TimeProvider const& t)
{
    return t.iteration();
}
double time(TimeProvider const& t)
{
    return t.time();
}

template<class T>
struct VTUWriter_ {
private:
  VTUWriter_() = delete;
public:
  VTUWriter_( ConfigParameters const& parameters, const T& timeProvider,
	     const std::string fnPrefix = "", unsigned loglevel=bLOG )
    : timeProvider_( timeProvider ), fnPrefix_( fnPrefix ),
    _loglevel(loglevel),
    _prefix(parameters.get< std::string >( "io.prefix" ))
  {
  }
  VTUWriter_( std::string const& prefix, const T& timeProvider,
	     const std::string fnPrefix = "", unsigned loglevel=bLOG )
    :
    timeProvider_( timeProvider ), fnPrefix_( fnPrefix ),
    _loglevel(loglevel), _prefix(prefix)
  {
  }
  VTUWriter_(T const& timeProvider)
    : timeProvider_( timeProvider ), _prefix(".")
  {
  }

  VTUWriter_( const VTUWriter_& other )
    : timeProvider_( other.timeProvider_ ),
    fnPrefix_( other.fnPrefix_ ), _loglevel(other._loglevel), _prefix(other._prefix)
  {
  }

  ~VTUWriter_() {
    if( pvd_ ) {
      closePvd();
    }else{ untested();
    }
  }
public:

  template< class DV1, class DV2, class DV3 >
  void writeVtu( const DV1 &X, const DV2 &Y, const DV3& P, double energy=0 /* HACK HACK */,
      unsigned framenumber=-1, //HACK
      unsigned iter=0 // HACK
      )
  {
    std::stringstream ss_head;
    ss_head << fnPrefix_ << "worm_" << std::setfill('0')
            << std::setw(6) <<  iteration(timeProvider_) << ".vtu";
    writeVtu( X, Y, P, ss_head.str(), energy, framenumber, iter);
  }

  template< class DV1, class DV2, class DV3 >
  void writeVtu( const DV1 &X, const DV2 &Y, const DV2& W, const DV3& P, double energy=0 /* HACK HACK */,
      unsigned framenumber=-1, //HACK
      unsigned iter=0 // HACK
      )
  {
    std::stringstream ss_head;
    ss_head << fnPrefix_ << "worm_" << std::setfill('0')
            << std::setw(6) <<  iteration(timeProvider_) << ".vtu";
    writeVtu( X, Y, W, P, ss_head.str(), energy, framenumber, iter);
  }

  template< class DV1, class DV2, class DV3 >
  void writeVtu( const DV1 &X, const DV2 &Y, const DV3& P, const std::string& vtufn,
      double energy=0 /*HACK HACK*/,
      unsigned framenumber=-1u, /*HACK*/
      unsigned iter=0)
  {
    using RangeVectorType = typename DV1::RangeVectorType;

    const unsigned int N = X.N();

    auto ss(_prefix + "/" + vtufn);

    std::ofstream file( ss.c_str() );
    if( not file.is_open() ){
      throw MyException( "unable to open file " + ss );
    }else{
    }

    // set to full precision
    file.precision( std::numeric_limits<double>::max_digits10 );

    file << "<?xml version=\"1.0\"?>\n";
    file << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
    file << "  <UnstructuredGrid>\n";
    file << "    <Piece NumberOfPoints=\"" << N << "\" NumberOfCells=\"" << N-1 << "\">\n";
//     file << "      <PointData Scalars=\"E\">17.\n"; 
//     incomplete();
//     file << "      </PointData>\n";
    file << "      <PointData Scalars=\"W\">\n";
    file << "        <DataArray type=\"Float32\" Name=\"W\" NumberOfComponents=\"" << Y.dim << "\">\n";
    file << "\n";
    for( unsigned i=0; i<Y.size(); ++i){
	file << Y[i];
        if((i+1)%Y.dim){
          file << " ";
        }else{
          file << "\n";
        }
    }
    file << "\n";
    file << "        </DataArray>\n";

    file << "        <DataArray type=\"Float32\" Name=\"u\" NumberOfComponents=\"" << 1 << "\">\n";
    file << "         ";
    for(unsigned j = 0; j < X.mesh().N(); ++j )
      {
	file << " " << X.mesh().u(j);
      }
    file << "\n";
    file << "        </DataArray>\n";
    file << "      </PointData>\n";

    file << "      <CellData Scalars=\"P\">\n";
    file << "        <DataArray type=\"Float32\" Name=\"P\" NumberOfComponents=\"" << P.dim << "\">\n";
    file << "         ";
    for( const double Pj : P )
      {
	file << " " << Pj;
      }
    file << "\n";
    file << "        </DataArray>\n";
    file << "      </CellData>\n";

    file << "      <Points>\n";
    file << "        <DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n";
    for( unsigned int j = 0 ; j < N; ++j )
      {
	file << "         ";
	RangeVectorType Xj = X.evaluate( j );
	for( unsigned int d = 0; d < X.dim; ++d )
	  {
	    file <<  " " << Xj[ d ];
	  }
	for( unsigned int d = X.dim; d < 3; ++d )
	  {
	    file << " 0";
	  }
	file << "\n";
      }
    file << "        </DataArray>\n";
    file << "      </Points>\n";

    file << "      <Cells>\n";
    file << "        <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n";
    file << "          ";
    for( unsigned int j = 0; j < N-1; ++j )
      {
	file << j << " " << j+1 << " ";
      }
    file << "\n";
    file << "        </DataArray>\n";
    file << "        <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n";
    file << "          ";
    for( unsigned int j = 0; j < N-1; ++j )
      {
	file << 2*(j+1) << " ";
      }
    file << "\n";
    file << "        </DataArray>\n";
    file << "        <DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n";
    file << "          ";
    for( unsigned int j = 0; j < N-1; ++j )
      {
	file << 3 << " ";
      }
    file << "        </DataArray>\n";
    file << "      </Cells>\n";
    file << "    </Piece>\n";
    file << "  </UnstructuredGrid>\n";
    file << "</VTKFile>\n";


    if( not pvd_.is_open() ){
      openPvd();
    }else{
    }

#ifdef XML_INDEX
    untested();
    // does not work.
    // - missing endtags after term/kill
    // - data not accessible before 48hrs
    pvd_ << "    <DataSet"
         << " timestep=\"" << time(timeProvider_) << "\""
         << " frame=\"" << framenumber << "\""
         << " energy=\"" << energy << "\""
         << " group=\"\" part=\"0\"\n"
         << "             file=\"" << vtufn <<"\"/>" << std::endl;
#else
    pvd_ << framenumber
         << " " << time(timeProvider_)
         << " " << energy
         << " " << vtufn
         << " " << iter;

#if 1
    file << "<!-- DATA\n";
    std::string space="";

    for(auto const& p: _probes){ untested();
      auto h=p->howmany();
      for(unsigned i=0; i<h; ++i){ untested();
        file << space << p->name(i);
        space=" ";
      }
    }
    file << "\n";

    space="";
    for(auto const& p: _probes){ untested();
      auto h=p->howmany();
      for(unsigned i=0; i<h; ++i){ untested();
        file << space << p->evaluate();
        space=" ";
      }
      for(unsigned i=0; i<h; ++i){ untested();
        pvd_ << " " << p->evaluate();
      }
    }
    file << "\n-->\n";
#else // embed probes into XML
#endif

#endif


    file.close();

    pvd_ << "\n" << std::flush;

    message(bTRACE, ss + " written\n");
  } // writeVtu

protected:
  void openPvd()
  {
    if( pvd_.is_open() )
      return;

    const std::string pvdFilename = _prefix + "/" + fnPrefix_ +
#ifdef XML_INDEX
      "worm.pvd";
#else
      "worm.txt";
#endif
    pvd_.open( pvdFilename.c_str() );

    if( not pvd_.is_open() )
      {
        throw "unable to open " + pvdFilename;
      }
    else
      {
#ifdef XML_INDEX
	pvd_ << "<?xml version=\"1.0\"?>" << std::endl;
	pvd_ << "<VTKFile type=\"Collection\" version=\"0.1\" byte_order=\"LittleEndian\">" << std::endl;
	pvd_ << "  <Collection>" << std::endl;
#else
	pvd_ << "#framenumber time energy filename iter";
        for(auto x: _probes){
            auto h=x->howmany();

            for(unsigned i=0;i<h;++i){
                auto l=x->name(i);
                pvd_ << " " << l;
            }
        }
        pvd_ << "\n";
#endif

      }
  }

  template< class DV1, class DV2, class DV3 >
  void writeVtu( const DV1 &X, const DV2 &Y, const DV2& W,const DV3& P, const std::string& vtufn,
      double energy=0 /*HACK HACK*/,
      unsigned framenumber=-1u, /*HACK*/
      unsigned iter=0)
  {
    using RangeVectorType = typename DV1::RangeVectorType;

    const unsigned int N = X.N();

    auto ss(_prefix + "/" + vtufn);

    std::ofstream file( ss.c_str() );
    if( not file.is_open() ){
      throw MyException( "unable to open file " + ss );
    }else{
    }

    // set to full precision
    file.precision( std::numeric_limits<double>::max_digits10 );

    file << "<?xml version=\"1.0\"?>\n";
    file << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" byte_order=\"LittleEndian\">\n";
    file << "  <UnstructuredGrid>\n";
    file << "    <Piece NumberOfPoints=\"" << N << "\" NumberOfCells=\"" << N-1 << "\">\n";
//     file << "      <PointData Scalars=\"E\">17.\n"; 
//     incomplete();
//     file << "      </PointData>\n";
    file << "      <PointData Scalars=\"u\">\n";
    file << "        <DataArray type=\"Float32\" Name=\"Y\" NumberOfComponents=\"" << Y.dim << "\">\n";
    file << "\n";
    for( unsigned i=0; i<Y.size(); ++i){
	file << Y[i];
        if((i+1)%Y.dim){
          file << " ";
        }else{
          file << "\n";
        }
    }
    file << "\n";
    file << "        </DataArray>\n";

    file << "        <DataArray type=\"Float32\" Name=\"W\" NumberOfComponents=\"" << W.dim << "\">\n";
    file << "\n";
    for( unsigned i=0; i<W.size(); ++i){
	file << W[i];
        if((i+1)%W.dim){
          file << " ";
        }else{
          file << "\n";
        }
    }
    file << "\n";
    file << "        </DataArray>\n";

    file << "        <DataArray type=\"Float32\" Name=\"u\" NumberOfComponents=\"" << 1 << "\">\n";
    file << "         ";
    for(unsigned j = 0; j < X.mesh().N(); ++j )
      {
	file << " " << X.mesh().u(j);
      }
    file << "\n";
    file << "        </DataArray>\n";
    file << "      </PointData>\n";

    file << "      <CellData Scalars=\"P\">\n";
    file << "        <DataArray type=\"Float32\" Name=\"P\" NumberOfComponents=\"" << P.dim << "\">\n";
    file << "         ";
    for( const double Pj : P )
      {
	file << " " << Pj;
      }
    file << "\n";
    file << "        </DataArray>\n";
    file << "      </CellData>\n";

    file << "      <Points>\n";
    file << "        <DataArray type=\"Float32\" NumberOfComponents=\"3\" format=\"ascii\">\n";
    for( unsigned int j = 0 ; j < N; ++j )
      {
	file << "         ";
	RangeVectorType Xj = X.evaluate( j );
	for( unsigned int d = 0; d < X.dim; ++d )
	  {
	    file <<  " " << Xj[ d ];
	  }
	for( unsigned int d = X.dim; d < 3; ++d )
	  {
	    file << " 0";
	  }
	file << "\n";
      }
    file << "        </DataArray>\n";
    file << "      </Points>\n";

    file << "      <Cells>\n";
    file << "        <DataArray type=\"Int32\" Name=\"connectivity\" format=\"ascii\">\n";
    file << "          ";
    for( unsigned int j = 0; j < N-1; ++j )
      {
	file << j << " " << j+1 << " ";
      }
    file << "\n";
    file << "        </DataArray>\n";
    file << "        <DataArray type=\"Int32\" Name=\"offsets\" format=\"ascii\">\n";
    file << "          ";
    for( unsigned int j = 0; j < N-1; ++j )
      {
	file << 2*(j+1) << " ";
      }
    file << "\n";
    file << "        </DataArray>\n";
    file << "        <DataArray type=\"Int32\" Name=\"types\" format=\"ascii\">\n";
    file << "          ";
    for( unsigned int j = 0; j < N-1; ++j )
      {
	file << 3 << " ";
      }
    file << "        </DataArray>\n";
    file << "      </Cells>\n";
    file << "    </Piece>\n";
    file << "  </UnstructuredGrid>\n";
    file << "</VTKFile>\n";


    if( not pvd_.is_open() ){
      openPvd();
    }else{
    }

#ifdef XML_INDEX
    untested();
    // does not work.
    // - missing endtags after term/kill
    // - data not accessible before 48hrs
    pvd_ << "    <DataSet"
         << " timestep=\"" << time(timeProvider_) << "\""
         << " frame=\"" << framenumber << "\""
         << " energy=\"" << energy << "\""
         << " group=\"\" part=\"0\"\n"
         << "             file=\"" << vtufn <<"\"/>" << std::endl;
#else
    pvd_ << framenumber
         << " " << time(timeProvider_)
         << " " << energy
         << " " << vtufn
         << " " << iter;

#if 1
    file << "<!-- DATA\n";
    std::string space="";

    for(auto const& p: _probes){ untested();
      auto h=p->howmany();
      for(unsigned i=0; i<h; ++i){ untested();
        file << space << p->name(i);
        space=" ";
      }
    }
    file << "\n";

    space="";
    for(auto const& p: _probes){ untested();
      auto h=p->howmany();
      for(unsigned i=0; i<h; ++i){ untested();
        file << space << p->evaluate();
        space=" ";
      }
      for(unsigned i=0; i<h; ++i){ untested();
        pvd_ << " " << p->evaluate();
      }
    }
    file << "\n-->\n";
#else // embed probes into XML
#endif

#endif


    file.close();

    pvd_ << "\n" << std::flush;

    message(bTRACE, ss + " written\n");
  } // writeVtu

  void closePvd()
  {
    if( pvd_.is_open() )
      {
#ifdef XML_INDEX
	pvd_ << "  </Collection>" << std::endl;
	pvd_ << "</VTKFile>" << std::endl;
#endif

	pvd_.close();
	message(bLOG, "data collated in " + fnPrefix_ + "worm.txt");
      }
  }

public:
  void push_probe(probe* p){
      _probes.push_back(p);
  }
private:
  std::ofstream pvd_;
  const T& timeProvider_;
  const std::string fnPrefix_;
  std::list<probe const*> _probes;
  unsigned _loglevel;
  const std::string _prefix;
};

typedef VTUWriter_<TimeProvider> VTUWriter;

#endif // guard

//  vim: set cinoptions=>4,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1
//  vim: set autoindent expandtab shiftwidth=2 softtabstop=2 tabstop=8:
