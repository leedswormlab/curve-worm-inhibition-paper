#ifndef FEMSCHEME_HH
#define FEMSCHEME_HH

// #define DISPLAY_FEMSCHEME_IMAGES // does it work? somethings wrong with waitkey
//
#include <stack>

#include "mesh.hh"
#include "modelbase.hh" // BUG
#include "discretefunction.hh"
#include "matrix.hh"
#include "solver.hh"
#include "entity.hh"
#include "meshobject.hh"
#include "stencil.hh"
#include "timeprovider.hh"

// for now.
#define prechecked_cast dynamic_cast

#ifdef OpenCV_FOUND
#include <opencv2/core.hpp> // cv::Mat
#ifdef DISPLAY_FEMSCHEME_IMAGES
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>
#endif
#endif

// load matrix from worm (under construction)
#define NEWLOAD

class MeshFunctionBase;

template<unsigned wd >
struct FemScheme {
  using ModelType = ModelBase;
  static const int worlddim = wd;

  using SolutionTupleType = PositionCurvaturePressureTuple< worlddim >;
  using RhsType = SolutionTupleType;
  using PositionFunctionType = typename SolutionTupleType :: PositionFunctionType;
  using CurvatureFunctionType = typename SolutionTupleType :: CurvatureFunctionType;
  using PressureFunctionType = typename SolutionTupleType :: PressureFunctionType;

  using RangeVectorType = typename PositionFunctionType :: RangeVectorType;
  typedef UmfpackLU<double, int> SolverType;

  using SystemOperatorType = SystemMatrix< PositionFunctionType, SolverType >;

  FemScheme(TimeProvider const& t)
    : //_meshes(1),
      _timeProvider(t),
      // solutionTuple_( mesh ),
      // oldRhsTuple_( mesh ),
      _solution_data(0),
      _rhs_data(0),
      _old_solution_data(0),
      // operators
      _iter(0),
      implicitOperator_(),
      _matrix_size(0)
  {
    //_meshes[0] = &mesh; // one mesh for now. should it come from the worm...
//    init(); // call from here??
  }

  FemScheme(const FemScheme&) = delete;
  ~FemScheme(){
    trace3("~FemScheme1", _stencils.size(), _stencilmap.size(), _mesh_functions.size());
    _stencils.clear();
    trace3("~FemScheme2", _stencils.size(), _stencilmap.size(), _mesh_functions.size());
    _stencilmap.clear();
    trace3("~FemScheme3", _stencils.size(), _stencilmap.size(), _mesh_functions.size());
    _mesh_functions.clear();
    trace3("~FemScheme4", _stencils.size(), _stencilmap.size(), _mesh_functions.size());
  }

public:

  void push_back(MeshObject<wd>& e){
    assert(!_entities.size()); // incomplete.
    _entities.push_back(&e);
    model_ = &e.model(); // incomplete!
    assert(model_);
  }

  void init(){
    for(auto i : _entities){
      // HACK
      auto* m=prechecked_cast<MeshObject<wd>* >(i);
      assert(m);
      m->expand(*this);
    }

    trace3("init", _matrix_size, _stencils.size(), _mesh_functions.size());
    assert(_stencils.size());
//    assert( _matrix_size==N()*2*wd + N()-1 ||
//            _matrix_size==N()*3*wd + N()-1 );

    SquareMatrixFootprint t(_matrix_size);
    _solution_data.resize(_matrix_size);
    _rhs_data.resize(_matrix_size);
    _old_solution_data.resize(_matrix_size);

    for(auto const& i : _mesh_functions){
      if(i.first=="hmmworm.X"){ untested();
        incomplete();
        continue;
      }else{
      }
      unsigned off=i.second->offset();
      unsigned end=off + i.second->footprint_size();
      trace3("connect mesh function data", i.first, off, end);
      i.second->solution().set_data(SubArray<Vector>(_solution_data, off, end));
      i.second->rhs().set_data(SubArray<Vector>(_rhs_data, off, end));
      i.second->oldsolution().set_data(
	  SubArray<Vector>(_old_solution_data, off, end));
    }

    for(auto const& i : _stencilmap){
      trace1("load/mark", i.first);
      i.second->mark(t);
    }

    implicitOperator_.init(t);

    for(auto const& i : _stencilmap){
      trace1("map", i.first);
      i.second->map(implicitOperator_);
    }

    implicitOperator_.assemble();

  } // init

  void push_solution(){
    _sol_stack.push(std::vector<double> (_solution_data));
  }
  void pop_solution(){
    _sol_stack.pop();
  }
  void restore_solution(){
    _solution_data = _sol_stack.top();
  }
  void store_solution(){
    _sol_stack.top() = _solution_data;
  }
public: // HACK HACK. remove...
//  const PositionFunctionType& position() const { untested();
//    unreachable();
//    return solutionTuple_.position();
//  }
//  const CurvatureFunctionType& curvature() const { untested();
//    unreachable();
//    return solutionTuple_.curvature();
//  }
//  const PressureFunctionType& pressure() const { untested();
//    unreachable();
//    return solutionTuple_.pressure();
//  }
//  const PositionFunctionType& oldPosition() const { untested();
//    incomplete();
//    return oldSolutionTuple_.position();
//  }
//  const CurvatureFunctionType& oldCurvature() const { untested();
//    incomplete();
//    return oldSolutionTuple_.curvature();
//  }

  // TODO: need refactoring...
//  const PositionFunctionType& firstblock() const { itested();
//    return solutionTuple_.position();
//  }
public: // hack. encapsulation problem.
#if 0
  #endif

protected:
public: // HACK HACK
#if GLOBAL_MESH
  unsigned N() const{ untested();
    incomplete();
    return mesh().N();
  }
#endif

public:

  void advance_time(){
    _old_solution_data = _solution_data;

//    if(time>0) TODO
    for(auto i : _entities){ itested();
      i->tr_advance();
    }
  }
  // FemScheme::
  void prepare(double timestep=0) {
    advance_time(); /* TODO worrying - what time? */
    clear();

    // TODO: this evaluates energy and sets weights
    // should it?
    for(auto i : _entities){ itested();
      i->eval();
    }

    for(auto i : _entities){ itested();
      i->tr_load();
    }
  }
  void clear(){
//    advance(); // too late? // HACK HACK
    _rhs_data.clear();
    implicitOperator_.clear();

    for(auto const& i : _stencils){
      i->clear();
    }
  }

  // FemScheme::
  void solve() {
    ++_iter;

    // HACK HACK.
    // later: load directly from entity
    for(auto const& i : _stencilmap){ itested();
      trace1("load", i.first);
      i.second->load(implicitOperator_);
    }

#ifdef DEBUG
    std::cout << "RHS " << _iter;
    for (auto x: _rhs_data){

      std::cout << " " << x;
    }
      std::cout << "\n";
#endif

    // trace1("rhs", rhsTuple_);
    implicitOperator_.solve(_solution_data, _rhs_data);
    // trace1("sol", solutionTuple_);
#ifdef DEBUG
    implicitOperator_.print(std::cout);
    std::cout << "SOL " << _iter;
    for (auto x: _solution_data){

      std::cout << " " << x;
    }
      std::cout << "\n";
#endif
  }

  bool converged() const { untested();
    incomplete();
#if 0
    auto deltans=0.;
    for(unsigned i=0; i<rhsTuple_.size(); ++i){ untested();
      //double s = rhsTuple_[i] - oldRhsTuple_[i];
      double s = _solution[i] - oldSolutionTuple_[i];
      s*=s;
      deltans += s;
    }

//    std::cerr << _iter << " delta " << deltans << "\n";

    // incomplete. not used.
#endif
    return false;
  }

  void reset_counters(){
    _iter = 0;
  }
  unsigned iterations() const{ untested();
    return _iter;
  }
protected:
  ModelInterface<wd> const& model() const{ itested();
    assert(model_);
    return *(prechecked_cast<ModelInterface<wd> const*>(model_));
  }
public:
  double time() const{ itested();
    return _timeProvider.time();
  }
#ifdef GLOBAL_MESH // rather not..
  Mesh const& mesh( unsigned n=0) const{ itested();
    assert(n<_meshes.size());
    assert(_meshes[n]);
    return *_meshes[n];
  }
#endif
public: // interaction
  template<class S>
  void new_mesh_function(S& s, std::string const & name, Mesh /*object?*/ const&m){
    if(s._solution){
      trace2("already there?", name, _matrix_size);
      unreachable();
      return;
    }else{
      Vector dummy(0);
      s.populate(m, _matrix_size);
      assert(!_mesh_functions[name]);
      _mesh_functions[name] = &s;
//      _mesh_function_offset[name] = _matrix_size;
      _matrix_size += s.footprint_size();
      trace3("new", name, s.footprint_size(), _matrix_size);
    }
  }
  template<class S>
  void new_stencil(S& s, std::string const& row,
                          std::string const& col, std::string const& key)
  {
    trace2("new_stencil", row, col);
    assert(_mesh_functions[row]);
    std::string k;
    if(key.size()){
      k="/"+key;
    }else{
      k=key;
    }

    new_stencil(s, row+'/'+col+k, _mesh_functions[row]->offset(),
        _mesh_functions[col]->offset());
  }
  template<class S>
  void new_stencil(S& s, std::string const & name, unsigned ro, unsigned co){
    trace1("new_stencil", name);
    assert(s.done());
    {
      s.init(ro, co);
      _stencils.push_back(&s);

      assert(!_stencilmap[name]);
      _stencilmap[name] = &s;
    }
  }
private:
//  std::vector<Mesh const*> _meshes;
  ModelType const* model_; // BUG
  TimeProvider const& _timeProvider;
  std::vector<Entity*> _entities;

  Vector _solution_data;
  Vector _rhs_data;
  Vector _old_solution_data;

  unsigned _iter;
  SystemOperatorType implicitOperator_;
  unsigned _matrix_size;
  std::stack<std::vector<double> > _sol_stack;

  // these should not be used after map.
  // (currently they are)
  std::vector<stencil_base*> _stencils;
  std::map<std::string, stencil_base*> _stencilmap;
  std::map<std::string, MeshFunctionBase*> _mesh_functions;
}; // FemScheme


#endif // #ifndef FEMSCHEME_HH

// vim:ts=8:sw=2:et
