#ifndef VECTOR_HH
#define VECTOR_HH

#ifdef OpenCV_FOUND
#include <opencv2/video.hpp>
#endif
  
#include <array>
#include <cassert>
#include <vector>
#include <cmath>
#include "trace.hh"

class Vector : public std::vector<double> {
public:
  // typedef the Base type and the type of this class 
  // since they are used below
  typedef std::vector<double> Base;
  typedef Vector This;

  Vector() : Base() {}
  // construction of a vector providing the size,
  // note that the vector values are initialized to 0.
  explicit Vector(const unsigned int N) : Base(N) {}
  // a second constructor is provided which initializes
  // the values to something else.
  Vector(const unsigned int N, const double a) : Base(N,a) {}
  // also add a copy constructor (although the defaul would do the same)
  Vector(const Vector &other) : Base(other) {}

  void assign( const Vector& other ) {
    assert( size() == other.size() );
    for( unsigned j = 0; j < size(); ++j ) {
      (*this)[j] = other[j];
    }
  }
  
  // scale
  This& operator*=(const double &y)
  {
    for (auto& x : *this){
      x*=y;
	 }
    return *this;
  }
  // -=
  This& operator-=( const This &other )
  {
    for( unsigned int d = 0; d < Base::size(); ++d )
      {
	 (*this)[ d ] -= other[ d ];
      }
    return *this;
  }
  // compute the scalar product of this vector with a second one
  double operator*(const This &y) const
  {
    double ret = 0;
    for (unsigned int i=0;i<Base::size();++i)
      ret += (*this)[i]*y[i];
    return ret;
  }
  // there's a better way.. later
  void i() { untested();
	  assert(Base::size()==2);
	  std::swap((*this)[0], (*this)[1]);
	  (*this)[0] *= -1.;
  }
  // axpy operation this -> a * x + this 
  void axpy(double a, const This &x)
  {
    for (unsigned int i=0;i<Base::size();++i)
      (*this)[i] += a*x[i];
  }
  // this -> x + a * this 
  void xpay(double a, const This &x)
  {
    for (unsigned int i=0;i<Base::size();++i)
      (*this)[i] = x[i] + a*(*this)[i];
  }
  void add_scalar( const unsigned i, const double val ) {
    (*this).at(i) += val;
  }
  // compute the squared l2 norm of this vector
  double norm2() const
  {
    return (*this)*(*this);
  }

  void clear()
  {
    for( unsigned int i = 0; i < Base::size(); ++i )
      (*this)[ i ] = 0;
  }

  bool is_valid() const {
    for (double v : *this) {
      // assert( v == v );
      if (not(v == v)) {
	return false;
      }
    }
    return true;
  }
};

template< unsigned int mydim >
struct RangeVector : private std::array< double, mydim >
{
  typedef std::array< double, mydim > Base;
  using Base::fill;
  typedef RangeVector< mydim > This;
  static const int dim = mydim;

  explicit RangeVector() : Base() {}
  explicit RangeVector( const double a )
    : Base()
  {
    fill(a);
  }
  RangeVector( std::array<double, mydim> a )
    : Base(a) {}

public: // forward
  double& operator[](size_t n){
	  return Base::operator[](n);
  }
  double const& operator[](size_t n) const{
	  return Base::operator[](n);
  }
  double& at(size_t n){
	  return Base::at(n);
  }
  double const& at(size_t n) const{
	  return Base::at(n);
  }
  size_t size() const{
	  return Base::size();
  }

  bool is_valid() const {
    for (double v : *this) {
      // assert( v == v );
      if (not(v == v)) {
	return false;
      }
    }
    return true;
  }

public: // modify
  void i() { itested();
	  assert(dim==2);
	  std::swap((*this)[0], (*this)[1]);
	  (*this)[0] *= -1.;
  }
public: // ops
  bool operator==(RangeVector const& o) const{
	  for(unsigned i=0; i<mydim; ++i){
		  if(o[i]!=at(i)){
			  return false;
		  }else{ itested();
		  }
	  }
	  return true;
  }
  bool operator!=(RangeVector const& o) const{
	  return !operator==(o);
  }

  RangeVector(int a, int b)
    : Base()
  {
    assert( dim == 2 );
    this->at( 0 ) = a;
    this->at( 1 ) = b;
  }
  
#ifdef OpenCV_FOUND
  RangeVector( const cv::Point2d& pt )
    : Base()
  {
    if( dim != 2 )
      {
	std::stringstream ss;
	ss << "unable to convert cv::Point2d to RangeVector<" << dim << ">";
	throw ss.str();
      }
    this->at( 0 ) = pt.x;
    this->at( 1 ) = pt.y;
  }

  RangeVector( const cv::Point3d& pt )
    : Base()
  {
    if( dim != 3 )
      {
	std::stringstream ss;
	ss << "unable to convert cv::Point3d to RangeVector<" << dim << ">";
	throw ss.str();
      }
    this->at( 0 ) = pt.x;
    this->at( 1 ) = pt.y;
    this->at( 2 ) = pt.z;
  }
#endif

  This operator+( const This &other ) const
  {
    This out;
    for( unsigned int d = 0; d < dim; ++d )
      {
	out[ d ] = (*this)[ d ] + other[ d ];
      }
    return out;
  }

  This operator-() const {
    This out;
    for( unsigned int d = 0; d < dim; ++d ) {
			out[ d ] = - (*this)[ d ];
	 }
    return out;
  }

  This operator-( const This &other ) const
  {
    This out;
    for( unsigned int d = 0; d < dim; ++d )
      {
	out[ d ] = (*this)[ d ] - other[ d ];
      }
    return out;
  }

  This operator*( const double l ) const
  {
    This out;
    for( unsigned int d = 0; d < dim; ++d )
      {
	out[ d ] = (*this)[ d ] * l;
      }
    return out;
  }

  This operator/(double l ) const {
    This out;
    for( unsigned int d = 0; d < dim; ++d ) {
		 out[ d ] = (*this)[ d ] / l;
	 }
    return out;
  }

  void operator+=( const This &other )
  {
    for( unsigned int d = 0; d < dim; ++d )
      {
	(*this)[ d ] += other[ d ];
      }
  }

  void operator-=( const This &other )
  {
    for( unsigned int d = 0; d < dim; ++d )
      {
	(*this)[ d ] -= other[ d ];
      }
  }

  void operator*=( const double l )
  {
    for( unsigned int d = 0; d < dim; ++d )
      (*this)[ d ] *= l;
  }

  void operator/=( const double l )
  {
    for( unsigned int d = 0; d < dim; ++d )
      (*this)[ d ] /= l;
  }

  // compute the scalar product of this vector with a second one
  double operator*(const This &y) const
  {
    double ret = 0;
    for (unsigned int i=0;i<dim;++i)
      ret += (*this)[i]*y[i];
    return ret;
  }

  double norm2() const
  {
    return (*this)*(*this);
  }

  double norm() const
  {
    return sqrt( norm2() );
  }

  double dist( const This& other ) const
  {
    double sum = 0;
    for (unsigned int d = 0; d < dim; ++d )
      {
	sum += ( (*this)[d] - other[d] ) * ( (*this)[d] - other[d] );
      }
    return std::sqrt( sum );
  }

  void clear() {
    fill(0.);
  }

  This perp() const
  {
    if( dim != 2 )
      {
	static bool warn = true;
	if( warn )
	  {
	    std::cerr << __FILE__ << ":" << __LINE__ << ":WARNING: perp() not implemented for dim != 2" << std::endl;
	    warn = false;
	  }

	This ret;
	ret.clear();
	return ret;
      }

    This ret;
    ret[0] = (*this)[1];
    ret[1] = -(*this)[0];

    return ret;
  }

#ifdef OpenCV_FOUND
  // implicit conversions to opencv point types
  operator cv::Point2d() const
  {
    if( dim != 2 )
      {
	std::stringstream ss;
	ss << "unable to convert RangeVector<" << dim << "> to cv::Point2d";
	throw ss.str();
      }

    cv::Point2d pt;
    pt.x = this->at( 0 );
    pt.y = this->at( 1 );
    return pt;
  }

  operator cv::Point3d() const
  {
    if( dim != 3 )
      {
	std::stringstream ss;
	ss << "unable to convert RangeVector<" << dim << "> to cv::Point3d";
	throw ss.str();
      }

    cv::Point3d pt;
    pt.x = this->at( 0 );
    pt.y = this->at( 1 );
    pt.z = this->at( 2 );
    return pt;
  }
#endif

  operator double&();
  operator const double&() const;

  template< const unsigned int thedim >
  friend std::ostream& operator<<(std::ostream& os, const RangeVector<thedim>& v);
};

template<unsigned N>
inline RangeVector<N> operator*(double s, RangeVector<N> const& x)
{
	return x*s;
}

template< const unsigned int dim >
inline std::ostream& operator<<(std::ostream& os, const RangeVector<dim>& v)
{
  os << "(";
  for( const auto vv : v )
    {
      os << " " << vv;
    }
  os << " )";
  return os;
}

inline std::ostream& operator<<(std::ostream& os, const Vector& v)
{
  os << "(";
  for( const auto vv : v )
    {
      os << " " << vv;
    }
  os << " )";
  return os;
}

inline RangeVector<3> cross_product( const RangeVector<3>& a, const RangeVector<3>& b ) {
  RangeVector<3> ret(0.0);

  ret[0] = a[1]*b[2] - a[2]*b[1];
  ret[1] = -a[0]*b[2] + a[2]*b[0];
  ret[2] = a[0]*b[1] - a[1]*b[0];

  return ret;
}

template<>
inline RangeVector<1>::operator double&() {
  return this->at(0);
}
template<>
inline RangeVector<1>::operator const double&() const {
  return this->at(0);
}

template< class T >
struct SubArray
{
  typedef typename T :: value_type value_type;
  typedef typename T :: iterator iterator;
  typedef typename T :: const_iterator const_iterator;

  SubArray( T& array )
    : array_( array ), begin_( 0 ), end_( array.size() )
  {}

  SubArray( T& array, const iterator& begin, const iterator& end )
    : array_( array ), begin_( std::distance( array_.begin(), begin)  ), end_( std::distance( array_.end(), end ) )
  {}
  SubArray( T& array, const unsigned begin, const unsigned end )
    : array_( array ), begin_( begin ), end_( end )
  {}

  SubArray( const SubArray<T>& other ) = default;

  SubArray<T>& operator=( const SubArray<T>& o ) {
    this->array_ = o.array_;
    this->begin_ = o.begin_;
    this->end_ = o.end_;
    return *this;
  }

  // backend access?
  value_type& operator[]( unsigned i )
  {
    assert( i < size() );
    iterator it = begin() + i;
    value_type& v = *it;
    return v;
  }

  value_type const& operator[]( unsigned i ) const
  {
    assert( i < size() );
    const_iterator it = begin() + i;
    const value_type& v = *it;
    return v;
  }

  iterator begin()
  {
    return array_.begin() + begin_;
  }

  iterator end()
  {
    return array_.begin() + end_;
  }

  const_iterator begin() const
  {
    return array_.begin() + begin_;
  }

  const_iterator end() const
  {
    return array_.end() + end_;
  }

  void operator/=( const double a )
  {
    for( auto& d : *this )
      d /= a;
  }

  unsigned int N() const
  {
    return end_ - begin_;
  }

  unsigned int size() const
  { //incomplete(); // don't use.
    return N();
  }

  bool is_valid() const {
    for (double v : *this) {
      // assert( v == v );
      if (not(v == v)) {
	return false;
      }
    }
    return true;
  }

private:
  T& array_;
  unsigned int begin_;
  unsigned int end_;
  // /*const?*/ iterator begin_;
  // /*const?*/ iterator end_;
};

#if 0
// hoorible hack!
// TODO work this out properly
template< class T >
struct SubArray2 : public SubArray<T>
{
  typedef typename T :: value_type value_type;
  typedef typename T :: iterator iterator;
  typedef typename T :: const_iterator const_iterator;

  SubArray2( T& array, const unsigned begin_offset,
	     const unsigned end_offset  )
    : array_( array ), begin_( begin_offset ), end_( end_offset ),
      SubArray<T>( array )
  {}

  SubArray2( const SubArray2<T>& other ) = delete;

  // backend access?
  value_type& operator[]( unsigned i )
  {
    assert( i < size() );
    iterator it = begin() + i;
    value_type& v = *it;
    return v;
  }

  value_type const& operator[]( unsigned i ) const
  {
    assert( i < size() );
    const_iterator it = begin() + i;
    const value_type& v = *it;
    return v;
  }

  iterator begin()
  {
    return array_.begin() + begin_;
  }

  iterator end()
  {
    return array_.begin() + end_;
  }

  const_iterator begin() const
  {
    return array_.begin() + begin_;
  }

  const_iterator end() const
  {
    return array_.end() + end_;
  }

  void operator/=( const double a )
  {
    for( auto& d : *this )
      d /= a;
  }

  unsigned int N() const
  {
    return end_ - begin_;
  }

  unsigned int size() const
  { //incomplete(); // don't use.
    return N();
  }

private:
  T& array_;
  unsigned begin_;
  unsigned end_;
};
#endif


template<unsigned N>
inline double is_zero(RangeVector<N> const& x)
{
	for(unsigned i=0; i<N; ++i){
		if(x[i]!=0){
			return false;
		}else{
		}
	}
  return true;
}
inline double norm2(RangeVector<2> const& x)
{
  return x*x;
}
inline double norm(RangeVector<2> const& x)
{
  return sqrt(x*x);
}
inline double norm(RangeVector<3> const& x)
{
  return sqrt(x*x);
}

template<unsigned N>
inline double scalar_product(RangeVector<N> const& a, RangeVector<N> const& b)
{
	return a*b;
}

#endif
