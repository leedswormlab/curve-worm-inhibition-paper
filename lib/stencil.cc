#include "stencil.hh"
#include "meshobject.hh"
#include "femscheme.hh"

#include "config.hh"

#ifndef DEPRECATED
#warning somethings wrong.
#define DEPRECATED
#endif

void stencil_base::new_stencil(
		std::string const &s1,
		std::string const &s2,
		Entity & e, std::string key)
{
	if(auto a=dynamic_cast<MeshObject<2>* >(&e)){
		if(done()){
			unreachable();
			return;
		}else{
		}
		trace1("alloc", a->N());
		alloc(a->N());
		return a->schemehack().new_stencil(*this, e.long_label()+"."+s1, e.long_label()+"."+s2, key);
	}else if(auto a=dynamic_cast<MeshObject<3>* >(&e)){
		if(done()){
			unreachable();
			return;
		}else{
		}
		trace1("alloc", a->N());
		alloc(a->N());
		return a->schemehack().new_stencil(*this, e.long_label()+"."+s1, e.long_label()+"."+s2, key);
	}else{ incomplete();
	}
}
