#include <iostream>

#include "../vector.hh"

// #define DEBUG 1

class Identity_preconditioner {
  template< class V >
  void inv_matvec( const V& x, V& y ) const {
    assert( x.size() == y.size() );
    y = x;
  }
};

template< class Matrix, class Preconditioner >
int bicgstab( const Matrix& A, const Preconditioner& M,
	      const Vector& b, Vector& x,
	      const double tol, const int max_it,
	      const bool verbose = false ) {
  const unsigned N = x.size();
  Vector r(N), r_star(N), p(N), s(N), tmp(N), z(N);
  std::array< double, 5 > global_dot;

  const double b2 = b*b;

  // init
  M.inv_matvec( x, z ); // z = M^{-1} x
#if DEBUG
  std::cout << "z: " << z << std::endl;
#endif
  A.matvec( z, r );     // r = A z

  // residual r = b - r
  r.xpay( -1.0, b );
#if DEBUG
  std::cout << "r: " << r << std::endl;
#endif

  // p = r
  p.assign( r );
  // r_star = r
  r_star.assign( r );

  double nu = r * r_star;
#if DEBUG
  std::cout << "nu: " << nu << "\n";
#endif

  // iterate
  int it = 0;
  while( true ) {
    // 2x linear operator, 1x dot
    M.inv_matvec( p, z ); // z = M^{-1} p
#if DEBUG
    std::cout << "z: " << z << std::endl;
#endif
    A.matvec( z, tmp );   // tmp = A z
#if DEBUG
    std::cout << "tmp: " << tmp << std::endl;
#endif

    global_dot[0] = tmp * r_star;
#if DEBUG
    std::cout << "dot: " << global_dot[0] << std::endl;
#endif
    const double alpha = nu / global_dot[0];
#if DEBUG
    std::cout << "alpha: " << alpha << std::endl;
#endif
    s.assign( r );
    s.axpy( -alpha, tmp );
#if DEBUG
    std::cout << "s: " << s << std::endl;
#endif

    M.inv_matvec( s, z ); // z = M^{-1} s
#if DEBUG
    std::cout << "z: " << z << std::endl;
#endif
    A.matvec( z, r );     // r = A z
#if DEBUG
    std::cout << "r: " << r << std::endl;
#endif

    // 5x dot:
    global_dot.at(0) = r * s;
    global_dot.at(1) = r * r;
    global_dot.at(2) = s * s;
    global_dot.at(3) = s * r_star;
    global_dot.at(4) = r * r_star;
#if DEBUG
    std::cout << "global_dot: "
	      << global_dot[0] << " " << global_dot[1] << " "
	      << global_dot[2] << " " << global_dot[3] << " "
	      << global_dot[4] << "\n";
#endif

    // scalars
    const double omega = global_dot[0] / global_dot[1];
    const double res = std::sqrt( global_dot[2]
				  - omega * ( 2.0 * global_dot[0] - omega * global_dot[1] ) );
    const double beta = ( global_dot[3] - omega * global_dot[4] ) * alpha / ( omega * nu );

    nu = ( global_dot[3] - omega * global_dot[4] );

    assert( res == res );

#if DEBUG
    std::cout << "omega: " << omega << "\n"
	      << "res: " << res << "\n"
	      << "beta: " << beta << "\n"
	      << "nu: " << nu << std::endl;
#endif

    // update
    x.axpy( alpha, p );
    x.axpy( omega, s );
#if DEBUG
    std::cout << "x: " << x << std::endl;
#endif

    ++it;

    if( verbose ) {
      std::cout << "it: " << it << " : " << res << "\n";
    }
    if( res < tol*b2 or it >= max_it ) {
      break;
    }

    // r = s - omega r
    r.xpay( -omega, s );

    // p = r + beta * ( p - omega * tmp )
    p *= beta;
    p.axpy( -omega*beta, tmp );
    // p += r;
    p.axpy( 1.0, r );
  }

  // reconstruct x
  M.inv_matvec( x, z );
  x.assign( z );

  return it;
}
