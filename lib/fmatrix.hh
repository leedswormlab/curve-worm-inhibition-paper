#ifndef FMATRIX_HH
#define FMATRIX_HH

#include <array>
#include "vector.hh"

template< unsigned int r, unsigned int c >
struct Matrix : public std::array< std::array< double, c >, r >
{
  double &operator()( const unsigned int i, const unsigned int j )
  {
    return this->at(i).at(j);
  }
  double operator()( const unsigned int i, const unsigned int j ) const
  {
    return this->at(i).at(j);
  }

  RangeVector<c> operator()( const RangeVector<r>& x ) const
  {
    RangeVector<c> y;
    for( unsigned int i = 0; i < r; ++i )
      {
	y.at(i) = 0;
	for( unsigned int j = 0; j < c; ++j )
	  {
	    y.at(i) += this->at(i).at(j) * x.at(j);
	  }
      }
    return y;
  }
};

struct Skew : public Matrix<3,3> {
  Skew( const RangeVector<3>& vec ) {
    (*this)(0,0) = 0.0;
    (*this)(0,1) = -vec.at(2);
    (*this)(0,2) = vec.at(1);

    (*this)(1,0) = vec.at(2);
    (*this)(1,1) = 0.0;
    (*this)(1,2) = -vec.at(0);

    (*this)(2,0) = -vec.at(1);
    (*this)(2,1) = vec.at(0);
    (*this)(2,2) = 0.0;
  }
};

#endif // #ifndef FMATRIX_HH
