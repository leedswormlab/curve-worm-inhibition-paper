#ifndef PROJECT_CAM_HPP
#define PROJECT_CAM_HPP

template<>
struct CameraProjectionOperator< 3, 2 >
  : public ProjectionOperator< 3, 2 >
{ //
public: // types
  using BaseType = ProjectionOperator< 3, 2 >;
  using WorldRangeVectorType = typename BaseType :: WorldRangeVectorType;
  using ProjectedRangeVectorType = typename BaseType :: ProjectedRangeVectorType;
  using BaseType::ooffset;

private: // construct
  CameraProjectionOperator( const CameraProjectionOperator& ): ProjectionOperator(){
	  unreachable();
  }
public: // construct
  CameraProjectionOperator(  CameraProjectionOperator<3,2>&& ) = default;
	template<class DD>
	CameraProjectionOperator( DD const& fs, unsigned cam=0);

#ifdef OpenCV_FOUND
  // is this even used?
  // yes, in bogus factory
  CameraProjectionOperator( const cv::Mat& K, const cv::Mat& D,
			    const cv::Mat& T, const cv::Mat& R )
  { untested();
	  incomplete();
    cameraMatrix = K.clone();
    distCoeffs = D.clone();
    tvec = T.clone();
    rvec = R.clone();

    cv::Rodrigues( rvec, RR );
    set_some_pointers();


    // find P
    cv::Mat pose;
    cv::hconcat( RR, tvec, pose );
    P = cameraMatrix * pose;
  }
#endif

  explicit CameraProjectionOperator( const std::string& filename )
  { untested();
    std::cout << "Reading camera parameters from " << filename << std::endl;
    cv::FileStorage fs;
    fs.open( filename, cv::FileStorage::READ );

    std::string calibration_time;
    fs["calibration_Time"] >> calibration_time;
    std::cout << "calibration time: " << calibration_time << std::endl;

    // intrinsic parameters
    fs["Camera_Matrix"] >> cameraMatrix;
    fs["Distortion_Coefficients"] >> distCoeffs;
    fs["Rotation"] >> rvec;
    fs["Translation"] >> tvec;

    cv::Rodrigues( rvec, RR );

    RangeVector<3> a;
    a[0] = RR.at<double>(2,0);
    a[1] = RR.at<double>(2,1);
    a[2] = RR.at<double>(2,2);
    std::cerr << "axpre " << a[0] << " " << a[1] << " " << a[2] << "\n";


    set_some_pointers();

    // find P
    cv::Mat pose;
    cv::hconcat( RR, tvec, pose );
    P = cameraMatrix * pose;
  }

  RangeVector<3> axis() const{
    return _axis;
  }


  void set_some_pointers(){
    pRR = (double*) RR.data;
    pT = (double*) tvec.data;
    pK = (double*) cameraMatrix.data;
    pD = (double*) distCoeffs.data;

    pcx = &pK[2];
    pcy = &pK[5];

    _axis[0] = RR.at<double>(2,0);
    _axis[1] = RR.at<double>(2,1);
    _axis[2] = RR.at<double>(2,2);

    std::cerr <<"dimen " << RR.cols << " " << RR.rows << "\n";
  }



#ifdef OpenCV_FOUND
  virtual ProjectedRangeVectorType operator()( const WorldRangeVectorType& X ) const
  { itested();
    // extrinsic
    const cv::Point3d x( pRR[ 0 ] * X[0] + pRR[ 1 ] * X[1] + pRR[ 2 ] * X[2] + pT[0],
			 pRR[ 3 ] * X[0] + pRR[ 4 ] * X[1] + pRR[ 5 ] * X[2] + pT[1],
			 pRR[ 6 ] * X[0] + pRR[ 7 ] * X[1] + pRR[ 8 ] * X[2] + pT[2] );

    // projection
    const cv::Point2d xp( x.x / x.z, x.y / x.z );

    // distortion
    const double k1 = pD[0];
    const double k2 = pD[1];
    const double p1 = pD[2];
    const double p2 = pD[3];
    const double k3 = pD[4];

    const double r2 = xp.x*xp.x + xp.y*xp.y;
    const double r4 = r2*r2;
    const double r6 = r4*r2;

    const cv::Point2d xpp( xp.x * ( 1 + k1 * r2 + k2 * r4 + k3 * r6 )
			   + 2 * p1 * xp.x * xp.y + p2 * ( r2 + 2.0 * xp.x*xp.x ),
			   xp.y * ( 1 + k1 * r2 + k2 * r4 + k3 * r6 )
			   + 2 * p2 * xp.x * xp.y + p1 * ( r2 + 2.0 * xp.y*xp.y ) );

    // intrinsic
    double fx = pK[0];
    double fy = pK[4];

    ProjectedRangeVectorType output;
    output[0] = fx * xpp.x + *pcx;
    output[1] = fy * xpp.y + *pcy;

    return output + ooffset();
  }

  // TODO is this really the jacobian?
  virtual WorldRangeVectorType jacobian(
      const WorldRangeVectorType& X,
      const ProjectedRangeVectorType& dir ) const
  { itested();

    // extrinsic
    Matrix< 3, 3 > dR;
    dR(0,0) = pRR[0];
    dR(0,1) = pRR[1];
    dR(0,2) = pRR[2];
    dR(1,0) = pRR[3];
    dR(1,1) = pRR[4];
    dR(1,2) = pRR[5];
    dR(2,0) = pRR[6];
    dR(2,1) = pRR[7];
    dR(2,2) = pRR[8];

    const cv::Point3d x( pRR[ 0 ] * X[0] + pRR[ 1 ] * X[1] + pRR[ 2 ] * X[2] + pT[0],
			 pRR[ 3 ] * X[0] + pRR[ 4 ] * X[1] + pRR[ 5 ] * X[2] + pT[1],
			 pRR[ 6 ] * X[0] + pRR[ 7 ] * X[1] + pRR[ 8 ] * X[2] + pT[2] );

    // projection
    Matrix< 2, 3 > dp;
    dp(0,0) = 1 / x.z;
    dp(0,1) = 0;
    dp(0,2) = -x.x / (x.z*x.z);
    dp(1,0) = 0;
    dp(1,1) = 1 / x.z;
    dp(1,2) = -x.y / (x.z*x.z);

    const cv::Point2d xp( x.x / x.z, x.y / x.z );

    // distortion
    const double k1 = pD[0];
    const double k2 = pD[1];
    const double p1 = pD[2];
    const double p2 = pD[3];
    const double k3 = pD[4];

    const double r2 = xp.x*xp.x + xp.y*xp.y;
    const double r4 = r2*r2;
    const double r6 = r4*r2;

    const double xpx2 = xp.x*xp.x;
    const double xpy2 = xp.y*xp.y;

    Matrix<2,2> dD;
    dD(0,0) = 1 + 6 * p2 * xp.x + 2 * p1 * xp.y
      + k1 * ( 3 * xpx2 + xpy2 )
      + r2 * ( k2 * ( 5 * xpx2 + xpy2 ) + k3 * r2 * ( 7 * xpx2 + xpy2 ) );
    dD(0,1) = 2 * ( p1 * xp.x + xp.y * ( p2 + xp.x * ( k1 + r2 * ( 2 * k2 + 3 * k3 * r2 ) ) ) );
    dD(1,0) = 2 * ( p1 * xp.x + xp.y * ( p2 + xp.x * ( k1 + r2 * ( 2 * k2 + 3 * k3 * r2 ) ) ) );
    dD(1,1) = 1 + 2 * p2 * xp.x + 6 * p1 * xp.y
      + k1 * ( xpx2 + 3 * xpy2 )
      + r2 * ( k2 * ( xpx2 + 5 * xpy2 ) + k3 * r2 * ( xpx2 + 7 * xpy2 ) );

    const cv::Point2d xpp( xp.x * ( 1 + k1 * r2 + k2 * r4 + k3 * r6 )
			   + 2 * p1 * xp.x * xp.y + p2 * ( r2 + 2.0 * xp.x*xp.x ),
			   xp.y * ( 1 + k1 * r2 + k2 * r4 + k3 * r6 )
			   + 2 * p2 * xp.x * xp.y + p1 * ( r2 + 2.0 * xp.y*xp.y ) );

    // intrinsic
    const double fx = pK[0];
    const double fy = pK[4];
    Matrix<2,2> dI;
    dI(0,0) = fx;
    dI(0,1) = 0;
    dI(1,0) = 0;
    dI(1,1) = fy;

    assert(pcx);
    assert(pcy);

//    ProjectedRangeVectorType output;
//    output[0] = fx * xpp.x + *pcx;
//    output[1] = fy * xpp.y + *pcy;

    WorldRangeVectorType ret;
    for( unsigned int i = 0; i < 3; ++i )
      { itested();
	ret[i] = 0.0;
	for( unsigned int j = 0; j < 2; ++j ) {
	  for( unsigned int i1 = 0; i1 < 2; ++i1 ){
	    for( unsigned int i2 = 0; i2 < 2; ++i2 ){
	      for( unsigned int i3 = 0; i3 < 3; ++i3 ) { itested();
		  ret[i] += dir[j] * dI(j,i1) * dD(i1,i2) * dp(i2,i3) * dR(i3,i);
              }
            }
          }
        }
      }

    return ret;
  }

  template< class DF1, class DF2 >
  void projectSolution( const DF1& X, DF2& pX ) const { itested();
    for( unsigned int j = 0; j < X.N(); ++j ) { itested();
	const typename DF1 :: RangeVectorType Xj = X.evaluate( j );
	const typename DF2 :: RangeVectorType pXj = operator()( Xj );
	pX.assign( j, pXj );
      }
  }
#endif // OPENCV_found

  double pixelPerMM() const
  { itested();
    // convert middle to WorldRangeVectorType
    WorldRangeVectorType worldMiddle;
    for( unsigned int d = 0; d < worldMiddle.size(); ++d )
      {
	worldMiddle.at( d ) = middle.at<double>( 0,d );
      }

    double maxDiff = 0.0;
    for( unsigned int d = 0; d < worldMiddle.size(); ++d )
      {
	// perturb middle
	WorldRangeVectorType ed(0.0);
	ed[ d ] = 0.5;

	WorldRangeVectorType perturbedPlus = worldMiddle + ed;
	WorldRangeVectorType perturbedMinus = worldMiddle - ed;

	// project
	ProjectedRangeVectorType projPlus = operator()( perturbedPlus );
	ProjectedRangeVectorType projMinus = operator()( perturbedMinus );

	// find difference and compare
	maxDiff = std::max( ( projPlus - projMinus ).norm(), maxDiff );
      }

    return maxDiff;
  }

public: // BUG. there is some stray factory out there.
  cv::Mat cameraMatrix; // intrinsic parameters
  cv::Mat distCoeffs; // distortion parameters

  // extrinsic parameters
  //
  cv::Mat tvec;
  cv::Mat rvec;
  cv::Mat RR;

  cv::Mat P; // camera P
  cv::Mat middle; // estimated middle

private: // pointers
  double* pRR;
  double* pT;
  double* pK;
  double* pD;
  double const* pcx;
  double const* pcy;
};

template<class DD>
inline
CameraProjectionOperator< 3, 2 >::CameraProjectionOperator( DD const& fs, unsigned cam)
{
	cv::Mat K, D, T, R, pose;
	std::stringstream ss;

	ss << "camera_matrix_" << cam;
	fs[ ss.str() ] >> K;
	ss.str( std::string() );
	ss.clear();

	ss << "camera_distortion_" << cam;
	fs[ ss.str() ] >> D;
	ss.str( std::string() );
	ss.clear();

	ss << "camera_pose_" << cam;
	fs[ ss.str() ] >> pose;
	ss.str( std::string() );
	ss.clear();

	cv::Rodrigues( pose.rowRange(0,3).colRange(0,3), R );
	pose.rowRange(0,3).col(3).copyTo( T );


	cameraMatrix = K.clone();
	distCoeffs = D.clone();
	tvec = T.clone();
	rvec = R.clone();

	cv::Rodrigues( rvec, RR );
	set_some_pointers();

	cv::Mat pose2;
	cv::hconcat( RR, tvec, pose2 );
	P = cameraMatrix * pose2;

      auto ax=axis();
      message(bTRACE, "axDEBUG %E %E %E\n", ax[0], ax[1], ax[2]);
}

#endif
