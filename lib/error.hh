#ifndef ERROR_HH
#define ERROR_HH
#include <exception>
#include <string>
#include "trace.hh"

// ripped off gnucap
#ifndef IO_ERROR_H_INCLUDED
#define bNOERROR  0
#define bTRACE    1
#define bDEBUG    2
#define bLOG      3
#define bPICKY    4
#define bWARNING  5
#define bDANGER   6
#endif

void message(unsigned,const char*,...);
void message(unsigned,const std::string&);

class exception_nosuchfile : public std::exception{
public:
	~exception_nosuchfile() throw() {}
   exception_nosuchfile(std::string const& what)
      : std::exception(), _w(what) { untested(); }

   const char* what() const throw(){ untested();
      return _w.c_str();
   }

private:
   std::string _w;
};

class exception_invalid : public std::exception{
public:
	~exception_invalid() throw() {}
   exception_invalid(std::string const& what)
      : std::exception(), _w("invalid " + what) { untested(); }

   const char* what() const throw(){ itested();
      return _w.c_str();
   }

private:
   std::string _w;
};

class exception_corrupt : public std::exception{
public:
	~exception_corrupt() throw() {}
   exception_corrupt(std::string const& what)
      : std::exception(), _w("corrupt " + what) { untested(); }

   const char* what() const throw(){ itested();
      return _w.c_str();
   }

private:
   std::string _w;
};

class exception_nomatch : public std::exception{
public:
	~exception_nomatch() throw() {}
   exception_nomatch(std::string const& what="")
      : std::exception(), _w("nomatch " + what) { }

   const char* what() const throw(){ untested();
      return _w.c_str();
   }

private:
   std::string _w;
};

class exception_cantfind : public std::exception{
public:
	~exception_cantfind() throw() {}
   exception_cantfind(std::string const& what)
      : std::exception(), _w("cantfind " + what) { }

   const char* what() const throw(){ untested();
      return _w.c_str();
   }

private:
   std::string _w;
};

class exception_cast : public std::exception{
public:
  ~exception_cast() throw() {}
  exception_cast(std::string const& what)
      : std::exception(), _w("cast " + what) { }

  const char* what() const throw(){ untested();
    return _w.c_str();
  }

private:
  std::string _w;
};

// basic custom exception (to be removed)
struct MyException : public std::exception {
  MyException( std::string const& message )
    : _m( message ) { }

  const char* what() const throw() {
    return _m.c_str();
  }

private:
  std::string _m;
};

#endif
