#ifndef WORM_MATRIX_GRAPH_HPP
#define WORM_MATRIX_GRAPH_HPP
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include "matrix.hh"
#include "graph/frames.hh"

#if 0
namespace boost{
	template<class A, class B>
	size_t num_vertices(draft::framesgraph<A,B> const& g){
		return g.num_vertices();
	}
	template<class A, class B>
	size_t num_edges(draft::framesgraph<A,B> const& g){
		return g.num_edges();
	}
}
#endif

template<class FLOAT>
template<class G>
inline	DIAGONAL_MATRIX<FLOAT>::DIAGONAL_MATRIX(const G& g, SQRTDEG):
	_data(boost::num_vertices(g)), _zero(0.)
{ untested();
	for(unsigned i=0; i<boost::num_vertices(g) ;++i){ untested();
		_data[i]=sqrt(degree(i, g));
	}
}

#ifdef HAVE_ARPACK_PP
template<class FLOAT>
template<class G>
SYM_DENSE_MATRIX<FLOAT>::SYM_DENSE_MATRIX(G const& g)
	: base(boost::num_vertices(g), new FLOAT[area(g)]){ untested();
	// incomplete. uses canonical numbering. for now.
	auto p=boost::vertices(g);
	for(; p.first!=p.second; ++p.first){ untested();
		auto v=*p.first;

		auto q=boost::adjacent_vertices(v, g);
		for(; q.first!=q.second; ++q.first){ untested();
			auto w=*q.first;
			assert(w!=v);
			if(w==v){ untested();
				// no self loops.
			}else if(w>v){ untested();
				auto e=boost::edge(v, w, g);
				if(e.second){ untested();
					assert(boost::edge(w, v, g).second);
					double wt=boost::get(boost::edge_weight, g, e.first);
					set(v, w, wt);
				}else{ untested();
					assert(!boost::edge(w, v, g).second);
				}
			}else{ untested();
				// upper half.
			}
		}
	}
}

template<class FLOAT>
template<class G>
unsigned SYM_DENSE_MATRIX<FLOAT>::area(G const& g) { untested();
	unsigned n=boost::num_vertices(g);
	return n*(n+1)/2;
}
#endif // ARPACK++

// SD=sqrt(degree)
// build 1+  deltat*(1 - SD W SD)
template<class G>
inline UmfpackMatrix::UmfpackMatrix( const G& g_,
		UmfpackMatrix::NORMALIZED_LAPLACIAN, double deltat )
	: data_(g_.num_vertices()),
     n_(g_.num_vertices())
{
	unsigned nv=::boost::num_vertices(g_);
	message(bLOG, "LAPL UmfpackMatrix %dx%d deltat=%f", nv, nv, deltat);
#ifdef USE_NESTED_MAP
	auto nt=g_.num_target_vertices();
#else
	auto nt=0;
#endif
	std::vector<double> degree(nv);
	auto const& g = g_.graph(); // tmp. HACK.
	auto ep=::boost::edges(g); // BUG/FIXME

	// BUG, not here.
	// { untested();
	for(; ep.first!=ep.second; ++ep.first){ itested();
		auto source=boost::source(*ep.first, g);
		auto target=boost::target(*ep.first, g);
		degree[source] += boost::get(boost::edge_weight, g, *ep.first);
		degree[target] += boost::get(boost::edge_weight, g, *ep.first);
	}
#ifdef USE_NESTED_MAP
	for(auto const& t : g_.target_edges()){ itested();
		degree[t._src] += t._wt;
		degree[t._tgt] += t._wt;
	}
#endif
	assert(nv>=nt);
	for(unsigned v=int(nv)-int(nt); v<nv; ++v){ itested();
		degree[v]=sqrt(degree[v]);
		add(v, v, 1+deltat);
	}
	// }

	auto p=boost::vertices(g); // BUG: only the learning size
	unsigned v=-1u;
	for(; p.first!=p.second; ++p.first){ itested();
		v=*p.first;
		// trace2("", v, nv);
		add(v, v, 1+deltat);
		assert(v<nv);
		degree[v]=sqrt(degree[v]);

		auto q=boost::adjacent_vertices(v, g);
		for(; q.first!=q.second; ++q.first){ itested();
			auto w=*q.first;
			assert(w!=v);
			if(w==v){ itested();
				// no self loops.
			}else if(v>w){ itested();
				auto e=boost::edge(v, w, g);
				if(e.second){ itested();
					double wt=boost::get(boost::edge_weight, g, e.first);
					wt*=degree[v]*degree[w];
					add(v, w, -wt*deltat);
					add(w, v, -wt*deltat);
				}else{ itested();
					assert(!boost::edge(w, v, g).second);
				}
			}else{ itested();
				assert(!boost::edge(v, w, g).second);
				// upper half.
			}
		}
	}

	//++v;
	//for(;v<nv; ++v){ itested();
	//	add(v, v, 1+deltat);
	//}

#ifdef USE_NESTED_MAP
//	hmm bug? sqrtD thing?
	for(auto const& t : g_.target_edges()){ itested();
		assert(t._src<nv);
		assert(t._tgt<nv);
		add(t._src, t._tgt, -t._wt*deltat);
		add(t._tgt, t._src, -t._wt*deltat);
	}
	message(bLOG, "added %d tgt edges\n", g_.target_edges().size());
#endif
}
#endif
