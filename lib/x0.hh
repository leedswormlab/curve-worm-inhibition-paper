#ifndef X0_HH
#define X0_HH

#ifndef WORLD_DIM
#warning BUG: template parameter handled by preprocessor.
#define WORLD_DIM 2
#endif

struct X0 {

public:
  template<typename RangeVectorType>
  static std::function<RangeVectorType(double)> relaxation() {
    return [](const double u) -> RangeVectorType {
      untested();
      RangeVectorType ret;
      ret[0] = std::cos(M_PI * u) / M_PI;
      ret[1] = std::sin(M_PI * u) / M_PI;
      return ret;
    };
  }

  template<typename RangeVectorType>
  static std::function<RangeVectorType(double)> driven() {
    return [](const double u) -> RangeVectorType {
      untested();
      RangeVectorType ret;
      ret[0] = u;
      for (unsigned int d = 1; d < WORLD_DIM; ++d) {
        ret[d] = 0.0;
      }
      return ret;
    };
  }

};

#endif //X0_HH
