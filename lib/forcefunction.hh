#ifndef LIB_FORCEFUNCTION_HH
#define LIB_FORCEFUNCTION_HH

// (c) Felix Salfelder
// leeds university

// force functions for use in models. inspired by Toms DistanceFunction
//
#include <functional>
#include <array>

#include "vector.hh"
#include "function.hh"
#include "mesh.hh"
#include "projectionoperator.hh"
#include "discretefunction.hh"

// dim? maybe mesh as template arg?
template<unsigned dim >
class ForceFunctionBase : public FunctionBase<dim>{
public:
  using RangeVectorType = RangeVector< dim >;
  using DFI = DiscreteFunctionInterface<dim>;
  using RangeCoTangent = RangeVectorType;
  using RangeTangent = RangeVectorType;
  using FunctionBase<dim>::label;
  typedef std::vector<RangeCoTangent> FORCE_CONTAINER;
public:
  ForceFunctionBase() : _X(NULL) {
  }
public:
  // compute extra energy
  virtual double extraEnergy( DiscreteFunctionInterface<dim> const& /*X*/ ) const = 0;

  // stamp rhs. sort of "load"
  virtual void extraForcing( DiscreteFunctionInterface<dim> const& /*X*/,
                             DiscreteFunctionInterface<dim>& /*f*/ ) const = 0;

  virtual void add_rhs( DiscreteFunctionInterface<dim>& rhs){
    // incomplete(); later
//    for( unsigned int j = 0; j < rhs.N(); ++j ) { untested();
 //     assert(_fm[j][0] == _fm[j][0]);
  //    rhs.add( j, _fm[j] );
   // }
  }

  // pass DFI, good idea?
  virtual void expand(/*hack*/ DFI const& x){
    // delegate?
    _X = &x;
  }
  virtual void eval() = 0;

  virtual double energy() const{
    trace2("probing energy", label(), _energy);
    return _energy;
  }
  virtual RangeCoTangent const& total_force() const{
    unreachable(); // not yet.
    return _total_force;
  }
  RangeCoTangent const& directional_energy() const{
    return _directional_energy;
  }


protected:
  Mesh const& mesh() const{ itested();
    return X().mesh();
  }
  DFI const& X() const{ itested();
    assert(_X);
    return *_X;
  }
protected:
  RangeTangent _directional_energy;
  RangeCoTangent _total_force;
  FORCE_CONTAINER _fm; // one up?
  double _energy;
private: // move one up?
//  Mesh const& _mesh;
  DFI const* _X;
  std::string _label; // function_base?!
}; // ForceFunctionBase

template< class Force, class Projection >
class ProjectedForceFunction
  : public ForceFunctionBase<Projection::worlddim /*3*/>
{ //
  static const unsigned int nCams = 1;
//   static_assert( Projection::projecteddim == Force::dim,
// 		 "dimension mismatch between projection operator and distance function");
  static constexpr unsigned worlddim = Projection::worlddim;
  static constexpr unsigned projecteddim = Projection::projecteddim;

//  using BaseType = DistanceFunction< worlddim >;
  using BaseClass = ForceFunctionBase< worlddim >;
  using BaseType = BaseClass;

  using RangeVectorType = typename BaseType :: RangeVectorType;
  using RangeCoTangent = typename BaseType :: RangeVectorType;
  using BaseRangeVectorType = typename Force :: RangeVectorType;
  typedef std::vector<RangeCoTangent> FORCE_CONTAINER;

//  using BaseClass::_mesh;
  using BaseClass::_energy;
  using BaseClass::_fm;
  using BaseClass::mesh;
  using BaseClass::X;

public:
  ProjectedForceFunction( Force& baseForce, const Projection& projection )
    : _baseForce( baseForce ), _projection( projection ),
      _pXdata(0), // later, in expand.
      _pX(NULL) // mesh(), SubArray< Vector >( _pXdata.begin(), _pXdata.end() ) )
  {
  }
  ~ProjectedForceFunction(){ untested();
    delete _pX;
  }
private:

  using DFI = DiscreteFunctionInterface<worlddim>;
  void expand(DFI const& d){
    BaseClass::expand(d);

    _pXdata.resize(projecteddim*mesh().N());
    assert(_pXdata.size() == 256);
    _fm.resize(mesh().N());
    if(_pX){ untested();
      delete _pX;
    }else{
    }
    _pX = new PiecewiseLinearFunction< projecteddim > (
      d.mesh(), SubArray< Vector >( _pXdata.begin(), _pXdata.end() ) );

    _baseForce.expand(*_pX);
  }

  // ProjectedForceFunction::
  void eval(){
    Vector pFdata( mesh().N()*2 );

    _pXdata.clear();
    pFdata.clear();

    // project down the locations.
    assert(_pX);
    _projection.projectSolution( X(), *_pX );

    // BUG: prealloc.
    PiecewiseLinearFunction< projecteddim > pF(
        mesh(), SubArray< Vector >( pFdata.begin(), pFdata.end() ) );

    _baseForce.eval();
    _baseForce.add_rhs(pF);
    _energy = _baseForce.energy(); // BUG: rescale??

    for( unsigned int j = 0; j < mesh().N(); ++j ) { itested();
      auto pFj = pF.evaluate( j );
      auto Xj = X().evaluate( j );
      assert(pFj[0] == pFj[0]);
      assert(Xj[0] == Xj[0]);
      auto fj = _projection.jacobian( Xj, pFj );

      assert(j<_fm.size());
      assert(fj[0] == fj[0]);
      // assert(fj[0] < 100);
      // assert(fj[1] < 100);
      _fm[j] = fj;
    }
  } // eval
  // ProjectedForceFunction::
  virtual void add_rhs( DiscreteFunctionInterface<worlddim>& rhs){
    assert(worlddim==3);
    for( unsigned int j = 0; j < rhs.N(); ++j ) { itested();
      //std::cerr << " addrhs " << _fm[j][0] << "\n";
      assert(_fm[j][0] == _fm[j][0]);
      //assert(_fm[j][0] < 100);
      //assert(_fm[j][1] < 100);
      rhs.add( j, _fm[j] );
    }
  }
public: // overrides
  // typedef DiscreteFunctionInterface<2> DF;
  // template<class DF>
  // double extraEnergy( const DF& X ) const
  double extraEnergy( DiscreteFunctionInterface<worlddim> const& X ) const { untested();
    incomplete();
    // assign data
    Vector pXdata( X.size() );
    // clear data
    pXdata.clear();

    auto const& subarray=SubArray< Vector >( pXdata.begin(), pXdata.end() );

    // project solution variable
    PiecewiseLinearFunction< projecteddim > pX(X.mesh(), subarray);
    _projection.projectSolution( X, pX );

    return _baseForce.extraEnergy( pX );
  }

  // ProjectedForceFunction::
  // obsolete.
  void extraForcing(
      DiscreteFunctionInterface<worlddim> const& X,
      DiscreteFunctionInterface<worlddim>& rhs ) const
  { untested();
    incomplete();
    // assign data
    Vector pXdata( X.size() );
    Vector pFdata( rhs.size() );

    // clear data
    pXdata.clear();
    pFdata.clear();

    // project solution variable
    PiecewiseLinearFunction< projecteddim > pX(
        X.mesh(), SubArray< Vector >( pXdata.begin(), pXdata.end() ) );
    _projection.projectSolution( X, pX );

    // compute extra forcing in 2d
    PiecewiseLinearFunction< projecteddim > pF(
        X.mesh(), SubArray< Vector >( pFdata.begin(), pFdata.end() ) );

    _baseForce.extraForcing( pX, pF );

    for( unsigned int j = 0; j < rhs.N(); ++j ) { itested();
	const auto pFj = pF.evaluate( j );
	const auto Xj = X.evaluate( j );
	auto fj = _projection.jacobian( Xj, pFj );

	rhs.add( j, fj );
    }
  }
  void load_force_rhs(){
    // still loaded from matrix
    incomplete();
  }

private:
  ForceFunctionBase<projecteddim>& _baseForce;
  Projection const& _projection;

  Vector _pXdata; // stash the projected midline
  PiecewiseLinearFunction< projecteddim >* _pX;

}; // ProjectedForceFunction

template< class Force, class Projection >
ProjectedForceFunction<Force, Projection>
make_ProjectedForceFunction ( Force const& df, Projection const& p )
{ untested();
  return ProjectedForceFunction<Force, Projection>(df, p);
}

template< class Force, class Projection >
ProjectedForceFunction<Force, Projection> *
new_ProjectedForceFunction ( Force& df, Projection const& p )
{
  return new ProjectedForceFunction<Force, Projection>(df, p);
}

/* ============================================================================= */

#ifdef OpenCV_FOUND // BUG
class VectorForceFunction : public ForceFunctionBase<2> {
public: // types
  typedef std::vector< cv::Point > points_type;
  static constexpr unsigned dim=2;
  using RangeVectorType = RangeVector<dim>;
  using RangeCoTangent = RangeVector<dim>;
private:
   VectorForceFunction() = delete;
public:
   virtual ~VectorForceFunction(){ untested();
     untested();
   }
protected: // construct
  VectorForceFunction(const std::vector<cv::Point>& points, double const& s)
    : _points(points), _strength(s)
  {
  }

  VectorForceFunction( const VectorForceFunction& other ) = default;
public:
  // virtual RangeVectorType middle() const

  // sum of the distances squared...
  typedef DiscreteFunctionInterface<2> DF;
  double extraEnergy( const DF& X ) const
  { untested();
    if( _points.size() ){ itested();
      return energy() / double(2*_points.size());
    }else{ untested();
      return 0.;
    }
  }

  // for each keypoint, find nearby midline point.
  // pull on that, stamp to rhs
  // VectorForceFunction::
  void extraForcing( const DF& X, DF& rhs ) const { itested();
    unreachable(); // obsolete, still there.
    /// TODO:
    //  need a map meshpoint->relevant keypoints
    //
    //  then call Force on meshpoint.
    //
    double _energy = 0.;
    for( auto p : _points ) { itested();
	double minDist_sq = 1e20;
	RangeVectorType minX;
	unsigned minI=-1u;

        // compute closest mesh point.
        assert(X.N());
	for( unsigned i = 0; i < X.N(); ++i ) { itested();
	    const auto Xi = X.evaluate(i);
	    const double d =  (p.x - Xi[0])*(p.x - Xi[0]) +
			      (p.y - Xi[1])*(p.y - Xi[1]);
	    if( d < minDist_sq ) { itested();
		minDist_sq = d;
		minX = Xi;
		minI = i;
            }else{ itested();
            }
        }

        _energy += minDist_sq;

	RangeVectorType f;
	f[0] = p.x - minX[0];
	f[1] = p.y - minX[1];
	f /= std::max( f.norm(), 1.0e-10 );
	// f *= std::min( minDist, 0.1 );
	// f *= ( minDist < 0.1 ) ? minDist : minDist * 1.0e-2;
	f *= sqrt(minDist_sq);
	f /= static_cast<double>(_points.size());

//	f/=5;

      if(minI==-1u){ untested();
        unreachable();
      }else{ untested();
        // assert(f[0] < 100);
        // assert(f[1] < 100);
        rhs.add( minI, _strength*f );
      }
    }
  }
  const std::vector< cv::Point >& retKnown() const
  { untested();
    return _points;
  }
  const size_t numKnown() const
  { untested();
    return _points.size();
  }
protected:
  points_type const& _points;
  double const& _strength;
}; // VectorForceFunction


// the Keypoint based force.
// each keypoint pulls on the closest midline point
class ReverseForceFunction : public VectorForceFunction {
  // map a numbered meshpoint on the midline to a force.
  typedef std::vector<RangeCoTangent> FORCE_CONTAINER;
  static constexpr unsigned dim=2;
public:
  ReverseForceFunction(const std::vector<cv::Point>& p, double const& s)
    : VectorForceFunction(p,s), _fm(0)
  {
  }

public:
  void expand(DFI const& x){ itested();
    VectorForceFunction::expand(x);
    _fm.resize(mesh().N()*dim);
  }
private:
  // ReverseForceFunction::
  void eval(){
    auto const& X=VectorForceFunction::X();
    assert(X.size()==256);
    _energy = 0.;
//    message(bTRACE, "rff eval %d %d\n", X.N(), _fm.size());
    assert(X.N()*2 == _fm.size());
    for( unsigned i=0; i<X.N(); ++i ) { itested();
      assert(i<_fm.size());
      _fm[i].clear();
    }
#if 0
    for( unsigned i=0; i<X.N(); ++i ) { itested();
      auto Xi = X.evaluate(i);
//      message(bTRACE, "rff eval %d %f %f\n", i, Xi[0], Xi[1]);
    }
#endif

    for( auto p : _points ) { itested();
//      std::cerr << "p " << p << "\n";
      double minDist_sq = 1e20;
      RangeVectorType minX;
      unsigned minI=-1u;
      assert(X.N());
      for( unsigned i=0; i<X.N(); ++i ) { itested();
        const auto Xi = X.evaluate(i);
        const double d = (p.x - Xi[0])*(p.x - Xi[0]) + (p.y - Xi[1])*(p.y - Xi[1]);
        if( d < minDist_sq ) { itested();
          minDist_sq = d;
          minX = Xi;
          minI = i;
        }else{ itested();
        }
      }

      _energy += minDist_sq;

      RangeVectorType f;
      f[0] = p.x - minX[0];
      f[1] = p.y - minX[1];
      assert(f[0]==f[0]);
      f /= std::max( f.norm(), 1.0e-10 );
      // f *= std::min( minDist, 0.1 );
      // f *= ( minDist < 0.1 ) ? minDist : minDist * 1.0e-2;
      f *= sqrt(minDist_sq);
      f /= double(_points.size());

      if(minI==-1u){ untested();
        unreachable();
      }else{ itested();
//        std::cerr << minX << "\n";
        assert(f[0]==f[0]);
        _fm[minI] += _strength*f;
        assert(_fm[minI][0]== _fm[minI][0]);
      }
    }
  }
  void add_rhs( DiscreteFunctionInterface<dim>& rhs){
    for( unsigned int j = 0; j < rhs.N(); ++j ) { itested();
      assert(_fm[j][0] == _fm[j][0]);
      rhs.add( j, _fm[j] );
    }
  }
private:
  FORCE_CONTAINER _fm;
}; // ReverseForce

class GravityForceFunction : public VectorForceFunction {
public: // types
  typedef std::vector< cv::Point > points_type;
  using DF=DiscreteFunctionInterface<2>;
public: // construct
  explicit GravityForceFunction(points_type const& p, double const& strength, double radius)
  :  VectorForceFunction(p, strength),
    _gravity_radius(radius)
  {
  }

  GravityForceFunction(GravityForceFunction const& other) = default;
private: // overrides
  void eval(){ untested();
    // see rhsstamp
  }
public:
  // should be center of gravity?
  // virtual RangeVectorType middle() const
  // median, for now.

  static constexpr unsigned dim = 2;
  using RangeVectorType = RangeVector<dim>;
  virtual double distance( const RangeVectorType& x ) const
  { incomplete();
    return 0;
  }

  // gravity forces of all keypoints in x.
  virtual double normal_distance( const RangeVectorType& x, RangeVectorType& dn) const
  { untested();
    double scale = _strength / static_cast<double>(_points.size());
    double pointradius_squared=_gravity_radius * _gravity_radius;
    for( const auto& pt : _points ) { itested();
      double square_distance = (x[0] - pt.x)*(x[0] - pt.x) + (x[1] - pt.y)*(x[1] - pt.y);

      if(square_distance>pointradius_squared){ untested();
	scale *= pow( (pointradius_squared/square_distance), 4./2.);
	// scale *= (pointradius_squared/square_distance);
      }else{ untested();
      }
      dn[0] += scale * (x[0] - pt.x);
      dn[1] += scale * (x[1] - pt.y);
    }

    return dn[0]*dn[0] + dn[1]*dn[1];
  }

  void add_rhs( DiscreteFunctionInterface<dim>& rhs){
    for( unsigned int j = 0; j < rhs.N(); ++j ) { untested();
      assert(_fm[j][0] == _fm[j][0]);
      rhs.add( j, _fm[j] );
    }
  }

  // the normalized normal
  virtual RangeVectorType normal( const RangeVectorType& x ) const { untested();
    incomplete();
    RangeVectorType nu;
    return nu;
  }

  // not even used?
  virtual RangeVectorType normalDistance( const RangeVectorType& x ) const { untested();
    incomplete();
    RangeVectorType nu;

    return nu;
  }

  double extraEnergy( const DF& X ) const { untested();
    return VectorForceFunction::extraEnergy(X);
  }

  template< class DF >
  void extraForcing_template( const DF& X, DF& rhs ) const
  { incomplete();
  }

  // GravityForceFunction::
  void extraForcing(
      DiscreteFunctionInterface<2> const& X,
      DiscreteFunctionInterface<2>& rhs ) const{ untested();

    if(!_strength){ itested();
      return;
    }else{ itested();
    }
//    std::cerr << _strength << "\n";;
    double scale=_strength / static_cast<double>(_points.size());
//    double energy = 0.;
    for( auto pt : _points ) { itested();
        double pointradius_squared=_gravity_radius * _gravity_radius;

	for( unsigned i = 0; i < X.N(); ++i ) { itested();
	    const auto x = X.evaluate(i);
	    const double square_distance =  (pt.x - x[0])*(pt.x - x[0]) +
			      (pt.y - x[1])*(pt.y - x[1]);

            if(square_distance>pointradius_squared){ untested();
              scale *= pow( (pointradius_squared/square_distance), 1.5);
              // scale *= (pointradius_squared/square_distance);
            }else{ untested();
            }
            RangeVectorType dn;
            dn[0] += scale * (x[0] - pt.x);
            dn[1] += scale * (x[1] - pt.y);

            rhs.add(i, dn);
        }
    }
  }

private:
//  double _gravity_scale;
  double _gravity_radius;
}; // GravityForceFunction
#endif // opencv

#endif // #ifndef FORCEFUNCTION_HH

// vim:ts=8:sw=2:et
