#pragma once

#include "trace.hh"
#include "matrix.hh"
#include "umfpackmatrix.hh"
#include "image/annotations.hh" // BUG
#include <boost/numeric/ublas/matrix.hpp>

#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

namespace misc{

void waitKey(unsigned ms=0)
{
	for(;;){
		auto k=cv::waitKey(ms);
		trace1("waitkey", k);
	
		switch(k){ untested();
			case 65507:
				continue;
			default:
				return;
		}

	}
}

} // misc

template< class S, class T>
S& operator<<(S& s, boost::numeric::ublas::matrix<T> const& m)
{
	for(unsigned c=0; c<m.size2(); ++c){
		for(unsigned r=0; r<m.size1(); ++r){
			s << m(r, c);
		   if(r+1!=m.size1())	s << ", ";
		}
		if(c+1!=m.size1())	s << "\n";
	}
	return s;
}

template< class S, class T>
S& operator<<(S& s, std::vector<T> const& m)
{ untested();
	for(unsigned c=0; c<m.size(); ++c){ itested();
		s << m[c];
		if(c+1!=m.size())	s << ", ";
	}
	return s;
}

template< class S, class T>
S& operator<<(S& s, boost::numeric::ublas::vector<T> const& m)
{ untested();
	for(unsigned c=0; c<m.size(); ++c){ untested();
		s << m(c);
		if(c+1!=m.size())	s << ", ";
	}
	return s;
}

#ifdef HAVE_ARPACK_PP
// TODO: should work with any matrix, really.
std::ostream& operator<<(std::ostream& s, SYM_DENSE_MATRIX<double> const& m)
{
	for(unsigned c=0; c<m.cols(); ++c){
		for(unsigned r=0; r<m.rows(); ++r){
			s << m.get(r, c);
		   if(r+1!=m.rows())	s << ", ";
		}
		s << "\n";
	}
	return s;
}
#endif // ARPACK++


namespace misc{
// from so
std::string type2str(int type) {
	std::string r;

	unsigned char depth = type & CV_MAT_DEPTH_MASK;
	unsigned char chans = 1 + (type >> CV_CN_SHIFT);

	switch ( depth ) {
		case CV_8U:  r = "8U"; break;
		case CV_8S:  r = "8S"; break;
		case CV_16U: r = "16U"; break;
		case CV_16S: r = "16S"; break;
		case CV_32S: r = "32S"; break;
		case CV_32F: r = "32F"; break;
		case CV_64F: r = "64F"; break;
		default:     r = "User"; break;
	}

	r += "C";
	r += (chans+'0');

	return r;
}

framesindex make_framesindex(const std::string& indexfilename){ untested();
	return framesindex(indexfilename);
}

} // misc

template< class S>
S& operator<<(S& s, DIAGONAL_MATRIX<double> const& m)
{
	for(unsigned c=0; c<m.cols(); ++c){
		for(unsigned r=0; r<m.rows(); ++r){
			s << m.get(r, c);
		   if(r+1!=m.rows())	s << ", ";
		}
		s << "\n";
	}
	return s;
}

#if 0
template<class S>
S& operator<<(S& s, UmfpackMatrix const& m)
{
	// m.print(s);
	for(unsigned c=0; c<m.cols(); ++c){
		for(unsigned r=0; r<m.rows(); ++r){
			s << m.get(r, c);
		   if(r+1!=m.rows())	s << ", ";
		}
		s << "\n";
	}
	return s;
}
#endif

/*--------------------------------------------------------------------------*/
namespace OS {
  inline bool access_ok(const std::string& file, int mode) {
    return (::access(file.c_str(), mode) == 0/*file_ok*/);
  }
}
// ripped off gnucap/lib/io_findf.cc
std::string findfile(const std::string& filename, const std::string& path, int mode)
{
	message(bTRACE, "tgt %s\n", path.c_str());
#ifdef CHECK_LOCAL_FIRST
  if (OS::access_ok(filename, mode)) {untested();
    return filename;
  }else{untested();
  }
#endif
					// for each item in the path
  for (std::string::const_iterator
	 p_ptr=path.begin(); p_ptr!=path.end(); ++p_ptr) {
    // p_ptr changed internally                  ^^^^^ skip sep
    std::string target = "";
    while (*p_ptr != ':'  &&  p_ptr != path.end()) {
		 // copy 1 path item
      target += *p_ptr++;
    }
    if (!target.empty() &&  !strchr("/", p_ptr[-1])) {
      target += "/";		// append '/' if needed
    }else{
    }
    
    target += filename;
	 message(bTRACE, "tgt %s\n", target.c_str());
    if (OS::access_ok(target, mode)) {
		 // found it
      return target;
    }else if (p_ptr==path.end()) {
		 // ran out of path, didn't find it
      return "";
    }else{ untested();
		 // else try again
    }
  }
  return ""; // path doesn't exist - didn't go thru loop at all
}


