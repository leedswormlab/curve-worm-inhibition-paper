#ifndef CG_HH
#define CG_HH

#include <iostream>
#include <cassert>

template <class Matrix, class Vec>
int cg( const Matrix &A, const Vec &b, Vec &x, double epsilon = 1.0e-6)
{
  const double eps2 = epsilon*epsilon;
  const unsigned int N = b.size();
  // compute first residual
  Vec p(b);
  Vec Ap(p);
  A.multiply(x,Ap);
  p.axpy(-1,Ap);
  Vec r(p);
  double error = r.norm2();
  int iter = 0;
  while (error > eps2)
  {
    A.multiply(p,Ap);
    double App = Ap*p;
    assert( App > 0 );
    double alpha = error/App;
    x.axpy(alpha,p);
    r.axpy(-alpha,Ap);
    double new_error = r.norm2();
    double beta = new_error / error;
    p.xpay(beta,r);
    error = new_error;
    if( iter % 10000 == 9999 )
      std::cout << "iteration: " << iter << " error: " << error << std::endl;
    assert( iter < N*N*N );
    ++iter;
  }
  return iter;
}

#endif
