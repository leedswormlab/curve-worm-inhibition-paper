#pragma once

#include <cassert>
#include <algorithm>
#include <string>
#include <sstream>
#include <vector>

#include "../../include/misc.hh"

/**
 *  Probe_base
 *
 *  The interface provides what it means to be a probe. A probe is the
 *  object which is passed around between different entities. This is
 *  the only way to access or store data within an entity.
 */
class Probe_base {
public:
  // named probe constructor
  explicit Probe_base( const std::string& name )
    : _name( name )
  {
    // check probe name does not contain ' ', ':' or '.'
    assert( std::all_of( name.begin(), name.end(),
			 []( const char c ){
			   return c != ' ' and
			     c != ':';
			     }));
  }

  virtual ~Probe_base() {}

  // deleted copy constructor
  Probe_base( const Probe_base& other ) = delete;

  // return name of the probe
  virtual const std::string& name() const {
    return _name;
  }

  // split name of the probe
  std::pair<std::string, std::string>
  split_name() const {
    std::vector<std::string> ret;
    std::stringstream ss(_name);
    std::string item;
    while (std::getline(ss, item, '.')) {
      ret.push_back(item);
    }
    assert(ret.size() == 2);
    return std::make_pair(ret.at(0), ret.at(1));
  }

private:
  const std::string _name;
};

class probe : public Probe_base {
public:
	explicit probe(std::string const& label)
	  : Probe_base( label ) {
	}
	virtual ~probe(){}
public:
        // DEPRECATED does not name a type ?!
	const std::string& label() const{ return name(); }
	virtual double evaluate() const = 0;
	virtual unsigned howmany() const{ return 1; }
	virtual std::string name(unsigned i) const{
		if(howmany()==1){
		  return name();
		}else{
		  return name() + std::to_string(i);
		}
	}

  using Probe_base::name;
};

class Entity;

class entity_probe : public probe{
protected:
	entity_probe(std::string const& label, Entity const& e)
		: probe(label), _e(e) {
	}
public:
	virtual ~entity_probe(){}
public:
protected: // maybe not?
	Entity const& _e;
};

class DEPRECATED ptrprobe : probe{
	ptrprobe( const std::string& s, double const* p)
		: probe(s), _p(p) {
		}
public:
	double evaluate() const{
		return *_p;
	}
private:
	double const* const _p;
};

template< typename T >
struct Probe_ptr : public Probe_base {
  Probe_ptr()
    : Probe_base( "empty probe" )
  {}

  explicit Probe_ptr( const std::string name, const T* ptr_ )
    : Probe_base( name ), ptr_( ptr_ )
  {}

  Probe_ptr( const Probe_ptr<T>& other ) = delete;
  Probe_ptr( Probe_ptr<T>&& other ) = default;

  const T& evaluate() const {
    assert( ptr_ );
    return *ptr_;
  }
  T& evaluate() {
    assert( ptr_ );
    return *ptr_;
  }

private:
  const T* ptr_;
};

template< typename T >
struct Probe_otf : public Probe_base {
  Probe_otf()
    : Probe_base( "empty probe" )
  {}

  explicit Probe_otf( const std::string name )
    : Probe_base( name ) {}

  Probe_otf( const Probe_otf<T>& other ) = delete;

  virtual T evaluate() const = 0;
};
