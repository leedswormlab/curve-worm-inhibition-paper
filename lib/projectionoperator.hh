#ifndef PROJETIONOPERATOR_HH
#define PROJETIONOPERATOR_HH

#ifdef OpenCV_FOUND
#include "opencv2/core.hpp"
#include "opencv2/calib3d.hpp"

#endif

#include "fmatrix.hh"
#include "vector.hh"
#include "error.hh"
#include "probe.hh"

template< const unsigned int myworlddim, const unsigned int myprojecteddim >
struct ProjectionOperator {
  static const unsigned int worlddim = myworlddim;
  static const unsigned int projecteddim = myprojecteddim;

  typedef RangeVector< worlddim > WorldRangeVectorType;
  using ProjectedRangeVectorType = RangeVector< projecteddim >;

  virtual ProjectedRangeVectorType operator()( const WorldRangeVectorType& /*pt*/ ) const
  { untested();
    throw "projection operator should be specialised";
    return ProjectedRangeVectorType(0);
  }
#if 0
  virtual WorldRangeVectorType jacobian( const WorldRangeVectorType& /*pt*/, const ProjectedRangeVectorType& /*dir*/ ) const = 0;
#else
  virtual WorldRangeVectorType jacobian( const WorldRangeVectorType& /*pt*/, const ProjectedRangeVectorType& /*dir*/ ) const
  { untested();
    throw "projection operator should be specialised";
    return WorldRangeVectorType(0);
  }
#endif
  ProjectedRangeVectorType const& ooffset() const{
    return _ooffset;
  }
  void set_ooffset(ProjectedRangeVectorType const& o){
    _ooffset = o;
  }
  virtual double pixelPerMM() const{
    unreachable();
    return 0;
  }
  void shift_ooffset(ProjectedRangeVectorType const& o){
    _ooffset += o;
    _ooffset[0] = std::min(10.,_ooffset[0]);
    _ooffset[0] = std::max(-10.,_ooffset[0]);
    _ooffset[1] = std::min(10.,_ooffset[1]);
    _ooffset[1] = std::max(-10.,_ooffset[1]);
  }
  virtual RangeVector<myworlddim> axis() const{ untested();
    return _axis;
  }
protected:
  RangeVector<myworlddim> _axis;
private:
  ProjectedRangeVectorType _ooffset;
};

template< const unsigned int myworlddim >
struct IdentityProjectionOperator
  : public ProjectionOperator< myworlddim, myworlddim >
{ //
  static const unsigned int worlddim = myworlddim;
  static const unsigned int projecteddim = myworlddim;

  using BaseType = ProjectionOperator< worlddim, projecteddim >;
  using WorldRangeVectorType = typename BaseType :: WorldRangeVectorType;
  using ProjectedRangeVectorType = typename BaseType :: ProjectedRangeVectorType;

  IdentityProjectionOperator() {}

  virtual ProjectedRangeVectorType operator()( const WorldRangeVectorType& pt ) const
  { untested();
    return pt;
  }
  virtual WorldRangeVectorType jacobian( const WorldRangeVectorType& /*pt*/, const ProjectedRangeVectorType& dir ) const
  { untested();
    return dir;
  }
};

template< const unsigned int worlddim, const unsigned int projecteddim >
struct CameraProjectionOperator;

/**
 *  \class CameraProjectionOperator< 3, 2 >
 *
 *  \brief implementation of general pin-hole camera model
 *
 *  The operator is defined as a generalisation of the model given by
 *  http://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html
 *
 *  We denote by (X,Y,Z) the three dimensional input point and (u,v) as the output point which
 *  are related via the formula:
 *  \f[
        s \left( \begin{matrix} u \\ v \\ 1 \end{matrix} \right)
	=
	\left( \begin{matrix}
	f_x & \alpha & c_x \\
	0   & f_y    & c_y \\
	0   & 0      & 1
	\end{matrix} \right)
	\left( \begin{matrix}
	r_{11} & r_{12} & r_{13} & t_1 \\
	r_{21} & r_{22} & r_{23} & t_2 \\
	r_{31} & r_{32} & r_{33} & t_3
	\end{matrix} \right)
	\left( \begin{matrix}
	X \\ Y \\ Z \\ 1
        \end{matrix} \right).
    \f]
 */

#ifdef OpenCV_FOUND
#include "projection_camera.hpp"
#endif // opencv

template<>
struct CameraProjectionOperator< 2, 2 >
  : public ProjectionOperator< 2, 2 >
{ //
  using BaseType = ProjectionOperator< 2, 2 >;
  using WorldRangeVectorType = typename BaseType :: WorldRangeVectorType;
  using ProjectedRangeVectorType = typename BaseType :: ProjectedRangeVectorType;

  CameraProjectionOperator( const double scaling, const double centre )
    : scaling_( scaling ), centre_( centre )
  { untested();
  }

  virtual ProjectedRangeVectorType operator()( const WorldRangeVectorType& X ) const
  { untested();
    ProjectedRangeVectorType output;
    for( unsigned int d = 0; d < 2; ++d )
      output[ d ] = X[d] * scaling_ + centre_;
    return output;
  }

  virtual WorldRangeVectorType jacobian(
      const WorldRangeVectorType& /*X*/,
      const ProjectedRangeVectorType& dir ) const
  { untested();
    WorldRangeVectorType ret = dir;
    ret *= scaling_;
    return ret;
  }

  template< class DF1, class DF2 >
  void projectSolution( const DF1& X, DF2& pX ) const
  { untested();
    for( unsigned int j = 0; j < X.N(); ++j )
      { untested();
	const typename DF1 :: RangeVectorType Xj = X.evaluate( j );
	const typename DF2 :: RangeVectorType pXj = operator()( Xj );
	pX.assign( j, pXj );
      }
  }

  WorldRangeVectorType inverse( const ProjectedRangeVectorType& P ) const
  { untested();
    WorldRangeVectorType output;
    for( unsigned int d = 0; d < 2; ++d )
      output[ d ] = ( P[d] - centre_ ) / scaling_;
    return output;
  }
private:
  const double scaling_;
  const double centre_;
};

template< const unsigned int mydim >
struct TestProjectionOperator;

template<>
struct TestProjectionOperator<2>
  : public ProjectionOperator<2,2>
{ //
  using BaseType = ProjectionOperator< 2, 2 >;
  using ProjectedRangeVectorType = typename BaseType :: ProjectedRangeVectorType;
  using WorldRangeVectorType = typename BaseType ::  WorldRangeVectorType;

  virtual ProjectedRangeVectorType operator()( const WorldRangeVectorType& pt ) const
  { untested();
    ProjectedRangeVectorType ret;
    for( unsigned int d = 0; d < 2; ++d )
      { untested();
	ret[d] = 64.0 * pt[d] + 256.0;
      }
    return ret;
  }
  virtual WorldRangeVectorType jacobian(
      const WorldRangeVectorType& /*pt*/,
      const ProjectedRangeVectorType& dir ) const
  { untested();
    WorldRangeVectorType ret = dir;
    ret *= 64.0;
    return ret;
  }
};

template<>
struct TestProjectionOperator<3>
  : public ProjectionOperator<3,2>
{ //
  using BaseType = ProjectionOperator< 3, 2 >;
  using ProjectedRangeVectorType = typename BaseType :: ProjectedRangeVectorType;
  using WorldRangeVectorType = typename BaseType ::  WorldRangeVectorType;

  TestProjectionOperator( const unsigned int cam )
    : cam_( cam )
  {}

  virtual ProjectedRangeVectorType operator()( const WorldRangeVectorType& pt ) const
  { untested();
    ProjectedRangeVectorType ret;
    // N?
    ret.at( 0 ) = 128.0 * pt.at( (cam_+1)%3 ) + 256.0;
    ret.at( 1 ) = 128.0 * pt.at( (cam_+2)%3 ) + 256.0;
    return ret;
  }
  virtual WorldRangeVectorType jacobian(
      const WorldRangeVectorType& /*pt*/,
      const ProjectedRangeVectorType& dir ) const
  { untested();
    WorldRangeVectorType ret(0);
    // N?
    ret.at( (cam_+1)%3 ) = 128.0 * dir.at(0);
    ret.at( (cam_+2)%3 ) = 128.0 * dir.at(1);

    return ret;
  }

private:
  const unsigned int cam_;
};

#ifdef OpenCV_FOUND


// BUG. this is a factory.
void readCalibrationParameters( const std::string& filename,
				std::vector< CameraProjectionOperator< WORLD_DIM, 2 > >& p )
{
  std::cout << "Reading camera parameters from " << filename << std::endl;
  cv::FileStorage fs;
  fs.open( filename, cv::FileStorage::READ );

  // check file is opened
  if( not fs.isOpened() ){ untested();
    throw "unable to open parameter file: " + filename;
  }else{
	  message(bTRACE, "opened calibration %s\n", filename.c_str());
  }

  std::string calibration_time;
  fs["calibration_time"] >> calibration_time;
  std::cout << "calibration time: " << calibration_time << std::endl;

  int nCams = 0;
  fs["nCameras"] >> nCams;

  for( int cam = 0; cam < nCams; ++cam ) {
      p.emplace_back(CameraProjectionOperator<3,2>(fs, cam));
  }

  // find middle of grids
  unsigned int nGrids = 0;
  cv::FileNode n = fs["image_filenames"];
  if (n.type() != cv::FileNode::SEQ)
    { untested();
      throw "image_filenames is not a sequence! FAIL";
    }
  cv::FileNodeIterator it = n.begin(), it_end = n.end();
  for (; it != it_end; ++it)
    {
      ++nGrids;
    }

  cv::Mat meanGridT;
  int count = 0;
  for( unsigned int grid = 1; grid <= nGrids; ++grid ) {
      cv::Mat gridPose;
      std::stringstream ss;
      ss << "pose_timestamp_" << grid;
      fs[ ss.str() ] >> gridPose;
      if( gridPose.empty() )
	continue;

      cv::Mat gridT;
      gridPose.rowRange(0,3).col(3).copyTo( gridT );
      count++;

      if( meanGridT.empty() )
	meanGridT = gridT.clone();
      else
	meanGridT += gridT;
  }
  meanGridT /= static_cast<double>(count);

#if WORLD_DIM == 3
  // BUG. what is this?
  std::cout << "meanGridT: " << meanGridT << std::endl;
  for( auto& cp : p ){
    cp.middle = meanGridT.clone();
  }
#endif
}

template< class V, class R, class P>
static double free_triangulate( const V &middles,
		      R &middle3d, P const& p)
{
  typedef typename  V::value_type BaseRangeVectorType;
  // triangulate pairwise and take mean
  middle3d = cv::Point3d(0.,0.,0.);
  int n2Cams = 0;
  for( unsigned i = 0; i < p.size(); ++i )
  {
    for( unsigned j = 0; j < i; ++j )
    {
      cv::Mat cam0pnts( 1, 1, CV_64FC2 );
      cam0pnts.at< cv::Vec2d >( 0 )[ 0 ] = static_cast<int>(middles[i][0]);
      cam0pnts.at< cv::Vec2d >( 0 )[ 1 ] = static_cast<int>(middles[i][1]);
      cv::Mat cam1pnts( 1, 1, CV_64FC2 );
      cam1pnts.at< cv::Vec2d >( 0 )[ 0 ] = static_cast<int>(middles[j][0]);
      cam1pnts.at< cv::Vec2d >( 0 )[ 1 ] = static_cast<int>(middles[j][1]);

      auto P1 = p[i].P;
      auto P2 = p[j].P;

      cv::Mat pts3dHom;
      cv::triangulatePoints( P1, P2, cam0pnts, cam1pnts, pts3dHom );

      cv::Point3d out;
      out.x = pts3dHom.at<double>( 0 ) / pts3dHom.at<double>( 3 );
      out.y = pts3dHom.at<double>( 1 ) / pts3dHom.at<double>( 3 );
      out.z = pts3dHom.at<double>( 2 ) / pts3dHom.at<double>( 3 );

      middle3d += out;
      ++n2Cams;
    }
  }

  if( n2Cams == 0 )
    throw "no cameras";

  middle3d.at(0) /= static_cast<double>(n2Cams);
  middle3d.at(1) /= static_cast<double>(n2Cams);
  middle3d.at(2) /= static_cast<double>(n2Cams);


  double ret = 0;
  for( unsigned int cam = 0; cam < p.size(); ++cam )
  {
    const BaseRangeVectorType px = p[ cam ]( middle3d );
    ret += ( px - middles.at(cam) ).norm();
  }

#if 1
  auto score = [&]( decltype(middle3d)& A ) -> double
  {
    double s = 0;
    for( unsigned int cam = 0; cam < p.size(); ++cam )
    {
      const BaseRangeVectorType px = p.at( cam )( A );
      const auto mj = middles.at(cam);
      double mys = ( px -  mj ).norm();
      // s += mys;
      s = std::max( s, mys );
    }
    return s;
  };

  double olds = 1e6;
  double firstScore = score(middle3d);

  // iterate camera preimage?
  for( unsigned long iter = 0; iter < 10000; ++iter ) {
    // for( unsigned int cam = 0; cam < nCams; ++cam )
    {
      // std::cout << middle3d << ": ";
      const double s = score(middle3d);
      // std::cout << " -> " << s << std::endl;
      olds = s;
      bool changes = false;

      std::pair< R, double > newCandidate
        = std::make_pair( middle3d, s );

      for( unsigned int d = 0; d < 3; ++d)
      {
        for( double h = 1.0; h > 1.0e-12; h /= 10.0 )
        {
          auto op = middle3d;
          op[d] += h;
          auto om = middle3d;
          om[d] -= h;

          double sp = score(op);
          if( sp < newCandidate.second )
          {
            changes = true;
            newCandidate = std::make_pair( op, sp );
            break;
          }
          double sm = score(om);
          if( sm < newCandidate.second )
          {
            changes = true;
            newCandidate = std::make_pair( om, sm );
            break;
          }
        }
      }

      if( changes ) {
        middle3d = newCandidate.first;
      } else {
        message(bDEBUG, "triangulate score: %f (from %f iter: %d)\n",
            olds, firstScore, iter);
        break;
      }
    }
  }
#endif
  // return olds / static_cast<double>(nCams);
  return olds;
} // triangulate

// know stuff about cameras,
// such as how they are positioned relative to each other.
// THIS ONLY WORKS FOR 3D AND 3 CAMERAS
template<unsigned WORLDIM>
class CAMERA_SETUP{
  typedef RangeVector<2> ProjRangeVector;
  typedef CameraProjectionOperator<WORLDIM, 2> CamType;
  typedef std::vector<CamType const*> CamContainer;
public:
  class offset_probe : public probe{
    offset_probe(CamContainer const& c)
      : probe("offset"),
        _index(0), _c(c)
    {
    }
    double evaluate() const{ untested();
      assert(_index< howmany());
      assert(_c[cam(_index)]);
      double ret = _c[cam(_index)]->ooffset()[ax(_index)];
      ++_index;
      _index %= howmany();
      return ret;
    }

    std::string name(unsigned i) const{ untested();
      return "offset" + std::to_string(cam(i)) + "_" + std::to_string(ax(i));
    }

    unsigned howmany() const{ untested();
      return _c.size()*2;
    }
   private:
    unsigned cam(unsigned i) const{
      return i/2;
    }
    unsigned ax(unsigned i) const{
      return i&1u;
    }
  private:
    mutable unsigned _index;
    CamContainer const& _c;
    friend class CAMERA_SETUP;
  };
public:
  CAMERA_SETUP(std::vector<CameraProjectionOperator<WORLDIM,2> >const& v)
    : _num_cams(v.size()),
      _cal_cam()
  {

       for( auto const& i : v){
         _p.push_back(&i);
       }


    RangeVector<WORLDIM> inversemiddle;
    std::vector< RangeVector<2> > middles(v.size(), RangeVector<2>(1023,1023));
    free_triangulate(middles, inversemiddle, v);
    message(bTRACE, "inverse middle %E %E %E\n", inversemiddle[0], inversemiddle[1], inversemiddle[2]);

    unsigned prevc=3;
    std::vector<bool> done(3);

    for (unsigned a=0; a<3 ; ++a){
      auto ax=v[prevc%3].axis();
      message(bTRACE, "ax%d %E %E %E\n", prevc%3, ax[0], ax[1], ax[2]);

      RangeVector<WORLDIM> inversemiddle_dir = inversemiddle + v[prevc%3].axis();
//      unsigned select=-1u;

      for (unsigned c=0; c<3 ; ++c){
        if(done[c]){
          continue;
        }else{
        }
        auto m0 = v[c](inversemiddle);
        auto m1 = v[c](inversemiddle_dir);
        auto delta = m1 - m0;

        auto n=norm(delta);
        message(bTRACE, "axis %d in camera %d, n %E\n", prevc%3, c, n);

        if(norm(delta)>100){
          message(bTRACE, "p%d middle %E %E\n", c, delta[0], delta[1]);
          // direction is visible here. use that.
          _cal_cam.push_back(c);
          prevc = c;
          done[c]=true;
          _cal_dir.push_back(delta / delta.norm() );
          break;
        }
      }
    }
  }

  probe* new_probe( const std::string& s){
    incomplete();
    if(s=="offset"){
      return new offset_probe(_p);
    }else{
      throw exception_invalid("no such probe, " + s);
    }
  }

public:
  ProjRangeVector const& cdir(unsigned i) const{
    assert(i<_cal_dir.size());
    return _cal_dir[i];
  }

private:
public: // incomplete
  CamContainer _p;
  const unsigned _num_cams;
  std::vector<unsigned> _cal_cam;
  std::vector<ProjRangeVector> _cal_dir;
}; // CAMERA_SETUP
#endif // opencv

#endif // #ifndef PROJETIONOPERATOR_HH

// vim:ts=8:sw=2:et
