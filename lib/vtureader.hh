#ifndef VTUREADER_HH
#define VTUREADER_HH

#include <iomanip>
// #include <string>
// #include <iostream>
#include <memory>

#include <iterator>
#include <sstream>
#include <limits>
#include <cassert>
// Regular expressions
#include <regex>
// File listing (UNIX only)
#include <dirent.h>

#include "tinyxml2.h"

#include "parameter.hh"
#include "mesh.hh"
#include "discretefunction.hh"
#include "vector.hh"
#include "error.hh"

// Parses a series of VTU files, for use with the worm model.
class VTUReader {
    explicit VTUReader(const char* directory_path):m_dir_path_(directory_path)
    {

        this->get_map_of_values();
        //this->debug_printout<std::string>(file_list);
        //std::cout << "The number: " << std::stoi(test_name.substr(5,6)) << std::endl;
        this->debug_pause();
    }

    ~VTUReader(){}

    // Get a map representing the position, curvature pressure tuples
    std::map<int, std::shared_ptr<PositionCurvaturePressureTuple<3>>> get_map_of_values()
    {
        std::map<int, std::shared_ptr<PositionCurvaturePressureTuple<3>>>  all_tuples;

        auto file_list = this->list_dir(m_dir_path_);
        for(auto file: file_list)
        {
            if(!this->is_valid_name(file)) {
                throw exception_invalid(file);
            }else{
                auto path = m_dir_path_ + file;
                auto tup = this->get_tuple_from_file(path.c_str());
                int time_step = this->extract_file_number(file);
                all_tuples[time_step] = tup;
            }
        }
        return all_tuples;
    }

    // Get the position, curvature, pressure tuples from the file at the specified
    // file path
    std::shared_ptr<PositionCurvaturePressureTuple<3>> get_tuple_from_file(const char* full_file_path)
    {
        this->open_file(full_file_path);

        auto curvature_vector = this->convert_data_string_to_vector(this->get_curvature());
        auto coordinate_vector = this->convert_data_string_to_vector(this->get_body_coordinates());
        // Number of points
        auto N = static_cast<unsigned int>(curvature_vector.size());
        // TODO: Get a,b, N from params file
        std::shared_ptr<Mesh> mesh(new Mesh(0.0, 1.0, N));
        std::shared_ptr<PositionCurvaturePressureTuple<3>> tup(new PositionCurvaturePressureTuple<3>(*mesh));

        for(unsigned int j = 0; j < mesh->N(); ++j)
        {
            tup->curvature().assign(j, curvature_vector[j]);
            tup->position().assign(j, coordinate_vector[j]);
        }

        return tup;
    }

    // Return a list of the directories and files in the given directory path
    static std::vector<std::string> list_dir(std::string const& directory_path)
    {
        std::vector<std::string> file_names;
        struct dirent *entry;
        DIR *dir = opendir(directory_path.c_str());
        if (dir == NULL) {
            return file_names;
        }
        // std::vector<std::string> file_names;

        while ((entry = readdir(dir)) != NULL) {
            file_names.push_back(std::string(entry->d_name));
            // printf("%s\n",entry->d_name);
        }

        closedir(dir);
        return file_names;
    }

    // Extract the file number of a specified VTU file
    int extract_file_number(std::string& file_name)
    {
        return std::stoi(file_name.substr(5,6));
    }

    // Check if a file name is a valid VTU file
    bool is_valid_name(const std::string& file_name,
                       const char* reg_ex = "^worm_([0-9]{6})\\.vtu$")
    {
        // Default assumes file name is "worm_", 6 numbers, ".vtu"
        std::regex regexp(reg_ex);
        return regex_match(file_name,regexp);
    }

protected:
    tinyxml2::XMLError open_file(const char* file_name)
    {
        return m_xml_doc.LoadFile(file_name);
    }

    // Extract the curvature from the main XML document
    const char* get_curvature()
    {
        // Gets curvature values. Assumes order in XML file.
        const char* w_whole_string = m_xml_doc.FirstChildElement("VTKFile")->
            FirstChildElement( "UnstructuredGrid" )->
            FirstChildElement( "Piece" )->
            FirstChildElement( "PointData" )->
            FirstChildElement( "DataArray" )->
            GetText();
        return w_whole_string;
    }

    // Extracts the body coordinates from the main XML document
    const char* get_body_coordinates()
    {
        // Gets the body coordinates. Assumes order in XML file.
        const char* u_whole_string = m_xml_doc.FirstChildElement("VTKFile")->
            FirstChildElement( "UnstructuredGrid" )->
            FirstChildElement( "Piece" )->
            FirstChildElement( "PointData" )->
            FirstChildElement( "DataArray" )->
            NextSiblingElement()->
            GetText();
        return u_whole_string;
    }
    // Extracts the cell data from the main XML document
    const char* get_cell_data()
    {
        //TODO: Get the right element
        assert(1==2);
        const char* w_whole_string = m_xml_doc.FirstChildElement("VTKFile")->
            FirstChildElement( "UnstructuredGrid" )->
            FirstChildElement( "Piece" )->
            FirstChildElement( "PointData" )->
            FirstChildElement( "DataArray" )->
            GetText();
        return w_whole_string;
    }

    // Converts a vector in string form to a std::vector<double>
    std::vector<double> convert_data_string_to_vector(const char* input_string)
    {
        std::string s(input_string);
        std::stringstream ss(s);
        std::istream_iterator<std::string> begin(ss);
        std::istream_iterator<std::string> end;
        std::vector<std::string> vstrings(begin, end);

        std::vector <double> doubleVec;
        std::transform(std::begin(vstrings),
                       std::end(vstrings),
                       std::back_inserter(doubleVec),
                       [](std::string s) { return std::stod(s); }
                      );

        return doubleVec;
    }

    // Prints out debug information
    template<typename T>
    void debug_printout(std::vector<T>& input_vector) {
        std::cout << "VTU READER DEBUG:" << std::endl;
        // Print the curvature
        std::copy(input_vector.begin(), input_vector.end(), std::ostream_iterator<T>(std::cout, "\n"));
    }

    // Adds breakpoint for debugging
    void debug_pause() {
        do {
            std::cout << '\n' << "Press key to continue...";
        }
        while (std::cin.get() != '\n');
    }

private:
    std::ifstream m_pvd_file;
    tinyxml2::XMLDocument m_xml_doc;
    const std::string m_dir_path_;
}; // VTUReader

#endif
