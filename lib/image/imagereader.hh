#ifndef IMAGEREADER_HH
#define IMAGEREADER_HH

#include <algorithm>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

#include "opencv2/core.hpp"

#include "../vector.hh"
#include "../distancefunction.hh"

// #include "known_points.hh"
#include "bbox.hh"

#define DISPLAY_KNOWN 0

const uchar c_FULL=-1;
static const cv::Vec3b c_RED(0,0,255);
static const cv::Vec3b c_GREEN(0,255,0);
static const cv::Vec3b c_BLUE(255,0,0);
static const cv::Vec3b c_PURPLE(255,0,255);


//typedef std::map< cv::Point, double, CvPointCompare > KnownList;

// BUG must use ANNOTATION. can be ordered etc.
//typedef std::map< cv::Point, double, CvPointCompare > KnownListType;

template< const unsigned int mydim >
struct DistanceFunctionImage;


// gets a sequence of pixel coordinates ((x_1, .... x_n), y)
// weeds out consecutive extrema (or something like that).
// pushes into known. this is used for the curve fitter objective.
template<class V, class K>
void filter_maxes(V const& myMaxes,
      unsigned x, unsigned y,
      unsigned dx, unsigned dy,
      /*cv::Mat& maxes,*/ K& known
      //cv::Vec3b color
      )
{
   if( myMaxes.size() > 0 ) { itested();

      std::vector< int > consecutive = { myMaxes.at(0) };
      auto mi=myMaxes.begin();
      if(mi!=myMaxes.end()) ++mi;
      for(; mi!=myMaxes.end(); ++mi ) {
	 if( *mi == consecutive.back()+1 ) { itested();
	    consecutive.push_back( *mi );
	 } else { itested();
	    int myX = -1;

	    if( consecutive.size() == 1 )
	       myX = consecutive.at(0);
	    else if( consecutive.size() % 2 == 0 )
	       myX = consecutive.at( consecutive.size()/2 );
	    else
	       myX = ( consecutive.at( consecutive.size()/2 )
		     + consecutive.at( consecutive.size()/2 ) ) / 2;

	    ++known[cv::Point( x+dx*myX, y+dy*myX)];
//	    maxes.at<cv::Vec3b>( y+dy*myX, x+dx*myX )=color;

	    consecutive = { *mi };
	 }
      }

      {
	 int myX = -1;

	 if( consecutive.size() == 1 )
	    myX = consecutive.at(0);
	 else if( consecutive.size() % 2 == 0 )
	    myX = consecutive.at( consecutive.size()/2 );
	 else
	    myX = ( consecutive.at( consecutive.size()/2 )
		  + consecutive.at( consecutive.size()/2 ) ) / 2;

//	 maxes.at<cv::Vec3b>(y+dy*myX, x+dx*myX) = color;
	    ++known[cv::Point( x+dx*myX, y+dy*myX)];
      }
   }else{
   }
} // f

#if 0 // unused?
static void findSrcEdges( cv::Mat& edges, cv::Mat const& src_ )
{
   cv::Canny( src_, edges, 50, 200 );
}
#endif

#endif // #ifndef IMAGEREADER_HH

// vim:ts=8:sw=2
