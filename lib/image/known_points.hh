#ifndef KNOWN_POINTS_HH
#define KNOWN_POINTS_HH
#include "opencv2/core.hpp"
#include "../trace.hh"
#include "bbox.hh"
#include "imagereader.hh"
#include <algorithm>
#include <deque>

#define KPassert(x)

static bool use_diagonals=true;

struct CvPointCompare {
   bool operator()( const cv::Point& a, const cv::Point& b ) const
   { untested();
      if( a.x == b.x )
	 return (a.y < b.y);
      else
	 return (a.x < b.x);
   }
};

#if 0
namespace std{

template<>
struct less<cv::Point> : public binary_function<cv::Point, cv::Point, bool>
				  , CvPointCompare {};

} // std
#endif


// BUG: must be a template arg.
// BUG2: need to get rid of virtual inheritance first
//typedef std::map< cv::Point, double, CvPointCompare > KnownList;

template<class KnownList>
class known_points_finder{

public: // BUG: uses class inheritance.
	known_points_finder(){};
	virtual ~known_points_finder(){};

	void set_cam(unsigned c){ _cam=c; }
	void set_frame(unsigned f, unsigned c=-1u){
		_frame = f;
		if(c==-1u){ untested();
			// omitted.
		}else{
			_cam = c;
		}
	}

	// YUCK. argument 2 is not const, but it's not scratch either....?
	// src_ is the silhouette.
	virtual void find_known_points(cv::Mat const& src_,
	                               cv::Mat& raw2,
	                               KnownList& known, float ppmm) const=0;

	virtual void dump_stats(std::ofstream&) const=0;
	// { unreachable(); }
	//
public: // more virtual. cleanup later
	void set_eucl_base(double){ }


protected:
	unsigned _cam;
	unsigned _frame;
};

namespace draft{

template<class KnownList>
class MidlineFinderA : public known_points_finder<KnownList>{
public:
  virtual ~MidlineFinderA(){}
public:


// YUCK. argument 2 is not const, but it's not scratch either....?
// src_ is the silhouette.
  void find_known_points(cv::Mat const& sil, cv::Mat& raw2,
		  KnownList& known, float ppmm) const
  {
	  message(bDEBUG, "default fkp\n");
	  int minx = sil.cols, miny = sil.rows, maxx = 0, maxy = 0;
	  for( int i = 0; i < 2048; ++i ){ itested();
		  for( int j = 0; j < 2048; ++j ) { itested();
			  if( sil.at<uchar>(j,i) == (uchar)255 ) { itested();
				  maxx = std::max( maxx, i+15 );
				  minx = std::min( minx, i-15 );
				  maxy = std::max( maxy, j+15 );
				  miny = std::min( miny, j-15 );
			  }
		  }
	  }

	  cv::GaussianBlur( raw2, raw2, cv::Size(3,3), 1, 1 );
	  //   double min, max;
	  //   cv::minMaxLoc( raw, &min, &max );
	  // raw *= 255.0 / max;


	  //  cv::cvtColor( raw, raw2, CV_BGR2GRAY );
#if 0 // not here.
	  cv::Mat maxes = raw.clone();
	  (void)maxes; // later;
#endif


	  std::deque<int> myMaxes;
	  // find max and min cols and rows
	  const int minCol = 0;
	  const int maxCol = raw2.cols-1;
	  const int minRow = 0;
	  const int maxRow = raw2.rows-1;

	  {
		  // window size
		  auto bbr(make_bounding_box_range(sil, raw2, 10, 0,
					  minCol, maxCol, minRow, maxRow));

		  for( int y = miny; y <= maxy; ++y ) { itested();
			  myMaxes.clear();
			  KPassert(maxx-minx>=0);
			  bbr.collect(myMaxes, minx, y, 1, 0, maxx-minx+1 /*yuck*/);
			  filter_maxes(myMaxes, minx, y, 1, 0, known);
			  //	 color_maxes(maxes, known, c_BLUE); later;
		  }
	  }
	  {
		  // define window size
		  auto bbr(make_bounding_box_range(sil, raw2, 0, 10,
					  minCol, maxCol, minRow, maxRow));

		  // columns
		  for( int x = minx; x <= maxx; ++x ) { itested();
			  myMaxes.clear();
			  bbr.collect(myMaxes, x, miny, 0, 1, maxy-miny+1 /*yuck*/);
			  filter_maxes(myMaxes, x, miny, 0, 1, known);
		  }
	  }
	  if(use_diagonals) { // not yet.
		  // define window size
		  auto bbr(make_bounding_box_range(sil, raw2, 0, 10 /* not used? */,
					  minCol, maxCol, minRow, maxRow));

		  // columns
		  for( int x = minx; x <= maxx-10; ++x ) { itested();
			  myMaxes.clear();
			  bbr.collect(myMaxes, x, miny, 1, 1, maxy-miny+1 /*yuck*/);
			  filter_maxes(myMaxes, x, miny, 1, 1, known);// , c_RED
		  }
		  for( int y = miny+1; y <= maxy-10; ++y ) { itested();
			  myMaxes.clear();
			  bbr.collect(myMaxes, minx, y, 1, 1, maxy-miny+1 /*yuck*/);
			  filter_maxes(myMaxes, minx, y, 1, 1, known);
		  }
	  }
	  if(use_diagonals) { // not yet.
		  // define window size
		  auto bbr(make_bounding_box_range(sil, raw2, 0, 10 /* not used? */,
					  minCol, maxCol, minRow, maxRow));

		  // columns
		  for( int x = minx; x <= maxx-10; ++x ) { itested();
			  myMaxes.clear();
			  bbr.collect(myMaxes, x, maxy, 1, -1, maxy-miny+1 /*yuck*/);
			  filter_maxes(myMaxes, x, maxy, 1, -1, known);
		  }
		  for( int y = 10; y <= maxy-1; ++y ) { itested();
			  myMaxes.clear();
			  bbr.collect(myMaxes, minx, y, 1, -1, maxy-miny+1 /*yuck*/);
			  filter_maxes(myMaxes, minx, y, 1, -1, known);
		  }
	  }
	  {
#if DISPLAY_KNOWN
		  { untested();
			  cv::Mat edges;
			  findSrcEdges( edges, sil);
			  KnownList myKnown;
			  convertWhitePointsToValueList( edges, myKnown, 0.0 );
			  for( const auto& pair : myKnown )
			  { untested();
				  const auto pt = pair.first;
				  maxes.at< cv::Vec3b >( pt )[0] = (uchar)(255);
				  maxes.at< cv::Vec3b >( pt )[1] = (uchar)(255);
				  maxes.at< cv::Vec3b >( pt )[2] = (uchar)(255);
			  }
		  }
		  cv::imshow( "tmp", maxes.colRange( minx -15, maxx+15 ).rowRange( miny-15, maxy+15 ) );
		  cv::imwrite( "tmp.png", maxes );
		  cv::waitKey(0);
#endif
	  }
  }

private: // override
	virtual void dump_stats(std::ofstream& o) const{ o << "incomplete"; }

}; // MidlineFinderA

} // draft

#endif
