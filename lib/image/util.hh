#pragma once

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

cv::Point& get_point(std::pair<cv::Point, double>& p)
{
  return p.first;
}
cv::Point const& get_point(std::pair<cv::Point, double> const& p)
{
  return p.first;
}

cv::Point const& get_point(cv::Point const& p)
{
  return p;
}

cv::Point& get_point(cv::Point& p)
{
  return p;
}

// get the bounding box. eg from a silhouette
std::pair<cv::Point, cv::Point> bbox(cv::Mat const& sil)
{
	cv::Point min, max;
   min.x = sil.cols;
   min.y = sil.rows;
	max.x = max.y = 0;

	for( int i = 0; i < sil.rows; ++i ){ itested();
		for( int j = 0; j < sil.cols; ++j ) { itested();
			if( sil.at<uchar>(j,i) == (uchar)255 ) {
				max.x = std::max( max.x, i );
				min.x = std::min( min.x, i );
				max.y = std::max( max.y, j );
				min.y = std::min( min.y, j );
			}else{
			}
		}
	}
	// it's a range...
	++max.x;
	++max.y;
	return std::make_pair(min,max);
}

void adjust_brightness_max(cv::Mat& image)
{
	unsigned max=0;
	for( int y = 0; y < image.rows; y++ ) {
		for( int x = 0; x < image.cols; x++ ) {
			if (255==max){
				return;
			}else if (image.at<unsigned char>(y,x) > max){
				max = image.at<unsigned char>(y, x);
			}else{
			}
		}
	}

	for( int y = 0; y < image.rows; y++ ) {
	  	for( int x = 0; x < image.cols; x++ ) {
			unsigned char c=image.at<unsigned char>(y,x);
			image.at<unsigned char>(y,x) = (c*255)/max;
		}
	}
}

void adjust_brightness_max(cv::Mat& image, cv::Mat const& mask)
{
	unsigned max=0;
	for( int y = 0; y < image.rows; y++ ) {
		for( int x = 0; x < image.cols; x++ ) {
			if (!mask.at<unsigned char>(y,x)){
				// skip
			}else if (255==max){
				return;
			}else if (image.at<unsigned char>(y,x) > max){
				max = image.at<unsigned char>(y, x);
			}else{
			}
		}
	}

	for( int y = 0; y < image.rows; y++ ) {
	  	for( int x = 0; x < image.cols; x++ ) {
			if (mask.at<unsigned char>(y,x)){
				unsigned char c=image.at<unsigned char>(y,x);
				image.at<unsigned char>(y,x) = (c*255)/max;
			}else{
			}
		}
	}
}

template<class K>
void kpframe(cv::Mat& m, K const& p)
{ itested();

	m.setTo(0);

	for(auto i: p){
		int X=get_point(i).x;
		int Y=get_point(i).y;
		m.at<uchar>(Y, X) = 255;
	}
}


void dilate_circular(cv::Mat& m, unsigned radius)
{ itested();
	if(!radius){
		return;
	}
   cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
	 cv::Size(radius*2+1, radius*2+1), cv::Point(radius, radius) );

   cv::Mat dilated;
   cv::dilate(m, dilated, element);
   m=dilated;
}
