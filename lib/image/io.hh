#ifndef WORM_IMAGE_HH
#define WORM_IMAGE_HH

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>

#include "../error.hh"
#include "util.hh"
//#include "../io_misc.hh"

// here?
int get_x(cv::Point_<int> const& p){ itested();
	 return p.x;
}
int get_y(cv::Point_<int> const& p){ itested();
	 return p.y;
}

// pairs are something like a point with a weight.
template<class A, class B>
int get_x(std::pair<A, B> const& p){ untested();
	 return p.first.x;
}
template<class A, class B>
int get_y(std::pair<A, B> const& p){ untested();
	 return p.first.y;
}


void getframefromfile(std::string filename, cv::Mat& bgrframe, unsigned framenumber);

inline void get_grayscale_image( const std::string& filename, cv::Mat& output )
{
  output = cv::imread( filename, CV_LOAD_IMAGE_GRAYSCALE );

  if( output.empty() ){
    throw exception_nosuchfile("unable to open background file '" + filename + "'");
  }else{
  }

#ifndef NDEBUG
  cv::Mat test;
  getframefromfile(filename, test, 0);

  cv::bitwise_xor(test, output, test);
  assert(!cv::countNonZero(test));

#endif
	     //do stu
}

inline void getBackgroundImage( const std::string& fn, cv::Mat& output )
{
	get_grayscale_image(fn, output);
}


void getframefromfile(std::string filename, cv::Mat& bgrframe, unsigned framenumber)
{
	trace2("getframefromfile", framenumber, filename);
	cv::VideoCapture vsrc;
	vsrc.open(filename); // dir+"/"+avifilename);
	if(!vsrc.isOpened()){
		throw exception_nosuchfile(filename);
	}else{
	}
	cv::Mat frame;

	// framenumber "0" means framenumber "1"

//	use grab instead
	for(unsigned i=0; i<=framenumber; ++i){ itested();
		// skip.
		vsrc >> frame;
		if(!frame.type()){
			throw exception_corrupt("broken frametype" + filename);
		}else if(frame.empty()){
			throw exception_corrupt(filename);
		}else{
		}
	}
	{
		// convert to b&w and store
		// // BUG convert if needed only
		cv::cvtColor(frame, bgrframe, CV_BGR2GRAY);
		assert(!bgrframe.empty());
	}
	// display_grayscale_frame("from avi", bgrframe);
	vsrc.release();
}


// :template<class S>
// :S& operator<<(S& s, cv::Point2f const& p)
// :{
// :	s << p.x << ", " << p.y;
// :	return s;
// :}

void dump_frame(std::string filename, cv::Mat input
		// , std::pair<cv::Point, cv::Point> bbox=all
		 )
{
//	auto min=bbox.first;
//	auto max=bbox.second;
//	auto bb=input.colRange( min.x, max.x ).rowRange( min.y, max.y );
	message(bTRACE, "imwrite %s\n", filename.c_str());
	cv::imwrite(filename, input );
}

void dump_grayscale_frame(std::string filename, cv::Mat input, std::pair<cv::Point, cv::Point> bbox)
{
	auto min=bbox.first;
	auto max=bbox.second;
	auto bb=input.colRange( min.x, max.x ).rowRange( min.y, max.y );
	message(bTRACE, "imwrite %s\n", filename.c_str());
	cv::imwrite(filename, bb );
}

template<class K>
static void pointset_bb( K const& p, int& lbx, int& lby, int& ubx, int& uby, cv::Mat const& input)
{
	assert(input.cols);
	assert(input.rows);

	message(bTRACE, "boundingbox %d pts\n", p.size());
	message(bTRACE, "boundingbox in %d %d, %d %d\n", lbx, lby, ubx, uby);
	for(auto i: p){
		int X=get_x(i);
		int Y=get_y(i);
		lbx=std::min(lbx, X);
		lby=std::min(lby, Y);
		ubx=std::max(ubx, X);
		uby=std::max(uby, Y);
	}
	message(bTRACE, "boundingbox raw %d %d, %d %d\n", lbx, lby, ubx, uby);
	lbx=std::max(0, lbx-15);
	lby=std::max(0, lby-15);
	ubx=std::min(input.cols, ubx+15);
	uby=std::min(input.rows, uby+15);
	message(bTRACE, "boundingbox %d %d, %d %d\n", lbx, lby, ubx, uby);
}

template<class K>
inline void dump_pointset_on_frame(std::string label, cv::Mat input, K const& p, uchar color=255)
{
	cv::Mat M;
	cv::cvtColor( input, M, CV_GRAY2BGR );

	if(p.empty()){
		message(bDANGER, "cannot display pointset %s. it's empty", label.c_str());
		return;
	}else{
	}

	int lbx=99999; // intmax...
	int lby=99999; // intmax...
	int ubx=0;
	int uby=0;

	pointset_bb(p, lbx, lby, ubx, uby, input);
	untested();

	for(auto i: p){
		int X=get_x(i);
		int Y=get_y(i);
		// M.at<uchar>(Y, X) = color;
		cv::Vec3b color( 0, 0, 255);
		M.at<cv::Vec3b>(Y, X) = color;
	}

	message(bTRACE, "show bb %i %i %i %i\n", lbx, lby, ubx, uby);
	auto bb=std::make_pair( cv::Point(lbx, lby), cv::Point(ubx, uby));
	dump_grayscale_frame(label, M, bb);
}

#endif
