#pragma once

#include "../trace.hh"

template<class M>
class bounding_box_range {
private:
   bounding_box_range(){unreachable();}
   // bounding_box_range(const bounding_box_range&){unreachable();}
public:
public:
   bounding_box_range(
	 M const& src,
	 M const& raw2,
      unsigned windowsizex,
      unsigned windowsizey,
      unsigned minCol, unsigned maxCol,
      unsigned minRow, unsigned maxRow
      )
    : _src(src), _raw2(raw2), _windowsizex(windowsizex),
    _windowsizey(windowsizey),
	_minCol(minCol),
	_maxCol(maxCol),
	_minRow(minRow),
	_maxRow(maxRow) {
	}
   
public:
   void find_maximum_value_in_box( unsigned x, unsigned y,
	 double&windowMin, double&windowMax) const
   {
      auto this_column=_raw2.colRange( std::max( x - _windowsizex, _minCol ),
	    std::min( x + _windowsizex + 1, _maxCol ) );
      {
	 cv::minMaxLoc( this_column.rowRange( std::max( y - _windowsizey, _minRow ),
		  std::min( y + _windowsizey +1, _maxRow ) ),
	       &windowMin, &windowMax );
      }
   }
   void find_maximum_value_on_stroke( unsigned x, unsigned y,
	 unsigned dx, unsigned dy, unsigned howmany,
	 double&windowMin, double&windowMax) const
   {
      windowMin=-1.;
      int i=-howmany;
      windowMax=-1.;
      for(; i<int(howmany); ++i){
	 int X=x+dx*i;
	 int Y=y+dy*i;

	 if(X<int(_minCol)){ itested();
	    continue;
	 }else if(Y<int(_minRow)){ itested();
	    continue;
	 }else if(X>=int(_maxCol)){ itested();
	    continue;
	 }else if(Y>=int(_maxRow)){ itested();
	    continue;
	 }

	 uchar value= _raw2.at<uchar>(Y, X);
	 if(windowMax < value){ itested();
	    windowMax=value;
	 }else{ itested();
	 }
      }
   }
   template<class V>
   void collect(V& myMaxes, unsigned x, unsigned y, int dx, int dy,
	 unsigned howmany){
      for(int i=0; i<int(howmany); ++i){

	 double windowMin;
	 double windowMax;

	      {

		 // hmm only works if x or y aligned
#if 0
		find_maximum_value_in_box(x+dx*i, y+dy*i, windowMin, windowMax);
		trace3("box", windowMax, dx, dy);
#else
		find_maximum_value_on_stroke(x+dx*i, y+dy*i, dx, dy, howmany, windowMin, windowMax);
#endif

		if(int(y)+dy*i>=_src.rows){
			// out if range...
		}else if(int(x)+dx*i>=_src.cols){
			// out if range...
		}else if(int(y)+dy*i<0){
			// out if range...
		}else if(int(x)+dx*i<0){
			// out if range...
		}else if( _src.at<uchar>(y+dy*i,x+dx*i)!=(uchar)255 ){ itested();
		   // outside of the worm silhouetta
		   // maxes.at<cv::Vec3b>( y, x )[0] = c_FULL; // paint it blue.
		}else if( windowMax < 1 ){ untested();
		   // all black
		}else if( (int)_raw2.at<uchar>(y+dy*i,x+dx*i) < windowMax){
		   // windowMax is the maximum value in this area.
		   // only mark the brightest pixels
		}else if( _src.at<uchar>(y+dy*i,x+dx*i)==(uchar)255 ) {
		    myMaxes.push_back(i);
		}else{
		    //maxes.at<cv::Vec3b>( y, x )[0] = c_FULL; // paint all candidates blue.
		}
	      } // done column
      }

   }
private:
   const cv::Mat& _src;
   const cv::Mat& _raw2;
	unsigned _windowsizex, _windowsizey;
	unsigned _minCol, _maxCol, _minRow, _maxRow;
}; // bounding_box_range

template<class M>
bounding_box_range<M> make_bounding_box_range(
      M const& s,
      M const& r,
      unsigned a,
      unsigned b,
      unsigned c,
      unsigned d,
      unsigned e,
      unsigned f
      )
{
	return bounding_box_range<cv::Mat>(s, r, a, b, c, d, e, f);
}
