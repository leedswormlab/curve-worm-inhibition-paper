#pragma once

#include "io.hh"
#include <opencv2/xfeatures2d.hpp>

using cv::xfeatures2d::FREAK;
// provide map keypoints->feature
class FREAKMAP{
public: // types
	typedef std::array<uchar, 64> descriptor_type;
	typedef cv::KeyPoint keypoint_type;
	typedef std::vector<keypoint_type> keypoints_type; // yuck, freak wants that.
private:
	FREAKMAP() {}
	FREAKMAP(const FREAKMAP&) {}
public: // construct
	FREAKMAP(keypoints_type /* BUG const */ & keypoints, cv::Mat const& frame, float patternScale)
		: _keypoints(keypoints.begin())
	{
		message(bTRACE, "computing descriptor for %d pxs, ps %f\n", keypoints.size(), patternScale);
		assert(patternScale);

		bool orientationNormalized=true;// Enable orientation normalization.
		bool scaleNormalized=false; // Enable scale normalization.
		unsigned nOctaves=4; // Number of octaves covered by the detected keypoints.


		auto _backend=FREAK::create( orientationNormalized, scaleNormalized,
				patternScale, nOctaves);

		const unsigned extrascale=4*patternScale; // why 4?!

		size_t oldsize=keypoints.size();
		for(auto& k:keypoints){
			k.pt.x += extrascale;
			k.pt.y += extrascale;
		}

		cv::Mat bigger(frame.cols+2*extrascale, frame.rows+2*extrascale, CV_8UC1, cv::Scalar(0));
		cv::Mat mid(bigger, cv::Rect(extrascale, extrascale, frame.cols, frame.rows));
		frame.copyTo(mid);

		trace2("b4", keypoints.front().pt.x, keypoints.front().pt.y);
		// display_grayscale_frame("frame", frame);
		// display_grayscale_frame("bigger", bigger);
		_backend->compute(bigger, keypoints, _descriptors);
		trace2("after", keypoints.front().pt.x, keypoints.front().pt.y);

		for(auto& k:keypoints){
			k.pt.x -= extrascale;
			k.pt.y -= extrascale;
		}
		if(oldsize!=keypoints.size()){ untested();
			std::cerr << "too close to border?\n";
		}else{
		}
		message(bTRACE, "freakmap successful\n");
		assert(oldsize==keypoints.size());
	}
public: // access
	const uchar* get_features(unsigned pos) const{ itested();
		const uchar* p = _descriptors.ptr<uchar>(pos);
		return p;
	}
	const uchar* get_features(const keypoint_type& k) const{ itested();
		unsigned position=(&k)-(&*_keypoints);
		const uchar* p = _descriptors.ptr<uchar>(position);
		return p;
	}
	const uchar* get_descriptor(const keypoint_type& k) const{ itested();
		return get_features(k);
	}
	long unsigned distance_square(const keypoint_type& k, uchar* b, unsigned t=-1u) const{ itested();
		uchar const* a=get_descriptor(k);

		unsigned ret=0;
		for(unsigned i=0; i<64; ++i){ itested();
			int delta = int(a[i]) - int(b[i]);
			ret+=delta*delta;
#if 0 // does it help?
			if(ret>=t){ untested();
				// too far. we do not care.
				return t;
			}else{ untested();
			}
#endif
		}
		return ret;
	}
	static long unsigned distance_square(const descriptor_type& a,
	                                     const descriptor_type& b){
		unsigned ret=0;
		for(unsigned i=0; i<64; ++i){ itested();
			int delta = int(a[i]) - int(b[i]);
			ret+=delta*delta;
		}
		return ret;
	}

private:
	const keypoints_type::const_iterator _keypoints;
	cv::Mat _descriptors;
};

// FIXME: class ... public K
template<class K>
void fill_keypoints(K& keypoints, cv::Mat const& sil)
{
	for( int y = 0; y < sil.rows; ++y ){ itested();
		for( int x = 0; x < sil.cols; ++x ) { itested();
			if( sil.at<unsigned char>(y, x) == (unsigned char)255 ) { itested();
//				trace2("fillkp", x, y);
				keypoints.push_back(cv::KeyPoint(float(x), float(y), 1));
			}else{ itested();
			}
		}
	}
}
