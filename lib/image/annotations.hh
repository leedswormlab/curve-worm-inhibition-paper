#ifndef WORM_ANNOT_HH
#define WORM_ANNOT_HH

#include "io.hh"
#include <set>
#include <list>
#include <deque>
#include "thinning.hh"
#include "display.hh"
#include "util.hh"

#define ANassert(x) assert(x)

double get_weight(std::pair<cv::Point, double>& p)
{
  return p.second;
}

double const& get_weight(std::pair<cv::Point, double> const& p)
{
  return p.second;
}
double get_weight(cv::Point& p)
{
  return 1.;
}

#if 1
// BUG. this is in some other place...?
struct CvPointCompare2
{
   bool operator()( const cv::Point& a, const cv::Point& b ) const
   {
      if( a.x == b.x )
	 return (a.y < b.y);
      else
	 return (a.x < b.x);
   }
};
namespace std{

template<>
struct less<cv::Point> : public binary_function<cv::Point, cv::Point, bool>
				  , CvPointCompare2 {};

} // std
#endif

namespace framestuff{

template<class P>
double normsq(P p)
{
	return (p.x*p.x) + (p.y*p.y);
}

typedef cv::Point coord_type;

class ANNOTATION : protected std::map<coord_type, double> {
public:
	typedef std::deque<coord_type> vector_type;
	typedef vector_type::const_iterator const_iterator;

	typedef std::map<coord_type, double> container_type;
	typedef container_type::const_iterator wconst_iterator; // weighted.
public: // construct
	explicit ANNOTATION(): _mask(NULL){
	}

public:
	// obsolete? just use weight!=0?
	bool contains(coord_type const& pt) const{ itested();
		return container_type::find(pt)!=container_type::end();
	}
	void postprocess(unsigned howoften, unsigned thinning_type);

  void printKnown( std::ostream& s ) const
  {
	  s << "x,y" << std::endl;

	  for( auto pt : _vector )
	  {
		  s << pt.x << ","
			  << pt.y << std::endl;
	  }
  }

public: // implementation
	template<class S>
	void parse(S& fs){
		if(_ordered){
		}else{
		}

		std::string line;
		std::getline(fs, line);
		unsigned skipped=0;
		while(!fs.eof()){
			auto p=line.find(',');
			ANassert(p!=std::string::npos);

			unsigned X; unsigned Y;
			X=atoi(line.substr(0,p).c_str());
			++p; // skip ,
			Y=atoi(line.substr(p, std::string::npos).c_str());
			//_vector.push_back(coord_type(X,Y));
			bool add_it;
			if(X<2048){
			}else{
				incomplete();
				X=2047;
			}
			if(Y<2048){
			}else{
				incomplete();
				Y=2047;
			}
			if(_mask){
				add_it=true;
				uchar sil=_mask->at<uchar>(Y, X);
//				std::cerr << "SIL " << X << " " << Y << " sil " << sil << "\n";
				if(sil){
				}else{
					add_it=false;
				}

			}else{
				add_it=true;
			}

			if(add_it){
				operator[](coord_type(X,Y));
			}else{
				++skipped;
			}
			std::getline(fs, line);
		}
		message(bDEBUG, "read %d keypoints. skipped %d\n", _vector.size(), skipped);
		size();
		size_t tmpsize=_vector.size()-1;
		unsigned pos=0;

		for(auto i : _vector){
			float where=float(pos)/float(tmpsize);
			double wt = .5 + (where-.5)*(where-.5)*2;
			operator[](i) = wt; // need std::less
			++pos;
		}
	}

	void clear(){
		container_type::clear();
		_vector.clear();
	}
	void push_back(coord_type x){
		_vector.push_back(x);
		container_type::operator[](x);
	}
	double& operator[](const coord_type& x){
		auto a=container_type::find(x);
		if(a==container_type::end()){
			_vector.push_back(x);
			return container_type::operator[](x);
		}else{
			return a->second;
		}
	}
	cv::Point_<int> const& front() const{return _vector.front();}
	const_iterator begin() const{return _vector.begin();}
	const_iterator end() const{return _vector.end();}

	wconst_iterator wbegin() const{return container_type::begin();}
	wconst_iterator wend() const{return container_type::end();}

	bool empty() const{
		return container_type::empty();
	}
	size_t size() const{
		if(_ordered){
			message(bTRACE, "%d %d\n", container_type::size(), _vector.size());
			assert(container_type::size()==_vector.size());
			return _vector.size();
		}else{
			return container_type::size();
		}
	}

	// HACK. should be free
	size_t numKnown() const{
		assert(container_type::size()==_vector.size());
		return _vector.size();
	}


//	template<class X>
//	const_iterator find(X x) const{return _vector.find(x);}
	bool is_ordered() const{
		// sanitycheck?!
		return _ordered;
	}
	void set_ordered(bool o=true){
		// sanitycheck?!
		_ordered = o;
	}

public:
//	void prune_silhouette(cv::Mat const& silh){
//      message(bDEBUG, "pruning non-silhouette points %d\n", known.size());
//	}

	void set_mask(cv::Mat const& m){
		assert(!_mask);
		_mask=&m;
	}
private:
	vector_type _vector;
	bool _ordered; // incomplete...
	cv::Mat const* _mask;
};

class annotated_frame{
public:
	typedef cv::Mat frame_type;
	typedef cv::Mat sil_type;
	typedef ANNOTATION annotation_type;
	enum CSV {_CSV};
public:
	annotated_frame() {
	}
	annotated_frame(const std::string& csv, CSV, bool bg);

	// use that background. don't attempt to find one yourself.
	annotated_frame(const std::string& csv, cv::Mat const&  bg);

	const frame_type& frame() const {
		return _bgrframe;
	}
	const annotation_type& annotation() const {
		return _annotation;
	}
	const float ppmm() const {
		return _ppmm;
	}

//	const sil_type& get_silhouette() const; // hrgl
	std::string label() const{return _label;}

	std::pair<coord_type, coord_type> bounding_box() const;
	std::pair<coord_type, coord_type> show_me(cv::Mat&) const;
	std::pair<coord_type, coord_type> show_me_colour(cv::Mat&) const;
	std::pair<coord_type, coord_type> rawimg(cv::Mat&) const;
	void postprocess(unsigned a, unsigned b){
		_annotation.postprocess(a,b);
	}

	template<class K>
	double distance(K const& guess) const{
		return distance(guess, _annotation);
	}
	template<class K, class A>
	static double distance(K const& guess, A const& ann){
		double ret=0.;
		for(auto ia=ann.wbegin(); ia!=ann.wend(); ++ia){
			auto a=*ia;
			double m=1e99;
			for(auto& k: guess){
				m = std::min(m, normsq(get_point(k) - get_point(a)));
			}
			ret += m*get_weight(a);
		}
		double s=ret/ann.size();
		ret=0.;
		for(auto& k: guess){
			double m=1e99;
			for(auto& a: ann){
				m = std::min(m, normsq(get_point(k) - get_point(a)));
			}
			ret += m*.5;
		}
		return ret/guess.size() + s;
	}
public:
	bool is_annotated(coord_type const& pt) const{ itested();
		return _annotation.contains(pt);
	}
private:
	std::string bgimgname(std::string const& aviname) {
		auto p=aviname.find("cam");
		std::string ret="BGimg_" + aviname.substr(0, p+4) + ".png";
		return ret;
	}

private:
	frame_type _bgrframe;
	annotation_type _annotation;
	std::string _label;
	float _ppmm;
};

static void parse_ifstream(std::ifstream& fs, float& ppmm,
		annotated_frame::annotation_type& annotation,
	std::string& avifilename,
	std::string& dir, unsigned& framenumber, cv::Mat const* sil=NULL);

static void parse_csv(std::string const& csv, float& ppmm,
		annotated_frame::annotation_type& annotation,
	std::string& avifilename,
	std::string& dir, unsigned& framenumber)
{
	message(bDEBUG, "annframe from csv " + csv + "\n");
	size_t last=csv.find_last_of("/");
	dir=csv.substr(0, last);

	std::ifstream fs(csv);
	assert(fs.is_open());

	try{
		parse_ifstream(fs, ppmm, annotation, avifilename, dir, framenumber);
	}catch (exception_corrupt& e){
		std::cerr << "????" << csv << "\n";
		message(bDANGER, "while reading %s... %f\n", csv, ppmm);
		throw e;
	}
}

static void parse_ifstream(std::ifstream& fs, float& ppmm,
		annotated_frame::annotation_type& annotation,
	std::string& avifilename,
	std::string& dir, unsigned& framenumber, cv::Mat const* sil)
{

	std::string line;

	// YUCK. ordered annotations are missing a tag.
	// try this...
	annotation.set_ordered(true);
	
	message(bTRACE, "first line " + line + "\n");
	while(!fs.eof()){ itested();
		message(bTRACE, "annframe line " + line + "\n");
		if(fs.peek()!='#'){
			break;
		}else{
			std::getline(fs, line);
		}

		if(line.substr(0, 18) == "# video file name:"){ itested();
			avifilename = line.substr(19);
		}else if(line.substr(0, 16) == "# magnification:"){ itested();
			ppmm = atof(line.substr(17).c_str());
			message(bTRACE, "mag %f\n", ppmm);
		}else if(line.substr(0, 22) == "# magnification px/mm:"){ itested();
			ppmm = atof(line.substr(23).c_str());
			message(bTRACE, "mag %f\n", ppmm);
		}else if(line.substr(0, 17) == "# unordered: true"){
			annotation.set_ordered(false);
		}else if(line.substr(0, 15) == "# frame number:"){ itested();
			framenumber = atoi(line.substr(16).c_str());
		}else{ itested();
		}
	}

	if(avifilename==""){ untested();
		//throw exception_corrupt("no avi file there");
		message(bLOG, "no avi file here\n");
	}else if(framenumber==-1u){ untested();
		throw exception_corrupt("missing frame number");
	}else{ itested();
	}

	annotation.parse(fs);
	message(bTRACE, "annframe line %d\n", annotation.size());

	if(!ppmm){
		message(bDANGER, "no ppmm, using random (150)\n");
		ppmm=150;
	}else{
	}

}

annotated_frame::annotated_frame(const std::string& csv, annotated_frame::CSV,
	  bool bg=true	)
	: _label(csv), _ppmm(0)
{
	std::string avifilename="";
	std::string dir="";
	unsigned framenumber=-1u;
	parse_csv(csv, _ppmm, _annotation, avifilename, dir, framenumber);

	if(!_annotation.size()){
		throw exception_invalid("empty " + csv + "\n");
	}else{
	}

	std::string bgname=bgimgname(avifilename);

	cv::Mat background;
	try{
		getBackgroundImage( dir+"/"+bgname, background);
		message(bLOG, "got bg from " + dir+"/"+bgname + "\n");
	}catch(exception_nosuchfile const& e){ untested();
		std::cerr << e.what() << "\n";
		throw;
	}catch(exception_corrupt const& e){ untested();
		std::cerr << e.what() << "\n";
		throw;
	}

	if(framenumber){ itested();
		// data hack. zero and one are both first frame
		--framenumber;
	}else{ itested();
	}
	message(bTRACE, "getting frame %d from %s/%s\n",
			framenumber, dir.c_str(), avifilename.c_str());

	cv::Mat bgrframe;

	auto filename = dir+"/"+avifilename;
	getframefromfile(filename, bgrframe, framenumber);

	if(bg){
		_bgrframe = background - bgrframe;
	}else{
		_bgrframe=bgrframe;
	}
//	display_grayscale_frame("tmp", _bgrframe);

	adjust_brightness_max(_bgrframe);

#ifdef DEBUG
	untested();
	cv::Mat silfrm(_bgrframe.clone());
	silhouetteImage(silfrm, silfrm, 0);
	std::cout << "debug annotated nonzero " << countNonZero(silfrm) << "\n";
#endif

}

annotated_frame::annotated_frame(const std::string& csv, cv::Mat const& background )
	: _label(csv), _ppmm(0)
{ untested();
	std::string avifilename="";
	std::string dir="";
	unsigned framenumber=-1u;
	parse_csv(csv, _ppmm, _annotation, avifilename, dir, framenumber);

	std::string bgname=bgimgname(avifilename);

	// BUG BUG BUG duplicate. see previous constructor.
	//
	if(framenumber){ itested();
		// data hack. zero and one are both first frame
		--framenumber;
	}else{ itested();
	}
	message(bTRACE, "getting frame %d from %s/%s\n",
			framenumber, dir.c_str(), avifilename.c_str());

	auto filename = dir+"/"+avifilename;
	getframefromfile(filename, _bgrframe, framenumber);

	if(background.rows==_bgrframe.rows){
		_bgrframe = background - _bgrframe;
	}else{
	}
	//display_grayscale_frame("tmp", _bgrframe);

	adjust_brightness_max(_bgrframe);

#ifdef DEBUG
	untested();
	cv::Mat silfrm(_bgrframe.clone());
	silhouetteImage(silfrm, silfrm, 0);
	std::cout << "debug annotated nonzero " << countNonZero(silfrm) << "\n";
#endif

}

std::pair<cv::Point, cv::Point> annotated_frame::bounding_box() const
{ untested();
	int lbx=99999; // intmax...
	int lby=99999; // intmax...
	int ubx=0;
	int uby=0;

	for(auto i: _annotation){
//		trace3("ann", i.first.y, i.first.x, i.second);
//		_bgrframe.at<uchar>(get_point(i).y, get_point(i).x) = 255*get_weight(i);
		lbx=std::min(lbx, get_point(i).x);
		lby=std::min(lby, get_point(i).y);
		ubx=std::max(ubx, get_point(i).x);
		uby=std::max(uby, get_point(i).y);
	}

	lbx=std::max(lbx-15, 0);
	lby=std::max(lby-15, 0);
	ubx=std::min(ubx+15, 2047);
	uby=std::min(uby+15, 2047);

	return std::make_pair( cv::Point(lbx, lby), cv::Point(ubx, uby));
}

//		silhouetteImageDilate(worm, tgtsil, dilate_tgt_sil);

std::pair<cv::Point, cv::Point> annotated_frame::show_me(cv::Mat& M) const
{ untested();
	M = _bgrframe; // does it do what it looks like?

	int lbx=99999; // intmax...
	int lby=99999; // intmax...
	int ubx=0;
	int uby=0;

	for(auto i: _annotation){
//		trace3("ann", i.first.y, i.first.x, i.second);
		M.at<uchar>(get_point(i).y, get_point(i).x) = 255*get_weight(i);
		lbx=std::min(lbx, get_point(i).x);
		lby=std::min(lby, get_point(i).y);
		ubx=std::max(ubx, get_point(i).x);
		uby=std::max(uby, get_point(i).y);
	}

	lbx=std::max(lbx-15, 0);
	lby=std::max(lby-15, 0);
	ubx=std::min(ubx+15, 2047);
	uby=std::min(uby+15, 2047);

	trace4("show bb", lbx, lby, ubx, uby);
	return std::make_pair( cv::Point(lbx, lby), cv::Point(ubx, uby));
}

void ANNOTATION::postprocess(unsigned howoften, unsigned thinning_type)
{
	if(!howoften || !thinning_type){
		return;
	}else{
	}

	// hack. should use maximum annotation coords
	int rows=2048;
	int cols=2048;

	cv::Mat ann=cv::Mat::zeros( rows, cols, CV_8U );

	for(auto i: *this){
//		trace3("ann", i.first.y, i.first.x, i.second);
		ann.at<uchar>(get_point(i).y, get_point(i).x) = 255*get_weight(i);
	}

	unsigned erosion_type=0;
	unsigned erosion_size=4;
	cv::Mat eelement = getStructuringElement( erosion_type,
				cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
				cv::Point( erosion_size, erosion_size ) );
	unsigned dilation_type=0;
	unsigned dilation_size=erosion_size;
	cv::Mat delement = getStructuringElement( dilation_type, cv::Size( 2*dilation_size
					+ 1, 2*dilation_size+1 ), cv::Point( dilation_size, dilation_size )
				);
	for(; howoften; --howoften){
		cv::Mat N;
#if 1
		cv::dilate(ann, N, delement );
		cv::erode(N, ann, eelement );
#else
		cv::erode(ann, N, eelement );
		cv::dilate(N, ann, delement );
#endif
	}

	if(thinning_type){ untested();
		cv::Mat tmp=cv::Mat::zeros( ann.rows, ann.cols, ann.type() );
		//display_grayscale_frame("tmp", ann);
		cv_pending::ximgproc::thinning(ann, tmp, thinning_type-1);
		//display_grayscale_frame("tmp", tmp);
		ann = tmp;
	}else{
	}

	clear();
	//_annotation[coord_type(1000, 400)] = 1.;
	//_annotation[coord_type(100, 100)] = 1.;

	for(int row=0; row<rows; ++row ){ itested();
		for(int col=0; col<cols; ++col ){ itested();
			if(ann.at<uchar>(row, col)){
				push_back(coord_type(col, row));
			}
		}
	}
}

std::pair<cv::Point, cv::Point> annotated_frame::rawimg(cv::Mat& M) const
{
	cv::cvtColor( _bgrframe, M, CV_GRAY2BGR );
	cv::RNG rng(12345);
	cv::Vec3b color( rng.uniform(255, 255), rng.uniform(0,0), rng.uniform(0,0) );

	int lbx=99999; // intmax...
	int lby=99999; // intmax...
	int ubx=0;
	int uby=0;

	for(auto i: _annotation){
		lbx=std::min(lbx, get_point(i).x);
		lby=std::min(lby, get_point(i).y);
		ubx=std::max(ubx, get_point(i).x);
		uby=std::max(uby, get_point(i).y);
	}

	lbx=std::max(lbx-15, 0);
	lby=std::max(lby-15, 0);
	ubx=std::min(ubx+15, 2047);
	uby=std::min(uby+15, 2047);

	trace4("show bb", lbx, lby, ubx, uby);
	return std::make_pair( cv::Point(lbx, lby), cv::Point(ubx, uby));
}

std::pair<cv::Point, cv::Point> annotated_frame::show_me_colour(cv::Mat& M) const
{
	cv::cvtColor( _bgrframe, M, CV_GRAY2BGR );
	cv::RNG rng(12345);
	cv::Vec3b color( rng.uniform(255, 255), rng.uniform(0,0), rng.uniform(0,0) );
	cv::Vec3b headcolor( rng.uniform(0, 0), rng.uniform(0,0), rng.uniform(255,255) );

	int lbx=99999; // intmax...
	int lby=99999; // intmax...
	int ubx=0;
	int uby=0;

	for(auto i: _annotation){
//		trace3("ann", get_point(i).y, get_point(i).x, i.second);
		M.at<cv::Vec3b>(get_point(i).y, get_point(i).x) = color;
		lbx=std::min(lbx, get_point(i).x);
		lby=std::min(lby, get_point(i).y);
		ubx=std::max(ubx, get_point(i).x);
		uby=std::max(uby, get_point(i).y);
	}
	if(_annotation.is_ordered()){
		auto i = _annotation.front();
		M.at<cv::Vec3b>(get_point(i).y, get_point(i).x) = headcolor;
	}else{ untested();
	}

	lbx=std::max(lbx-15, 0);
	lby=std::max(lby-15, 0);
	ubx=std::min(ubx+15, 2047);
	uby=std::min(uby+15, 2047);

	trace4("show bb", lbx, lby, ubx, uby);
	return std::make_pair( cv::Point(lbx, lby), cv::Point(ubx, uby));
}

} // framestuff

using framestuff::ANNOTATION;

class framesindex{
public:
	typedef std::list<std::string> container_type;
	typedef container_type::const_iterator const_iterator;
	enum SINGLE{ _SINGLE };
public:
	framesindex(){untested();
	}
	framesindex(const std::string& s, SINGLE) : _dir(".") { untested();
		trace1("pushback", s);
		_c.push_back(s);
	}
	framesindex(const std::string& indexfilename){ untested();
		size_t last=indexfilename.find_last_of("/");
		_dir=indexfilename.substr(0, last);
		std::string line;

		message(bDEBUG, "opening index %s\n", indexfilename.c_str());
		std::ifstream fs(indexfilename);
		if(!fs.is_open()){ untested();
			throw exception_cantfind(indexfilename + "\n");
		}else{ untested();
		}
		std::getline(fs, line);

		assert(line!="");

//		unsigned nofile=0;
//		unsigned corrupt=0;

		while(!fs.eof()){ itested();
			if(line[0]=='#'){ untested();
				continue;
			}else{ itested();
				_c.push_back(line);
			}
			std::getline(fs, line);
		}
	}
public: // const access
	const_iterator begin() const{return _c.begin();}
	const_iterator end() const{return _c.end();}
	std::string const& dir() const {return _dir;}
private:
	std::list<std::string> _c;
	std::string _dir;
};

template<template<class X, class ...> class container=std::deque>
class AFD{
public: // types
	typedef container<framestuff::annotated_frame> annotated_frame_deque_type;
	typedef typename annotated_frame_deque_type::const_iterator const_iterator;
public: // construct
	AFD(const std::string& indexfilename){ untested();

		framesindex I(indexfilename);

		unsigned nofile=0;
		unsigned corrupt=0;

		typedef framestuff::annotated_frame annotated_frame;
		for( auto const& line : I){
			try{ untested();
				framestuff::annotated_frame a(I.dir() + "/" + line, annotated_frame::_CSV);

				_afd.push_back(a);
				assert(a.annotation().size());
			}catch(exception_nosuchfile const& e){ untested();
				std::cerr << "not there\n";
				std::cerr << e.what() << "\n";
				++nofile;
			}catch(exception_corrupt const& e){ untested();
				std::cerr << "skipping line\n";
				std::cerr << e.what() << "\n";
				++corrupt;
			}
		}
	}
	const_iterator begin() const{return _afd.begin(); }
	const_iterator end() const{return _afd.end(); }
private:
	annotated_frame_deque_type _afd;
}; // AFD


AFD<std::deque> make_annotated_frames_deque(std::string& indexfilename)
{ untested();
	return AFD<std::deque>(indexfilename);
}

using framestuff::annotated_frame;

#endif
