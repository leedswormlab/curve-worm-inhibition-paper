#ifndef SILHOUETTE_HH
#define SILHOUETTE_HH

#include "opencv2/core.hpp"
#include <string>
#include "../error.hh"
#include "io.hh"
#include "display.hh" // BUG

#include <stdlib.h>

#include "../projectionoperator.hh"

#define SILassert(x)

#define SHOW_SILHOUETTE_IMAGES 0

void silhouetteImage( const cv::Mat& input, cv::Mat& output, double chunksize);

void silhouetteImage( const cv::Mat& input,
      const cv::Mat& background, cv::Mat& output, const double minValue )
{ untested();
  // subtract background image
  output = background - input;
  silhouetteImage(input, output, minValue);

}

inline void dilate_mask(cv::Mat& io, unsigned radius)
{
   cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
	 cv::Size(radius*2+1, radius*2+1), cv::Point(radius, radius) );

   cv::Mat dilated;
   cv::dilate(io, dilated, element);
   io=dilated;
}

void silhouetteImageDilate( const cv::Mat& input, cv::Mat& output, unsigned radius,
      double min_chunksize=200.)
{
   message(bLOG, "silImgDil radius %d, min_chunksize %f\n", radius, min_chunksize);
   silhouetteImage(input, output, min_chunksize);
   if(radius==0){
      return;
   }else{
   }

   dilate_mask(output, radius);

}


void silhouetteImageContours( cv::Mat const& input,
  std::vector<std::vector<cv::Point> >& contours,
  double contour_minsize=200.)
{ itested();
  // blur
  cv::Mat blur;
  cv::GaussianBlur( input, blur, cv::Size( 11, 11 ), 0, 0 );

  // computes global statistics
  cv::Scalar meanBlur, stdDevBlur;
  cv::meanStdDev(blur, meanBlur, stdDevBlur );

  message(bTRACE, "blur %f %f\n", meanBlur[0], stdDevBlur[0]);

  std::vector<cv::Vec4i> hierarchy;

  const double nStdDev = 3.; // was 6.
  cv::threshold( blur, blur, (meanBlur[0] + nStdDev * stdDevBlur[0]), 255, CV_THRESH_BINARY );
  // display_grayscale_frame("blur", blur);

  /// Find contours
#if 1 // which one?
  findContours( blur, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );
#else
  findContours( blur, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE );
#endif
  if( contours.size() == 0 ){ untested();
     message(bDANGER, "can't find contours\n");
//   BUG     should throw...
//     display_grayscale_frame("zerosilhouette", output);
//     display_grayscale_frame("zerosilhouette", input);
//     display_grayscale_frame("zerosilhouette", blur);
  }else{ itested();
  }

  unsigned numall=contours.size();
  // sort by size
  std::sort( contours.begin(), contours.end(),
	     []( const std::vector<cv::Point>& a, const std::vector<cv::Point>& b ) -> bool
	     {
	       return cv::contourArea( a ) > cv::contourArea( b );
	     }
	     );


  // remove all smaller than threshold
  while( contours.size() && cv::contourArea( contours.back() ) < contour_minsize ){ itested();
    contours.pop_back();
  }
  message(bLOG, "%d/%d contours left\n", contours.size(), numall);
}

void silhouetteImage( const cv::Mat& input, cv::Mat& output,
  double contour_minsize=200.)
{ itested();

   std::vector<std::vector<cv::Point> > contours;

   silhouetteImageContours(input, contours, contour_minsize);

   output = cv::Mat::zeros(output.size(), output.type());
#if 1
   cv::drawContours( output, contours, -1, 255, CV_FILLED ); // (, 8, hierarchy, 0, cv::Point() );
#else
  cv::drawContours( output, contours, -1, 255, CV_FILLED, 8, hierarchy, 0, cv::Point() );
#endif
  // display_grayscale_frame("blur", output);
}

// trying to extract a silhouette from a single frame.
// "output" is the input.
//
// **** this seems to be a copy of SilhouetteImage. ****
//
template<class V>
void single_silhouette(V& contours, cv::Mat const& output)
{
	cv::Mat blur;
	cv::GaussianBlur( output, blur, cv::Size( 11, 11 ), 0, 0 );

	cv::Scalar meanBlur, stdDevBlur;
	cv::meanStdDev(blur, meanBlur, stdDevBlur );

	std::vector<cv::Vec4i> hierarchy;

	const double nStdDev = 6.0;
	cv::threshold( blur, blur, (meanBlur[0] + nStdDev * stdDevBlur[0]), 255, CV_THRESH_BINARY );

	/// Find contours
	findContours( blur, contours, hierarchy, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE );

	// sort by size
	std::sort( contours.begin(), contours.end(),
			[]( const std::vector<cv::Point>& a, const std::vector<cv::Point>& b ) -> bool
			{
			return cv::contourArea( a ) > cv::contourArea( b );
			}
			);

	// remove all smaller than 100.0
	while( cv::contourArea( contours.back() ) < 100.0 )
		contours.pop_back();

} // singlesilhouette

#if 0
void show_silhouette(cv::RotatedRect const& ellipse, cv::Mat const& tmp,
	std::vector<cv::Point> const&	contour,
	cv::Point2f middle, std::string windowname,
	cv::Point mymiddle)
{
	const auto rect = ellipse.boundingRect();


	cv::drawContours( tmp, std::vector<std::vector<cv::Point> >(1,  contour), 0, cv::Scalar(0,0,255) );

	// rotated rectangle
	cv::Point2f rect_points[4];
	ellipse.points( rect_points );
	for( int j = 0; j < 4; j++ )
		cv::line( tmp, rect_points[j], rect_points[(j+1)%4], cv::Scalar(0,255,0), 1, 8 );

	// cv::ellipse( tmp, ellipses[cam], cv::Scalar(0,255,0 ) );
	cv::circle( tmp, middle, 2, cv::Scalar(0,255,0) );

	cv::circle( tmp, mymiddle, 2, cv::Scalar(255,0,0) );

	cv::imshow( windowname, tmp( rect ) );

} // show_silhouette
#endif
// partly duplicates silhouetteImage
// does fancy 3d stuff.
// TODO: produce contour triplets, not silhouettes
template< std::size_t nCam, unsigned dim>
      // silhouetteImages( p, frames, backgrounds, srcs, 10 );

void silhouetteImages( const std::vector< CameraProjectionOperator< dim, 2 > >& proj,
		       const std::array<cv::Mat,nCam>& allInput,
		       std::array<cv::Mat,nCam>& allOutput, const double /*minValue*/,
                       RangeVector<dim> const* middle3d=NULL)
{
  std::vector<std::vector<cv::Point> > allContours[nCam];
  for( unsigned int cam = 0; cam < nCam; ++cam ) {
      auto& input = allInput.at(cam);
      auto& silh = allOutput.at(cam);

      std::vector<std::vector<cv::Point> > contours;

      // display_grayscale_frame("contours?", output);
      single_silhouette(contours, input);
      allContours[cam] = contours;

      if(nCam==1){
          silh = cv::Mat::zeros(silh.size(), silh.type());

          cv::drawContours( silh, contours, -1, 255, CV_FILLED );
          assert(!middle3d); // bug: why 3?
          return;
      }else{
      }
  }


#if NCAMS == 3
  unsigned s0=allContours[0].size();
  unsigned s1=allContours[1].size();
  unsigned s2=allContours[2].size();
  message(bDEBUG, "matching up %d, %d, %d contours\n", s0, s1, s2);

  double contourtolerance=50.;
  if(s0==1 && s1==1 && s2==1){
      contourtolerance=60.;
  }else{ untested();
  }

  std::vector< std::array< std::vector<cv::Point>, nCam > > matchingContours;
  for( unsigned int i = 0; i < s0; ++i ) {
    for( unsigned int j = 0; j < s1; ++j ) {
      for( unsigned int k = 0; k < s2; ++k ) {
	  // find contours
	  const auto& contourCam0 = allContours[0].at(i);
	  const auto& contourCam1 = allContours[1].at(j);
	  const auto& contourCam2 = allContours[2].at(k);

	  // fit min area rect
	  const auto& boundingRectCam0 = cv::minAreaRect( contourCam0 );
	  const auto& boundingRectCam1 = cv::minAreaRect( contourCam1 );
	  const auto& boundingRectCam2 = cv::minAreaRect( contourCam2 );

	  // find center point
	  std::array< cv::Point2f, nCam > middles
	    = { { boundingRectCam0.center, boundingRectCam1.center, boundingRectCam2.center } };

	  // find matching 3d point
	  // triangulate pairwise and take mean
	  cv::Point3d middle3d(0.,0.,0.);
	  int n2Cams = 0;
	  for( unsigned i = 0; i < nCam; ++i ){
	    for( unsigned j = 0; j < i; ++j ) {
		cv::Mat cam0pnts( 1, 1, CV_64FC2 );
		cam0pnts.at< cv::Vec2d >( 0 )[ 0 ] = static_cast<int>(middles[i].x);
		cam0pnts.at< cv::Vec2d >( 0 )[ 1 ] = static_cast<int>(middles[i].y);
		cv::Mat cam1pnts( 1, 1, CV_64FC2 );
		cam1pnts.at< cv::Vec2d >( 0 )[ 0 ] = static_cast<int>(middles[j].x);
		cam1pnts.at< cv::Vec2d >( 0 )[ 1 ] = static_cast<int>(middles[j].y);

		const auto P1 = proj.at(i).P;
		const auto P2 = proj.at(j).P;

		cv::Mat pts3dHom;
		cv::triangulatePoints( P1, P2, cam0pnts, cam1pnts, pts3dHom );

		cv::Point3d out;
		out.x = pts3dHom.at<double>( 0 ) / pts3dHom.at<double>( 3 );
		out.y = pts3dHom.at<double>( 1 ) / pts3dHom.at<double>( 3 );
		out.z = pts3dHom.at<double>( 2 ) / pts3dHom.at<double>( 3 );

		middle3d += out;
		++n2Cams;
            }
          }
	  middle3d.x /= static_cast<double>(n2Cams);
	  middle3d.y /= static_cast<double>(n2Cams);
	  middle3d.z /= static_cast<double>(n2Cams);

	  // re-project and compute errors
	  double error = 0.0;
	  for( unsigned int cam = 0; cam < nCam; ++cam ) {
	      const auto pMiddle = proj.at(cam)( middle3d );
	      error += ( pMiddle[0] - middles.at(cam).x ) * ( pMiddle[0] - middles.at(cam).x );
	      error += ( pMiddle[1] - middles.at(cam).y ) * ( pMiddle[1] - middles.at(cam).y );
          }
	  error = std::sqrt( error );

          message(bDEBUG, "contour %d, %d, %d mismatch %f\n", i, j, k, error);
	  if( error < contourtolerance ){
	    matchingContours.push_back( {{ contourCam0, contourCam1, contourCam2 }} );
          }else{
          }
      }
    }
  }

  //display_contours("raw", allContours[0], allInput[0]);
  //display_contours("raw", allContours[1], allInput[1]);
  //display_contours("raw", allContours[2], allInput[2]);

  for( unsigned int cam = 0; cam < nCam; ++cam ) {
     auto const& input = allInput.at(cam);
     allOutput[cam] = cv::Mat::zeros(input.size(), input.type());
  }

  if( matchingContours.empty() ) {
     // this is REALLY strange.
      message(bWARNING, "no matching contours out of %d %d %d\n",
	allContours[0].size(),
	allContours[1].size(),
	allContours[2].size());
      message(bWARNING, "camera offsets %E %E %E %E %E %E\n",
          proj[0].ooffset()[0],
          proj[0].ooffset()[1],
          proj[1].ooffset()[0],
          proj[1].ooffset()[1],
          proj[2].ooffset()[0],
          proj[2].ooffset()[1]
          );

#if 0
      display_grayscale_frame("1", allInput[0]);
      display_grayscale_frame("2", allInput[1]);
      display_grayscale_frame("3", allInput[2]);
#endif

      throw exception_corrupt("no matching contours");
  } else if( middle3d ) {
      message(bDEBUG, "have persistent. more picky...\n");
      // find array of middle 2d points
      std::array< cv::Point2d, nCam > previousMiddles2d;
      // find 3d middle of previous solution
      for( unsigned c=0; c < nCam; ++c ) {
	previousMiddles2d[c] = proj[c]( *middle3d );
        message(bDEBUG, "previous middle %d (%f, %f)\n", c, previousMiddles2d[c].x,
            previousMiddles2d[c].y);
      }

      // define score of closeness to previous 2d middles
      const auto score = [&]( const std::array< std::vector<cv::Point>, nCam >& a ) -> double {
	double ret = 0.0;
	for( unsigned int cam = 0; cam < nCam; ++cam )
	  {
	    // fit ellipse
	    const auto& ellipseA = cv::fitEllipse( a[cam] );

	    // middle of ellipse
	    const auto& middle2dA =
	      static_cast<cv::Point2d>(ellipseA.center);

	    // add to total
	    ret += cv::norm( middle2dA - previousMiddles2d.at(cam) );
	  }

	return ret;
      };

      // sort matching contours by score
      std::sort( matchingContours.begin(), matchingContours.end(),
		 [&]( const std::array< std::vector<cv::Point>, nCam >& a,
                      const std::array< std::vector<cv::Point>, nCam >& b ) -> bool {
		   return score(a) < score(b);
		 } );
  }

  // put into output
  for( unsigned int cam = 0; cam < nCam; ++cam ) {
      auto& output = allOutput.at(cam);
      const auto& contour = matchingContours.at(0).at(cam);
      cv::drawContours( output, std::vector<std::vector<cv::Point> >(1,contour), 0, 255, CV_FILLED );
      //display_grayscale_frame("raw", output);
  }
#else
  {
    throw "Silhouette not implemented for != 1,3 cams";
  }
#endif
 }

#endif // #ifndef SILHOUETTE_HH

#if 0 // OLD ELLLIPSE CODE. remove?
  // sort by sum of aspect ratios
  std::sort( matchingContours.begin(), matchingContours.end(),
	     []( const std::array< std::vector<cv::Point>, nCam >& a, const std::array< std::vector<cv::Point>, nCam >& b ) -> bool
	    {
	      double scoreA = 0, scoreB = 0;
	      for( unsigned int cam = 0; cam < nCam; ++cam )
		{
		  // fit ellipse
		  const auto& ellipseA = cv::fitEllipse( a.at(cam) );
		  const auto& ellipseB = cv::fitEllipse( b.at(cam) );

		  // compute aspect ratio
		  const double aspA = std::max( ellipseA.size.width / ellipseA.size.height, ellipseA.size.height / ellipseA.size.width );
		  const double aspB = std::max( ellipseB.size.width / ellipseB.size.height, ellipseB.size.height / ellipseB.size.width );

		  // add to total
		  scoreA += aspA;
		  scoreB += aspB;
		}

	      return scoreA > scoreB;
	    }
	     );
#endif

//  vim: set cinoptions=>4,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1
//  vim: set autoindent expandtab shiftwidth=2 softtabstop=2 tabstop=8:
