
// here?
//
#pragma once

template<>
struct DistanceFunctionImage<2> : //public DistanceFunction<2>
 public VectorDistanceFunction /// <2>??
{

// BUG BUG BUG
// this must be template arg.
  typedef draft::MidlineFinderA<KnownList> midfind_type;

  using FarList = std::vector< cv::Point >;
  using TrialList = std::map< cv::Point, double, CvPointCompare >;

  using RangeVectorType = typename DistanceFunction<2> :: RangeVectorType;
  static const unsigned int dim = 2;

  typedef known_points_finder<KnownList> kpf_type;
  typedef draft::MidlineFinderA<KnownList> kpf_default_type;

  DistanceFunctionImage( const cv::Mat& src,  cv::Mat& raw,
			 const RangeVectorType& head, const RangeVectorType& tail,
			 const bool useHT = true,
			 const kpf_type& kpf=kpf_default_type())
    : src_( src ), raw_( raw ), head_( head ), tail_( tail ), useHT_( useHT ),
      kpf_(kpf)
  {
    if (!src_.data)
      throw "no data for distance function image";

#if DISPLAY_KNOWN
    cv::namedWindow( "tmp", CV_WINDOW_NORMAL );
    cv::resizeWindow( "tmp", 1024, 1024 );
#endif

    cv::Mat raw2;
    cv::cvtColor( raw, raw2, CV_BGR2GRAY );
    kpf_.find_known_points(src_, raw2, known, 0 /* doesnt matter */ );

    // initialize();

    std::cout << "found " << known.size() << " points in skeleton" << std::endl;
    // erase weights for worm-skeleton.
    // (no weights=previous implementation)
    for( auto& pair : known ){
       pair.second=0.;
    }
    set_points(known.begin(), known.end());

#if DISPLAY_KNOWN
    cv::Mat tmp = raw.clone();

    int minx = src_.cols, miny = src_.rows, maxx = 0, maxy = 0;
    for( const auto& pair : known )
      {
	const auto pt = pair.first;
	tmp.at< cv::Vec3b >( pt )[0] = (uchar)(0);
	tmp.at< cv::Vec3b >( pt )[1] = (uchar)(0);
	tmp.at< cv::Vec3b >( pt )[2] = (uchar)(255);

	minx = std::min( minx, pt.x - 15 );
	miny = std::min( miny, pt.y - 15 );
	maxx = std::max( maxx, pt.x + 15 );
	maxy = std::max( maxy, pt.y + 15 );
      }

    for( const auto& pt : erased )
      {
	tmp.at< cv::Vec3b >( pt )[0] = (uchar)(0);
	tmp.at< cv::Vec3b >( pt )[1] = (uchar)(255);
	tmp.at< cv::Vec3b >( pt )[2] = (uchar)(255);
      }

    {
      const cv::Point pt( middle_[0], middle_[1] );
      tmp.at< cv::Vec3b >( pt )[0] = (uchar)(0);
      tmp.at< cv::Vec3b >( pt )[1] = (uchar)(255);
      tmp.at< cv::Vec3b >( pt )[2] = (uchar)(0);

      tmp.at< cv::Vec3b >( end0 )[0] = (uchar)(0);
      tmp.at< cv::Vec3b >( end0 )[1] = (uchar)(255);
      tmp.at< cv::Vec3b >( end0 )[2] = (uchar)(0);

      tmp.at< cv::Vec3b >( end1 )[0] = (uchar)(0);
      tmp.at< cv::Vec3b >( end1 )[1] = (uchar)(255);
      tmp.at< cv::Vec3b >( end1 )[2] = (uchar)(0);

      minx = std::min( minx, pt.x - 15 );
      miny = std::min( miny, pt.y - 15 );
      maxx = std::max( maxx, pt.x + 15 );
      maxy = std::max( maxy, pt.y + 15 );
    }

    cv::Mat edges;
    findSrcEdges( edges );
    KnownList myKnown;
    convertWhitePointsToValueList( edges, myKnown, 0.0 );
    for( const auto& pair : myKnown )
      {
	const auto pt = pair.first;
	tmp.at< cv::Vec3b >( pt )[0] = (uchar)(255);
	tmp.at< cv::Vec3b >( pt )[1] = (uchar)(255);
	tmp.at< cv::Vec3b >( pt )[2] = (uchar)(255);
      }

    cv::imshow( "tmp", tmp.colRange( minx, maxx ).rowRange( miny, maxy ) );
    // cv::waitKey(0);
#endif
  }

  DistanceFunctionImage( const cv::Mat& src, const cv::Mat& raw,
			 const kpf_type& kpf=kpf_default_type())
    : src_( src ), raw_( raw ), head_( -1.0 ), tail_( -1.0 ), useHT_( false ),
      kpf_(kpf)
  {

    incomplete(); // need to pass down points?
                  // (anyway this is not used...)
    if (!src_.data)
      throw "no data for distance function image";

#if DISPLAY_KNOWN
    cv::namedWindow( "tmp", CV_WINDOW_NORMAL );
    cv::resizeWindow( "tmp", 1024, 1024 );
#endif

    initialize();

    std::cout << "found " << known.size() << " points in skeleton" << std::endl;

#if DISPLAY_KNOWN
    cv::Mat tmp = raw.clone();

    int minx = src_.cols, miny = src_.rows, maxx = 0, maxy = 0;
    for( const auto& pair : known )
      {
	const auto pt = pair.first;
	tmp.at< cv::Vec3b >( pt )[0] = (uchar)(0);
	tmp.at< cv::Vec3b >( pt )[1] = (uchar)(0);
	tmp.at< cv::Vec3b >( pt )[2] = (uchar)(255);

	minx = std::min( minx, pt.x - 15 );
	miny = std::min( miny, pt.y - 15 );
	maxx = std::max( maxx, pt.x + 15 );
	maxy = std::max( maxy, pt.y + 15 );
      }

    for( const auto& pt : erased )
      {
	tmp.at< cv::Vec3b >( pt )[0] = (uchar)(0);
	tmp.at< cv::Vec3b >( pt )[1] = (uchar)(255);
	tmp.at< cv::Vec3b >( pt )[2] = (uchar)(255);
      }

    {
      const cv::Point pt( middle_[0], middle_[1] );
      tmp.at< cv::Vec3b >( pt )[0] = (uchar)(0);
      tmp.at< cv::Vec3b >( pt )[1] = (uchar)(255);
      tmp.at< cv::Vec3b >( pt )[2] = (uchar)(0);

      tmp.at< cv::Vec3b >( end0 )[0] = (uchar)(0);
      tmp.at< cv::Vec3b >( end0 )[1] = (uchar)(255);
      tmp.at< cv::Vec3b >( end0 )[2] = (uchar)(0);

      tmp.at< cv::Vec3b >( end1 )[0] = (uchar)(0);
      tmp.at< cv::Vec3b >( end1 )[1] = (uchar)(255);
      tmp.at< cv::Vec3b >( end1 )[2] = (uchar)(0);

      minx = std::min( minx, pt.x - 15 );
      miny = std::min( miny, pt.y - 15 );
      maxx = std::max( maxx, pt.x + 15 );
      maxy = std::max( maxy, pt.y + 15 );
    }

    cv::imshow( "tmp", tmp.colRange( minx, maxx ).rowRange( miny, maxy ) );
    // cv::waitKey(0);
#endif
  }

  void printKnown( std::ostream& s ) const
  {
    s << "x,y,val" << std::endl;

    for( auto pair : known )
      {
	s << pair.first.x << ","
	  << pair.first.y << ","
	  << pair.second << std::endl;
      }
  }

protected:
  void initialize()
  {
    KnownList boundaryKnown;
    TrialList boundaryTrial;

    initializeBoundary( boundaryKnown, boundaryTrial );
    initializeKnown( boundaryKnown, boundaryTrial );
  }

  void initializeBoundary( KnownList& boundaryKnown, TrialList& boundaryTrial ) const
  {
    cv::Mat edges = cv::Mat::zeros( src_.rows, src_.cols, CV_8U );
    findSrcEdges( edges, src_);
    convertWhitePointsToValueList( edges, boundaryKnown, 0.0 );
    initializeTrialPoints( boundaryKnown, boundaryTrial );
  }

  void initializeKnown( KnownList& boundaryKnown, TrialList& boundaryTrial )
  {
    // create wanted list
    FarList wanted;
    const uchar white = static_cast<uchar>( 255 );
    for( int i = 0; i < src_.rows; ++i )
      {
	const uchar* p = src_.ptr<uchar>(i);
	for( int j = 0; j < src_.cols; ++j )
	  {
	    if( p[j] == white )
	      {
		const cv::Point pt( j, i );

		if( boundaryKnown.find( pt ) == boundaryKnown.end() and
		    boundaryTrial.find( pt ) == boundaryTrial.end() )
		  wanted.push_back( pt );
	      }
	  }
      }

    // add all wanted to boundary known
    while( wanted.size() > 0 )
      {
	// find minimum trial point
	const auto minPairIt = std::min_element( boundaryTrial.begin(), boundaryTrial.end(),
						 []( const std::pair< cv::Point, double >& a,
						     const std::pair< cv::Point, double >& b ) -> bool
						 {
						   return a.second < b.second;
						 }
						 );

	// check minimum is in list
	if( minPairIt == boundaryTrial.end() )
	  {
	    std::cout << "trial empty but still want points" << std::endl;
	    std::cout << "ignoring " << wanted.size() << " points outside Canny edge" << std::endl;
	    break;
	  }

	// cache value
	const auto minPair = *minPairIt;
	const cv::Point& minPt = minPair.first;

	// add to known and remove from trial and wanted
	boundaryKnown.insert( minPair );
	boundaryTrial.erase( minPairIt );
	auto wantedIt = std::find( wanted.begin(), wanted.end(), minPt );
	if( wantedIt != wanted.end() )
	  wanted.erase( wantedIt );

	// if minPair is meeting of fronts add to known
	{
	  const auto inBoundaryKnown = [&]( const cv::Point& p ) -> bool
	    {
	      return boundaryKnown.find( p ) != boundaryKnown.end();
	    };

	  const cv::Point N( minPt.x+1, minPt.y );
	  const cv::Point S( minPt.x-1, minPt.y );
	  const cv::Point E( minPt.x, minPt.y+1 );
	  const cv::Point W( minPt.x, minPt.y-1 );

	  if( ( inBoundaryKnown( N ) and inBoundaryKnown( S ) ) or
	      ( inBoundaryKnown( E ) and inBoundaryKnown( W ) ) )
	    {
	      known.insert( { minPt, 0 } );
	    }
	}

	// update neighbours
	updateNeighbours( minPt, boundaryKnown, boundaryTrial );

#if DISPLAY_KNOWN
	{
	  static int mycount = 0;
	  cv::Mat tmp = raw_.clone();

	  int minx = src_.cols, miny = src_.rows, maxx = 0, maxy = 0;
	  for( const auto& pair : boundaryKnown )
	    {
	      const cv::Point& pt = pair.first;
	      const double val = pair.second;

	      tmp.at< cv::Vec3b >( pt )[0] = (uchar)( val * 20.0 );
	      tmp.at< cv::Vec3b >( pt )[1] = (uchar)(0);
	      tmp.at< cv::Vec3b >( pt )[2] = (uchar)(0);

	      minx = std::min( minx, pt.x - 15 );
	      miny = std::min( miny, pt.y - 15 );
	      maxx = std::max( maxx, pt.x + 15 );
	      maxy = std::max( maxy, pt.y + 15 );
	    }

	  minx = std::max( minx, 0 );
	  miny = std::max( miny, 0 );
	  maxx = std::min( maxx, src_.cols );
	  maxy = std::min( maxy, src_.rows );

	  for( const auto& pair : known )
	    {
	      const auto pt = pair.first;
	      tmp.at< cv::Vec3b >( pt )[0] = (uchar)(0);
	      tmp.at< cv::Vec3b >( pt )[1] = (uchar)(0);
	      tmp.at< cv::Vec3b >( pt )[2] = (uchar)(255);
	    }

	  // std::stringstream ss;
	  // ss << "building_known_" << std::setfill('0') << std::setw(6) << mycount << ".png";
	  // cv::imwrite( ss.str(), tmp.colRange( minx, maxx ).rowRange( miny, maxy ) );
	  cv::imshow( "tmp", tmp.colRange( minx, maxx ).rowRange( miny, maxy ) );
	  // cv::waitKey(10);

	  mycount++;
	}
#endif
      }
    // cv::waitKey(0);

    // trim
    trim();

    // initialize trial points
    initializeTrialPoints( known, trial );
  }

  void trim()
  {
    const auto inSrc = [&]( const cv::Point& pt ) -> bool
      {
	if( 0 > pt.x or pt.x >= src_.cols or
	    0 > pt.y or pt.y >= src_.rows )
	  return false;
	return true;
      };

    const auto nNeigh = [&]( const cv::Point& p ) -> int
      {
	int count = 0;
	const Neighbours nn( p, inSrc, true );
	for( const auto& n : nn )
	  {
	    if( known.find( n ) != known.end() )
	      ++count;
	  }
	return count;
      };

    bool changed = true;
    for( int d = 0; changed; ++d )
      {
	changed = false;

	for( auto knownIt = known.begin(); knownIt != known.end(); )
	  {
	    const auto p = *knownIt;
	    const int N = nNeigh(p.first);
	    if( N == 0 or N == 1 )
	      {
		known.erase( knownIt++ );
		changed = true;

		if( N == 1 )
		  {
		    erased.push_back( p.first );
		  }
	      }
	    else
	      {
		++knownIt;
	      }
	  }

#if DISPLAY_KNOWN
	{
	  static int mycount = 0;
	  cv::Mat tmp = raw_.clone();

	  int minx = src_.cols, miny = src_.rows, maxx = 0, maxy = 0;

	  for( const auto& pair : known )
	    {
	      const auto pt = pair.first;
	      tmp.at< cv::Vec3b >( pt )[0] = (uchar)(0);
	      tmp.at< cv::Vec3b >( pt )[1] = (uchar)(0);
	      tmp.at< cv::Vec3b >( pt )[2] = (uchar)(255);

	      minx = std::min( minx, pt.x - 15 );
	      miny = std::min( miny, pt.y - 15 );
	      maxx = std::max( maxx, pt.x + 15 );
	      maxy = std::max( maxy, pt.y + 15 );
	    }

	  for( const auto& pair : erased )
	    {
	      const auto pt = pair;
	      tmp.at< cv::Vec3b >( pt )[0] = (uchar)(255);
	      tmp.at< cv::Vec3b >( pt )[1] = (uchar)(0);
	      tmp.at< cv::Vec3b >( pt )[2] = (uchar)(0);

	      minx = std::min( minx, pt.x - 15 );
	      miny = std::min( miny, pt.y - 15 );
	      maxx = std::max( maxx, pt.x + 15 );
	      maxy = std::max( maxy, pt.y + 15 );
	    }

	  // std::stringstream ss;
	  // ss << "building_erased_" << std::setfill('0') << std::setw(6) << mycount << ".png";
	  // cv::imwrite( ss.str(), tmp.colRange( minx, maxx ).rowRange( miny, maxy ) );
	  cv::imshow( "tmp", tmp.colRange( minx, maxx ).rowRange( miny, maxy ) );
	  // cv::waitKey(100);

	  mycount++;
	}
#endif
      }

    // remove any further isolated points
    {
      for( auto erasedIt = erased.begin(); erasedIt != erased.end(); )
	{
	  const cv::Point pt = *erasedIt;
	  int count = 0;
	  for( auto n : Neighbours( pt, inSrc, true ) )
	    {
	      if( std::find( erased.begin(), erased.end(), n ) != erased.end() )
		count++;
	    }

	  if( count == 0 )
	    erased.erase( erasedIt++ );
	  else
	    ++erasedIt;
	}
    }
    // re-add branch closest to head
    if( useHT_ )
      {
	{
	  const auto& closest0 = std::min_element( erased.begin(), erased.end(),
						    [&]( const cv::Point& a,
							 const cv::Point& b ) -> bool
						    {
						      return ( RangeVectorType(a) - head_ ).norm() < ( RangeVectorType(b) - head_ ).norm();
						    }
						    );

	  if( closest0 != erased.end() )
	    {
	      end0 = *closest0;
	      cv::Point closest0Pt = *closest0;

	      known.insert( { closest0Pt, 0.0 } );
	      erased.erase( closest0 );

	      changed = true;
	      while( changed )
		{
		  changed = false;

		  for( auto n : Neighbours( closest0Pt, [&]( const cv::Point& ){ return true; }, true ) )
		    {
		      auto nIt = std::find( erased.begin(), erased.end(), n );
		      if( nIt != erased.end() )
			{
			  closest0Pt = *nIt;
			  known.insert( {closest0Pt, 0} );
			  erased.erase( nIt );
			  changed = true;
			  break;
			}
		    }
		}
	    }
	}
	// re-add branch closest to tail
	{
	  const auto& closest1 = std::min_element( erased.begin(), erased.end(),
						   [&]( const cv::Point& a,
							const cv::Point& b ) -> bool
						   {
						     return ( RangeVectorType(a) - tail_ ).norm() < ( RangeVectorType(b) - tail_ ).norm();
						   }
						   );

	  if( closest1 != erased.end() )
	    {
	      end1 = *closest1;
	      cv::Point closest1Pt = *closest1;
	      known.insert( { closest1Pt, 0.0 } );
	      erased.erase( closest1 );

	      changed = true;
	      while( changed )
		{
		  changed = false;

		  for( auto n : Neighbours( closest1Pt, [&]( const cv::Point& ){ return true; }, true ) )
		    {
		      auto nIt = std::find( erased.begin(), erased.end(), n );
		      if( nIt != erased.end() )
			{
			  closest1Pt = *nIt;
			  known.insert( {closest1Pt, 0} );
			  erased.erase( nIt );
			  changed = true;
			  break;
			}
		    }
		}
	    }
	}
      }
  }


  void convertWhitePointsToValueList( const cv::Mat& edges, KnownList& knownList, const double value ) const
  {
    const uchar white = static_cast<uchar>( 255 );

    for( int i = 0; i < edges.rows; ++i )
      {
	const uchar* p = edges.ptr<uchar>(i);
	for( int j = 0; j < edges.cols; ++j )
	  {
	    if( p[j] == white )
	      {
		knownList.insert( { cv::Point( j, i ), value } );
	      }
	  }
      }
  }

  void initializeTrialPoints( const KnownList& k, TrialList& t ) const
  {
    // loop over known points
    for( auto p : k )
      updateNeighbours( p.first, k, t );
  }

  void updateNeighbours( const cv::Point p, const KnownList& k, TrialList& t ) const
  {
    const auto inSrcWhitePoints = [&]( const cv::Point& pt ) -> bool
      {
	if( 0 > pt.x or pt.x >= src_.cols or
	    0 > pt.y or pt.y >= src_.rows )
	  {
	    return false;
	  }

	const uchar white = static_cast<uchar>(255);
	if( src_.at<uchar>( pt ) == white )
	  return true;
	else
	  return false;
      };

    // loop of neighbours in src white points
    for( auto n : Neighbours( p, inSrcWhitePoints ) )
      {
	// if not in k already
	if( k.find( n ) == k.end() )
	  {
	    // compute new suggested distance
	    const double value = suggestedDistance( n, k );

	    // search for in t
	    auto tit = t.find( n );
	    if( tit != t.end() )
	      {
		// if found update value
		tit->second = std::min( value, tit->second );
	      }
	    else
	      {
		// otherwise
		t.insert( { n, value } );
	      }
	  }
      }
  }

  double suggestedDistance( const cv::Point& pt, const KnownList& k ) const
  {
    const auto inSrc = [&]( const cv::Point& pt ) -> bool
      {
	if( 0 > pt.x or pt.x >= src_.cols or
	    0 > pt.y or pt.y >= src_.rows )
	  return false;
	return true;
      };

    double a = 0, b = 0, c = -1;
    bool foundNeighbours = false;
    for( auto n : Neighbours( pt, inSrc ) )
      {
	// find neighbour in known list
	auto it = k.find( n );
	if( it != k.end() )
	  {
	    const double d = it->second;

	    a += 1.0;
	    b += -2.0 * d;
	    c += d*d;

	    foundNeighbours = true;
	  }
      }

    if( not foundNeighbours )
      {
	throw "no neighbours found 2";
      }

    const double det = ( b*b - 4*a*c );
    if( det < 0 )
      {
	return std::numeric_limits<double>::max();
      }

    const double val = ( - b + std::sqrt(b*b - 4*a*c) ) / ( 2.0 * a );
    return val;
  }

  double median( std::vector<int>& v ) const
  {
    std::sort( v.begin(), v.end() );
    const unsigned int s = v.size();
    if( s % 2 == 0 )
      {
	return 0.5 * ( (double) v.at( s/2 ) + v.at( s/2 - 1 ) );
      }
    else
      {
	return v.at( s/2 );
      }
  }

  struct Neighbours
    : public std::vector< cv::Point >
  {
    Neighbours( const cv::Point& base, const std::function<bool(cv::Point)>& test,
		const bool all = false )
    {
      {
	const cv::Point pt( base.x + 1, base.y );
	if( test( pt ) )
	  this->push_back( pt );
      }
      {
	const cv::Point pt( base.x - 1, base.y );
	if( test( pt ) )
	  this->push_back( pt );
      }
      {
	const cv::Point pt( base.x, base.y + 1 );
	if( test( pt ) )
	  this->push_back( pt );
      }
      {
	const cv::Point pt( base.x, base.y - 1 );
	if( test( pt ) )
	  this->push_back( pt );
      }
      if( all )
	{
	  {
	    const cv::Point pt( base.x + 1, base.y + 1 );
	    if( test( pt ) )
	      this->push_back( pt );
	  }
	  {
	    const cv::Point pt( base.x - 1, base.y - 1 );
	    if( test( pt ) )
	      this->push_back( pt );
	  }
	  {
	    const cv::Point pt( base.x - 1, base.y + 1 );
	    if( test( pt ) )
	      this->push_back( pt );
	  }
	  {
	    const cv::Point pt( base.x + 1, base.y - 1 );
	    if( test( pt ) )
	      this->push_back( pt );
	  }
	}
    }
  };

private:
  const cv::Mat& src_;
  const cv::Mat& raw_;

  const RangeVectorType head_;
  const RangeVectorType tail_;
  const bool useHT_;
  kpf_type const& kpf_;

  cv::Point end0, end1;

  KnownList known;
  TrialList trial;

  std::vector< cv::Point > erased;
};
