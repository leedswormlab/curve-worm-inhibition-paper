#ifndef CALIBRATIONREADER_HH
#define CALIBRATIONREADER_HH

#include "opencv2/opencv.hpp"

#include <array>
#include <cmath>
#include <map>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <utility>

#include "../vector.hh"

struct CameraParametersHolder
{
  CameraParametersHolder()
    : kc( 5 ), cc( 2 ), fc( 2 ), T( 3 ), om( 3 )
  {}

  // distorsion parameters
  Vector kc;

  // intrinsic parameters
  Vector cc;
  Vector fc;

  // extrinsic parameters
  Vector T;
  Vector om;

  cv::Mat cameraMatrix() const
  {
    cv::Mat ret = cv::Mat::zeros( 3, 3, CV_64F );
    ret.at<double>( 0, 0 ) = fc.at(0);
    ret.at<double>( 1, 1 ) = fc.at(1);
    ret.at<double>( 0, 2 ) = cc.at(0);
    ret.at<double>( 1, 2 ) = cc.at(1);
    ret.at<double>( 2, 2 ) = 1.0;
    return ret;
  }
};

struct CameraCalibration
{
  CameraCalibration( const CameraParametersHolder& parameters )
    : params_( parameters )
  {}

  RangeVector<2> project3dTo2d( const RangeVector<3>& input ) const
  {
    cv::Point3d pt;
    pt.x = input.at( 0 );
    pt.y = input.at( 1 );
    pt.z = input.at( 2 );
    return project3dTo2d( pt );
  }

  cv::Point2d project3dTo2d( const cv::Point3d& input ) const
  {
    const std::vector<cv::Point3d> inputArray = { input };
    const auto outputArray = project3dTo2d( inputArray );
    return outputArray.at( 0 );
  }

  std::vector<cv::Point2d> project3dTo2d( const std::vector<cv::Point3d>& input ) const
  {
    // get parameters
    const cv::Mat A = params_.cameraMatrix();
    std::vector<cv::Point2d> output;

    cv::projectPoints( input,
		       params_.om,
		       params_.T,
		       params_.cameraMatrix(),
		       params_.kc,
		       output );

    return output;
  }

  void print() const
  {
    std::cout << "T: " << params_.T.at( 0 ) << " "<< params_.T.at( 1 ) << " " << params_.T.at( 2 ) << std::endl;
    std::cout << "om: " << params_.om.at( 0 ) << " " << params_.om.at( 1 ) << " " << params_.om.at( 2 ) << std::endl;
    std::cout << "cc: " << params_.cc.at( 0 ) << " " << params_.cc.at( 1 ) << std::endl;
    std::cout << "fc: " << params_.fc.at( 0 ) << " " << params_.fc.at( 1 ) << std::endl;
    std::cout << "kc: " << params_.kc.at( 0 ) << " " << params_.kc.at( 1 ) << " "
	      << params_.kc.at( 2 ) << " " << params_.kc.at( 3 ) << " "
	      << params_.kc.at( 4 ) << std::endl;
  }

private:
  CameraParametersHolder params_;
};

class CalibrationReader
  : public std::vector< CameraCalibration >
{
  using Vec = std::vector< double >;
  using Array = std::vector< Vec >;
  using DataType = std::map< std::string, Array >;

public:
  CalibrationReader( const std::string& filename )
  {
    std::ifstream file;
    file.open( filename );

    if( not file.is_open() )
      throw "unable to open file " + filename;

    readCalibrationFile( file );

    file.close();

    // extract camera calibration parameters
    for( unsigned int i = 0; i < nCams(); ++i )
      {
	CameraParametersHolder h;
	fillCameraParameterHolder( i, h );
	CameraCalibration calib( h );
	this->push_back( calib );
	std::cout << "cam " << i << std::endl;
	calib.print();
      }
  }

  cv::Point3d project2dTo3d( const std::vector< RangeVector<2> >& input ) const
  {
    std::vector< cv::Point2d > a;
    for( auto i : input )
      a.push_back( static_cast< cv::Point2d >( i ) );
    return project2dTo3d( a );
  }

  cv::Point3d project2dTo3d( const std::vector<cv::Point2d>& input ) const
  {
    cv::Point3d ret;
    ret.x = 0;
    ret.y = 0;
    ret.z = 0;
    return ret;
  }

  unsigned int nCams() const
  {
    const Array& nCamsA = get( "num_cam" );
    return nCamsA.at( 0 ).at( 0 );
  }

protected:
  void readCalibrationFile( std::ifstream& stream )
  {
    std::string namedLineStarted = "#";

    std::string key;
    int dim0 = 0;
    int dim1 = 0;

    std::string line;
    Array a;
    while( getline( stream, line ) )
      {
	if( not line.compare( 0, 1, namedLineStarted ) )
	  {
	    if( not a.empty() and not key.empty() )
	      {
                auto p = std::make_pair( key, a );
		data_.insert( p );
		a.clear();
	      }

	    std::string hash;
	    std::string colon;

	    std::istringstream ss( line );
	    ss >> hash >> key >> colon >> dim0 >> dim1;
	  }
	else
	  {
	    // read data line
	    std::istringstream ss( line );
	    Vec l;
	    double v;
	    while( ss >> v )
	      {
		l.push_back( v );
	      }

	    // add to array
	    a.push_back( l );
	  }
      }
  }

  void fillCameraParameterHolder( const unsigned int i, CameraParametersHolder& h ) const
  {
    if( i >= nCams() )
      throw "camera number too large!";

    // distortion parameters : kc
    const Array& kcA = get( "kc" );
    for( unsigned int j = 0; j < 5; ++j )
      {
	h.kc.at( j ) = kcA.at( j ).at( i );
      }

    // intrinsic parameters : fc
    const Array& fcA = get( "fc" );
    for( unsigned int j = 0; j < 2; ++j )
      {
	h.fc.at( j ) = fcA.at( j ).at( i );
      }

    // intrinsic parameters : cc
    const Array& ccA = get( "cc" );
    for( unsigned int j = 0; j < 2; ++j )
      {
	h.cc.at( j ) = ccA.at( j ).at( i );
      }

    // extrinsic parameters : T
    const Array& TA = get( "T_cams" );
    for( unsigned int j = 0; j < 3; ++j )
      {
	h.T.at( j ) = TA.at( j ).at( i );
      }

    // extrinsic parameters : om
    const Array& omA = get( "om_cams" );
    for( unsigned int j = 0; j < 3; ++j )
      {
	h.om.at( j ) = omA.at( j ).at( i );
      }
  }

  const Array& get( const std::string& key ) const
  {
    return data_.at( key );
  }

private:
  DataType data_;
};

#endif // CALIBRATIONREADER_HH
