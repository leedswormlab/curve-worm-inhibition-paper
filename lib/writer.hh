#ifndef WRITER_HH
#define WRITER_HH

#include <functional>
#include "probe.hh"

struct Writer {
  Writer(std::string filename, std::function<void(std::ofstream &)> value) : valueGetter(value), outputStream(filename) {
    outputStream.precision(std::numeric_limits<double>::max_digits10);
  }
  std::ofstream outputStream;
  std::function<void(std::ofstream &)> valueGetter;
};

struct WriterManager {
public:

  WriterManager(double writeTimeStep, std::string outputFolder)
      : writeTimeStep_(writeTimeStep), outputFolder_(outputFolder), nextWriteTime_(0) {}

  void add(std::string identifier,
           std::string filename,
           probe *probe_) {
    std::function<void(std::ofstream &)> listener = [probe_, this](std::ofstream &file_stream) -> void {
      writeData(file_stream, probe_);
    };
    listeners.insert(std::make_pair(identifier, Writer(outputFolder_ + filename, listener)));
  }

  void add(std::string identifier,
           std::string filename,
           std::vector<probe *> probes) {
    std::function<void(std::ofstream &)> listener = [probes, this](std::ofstream &file_stream) -> void {
      writeData(file_stream, probes);
    };
    listeners.insert(std::make_pair(identifier, Writer(outputFolder_ + filename, listener)));
  }

  void addVTU(std::string prefix, TimeProvider timeProvider, Entity &entity) {
    std::string indentifier = "vtu";
    VTUWriter vtuWriter_(prefix, timeProvider);

    // TODO: implement
  }

  void write(double time) {
    if (time >= nextWriteTime_) {
      for (auto &listener : listeners) {
        std::string identifier = listener.first;
        std::function<void(std::ofstream &)> callback = listener.second.valueGetter;
        std::ofstream &outputStream = listener.second.outputStream;

        callback(outputStream);
      }

      nextWriteTime_ += writeTimeStep_;
    }
  }

  void close() {
    for (auto &listener : listeners) {
      listener.second.outputStream.close();
    }
  }

private:
  typedef std::map<std::string, Writer> ListenerType;
  ListenerType listeners;
  const double writeTimeStep_;
  const std::string outputFolder_;
  double nextWriteTime_;

  std::ofstream createFileStream(std::string filename) {
    std::ofstream stream;
    stream.open(filename);
    stream.precision(std::numeric_limits<double>::max_digits10);
    return stream;
  }

  void writeData(std::ofstream &file, probe *probe_) {
    for (unsigned int i = 0; i < probe_->howmany(); ++i) {
      const double value = probe_->evaluate();
      if (i > 0) {
        file << ", ";
      }
      file << value;
    }
    file << std::endl;
  }

  void writeData(std::ofstream &file, std::vector<probe *> probes) {
    for (unsigned int p = 0; p < probes.size(); ++p) {
      probe *probe_ = probes.at(p);
      if (p > 0) {
        file << ", ";
      }

      for (unsigned int i = 0; i < probe_->howmany(); ++i) {
        const double value = probe_->evaluate();
        if (i > 0) {
          file << ", ";
        }
        file << value;
      }
    }
    file << std::endl;
  }
};

#endif //WRITER_HH
