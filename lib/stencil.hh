#pragma once

#include <assert.h>
#include <string>
#include <vector>
#include <array>
#include "trace.hh"
#include "fmatrix.hh"

class Entity;

/**
 *  stencil_base
 *
 *  holder for sparse matrices
 */
class stencil_base{
public:
	typedef unsigned hint_type;
protected:
  //! named contructor
  stencil_base( const std::string name = "" )
    : _name( name )
	{
	}
	//stencil_base(unsigned n, unsigned r, unsigned c)
	//	: _d(n), _p(n), _roff(r), _coff(c)
	//{ itested();
	//}
	virtual ~stencil_base(){
	}
public:
  //! name of stencil
  const std::string& name() const { return _name; }

  //! allocated memory for storage
	void alloc(unsigned meshsize){
		trace1("alloc", meshsize);
		unsigned ds = datasize(meshsize);
		_d.resize(ds);
		_p.resize(ds);
	}
public:
  //! initialise with r rows offset and c columns offset
	void init(unsigned r, unsigned c){
		_roff = r;
		_coff = c;
	}
public:
	void new_stencil(
			std::string const &s1,
		  	std::string const &s2,
			Entity& e, std::string key="");
private:
	virtual unsigned datasize(unsigned meshsize) = 0;
public:
  // clear all data
	void clear(){
		std::fill(_d.begin(), _d.end(), 0.);
	}

  // add d to entry n
	void add(unsigned n, double d){ itested();
		assert(d==d);
		// trace2("stencil_add", n, d);
		assert(n<_d.size());
		_d[n] += d;
		// want:
		//m.add( c+_coff, r+_roff, d, _p[n]);
	}

  // load entries into m
	template<class M>
	void load(M& m){
		for(unsigned i=0; i<_d.size(); ++i){ itested();
			unsigned c=col(i);
			unsigned r=row(i);
//			trace6("load", i, r, c, _d[i], _coff, _roff);
			m.add( r, c, _d[i], _p[i]);
		}
	}

  // mark entries in m for filling later
	template<class M>
	void mark(M& m){
		assert(_roff<10000);
		assert(_coff<10000);
//		trace1("mark", _d.size());
		for(unsigned i=0; i<_d.size(); ++i){ itested();
			unsigned c=col(i);
			unsigned r=row(i);
//			trace6("stencil_load", i, r, c, _d[i], _coff, _roff);
			assert(r<m.rows());
			assert(c<m.cols());
			// CHECK: offsets. src/tgt?
			// this only works left top
			m.mark( r, c);
		}
	}

	// collect positions in real matrix.
	template<class B>
	void map(B& b){
		for(unsigned i=0; i<_d.size(); ++i){ itested();
			// better: auto p=colrow(i);
			unsigned c=col(i);
			unsigned r=row(i);
			_p[i] = b.pos(r, c);
		}
	}
	bool done() const {
		return _d.size();
	}
public: // virtual interface
	virtual unsigned col(unsigned i) const=0;
	virtual unsigned row(unsigned i) const=0;
protected:
	std::vector<double> _d;
	std::vector<hint_type> _p;
protected:
	unsigned _roff;
	unsigned _coff;

private:
  const std::string _name;
};

// a diagonal of dim x dim blocks + 2 secondary diagonals
template<unsigned dim>
class stencil_dim_3diag : public stencil_base{
public:
  stencil_dim_3diag( const std::string name = "" )
    : stencil_base( name )
	{
	}
	stencil_dim_3diag(unsigned meshsize, unsigned r, unsigned c)
		: stencil_base(( 3*meshsize - 2 ) *dim*dim, r, c)
	{ itested();
		if(dim==2){ itested();
		//  0  1  4  5
		//  2  3  6  7
		//  8  9 12 13 16 17
		// 10 11 14 15 18 19
		//       20 21 24 25 28 29
		//       22 23 26 27 30 31
		//

		assert(block(0)==0);
		assert(block(1)==0);
		assert(block(2)==0);
		assert(block(3)==0);
		assert(block(4)==1);
		assert(block(5)==1);
		assert(block(8)==2);
		assert(block(11)==2);
		assert(block(27)==6);
		assert(block(28)==7);

		assert(blockrow(8)==1);

		assert(blockcol(0)==0);
		assert(blockcol(1)==0);
		assert(blockcol(2)==0);
		assert(blockcol(3)==0);
		assert(blockcol(4)==1);
		assert(blockcol(5)==1);
		assert(blockcol(6)==1);
		assert(blockcol(8)==0);
		assert(blockcol(9)==0);
		assert(blockcol(11)==0);
		assert(blockcol(28)==3);
		assert(blockcol(29)==3);

		assert(col(0)==_coff+0);
		assert(col(1)==_coff+1);
		assert(col(2)==_coff+0);
		assert(col(3)==_coff+1);
		assert(col(4)==_coff+2);
		assert(col(8)==_coff+0);
		assert(col(11)==_coff+1);

		assert(blockrow(0)==0);
		assert(blockrow(1)==0);
		assert(blockrow(2)==0);
		assert(blockrow(3)==0);
		assert(blockrow(4)==0);
		assert(blockrow(5)==0);
		assert(blockrow(11)==1);
		assert(blockrow(13)==1);
		assert(blockrow(18)==1);
		assert(blockrow(31)==2);
		assert(blockrow(32)==3);

		assert(row(0)==_roff+0);
		assert(row(1)==_roff+0);
		assert(row(2)==_roff+1);
		assert(row(3)==_roff+1);
		assert(row(4)==_roff+0);
		assert(row(8)==_roff+2);
		assert(row(11)==_roff+3);

		}
	}
public: // delinearize
	// dim per row.
	unsigned col(unsigned i) const{ itested();
		unsigned bc=blockcol(i);
		unsigned bp=blockpos(i);
		return bc*dim + bp % dim + _coff;
	}
	unsigned row(unsigned i) const{ itested();
		unsigned br=blockrow(i);
		unsigned bp=blockpos(i);
		return dim*br + bp/dim + _roff;
	}
  std::pair< unsigned, unsigned > raw_rowcol( const unsigned i ) const {
    std::pair< unsigned, unsigned > ret;

    const unsigned bp = i % (dim*dim);
    const unsigned b  = i / ( dim*dim );
    const unsigned br = (b+1) / 3;
    const unsigned bb = (b+1) % 3;
    const unsigned bc = bb+br-1;

    ret.first  = br*dim + bp / dim;
    ret.second = bc*dim + bp % dim;

    return ret;
  }
	void add_(unsigned brow, unsigned bcol, unsigned lrow, unsigned lcol, double d){ itested();
		return add(brow, bcol, lrow, lcol, d);
	}
	void add(unsigned brow, unsigned bcol, unsigned lrow, unsigned lcol, double d){ itested();
		assert(lrow<dim);
		assert(lcol<dim);
		assert(bcol<brow+2);
		assert(brow<bcol+2);
		//  0  1  4  5
		//  2  3  6  7
		//  8  9 12 13 16 17
		// 10 11 14 15 18 19
		//       20 21 24 25 28 29
		//       22 23 26 27 30 31
		//
		// linearized block address
		unsigned blin=bcol + brow*2;

		unsigned addr = blin*dim*dim + dim*lrow + lcol;
		// trace2("add", addr, d);
		stencil_base::add(addr, d);
	}
	// compute w=this*v (should be renamed or set w to 0)
  template<class W, class V>
	void mv(W& w, V const& v){ itested();
		assert(w.size()==v.size());

		for( unsigned i = 0; i < _d.size(); ++i ) {
		  // const unsigned my_row = row(i) - _roff;
		  // const unsigned my_col = col(i) - _coff;
		  const std::pair<unsigned,unsigned> p = raw_rowcol(i);
		  const unsigned my_row = p.first;
		  const unsigned my_col = p.second;
		  const double val = _d.at(i) * v.at( my_col );
		  w.add_scalar( my_row, val );
		}

		// return w;
	}
private:
	unsigned datasize(unsigned meshsize){
		assert(meshsize);
		return (3*meshsize - 2 ) *dim*dim;
	}
	unsigned block(unsigned i) const{ itested();
	  return i / ( dim*dim );
	}
	// position in a block
	unsigned blockpos(unsigned i) const{ itested();
		return i % (dim*dim);
	}
	// the row the block with i is aligned to.
	unsigned blockrow(unsigned i) const{ itested();
		unsigned b=block(i);
		return ( b + 1 ) / 3;
	}
	// the col the block with i is aligned to.
	unsigned blockcol(unsigned i) const{ itested();
		unsigned b=block(i) + 1;
		unsigned bb = b % 3 + b/3;
		return bb - 1;
	}
private:
//	M& _m;
// todo select block...
};

// a diagonal of dim x dim blocks
template<unsigned dim>
class stencil_dim_diag : public stencil_base{
public:
	stencil_dim_diag( const std::string name = "" )
	  : stencil_base( name ){
	}
	stencil_dim_diag(unsigned meshsize, unsigned r, unsigned c)
		: stencil_base(meshsize*dim*dim, r, c)
	{ itested();

		assert(block(0)==0);
		assert(block(1)==0);
		assert(block(2)==0);
		assert(block(3)==0);

		assert(col(0)==_coff+0);
		assert(col(1)==_coff+1);

		assert(row(0)==_roff+0);
		assert(row(1)==_roff+0);
		assert(row(2)==_roff+1);
	}
public: // add
	void add_(unsigned br, unsigned bc, unsigned r, unsigned c, double d){ itested();
	  assert( r < dim );
	  assert( c < dim );
		assert(br==bc);
		add(dim*(dim*br + r) + c, d);
	}
public: // delinearize
	// dim per row.
	unsigned col(unsigned i) const{ itested();
		return(block(i) * dim + i % dim + _coff);
	}
	// 0 1
	// 2 3
	//     4 5
	//     6 7
	unsigned row(unsigned i) const{ itested();
		unsigned blockrow = (i + dim ) / dim -1 ;
		return(blockrow + _roff);
	}

  std::pair< unsigned, unsigned > raw_rowcol(const unsigned i ) const {
    std::pair< unsigned, unsigned > p;
    p.first = i/dim;
    p.second = block(i)*dim + i%dim;
    return p;
  }

  /*
   * \brief sets row r to zeros except on diagonal which is set to id (if diag1)
   */
  void setDirichletRow( const unsigned r, const bool diag1 ){ untested();
    for( unsigned k = 0; k < dim; ++k ) {
      for( unsigned l = 0; l < dim; ++l ) {
	const unsigned i = dim*(dim*r+k)+l; // this might be only for a particular layout
	if( k == l and diag1 ) { // diagonal
	  _d.at(i) = 1.0;
	}
	else { // off diagonal
	  _d.at(i) = 0.0;
	}
      }
    }
  }

	// compute w=this*v (should be renamed or set w to 0)
  template<class W, class V>
	V& mv(W& w, V const& v){ itested();
		assert(w.size()==v.size());

		for( unsigned i = 0; i < _d.size(); ++i ) {
		  const auto p = raw_rowcol(i);
		  const unsigned my_row = p.first;
		  const unsigned my_col = p.second;
		  assert( my_row == row(i)-_roff );
		  assert( my_col == col(i)-_coff );
		  const double val = _d.at(i) * v.at( my_col );
		  w.add_scalar( my_row, val );
		}

		return w;
	}
  #if 0
	// compute w=this*v. hackish...
	template<class V>
	V& mv(V& w, V const& v){
		unsigned i=0;
		unsigned j=0;
		for(unsigned row=0; j<_d.size(); ){ itested();
			for(unsigned o=0; o<dim; ++o){ itested();
				for(unsigned lc=0; lc<dim; ++lc){ itested();
//					trace5("MV",  row, lc, i, j, _d.size());
					w[row] += v[lc+i] * _d[j];
					++j;
				}
				++row;
			}
			i += dim;
		}
		return w;
	}
#endif

private:
	unsigned datasize(unsigned meshsize){
		return meshsize*dim*dim;
	}
	unsigned block(unsigned i) const{ itested();
		return(i + dim*dim )/ (dim*dim) - 1 ;
	}
private:
//	M& _m;
// todo select block...
};

// a diagonal of dim x dim blocks
template<unsigned dim>
class stencil_dim_diag_Q : public stencil_base{
public:
	stencil_dim_diag_Q( const std::string name = "" )
	  : stencil_base( name ){
	}
  #if 0
  // this base constuctor doesn't exist?
	stencil_dim_diag_Q(unsigned meshsize, unsigned r, unsigned c)
		: stencil_base(meshsize*dim*dim, r, c)
	{ itested();

		assert(block(0)==0);
		assert(block(1)==0);
		assert(block(2)==0);
		assert(block(3)==0);

		assert(col(0)==_coff+0);
		assert(col(1)==_coff+1);

		assert(row(0)==_roff+0);
		assert(row(1)==_roff+0);
		assert(row(2)==_roff+1);
	}
  #endif
public: // add
	void add_(unsigned br, unsigned bc, unsigned r, unsigned c, double d){ itested();
		assert(br==bc);
		add(dim*(dim*br + r) + c, d);
	}
public: // delinearize
	// dim per row.
	unsigned col(unsigned i) const{ itested();
		return(block(i) * dim + i % dim + _coff);
	}
	// 0 1
	// 2 3
	//     4 5
	//     6 7
	unsigned row(unsigned i) const{ itested();
		unsigned blockrow = (i + dim ) / dim -1 ;
		return(blockrow + _roff);
	}

	// compute w=this*v (should be renamed or set w to 0)
  template<class W, class V>
	V& mv(W& w, V const& v){ itested();
		assert(w.size()==v.size());

		for( unsigned i = 0; i < _d.size(); ++i ) {
		  const unsigned my_row = row(i) - _roff;
		  const unsigned my_col = col(i) - _coff;
		  const double val = _d.at(i) * v.at( my_col );
		  w.add_scalar( my_row, val );
		}

		return w;
	}
  #if 0
	// compute w=this*v. hackish...
	template<class V>
	V& mv(V& w, V const& v){
		unsigned i=0;
		unsigned j=0;
		for(unsigned row=0; j<_d.size(); ){ itested();
			for(unsigned o=0; o<dim; ++o){ itested();
				for(unsigned lc=0; lc<dim; ++lc){ itested();
//					trace5("MV",  row, lc, i, j, _d.size());
					w[row] += v[lc+i] * _d[j];
					++j;
				}
				++row;
			}
			i += dim;
		}
		return w;
	}
  #endif
private:
	unsigned datasize(unsigned meshsize){
	  return (meshsize-1)*dim*dim;
	}
	unsigned block(unsigned i) const{ itested();
		return(i + dim*dim )/ (dim*dim) - 1 ;
	}
private:
//	M& _m;
// todo select block...
};

// TODO missing add specialisation for ADD
template<unsigned dim>
class stencil_asym : public stencil_base{
public:
	stencil_asym( const std::string name = "" )
	  : stencil_base( name ){
	}
	stencil_asym(unsigned meshsize, unsigned src, unsigned tgt)
		: stencil_base(4*(meshsize-1)*dim, 0, 0) // , _src(src), _tgt(tgt)
	{ unreachable();
		unsigned N=_d.size()/4/dim + 1; (void)N;
		assert(N==meshsize);
		// assert(col(0) == dim*N*2);

		assert(row(0) == 0);
		assert(row(1) == 1);
		assert(row(2) == 2);
		assert(row(3) == 3);
		assert(row(4) == 4);
		assert(row(5) == 5);

		assert(row(dim*(N-1)) == dim);
		assert(_d.size() == (N-1)*dim*4);
	}
public: // delinearize
	// dim per row.
	unsigned row(unsigned i) const{ itested();
		assert(i<_d.size());
		unsigned ret;
		if(i<_d.size()/4){ itested();
			ret = i;
		}else if(i<_d.size()/2){ itested();
			ret = i - _d.size()/4 + dim;
		}else{ itested();
			ret = col(i-_d.size()/2);
		}
//		trace2("asym row", i, ret);
		return ret;
	}
	unsigned col(unsigned i) const{ itested();
		assert(i<_d.size());
		unsigned N=_d.size()/4/dim + 1; (void)N;
		unsigned offset = _roff; // _src?
		//assert(offset== 2*N*dim);
		unsigned ret;
		if(i<_d.size()/4){ itested();
			ret = offset + (i + dim) / dim - 1;
		}else if(i<_d.size()/2){ itested();
			ret = offset + (i-_d.size()/4 + dim) / dim - 1;
		}else{
			assert(!_coff); // for now.
			ret = _coff /*_tgt?*/ + row(i-_d.size()/2);
		}
//		trace2("asym col", i, ret);
		return ret;
	}
private:
	unsigned datasize(unsigned meshsize){
		return 4*(meshsize-1)*dim;
	}
private:
//	unsigned _src;
//	unsigned _tgt;
}; // stencil_asym

// stencil_grad is for terms of the form
// the row index is the test function index
// the col index is the trial function index
// the block layout is
/*
[ X       ]
[ XX      ]
[  XX     ]
[   ..    ]
[     ..  ]
[      XX ]
[       X ]
 */
template< unsigned row_dim, unsigned col_dim >
class stencil_grad : public stencil_base {
  static const unsigned block_size = row_dim*col_dim;

public:
  stencil_grad( const std::string name = "" )
    : stencil_base( name ) {}

  unsigned row( const unsigned i ) const {
    assert( i < _d.size() );

    const unsigned block_index = i / block_size;
    const unsigned inner_index = i % block_size;

    const unsigned block_row = ( block_index + 1 ) / 2;
    assert( block_row < n_row_blocks() );
    const unsigned inner_row = inner_index / col_dim;
    assert( inner_row < row_dim );

    const unsigned raw_row = block_row * row_dim + inner_row;
    assert( raw_row < row_dim * n_row_blocks() );

    return raw_row + _roff;
  }

  unsigned col( const unsigned i ) const {
    assert( i < _d.size() );

    const unsigned block_index = i / block_size;
    const unsigned inner_index = i % block_size;

    const unsigned block_col = block_index / 2;
    assert( block_col < n_col_blocks() );
    const unsigned inner_col = inner_index % col_dim;
    assert( inner_col < col_dim );

    const unsigned raw_col = block_col * col_dim + inner_col;
    assert( raw_col < col_dim * n_col_blocks() );

    return raw_col + _coff;
  }

  void add( const unsigned block_row, const unsigned block_col,
	    const unsigned inner_row, const unsigned inner_col,
	    const double value ) {
    assert( block_row < n_row_blocks() );
    assert( block_col < n_col_blocks() );
    assert( inner_row < row_dim );
    assert( inner_col < col_dim );
    assert( block_row == block_col or block_row == block_col + 1 );
    assert( value == value );

    const unsigned block_index = 2 * block_col + ( block_row - block_col );
    const unsigned inner_index = inner_col + inner_row * col_dim;

    const unsigned index = block_index * block_size + inner_index;

    stencil_base::add( index, value );
    return;
  }

  template< class V, class W >
  void mv( V& y, const W& x ) const {
    // TODO should this be and really we want some sort of axpy
    // method?
    // std::fill( y.begin(), y.end(), 0.0 );

    for( unsigned i = 0; i < _d.size(); ++i ) {
      const unsigned my_row = row(i) - _roff;
      const unsigned my_col = col(i) - _coff;
      const double val = x.at( my_col ) * _d.at(i);
      y.add_scalar( my_row, val );
    }
  }

private:
  unsigned n_row_blocks() const {
    return _meshsize;
  }
  unsigned n_col_blocks() const {
    return _meshsize-1;
  }
  unsigned datasize( const unsigned meshsize ) {
    _meshsize = meshsize;
    return 2*(meshsize-1)*block_size;
  }

  unsigned _meshsize;
};

// stencil_div is for terms of the form
// the row index is the test function index
// the col index is the trial function index
// the block layout is
/*
[ XX      ]
[  XX     ]
[   XX    ]
[   ..    ]
[     ..  ]
[     XX  ]
[      XX ]
 */
template< unsigned row_dim, unsigned col_dim >
class stencil_div : public stencil_base {
  static const unsigned block_size = row_dim*col_dim;

public:
  stencil_div( const std::string name = "" )
    : stencil_base( name ) {}

  unsigned row( const unsigned i ) const {
    assert( i < _d.size() );

    const unsigned block_index = i / block_size;
    const unsigned inner_index = i % block_size;

    const unsigned block_row = block_index / 2;
    assert( block_row < n_row_blocks() );
    const unsigned inner_row = inner_index / col_dim;
    assert( inner_row < row_dim );

    const unsigned raw_row = block_row * row_dim + inner_row;
    assert( raw_row < row_dim * n_row_blocks() );

    return raw_row + _roff;
  }

  unsigned col( const unsigned i ) const {
    assert( i < _d.size() );

    const unsigned block_index = i / block_size;
    const unsigned inner_index = i % block_size;

    const unsigned block_col = ( block_index + 1 ) / 2;
    assert( block_col < n_col_blocks() );
    const unsigned inner_col = inner_index % col_dim;
    assert( inner_col < col_dim );

    const unsigned raw_col = block_col * col_dim + inner_col;
    assert( raw_col < col_dim * n_col_blocks() );

    return raw_col + _coff;
  }

  void add( const unsigned block_row, const unsigned block_col,
	    const unsigned inner_row, const unsigned inner_col,
	    const double value ) {
    assert( block_row < n_row_blocks() );
    assert( block_col < n_col_blocks() );
    assert( inner_row < row_dim );
    assert( inner_col < col_dim );
    assert( block_row == block_col or block_row + 1 == block_col );
    assert( value == value );

    const unsigned block_index = 2 * block_row + ( block_col - block_row );
    const unsigned inner_index = inner_col + inner_row * col_dim;

    const unsigned index = block_index * block_size + inner_index;

    stencil_base::add( index, value );
    return;
  }

  template< class DF1, class DF2 >
  void mv( DF1& y, const DF2& x ) const {
    // TODO should this be and really we want some sort of axpy
    // method?
    // std::fill( y.begin(), y.end(), 0.0 );

    for( unsigned i = 0; i < _d.size(); ++i ) {
      const unsigned my_row = row(i) - _roff;
      const unsigned my_col = col(i) - _coff;
      const double val = _d.at(i) * x.at( my_col );

      y.add_scalar( my_row, val );
    }
  }

private:
  unsigned n_row_blocks() const {
    return _meshsize-1;
  }
  unsigned n_col_blocks() const {
    return _meshsize;
  }
  unsigned datasize( const unsigned meshsize ) {
    _meshsize = meshsize;
    return 2*(meshsize-1)*block_size;
  }

  unsigned _meshsize;
};
