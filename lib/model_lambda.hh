#ifndef MODEL_LAMBDA_HH
#define MODEL_LAMBDA_HH

#include <memory>
#include <iostream>
#include <functional>
#include <vector>
#include "vector.hh"
#include "timeprovider.hh"
#include "parameter.hh"
#include "matrix.hh"
#include "readMat.hh"
#include <vector>
#include "worm.hh"

#ifdef OpenCV_FOUND
#include "distancefunction.hh"
#include "forcefunction.hh"
#include "model_distance.hh"
#endif

#include "femscheme.hh"


ConfigParameters hack_emptyParm; // REMOVE (didn't compile without)

template<const unsigned int mydim>
struct LambdaMuscleModel : public ModelDefault<mydim> {
private:
  using BaseType = ModelDefault<mydim>;

public:
  static const unsigned int dim = BaseType::dim;
  using RangeVectorType = typename BaseType::RangeVectorType;
  using Constraint = typename BaseType::Constraint;
  typedef const std::function<RangeVectorType(double)> initialtype;
  typedef std::function<double(double, double)> betatype;

  LambdaMuscleModel(ConfigParameters &parameters) // TODO: only pass relevant parameters
      : BaseType(parameters),
        _X0(NULL),
        _beta(NULL),
      // TODO: why not move to ModelDefault?
        constraint_(parameters.getFromList("model.constraint", {"velocity", "measure"})),
        eps_(parameters.get<double>("model.I2.eps")) {
  }
  // use setbeta instead.
  LambdaMuscleModel(std::function<double(double, double)> beta)
      : BaseType(hack_emptyParm),
        _beta(beta),
        eps_(1e-4),
        constraint_(BaseType::Constraint::measure) {
    incomplete();
  }
  explicit LambdaMuscleModel()
      : BaseType(hack_emptyParm),
        _beta(beta),
        eps_(1e-4),
        constraint_(BaseType::Constraint::measure)
  {
    incomplete();
  }
  LambdaMuscleModel(LambdaMuscleModel const &l)
      : BaseType(l),
        _beta(l._beta),
        constraint_(l.constraint_),
        eps_(l.eps_) {
    assert(eps_);
  }

  // TODO: replace with setParameters(k, v)
  void setX0(initialtype const *p) {
    untested();
    _X0 = p;
  }
  void setBeta(betatype const *p) {
    untested();
    _beta = *p;
  }

  virtual RangeVectorType X0(const double s) const {
    //  assert(_X0);
    //return (*_X0)( s );
    if (_X0) {
      untested();
      return (*_X0)(s);
    }

    RangeVectorType ret;
    ret[0] = s;
    ret[1] = 0.;
    return ret;
  }

  using BaseType::Force;
  using BaseType::gamma;
  using BaseType::K;

  virtual double regularisation(double const &s, const Entity &e) const {
    untested();

    // now what?!
    const double num = 2.0 * std::sqrt((eps_ + s) * (eps_ + 1.0 - s));
    const double den = 1.0 + 2.0 * eps_;

    return BaseType::regularisation(s, e) * (num * num * num) / (den * den * den);
  }

  virtual double mu(const double &s, const Entity &e) const {
    // now what?!
    const double num = 2.0 * std::sqrt((eps_ + s) * (eps_ + 1.0 - s));
    const double den = 1.0 + 2.0 * eps_;

    return BaseType::mu(s, e) * (num * num * num) / (den * den * den);
  }

#if 0
  // BUG? why is this virtual?
  virtual double beta( double const& s, const Entity& e ) const { itested();
      assert(_beta);
    return (*_beta)( s, e.time() );
  }
#else
  /* TODO is this the correct way to write this in? */
  virtual double beta(double const &s, const Entity &e) const {
    itested();
    assert(_beta);

    double beta_val=(_beta)(s, e.time());
    double beta_old=(_beta)(s, e.time() - eps_);

    return regularisation(s, e) * beta_val + mu(s, e) * (beta_val - beta_old) / eps_;
  }
#endif

  virtual double energyDensity(const RangeVectorType &X, const RangeVectorType &Y) const {
    untested();
    unreachable();
    return 0;
  }

  virtual Constraint constraint() const {
    switch (constraint_) {
    case 0: untested();
      return Constraint::velocity;
    case 1:return Constraint::measure;
    default:return Constraint::measure;
    }

  }

  double energyDensity(double const &s, Entity const &e) const {
    return 0.;
  }

private:
  const std::function<RangeVectorType(double)> *_X0;
  std::function<double(double, double)> _beta;
  const int constraint_;
  const double eps_;
};
#endif // MODEL_LAMBDA_HH

// vim:ts=8:sw=2
