#pragma once

#include <map>
#include <iostream>
#include <array>
#include <math.h>
#include <assert.h>
#include "../trace.hh"

typedef unsigned char uchar;

#define NMassert(x)

// nested map. container similar to std::map< array, value_type >
//
// but with the intent to (quickly) iterate through all elements close to a
// key.
//
// should be doable for backend containers in general (how?).
namespace nest{

template<class K, class val, unsigned depth,
         template<class X, class ...> class C>
class iter_root_;

// needed to stop carry propagation at the top.
class dummy_root{
public:
	template<class T>
	static bool carry(T const&) { untested();
	  	unreachable(); return false;
	}

	template<class T>
	bool is_end(T const& t)const{ untested();
		return false;
	}

	unsigned top_radius_sq() const{ untested();
		return 0;
	}
	unsigned radius_sq() const{ untested();
		return 0;
	}

	int* center() const{ unreachable();
		return NULL;
	}
};

struct better_t {} better;
struct END_t {} _END;
struct TMP {} _TMP;
struct noroot_t {} _noroot;
struct child_t {} _child;

template<class I>
bool is_empty(std::pair<I,I> i)
{
	return i.first==i.second;
}
template<class I>
unsigned count_range(std::pair<I,I> i)
{ untested();
	unsigned cnt=0;
	for(;i.first!=i.second; ++i.first){ untested();
		++cnt;
	}
	return cnt;
}

unsigned my_round(float i)
{
	return ceil(i)-1;
}

unsigned short isqrt(unsigned short value)
{
    int i;
    unsigned short rem, root;

    rem  = 0;
    root = 0;

    // loop over the eight bits in the root
    for ( i = 0; i < 8; i++ ) {
        // shift the root up by one bit
        root <<= 1;

        // move next two bits from the input into the remainder
        rem = ((rem << 2) + (value >> 14));
        value <<= 2;

        // test root is (2n + 1)
        root++;

        if ( root <= rem ) {
            // root not more than the remainder, so the new bit is one
            rem -= root;
            root++;
        } else {
            // root is greater than the remainder, so the new bit is zero
            root--;
        }
    }
    return (root >> 1);
}

unsigned my_sqrt(unsigned i)
{

//	assert(isqrt(i) == unsigned(sqrt(float(i))));
	// assert(i<30001);
//	std::cout << "S " << i << "\n";
	if(i==0){return 0;}

	assert(my_round(sqrt(float(i)))== unsigned(sqrt(i-1)));
	return unsigned(sqrt(i-1));

   const uchar lut[]={
		// ...
	};
	return lut[i];
}

size_t radius_sq_left(unsigned iter,
		                  unsigned par_radius_sq,
								unsigned ccnt)
{
	long delta=long(iter) - long(ccnt);
	NMassert(par_radius_sq >= delta*delta);
	return long(par_radius_sq) - delta*delta ;
}


template<class K, class val, unsigned depth,
         template<class X, class ...> class C=std::map>
class nested_map;
template<class K, class val,
         template<class X, class ...> class C>
class nested_map<K,val,1, C>;

template<class K, class val, unsigned depth,
         template<class X, class ...> class C>
class nested_map { //
private:
	typedef nested_map<K, val, depth-1, C> lower_map_type;
	typedef C<K, lower_map_type> container_type;
//private:
	typedef typename container_type::const_iterator base_iterator;
public:
	typedef val value_type;
	typedef std::array<K, depth> key_type;
protected: // BUG?? // depth >1
public:
	nested_map(const nested_map&) = default; // BUG??{unreachable();}
public: // construct >1
	nested_map(){ }
public: // move
	nested_map& operator=(nested_map&& o) { itested();
		_c = std::move(o._c);
		return *this;
	}
public: // depth >1
	template<class parent_iterator /*, ... constraints */ >
	class const_iterator_ : /*protected*/ public lower_map_type::template
								  const_iterator_<const_iterator_<parent_iterator> >{ //
	public:
		typedef container_type ct;
		typedef typename container_type::const_iterator base_iterator;
		typedef typename lower_map_type::template const_iterator_<const_iterator_<parent_iterator> >
		                 base_class;
		typedef typename lower_map_type::template const_iterator_<const_iterator_> lower_iterator;
	private:
		const_iterator_() { incomplete(); }
	public: // BUG. friends..

		explicit const_iterator_(TMP, parent_iterator* p)
		    : lower_iterator(_TMP, this), _radius_sq(-1u) { itested();
				 // incomplete(); // remove
		}

		const_iterator_(END_t, base_iterator e, parent_iterator* p)
		  	 : lower_iterator(_END, this),
			   _base(e), _radius_sq(-1)
		{
			// necessary?
		}
		// need to pass parent for leaf..
		const_iterator_(END_t, parent_iterator* p)
		  	 : lower_iterator(_END, this),
			   _base(), _radius_sq(-1)
		{ itested();
		}
	private: // deep copy, only accessible from root_iter.
		const_iterator_(const_iterator_ const& o, child_t)
	     : base_class(o, _child),
		    _base(o._base),
			 _end(o._end),
			 _radius_sq(o._radius_sq)
		{ itested();
		}
	public:
		const_iterator_(base_iterator const& i, END_t)
			: lower_iterator(_END, this), _base(i), _radius_sq(-1u)
		{ untested();
			incomplete();
		}

      // >1
		// this is used to pass a range to the lower iterator constructor.
		// also saves _end here.
		std::pair<typename lower_iterator::base_iterator,
		          typename lower_iterator::base_iterator>
		help_construct(typename lower_iterator::ct const& c)
		{
			auto rs=radius_sq();
			if(c.empty()){ untested();
				//trace1("help empty range", radius_sq());
				// dummy.
				auto x=typename lower_iterator::base_iterator();
				_end = x;
				return std::make_pair(x,x);
			}else if(rs==-1u){
				_end = c.end();
				return std::make_pair( c.begin(), c.end());
			}else{
				unsigned radius = my_sqrt(rs);

				int lb=int(*center()) - int(radius);
				int ub=int(*center()) + int(radius);

				lb = std::max(0, lb);
				ub = std::min(255, ub);

				auto bb=c.lower_bound(lb);
				auto ee=c.upper_bound(ub);

				_end = ee;
				return std::make_pair (bb, ee);
			}
		}

	private: // >1
		bool is_end() const{
			return parent().is_end(_base);
		}
	public: // >1
		unsigned const& top_radius_sq() const{
			return parent().top_radius_sq();
		}
		unsigned const& radius_sq() const{
			return _radius_sq;
		}
	private: // used in deep copy
		void assign(const const_iterator_& o){
			_base=o._base;
			_end=o._end;
			_radius_sq=o._radius_sq;

			lower_iterator::assign(o);
		}
	public: // >1
		bool operator==( const const_iterator_& o) const{
			if( is_end() && o.is_end()){
				return true;
			}else if( is_end()){
				return false;
			}else if( o.is_end()){
				return false;
			}else if( _base!=o._base) { untested();
				return false;
			}else{
				return lower_iterator::operator==(o);
			}
		}
		bool operator!=( const const_iterator_& o) const{ untested();
			unreachable();
			return !operator==(o);
		}

		const_iterator_& assign_increment(base_iterator const& i){
			_base = i;

			if(_base->second.empty()){ untested();
			}else{
			}
			lower_iterator::assign_increment(_base->second.begin___A());
			_end = _base->second.end___A();
			// NMassert(lower_iterator::_parent==this); end?!
			lower_iterator::set_parent(this);
			return *this;
		}

		// >1
		template<class P>
		bool assign_pair_range(P const& p_){
			auto p=p_; //why?
			while(!is_empty(p)){
				bool done=assign_pair(p);
				if(done){
					return true;
				}else{
					++p.first;
				}
			}
			_base = p_.first;
			return false;
		}
	private:
		bool assign_pair(std::pair<base_iterator, base_iterator> const& r) {
			NMassert(r.first!=r.second);
			
			if(parent().radius_sq()==-1u){
				assert(_radius_sq == -1u );
			}else{ itested();
				NMassert(parent().center());
				int ccnt=int(*parent().center());
			//	trace4("left square ", ccnt, depth, parent().radius_sq(), int(r.first->first));

				_radius_sq = radius_sq_left(r.first->first, parent().radius_sq(), ccnt );
			}

			_base = r.first;
			//trace2("assigned pair", depth, int(_base->first));

			lower_iterator::set_parent(this); // TODO, pass to constructor
			auto P(help_construct(r.first->second._c));

			//trace2("assigning pair0", depth, this);
			return lower_iterator::assign_pair_range(P);
		}
	public: // op
		const_iterator_& operator++(){
			// redirect down, then propagate back.
			lower_iterator::operator++();
			return *this;
		}
		std::pair<unsigned /* for now */, val> operator*() const{
			// trace2("op*", depth, int(_base->first));
			return lower_iterator::operator*();
		}
		std::pair<base_iterator, bool> erase_(){ untested();
			auto x=lower_iterator::erase_();

			if(x.second){ untested();
				auto& S=const_cast<typename base_iterator::value_type::second_type::container_type&>(_base->second._c);
				S.erase(x.first);
				return std::make_pair(_base, _base->second.empty());
			}else{ untested();
				return std::make_pair(_base, false);
			}
		}
	protected: // >1
		void increment(){
			NMassert(!is_end());
			++_base;

			if(parent().carry(*this)){
			}else if(is_end()){ untested();
				unreachable();
				//trace1("no prop down", depth);
				return;
			}else if(_base->second.empty()){ untested();
				incomplete(); // not yet..
			}else if(parent().center()){
				int ccnt=int(*parent().center());
				_radius_sq = radius_sq_left(_base->first, parent().radius_sq(), ccnt );

				typename lower_iterator::ct const& cont=_base->second._c;
				auto P(help_construct(cont));
				assert(_end==P.second);

				bool success=lower_iterator::assign_pair_range(P);

				if(success){
				}else{
					increment();
				}
			}else{
				_end = _base->second.end___A();
				lower_iterator::assign_increment(_base->second.begin___A());
			}
			assert(&lower_iterator::parent()==this);
		}
	private:
	public: // BUG, should be private...
		bool is_end(typename lower_iterator::base_iterator const& e) const{
			bool end(_end==e);
			return end;
		}
	public: // > 1
		bool carry(lower_iterator const& e){
			if( is_end(e.base()) ){
				increment();
				return true;
			}else{
				return false;
			}
		}
	public:
		base_iterator const& base() const{return _base;}
	public:
		K const* center() const{
		  	if( parent().center() ) {
				return parent().center()+1;
			}else{ itested();
				return NULL;
			}
		}
	private:
		parent_iterator const& parent() const{ itested();
			return *(parent_iterator const*)(this);
		}
		parent_iterator& parent(){
			return *(parent_iterator*)(this);
		}
	public: // should be a base class, probably
		base_iterator _base;
	private:
		typename lower_iterator::base_iterator _end;
		unsigned _radius_sq;
	public:
		friend parent_iterator;
	}; // const_iterator_ > 1

	typedef iter_root_<K, val, depth, C> iter_root;
	typedef iter_root const_iterator;

	// needed?
	typedef const_iterator_<iter_root> top_const_iterator;

public:
	val& operator[](key_type const& a){
		return get(&a.front());
	}
	unsigned erase(key_type const& a){
		assert(a.size()==depth);
		return erase_(a.data());
	}
	void erase(const_iterator& a){
		a.erase_(_c);
	}
	size_t recount() const{
		auto p=begin();
		size_t c=0;
		for(;p!=end();++p){
			++c;
		}
		return c;
	}
private:
	unsigned erase_(K const* a){
		auto i=_c.find(*a);
		if(i==_c.end()){ untested();
			// not found
			return 0;
		}else if(i->second.erase_(&a[1])){
			if(i->second.empty()){ itested();
				_c.erase(i);
			}else{ itested();
			}
			return 1;
		}else{ untested();
			return 0;
		}
	}
public: // iterator access >1
	const_iterator begin(K const* center=NULL, unsigned radius_squared=-1u) const{
		if(radius_squared!=-1u){
			NMassert(center);
			// this is a "root" thing.
			return const_iterator(better, _c, center, radius_squared);
		}else{
			//trace3("creating unconstrained begin", depth, container_type::size(), radius_squared);
			return const_iterator(better, _c, NULL, radius_squared);
		}
	}
	const_iterator end(K  const* center=NULL, unsigned radius_squared=-1u) const{
		if(center && radius_squared!=-1u){
			unsigned radius = my_sqrt(radius_squared);

			auto i=_c.upper_bound(radius + *center);
			return const_iterator(_END, i);
		}else{
			auto e=_c.end();
			return const_iterator(_END, e);
		}
	}
// private:
 	base_iterator begin___A() const{
 		return _c.begin();
 	}
 	base_iterator end___A() const{
 		return _c.end();
 	}
protected:
	bool empty() const{
		return _c.empty();
	}
	val& get(K const* v){
		return _c[*v].get(v+1);
	}
public:
	bool contains(std::array<K,depth> const& a) const{
		return contains(a.data());
	}
//protected:?
	bool contains(K const* v) const{
		auto I=_c.find(*v);
		if(_c.find(*v)==_c.end()){
			return false;
		}else{
			return I->second.contains(v+1);
		}
	}
private:
	container_type _c;
public:
	friend class nested_map<K, val, depth+1, C>;
}; // nested_map >1


template<class K, class val,
         template<class X, class ...> class C>
class nested_map<K, val, 1, C>{
public:
	typedef val value_type;
	typedef std::array<K, 1> key_type;
public: // really?
	typedef C<K, val> container_type;
	typedef typename container_type::const_iterator base_iterator;
public: // really?
	template<class parent_iterator>
	class const_iterator_
	//    : public /*protected?*/ base_iterator
		//	, protected const_iterator_<const_iterator_<parent_iterator> >
	{ //
	private:
	public: // types
		typedef container_type ct;
		typedef typename container_type::const_iterator base_iterator;
		typedef base_iterator leaf_iterator; // ?
	public: // BUG. friends.
//		const_iterator_(END_t) {}
	private: // construct ==1
#if 1
		explicit const_iterator_(container_type const& m)
		{ incomplete();
		}
#endif
  		explicit const_iterator_(const_iterator_ const& x, noroot_t)
  			 : _leaf_base(x), _has_parent(false)
		{ untested();
  		}
  		explicit const_iterator_(const_iterator_ const& x, child_t)
  			 : _leaf_base(x._leaf_base), _has_parent(true)
		{
  		}
	public: // used in ==1 begin
  		explicit const_iterator_(base_iterator const& x, noroot_t)
  			 : _leaf_base(x), _has_parent(false)
		{
  		}
	public: // construct "==1"
		explicit const_iterator_(TMP, parent_iterator* p): _has_parent(true)
		{ itested();
		}

		explicit const_iterator_(END_t, parent_iterator* p)
		  	 //: lower_iterator(_END, this),
			 :  _leaf_base(), _has_parent(p)
		{
		}
	public: // op "==1"
		bool operator==( const const_iterator_& o) const{
			return _leaf_base==o._leaf_base;
		}
		bool operator==( const base_iterator& o) const{ untested();
			return _leaf_base==o;
		}
		const_iterator_& operator=(base_iterator const& o){ untested();
			incomplete(); // really?
			_leaf_base = o;
			NMassert(!o._has_parent);
			NMassert(!_has_parent);
			return *this;
		}
		const_iterator_& operator=(const_iterator_ const& o){ untested();
			//trace0("leaf op=");
			unreachable();
			NMassert(!o._has_parent);
			NMassert(!_has_parent);
			_leaf_base = o._leaf_base;
			return *this;
		}
	public: //const access
		base_iterator const& base() const{return _leaf_base;}
	private: // used in deep assign
		void assign(const_iterator_ const& o){
			//trace1("leaf assign depth=1", parent().radius_sq());
			_leaf_base = o._leaf_base;
			_has_parent = true;
		}
	private:
		const_iterator_& assign_increment(base_iterator const& o){
			_leaf_base = o;
			return *this;
		}
		bool is_end() const{
			NMassert(_has_parent);
			return parent().is_end(_leaf_base);
		}
	public: // ==1
		template<class P>
		bool assign_pair_range(P const& p_){
			auto p=p_;
			while(!is_empty(p)){
				//trace1("trying 1", int(p.first->first));
				bool done=assign_pair(p);
				if(done){
					//trace1("assigned 1", int(p.first->first));
					//trace1("assigned 1", int(_leaf_base->first));
					//trace1("assigned 1", operator*().first);
					return true;
				}else{ untested();
					++p.first;
					// not in leaf...
					// _radius_sq=radius_sq_left(p.first, _parent->radius_sq(), *center())
				}
			}
			_leaf_base = p_.first;
			return false;
		}
		bool assign_pair(std::pair<base_iterator, base_iterator> const& x){
			NMassert(_has_parent);
			if(x.first!=x.second){
				//trace1("leaf assign", int((x.first)->first));
				//trace1("leaf assign", ((x.first)->second));
				_leaf_base = x.first;
				return true;
			}else{ unreachable();
				//trace0("leaf assign empty range depth=1");
				return false;
			}
		}
	public: // ==1
		const_iterator_& operator++(){
			//trace2("1 ++", int((*_leaf_base).first), (*_leaf_base).second);
			_leaf_base.operator++();
			if(!_has_parent){
				//trace0("carry end");
			}else{
				//trace1("parent carry depth=1", is_end());
				parent().carry(*this);
				//trace1("parent carry depth=1", is_end());
//				trace1("parent carried", &leaf());
				// trace2("test leaf", &_leaf_base, _leaf_base->second);
			}
			return *this;
		}
		// ==1
		std::pair<unsigned /* for now */, val> operator*() const{
			//trace2("op* leaf", this, int(_leaf_base->first));
			if(_has_parent){
				NMassert(!is_end());
				NMassert(!parent().is_end(_leaf_base));
			}else{
			}
			return std::make_pair(distance_sq(), _leaf_base->second);
		}
		std::pair<base_iterator, bool> erase_(){ untested();
			return std::make_pair(_leaf_base, true);
		}
#if 0
		leaf_iterator const& leaf() const{ untested();
			return _leaf_base;
		}
#endif
		bool operator!=(const base_iterator& o) const{ untested();
		  return _leaf_base!=o;
		}
		bool operator!=(const const_iterator_& o) const{
		  return _leaf_base!=o._leaf_base;
		}
	private:
		unsigned top_radius_sq() const{
			if(_has_parent){
				return parent().top_radius_sq();
			}
			return 0;
		}
		unsigned distance_sq() const{
			// trace1("dsq", _has_parent);
			//trace1("dsq",int(_leaf_base->first));
			if(!_has_parent){
				return 0;
			}else if(parent().center()){
				int bound=top_radius_sq();
				int ccnt=int(*parent().center());
				// int delta=int(_leaf_base->first) - ccnt;
				unsigned r=bound - radius_sq_left(_leaf_base->first, parent().radius_sq(), ccnt);
				return r;
			}else{
				return 0;
			}
		}
	public:// hack
		void set_parent(parent_iterator* p){
			NMassert(_has_parent);
			NMassert(&parent()==p);
//			parent()=p;
		}
	private:
		parent_iterator const& parent() const{
			// reinterpret_cast<parent_iterator const*>(this) // no!
			NMassert(_has_parent);
			return *(parent_iterator*)this;
		}
		parent_iterator& parent(){
			NMassert(_has_parent);
			return *(parent_iterator*)this;
		}
	public:
		template<class E>
		E const& end(E const& e){
			return e;
		}
	public:
		base_iterator _leaf_base;
	private:
		bool _has_parent; // BUG, ask dummy_root.
	public:
		friend parent_iterator;
	};

	typedef const_iterator_<dummy_root> const_iterator;
private:
public: // BUG
	nested_map( const nested_map& o) = default; // ?? { unreachable(); }
public: // construct
	nested_map(){}
	nested_map(nested_map&& o) = default;
//	    : container_type(std::move(o)){}
public: // move ==1
	nested_map& operator=(nested_map&& o) = default;
#if 0
  	{
		container_type::operator=(std::move(o));
		return *this;
	}
#endif
public: // map access, only needed if !up...
	size_t size() const{
		return _c.size();
	}
	val& operator[](std::array<K, 1> const& a){
		return get(&a.front());
	}
	bool contains(K const* v) const{ itested();
		return _c.find(*v)!=_c.end();
	}

	// want all keys p with |center-p|^2 < radius_squared
	// need to start at center-p < radius, center-radius < p
	const_iterator begin(K const* center=NULL, unsigned radius_squared=-1u) const{
		if(center && radius_squared!=-1u){
			// lut?
			unsigned radius = my_sqrt(radius_squared);

			// need max, if key is unsigned...
			auto i=_c.lower_bound(std::max(0, int(*center) - int(radius)));
		
			if(i!=_c.end()){
				//trace1("startat", i->second);
			}else{ untested();
				untested();
			}

			return const_iterator(i, _noroot);
		}else{
			return const_iterator(_c.begin(), _noroot);
		}
	}
	// want all keys p with |center-p|^2 < radius_squared
	// need to end at p-center < radius, p < radius+center
	const_iterator end(K  const* center=NULL, unsigned radius_squared=-1u) const{
		if(center && radius_squared!=-1u){
			unsigned radius = my_sqrt(radius_squared);
			auto i=_c.upper_bound(radius + *center);
			return const_iterator(i, _noroot);
		}else{
			return const_iterator(_c.end(), _noroot);
		}
	}
	base_iterator begin___A() const{
		return _c.begin();
	}
	base_iterator end___A() const{
		return _c.end();
	}
private: // leaf
	unsigned erase_(K const* a){ untested();
		return _c.erase(*a);
	}
protected:
	bool empty() const{
		return _c.empty();
	}
	val& get(K const* v){
		// if(v\notin _c) ++size...
		return _c[*v];
	}
public: // HACK
	base_iterator const& base(){return *this;}
private:
	container_type _c;
public:
	friend class nested_map<K, val, 2, C>;
}; // nm ==1

template<class T>
class center_t{
public:
	center_t(const center_t& o) : _val(o._val), _radius_sq(o._radius_sq)
	{ }
private:
	center_t(){ unreachable(); }
public:
	center_t(T const& t, unsigned r)
	  	: _val(t), _radius_sq(r)
	{
	}
	T _val;
	unsigned _radius_sq;
};

template<class K, class val, unsigned depth,
         template<class X, class ...> class C>
class iter_root_ : protected center_t<K const*>,
	protected nested_map<K, val, depth, C>::top_const_iterator { //
public:
	typedef center_t<K const*> center_type;
   typedef nested_map<K, val, depth, C> base_type;
   typedef nested_map<K, val, depth-1, C> l_type;
   typedef C<K, l_type> container_type;
   typedef typename C<K, l_type >::const_iterator container_iterator;
	typedef typename base_type::top_const_iterator base_iterator;
	typedef std::pair<unsigned, val> value_type;
public: // cons
	iter_root_(END_t, container_iterator const& e,
	           K const* center=NULL, unsigned radius_sq=-1u)
	    :	center_type(center, radius_sq),
	      base_iterator(_END, e, this),
	      _end(e)
	{
	}
public: // deep copy
	iter_root_(const iter_root_& o)
	    : center_type(o),
		   base_iterator(o, _child), // !
		   _end(o._end)
	{
	}
public:
	iter_root_& operator=(const iter_root_& o){
		center_type::operator=(o);
		_end=o._end;
		base_iterator::assign(o);
		return *this;
	}
	value_type operator*() const{
		return base_iterator::operator*();
	}
	template<class CC>
	void erase_(CC& c){
		auto x=base_iterator::erase_();

		if(x.second){
			c.erase(x.first);
		}else{
		}
	}
	bool operator==(const iter_root_& o) const {
		return base_iterator::operator==(o);
	}
	bool operator!=( const iter_root_& o) const {
		return !operator==(o);
	}
	iter_root_& operator++(){
		base_iterator::operator++();
		return *this;
	}
public: // iter_root
	// hmm, also in >1 iter. dedup?
	std::pair<container_iterator, container_iterator>
	help_construct(container_type const& c) {
		auto rs=radius_sq();
		if(c.empty()){
			// dummy.
			_end = c.end();
			return std::make_pair(c.begin(), c.end());
		}else if(rs!=-1u){
			unsigned radius = my_sqrt(rs);
			int lb=int(*center_type::_val) - int(radius);
			int ub=int(*center_type::_val) + int(radius);
			lb=std::max(0, lb);
			ub=std::min(255, ub);
		   _end = c.upper_bound(ub);
			auto beg=c.lower_bound(lb);

			return std::make_pair(beg, _end);
		}else{
			_end = c.end();
			return std::make_pair(c.begin(), c.end());
		}
	}

	explicit iter_root_(better_t, container_type const& c,
	                    K const* ccnt=NULL, unsigned rs=-1)
	    : center_type(ccnt, rs),
		   base_iterator(_TMP, this),
			_end(c.end())
	{
		NMassert(rs!=-1u || !ccnt);

		if(c.empty()){
			NMassert(_end==c.end());
		}else{
		}

		auto P=help_construct(c); // sets _end...
		bool done=base_iterator::assign_pair_range(P);

		if(!done){
			_end = P.first;
		}else{
		}
	}

#if 0
	template<class P>
	bool assign_pair_range(P& p){ untested();
		while(!is_empty(p)){ untested();
			bool done=base_iterator::assign_pair(p);
			if(done){ untested();
				return true;
			}else{ untested();
				++p.first;
			}
		}
		return false;
	}
#endif

public:
	template<class T>
	bool carry(T const& e)
	{
		if( is_end(e._base) ){
			return true;
		}else{
			return false;
		}
	}

	bool is_end() const {
		return base_iterator::is_end();
	}

	bool is_end(container_iterator const& t) const
	{
		if(t==_end){
			return true;
		}else{
			return false;
		}
	}

	K const* center() const{
		if( center_type::_val ) {
			return center_type::_val;
		}else{
			return NULL;
		}
	}
public: // root
	unsigned const& top_radius_sq() const{
		NMassert(uintptr_t(this)>3);
		return center_type::_radius_sq;
	}
	unsigned const& radius_sq() const{
		return center_type::_radius_sq;
	}
public:
	template<class E>
	iter_root_ end(E const&){
		return iter_root_(_END, _end);
	}
private:
   container_iterator _end;
}; // iter_root

#if 0
template<class K, class val>
class nested_map<K, val, 0> : public std::map<K, val> {
public:
	nested_map(){ unreachable(); }
};
#endif

template<class N>
std::pair<typename N::const_iterator, typename N::const_iterator>
make_range(N const& n)
{
	auto b=n.begin();
	auto e=b.end(n.end());
	return std::make_pair(b, e);
}

// close neighbours
template<class N, class C>
std::pair<typename N::const_iterator, typename N::const_iterator>
make_cn_range(N const& n, C const& center, unsigned r)
{
	auto b=n.begin(center.data(), r);
	auto e=b.end(n.end(center.data(), r)); // n.end(c.data(), r); // why?
	if(b==e){
	}else{
	}
	if(b==n.end(center.data(), r)){
	}else{
	}
	return std::make_pair(b, e);
}

} // nest
