#pragma once

#include "entity.hh"
#include "error.hh"
#include "mesh.hh"
#include <cmath> // BUG?
#include "discretefunction.hh"
#include "probe.hh"

template<unsigned dim>
class FemScheme;

class Mesh;

template<unsigned dim>
class MeshObject : public Entity{
public:
	MeshObject(std::string const& name) : Entity(name), _N(0) {
		_time[0] = 0.; // incomplete
		_time[1] = 0.; // incomplete
	}

public: // modelling
	void setModel(ModelBase const* m){
		_model = m;
	}
	void setN(unsigned n){
		_N = n;
	}
protected:
	template<class S>
	void new_mesh_function(S& s, std::string const &s2){
		return schemehack().new_mesh_function(s, long_label()+"."+s2, mesh());
	}
	template<class S>
	void new_stencil(S*& s,
			std::string const &hmm,
			std::string const &s1,
		  	std::string const &s2)
	{
		unreachable();
		return schemehack().new_stencil(s, hmm+long_label()+"."+s1, hmm+long_label()+"."+s2);
	}
	template<class S>
	void new_stencil(S*& s,
			std::string const &s1, std::string const &s2)
	{
		return schemehack().new_stencil(s, long_label()+"."+s1, long_label()+"."+s2);
	}
	// BUG. need entity
//	virtual void expand(FemScheme<2>& s) = 0;
public: // incomplete, probes maybe
  // FemScheme::
  double energy( const bool verbose = false ) const {
#ifndef NDEBUG
    incomplete();
#endif
    return 0; // incomplete. move to meshobject?
#if 0
    const auto& X = _solution.position();
    const auto& Y = _solution.curvature();

    auto const& M=X.mesh();
    unsigned N=M.N();
    double ret = 0;

    for( unsigned int e = 0; e < N-1; ++e ) {
	// find element geometry
	const auto xe = X.evaluate( e );
	const auto xep = X.evaluate( e+1 );
	const auto xd = xe - xep;
	const double q = xd.norm();

	// find curvature at point
	const auto ye = Y.evaluate( e );

	// find energy density
	const double s = 0.5 * ( M.u( e ) + M.u( e+1 ) );
	const double ed = model().energyDensity_at( xe, ye, s );

	ret += ed * q;
    }

    // extra energy from model
    ret += model().extraEnergy( X );

    message(bDEBUG, "energy: %f\n", ret);

    return ret;
#endif
  }

  double energy( std::ofstream& file, const bool verbose = false ) const
  { untested();
    double ret = energy( verbose );
#if 0
    incomplete();
    file << double(model_.timeProvider().time()) << ",";

    const auto& X = solutionTuple_.position();
    const auto& Y = solutionTuple_.curvature();

    const unsigned int N = X.mesh().N();
    double ret = 0;

    std::vector<double> energyTuple( model_.nCam+1,0.0);
    std::vector<double> list;

    for( unsigned int e = 0; e < N-1; ++e )
      { untested();
	// find element geometry
	const auto xe = X.evaluate( e );
	const auto xep = X.evaluate( e+1 );
	const auto xd = xe - xep;
	const double q = xd.norm();

	// find curvature at point
	const auto ye = Y.evaluate( e );

	// find energy density
	const double ed = model_.energyDensity( xe, ye, list );

	ret += ed * q;

	for( unsigned d = 0; d < list.size(); ++d ){ untested();
	  energyTuple.at(d) += list.at(d) * q;
        }
      }
#endif

#if 0 // not yet.
    for( auto e : model().energyVector() ){ untested();
      file << e << ",";
    }
#endif

    // extra energy from model. done already.
    // const auto& X = solutionTuple_.position();
    // const double extraEnergy = model_.extraEnergy( X );
    // ret += extraEnergy;
    // file << extraEnergy;

#if 0 // not yet.
    if( verbose )
      { untested();
	std::cout << "energy: " << ret << " ( ";
	for( auto e : model().energyVector() )
	  std::cout << " " << e << ",";
//	std::cout << " " << extraEnergy << " )" << std::endl;
      }
#endif

//    file << "," << updateSize();
//    file << std::endl;

    return ret;
  }
  // does not update anything.
  double updateSize() const
  {
//    incomplete();
    return 0;
#if 0
    const auto& X = solutionTuple_.position();
    const auto& Xold = oldSolutionTuple_.position();

    const unsigned int N = X.mesh().N();

    double ret = 0;
    for( unsigned int e = 0; e < N-1; ++e )
      { untested();
	// find element geometry
	const auto xe = X.evaluate( e );
	const auto xep = X.evaluate( e+1 );
	const auto xd = xe - xep;
	const double q = xd.norm();

	const auto xeOld = Xold.evaluate( e );
	const auto xepOld = Xold.evaluate( e+1 );

	ret += ( ( xeOld - xe ).norm2() + ( xepOld - xep ).norm2() ) * q;
      }

    return std::sqrt( ret );
#endif
  }

  std::pair<double,double> constraintError() const
  {
#ifndef NDEBUG
   // incomplete();
#endif

    return std::make_pair(0.,0.);
#if 0
    const auto& X = _solution; // _.position();
    const unsigned int N = X.mesh().N();

    std::pair< double, double > minMax = { 1e10, 0.0 };
    for( unsigned int e = 0; e < N-1; ++e ) {
	// find element geometry
	const auto xe = X.evaluate( e );
	const auto xep = X.evaluate( e+1 );
	const auto xd = xe - xep;
	const double q = xd.norm() / X.mesh().h( e+1 );

	minMax.first = std::min( minMax.first, q );
	minMax.second = std::max( minMax.second, q );
    }

    return minMax;
#endif
  }

public:
	ModelBase const& model() const{
		assert(_model);
		return *_model;
	}

	void tr_regress(){ untested();
		trace3("tr_regress", _time[0], _time[1], scheme().time());

		assert(_time[0] >= scheme().time()); // moving backwards
		assert(_time[1] <= scheme().time()); // but not too far backwards

		for (int i=2; i>0; --i) {
			assert(_time[i] < _time[i-1] || _time[i] == 0.);
		}
		trace2("meshob::tr_regress", _time[0], scheme().time());
		_time[0] = scheme().time();

	}
	void tr_begin(){
		std::fill(_time, _time+KEEP_TIMESTEPS, 0.);
	}
	void tr_restore(){ untested();
		std::fill(_time, _time+KEEP_TIMESTEPS, _time[0]);
	}
	void tr_advance(){
	  trace2("tr_advance", scheme().time(), _time[0]);
	  if(_time[0]==0.) {
		  untested();
	  }else if(_time[0]==scheme().time()){ untested();
		  trace2("BUG", _time[0], scheme().time());
		  unreachable();
	  }
	  // baseclass?
	  _time[2] = _time[1];
	  _time[1] = _time[0];

	  trace1("tr_advance1", scheme().time()); // BUG. scheme does not know the time.
	  _time[0] = scheme().time();
	}
	unsigned N() const { return mesh().N(); }
private:
	unsigned order() const{
		return 1;
	}
	double error_factor() const{
		// incomplete
		return 1.;
	}
protected:
//	ModelInterface<dim> const& model() const{
//		auto mm=&Entity::model();
//		assert(mm);
//		ModelInterface<dim> const* m=prechecked_cast<ModelInterface<dim> const* >(mm);
//		assert(m);
//		return *m;
//	}
//
public: // good idea?
	typedef PiecewiseLinearFunction< dim > PositionFunctionType;
	virtual PositionFunctionType const& position() const=0;
	typedef PiecewiseLinearFunction< dim > CurvatureFunctionType;
	// BUG? need ref here.
	virtual CurvatureFunctionType curvature() const=0;

protected: // from gnucap ELEMENT
	template<class T>
	double tr_review_trunc_error(const T* q) {
		static double fact[5]={1.,1.,2.,6.,24.};
		double timestep;
		if (_time[0] <= 0.) {
			// DC, I know nothing
			timestep = NEVER;
		}else{
			int error_deriv; // which derivative to use for error estimate
			if (order() >= KEEP_TIMESTEPS - 2) {
				error_deriv = KEEP_TIMESTEPS - 1;
			}else if (order() < 0) {untested();
				error_deriv = 1;
			}else{
				error_deriv = order()+1;
			}
			while (_time[error_deriv-1] <= 0.) {
				// not enough info to use that derivative, use a lower order derivative
				--error_deriv;
			}
			assert(error_deriv > 0);
			assert(error_deriv < KEEP_TIMESTEPS);
			for (int i=error_deriv; i>0; --i) {
				trace3("ed", i, _time[i], _time[i-1]);
				assert(_time[i] < _time[i-1]);
			}

			T c[1+error_deriv];
			for (int i=0; i<=error_deriv; ++i) {
				c[i] = q[i];
			}
			divdiff(c, error_deriv+1, _time);
			// now c[i] is i'th derivative, up to error_deriv

#if 0
			trace4(("ts " + long_label()).c_str(), error_deriv, error_factor(),
					OPT::trsteporder, OPT::trstepcoef[OPT::trsteporder] );
			trace5("time", _time[0], _time[1], _time[2], _time[3], _time[4]);
			trace5("charge", q[0].f0, q[1].f0, q[2].f0, q[3].f0, q[4].f0);
			trace5("deriv", c[0], c[1], c[2], c[3], c[4]);
#endif

			if (is_zero(c[error_deriv])) {
				// avoid divide by zero
				timestep = NEVER;
			}else{
				c[error_deriv] *= fact[error_deriv];
				double chargetol = std::max(CHGTOL,
						RELTOL * std::max(norm(q[0]), norm(q[1])));
				double tol = TRTOL * chargetol;
				double denom = error_factor() * norm(c[error_deriv]);
				assert(tol > 0.);
				assert(denom > 0.);
				switch (error_deriv) { // pow is slow.
					case 1:  timestep = tol / denom; break;
					case 2:  timestep = sqrt(tol / denom); break;
					case 3:  timestep = cbrt(tol / denom); break;
					default: timestep = pow((tol / denom), 1./(error_deriv)); break;
				}
				trace4("", chargetol, tol, denom, timestep);
			}
		}
		assert(timestep > 0.);
		return timestep;
	}
	double tr_review_check_and_convert(double timestep)
	{
		double time_future;
		if (timestep == NEVER) {
			time_future = NEVER;
		}else{
			if (timestep < DTMIN) {
				timestep = DTMIN;
			}else{
			}

			if (timestep < deltat() * TRREJECT) {
				if (_time[order()] == 0) {
					message(bTRACE, "initial step rejected:\n");
					message(bTRACE, "new=%g  old=%g  required=%g\n",
							timestep, deltat(), deltat() * TRREJECT);
				}else{
					message(bTRACE, "step rejected:\n");
					message(bTRACE, "new=%g  old=%g  required=%g\n",
							timestep, deltat(), deltat() * TRREJECT);
				}
				time_future = _time[1] + timestep;
				trace3("reject", timestep, deltat(), time_future);
			}else{
				time_future = _time[0] + timestep;
				trace3("accept", timestep, deltat(), time_future);
			}
		}
		assert(time_future > 0.);
		assert(time_future > _time[1]);
		return time_future;
	}

public:
	double deltat() const{
		trace2("deltat", _time[0], _time[1]);
		assert(! _time[0] || _time[1]!=_time[0] );
		return _time[0] - _time[1];
		// assert (_dt==_time[0] - _time[1]); ...
	}
	double time1() const{
		return _time[1];
	}
	double newtime() const{
		return _newtime;
	}
protected:
	// a hack. scheme must be global..?
public:
	virtual void expand(FemScheme<dim>& s){
		assert(_N);
		_mesh = new Mesh(0., 1., _N);
		_scheme = &s;
	}
	FemScheme<dim> const& scheme() const {
		assert(_scheme);
	  	return *_scheme;
	}
  FemScheme<dim>& schemehack() { /* TODO what not just call it scheme? */
		assert(_scheme);
	  	return *_scheme;
	}
protected:
	Mesh const& mesh() const {
		assert(_mesh);
	  	return *_mesh;
	}
private:
	ModelBase const* _model;
private: // meshobject?
	// yuck, nCams is part of the type?
	FemScheme<dim>* _scheme;

	Mesh* _mesh;
	unsigned _N;

public:
  // real hack
  virtual void initialiseXY() {
    throw MyException("entered base class by mistake");
  }
  virtual probe* new_probe(const std::string& ) {
    throw MyException("entered base class by mistake");
  }
  virtual void updateScalarCurvature() {
    throw MyException("entered base class by mistake");
  }
  virtual void updatePropFeedback() {
    throw MyException("entered base class by mistake");
  }
  virtual std::vector<double> getPropFeedback() {
    throw MyException("entered base class by mistake");
  }
  virtual void write_Kappa( const double ) {
    throw MyException("entered base class by mistake");
  }
  virtual void write_feedback( const double ) {
    throw MyException("entered base class by mistake");
  }
  virtual void set_deltaT( const double ) {
    throw MyException("entered base class by mistake");
  }
};
