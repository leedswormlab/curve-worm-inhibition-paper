#ifndef LIB_FUNCTION_HH
#define LIB_FUNCTION_HH

class SomeBase{
};

template< const unsigned int mydim >
class FunctionBase : public SomeBase {
protected:
	FunctionBase() : _label("unknown"){
	}
	FunctionBase( const FunctionBase& o): _label(o._label){
	}
public:
  virtual ~FunctionBase() { untested();
  }
  virtual void advance_time(){ untested();
  }
  virtual void load_rhs(){ untested();
  }

  std::string const& label() const{return _label;}
  void set_label(const std::string& l){ _label=l;}

private:
  std::string _label;
};

#endif // #ifndef LIB_FUNCTION_HH
