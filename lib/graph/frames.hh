#ifndef WORM_GRAPH_HH
#define WORM_GRAPH_HH
#include <iostream>
#include <set>
#include <boost/graph/properties.hpp>
#include <boost/container/flat_map.hpp>
#include <boost/functional/hash.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/adjacency_list.hpp>
#include "../image/annotations.hh"
#include "../image/freak.hh"
#include "../image/silhouette.hh"
//#include "../image/known_points.hh"
#include "../timer.hh"
#include "../error.hh"
#include "../bits/map.hh"
//#include "he.hh"

#define FGassert(x) assert(x)
#define USE_FREAK

namespace draft{

using framestuff::annotated_frame;
typedef std::array<uchar, 64> features_type;

unsigned feature_dist_sq(features_type const& f, features_type const& a)
{ untested();
	size_t ret=0;

	for(unsigned i=0; i<64; ++i){ untested();
		size_t delta=long(f[i])-long(a[i]);
		ret+=delta*delta;
		std::cout << ret << "\n";
	}
	return ret;
}

template<class S>
S& operator<<(S& s, features_type const& a){ untested();
	std::string space("");
	for(auto i=a.begin(); i!=a.end(); ++i){ untested();
		s << space << int(*i);
		space=" ";
	}
	return s;
}

template <typename T> int sgn(T val) { itested();
    return (T(0) < val) - (val < T(0));
}

typedef std::deque< cv::Mat > frame_deque_type;
typedef cv::Mat frame_type;
typedef cv::Point coord_type;
//typedef std::set<coord_type> annotation_type;

// represent a pixel on a frame
// this is expensive...
struct vertex_repr_type{
	vertex_repr_type() : _frame(NULL) { untested();
	}
	vertex_repr_type(coord_type const& c, frame_type const& f)
		: _frame(&f), _which(c){ itested();
	}
	vertex_repr_type(vertex_repr_type const& o)
		: _frame(o._frame), _which(o._which){ itested();
	}

public:
	frame_type const& frame()const{ untested();
		FGassert(_frame);
		return *_frame;
	}
	coord_type const& coord()const{ untested();
		FGassert(_which.x<99999);
		FGassert(_which.y<99999);
		return _which;
	}
private:
#ifndef NDEBUG
public:
#endif
	frame_type const* _frame;
	coord_type _which;
}; // vertex_repr_type

typedef vertex_repr_type pixel_address;
typedef std::array<uchar, 64> pixel_features;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::directedS,
		  boost::property<boost::vertex_color_t, long int,
#ifdef USE_FREAK
		  boost::property<boost::vertex_owner_t, pixel_features> >,
#else
		  boost::property<boost::vertex_owner_t, vertex_repr_type> >,
#endif
		  boost::property<boost::edge_weight_t, double>
			  > edgeweighted_graph_type1;

frame_type const& get_frame(framestuff::annotated_frame const& f)
{
	return f.frame();
}

typedef framestuff::annotated_frame::annotation_type annotation_type;
annotation_type const& get_annotation(framestuff::annotated_frame const& f)
{
	return f.annotation();
}

#ifdef USE_FREAK
class neigh_similarity1{
public:
	neigh_similarity1(frame_type const& f) { untested();
	}
};
#else
// maybe later
class neigh_similarity1 : public pixel_sim_base {
public:
	neigh_similarity1() : pixel_sim_base(){ untested();
	}
	neigh_similarity1(frame_type const& f)
		: pixel_sim_base(f) { untested();
	}
	neigh_similarity1(const neigh_similarity1& o)
		: pixel_sim_base(o) { untested();
	}
public: // yuck. base class?!
	unsigned distance_square( coord_type x, vertex_repr_type b, unsigned t=0) const{ itested();
		_f2 = &b.frame();
		return distance_square(x, b.coord(), t);
	}
	unsigned distance_square( vertex_repr_type a, vertex_repr_type b, unsigned t=0) const{ untested();
		_f1 = &a.frame();
		_f2 = &b.frame();
		return distance_square(a.coord(), b.coord(), t);
	}
private:
	unsigned distance_part( coord_type x, coord_type y, int select, int y1, int y2) const
	{ itested();

		int dist=10;
		unsigned ret=0;
		for(int i=-dist; i<=dist; ++i){ untested();
			if(!i) continue;
			int delta=get(*_f1, int(x.x)+i*select, int(x.x)+i*(1-select)) 
				     - get(*_f2, int(y.y)+y1*i, int(y.y)+y2*i);
			ret+=delta*delta;
		}
		return ret;
	}
public:
	unsigned distance_square( coord_type x, coord_type y, unsigned th=0 ) const{ itested();
		FGassert(_f1);
		FGassert(_f2);
		unsigned ret = get(*_f1, x.x, x.y) - get(*_f2, y.x, y.y);
		ret*=ret;
		if(ret>=th){ untested();
			return th;
		}

		unsigned ret0 = ret+distance_part(x,y,1,1,0);
		unsigned ret1;
		unsigned m1;
		if(ret0<th){ untested();
			ret1 = ret0 + distance_part(x,y,0,0,1);
			ret0 += distance_part(x,y,0,0,-1);
			m1 = std::min(ret0, ret1);
		}else{ untested();
			m1 = th;
		}

		unsigned m2;
		ret0 = ret+distance_part(x,y,1,-1,0);
	   if(ret0<th){ untested();
			ret1 = ret0 + distance_part(x,y,0,0,1);
		   ret0 =+ distance_part(x,y,0,0,-1);
			m2=std::min(ret0, ret1);
		}else{ untested();
			m2 = th;
		}

		unsigned m3;
		ret0 = ret+distance_part(x,y,1,0,1);
	   if(ret0<th){ untested();
			ret1=ret0 + distance_part(x,y,0,1,0);
		   ret0 =+ distance_part(x,y,0,-1,0);
			m3=std::min(ret0, ret1);
		}else{ untested();
			m3 = th;
		}

		unsigned m4;
		ret0 = ret+distance_part(x,y,1,0,-1);
	   if(ret0<th){ untested();
			ret1 = ret0 + distance_part(x,y,0,1,0);
		   ret0 =+ distance_part(x,y,0,-1,0);
			m4 = std::min(ret0, ret1);
		}else{ untested();
			m4 = th;
		}

		m1 = std::min(m1, m2);
		m3 = std::min(m3, m4);

		m1 = std::min(m1,m3);

//		trace1("dist_square", ret);
		return m1;
	}
};
#endif

// frames is a vector of frames...
template<class G, class W=void>
// probably should be a frames range...
// the first frame is the image-to-be-learnt
// frame_deque should be tagged frames...
class framesgraph {
public: // types
	typedef typename boost::graph_traits<G>::vertex_descriptor vertex_descriptor;
	typedef std::pair<vertex_descriptor, unsigned> vertex_with_distance;
#ifdef USE_FREAK
	typedef std::array<uchar, 64> features_type;
	typedef std::map<features_type, vertex_descriptor> lut_type;
#endif
	struct fgstats_type{
		unsigned _max_radius_sq;
		std::list<unsigned> _points_left;
		double _elapsed;
	};
	typedef nest::nested_map<uchar, vertex_descriptor, 64, boost::container::flat_map> nm_type;
	// typedef nest::nested_map<uchar, vertex_descriptor, 64> nm_type;
	struct target_edg_t{
		target_edg_t(){untested();
		}
		target_edg_t(unsigned from, unsigned to, double wt) : _src(from), _tgt(to), _wt(wt){}
		unsigned _src, _tgt;
		double _wt;
	};
public: // construct
	framesgraph(std::ifstream& s, double ds); // read from file

	// incomplete.
	framesgraph(frame_deque_type const& d, double dilate, float ps, double ds)
	    : _pattern_scale(ps), _nvt(0), _distance_scale(ds) { untested();

		FGassert(d.size()); // for now
		auto const& f1=d[0];

		unsigned wd=f1.cols;
		unsigned ht=f1.rows;
		unsigned np=ht*wd;
		_p2g.reserve((d.size()+1) * np);

		for(auto const& f : d){ untested();
			cv::Mat silfrm=f.clone();
			silhouetteImageDilate(f, silfrm, dilate, 200.);

			import_frame(f);
			trace2("imported a worm", ::boost::num_vertices(_g), ::boost::num_edges(_g));
		}

	}
	framesgraph() : _nvt(0) {}

	template<class Iter>
	framesgraph(Iter i, Iter e,
			unsigned thresh_collapse, unsigned thresh_edge,
			double ds,
			unsigned dilate_sil_radius,
			float pattern_scale)
	    : _pattern_scale(pattern_scale), _nvt(0),
	      _distance_scale(ds) { untested();
		s_timer tmr("framerange import");
		unsigned w=0;
		for(; i!=e; ++i){ untested();
			// message? trace1("importing", i->label());
			std::cout << "import frame #" << w << " " << i->label() << "\n";
			try{ untested();
				auto const& worm=get_frame(*i);
				cv::Mat silfrm=worm.clone();
				silhouetteImageDilate(worm, silfrm, dilate_sil_radius, 200);

				import_frame(worm, silfrm,
						thresh_collapse, thresh_edge,
						i->label(),
						&get_annotation(*i));
			}catch (exception_corrupt e){ untested();
				std::cerr << "skipping " << i->label() << ". " << e.what() << "\n";
			}
			trace3("imported", w, boost::num_vertices(_g), boost::num_edges(_g));
			++w;
		}
	}
public: // move
	framesgraph(const framesgraph&& f)
		: _g(std::move(f)),
#ifdef USE_NESTED_MAP
	     _feat_stash(std::move(f._feat_stash)),
#endif
	     _pattern_scale(f._pattern_scale),
	     _nvt(0),
	     _distance_scale(f._distance_scale)
	{ untested();
	}
	framesgraph& operator=(framesgraph&& f){
		_distance_scale=f._distance_scale;
		_g = std::move(f._g);
#ifdef USE_NESTED_MAP
		_feat_stash = std::move(f._feat_stash);
#endif
		_pattern_scale=f._pattern_scale;
		return *this;
	}

public:
	void clear(){
		_g.clear();
		_p2g.clear();
	}
	void load(std::ifstream& s);
	void dump(std::ofstream& o);

	// import standalone frame
	// without connecting to previous frames.
	void import_frame(frame_type const& frm, frame_type const& sil,
		unsigned thresh_collapse, unsigned thresh_edge,
		std::string const& label, annotation_type const* a);

//	void learn_frame(annotated_frame const& a);
	void learn_pixel(uchar const* d, int ann) {
		FREAKMAP::descriptor_type x;
		memcpy(x.data(), d, 64);
		learn_pixel(x, ann);
	}
	void learn_pixel(FREAKMAP::descriptor_type const& d, int ann);

	// import worm to detect
	void import_target(frame_type const& frm, frame_type const& sil,
			unsigned te, unsigned tc, double scf, float pxmm, bool internal_edges
			, fgstats_type&, double eucl_base);
public: // stats
	unsigned pot_edges(unsigned);
private:
	void check_consistency() const;
	vertex_descriptor add_vertex(FREAKMAP::descriptor_type const& d,
			unsigned ann=0){
		assert( ::boost::num_vertices(_g)==_feat_stash.recount());
	//	return add_vertex(d.data(), ann);
		vertex_descriptor nv=::boost::add_vertex(_g);
		set_desc(nv, d);
//		set_wt(nv, wt);
		::boost::get(::boost::vertex_color, _g, nv) = ann;
		assert( ::boost::num_vertices(_g)==_feat_stash.recount());
		return nv;
	}
	FREAKMAP::descriptor_type get_desc(vertex_descriptor v) const {
		return ::boost::get(::boost::vertex_owner, _g, v);
	}
#ifdef USE_NESTED_MAP
	void set_desc(vertex_descriptor v, const uchar* f, bool create=false) { untested();
		assert( ::boost::num_vertices(_g)==_feat_stash.recount());
		memcpy(::boost::get(::boost::vertex_owner, _g, v).data(), f, 64);
		auto d=::boost::get(::boost::vertex_owner, _g, v);
		set_desc(v, d, create);
		assert( ::boost::num_vertices(_g)==_feat_stash.recount());
	}
	void distort(FREAKMAP::descriptor_type& f){ untested();
		unsigned r=std::rand()%128;
		bool flag=r&1;
		r/=2;
		if(f[r]==255){ untested();
			--f[r];
		}else if(f[r]==0){ untested();
			++f[r];
		}else{ untested();
			f[r]+=flag;
		}
	}
	void set_desc(vertex_descriptor v, FREAKMAP::descriptor_type const& f) {
		::boost::get(::boost::vertex_owner, _g, v) = f;
		_feat_stash[f] = v;
		assert( ::boost::num_vertices(_g)==_feat_stash.recount());
	}
	void set_desc(vertex_descriptor v, FREAKMAP::descriptor_type& f, bool create=false) {
		::boost::get(::boost::vertex_owner, _g, v) = f;

		if(create) { untested();
			incomplete();
			while(_feat_stash.contains(f)){
				distort(f);
			}
			boost::get(boost::vertex_owner, _g, v) = f;
		}else{ untested();
		}
		_feat_stash[f] = v;

		assert( ::boost::num_vertices(_g)==_feat_stash.recount());
	}
#endif
	void scan_neighbourhood(std::vector<vertex_descriptor>& v,
	                        FREAKMAP::descriptor_type const& center,
	                        unsigned search_square, unsigned retry=0) const;
	template<class N>
	inline void reconnect( vertex_descriptor center, N const& neigh, int ann);
	template<class N>
	inline void connect( vertex_descriptor center, N const& neigh, int ann);

public:
	double distance_square_to_weight(double distance_sq) const;
private:

	unsigned update_edgwt(vertex_descriptor a, vertex_descriptor b, double wt);
/*--------------------------------------------------------------------------*/
	template<class K, class F>
	unsigned find_nn(const K& keypoints, F&, unsigned te);
	template<class K, class F>
	void find_te(const K& keypoints, F&, unsigned te, bool inte, fgstats_type&, double eucl_base);

public:
	// set initial guess on target frame
	template<class K>
	void put_guess(const cv::Mat& sil, K const& known, Vector & u,
			double initialguess_known, double initialguess_unknown) const{ untested();
		unsigned ht=sil.rows;
		unsigned wd=sil.cols;
		unsigned row=0;
		unsigned p=0;
		unsigned i=0;
		for(;row<wd; ++row ){ itested();
			unsigned col=0;
			for(;col<ht; ++col ){ itested();
				const uchar sil_color = sil.at<uchar>(row, col);
				if(!sil_color){ itested();
					continue;
				}else{ itested();
				}

//				trace2("putguess", i, p2g(i));

				if(known.contains( cv::Point( col, row) )) { untested();
					++p;
					u[p2g(i)] = initialguess_known;
				}else{ untested();
					u[p2g(i)] = initialguess_unknown;
				}

				++i;
			}
		}
		std::cout << i << " pixels in worm\n";
		std::cout << p << " pixels putguess\n";
//		display_grayscale_frame("result", sil);
		//display_silhouette_image("putguess", sil, u);
	}

	template<class R>
	void get_result(const cv::Mat& sil,
			Vector const& u, R& result) const{
		unsigned ht=sil.rows;
		unsigned wd=sil.cols;
		unsigned row = 0;
		unsigned i=0;
		// auto m=bbox(sil);
		for(;row<ht; ++row ){ itested();
			for(unsigned col=0 ;col<wd; ++col ){ itested();
				const uchar sil_color=sil.at<uchar>(row, col);

				if(!sil_color){ itested();
					continue;
				}else{ itested();
				}
				double x=u[p2g(i)];

				if(x>0){
					result[cv::Point(col, row)]; // BUG/fixme. it's a map.
				}else{ itested();
				}
				++i;
			}
		}
	}

	// BUG: use get_result
	void display_result(const cv::Mat& worm, const cv::Mat& sil,
			Vector const& u, std::string infix="") const{
		incomplete();
		unsigned ht=worm.rows;
		unsigned wd=worm.cols;
		unsigned row = 0;
		unsigned i=0;
		auto m=bbox(sil);
		cv::Mat wormline=worm.clone();
		for(;row<ht; ++row ){ itested();
			for(unsigned col=0 ;col<wd; ++col ){ itested();
				const uchar sil_color=sil.at<uchar>(row, col);

				if(!sil_color){itested();
					continue;
				}else{itested();
				}
//				trace1("silpt", i);
				double x=u[p2g(i)];

				if(x>0){itested();
					wormline.at<unsigned char>(row, col)=255;
				}else{itested();
					// hmm somehow this is the silhouette.
					wormline.at<unsigned char>(row, col)=40;
				}
				++i;
			}
		}
//		std::cout << i << " pixels in result worm\n";

		auto min=m.first;
		auto max=m.second;
		auto bb=wormline.colRange( min.x, max.x ).rowRange( min.y, max.y );

		// BUG.
		cv::imwrite( "result"+infix+".png", bb );
		// display_grayscale_frame("result", bb);
	}
	vertex_descriptor p2g(unsigned i)const{ itested();
#ifdef USE_NESTED_MAP
		size_t nv=::boost::num_vertices(_g);
		if(i>=_p2g.size()){ untested();
			
		}else if(_p2g[i] != nv + i){ itested();
		}else{
		}
#endif
		assert(i<_p2g.size());

		return _p2g[i];
	}
	void set_annotations(Vector & u, bool thresh=false) const{
		auto p=boost::vertices(_g);
		for(; p.first!=p.second; ++p.first){ itested();
			auto v=*p.first;
			int c=boost::get(boost::vertex_color, _g, v);
			if(c){ itested();
				// this is an annotated vertex. reset to what we know.
			  	u[v] = c; // sgn(c);
			}else if(thresh){ itested();
				// do threshholding, unless in first iteration
				u[v] = sgn(u[v]);
			}
		}
		// bug/feature. what about the target vertices?!
	}
private:
	mutable std::vector<vertex_descriptor> _p2g; // obsolete!
	std::deque<vertex_with_distance> _edgq;
#if 0
private: // stuff
	add_annotated_frame(){ untested();
		std::cout << "converted " << frames.at(cam).type() << "\n";
		std::cout << "stored frame " << frame.type() << "\n";
		std::cout << "more on frame " << frame.rows << "\n";
		std::cout << "more on frame " << frame.cols << "\n";
		std::cout << "more on frame " << frame.size << "\n";

		std::cout << "gray frame rows " << frame.rows << "\n";
		std::cout << "gray frame cols " << frame.cols << "\n";
		std::cout << "type cam frame " << misc::type2str(frame.type()) << "\n";
		std::cout << "type of gray frame " << misc::type2str(frames.at(cam).type()) << "\n";
	}
#endif

public:
	void set_distance_scale(double x){_distance_scale=x;}
	float const& pattern_scale() const{
		return _pattern_scale;
	}
	G const& graph() const{
		return _g;
	}
	size_t num_edges() const{
		return ::boost::num_edges(_g)
#ifdef USE_NESTED_MAP
			+ _target_edgs.size()
#endif
			;
	}
	size_t num_vertices() const{
		return ::boost::num_vertices(_g) + _nvt;
	}
	size_t num_all_vertices() const{
		return ::boost::num_vertices(_g) + _nvt;
	}
	typedef  std::vector<target_edg_t> target_edg_container;
	target_edg_container const& target_edges() const{return _target_edgs;}
	size_t num_target_vertices() const{return _nvt;}
private:
	nm_type _feat_stash;
	std::vector<target_edg_t> _target_edgs;

   G _g;
//	double _dilate_radius;
	float _pattern_scale;
	unsigned _nvt;
	double _distance_scale;
}; // framesgraph
/*--------------------------------------------------------------------------*/
} // draft
/*--------------------------------------------------------------------------*/
using draft::pixel_features;
/*--------------------------------------------------------------------------*/
namespace boost{
template<class A, class B>
size_t num_vertices(draft::framesgraph<A,B> const& g)
{ itested();
	return g.num_vertices();
}
template<class A, class B>
size_t num_edges(draft::framesgraph<A,B> const& g)
{
	return g.num_edges();
}
}
/*--------------------------------------------------------------------------*/
namespace draft{
/*--------------------------------------------------------------------------*/
template<class G, class W>
inline unsigned framesgraph<G, W>::update_edgwt(
		vertex_descriptor a, vertex_descriptor b, double wt)
{
	assert(a!=b);
	if(a<b){
		std::swap(b, a);
	}else{
	}

	auto e=::boost::edge(a, b, _g);
	if(e.second){ itested();
		message(bTRACE, "update: not adding edge %d %d %f\n", a, b, wt);
		// there's already an edge.
		wt += ::boost::get(::boost::edge_weight, _g, e.first);
		wt/=2.;
		::boost::put(::boost::edge_weight, _g, e.first, wt);
	}else{ untested();
		message(bTRACE, "update: adding edge %d %d %f\n", a, b, wt);
		// there should really be an edge here.
		e.first=::boost::add_edge(a, b, _g).first;
		::boost::put(::boost::edge_weight, _g, e.first, wt);
	}

	untested();

	// report back, whether new edge has been added.
	return !e.second;
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
inline double framesgraph<G, W>::distance_square_to_weight(double distance_sq) const
{
	assert(_distance_scale);
//	message(bTRACE, "dsw %f\n", _distance_scale);
	double norm=sqrt(distance_sq) * _distance_scale;
	double w=exp(-norm); // exponential?
//	message(bTRACE, "dsw ds %f norm %f w %f\n", distance_sq, norm, w);
	return w;
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
template<class N>
inline void framesgraph<G, W>::connect(
		vertex_descriptor center, N const& neigh, int ann)
{
//	assert(boost::degree(center, _g)==0);
	message(bTRACE, "fg connect center %d, %d neighs\n", center, neigh.size());
	// center should already be connected to neigh.
	// but the center feature vector has moved.
	//
	double wtsum=0.;
	unsigned total_edg=0;
	unsigned new_edg=0;

	auto center_feat=::boost::get(::boost::vertex_owner, _g, center);
	for(auto const& n : neigh){
		++total_edg;
		if(n==center){
		}else if(std::rand() & 1){ itested();
			// inefficient.
			auto nf=get_desc(n);
			auto color=boost::get(::boost::vertex_color, _g, n);

			auto ds=FREAKMAP::distance_square(center_feat, nf);
			double wt=distance_square_to_weight(ds);
			wtsum += wt;

			if(int(color)*int(ann)<0){ untested();
			}else if(wt<50000){
				new_edg += update_edgwt(center, n, wt);
			}else{
			}
		}else{
		}
	}
	message(bDEBUG, "changed edgewt net %f. new edges %d/%d\n", wtsum, new_edg, total_edg);
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
template<class N>
inline void framesgraph<G, W>::reconnect(
		vertex_descriptor center, N const& neigh, int ann)
{ untested();
	message(bTRACE, "fg reconnect center %d, %d neighs\n", center, neigh.size());
	check_consistency();
	// center should already be connected to neigh.
	// but the center feature vector has moved.
	//
	double wtsum=0.;
	unsigned total_edg=0;
	unsigned new_edg=0;

	auto center_feat=::boost::get(::boost::vertex_owner, _g, center);
	for(auto const& n : neigh){
		++total_edg;
		if(n==center){ untested();
		}else if(std::rand() & 1){ itested();
			// inefficient.
			auto color=boost::get(::boost::vertex_color, _g, n);
			auto nf=get_desc(n);
			auto ds=FREAKMAP::distance_square(center_feat, nf);
			double wt=distance_square_to_weight(ds);
			wtsum += wt;
			if(int(color)*int(ann)<0){ untested();
				new_edg += update_edgwt(center, n, wt);
			}
		}else{ untested();
		}
	}
	message(bDEBUG, "changed edgewt net %f. new edges %d/%d\n", wtsum, new_edg, total_edg);
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
inline void framesgraph<G, W>::scan_neighbourhood(
	 std::vector<vertex_descriptor>& neighs,
    FREAKMAP::descriptor_type const& center,
    unsigned search_square, unsigned retry /*=0*/) const
{
	assert(_distance_scale);
	neighs.clear();
	auto p=nest::make_cn_range(_feat_stash, center, search_square);

	while(p.first==p.second && retry){
		search_square*=2;
		p=nest::make_cn_range(_feat_stash, center, search_square);
		--retry;
	}

	// yuck. why?! return pair...
	for(; p.first!=p.second; ++p.first){
		neighs.push_back((*p.first).second);
	}

	message(bTRACE, "found %d in neighbourhood\n", neighs.size());
	// std::sort(v);
	// return v;
}
/*--------------------------------------------------------------------------*/
static FREAKMAP::descriptor_type avg(
		FREAKMAP::descriptor_type const& a,
		FREAKMAP::descriptor_type const& b)
{
	FREAKMAP::descriptor_type r;
	for(unsigned i=0; i<r.size(); ++i){
		unsigned s=unsigned(a[i])+unsigned(b[i]);
		s/=2;
		assert(s<256);
		r[i] = s;
	}
	return r;
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
inline void framesgraph<G, W>::check_consistency() const
{
	assert( ::boost::num_vertices(_g)==_feat_stash.recount());
#ifndef NDEBUG
	auto V=boost::vertices(_g);
	for(;V.first!=V.second; ++V.first){
		assert(_feat_stash.contains(get_desc(*V.first)));
	}
#endif
}
/*--------------------------------------------------------------------------*/
// pixel_learner::
template<class G, class W>
inline void framesgraph<G, W>::learn_pixel(
	  	FREAKMAP::descriptor_type const& d, int ann)
{
	check_consistency();
	assert(_distance_scale);
#ifdef USE_NESTED_MAP
	unsigned learn_thresh=40000;
	unsigned learn_retry=3;

	auto p=nest::make_cn_range(_feat_stash, d, 1);
	if(p.first!=p.second){
		assert(_feat_stash.contains(d));
		// its already there...
		vertex_descriptor nv=(*p.first).second;
		auto& color=boost::get(::boost::vertex_color, _g, nv);
		if(int(color)*int(ann)<0){
			message(bTRACE, "learn_pixel, already there, wrong, have %d need %d\n", int(color), ann);
			// the bad case. contradictive annotation.
			// need to figure out edges, how?
			int oldcolor=color;
			assert(color>-99999);
			color = ann;
			{ // force_add?!
				nv = add_vertex(d);
				auto dd=d;
				set_desc(nv, dd, true);
			}
			boost::get(::boost::vertex_color, _g, nv) = oldcolor;

			assert( ::boost::num_vertices(_g)==_feat_stash.recount());

		}else{
			auto deg=0; // ::boost::degree(nv, _g);
			message(bTRACE, "learn_pixel, %d already there, degree %d, color %d, reinforce %d\n", nv, deg, color, ann);
			// good idea?
			// probably just more edges?
			// need more edges7
		//	color += double(ann)*.5;
			color += ann;
		}

	}else{ // try neighbours
		assert(!_feat_stash.contains(d));
		++p.first;
		assert(p.first==p.second);

		std::vector<vertex_descriptor> neigh;
		scan_neighbourhood(neigh, d, learn_thresh, learn_retry);

		vertex_descriptor nv;
		if(neigh.empty()){
			message(bTRACE, "learn_pixel, not there, no neighbours\n");
			// nothing nearby. create new...?
			// better: extend/shift?
			// repurpose a random vertex?
			nv = add_vertex(d);
			set_desc(nv, d);
			auto& color=boost::get(::boost::vertex_color, _g, nv);
			color=ann;

			// connect(nv); // perhaps not needed?
//		}else if(neigh.size()==1){
			
		}else{ untested();
			message(bTRACE, "learn_pixel, not there, but has close neighs\n");
			assert( ::boost::num_vertices(_g)==_feat_stash.recount());
			// found something close...
			// replace?

			unsigned r=std::rand()%(neigh.size()+1);
			unsigned color=0;
			if(r==neigh.size()){ untested();
				//add new.
				nv = add_vertex(d, ann);
				connect(nv, neigh, ann);
			}else{ itested();
				// try hijack
				check_consistency();
				nv = neigh[r];
				assert(_feat_stash.contains(get_desc(nv)));
				color = boost::get(::boost::vertex_color, _g, nv);

				if(int(color)*int(ann)<0){ untested();
					// one of the neighbours. has wrong color.
					// refine.
					nv = add_vertex(d,ann);
					connect(nv, neigh, ann);
					// nv = add_vertex(d, ann);
					// connect(nv, neigh, ann);
				}else{ itested();
					check_consistency();
					// has the right color, move closer.
					// assert(_feat_stash.contains(get_desc(nv)));
					// assert( ::boost::num_vertices(_g)==_feat_stash.recount()+1);

					auto new_feat=avg(get_desc(nv), d);
					if(_feat_stash.contains(new_feat)){ untested();
						// already there.. hmmm
						incomplete();
					}else{ itested();
						check_consistency();
						int num_erased = _feat_stash.erase(get_desc(nv));
						assert(num_erased==1); (void) num_erased;
						// _feat_stash[new_feat]=nv;
						set_desc(nv, new_feat); // sets both.
						check_consistency();
						reconnect(nv, neigh, ann);
					}
					check_consistency();
					// update nv?
					// color+=ann;
					// boost::get(::boost::vertex_color, _g, nv)=color;
				}
			}
		}
	}

#endif
	assert( ::boost::num_vertices(_g)==_feat_stash.recount());
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
inline void framesgraph<G, W>::import_frame(
		frame_type const & frm, frame_type const& silfrm,
		unsigned thresh_collapse, unsigned thresh_edge, // double similarity_sigma,
	  	std::string const& label,
		annotation_type const* ann)
{
//	display_grayscale_frame("importworm", frm);
	message(bDEBUG, "import_frame te/tc %d/%d ds %f\n",
			thresh_edge, thresh_collapse, _distance_scale);

	std::vector<cv::KeyPoint> keypoints;

	//	std::cout << "import_frame nonzero " << countNonZero(silfrm) << (ann?" *":"") << "\n";
	// display_grayscale_frame("import frame", silfrm);
	// rearrange(frm);

	unsigned vertices_before_import=::boost::num_vertices(_g);
	unsigned start=::boost::num_vertices(_g);

	// introduces a lot of edges...
#ifdef FULL_CLIQUE
	start=0;
#endif

	if(!ann){
		message(bDEBUG, "import_frame, unannotated\n");
		// HACK
		// import final image, connect to all.
	  	start = 0;
	}
	unsigned row=0;

	assert(silfrm.rows == frm.rows);
	assert(silfrm.cols == frm.cols);

#ifndef USE_FREAK
	unsigned ht=silfrm.rows;
	unsigned wd=silfrm.cols;
	W myweight(frm);
#else
	keypoints.clear();
	fill_keypoints(keypoints, silfrm);
	// visit all keypoints, create descriptor matrix...
	FREAKMAP myfreak(keypoints, frm, _pattern_scale);

	unsigned size=keypoints.size();

	if(size!=keypoints.size()){ untested();
		// FREAK removes keypoints to close to the border
		// need to extend the scene...
		unreachable(); incomplete();
		// currently, we lose a few annotated pixels...
		std::cerr << "keypoints gone?! b4 " << size << " now " << keypoints.size() << "\n";
		std::cerr << "pattern_scale is " << _pattern_scale << "\n";
	}else{
		message(bDEBUG, "have %d keypoints\n", size);
	}
#endif

	// go through pixels in that frame
	// TODO:linearize
	// display_grayscale_frame("silhouette", silfrm);
	// display_grayscale_frame("worm", frm);
	double totalwt=0.;
	unsigned collapse=0.;
	unsigned wormpixels=0;
	unsigned annotated=0;
#ifndef USE_FREAK
	unsigned ne_before=::boost::num_edges(*this);
	for(;row<wd; ++row ){ untested();
		for(unsigned col=0; col<ht; ++col ){ untested();
			const uchar sil_color = silfrm.at<uchar>(row, col);

			if(!sil_color){ untested();
				continue;
			}else{ untested();
				++wormpixels;
			}
#else // USE_FREAK
	wormpixels = keypoints.size();

	// keypoints are exactly the silhouette points.
	// now compare to all known points in the graph.
	unsigned newedgs=0;
	for(auto const& k : keypoints){ itested();
		   row = k.pt.y;
		   int col=k.pt.x;
#endif
		   coord_type curpix(col, row);
			if(!ann){ itested();
			}else if(ann->contains(curpix)){ untested();
			}else{ untested();
			}

			// check if we know that pixel type
			_edgq.clear();
			bool foundit=false;
			unsigned key_vertex; // vertex corresponding to keypoint

			// check if we need a new node or not.
			// visit all nodes...
			for(unsigned pk=start; pk<::boost::num_vertices(*this); ++pk){ itested();
				// abuse pk as vertex_descriptor.
				// get a pixel corresponding to vertex
				auto repr=::boost::get(::boost::vertex_owner, _g, pk);

#ifndef USE_FREAK
				FGassert(repr._frame);
				auto distance=myweight.distance_square(curpix, repr, thresh_edge);
#else
				auto distance=myfreak.distance_square(k, repr.data(), thresh_edge);
#endif

				if(!ann || distance>=thresh_collapse){ itested();
					// they are far away, don't collapse.
					// stash weighted node, just in case we need it.

					if(distance<thresh_edge){ itested();
						vertex_with_distance v(pk, distance);
						// trace1("dist", distance);
						_edgq.push_back(v);
					}else{ itested();
					}
				}else{ itested();
					++collapse;
					foundit = true;
					// only need to map pixel to the node we found.
					if(!ann){ untested();
						unreachable();
						_p2g.push_back(pk);
					}else{ itested();
					}
					key_vertex = pk;
					break; // continue outer loop.
				}
			}

			if(foundit){ itested();
				// there was already a vertex/pixel similar to this one.
				if(!ann){ untested();
					// hack...
#ifdef USE_FREAK
					auto f=myfreak.get_features(k);
					memcpy( ::boost::get(::boost::vertex_owner, _g, key_vertex).data() ,
							f, 64);
#else
					::boost::get(::boost::vertex_owner, _g, key_vertex)=
						vertex_repr_type( curpix, frm );
#endif
				}else{ itested();
					// first pixel(s) in annotaded frame?
				}

			}else{ itested();
				// new node, with weigted edges
				// need to connect the edges that have been collected
				auto nv=boost::add_vertex(_g);
//				trace1("added vert", nv);
				key_vertex = nv;
				if(!ann){ itested();
					// target. keep track of vertices
					_p2g.push_back(nv);
				}else{ itested();
//					trace3("addv", nv, curpix.x, curpix.y);
				}
				//trace3("new node", nv, curpix.first, curpix.second);

#ifndef USE_FREAK
				boost::get(boost::vertex_owner, _g, nv) = vertex_repr_type( curpix, frm );
				FGassert(boost::get(boost::vertex_owner, _g, nv)._frame);
#else
				auto f=myfreak.get_features(k);


				auto& prop=::boost::get(::boost::vertex_owner, _g, nv);
				memcpy( prop.data(), f, 64);


#ifdef USE_NESTED_MAP
				// _feat_stash[prop] = nv;
#endif
#endif

				// trace2("edg node", e.first, e.second);
//				std::cout << "IMPORT " << nv << " found " << _edgq.size() << " edges\n";
				for(auto e : _edgq){ itested();
					auto newedge=::boost::add_edge(nv, e.first, _g).first;
					double norm=sqrt(_distance_scale*e.second);
					double wt=exp(-norm); // exponential?
					::boost::put(::boost::edge_weight, _g, newedge, wt);
					totalwt+=wt;
					// trace3("", e.second, wt, totalwt);
				}
			}
		
		   // coord_type curpix(col, row);
			if(!ann){ itested();
				// no annotation...
			}else if(ann->contains(curpix)){ untested();
//				trace3("ann", curpix.x, curpix.y, key_vertex);
				// annotated pixel
				++::boost::get(::boost::vertex_color, _g, key_vertex);
				++annotated;
			}else{ itested();
				--::boost::get(::boost::vertex_color, _g, key_vertex);
			}

		newedgs+=_edgq.size();
#ifndef USE_FREAK
		} // col loop
	} // row loop
#else
	}// keypoint loop
#endif

//	display_sil_bb("learnsil", silfrm);
//	std::cout << "learn silouette nonzero " << countNonZero(silfrm) << "\n";

	FGassert(wormpixels);
	message(bLOG, "imported " + label + " with " + std::to_string(wormpixels) + "px\n");
	if(!ann){
//		std::cout << "imported target worm with " << wormpixels << "px\n";
		message(bLOG, "%d new edges\n", newedgs);
		//std::cout << ::boost::num_edges(_g)-ne_before << " new edges\n";
	}else if((100*annotated)/ann->size() < 10){ untested();
		std::cerr << "too few annotated " << annotated << "/"
			       << ann->size() << " worm: " << wormpixels << "\n";
		throw exception_corrupt("annotation " + std::to_string(annotated));

		while(::boost::num_vertices(_g) > vertices_before_import){ untested();
			::boost::remove_vertex(::boost::num_vertices(_g)-1, _g);
		}


	}else { untested();
		FGassert(annotated <= ann->size());

		std::cout << annotated << " out of "  << ann->size() << " annotated\n";
		std::cout << "edges/edgewt " << newedgs << "/" << totalwt << "\n";
		std::cout << "collapses " << collapse << "\n";
//		if(trace>X)
		std::cout << "v/e " << ::boost::num_vertices(_g) << "/"
		          << ::boost::num_edges(_g) << "\n";
	}
} // import_frame
/*--------------------------------------------------------------------------*/
template<class A, class B>
void framesgraph<A,B>::dump(std::ofstream& o)
{
	check_consistency();
	auto nv=::boost::num_vertices(_g);
	message(bLOG, "dumping framesgraph with %d vertices\n", nv);
	assert(nv==_feat_stash.recount());

	o << "FG0 " << ::boost::num_vertices(_g) << " " << ::boost::num_edges(_g) << "\n";

	auto V=::boost::vertices(_g);
	for(; V.first!=V.second; ++V.first){
		vertex_descriptor v=*V.first;
		o << long(v);
		int c=::boost::get(::boost::vertex_color, _g, v);
		o << " " << long(c);
		auto repr=::boost::get(::boost::vertex_owner, _g, v);
		for(auto i=repr.begin(); i!=repr.end(); ++i){ itested();
			o << " " << int(*i);
		}
		o << "\n";
	}
	o << "FREAK0 " << _pattern_scale << "\n"; // number octaves..? orient?

	auto b=::boost::edges(_g);
	for(; b.first!=b.second; ++b.first){
		auto s=::boost::source(*b.first, _g);
		auto t=::boost::target(*b.first, _g);
		auto EWM=::boost::get(::boost::edge_weight, _g);
		double w=::boost::get(EWM, *b.first);

		if (s<t){ untested();
			o << long(s) << " " << long(t) << " " << w << "\n";
		}else{ itested();
			o << long(t) << " " << long(s) << " " << w << "\n";
		}

	}
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
framesgraph<G, W>::framesgraph(std::ifstream& s, double ds)
	: _g(),
	  _pattern_scale(-1u), _nvt(0),
	  _distance_scale(ds) // pick from file...??
	  //_similarity_sigma(4.)
{
	assert(_distance_scale);
	load(s);
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
void framesgraph<G, W>::load(std::ifstream& s)
{ itested();

	std::string label;

	while(s.peek()=='\n' || s.peek()=='#'){ itested();
		// how to ignore a full line?
		s.ignore(4096, '\n');
	}

	s >> label;
	if(label=="FG0"){
	}else{ untested();
		throw exception_corrupt("wrong label '" + label + "'");
	}
	unsigned nv, ne;
	s >> nv >> ne;
	vertex_descriptor v;
	int color;
	for(unsigned i=0; i<nv; ++i){ itested();
		s >> v;
		FGassert(v==i);
		vertex_descriptor nn=::boost::add_vertex(_g);
		FGassert(v==nn); (void)nn;

		s >> color;
		if(color<-99999 || color > 999999){
			throw(exception_invalid("color " + std::to_string(color) + "\n"));
		}
		::boost::get(::boost::vertex_color, _g, v) = color;

		auto& feat=::boost::get(::boost::vertex_owner, _g, v);

		for(unsigned j=0; j<64; ++j){ itested();
			unsigned number;
			s >> number;
			feat[j] = number;
		}

#ifdef USE_NESTED_MAP
		_feat_stash[feat] = i;
#endif

	}
	s >> label;
	if(label!="FREAK0"){ untested();
		throw exception_corrupt("reading FGdump, missing FREAK0 edges");
	}else{
	}
	s >> _pattern_scale;
	trace2("parse found", _pattern_scale, ne);

	auto EWM=::boost::get(::boost::edge_weight, _g);
	for(unsigned i=0; i<ne; ++i){ itested();
		vertex_descriptor a, b;
		double w;
		s >> a >> b >> w;
		//trace3("edgparse", a, b, w);

		if(s.eof()){ untested();
			throw exception_corrupt("reading FGdump, wrong size");
		}else{
		}

		if(a>b){ itested();
			auto e=::boost::add_edge(a, b, _g).first;
			::boost::get(EWM, e) = w;
		}else{ itested();
			auto e=::boost::add_edge(b, a, _g).first;
			::boost::get(EWM, e) = w;
		}
	}
	s >> label;

	if(!s.eof()){ untested();
		throw exception_corrupt("reading FGdump, wrong size");
	}else{
	}
	nv=::boost::num_vertices(_g);
	ne=::boost::num_edges(_g);

	message(bLOG, "read FG %d %d %f\n", nv, ne, _pattern_scale);

#if 0
#ifndef NDEBUG
#ifdef USE_NESTED_MAP
	auto x=nest::make_range(_feat_stash);
	auto cnt=0u;
	for(;x.first!=x.second;++x.first){ itested();
		++cnt;
	}
	if(cnt!=nv){ untested();
		throw exception_corrupt("reading FGdump, wrong vertexcount " +
				std::to_string(cnt) + " " + std::to_string(nv));
	}else{ untested();
	}
#endif
#endif
#endif
	check_consistency();
}
/*--------------------------------------------------------------------------*/
unsigned feature_distance_squared(
		pixel_features const& a, pixel_features const& b)
{
	unsigned ret=0;
	for(unsigned i=0; i<64; ++i){
		int x=a[i];
		x -= int(b[i]);
		x *= x;
		ret += x;
	}
	return ret;
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
unsigned framesgraph<G, W>::pot_edges(unsigned radius)
{
	unsigned cnt=0;
	radius*=radius;
	auto r=boost::vertices(_g);
	auto next=r.first;
	for(;r.first!=r.second; r.first=next){
		auto a=::boost::get(::boost::vertex_owner, _g, *r.first);
		++next;
		for(auto other=next ; other!=r.second; ++other){
			auto b=::boost::get(::boost::vertex_owner, _g, *other);

			auto ds=feature_distance_squared(a, b);
			message(bTRACE, "%d, %d, %d\n", ds, radius, cnt);
			if(ds<radius){
				++cnt;
			}else{
			}
		}
	}
	return cnt;
}
/*--------------------------------------------------------------------------*/
} // draft
/*--------------------------------------------------------------------------*/
template<class FG>
void make_framesgraph_from_indexfile(FG& arg, ConfigParameters const& parameters)
{ untested();
	unsigned dilate_sil_radius=parameters.get<unsigned>("dilate-sil");

	// not yet
// 	unsigned restrict_sil=parameters.get<unsigned>("restrict_sil");

	unsigned pattern_scale=parameters.get<unsigned>("pattern-scale");
	unsigned thresh_collapse=parameters.get<unsigned>("thresh-collapse");
	unsigned thresh_edge=parameters.get<unsigned>("thresh-edge");
	double distance_scale=parameters.get<double>("distance-scale");
	std::string indexfilename=parameters.get<std::string>("headclips-index");

	AFD<> afd(indexfilename);

	//std::cout << "read " << afd.size() << " frames. "
	//	<< corrupt << " corrupt, "
	//	<< nofile << " not there. dilate radius"
	//	<< dilate_sil_radius << "\n";

	FG fg(afd.begin(), afd.end(),
			thresh_collapse, thresh_edge, distance_scale,
			dilate_sil_radius, pattern_scale);
	arg = std::move(fg);
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
#endif
