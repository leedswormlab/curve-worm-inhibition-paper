#ifndef GRAPHLEARN_HH
#define GRAPHLEARN_HH

#include "../image/annotations.hh"
#include "../image/freak.hh"
#include "../image/display.hh"
#include "frames.hh"

// select subset of silhouette close to a
template<class K>
static void align_silhouette(cv::Mat& s, K const& a)
{ untested();
	unsigned wd=s.cols;
	unsigned ht=s.rows;

	cv::Mat annmask(wd, ht, CV_8UC1);
//	display_grayscale_frame("s", s);

	for(auto i: a){ untested();
		int row=i.first.y;
		int col=i.first.x;
		annmask.at<uchar>(row, col)=255;
	}

	////display_grayscale_frame("m", annmask);

	unsigned radius=30; // just to be sure.
   cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
	 cv::Size(radius*2+1, radius*2+1), cv::Point(radius, radius) );

   cv::Mat dilated;
   cv::dilate(annmask, dilated, element);
   annmask = dilated;
	//display_grayscale_frame("annmask", annmask);

	cv::bitwise_and(annmask, s, s);
	//display_grayscale_frame("done", s);
}

template<class FGPF>
void learn_frame(FGPF& fgpf, draft::annotated_frame const& a)
{
	KnownListType detectlist;

	auto& g=fgpf.fg();

	// need annotated_frame::get_silhouette()?
	//FREAKMAP myfreak(a, g.pattern_scale());
	std::vector<cv::KeyPoint> keypoints;
	cv::Mat sil=a.frame().clone();
	silhouetteImageDilate(a.frame(), sil, fgpf.dilate_tgt_sil(), 200);

	//display_sil_bb("siluncropped", sil);
	align_silhouette(sil, a.annotation());
	//display_sil_bb("silcropped", sil);

	fill_keypoints(keypoints, sil);

	// visit all keypoints, create descriptor matrix...
	FREAKMAP myfreak(keypoints, a.frame(), fgpf.pattern_scale());
	fgpf.find_known_points(sil, a.frame(), detectlist, a.ppmm());

	message(bDEBUG, "learning %i keypoints\n", keypoints.size());

	unsigned cnt=0;
	for(auto const& keypoint: keypoints){
		auto feat=myfreak.get_features(keypoint);
		int ann=0;
		bool annotated=a.is_annotated(keypoint.pt);
		bool detected=detectlist.find(keypoint.pt)!=detectlist.end();
		if (annotated == detected){ itested();
			continue;
		}else{
			ann = annotated;
			ann -= detected;
		}
		++cnt;

		g.learn_pixel(feat, ann);
	}
	message(bDEBUG, "relearnt %i keypoints\n", cnt);

//	for(auto const& keypoint: keypoints){
//		// add more edges...
//	}

}

#endif
