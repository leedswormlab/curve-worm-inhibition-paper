#pragma once
#include "../matrix_graph.hh"

namespace draft{

// solve a "heat equation" time step on a graph.
template<class G>
class HEsolver{
public:

	HEsolver(G const& g, double deltat, double ik=0, double iu=0, std::string label="")
		: _m(g, UmfpackMatrix::_NORMALIZED_LAPLACIAN, deltat),
		   _g(g),
	     _label(label), _ik(ik), _iu(iu), _numberofsteps(3), _u(_m.n())
	{
	}

	template<class K=ANNOTATION>
	void do_it(cv::Mat const& sil, K const* guessknown=NULL) {

		_sil=sil; // hmm cleanup later.

		unsigned nv=_m.n();
		unsigned ne=_m.e();

		if(guessknown){ untested();
			_g.put_guess(sil, *guessknown, _u, _ik, _iu);
		}else{
		}
		// display_silhouette_image("ann", sil, u); // ???!
		impl::UMF_LU lu;
		impl::UMF_LU lu2;
		lu = std::move(lu2);
		Vector x(nv);
		try{
			impl::UMF_LU t(_m);
			lu = std::move(t);
		}catch(UmfpackError e){ untested();
			message(bDANGER, "problem with matrix of size %dx%d, %d\n", nv, nv, ne);
	//		std::cerr << _m << "\n";
			throw e;
		}

		message(bLOG, _label + ": running" + std::to_string( _numberofsteps) + "\n");

		for(unsigned i=0; i<_numberofsteps; ++i){
			_g.set_annotations(_u, i); // 0 means initial.
			// _g.display_result(sil, sil, _u, "_"+_label+"_"+std::to_string(i));
#if 1
			lu.backsubst(x, _u);
			_u = x;
#endif
		}
		// bug. writes out files
		// _g.display_result(sil, sil, _u, "_" + _label + "_99");
	}
	template<class K>
	void get_result(K& k) const{
		// fill kp?
		_g.get_result(_sil, _u, k);
	}
	template<class K>
	void append_result(K& k) const{
		// fill kp?
		_g.get_result(_sil, _u, k);
	}
private:
	UmfpackMatrix _m;
	G const& _g;
	std::string _label;
	double _ik, _iu;
	unsigned _numberofsteps;
	cv::Mat _sil;
	Vector _u;
}; // HEsolver

// solve a "heat equation" forward euler
template<class G>
class FEsolver{
public:

	FEsolver(G const& g, double deltat, double ik=0, double iu=0, std::string label="")
		   :_g(g),
	     _label(label), _ik(ik), _iu(iu), _numberofsteps(3),
		  _u(boost::num_vertices(g)), _dt(deltat)
	{
	}

	template<class K=ANNOTATION>
	void do_it(cv::Mat const& sil, K const* guessknown=NULL){
		s_timer tmr("forward euler");
		_sil=sil; // BUG
		size_t nv=_u.size();
		Vector x(nv);
		Vector s(nv);

		_g.set_annotations(_u, 0); // 0 means initial.

		for(unsigned i=0; i<nv; ++i){
			x[i]=(1.-_dt)*_u[i];

			// fixme: store triangle only.
			auto q=boost::adjacent_vertices(i, _g.graph());
			for(; q.first!=q.second; ++q.first){
				auto w=*q.first;
				assert(i!=w);
				auto e=boost::edge(i, w, _g.graph());
				s[i] += boost::get(boost::edge_weight, _g.graph(), e.first);
			}
			s[i]=sqrt(s[i]);
		}

		for(unsigned i=0; i<nv; ++i){
			auto q=boost::adjacent_vertices(i, _g.graph());
			for(; q.first!=q.second; ++q.first){
				auto w=*q.first;
				x[i] += s[w]*_u[w];
			}
			x[i] *= s[i];
		}
		_u = x;
	}

	void dump_result() const{ untested();
		incomplete();
//		display_sil_bb("detectsil", _sil);
//		_g.display_result(_sil, _sil, _u, "_" + _label + "_99");
	}
	template<class K>
	void get_result(K& k) const{
		// fill kp?
		_g.get_result(_sil, _u, k);
	}
	template<class K>
	void append_result(K& k) const{ untested();
		// fill kp?
		_g.get_result(_sil, _u, k);
	}
private:
	G const& _g;
	std::string _label;
	double _ik, _iu;
	unsigned _numberofsteps;
	cv::Mat _sil;
	Vector _u;
	double _dt;
}; // FE

} //draft
