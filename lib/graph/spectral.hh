
#include "../umfpackmatrix.hh"
#include "../trace.hh"
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>

// #include "dsmatrxa.h"
#include <arpack++/ardsmat.h>
#include <arpack++/ardssym.h>
// #include "lsymsol.h"

namespace spectral{

template<class G>
double /*fixme*/ degree(unsigned i, G const& g)
{
	auto p=boost::adjacent_vertices(i, g);
	double ret=0;
	for(;p.first!=p.second; ++p.first){
		auto j=*p.first;
		auto e=boost::edge(i, j, g);
		if(e.second){
			ret+=boost::get(boost::edge_weight, g, e.first);
		}else{
		}
	}
	return ret;
}






#if 0
// weight matrix "W" in [1]
class WeightMatrixUMF
  : public UmfpackMatrix
{
public: // types
	typedef UmfpackMatrix BaseType;
public:
	WeightMatrix(G const& g)
		: UmfpackMatrix(boost::num_vertices(g) * boost::num_vertices(g) )
	{ untested();
		auto p=boost::edges(g);
		double ret=0;
		for(;p.first!=p.second; ++p.first){
			auto e=*p.first;
			ret-=boost::get(edge_weight, g, e);
			auto s=boost::source(e, g);
			auto t=boost::target(e, g);
			add( s, t, value );
		}
	}
}
#endif

// get the matrix L_s (2.8) in [1] at i,j from an edgeweighted undirected
// graph. (this is probably slow).
template<class G>
double get_norm_L(unsigned i, unsigned j, G const& g)
{
	auto e=boost::edge(i, j, g);
	auto f=boost::edge(j, i, g);
	if(i==j){
		return 1.;
	}else if(!e.second){
		assert(!f.second);
		return 0;
	}else{
		auto we=(boost::get(boost::edge_weight, g, e));
		auto wf=(boost::get(boost::edge_weight, g, f));
		assert(we=wf);

		auto di=degree(i, g);
		auto dj=degree(j, g);
		assert(di>0);
		assert(dj>0);

		return sqrt(di)*sqrt(dj)*we;
	}
}

} // spectral

// [1] L calatroni &al. Graph clustering etc.
