#ifndef SOLVER_HH
#define SOLVER_HH

#include <cassert>
#include <exception>
#include <sstream>

extern "C" {
#include <umfpack.h>
}

#include "vector.hh"
#include "umfpackmatrix.hh"
#include "error.hh"

#include "matrix.hh" // ???

struct UmfpackError : public std::exception {
  UmfpackError( const int& e )
    : e_( e )
  {}

  virtual const char* what() const throw()
  { untested();
    switch( e_ ) {
      case UMFPACK_OK:
	return "UMFPACK was successful";
      case UMFPACK_WARNING_singular_matrix:
	return "Matrix is singular. There are exact zeros on the diagonal of U";
      case UMFPACK_WARNING_determinant_underflow:
	return "The determinant is nonzero, but smaller in magnitude than the smallest positive floating-point number.";


      case UMFPACK_ERROR_n_nonpositive:
      case UMFPACK_ERROR_out_of_memory:
	return "\
Insufficient memory to perform the symbolic analysis. If the \
analysis requires more than 2GB of memory and you are using \
the 32-bit (\"int\") version of UMFPACK, then you are guaranteed \
to run out of memory. Try using the 64-bit version of UMFPACK.";
      case UMFPACK_ERROR_argument_missing:
	return "One or more required arguments is missing.";
      case UMFPACK_ERROR_internal_error:
	return "\
Something very serious went wrong. This is a bug. \
Please contact the author (davis@cise.ufl.edu).";
      default:
	std::stringstream ss;
	ss << "unknown error: " << e_;
	return ss.str().c_str();
      }
  }
private:
  const int e_;
};

namespace impl{

// BUG: inefficient
class UMF_LU{

public:
#if 0
   UMF_LU(UMF_LU const&& o)
      : Numeric(std::move(o.Numeric)), _m(std::move(o._m))
       // 	_control(o._control), _info(o._info) whatever.
   { untested();
    //  o.Numeric=NULL;
   }
#endif
   UMF_LU(UMF_LU&& o)
      : Numeric(std::move(o.Numeric)), _m(std::move(o._m))
       // 	_control(o._control), _info(o._info) whatever.
   { untested();
      o.Numeric=NULL;
      umfpack_di_defaults(_control);
    //  _control[UMFPACK_IRSTEP]=20;
      //_control[UMFPACK_PRL]=4;
   }
   UMF_LU() : Numeric(NULL) { untested();
      umfpack_di_defaults(_control);
    //  _control[UMFPACK_IRSTEP]=20;
   //   _control[UMFPACK_PRL]=4;
   }
   template<class M>
   UMF_LU(M const& A){ untested();

      // get matrix data
      A.matrix(_m);
      trace1("got matrix", _m.n);
#if 0
      for(unsigned i=0; i<_m.n; ++i){ untested();
	 std::cerr << i << ": " << _m._Ap[i] << " " << _m._Ap[i+1] << ": ";
	 for(int j=_m._Ap[i]; j<_m._Ap[i+1]; ++j){ untested();
	    std::cerr << _m._Ai[j] << " " << _m._Ax[j] << " ";
	 }
	 std::cerr << "\n";
      }
#endif

      // helper variables
      int status;
      void *Symbolic;

      // set defaults
      // umfpack_di_defaults(_control);

      assert(_m.Ap.size());
      assert(_m.Ai.size());
      assert(_m.Ax.size());
      // symbolic factorisation
      message(bTRACE, "performing symbolic LU %d\n", _m.n);
      _control[UMFPACK_ORDERING] = UMFPACK_ORDERING_BEST;
      status = umfpack_di_symbolic ( _m.n, _m.n, _m.Apdata(),
	    _m.Aidata(), _m.Axdata(),
	    &Symbolic, _control, _info) ;
      if( status != UMFPACK_OK ){ untested();
	 umfpack_di_free_symbolic (&Symbolic) ;
	 throw UmfpackError( status );
      }else{ untested();
      }

      // numeric factorisation
      trace1("numerical fact", _m.n);
      message(bTRACE, "performing numerical LU %d\n", _m.n);
      message(bTRACE, "sizes: %d %d %d\n", _m.Ap.size(),  _m.Ai.size(), _m.Ax.size());
      status = umfpack_di_numeric ( _m.Apdata(), _m.Aidata(), _m.Axdata(),
	    Symbolic, &Numeric, _control, _info) ;
      message(bTRACE, "done numerical LU %d\n", _m.n);

      // free symbolic factorisation
      umfpack_di_free_symbolic (&Symbolic) ;

      if( status != UMFPACK_OK ){ untested();
	 throw UmfpackError( status );
      }else{ untested();
      }


   }
public: //assign
   UMF_LU& operator=(UMF_LU&& o)
       // 	_control(o._control), _info(o._info) whatever.
   { itested();
      Numeric = std::move(o.Numeric);
      _m = std::move(o._m);
      o.Numeric = NULL;
      return *this;
   }

   void backsubst(Vector&x, const Vector& b){ untested();
      message(bTRACE, "backsubst %d\n", b.size());
      assert( b.size() == unsigned(_m.n) );
      assert( x.size() == unsigned(_m.n) );

      x.clear();

      int status = umfpack_di_solve (UMFPACK_A, _m.Apdata(),
	    _m.Aidata(), _m.Axdata(),
	    x.data(), b.data(), Numeric, _control, _info) ;


#ifdef DEBUG_
      std::cout << "IRSTEPS " << _info[UMFPACK_IR_TAKEN] << "\n";
#endif

      if( status != UMFPACK_OK ){ untested();
	 throw UmfpackError( status );
      }else{ untested();
      }
   }

   ~UMF_LU(){ untested();
      umfpack_di_free_numeric(&Numeric);
   }

private:
   void* Numeric;
   UmfpackMatrixHolder _m;
   double _control [UMFPACK_CONTROL];
   double _info[UMFPACK_INFO];
}; // UMF_LU

} // impl

template<class Matrix, class Vector >
void umfpackSolve( const Matrix& A, Vector&x, const Vector& b )
{ untested();

   impl::UMF_LU lu(A);

   lu.backsubst(x, b);

}

//template<class Matrix, class Vector >
//void solve( const Matrix& A, Vector&x, const Vector& b ){ untested();
//  return umfpackSolve(A, x, b);
//}
//
// TODO: mopve to umfpack_something.h
template < class datatype, class indextype >
class UmfpackLU{
public:
  typedef CompressedColumnMatrix<datatype, indextype> BackendType;
  typedef void* SymbolicSolutionType; // HACK
public:
  UmfpackLU(CompressedColumnMatrix<datatype, indextype> const& b)
    : _b(b), _s(NULL), _w(NULL){
      umfpack_di_defaults(_control);
      _control[UMFPACK_IRSTEP]=20;
      _control[UMFPACK_PRL]=4;
  }
  ~UmfpackLU(){ untested();
     if(_s){ untested();
	 umfpack_di_free_symbolic (&_s) ;
     }else{ untested();
     }
  }

  // template<class Matrix, class Vector > //TODO????
  template< class Matrix >
  void solve( const Matrix& A, Vector&x, const Vector& b ){

     if(!_s){
	ss(A);
	message(bTRACE, "alloc.... %d\n", x.size());
	// _wi = (int*) new char[x.size() * sizeof(int) + 5*x.size()*sizeof(double)];
        // _w = (double*) ((char*)_w)+sizeof(int)*x.size();
	_wi = new int[x.size()];
        _w = new double[5*x.size()];
     }else{
     }

     int status;

#ifdef RESOLVE
     unsigned n=A.cols();
     umfpack_di_free_symbolic (&_s) ;
     status = umfpack_di_symbolic ( n, n, A.Ap(),
	    A.Ai(), A.Ax(),
	    &_s, _control, _info) ;
#endif

     void* Numeric;
//     std::cerr << A << "\n";
     message(bTRACE, "performing numerical LU %d\n", A.size());
//     message(bTRACE, "sizes %d %d\n", A.Ap().size(),  A.Ai().size());
     status = umfpack_di_numeric ( A.Ap(), A.Ai(), A.Ax(),
	   _s, &Numeric, _control, _info) ;

#if 0
     // show info
     {
       std::cout << "umfpack numeric info:\n";
       std::cout << "- system size: " << A.size() << "\n";
       std::cout << "- [UMFPACK_RCOND]: "
		 << _info[UMFPACK_RCOND] << "\n";
       std::cout << "- [UMFPACK_NUMERIC_SIZE]: "
		 << _info[UMFPACK_NUMERIC_SIZE] << "\n";
       std::cout << "- [UMFPACK_PEAK_MEMORY]: "
		 << _info[UMFPACK_PEAK_MEMORY] << "\n";
       std::cout << "- [UMFPACK_FLOPS]: "
		 << _info[UMFPACK_FLOPS] << "\n";
       std::cout << "- [UMFPACK_LNZ]: "
		 << _info[UMFPACK_LNZ] << "\n";
       std::cout << "- [UMFPACK_UNZ]: "
		 << _info[UMFPACK_UNZ] << "\n";
       std::cout << "- [UMFPACK_NUMERIC_TIME]: "
		 << _info[UMFPACK_NUMERIC_TIME] << std::endl;
     } // end show info
#endif

     if( status != UMFPACK_OK ){ untested();
        umfpack_di_free_numeric (&Numeric) ;
	throw UmfpackError( status );
     }else{
     }

     message(bTRACE, "wsolve.... %d %d\n", x.size(), b.size());
#if 1
       status = umfpack_di_wsolve (UMFPACK_A, A.Ap(),
	    A.Ai(), A.Ax(),
	    x.data(), b.data(), Numeric, _control, _info, _wi, _w);
#else
       status = umfpack_di_solve (UMFPACK_A, A.Ap(),
	    A.Ai(), A.Ax(),
	    x.data(), b.data(), Numeric, _control, _info);
#endif

#ifdef DEBUG_
      std::cout << "IRSTEPS " << _info[UMFPACK_IR_TAKEN] << "\n";
#endif
      // check solve
      Vector res( x.size() );

      res = A.call( x, res );
      res.axpy(-1., b);
      double nrm=res.norm2();
#ifdef DEBUG
      std::cout << "RESIDUAL " << nrm <<"\n";
#endif

      assert( x.is_valid() );
      if( nrm >= 1.0e-10 ) { untested();
      	 message(bDANGER, "WARNING: solver residual = %e\n", nrm);
      }else{
      }

      umfpack_di_free_numeric (&Numeric) ; // BUG (incomplete UMFPACK)

      if( status != UMFPACK_OK ){ untested();
	 throw UmfpackError( status );
      }else{
      }

//	return umfpackSolve(A, x, b);
  }

private:
  template<class Matrix >
  void ss(Matrix const& A){
      int status;

      // symbolic factorisation
      unsigned n=A.cols();
      message(bTRACE, "performing symbolic LU %d\n", n);
      status = umfpack_di_symbolic ( n, n, A.Ap(),
	    A.Ai(), A.Ax(),
	    &_s, _control, _info) ;
      if( status != UMFPACK_OK ){ untested();
	 umfpack_di_free_symbolic (&_s) ;
	 throw UmfpackError( status );
      }else{
      }
  }
public:
private:
  BackendType const& _b;
  SymbolicSolutionType _s;
  double* _w;
  int* _wi;
  double _control [UMFPACK_CONTROL];
  double _info[UMFPACK_INFO];
}; // UmfpackLU

#endif
// vim:ts=8
