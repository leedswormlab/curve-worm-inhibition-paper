#ifndef TIMEPROVIDER_HH
#define TIMEPROVIDER_HH

#include <cassert>

#include "parameter.hh"
#include "error.hh"
#include "../include/misc.hh"

struct TimeProvider
{
  TimeProvider( double startTime=0., double endTime=0. )
    : startTime_( startTime ),
      endTime_( endTime ),
      time_( startTime ),
      _it( 0 ),
      dt_( 0.0 ),
      dtValid_( false )
  {}

  TimeProvider( const TimeProvider& other ) = default;
//    : startTime_( other.startTime_ ),
//      time_( other.time_ ),
//      _it( other._it ),
//      dt_( other.dt_ ),
//      dtValid_( other.dtValid_ )
//  {}

  double time() const
  {
    return time_;
  }

  DEPRECATED
  double& timehack() {
    return time_;
  }

  unsigned int iteration() const
  {
    return _it;
  }

  void regress(double newtime){ untested();
	  assert(newtime<=time_);
	  assert(newtime>time_-dt_ );

	  dt_+= newtime-time_;
	  time_ = newtime;
	  trace3("tp::regress", dt_, time_, deltaT());
  }

  // advance.
  bool next( double dt ) {
    setDeltaT( dt );
	 return next();
  }

  bool next() {
    // TODO: what did felix have in mind here?
	  trace5("tp::next", dt_, time_, deltaT(), endTime_, _it);

	  time_ += deltaT();
	 ++_it;
	 return true;
	 // double newtime = time_ +  deltaT();
	 // if(newtime <= endTime_ + 1e-20){
	 // 	 time_ = newtime;
	 // 	 ++_it;
	 // 	 return true;
	 // }else{ untested();
	 // 	 return false;
	 // }
  }

  bool is_end() const {
    if( endTime_ < startTime_ ) {
      return false;
    }

    return ( time_ < endTime_ );
  }

  double deltaT() const
  {
    if( not dtValid_ )
      throw exception_invalid("time step");
    return dt_;
  }

  void setDeltaT( const double dt )
  {
    dt_ = dt;
    dtValid_ = true;
  }

  void reset()
  {
	  _it=0;
    time_ = startTime_;
  }

  double startTime() const{ return startTime_; }
  double endTime() const{ return endTime_; }

private:
  const double startTime_;
  const double endTime_;
  double time_;
  unsigned _it;

  double dt_;
  bool dtValid_;
};

#endif // #ifndef TIMEPROVIDER_HH
