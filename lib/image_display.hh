#ifndef IMG_DISP_H
#define IMG_DISP_H

#include "image/io.hh"
#include "io_misc.hh"

typedef std::pair<cv::Point, cv::Point> bounding_box_type;

// can do rgb as well
void display_grayscale_frame(std::string label, cv::Mat input, unsigned wait=0)
{
//   std::cerr<<"display_grayscale_frame " << label << input.total() << "\n";
	cv::namedWindow( label, CV_WINDOW_NORMAL );
   cv::resizeWindow(  label, 1024, 1024 );
   // int minx = input.cols, miny = input.rows, maxx = 0, maxy = 0;
   cv::Mat tmp;

	// auto type=input.type();
	// auto t=type & CV_MAT_DEPTH_MASK;

	bool is_gray = input.channels()==1;
	
	if(is_gray){ untested();
		cv::cvtColor( input, tmp, CV_GRAY2BGR );
	}else{
		tmp=input;
	}

   cv::imshow( label, tmp);
   // misc::waitKey(wait);
	cv::waitKey(wait);
}

void display_grayscale_frame(std::string label, cv::Mat input, std::pair<cv::Point, cv::Point> bbox);

// hack. only works for sil
void display_sil_bb(std::string label, cv::Mat sil)
{
	auto bb=bbox(sil);
	display_grayscale_frame(label, sil, bb);
}

inline void display_grayscale_frame(std::string label, cv::Mat input, std::pair<cv::Point, cv::Point> bbox)
{

	auto min=bbox.first;
	auto max=bbox.second;
	auto bb=input.colRange( min.x, max.x ).rowRange( min.y, max.y );
	display_grayscale_frame(label, bb);
//		cv::imwrite( "result"+infix+".png", bb );
}

template<class V>
inline void display_silhouette_image(std::string label, cv::Mat const& sil, V const& img)
{
	auto bb=bbox(sil);
	auto min=bb.first;
	auto max=bb.second;
	auto range=sil.colRange( min.x, max.x ).rowRange( min.y, max.y );

	cv::Mat M(range.clone());

	unsigned wd=M.cols;
	unsigned ht=M.rows;

	unsigned i=0;
	for(unsigned row=0 ;row<ht; ++row ){ itested();
		for(unsigned col=0 ;col<wd; ++col ){
			const uchar sil_color = M.at<uchar>(row, col);
			if(!sil_color){
				continue;
			}else if(img[i]<=0){
				M.at<uchar>(row, col)=50;
			}
			++i;
		}
	}
	trace1("sil", i);

	display_grayscale_frame(label, M);
}

// DUP!
template<class V>
inline void display_silhouette_image_trans(std::string label, cv::Mat const& sil, V const& img)
{
	auto bb=bbox(sil);
	auto min=bb.first;
	auto max=bb.second;
	auto range=sil.colRange( min.x, max.x ).rowRange( min.y, max.y );

	cv::Mat M(range.clone());

	unsigned wd=M.cols;
	unsigned ht=M.rows;

	unsigned i=0;
	for(unsigned col=0 ;col<wd; ++col ){
		for(unsigned row=0 ;row<ht; ++row ){
			const uchar sil_color = M.at<uchar>(row, col);
			if(!sil_color){
				continue;
			}else if(img[i]<=0){
				M.at<uchar>(row, col)=0;
			}
			++i;
		}
	}
	trace1("sil", i);

	display_grayscale_frame(label, M);
}

#if 0
// duplicate, io.hh
template<class K>
static void pointset_bb( K const& p, int& lbx, int& lby, int& ubx, int& uby,
		cv::Mat const& input)
{ untested();

	for(auto i: p){
		int X=i.first.x; // BUG,vector?
		int Y=i.first.y;
		lbx=std::min(lbx, X-15);
		lby=std::min(lby, Y-15);
		ubx=std::max(ubx, X+15);
		uby=std::max(uby, Y+15);
	}
	lbx=std::max(0, lbx-15);
	lby=std::max(0, lby-15);
	ubx=std::min(input.cols, ubx+15);
	uby=std::min(input.rows, uby+15);
}
#endif

// duplicate, dump_pointset...?
template<class K>
inline void display_pointset_on_frame(std::string label, cv::Mat input, K const& p, uchar color=255,
	int lbx=99999,
	int lby=99999, // intmax...
	int ubx=0,
	int uby=0)
{ itested();
	cv::Mat M = input.clone();

	if(p.empty()){
		message(bDANGER, "cannot display pointset %s. it's empty", label.c_str());
		return;
	}else{
	}

	pointset_bb(p, lbx, lby, ubx, uby, input);

	for(auto i: p){
		int X=get_x(i);
		int Y=get_y(i);
		//	cv::Vec3b color( 0, 0, 255);
		// M.at<cv::Vec3b>(i.first.y, i.first.x) = color;
		M.at<uchar>(Y, X) = color;
	}

	message(bTRACE, "show bb %i %i %i %i\n", lbx, lby, ubx, uby);
	auto bb=std::make_pair( cv::Point(lbx, lby), cv::Point(ubx, uby));
	display_grayscale_frame(label, M, bb);
}

// BUG: copy
template<>
inline void display_pointset_on_frame(std::string label, cv::Mat input,
		std::vector<cv::KeyPoint> const& p, uchar color,
	int in_lbx,
	int in_lby,
	int in_ubx,
	int in_uby)
{ itested();
	cv::Mat M = input.clone();

	if(p.empty()){
		message(bDANGER, "cannot display pointset %s. it's empty", label.c_str());
		return;
	}else{
	}

int lbx=99999; // intmax...
int lby=99999; // intmax...
int ubx=0;
int uby=0;

	for(auto i: p){
		int X=i.pt.x;
		int Y=i.pt.y;
		M.at<uchar>(Y, X) = color;
		lbx=std::min(lbx, X-15);
		lby=std::min(lby, Y-15);
		ubx=std::max(ubx, X+15);
		uby=std::max(uby, Y+15);
	}
	lbx=std::max(0, lbx-15);
	lby=std::max(0, lby-15);
	ubx=std::min(input.cols, ubx+15);
	uby=std::min(input.rows, uby+15);

	lbx=std::max(in_lbx, lbx);
	lby=std::max(in_lby, lby);
	ubx=std::min(in_ubx, ubx);
	uby=std::min(in_uby, uby);

	message(bTRACE, "show bb %i %i %i %i\n", lbx, lby, ubx, uby);

	auto bb=std::make_pair( cv::Point(lbx, lby), cv::Point(ubx, uby));

	display_grayscale_frame(label, M, bb);

}

// obsolete.
template<class V>
void display_silhouette_image_nobb(std::string label, cv::Mat const& sil, V const& img)
{
	auto bb=bbox(sil);
	auto min=bb.first;
	auto max=bb.second;

	cv::Mat M(sil.clone());

	unsigned wd=M.cols;
	unsigned ht=M.rows;

	unsigned i=0;
	for(unsigned row=0 ;row<ht; ++row ){ itested();
		for(unsigned col=0 ;col<wd; ++col ){
			const uchar sil_color = M.at<uchar>(row, col);
			if(!sil_color){
				continue;
			}else if(img[i]<=0){
				M.at<uchar>(row, col)=50;
			}
			++i;
		}
	}
	trace1("sil", i);

	display_grayscale_frame(label, M, bb);
}

inline void display_contours(std::string const& label, std::vector<std::vector<cv::Point> > contours,
                             cv::Mat const& bg)
{
	cv::Mat output(cv::Mat::zeros(bg.size(), bg.type()));
	cv::drawContours( output, contours, -1, 255, CV_FILLED );
	display_grayscale_frame(label, output);
}

#endif
