#include "options.hh"
#include <iostream>
#include <stdarg.h>
#include <stdio.h>

/*--------------------------------------------------------------------------*/
/* error: message printer, from gnucap, simplified. */
void message(unsigned badness, const char* fmt, ...)
{
  if (badness >= options::errorlevel) {
    char buffer[2048] = "";
    va_list arg_ptr;
    va_start(arg_ptr,fmt);
    vsprintf(buffer,fmt,arg_ptr);
    va_end(arg_ptr);
	 std::cout << buffer;
	 std::cout << std::flush; // HACK
  }else{
  }
}
/*--------------------------------------------------------------------------*/
void message(unsigned badness, const std::string& msg)
{
  if (badness >= options::errorlevel) {
	  std::cout << msg;
	 std::cout << std::flush; // HACK
  }else{
  }
}
