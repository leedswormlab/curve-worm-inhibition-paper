#ifndef DISCRETEFUNCTION_HH
#define DISCRETEFUNCTION_HH

#include <fstream>
#include <memory> // shared_ptr
#include <map>

#include "vector.hh"
#include "mesh.hh"
#include "error.hh"

class DiscreteFunctionBase{
public:
  typedef SubArray< Vector > DataType; // what else? why?
public:
	DiscreteFunctionBase(DataType d)
	  : data_(d){
	}
	virtual ~DiscreteFunctionBase(){
	}
	virtual unsigned int size() const = 0;
//	virtual void set_data(DataType d) = 0;
//
public:
  void set_data(DataType d){
	  data_ = d;
  }

protected:
  DataType data_;
};

class MeshFunctionBase{
public:
	virtual ~MeshFunctionBase(){
	}
	virtual DiscreteFunctionBase const& solution() const = 0;
	virtual DiscreteFunctionBase& solution() = 0;

//	virtual DiscreteFunctionBase const& rhs() const = 0;
	virtual DiscreteFunctionBase& rhs() = 0;

	virtual DiscreteFunctionBase& oldsolution() = 0;
	virtual DiscreteFunctionBase const& oldsolution() const = 0;
	virtual size_t footprint_size() const = 0;

	unsigned offset()const{ return _offset; }
public: // HACK
	unsigned _offset;
};
// a mesh function as seen from an entity
template<class DF>
class MeshFunction : public MeshFunctionBase{
public:
	MeshFunction()
	: _solution(NULL), _rhs(NULL), _oldsolution(NULL){
	}
	MeshFunction(const MeshFunction&) = delete;
public: // for now
	DF const& solution() const{
		assert(_solution);
		return *_solution;
	}
	DF& solution(){
		assert(_solution);
		return *_solution;
	}
	DF& rhs(){
		assert(_rhs);
		return *_rhs;
	}
	DF const& oldsolution() const{ itested();
		assert(_oldsolution);
		return *_oldsolution;
	}
	DF& oldsolution(){
		assert(_oldsolution);
		return *_oldsolution;
	}
public:
	void populate(Mesh const& m, unsigned offset){
      Vector dummy(0); // hmmm?!
      _solution = new DF(m, dummy);
      _rhs = new DF(m, dummy);
      _oldsolution = new DF(m, dummy);
		_offset = offset;
	}
	size_t footprint_size() const{
		assert(_solution);
		return _solution->footprint_size();
	}
public: // for now
	DF* _solution;
	DF* _rhs;
	DF* _oldsolution; // really?
};

/**
 *  \class DiscreteFunctionInterface
 *
 *  \brief interface class for discrete functions
 *
 *  This class implements a discrete function over a mesh.
 */
template< unsigned int mydim >
class DiscreteFunctionInterface : public DiscreteFunctionBase
{
public:
  static const int dim = mydim;

  typedef RangeVector< dim > RangeVectorType;

  /**
   *  \brief constructor
   */
  explicit DiscreteFunctionInterface( const Mesh& mesh, DataType data )
    : DiscreteFunctionBase(data), mesh_( mesh )
  {}

  size_t footprint_size() const{
	  return dim*N();
  }

  /**
   *  \brief copy constructor
   */
  DiscreteFunctionInterface( const DiscreteFunctionInterface<dim>& other )
    : DiscreteFunctionBase(other), mesh_( other.mesh_ )
  {}

  /**
   *  \brief evaluation operator
   *
   *  evaluates discrete function at entry j
   *  \param[in]  j index
   *  \return range vector
   */
  virtual const RangeVectorType evaluate( const unsigned int j ) const = 0;

  /**
   *  \brief assignment operator
   *
   *  assigns discrete function at entry j
   *  \param[in]  j index
   *  \param[in]  val range vector
   */
  virtual void assign( const unsigned int j, const RangeVectorType& val ) = 0;

  /**
   *  \brief assignment operator
   *
   *  assigns discrete function at entry j
   *  \param[in]  j index
   *  \param[in]  val all values at set to val
   */
  virtual void assign( const unsigned int j, const double val ) = 0;

  /**
   *  \brief addition operator
   *
   *  addition operator for discrete function at entry j.
   *  the result should be data[ j ][ d ] = val [ d ]
   *  \param[in]  j index
   *  \param[in]  val range vector
   */
  virtual void add( const unsigned int j, const RangeVectorType& val ) = 0;

  /**
   *  \brief addition operator
   *
   *  addition operator for discrete function at entry j.
   *  the result should be data[ j ][ d ] = val
   *  \param[in]  j index
   *  \param[in]  val all dimension use val
   */
  virtual void add( const unsigned int j, const double val ) = 0;

  /**
   *  \brief scaling operator
   *
   *  scales discrete function by scalar
   *  \param val scale factor
   */
  virtual void operator*=( const double val ) = 0;

  /**
   *  \brief number of range vectors stored
   */
  virtual unsigned int N() const = 0;

  /**
   *  \brief number of doubles stored
   */
  virtual unsigned int size() const = 0;

  /**
   *  \brief access to individual entries
   */
  virtual double& operator[]( const unsigned int j ) = 0;

  /**
   *  \brief const access to individual entries
   */
  virtual double const& operator[]( const unsigned int j ) const = 0;

  /**
   *  \brief access to individual entries
   */
  virtual double& at( const unsigned int j )
  {
    return operator[]( j );
  }

  /**
   *  \brief const access to individual entries
   */
  virtual double at( const unsigned int j ) const
  {
    return operator[]( j );
  }

  /**
   *  \brief const access to the mesh
   */
  const Mesh& mesh() const
  {
    return mesh_;
  }

  // bug, feature?
  virtual void setDirichletBoundaries( const double /*val*/ ) { incomplete();
  }

private:
  const Mesh& mesh_;

  template< const unsigned int thedim >
  friend std::ostream& operator<<(std::ostream& os, const DiscreteFunctionInterface<dim>& f);
};

template< const unsigned int dim >
inline std::ostream& operator<<(std::ostream& os, const DiscreteFunctionInterface<dim>& f)
{
  os << "(";
  for( unsigned j = 0; j < f.size(); ++j ) {
    os << " " << f.at(j);
  }
  os << " )";
  return os;
}

/**
 *  \class PiecewiseLinearFunction
 *
 *  \brief implementation of piecewise linear function
 *
 *  data is stored elsewhere
 */
template< unsigned int mydim >
class PiecewiseLinearFunction
  : public DiscreteFunctionInterface< mydim >
{
  typedef DiscreteFunctionInterface<mydim > BaseType;
public: // types
  using typename BaseType::DataType;
  typedef typename DataType::const_iterator const_iterator; // ??
  typedef typename DataType::iterator iterator;             // ??
  static const unsigned int dim = BaseType :: dim;
  typedef typename BaseType :: RangeVectorType RangeVectorType;
  using BaseType::data_;

public: // construct
  PiecewiseLinearFunction( const Mesh& mesh, DataType data )
    : BaseType( mesh, data )
  {}

  // this leads to all sorts of memory problems (not sure why)
  // let's just remove it and not worry any more
  DEPRECATED
  PiecewiseLinearFunction( const Mesh& mesh, Vector vector )
    : BaseType( mesh, vector )
  {
  }

  PiecewiseLinearFunction( const PiecewiseLinearFunction& other )
    : BaseType( other )
  {}

  virtual ~PiecewiseLinearFunction(){ itested();
  }

public: // const access
  size_t footprint_size() const{
	  return dim*N();
  }
  virtual const RangeVectorType evaluate( const unsigned int j ) const
  {
    RangeVectorType ret;
    for( unsigned int d = 0; d < dim; ++d )
    {
      ret[ d ] = data_[ j * dim + d ];
    }
    return ret;
  }
  RangeVectorType eval( unsigned int j ) const{
	  return evaluate(j);
  }

  virtual void assign( const unsigned int j, const RangeVectorType& val )
  {
    for( unsigned int d = 0; d < dim; ++d )
    {
      const double v = val.at( d );
      data_[ j * dim + d ] = v;
    }
  }

  virtual void assign( const unsigned int j, const double val )
  {
    for( unsigned int d = 0; d < dim; ++d )
    {
      data_[ j * dim + d ] = val;
    }
  }

  virtual void assign( const PiecewiseLinearFunction<mydim>& other )
  {
    for( unsigned int n = 0; n < size(); ++n )
    {
      data_[ n ] = other.data()[ n ];
    }
  }

  virtual void add( const unsigned int j, const RangeVectorType& val )
  {
    for( unsigned int d = 0; d < dim; ++d )
    {
      const double v = val.at( d );
      data_[ j * dim + d ] += v;
    }
  }

  // this is a stupid function
  DEPRECATED
  virtual void add( const unsigned int j, const double val )
  { untested();
    for( unsigned int d = 0; d < dim; ++d )
    {
      data_[ j * dim + d ] += val;
    }
  }
  virtual void add_scalar( const unsigned int j, const double val ) {
    data_[ j ] += val;
  }

  virtual void operator*=( const double val )
  {
    for( auto& d : data_ )
      d *= val;
  }

  virtual unsigned int N() const
  {
    return mesh().N();
  }

  // YIKES: doesn't do the expected thing...
  // remove?!
  virtual double& operator[]( const unsigned int j ) {
    return data_[ j ];
  }

  virtual double const& operator[]( const unsigned int j ) const {
    return data_[ j ];
  }

  iterator begin() {
    return data_.begin();
  }

  iterator end() {
    return data_.end();
  }

  const_iterator begin() const {
    return data_.begin();
  }

  const_iterator end() const
  {
    return data_.end();
  }

  void print( std::ostream& s ) const { untested();
    for( unsigned int i = 0; i < N(); ++i ) { untested();
      RangeVectorType v = evaluate( i );
      for( auto vc : v ) { untested();
        s << vc << " ";
      }
      s << std::endl;
    }
  }

  // datasize?!
  unsigned int size() const { itested();
    return data_.size();
  }

  void setDirichletBoundaries( const double val ) { itested();
    assign( 0, RangeVectorType(val) );
    assign( N()-1, RangeVectorType(val) );
  }

  using BaseType::mesh;

  DataType& data() { return data_; }
  const DataType& data() const { return data_; }

}; // PWL function

/**
 *  \class PiecewiseConstantFunction
 *
 *  \brief implementation of piecewise constant function
 *
 *  data is stored elsewhere
 */
template< unsigned int mydim >
class PiecewiseConstantFunction
  : public DiscreteFunctionInterface< mydim >
{
  typedef DiscreteFunctionInterface<mydim > BaseType;
  typedef SubArray< Vector > DataType;

  using BaseType::data_;

public:
  static const unsigned int dim = BaseType :: dim;
  typedef typename BaseType :: RangeVectorType RangeVectorType;

  PiecewiseConstantFunction( const Mesh& mesh, DataType data )
    : BaseType( mesh, data )
  {}

  PiecewiseConstantFunction( const PiecewiseConstantFunction& other )
    : BaseType( other )
  {}

public: // const access
  virtual const RangeVectorType evaluate( const unsigned int j ) const
  {
    RangeVectorType ret;
    for( unsigned int d = 0; d < dim; ++d )
    {
      ret[ d ] = data_[ j * dim + d ];
    }
    return ret;
  }
  RangeVectorType eval( unsigned int j ) const{
	  return evaluate(j);
  }

  // baseclass?
  void assign( PiecewiseConstantFunction<mydim> const& other )
  {
    for( unsigned int i = 0; i < data_.size(); ++i ) {
      data_[i] = other.data_[i];
    }
  }

  virtual void assign( const unsigned int j, const RangeVectorType& val )
  {
    for( unsigned int d = 0; d < dim; ++d )
    {
      data_[ j * dim + d ] = val.at( d );
    }
  }

  virtual void assign( const unsigned int j, const double val )
  {
    for( unsigned int d = 0; d < dim; ++d )
    {
      data_[ j * dim + d ] = val;
    }
  }

  virtual void add( const unsigned int j, const RangeVectorType& val )
  {
    for( unsigned int d = 0; d < dim; ++d )
    {
      data_[ j * dim + d ] += val.at( j );
    }
  }

  // this is a stupid function
  DEPRECATED
  virtual void add( const unsigned int j, const double val )
  { untested();
    for( unsigned int d = 0; d < dim; ++d )
    {
      data_[ j * dim + d ] += val;
    }
  }
  virtual void add_scalar( const unsigned int j, const double val ) {
    data_[ j ] += val;
  }

  virtual void operator*=( const double val )
  {
    for( auto& d : data_ )
      d *= val;
  }

  virtual unsigned int N() const
  {
    return mesh().N()-1;
  }

  virtual double& operator[]( const unsigned int j )
  {
    return data_[ j ];
  }

  virtual double const& operator[]( const unsigned int j ) const
  {
    return data_[ j ];
  }

  typename DataType :: iterator begin()
  {
    return data_.begin();
  }

  typename DataType :: iterator end()
  {
    return data_.end();
  }

  typename DataType :: const_iterator begin() const
  {
    return data_.begin();
  }

  typename DataType :: const_iterator end() const
  {
    return data_.end();
  }

  unsigned int size() const
  {
    return data_.size();
  }

  void print( std::ostream& s ) const
  {
    for( unsigned int i = 0; i < N(); ++i )
    {
      RangeVectorType v = evaluate( i );
      for( auto vc : v )
      {
        s << vc << " ";
      }
      s << std::endl;
    }
  }

protected:
  using BaseType::mesh;
}; // PCF

// always one dimensional
template< unsigned int mydim >
class DEPRECATED PositionCurvaturePressureTuple
  : public Vector
{
  typedef Vector BaseType;

public:
  static const unsigned int dim = mydim;

  typedef PiecewiseLinearFunction< dim > PositionFunctionType;
  typedef PiecewiseLinearFunction< dim > CurvatureFunctionType;
  typedef PiecewiseConstantFunction< 1 > PressureFunctionType;

  typedef RangeVector< dim > RangeVectorType;

  typedef SubArray< Vector > SubArrayType;

  PositionCurvaturePressureTuple( const Mesh& mesh )
    : BaseType( ( 2*dim * mesh.N() + (mesh.N()-1) ) ), mesh_( mesh ),
      position_( mesh, SubArrayType( *this, 0, dim*mesh.N() ) ),
      curvature_( mesh, SubArrayType( *this, dim*mesh.N(), 2*dim*mesh.N() ) ),
      pressure_( mesh, SubArrayType( *this, 2*dim*mesh.N(), this->size() ) )
  {}

  // offset of position storage in full storage vector
  unsigned int position_offset() const {
    return 0;
  }
  // offset of curvature storage in full storage vector
  unsigned int curvature_offset() const {
    return dim*mesh_.N();
  }
  // offset of pressure storage in full storage vector
  unsigned int pressure_offset() const {
    return 2*dim*mesh_.N();
  }

  const PositionFunctionType& position() const {
	  return position_;
  }
  PositionFunctionType& position() {
	  return position_;
  }
  const CurvatureFunctionType& curvature() const { return curvature_; }
  CurvatureFunctionType& curvature() { return curvature_; }
  const PressureFunctionType& pressure() const { return pressure_; }
  PressureFunctionType& pressure() { return pressure_; }

  void assign( const PositionCurvaturePressureTuple<mydim> &other )
  {
    for( unsigned int i = 0; i < size(); ++i ) {
      at( i ) = other.at( i );
    }
  }

  PositionCurvaturePressureTuple& operator=(
		  const PositionCurvaturePressureTuple &other )
  {
	  assign(other);
	  return *this;
  }

private:
  const Mesh& mesh_;

  PositionFunctionType position_;
  CurvatureFunctionType curvature_;
  PressureFunctionType pressure_;
};

template< unsigned int mydim >
class DEPRECATED PositionMomentCurvaturePressureTuple
  : public Vector
{
  typedef Vector BaseType;

public:
  static const unsigned int dim = mydim;

  typedef PiecewiseLinearFunction< dim > PositionFunctionType;
  typedef PiecewiseLinearFunction< dim > MomentFunctionType;
  typedef PiecewiseLinearFunction< dim > CurvatureFunctionType;
  typedef PiecewiseConstantFunction< 1 > PressureFunctionType;

  typedef RangeVector< dim > RangeVectorType;

  typedef SubArray< Vector > SubArrayType;

  PositionMomentCurvaturePressureTuple(const Mesh &mesh)
      : BaseType((3 * dim * mesh.N() + (mesh.N() - 1))), mesh_(mesh),
	position_(mesh, SubArrayType(*this, 0, moment_offset())),
	moment_(mesh, SubArrayType(*this, moment_offset(), curvature_offset())),
	curvature_(mesh,
		   SubArrayType(*this, curvature_offset(), pressure_offset())),
	pressure_(mesh, SubArrayType(*this, pressure_offset(), this->size())) {}

  // offset of position storage in full storage vector
  unsigned int position_offset() const {
    return 0;
  }
  // offset of moment storage in full storage vector
  unsigned int moment_offset() const {
    return dim*mesh_.N();
  }
  // offset of curvature storage in full storage vector
  unsigned int curvature_offset() const {
    return 2*dim*mesh_.N();
  }
  // offset of pressure storage in full storage vector
  unsigned int pressure_offset() const {
    return 3*dim*mesh_.N();
  }

  const PositionFunctionType& position() const {
	  return position_;
  }
  PositionFunctionType& position() {
	  return position_;
  }
  const MomentFunctionType& moment() const { return moment_; }
  MomentFunctionType& moment() { return moment_; }
  const CurvatureFunctionType& curvature() const { return curvature_; }
  CurvatureFunctionType& curvature() { return curvature_; }
  const PressureFunctionType& pressure() const { return pressure_; }
  PressureFunctionType& pressure() { return pressure_; }

  void assign( const PositionMomentCurvaturePressureTuple<mydim> &other )
  {
    for( unsigned int i = 0; i < size(); ++i ) {
      at( i ) = other.at( i );
    }
  }

  PositionMomentCurvaturePressureTuple& operator=(
		  const PositionMomentCurvaturePressureTuple &other )
  {
	  assign(other);
	  return *this;
  }

private:
  const Mesh& mesh_;

  PositionFunctionType position_;
  MomentFunctionType moment_;
  CurvatureFunctionType curvature_;
  PressureFunctionType pressure_;
};

template< unsigned mydim >
class NamedFunctionTuple
  : public Vector
{
  typedef Vector BaseType;

  template< unsigned d >
  using Function_ptr_map = std::map< std::string, std::shared_ptr<DiscreteFunctionInterface<d>> >;

  template< unsigned d, unsigned order >
  struct DF_type_traits;

  template< unsigned d>
  struct DF_type_traits<d,0> {
    using DF = PiecewiseConstantFunction<d>;
    static unsigned expected_size( const unsigned N ) {
      return (N-1) * d;
    }
  };

  template< unsigned d>
  struct DF_type_traits<d,1> {
    using DF = PiecewiseLinearFunction<d>;
    static unsigned expected_size( const unsigned N ) {
      return N * d;
    }
  };

  template< unsigned d, bool t >
  struct fwd_unsigned_if;
  template< unsigned d >
  struct fwd_unsigned_if< d, true > {
    static const unsigned dim = d;
  };
  template< unsigned d>
  struct fwd_unsigned_if<d, false > {
  };

public:
  static const unsigned int dim = mydim;

  template< unsigned d, unsigned order >
  using Discrete_function_type = typename DF_type_traits<d, order>::DF;

  typedef SubArray< Vector > SubArrayType;

  NamedFunctionTuple( const Mesh &mesh )
      : BaseType(), mesh_(mesh) {}

  NamedFunctionTuple( const NamedFunctionTuple<dim>& ) = delete;

  template <unsigned d, unsigned order>
  void add_function(const std::string &name) {
    auto& df_new = functions<d>()[name];
    if (df_new) {
      // throw bad name
      assert(0);
    }

    const unsigned old_size = BaseType::size();
    using DF = typename DF_type_traits<d, order>::DF;
    const unsigned df_size = DF_type_traits<d, order>::expected_size( mesh_.N() );

    BaseType::resize( old_size + df_size );
    df_new.reset( new DF( mesh_, SubArrayType( *this, old_size,
					       old_size + df_size ) ) );
    _offsets[name] = old_size;

    assert( df_new );
  }

  // offset of position storage in full storage vector
  unsigned int offset( const std::string& name ) const {
#ifndef NDEBUG
    auto it = _offsets.find(name);
    if( it == _offsets.end() ) {
      std::cerr << "ERROR unable to find discrete function with name " << name << "\n";
      std::cerr << "possible functions:";
      for( auto p : _offsets ) {
	std::cerr << " " << p.first;
      }
      std::cerr << std::endl;
    }
#endif
    return _offsets.at(name);
  }

  template< class DF >
  const DF& function( const std::string& name ) const {
    auto& p0 = functions<DF::dim>().at( name );
    assert( p0 );
    if( std::shared_ptr<const DF> p = std::dynamic_pointer_cast<const DF>( p0 ) ) {
      assert( p );
      return *p;
    } else {
      // throw
      assert( 0 );
    }
  }
  template< class DF >
  DF& function( const std::string& name ) {
#ifndef NDEBUG
    auto it = functions<DF::dim>().find(name);
    if( it == functions<DF::dim>().end() ) {
      std::cerr << "ERROR unable to find discrete function with name " << name << " in dim " << DF::dim << "\n";
      std::cerr << "possible functions for dim " << dim << ":";
      for( auto p : functions<DF::dim>() ) {
	std::cerr << " " << p.first;
      }
      std::cerr << std::endl;
    }
#endif
    auto& p0 =  functions<DF::dim>().at(name);
    assert( p0 );
    if( std::shared_ptr<DF> p = std::dynamic_pointer_cast<DF>( p0 ) ) {
      assert( p );
      return *p;
    } else {
      // throw
      throw exception_invalid("discrete function name");
    }
  }

  // TODO this should either check for equal function names or else
  // copy functions structures across?
  void assign( const NamedFunctionTuple &other )
  {
    for( unsigned int i = 0; i < size(); ++i ) {
      at( i ) = other.at( i );
    }
  }

  NamedFunctionTuple& operator=( const NamedFunctionTuple &other )
  {
    assign(other);
    return *this;
  }

  void print() const {
    for( unsigned i = 0; i < size(); ++ i ){
      std::cout << " " << this->at(i);
    }
    std::cout << std::endl;

    std::cout << "print:";
    const auto& f = function< PiecewiseLinearFunction<2> >( "position" );
    for( unsigned i = 0; i < f.size(); ++i ) {
      std::cout << " " << f.at(i);
    }
    std::cout << std::endl;
  }

protected:
  /**
   *  Accessor functions which convert different dimension function
   *  maps to a single templated access function.  This results in a
   *  function that can be called functions<d>() for d = 1,2,3.
   */
  template< unsigned d >
  Function_ptr_map< fwd_unsigned_if<d, d==1 >::dim > & functions() {
    return _functions_1;
  }
  template< unsigned d >
  Function_ptr_map< fwd_unsigned_if<d, d==2 >::dim > & functions() {
    return _functions_2;
  }
  template< unsigned d >
  Function_ptr_map< fwd_unsigned_if<d, d==3 >::dim > & functions() {
    return _functions_3;
  }
  template< unsigned d >
  const Function_ptr_map< fwd_unsigned_if<d, d==1 >::dim > & functions() const {
    return _functions_1;
  }
  template< unsigned d >
  const Function_ptr_map< fwd_unsigned_if<d, d==2 >::dim > & functions() const {
    return _functions_2;
  }
  template< unsigned d >
  const Function_ptr_map< fwd_unsigned_if<d, d==3 >::dim > & functions() const {
    return _functions_3;
  }
private:
  const Mesh& mesh_;

  Function_ptr_map<1> _functions_1;
  Function_ptr_map<2> _functions_2;
  Function_ptr_map<3> _functions_3;
  std::map< std::string, unsigned > _offsets;
  unsigned _max_offset;
};

#endif // #ifndef DISCRETEFUNCTION_HH
