#ifndef MODEL_VALUE_DRIVEN_HH
#define MODEL_VALUE_DRIVEN_HH

#include <memory>
#include <iostream>
#include <functional>
#include <vector>
#include "vector.hh"
#include "timeprovider.hh"
#include "parameter.hh"
#include "matrix.hh"
#include "readMat.hh"
#include <vector>
#include "worm.hh"

#ifdef OpenCV_FOUND
#include "distancefunction.hh"
#include "forcefunction.hh"
#include "model_distance.hh"
#endif

#include "femscheme.hh"

template<const unsigned int mydim>
struct ValueDrivenMuscleModel : public ModelDefault<mydim> {
private:
  using BaseType = ModelDefault<mydim>;

public:
  static const unsigned int dim = BaseType::dim;
  using RangeVectorType = typename BaseType::RangeVectorType;
  using Constraint = typename BaseType::Constraint;
  typedef const std::function<RangeVectorType(double)> initialtype;
  typedef const std::function<double(double, double)> betatype;

private:
  const std::function<RangeVectorType(double)> default_X0 = X0::driven<RangeVectorType>();
  const std::vector<std::string> default_beta = {};
  const Constraint default_constraint = Constraint::measure;
  const double default_eps = 1.0e-4;

public:
  ValueDrivenMuscleModel(ConfigParameters &parameters) // TODO: only pass relevant parameters
      : BaseType(parameters),
        _X0(default_X0),
        _beta(default_beta),
        constraint_(default_constraint), // TODO: why not move to ModelDefault?
        eps_(default_eps) {
    eps_ = parameters.get<double>("model.I2.eps", default_eps);
    switch (parameters.getFromList("model.constraint", {"velocity", "measure"})) {
    case 0:constraint_ = Constraint::velocity;
    case 1:constraint_ = Constraint::measure;
    default:constraint_ = default_constraint;
    }
    setBeta(parameters.get<std::string>("model.beta.src", ""));
  }

  // TODO: replace with setParameters(k, v)
  void setX0(std::function<RangeVectorType(double)> p) {
    untested();
    _X0 = p;
  }
  void setBeta(std::string filename) {
    std::ifstream input(filename, std::ios::in);
    std::string line;
    while (std::getline(input, line)) {
      _beta.push_back(line);
    }
  }

  virtual RangeVectorType X0(const double s) const {
    if (_X0) {
      untested();
      return (_X0)(s);
    }

    RangeVectorType ret;
    ret[0] = s;
    ret[1] = 0.;
    return ret;
  }

  using BaseType::Force;
  using BaseType::gamma;
  using BaseType::K;

  virtual double regularisation(double const &s, const Entity &e) const {
    untested();

    // now what?!
    const double num = 2.0 * std::sqrt((eps_ + s) * (eps_ + 1.0 - s));
    const double den = 1.0 + 2.0 * eps_;

    return BaseType::regularisation(s, e) * (num * num * num) / (den * den * den);
  }

  virtual double mu(const double &s, const Entity &e) const {
    // now what?!
    const double num = 2.0 * std::sqrt((eps_ + s) * (eps_ + 1.0 - s));
    const double den = 1.0 + 2.0 * eps_;

    return BaseType::mu(s, e) * (num * num * num) / (den * den * den);
  }

  virtual double beta(double const &s, const Entity &e) const {
    const MeshObject<dim> &meshObject = dynamic_cast<const MeshObject<dim> &>(e);

    if (&meshObject == 0)
      throw exception_cast("Entity to MeshObject");

    unsigned int frame = static_cast<unsigned int>(meshObject.time() / meshObject.deltat());
    unsigned int position = static_cast<unsigned int>(s);
    std::string betaAtTimepoint = _beta.at(frame);
    std::string betaAtTimeAndPosition = split(betaAtTimepoint, ',').at(position);
    return std::stod(betaAtTimeAndPosition);
  }

  double energyDensity(double const &s, Entity const &e) const {
    return 0;
  }

  virtual Constraint constraint() const {
    return constraint_;
  }

private:
  std::function<RangeVectorType(double)> _X0;
  std::vector<std::string> _beta;
  Constraint constraint_;
  double eps_;

  //TODO: move to utils/helper class
  std::vector<std::string> split(const std::string &s, char delim) const {
    std::vector<std::string> result;
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
      result.push_back(item);
    }
    //TODO: check if order of values is correct
    return result;
  }
};
#endif // MODEL_LAMBDA_HH

// vim:ts=8:sw=2
