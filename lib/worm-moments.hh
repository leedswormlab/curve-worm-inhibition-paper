// (c) 2017 Felix Salfelder
//     University of Leeds
//

#ifndef LIB_WORM_MOMENTS_HH
#define LIB_WORM_MOMENTS_HH
#define NEWADD

#include "model.hh"
#include "femscheme.hh"
#include "entity.hh"
#include "stencil.hh"
#include "probe.hh"

// TODO: move non-worm specific stuff to meshobject : entity.
template<unsigned dim>
class worm_moments_ : public MeshObject<dim> {
public:
	class gammaprobe : entity_probe{
		gammaprobe(worm_moments_ const& w)
			: entity_probe("gamma", w) {
		}
	public:
		double evaluate() const{
			auto& w=prechecked_cast<worm_moments_ const&>(_e);
			return w.gamma();
		}
		friend class worm_moments_;
	};
	class energyprobe : entity_probe{
		energyprobe(worm_moments_ const& w)
			: entity_probe("energy", w) {
		}
	public:
		double evaluate() const{
			auto& w=prechecked_cast<worm_moments_ const&>(_e);
			return w.energy();
		}
		friend class worm_moments_;
	};
  class kappaprobe : entity_probe{
    kappaprobe(worm_moments_ const& w)
        : entity_probe("kappa", w), _index(0) {
    }
  public:
    double evaluate() const {
      untested();
      auto &w = prechecked_cast<worm_moments_ const &>(_e);
      assert(_index < howmany());
      double ret = w.curvature().at(_index);
      ++_index;
      _index %= howmany();
      return ret;
    }

    unsigned howmany() const {
      untested();
      auto &w = prechecked_cast<worm_moments_ const &>(_e);
      return w.curvature().N()*w.curvature().dim;
    }

  private:
    friend class worm_moments_;
    mutable unsigned _index;
  };
public:
	// it's abit weird.
	// if you know a better way, let me know.
	virtual probe* new_probe(const std::string& s){
		if(s=="energy"){
			return new energyprobe(*this);
		}else if(s=="gamma"){
			return new gammaprobe(*this);
		}else if(s=="kappa"){
			return new kappaprobe(*this);
		}else{
			throw exception_invalid("no such probe, " + s);
		}
	}
public:
	using RangeVectorType = RangeVector< dim >;
	using RangeCoTangent = RangeVector< dim >;
	typedef PiecewiseLinearFunction< dim > CurvatureFunctionType;
	typedef PiecewiseLinearFunction< dim > PositionFunctionType;
	typedef PiecewiseLinearFunction< dim > SomeFunctionType;
	typedef PiecewiseConstantFunction< 1 > PressureFunctionType;
	typedef ModelInterface<dim> ModelType;
	typedef double MeshCoordinate;

	using MeshObject<dim>::model;
	using MeshObject<dim>::tr_review_check_and_convert;
	using MeshObject<dim>::mesh;
	using MeshObject<dim>::time;
	using MeshObject<dim>::_time;
	using MeshObject<dim>::scheme; //?
	using MeshObject<dim>::schemehack; //?
	using MeshObject<dim>::tr_review_trunc_error;

	using MeshObject<dim>::_newtime; //?

//	using MeshObject<dim>::new_stencil;
	using MeshObject<dim>::new_mesh_function;
public: // TODO: move up
	worm_moments_(std::string name="worm")
		: MeshObject<dim>(name),
		  _force_data(0), _curvature_data(0)
	{ untested();
	}
	worm_moments_(ConfigParameters const & parameters, std::string name="worm")
		: MeshObject<dim>(name),
		  _force_data(0), _curvature_data(0)	{
	}


	~worm_moments_(){
	}


public:
	void set_param_by_name(const std::string& name, const std::string& value){ untested();
	  assert(0);
	}

//      void update_beta()
//  {
//	model().update_beta(*this);
//  }

	// BUG: this is needed in femscheme type
//	typedef DistanceModel< 3, 3 > model_typehack;
	 CurvatureFunctionType curvature() const{
		 return CurvatureFunctionType(mesh(),
		  SubArray< Vector >( _curvature_data.begin(), _curvature_data.end() ) );
	 }

	 // actually that makes sense for all meshes
	 // (not just worms)
	 // meshobject base type?
	 void computeCurvature() {
		 CurvatureFunctionType curvature(mesh(),
				 SubArray< Vector >( _curvature_data.begin(), _curvature_data.end() ));
#if 0
		std::cout << "correct " << std::endl;
		 // construct mass and stiffness matrices of size worlddim*N
		 typedef InverseMassMatrix< PositionFunctionType > InverseMassMatrixType;
		 InverseMassMatrixType inverseMass( mesh(), position() );
		
		 typedef StiffnessMatrix< PositionFunctionType > StiffnessMatrixType;
		 StiffnessMatrixType stiff( mesh(), position() );

		 Vector rhsData( dim * mesh().N() );
		 PositionFunctionType rhs( mesh(), SubArray< Vector >( rhsData.begin(), rhsData.end() ) );
		 stiff.call(position(), rhs );
		 rhs *= -1;

		 rhs.setDirichletBoundaries( 0 );
		 inverseMass.setDirichletBoundaries();
		 inverseMass.call( rhs, curvature );
#else

		 auto& W = _whatnot.solution();
		 auto const& X=position();
		 /// XtoW
		 unsigned wl = mesh().N();
		 double q[wl-1];
		 double M[wl-2];
		 RangeVectorType SX[wl-2];


		 q[0] = norm( X.eval(1) - X.eval(0) );
		 for(unsigned i=1; i<wl-1; ++i){ itested();
			 q[i] = norm( X.eval(i+1) - X.eval(i) );
			 M[i-1] = .5*(q[i] + q[i-1]);
			 SX[i-1] = (X.eval(i-1) - X.eval(i))/q[i-1] - (X.eval(i) - X.eval(i+1)) / q[i];
			 W.assign(i, SX[i-1]/M[i-1] );
		 }

		 W.assign(0,0);
		 W.assign(wl-1,0);
#endif
	 }
public:
	PiecewiseLinearFunction<dim> const & X0() const{ untested();
	  return _position.solution();
	}
	PiecewiseLinearFunction<dim> const & X1() const{
	  return _position.oldsolution();
	}
public:
	PiecewiseLinearFunction<dim> const & Y0() const{ untested();
		// incomplete(); // it's not the curvature, careful!
	  return _curvature.solution();
	}
	PiecewiseLinearFunction<dim> const & Y1() const{ untested();
	  return _curvature.oldsolution();
	}
	PiecewiseLinearFunction<dim> const & W0() const{ untested();
	  return _whatnot.solution();
	}
	PiecewiseLinearFunction<dim> const & W1() const{ untested();
	  return _whatnot.oldsolution();
	}
	// don't use
	PiecewiseLinearFunction<dim> const & X() const{
	  return _position.oldsolution();
	}
	PiecewiseLinearFunction<dim> const & Y() const{
	  return _curvature.oldsolution();
	}

	PiecewiseLinearFunction<dim>&	rhsposition(){
		return _position.rhs();
	}
	PiecewiseLinearFunction<dim>&	rhscurvature(){
		return _curvature.rhs();
	}
	PiecewiseConstantFunction<1>&	rhspressure(){
		return _pressure.rhs();
	}
public:

	// a hack. scheme must be global..?
	void expand(FemScheme<dim>& s){
		MeshObject<dim>::expand(s);
		assert(mesh().N());
		_force_data.resize(dim*mesh().N());
		_curvature_data.resize(dim*mesh().N()); // ask scheme?
		for(auto& i : _extra_forces){
			i->expand(_position.oldsolution());
		}

		// good idea?
		// no. _position.new_mesh_function("X", *this) ...
		new_mesh_function(_position, "X");
		new_mesh_function(_curvature, "Y");
		new_mesh_function(_pressure, "P");

		{ untested();// assert(false);
			new_mesh_function(_whatnot, "W");
			_stiffstencil.new_stencil("W", "X", *this);
			_massstencil.new_stencil("W", "W", *this); // M/e
			_massstencil2.new_stencil("Y", "Y", *this); // M
			_massstencil3.new_stencil("Y", "W", *this, "2"); // M
			_pmassstencil.new_stencil("Y", "W", *this); // -M/e
		}
		_pstiffstencil.new_stencil("X", "Y", *this);
		_presstencil.new_stencil("P", "X", *this);
		_dragstencil.new_stencil("X", "X", *this);

	}

// new.
  void eval(){} // eval

	double energy() const{
		return _energy;
	}

	PositionFunctionType const& position() const{ itested();
		return _position.solution(); // scheme().position();
	}
	PressureFunctionType const& pressure() const{ itested();
		return _pressure.solution(); // scheme().pressure();
	}

private: // worm state
  // different types of energy for debugging maybe
  // TODO properly tap model to dump internals into file.
  std::vector<double> _energyVector;

private: // stamping. called from matrix.hh
  bool velocitymodel() const{ untested();
	  return
		  model().constraint() == ModelType :: Constraint :: velocity;
  }
  bool measuremodel() const{
	  return
		  model().constraint() == ModelType :: Constraint :: measure;
  }
private: // baseclass?
  unsigned N() const{
	  return mesh().N();
  }
private: // overrides
  void tr_accept(){ untested();
	  computeCurvature();
  }
  void tr_regress(){ untested();
	  MeshObject<dim>::tr_regress();
  }
  void tr_advance(){
	  MeshObject<dim>::tr_advance();
  }
  void tr_load(){
	  trace2("worm tr_load", _time[0], _time[1]);
	  /* TODO
	   * each "load" method has its own mesh loop!
	   * should this be as one
	   */

	  // non symmetric terms
	  load_matrix_pres();
	  // drag, mass, Pmass and Pstiff terms
	  load_matrix_drag();
	  // stiffness only
	  load_matrix_stiff();
	  // more drag?
	  load_dynamic_drag();

	  // boundary conditions
	  _massstencil2.setDirichletRow( 0, true );
	  _massstencil2.setDirichletRow( mesh().N()-1, true );
	  _massstencil3.setDirichletRow( 0, false );
	  _massstencil3.setDirichletRow( mesh().N()-1, false );
	  _pmassstencil.setDirichletRow( 0, false );
	  _pmassstencil.setDirichletRow( mesh().N()-1, false );

	  // body forcing
	  load_force_rhs();
	  // explicit terms (plus hack?) and rhs of velocity model
	  load_more_rhs();
	  // beta and rhs of measure model
	  load_call_rhs();
  }
  double review(){
// 	 double timestep = tr_review_trunc_error(_head);
//     _newtime = tr_review_check_and_convert(timestep);
// 	 trace2("worm::review", _newtime, _time[1]);

// 	 message(bDEBUG, "review head %f, %f\n", _head[0], _head[1]);
// //	 message(bDEBUG, "review time %f, %f\n", _time[0], timestep);
// //	 message(bLOG, "DELTA %f %f %f\n", _time[0], timestep-_time[0], timestep );
// 	 return _newtime;
    return -1;
  }
private: // load implementation
  /**
   *  \brief loads stiffness matrix
   *
   *  Assembles the stiffness matrix which has terms of the form:
   *
   *   \int_0^1 1/|x_u| \phi_{i,u} \cdot \phi_{j,u} du.
   *
   *  Note that each block is diagonal.
   *  This is matrix S in the equation
   *
   *   M W + S X = 0.
   */
  void load_matrix_stiff() {
	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
	  // stiffness matrix
	  // [   |   |  ]
	  // [---+---+--]
	  // [\\\|   |  ]
	  // [---+---+--]
	  // [   |   |  ]
		  const RangeVectorType xe = X().evaluate( e );
		  const RangeVectorType xep = X().evaluate( e+1 );
		  RangeVectorType tau = ( xep - xe );
		  double q = tau.norm();
		  assert(q); // check non zero

		  double stiff = 1.0 / q;
		  assert(stiff); // check non zero
		  assert(stiff==stiff); // check non nan/inf

			  for( unsigned int k = 0; k < X().dim; ++k ) {
			  unsigned l=k;


			  if( e == 0 ) {
			  } else {
				  // add( dim*(e) + l, stiffOffset + dim*(e) + k, stiff );
				  // add( dim*(e+1) + l, stiffOffset + dim*(e) + k, -stiff );
				  _stiffstencil.add(e,   e, k, l, stiff);
				  _stiffstencil.add(e, e+1, k, l, -stiff);
			  }
			  if( e == mesh().N()-2 ) {
			  }else{
				  _stiffstencil.add(e+1, e,   k, l, -stiff);
				  _stiffstencil.add(e+1, e+1, k, l, stiff);
				  //add( dim*(e) + l, stiffOffset + dim*(e+1) + k, -stiff );
				  //add( dim*(e+1) + l, stiffOffset + dim*(e+1) + k, stiff );
			  }
		  } // k
	  } // e
  }

  /**
   *  \brief loads the non-symmetric pressure type matrix
   *
   *  Assembles the non-symmetric matrix which has terms of the form:
   *
   *   \int_0^1 \phi_{i,u} \tau \cdot q_j du.
   *
   *  Note that each block is 1 x dim (or the transpose).
   *  This is the matrix B (resp. B^T) is the equations:
   *
   *   B X = G
   *   M_D ( X - Xold ) / tau + B^T P - Sp Y = F
   */
  void load_matrix_pres(){
	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
		  // fixme: order:
		  for( unsigned int k = 0; k < X().dim; ++k ) {
			  // non symmetric terms
			  const RangeVectorType xe = X().evaluate( e );
			  const RangeVectorType xep = X().evaluate( e+1 );
			  RangeVectorType tau = ( xep - xe );
			  double q = tau.norm();
			  assert(q); // check non zero
			  tau /= q;

			  double pres = tau[k];
			  /* assert(pres) */ // allowed to be zero
			  assert(pres==pres); // check non nan/inf

			  // add to matrix
//			  const unsigned int presOffset = 2 * dim * mesh().N();
			  unsigned N=mesh().N();
			  if( 1 /*implicit_*/ or velocitymodel()){

				  /*   dim*N     dim*N   N-1
					* [         |        | H  ]
					* [  ...    |....    | E  ]
					* [         |        | R  ]
					* [         |        | E  ]
					* [ --------+--------+--- ]
					* [ ...     | ...    |..  ]
					* [ --------+--------+--- ]
					* [ HERE    | ...    |..  ]
					*/
				  _presstencil.add(              e*dim + k,  pres);
				  _presstencil.add((N-1)*dim   + e*dim + k, -pres);

				  _presstencil.add((N-1)*dim*2 + e*dim + k,  pres);
				  _presstencil.add((N-1)*dim*3 + e*dim + k, -pres);
			  }else{ untested();
			  }
		  } // k
	  } // e
  }

  /**
   *  \brief one massive function that does lots of matrix loading
   *
   *  \todo doc me!
   *  \todo split me up?
   */
  void load_matrix_drag(){
    /* TODO do better deltaT finder */
    // double deltaT = scaled_deltat(); // time provider?
    const double deltaT = _deltaT;

	  RangeVectorType xep=X().evaluate(0);
	  RangeVectorType yep=curvature().evaluate(0);

	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
		  MeshCoordinate s=.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
		  double K=model().K(s);
		  double E=model().regularisation(s, *this);
		  double mu=model().mu( s, *this );

//Jack		  trace2("reg", s, E);

		  RangeVectorType xe = xep;
		  xep = X().evaluate( e+1 );

		  RangeVectorType tau = ( xep - xe );
		  double q = tau.norm();
		  assert(q==q); // check non nan/inf
		  assert(q); // check non zero
		  // trace4("worm X", e, xe,xep, q);
		  tau /= q;

		  for( unsigned k=0; k<dim; ++k ){
		    double mass=.5 * q;
		    assert(mass); // check non zero
		    assert(mass==mass); // check non nan/inf

		    {
		      /**
		       *   mass matrix
		       */
		      // mass matrix
		      // [   |   |  ]
		      // [---+---+--]
		      // [   | \ |  ]
		      // [---+---+--]
		      // [   |   |  ]
		      _massstencil.add_(e, e, k, k, mass);
		      _massstencil.add_(e+1, e+1, k, k, mass);
		    } // end of massstencil
			  // trace4("MASS", s, k, mass, mass_EN);

		    { itested();
		      /**
		       *   another mass matrix
		       */
		      //_massstencil2.add(e, e, k, k, mass);
		      //_massstencil2.add(e+1, e+1, k, k, mass);
		      _massstencil2.add(dim*(dim*e +    k) + k, mass);
		      _massstencil2.add(dim*(dim*(e+1) +k) + k, mass);
		    } // end of massstencil2

		    {
		      /**
		       *  a mass matrix weighted by E
		       */
		      // DUP
		      //same as 2, but different matrix location...
		      //hmmm
		      // _massstencil3.add(e,e,k,k, -mass_E);
		      // _massstencil3.add(e+1, e+1, k,  k, -mass_E);
		      _massstencil3.add(dim*(dim*e +    k) + k, -mass*E);
		      _massstencil3.add(dim*(dim*(e+1) +k) + k, -mass*E);
		    } // end of massstencil3

		    for( unsigned int l = 0; l < dim; ++l ) {

		      // drag term
		      // load_dynamic?
		      {
			/**
			 *  drag matrix
			 */
			const double id = (double) (k == l);
			const double hilf = ( 1.0 - K ) * tau[l]*tau[k];
			const double drag = 0.5 * q * ( K * id +  hilf ) / deltaT;
			/* assert(drag) */; // allow zero
			assert(drag==drag); // check non nan/inf


			// add to matrix, dim=3
			/* [ \       |     |    ]
			 * [   3x3   |     |    ]
			 * [   diag  |.... |..  ]
			 * [  stamps |     |    ]
			 * [        \|     |    ]
			 * [ --------+-----+--- ]
			 * [ ...     | ... |..  ]
			 */
			//  1*
			//  *1
			//    2*
			//    *2
			//      2*
			//      *2
			//        1*
			//        *1

			//					  assert(_dragstencil);
			// trace4("",e,k,l,drag);
#ifdef DEBUG
			std::cout << "DRAG " << e << " "  << l << " " << k << ": " << K << " " << hilf << " " << drag << "\n";
			std::cout << "DRAG " << tau[l] << " "  << tau[k] << "\n";
#endif
			//_dragstencil.add(dim*(dim*e +     k) + l, drag);
			//_dragstencil.add(dim*(dim*(e+1) + k) + l, drag);
			_dragstencil.add_(e,e,k,l,drag); // or l,k?
			_dragstencil.add_(e+1,e+1,k,l,drag);
		      } // end of dragstencil

		      {
			/**
			 *  projected mass matrix
			 */
			// double mass = 0.5 * q;

			// something like this
			const double id = (double) (k == l);
			const double hilf = id - tau[l]*tau[k];

			{ itested();
			  double stamp = mu * mass * hilf / deltaT;
			  assert(stamp==stamp); // check non nan/inf
			  _pmassstencil.add_(e, e, k, l, -stamp);
			  _pmassstencil.add_(e+1, e+1, k, l, -stamp);
			}
		      } // end of pmassstencil

		      //-======================================================
		      {
			/**
			 *  projected stiffness matrix
			 */
			const double Ptau = ( (double) (k == l) - tau[l]*tau[k] );
#ifdef DEBUG
			std::cout << "PTAU "  << k << l << " " << Ptau << "\n";
#endif
			assert(Ptau==Ptau); // check non nan/inf
			double pStiff = -Ptau/q;

			assert(pStiff==pStiff); // check non nan/inf
			// [ |\| ]
			// [-+-+-]
			// [ | | ]
			// [-+-+-]
			// [ | | ]
			// stamp_symmetric y <+ x ...
			//				  trace2("pss", l, pStiff);
			_pstiffstencil.add(e,   e,   k, l, pStiff);
			_pstiffstencil.add(e+1, e+1, k, l, pStiff);

			_pstiffstencil.add(e+1, e,   k, l, -pStiff);
			_pstiffstencil.add(e,   e+1, k, l, -pStiff);
		      } // end of pstiffstencil
		    } // l loop
		  } // k loop

	  }


  }

  /**
   *  \brief loads dynamic drag
   *
   *  this is not used in this formulation
   */
  void load_dynamic_drag() {
    return;
  }

  /**
   *  \brief loads rhs from explicit terms
   *
   *  this uses the drag and viscosity stencils to add to the right hand sides
   */
  void load_more_rhs(){
    /* TODO: check is this and add or set?
     * which do we want
     */
	  _dragstencil.mv(rhsposition(), X1());
	  _pmassstencil.mv(rhscurvature(), W1());
  }

  /**
   *  \brief loads forcing rhs
   *
   *  beta for moment and gamma for continuity equation only
   */
  void load_call_rhs(){
	  auto& rhsY = rhscurvature();
	  auto& rhsP = rhspressure();

	  // element loop
	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
		  // extract geometry
		  const double s = 0.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
		  const auto xe = X().evaluate( e );
		  const auto xep = X().evaluate( e+1 );
		  const double q = ( xe - xep ).norm();
		  RangeVectorType nu = ( xep - xe ).perp();
		  assert(q); // check non zero
		  nu /= q;

		  // integrate curvature forcing
		  for( unsigned int ie = 0; ie < 2; ++ie ) {
			  const auto sie = mesh().u( e + ie );

			  RangeVectorType valueY;
			  double beta=model().beta( sie, *this );
			  /* assert( beta ); */ // allowed to be zero
			  assert(beta == beta); // check non nan/inf

			  for( unsigned int d = 0; d < X().dim; ++d ) {

		  		valueY[ d ] = 0.5 * q * beta * nu[ d ];
			  }

			  rhsY.add( e+ie, valueY );
		  }

		  // integrate pressure forcing
		  if( measuremodel() ){ untested();
			  assert(mesh().h(1)==mesh().H(1));
			  double gamma = model().gamma(s);
			  assert(gamma); // check non zero
			  assert(gamma==gamma); // check non nan/inf
			  rhsP[e] = mesh().H( e ) * -gamma;
		  }else{ untested();
		  }
	  }

     rhsY.setDirichletBoundaries( 0 ); // hmm, here?
  }

  double gamma() const{
//	  average maybe?
		return model().gamma( 0/*?*/ );
  }


  /**
   *  \brief load position forcing for rhs
   *
   *  \todo doc me!
   */
	void load_force_rhs(){
		if(!_extra_forces.size()){
			// HACK, bypass force, if there's no other forc
			// (e.g. as in worm-model)
			return;
		}else{
			// assert(0); // for now.
		}
		_force_data.clear();

		PiecewiseLinearFunction<dim> tmpforce(mesh(),
			  	SubArray< Vector >( _force_data.begin(), _force_data.end() ));

		// BUG: this should be an extra force
		// this is eval. do it here, for now.
		for( unsigned int e = 0; e < mesh().N()-1; ++e ) { itested();
			// extract geometry
//			const double s = 0.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
			const auto xe = X().evaluate( e );
			const auto xep = X().evaluate( e+1 );
			const double q = ( xe - xep ).norm();
			RangeVectorType nu = ( xe - xep ).perp();
			nu /= q;


			// integrate position and curvature forcing
			for( unsigned int ie = 0; ie < 2; ++ie ) { itested();
				const auto xie = X().evaluate( e + ie );
//				const auto sie = mesh().u( e + ie );

				// the distanceforce
				RangeVectorType f = model().Force( xie );
				assert(f.size()==X().dim);

//				message(bTRACE, "size %d\n", f.size());
//				message(bTRACE, "f size %d\n", _force_data.size());
				RangeVectorType valueX;
				for( unsigned d=0; d < X().dim; ++d ) { itested();
					valueX[d] += 0.5 * q * f[ d ];
				}

				assert((ie+e)*3+2<_force_data.size());
				tmpforce.add(ie+e, valueX);
			}
		}

		// collect the extra forces
		for(auto& i : _extra_forces){
			// hmm delegate or stamp from here?
			i->add_rhs(tmpforce);
		}

		auto& R = rhsposition();
		assert(R.size()==tmpforce.size());
//		assert(scheme().N()==tmpforce.N());
		for(unsigned i=0; i<tmpforce.N(); ++i){
			R.add(i, tmpforce.evaluate(i));
		}

	} // worm_moments_3d::load_force_rhs

public:
	template<class S>
	void dump_energies(S& s) const{ untested();
		for(auto& i : _extra_forces){ untested();
			s << "," << i->energy();
		}
	}
public: // parameters
	virtual void set_deltaT( const double x ){ untested();
	  _deltaT = x;
	}
public: // HACK. cleanup later
  std::vector< ForceFunctionBase<dim>* > _extra_forces;
  virtual void initialiseXY() {
	  RangeVectorType Xu(0);
	  //PositionFunctionType HACKposition( mesh(),
		//	  SubArray< Vector >( _solution_data.begin(), _solution_data.end() ) );

	  for( unsigned int j = 0; j < mesh().N(); ++j )
	  {
		  const double uj = mesh().u(j);
		  const auto Xuj = model().X0( uj );
		  // HACK HACK
		  _position.solution().assign( j, Xuj );
	  }

	  // this is initialising W, Y is fine being zero
	  computeCurvature();
  }
private:
//  PiecewiseLinearFunction<dim> _force_data;
  Vector _force_data;
  mutable Vector _curvature_data; // BUG: altered by probe
                                  // should be in accept?
  double _energy;

  SystemMatrix<PositionFunctionType, void>* _matrix_hack;
private: // hmmm
  ModelInterface<dim> const& model() const{
	  auto mm=&MeshObject<dim>::model();
	  assert(mm);
	  ModelInterface<dim> const* m=prechecked_cast<ModelInterface<dim> const* >(mm);
	  assert(m);
	  return *m;
  }

private: // meshfunctions
	MeshFunction<PositionFunctionType> _position; // "X"
	MeshFunction<CurvatureFunctionType> _curvature; // "Y"
	MeshFunction<CurvatureFunctionType> _whatnot; // "W"
	MeshFunction<PressureFunctionType> _pressure; // "P"
private: // stencils
     // [ D | S || B ]
     // [---+---++---]
     // [ S | M ||   ]
     // [---+---++---]
     // [---+---++---]
     // [ BT|   ||   ]
  // stencil_dim_diag<dim>  _dragstencil; // D
  stencil_dim_3diag<dim>  _dragstencil; // D  required for drag forses in skel
  stencil_dim_diag<dim>  _massstencil; // M/e
  stencil_dim_diag<dim>  _massstencil2; // M   if eta
  stencil_dim_diag<dim>  _massstencil3; // M   if eta
  stencil_dim_diag<dim>  _pmassstencil; // -M + ...  if eta
  stencil_asym<dim>      _presstencil; // B
  stencil_dim_3diag<dim> _stiffstencil;
  stencil_dim_3diag<dim> _pstiffstencil;

private: // parameters
	double _deltaT;
}; // worm_moments_3d

#endif // guard
