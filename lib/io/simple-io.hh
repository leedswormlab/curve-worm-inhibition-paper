#pragma once

#include <ostream>
#include <sstream>
#include <limits>

#include "../entity/entity.hh"
#include "../discretefunction.hh"
#include "../vector.hh"
#include "../timeprovider.hh"
#include "../parameter.hh"

class SimpleWriter : public Entity_base {
  template< unsigned int dim >
  using PWL_Probe_ptr = Probe_ptr< const PiecewiseLinearFunction<dim> >;
  template< unsigned int dim >
  using PWC_Probe_ptr = Probe_ptr< const PiecewiseConstantFunction<dim> >;
  template< unsigned int dim >
  using RV_Probe_otf = Probe_otf< RangeVector<dim> >;
  template< unsigned int dim >
  using PWL_Probe_otf = Probe_otf< const PiecewiseLinearFunction<dim>& >;
  template< unsigned int dim >
  using PWC_Probe_otf = Probe_otf< const PiecewiseConstantFunction<dim>& >;
  using Double_probe_otf = Probe_otf< double >;

public:
  SimpleWriter( const TimeProvider& time_provider, const std::string prefix,
		const double output_time_step = 0.0,
		const double output_start = 0.0 )
    : Entity_base( "simple_writer" ),
      _time_provider( time_provider ),
      _prefix( prefix ),
      _output_time_step( output_time_step ),
      _next_write_time( output_start ),
      _essential_ports( Parameters::getStringVector( "output.simple.essential_ports" ) ),
      _optional_ports( Parameters::getStringVector( "output.simple.optional_ports" , "" ) ) {}

  ~SimpleWriter() {
    // close any remaining files
    for( auto& p : _files ) {
      auto& f = p.second;
      if( f.is_open() ) {
	f.close();
      }
    }
  }

  virtual std::vector< std::string > essential_ports() const final {
    return _essential_ports;
  }
  virtual std::vector< std::string > optional_ports() const final {
    return _optional_ports;
  }

  void load() final {
    for (const auto &pair : probe_map()) {
      const auto &full_name = pair.first;
      const auto &probe_ptr = pair.second;
      const auto name = probe_ptr->split_name().second;

      if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_ptr<1>>(probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<2>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<3>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<1>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<2>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<3>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_otf<1>>(probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<2>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<3>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<1>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<2>>(
			 probe_ptr)) {
	open_file( name );
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<3>>(
			 probe_ptr)) {
	open_file( name );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<1>>( probe_ptr ) ) {
	open_file( name );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<2>>( probe_ptr ) ) {
	open_file( name );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<3>>( probe_ptr ) ) {
	open_file( name );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<4>>( probe_ptr ) ) {
	open_file( name );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<5>>( probe_ptr ) ) {
	open_file( name );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<6>>( probe_ptr ) ) {
	open_file( name );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const Double_probe_otf >( probe_ptr ) ) {
	open_file( name );
      } else {
        std::cerr << "WARNING: " << long_label() << " unable to output " << name << std::endl;
      }
    }
  }

  void tr_begin_accept() final {
    if( _time_provider.time() < _next_write_time ) {
      return;
    }
    _next_write_time += _output_time_step;
    do_write();
  }

  void tr_accept() final {
    if( _time_provider.time() < _next_write_time ) {
      return;
    }
    _next_write_time += _output_time_step;
    do_write();
  }

protected:
  void open_file( const std::string& name ) {
    // choose filename
    const std::string fn = _prefix + "/" + name + ".dat";

    // check if it already exists in entity
    auto it = _files.find( name );
    if( it != _files.end() ) {
      // error!
      throw exception_invalid( "file for probe " + name + " already in " + long_label() );
    }

    // open file
    std::ofstream& f = _files[ name ];
    if( not f.is_open() ) {
      f.open( fn );
    }

    // check its open
    if( not f.is_open() ) {
      throw exception_nosuchfile( "unable to open file " + fn );
    }

    // set to full precision
    f.precision( std::numeric_limits<double>::max_digits10 );
  }

  void do_write() {
    for (const auto &pair : probe_map()) {
      const auto &probe_ptr = pair.second;
      const auto name = probe_ptr->split_name().second;

      if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_ptr<1>>(probe_ptr)) {
	auto& file = _files.at( name );
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<2>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<3>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<1>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<2>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<3>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_otf<1>>(probe_ptr)) {
	auto& file = _files.at( name );
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<2>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<3>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<1>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<2>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<3>>(
			 probe_ptr)) {
	auto& file = _files.at( name );
	  write(file, name, casted_probe_ptr->evaluate());
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<1>>( probe_ptr ) ) {
	auto& file = _files.at( name );
	write( file, name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<2>>( probe_ptr ) ) {
	auto& file = _files.at( name );
	write( file, name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<3>>( probe_ptr ) ) {
	auto& file = _files.at( name );
	write( file, name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<4>>( probe_ptr ) ) {
	auto& file = _files.at( name );
	write( file, name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<5>>( probe_ptr ) ) {
	auto& file = _files.at( name );
	write( file, name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<6>>( probe_ptr ) ) {
	auto& file = _files.at( name );
	write( file, name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const Double_probe_otf >( probe_ptr ) ) {
	auto& file = _files.at( name );
	write( file, name, casted_probe_ptr->evaluate() );
      }
    }
  }

  template< unsigned dim >
  void write( std::ofstream& file, const std::string& name,
	      const PiecewiseLinearFunction<dim>& func ) const {
    file << _time_provider.time();
    for( unsigned int n = 0; n < func.footprint_size(); ++n ) {
      const double val = func.at(n);
      assert( val == val );
      file << " " << val;
    }
    file << std::endl;
  }

  template< unsigned dim >
  void write( std::ofstream& file, const std::string& name,
	      const PiecewiseConstantFunction<dim>& func ) const {
    file << _time_provider.time();
    for( unsigned int n = 0; n < func.footprint_size(); ++n ) {
      const double val = func.at(n);
      assert( val == val );
      file << " " << val;
    }
    file << std::endl;
  }

  template <unsigned dim>
  void write(std::ofstream &file, const std::string &name,
	     const RangeVector<dim>& vec) const {
    file << _time_provider.time();
    for (unsigned int d = 0; d < dim; ++d) {
      const double val = vec.at(d);
      assert( val == val );
      file << " " << val;
    }
    file << std::endl;
  }

  void write(std::ofstream &file, const std::string &name,
	     const double& val) const {
    file << _time_provider.time();
    assert( val == val );
    file << " " << val;
    file << "\n";
  }
private:
  const TimeProvider& _time_provider;
  const std::string _prefix;

  const double _output_time_step;
  double _next_write_time;

  const std::vector< std::string > _essential_ports;
  const std::vector< std::string > _optional_ports;

  std::map< std::string, std::ofstream > _files;
};
