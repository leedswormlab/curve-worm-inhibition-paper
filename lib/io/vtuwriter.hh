#pragma once

#include <iomanip>
#include <sstream>
#include <limits>
#include <list>

#include "../parameter.hh"
#include "../mesh.hh"
#include "../timeprovider.hh"
#include "../probe/probe.hh"
#include "../error.hh"

#include "../entity/entity.hh"
#include "../discretefunction.hh"

class VTUWriter_entity : public Entity_base {
  template< unsigned int dim >
  using PWL_Probe_ptr = Probe_ptr< const PiecewiseLinearFunction<dim> >;
  template< unsigned int dim >
  using PWC_Probe_ptr = Probe_ptr< const PiecewiseConstantFunction<dim> >;
  template< unsigned int dim >
  using RV_Probe_otf = Probe_otf< RangeVector<dim> >;
  template< unsigned int dim >
  using PWL_Probe_otf = Probe_otf< const PiecewiseLinearFunction<dim>& >;
  template< unsigned int dim >
  using PWC_Probe_otf = Probe_otf< const PiecewiseConstantFunction<dim>& >;

public:
  VTUWriter_entity( const Mesh& mesh, const TimeProvider& time_provider, const std::string prefix, const double io_timestep,
		    const double output_start = 0.0 )
    : Entity_base( "vtu_writer" ),
      _mesh( mesh ),
      _time_provider( time_provider ),
      _prefix( prefix ),
      _io_timestep( io_timestep ),
      _next_write_time( output_start ),
      _essential_ports( Parameters::getStringVector( "output.vtu.essential_ports" ) ),
      _optional_ports( Parameters::getStringVector( "output.vtu.optional_ports", "" ) ) {}

  ~VTUWriter_entity() {
    if( _pvd ) {
      closePvd();
    }
  }

  virtual std::vector< std::string > essential_ports() const final {
    std::vector< std::string > ret( _essential_ports );

    if( std::find( ret.begin(), ret.end(), "mechanics.position") == ret.end() ) {
      ret.push_back( "mechanics.position" );
    }

    return ret;
  }
  virtual std::vector< std::string > optional_ports() const final {
    return _optional_ports;
  }

  void load() final {
    for (const auto &pair : probe_map()) {
      const auto &full_name = pair.first;
      const auto &probe_ptr = pair.second;
      const auto name = probe_ptr->split_name().second;

      if (not _pvd.is_open()) {
	openPvd();
      }

      if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_ptr<1>>(probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<2>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<3>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<1>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<2>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<3>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		 std::dynamic_pointer_cast<const PWL_Probe_otf<1>>(probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<2>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<3>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<1>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<2>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<3>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const RV_Probe_otf<1>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const RV_Probe_otf<2>>(
			 probe_ptr)) {
      } else if (auto casted_probe_ptr =
                     std::dynamic_pointer_cast<const RV_Probe_otf<3>>(
                         probe_ptr)) {
      } else {
        std::cerr << "WARNING: " << long_label() << " unable to output " << name
                  << std::endl;
      }
    }
  }

  void tr_begin_accept() final {
    if( _time_provider.time() >= _next_write_time ) {
      do_write();

      _next_write_time += _io_timestep;
    }
  }

  void tr_accept() final {
    if( _time_provider.time() >= _next_write_time ) {
      do_write();

      _next_write_time += _io_timestep;
    }
  }
  protected:
  std::string filename() const {
    std::stringstream ss;
    ss << _prefix << "/" << filename_head();
    return ss.str();
  }
  std::string filename_head() const {
    std::stringstream ss;
    ss << "output_" << _time_provider.iteration() << ".vtu";
    return ss.str();
  }
  std::string pvd_filename() const {
    std::stringstream ss;
    ss << _prefix << "/"
       << "output.pvd";
    return ss.str();
  }

void do_write() {
    std::ofstream file;
    file.open(filename().c_str());
    assert(file.is_open());

    // set file to full precision
    file.precision(std::numeric_limits<double>::max_digits10);

    file << "<?xml version=\"1.0\"?>\n";
    file << "<VTKFile type=\"UnstructuredGrid\" version=\"0.1\" "
	    "byte_order=\"LittleEndian\">\n";
    file << "  <UnstructuredGrid>\n";
    file << "    <Piece NumberOfPoints=\"" << _mesh.N() << "\" NumberOfCells=\""
	 << _mesh.N() - 1 << "\">\n";

    file << "      <PointData Scalars=\"u\">\n";
    write(file, "u", _mesh);
    for (const auto &pair : probe_map()) {
      const auto &full_name = pair.first;
      const auto &probe_ptr = pair.second;
      const auto name = probe_ptr->split_name().second;

      if (name == "position") {
	continue;
      }

      if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_ptr<1>>(probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<2>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<3>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_otf<1>>(probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<2>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<3>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      }
    }
    file << "      </PointData>\n";

    file << "      <CellData>\n";
    for (const auto &pair : probe_map()) {
      const auto &full_name = pair.first;
      const auto &probe_ptr = pair.second;
      const auto name = probe_ptr->split_name().second;

      if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWC_Probe_ptr<1>>(probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<2>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_ptr<3>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWC_Probe_otf<1>>(probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<2>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWC_Probe_otf<3>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      }
    }
    file << "      </CellData>\n";

    file << "      <Points>\n";
    for (const auto &pair : probe_map()) {
      const auto &probe_ptr = pair.second;
      const auto name = probe_ptr->split_name().second;
      if (name != "position") {
	continue;
      }

      if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_ptr<1>>(probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<2>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_ptr<3>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
	      std::dynamic_pointer_cast<const PWL_Probe_otf<1>>(probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<2>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      } else if (auto casted_probe_ptr =
		     std::dynamic_pointer_cast<const PWL_Probe_otf<3>>(
			 probe_ptr)) {
	write(file, name, casted_probe_ptr->evaluate());
      }
    }
    file << "      </Points>\n";

    file << "      <Cells>\n";
    file << "        <DataArray type=\"Int32\" Name=\"connectivity\" "
	    "format=\"ascii\">\n";
    file << "         ";
    for ( auto&& element : _mesh.elements() ) {
      file << " " << element.left().index() << " " << element.right().index();
    }
    file << "\n";
    file << "        </DataArray>\n";
    file << "        <DataArray type=\"Int32\" Name=\"offsets\" "
	    "format=\"ascii\">\n";
    file << "         ";
    for ( auto&& element : _mesh.elements() ) {
      file << " " << 2 * ( element.index() + 1);
    }
    file << "\n";
    file << "        </DataArray>\n";
    file << "        <DataArray type=\"Int32\" Name=\"types\" "
            "format=\"ascii\">\n";
    file << "         ";
    for( unsigned e_idx = 0; e_idx < _mesh.elements().size(); ++e_idx ) {
      file << " " << 3;
    }
    file << "        </DataArray>\n";
    file << "      </Cells>\n";
    file << "    </Piece>\n";
    file << "  </UnstructuredGrid>\n";
    file << "</VTKFile>\n";

    file.close();


    if( not _pvd.is_open() ) {
      openPvd();
    }

    _pvd << "    <DataSet"
	 << " timestep=\"" << _time_provider.time() << "\""
	 << " group=\"\" part=\"0\"\n"
	 << "             file=\"" << filename_head() << "\"/>" << std::endl;
  }

  void openPvd() {
    if( _pvd.is_open() ) {
      return;
    }

    const std::string pvd_fn = _prefix + "/" + "output.pvd";
    _pvd.open( pvd_fn.c_str() );
    assert( _pvd.is_open() );

    _pvd << "<?xml version=\"1.0\"?>" << std::endl;
    _pvd << "<VTKFile type=\"Collection\" version=\"0.1\" "
	    "byte_order=\"LittleEndian\">"
	 << std::endl;
    _pvd << "  <Collection>" << std::endl;
  }

  void closePvd() {
    if (_pvd.is_open()) {
      _pvd << "  </Collection>" << std::endl;
      _pvd << "</VTKFile>" << std::endl;

      _pvd.close();
      message(bLOG, "data collated in " + pvd_filename() );
    }
  }

  template <unsigned dim>
  void write(std::ofstream &file, const std::string &name,
	     const PiecewiseLinearFunction<dim> &func) const {
    const unsigned noc = (name == "position") ? 3 : dim;
    file << "        <DataArray type=\"Float32\" "
	 << "Name=\"" << name << "\" "
	 << "NumberOfComponents=\"" << noc << "\">\n";
    for (auto &&v : _mesh.vertices()) {
      file << "         ";
      const auto func_v = func.evaluate(v.index());
      for (unsigned d = 0; d < noc; ++d) {
	if (d < dim) {
	  const double val = func_v.at(d);
	  assert( val == val );
	  file << " " << val;
	} else {
	  file << " " << 0.0;
	}
      }
      file << "\n";
    }
    file << "        </DataArray>\n";
  }

  template< unsigned dim >
  void write( std::ofstream& file, const std::string& name,
	      const PiecewiseConstantFunction<dim>& func ) const {
    file << "        <DataArray type=\"Float32\" "
	 << "Name=\"" << name << "\" "
	 << "NumberOfComponents=\"" << dim << "\">\n";
    for (auto &&e : _mesh.elements()) {
      file << "         ";
      const auto func_e = func.evaluate(e.index());
      for (unsigned d = 0; d < dim; ++d) {
	const double val = func_e.at(d);
	assert( val == val );
	file << " " << val;
      }
      file << "\n";
    }
    file << "        </DataArray>\n";
  }

  void write( std::ofstream& file, const std::string& name,
	      const Mesh& mesh ) const {
    file << "        <DataArray type=\"Float32\" "
	 << "Name=\"" << name << "\" "
	 << "NumberOfComponents=\"" << 1 << "\">\n";
    for (auto &&v : _mesh.vertices()) {
      file << "          " << v.u() << "\n";
    }
    file << "        </DataArray>\n";
  }

private:
  const Mesh& _mesh;
  const TimeProvider& _time_provider;
  const std::string _prefix;
  std::ofstream _pvd;
  const double _io_timestep;
  double _next_write_time;
  const std::vector< std::string > _essential_ports;
  const std::vector< std::string > _optional_ports;
};
