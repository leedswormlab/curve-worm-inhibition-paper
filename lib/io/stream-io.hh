#pragma once

#include <ostream>
#include <sstream>

#include "../entity/entity.hh"
#include "../vector.hh"
#include "../timeprovider.hh"
#include "../parameter.hh"

class StreamWriter : public Entity_base {
  template< unsigned int dim >
  using RV_Probe_otf = Probe_otf< RangeVector<dim> >;
public:
  StreamWriter( const TimeProvider& time_provider, std::ostream& stream,
		const double io_timestep,
		const double output_start = 0.0 )
    : Entity_base( "stream_writer" ),
      _time_provider( time_provider ),
      _stream( stream ),
      _io_timestep( io_timestep ),
      _next_write_time( output_start ),
      _essential_ports( Parameters::getStringVector( "output.stream.essential_ports" ) ),
      _optional_ports( Parameters::getStringVector( "output.stream.optional_ports" , "" ) ) {}

  virtual std::vector< std::string > essential_ports() const final {
    return _essential_ports;
  }
  virtual std::vector< std::string > optional_ports() const final {
    return _optional_ports;
  }

  void load() final {
    for (const auto &pair : probe_map()) {
      const auto &name = pair.first;
      const auto &probe_ptr = pair.second;

      if ( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<1>>( probe_ptr ) ) {
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<2>>( probe_ptr ) ) {
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<3>>( probe_ptr ) ) {
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<4>>( probe_ptr ) ) {
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<5>>( probe_ptr ) ) {
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<6>>( probe_ptr ) ) {
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const Probe_otf<double> >( probe_ptr ) ) {
      } else {
        std::cerr << "WARNING: " << long_label() << " unable to output " << name << std::endl;
      }
    }
  }

  void tr_begin_accept() final {
    if( _time_provider.time() < _next_write_time ) {
      return;
    }
    _next_write_time += _io_timestep;
    do_write();
  }

  void tr_accept() final {
    if( _time_provider.time() < _next_write_time ) {
      return;
    }
    _next_write_time += _io_timestep;
    do_write();
  }

protected:
  void do_write() {
    for (const auto &pair : probe_map()) {
      const auto &name = pair.first;
      const auto &probe_ptr = pair.second;

      if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<1>>( probe_ptr ) ) {
	write( name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<2>>( probe_ptr ) ) {
	write( name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<3>>( probe_ptr ) ) {
	write( name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<4>>( probe_ptr ) ) {
	write( name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<5>>( probe_ptr ) ) {
	write( name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const RV_Probe_otf<6>>( probe_ptr ) ) {
	write( name, casted_probe_ptr->evaluate() );
      } else if( auto casted_probe_ptr =
		 std::dynamic_pointer_cast< const Probe_otf<double> >( probe_ptr ) ) {
	write( name, casted_probe_ptr->evaluate() );
      }
    }
  }

  template <unsigned dim>
  void write(const std::string &name,
	     const RangeVector<dim>& vec) const {
    _stream << "# " << name << ": ";
    _stream << "(time: " << _time_provider.time() << ")";

    const auto old_precision = _stream.precision();
    _stream.precision( std::numeric_limits<double>::max_digits10);

    for (unsigned int d = 0; d < dim; ++d) {
      _stream << " " << vec.at(d);
    }

    _stream.precision( old_precision );
    _stream << std::endl;
  }

  void write(const std::string &name,
	     const double val ) const {
    _stream << "# " << name << ": ";
    _stream << "(time: " << _time_provider.time() << ")";
    const auto old_precision = _stream.precision();
    _stream.precision( std::numeric_limits<double>::max_digits10);
    _stream << " " << val;
    _stream.precision( old_precision );
    _stream << std::endl;
  }

private:
  const TimeProvider& _time_provider;
  std::ostream& _stream;

  const double _io_timestep;
  double _next_write_time;
  const std::vector< std::string > _essential_ports;
  const std::vector< std::string > _optional_ports;
};
