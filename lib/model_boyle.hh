#ifndef MODEL_BOYLE_HH
#define MODEL_BOYLE_HH

#include <memory>
#include <iostream>
#include <functional>
#include <vector>
#include "vector.hh"
#include "timeprovider.hh"
#include "parameter.hh"
#include "matrix.hh"
#include "readMat.hh"
#include <vector>
#include "worm.hh"
#ifdef OpenCV_FOUND
#include "distancefunction.hh"
#include "forcefunction.hh"
#endif

template< const unsigned int mydim >
struct BoyleModel
  : public ModelDefault< mydim > { //
private:
  using BaseType = ModelDefault< mydim >;
  typedef typename BaseType::MeshCoordinate MeshCoordinate;

public:
  static const unsigned int dim = BaseType :: dim;
  using RangeVectorType = typename BaseType :: RangeVectorType;


    BoyleModel( ConfigParameters& parameters )
    : BaseType( parameters),
      //curvatureData_( mesh.N()*dim ),
      //curvature_( mesh, SubArray< Vector >( curvatureData_.begin(), curvatureData_.end() ) ),
      //positionData_( mesh.N()*dim ),
      //position_( mesh, SubArray< Vector >(positionData_.begin(), positionData_.end() ) ),
      N(parameters.get<double>("mesh.N")),
      M(parameters.get<double>("model.beta.M")),
      eps_( parameters.get<double>( "model.I2.eps" ) ),
      timeToUpdate(0.0),
      dt_(parameters.get<double>("time.deltaT")),
      a_(parameters.get<double>("model.torque.a")*3.3),
      b_(parameters.get<double>("model.torque.b")*3.3),
      tau(N),
      dtau(N),
      propFeedback(N),
      Switch(2, std::vector<double>(N)), // [0] Dorsal  [1] Ventral
      thd_0(parameters.get<double>("model.threshold.thd_0")),
      thd_1(parameters.get<double>("model.threshold.thd_1")),
      thv_00(parameters.get<double>("model.threshold.thv_00")),
      thv_01(parameters.get<double>("model.threshold.thv_01")),
      thv_10(parameters.get<double>("model.threshold.thv_10")),
      thv_11(parameters.get<double>("model.threshold.thv_11")),
      //nu_out(NULL),
      reset(parameters.get<bool>("model.threshold.reset")),
      Activation(NULL),
      Beta(NULL),
      Kappa(NULL),
      feedback(NULL),
      pointx(NULL),
      pointy(NULL)
      {
        for(int j=0;j<N;j++)
        {
                Switch[0][j] = 0;
                Switch[1][j] = 1; //0
        }
        Activation = new std::ofstream("Activation.txt");
        if( !Activation->is_open() )
        {
          std::cerr << "Error: Activation.txt didn't open" << std::endl;
        }
        Beta = new std::ofstream("Beta.txt");
        if( !Beta->is_open() )
        {
          std::cerr << "Error: Beta didn't open" << std::endl;
        }
        Kappa = new std::ofstream("Kappa.txt");
        if( !Kappa->is_open() )
        {
          std::cerr << "Error: Kappa didn't open" << std::endl;
        }
        feedback = new std::ofstream("feedback.txt");
        if( !feedback->is_open() )
        {
          std::cerr << "Error: feedback file didn't open" << std::endl;
        }
        pointx = new std::ofstream("pointx");
        if( !pointx->is_open() )
        {
          std::cerr << "Error: pointx file didn't open" << std::endl;
        }
        pointy = new std::ofstream("pointy");
        if( !pointy->is_open() )
        {
          std::cerr << "Error: pointy file didn't open" << std::endl;
        }
 }
  

~BoyleModel() // destructor causes core dump for some reason :s
{

  Activation->close();
  if( Activation )
   //  delete Activation;
   Beta->close();
   //if( Beta )
   //  delete Beta;
   Kappa->close();
   //if( Kappa )
   //  delete Kappa;
   feedback->close();
   pointx->close();
   pointy->close();
}
                      
	mutable double N;
	mutable double M; 
                      
        mutable double dt;
	double energyDensity(const MeshCoordinate&, const Entity&) const {return 0;}
	typedef PiecewiseLinearFunction< dim > PositionFunctionType;

  virtual RangeVectorType X0( const double s ) const
  { untested();
    RangeVectorType ret;
    ret[ 0 ] = s;
    for( unsigned int d = 1; d < dim; ++d )
      { untested();
	ret[ d ] = 0.0;
      }

    return ret;
  }

  //using BaseType::f;     // problem here
  using BaseType::gamma;
  using BaseType::K;
  //using BaseType::e;    // problem here (also with timeProvider() )





  void write_Activation(const double time) const
  {

	*Activation << " " << time;

	for(unsigned int j=0 ; j<N; j++)
	{
		double pos = double(j)/double(N);
		*Activation << " " << Switch[0][j] - Switch[1][j];
	}
	
	*Activation << "\n";
  }


  void write_Beta(const double time) const
  {

	*Beta << " " << time;

	for(unsigned int j=0; j<N; j++)
	{
		*Beta << " " << tau[j];
	}
	
	*Beta << "\n";
  }








void storePropFeedback(std::vector<double>& currentFB) const
	{
		propFeedback = currentFB;
	}


void evaluateState(double const& s) const
	{
		int S = round(s*(N-1));
		//std::vector<double> propFeedback = worm.getPropFeedback();

		if( propFeedback[S] < thd_0 )  {Switch[0][S] = 1;}
		if( propFeedback[S] > thd_1 )  {Switch[0][S] = 0;}

		if(reset)
		{
			if( std::abs(Switch[0][S] - 0) < 1e-10 && propFeedback[S] > thv_00 )   {Switch[1][S] = 1;}
			if( std::abs(Switch[0][S] - 0) < 1e-10 && propFeedback[S] < thv_01 )   {Switch[1][S] = 0;}
			if( std::abs(Switch[0][S] - 1) < 1e-10 && propFeedback[S] > thv_10 )   {Switch[1][S] = 1;}	
			if( std::abs(Switch[0][S] - 1) < 1e-10 && propFeedback[S] < thv_11 )   {Switch[1][S] = 0;}
		}
		else
		{
			if( propFeedback[S] > thv_00 )  { Switch[1][S] = 1; }
			if( propFeedback[S] < thv_01 )  { Switch[1][S] = 0; } 
		}

	}


  virtual double beta( double const& s, const Entity& e ) const
  { untested();

	int S = round(s*(N-1));
	evaluateState(s);	

	dtau[S] = -a_*(Switch[0][S] - Switch[1][S]) - b_*tau[S];

	
	if(timeToUpdate <= e.time())
	{
		for(int k=0; k<N; k++)
		{
			tau[k] = tau[k] + dt_*dtau[k];
		}
	timeToUpdate = timeToUpdate + dt_;
	}

	return tau[S]; //10.0*sin(6*s/0.5 - 6*e.time()/0.5);//tau[S];
  }


/*
  virtual double beta( double const& s, const Entity& e ) const
  { untested();

	
    if(neurtime >= e.time()) //[CHECK]
    {
 //     update_beta(e);

      int S = floor(s*(12-1));
      return 0.0;//torque[S];
    }
  neurtime += dt; // how to get step size?? from param file?? 
  }
*/

private:
  const double eps_;
  mutable double timeToUpdate;
  const double dt_;
  const double a_, b_;
  mutable std::vector<double> tau;
  mutable std::vector<double> dtau;
  mutable std::vector<double> propFeedback;
  mutable std::vector<std::vector<double>> Switch;
  const double thd_0, thd_1, thv_00, thv_01, thv_10, thv_11;
//  mutable std::vector<double> propFeedback;
  mutable bool reset;
  std::ofstream* nu_out;
  mutable std::ofstream* Activation;
  mutable std::ofstream* Beta;
  mutable std::ofstream* Kappa;
  mutable std::ofstream* feedback;
  mutable std::ofstream* pointx;
  mutable std::ofstream* pointy;      
 // mutable double neurtime;
 
}; // NeuroMuscularModel

#endif


