// (c) 2017 Felix Salfelder
//     University of Leeds
//

#ifndef LIB_WORM_HH
#define LIB_WORM_HH
#define NEWADD

#include "model.hh"
#include "femscheme.hh"
#include "entity.hh"
#include "forcefunction.hh"
#include "stencil.hh"
#include "probe.hh"
#include "parameter.hh"
#include "trace.hh"
// there is something wrong. perhaps E_next vs Eprevious?
static const bool rescale_E=true;

#define SOME_DISPLAY_CODE\
    for( unsigned int cam = 0; cam < 3; ++cam )\
      { untested();\
	bounds.at(cam).at(0) -= 15;\
	bounds.at(cam).at(1) += 15;\
	bounds.at(cam).at(2) -= 15;\
	bounds.at(cam).at(3) += 15;\
\
	if( bounds.at(cam).at(0) < 0 ) bounds.at(cam).at(0) = 0;\
	if( bounds.at(cam).at(1) >= 2048 ) bounds.at(cam).at(1) = 2047;\
	if( bounds.at(cam).at(2) < 0 ) bounds.at(cam).at(2) = 0;\
	if( bounds.at(cam).at(3) >= 2048 ) bounds.at(cam).at(3) = 2047;\
\
	cv::imshow( "initial" + std::to_string(cam),\
		    ims.at(cam).rowRange( bounds.at(cam).at(0),\
					  bounds.at(cam).at(1) )\
		    .colRange( bounds.at(cam).at(2),\
			       bounds.at(cam).at(3)) );\
      }\
    cv::waitKey(10);

// stolen from gnucap
// input
// c[0] ... c[n-1] values at t[0] ... t[n-1]
// output
// c[0] ... c[n-2] divided differences of order #index at t=t[0]
//
template<class T1, class T2>
inline void divdiff(T1 c[], int n, const T2 t[]) {
  for (int d=1; d<n; ++d) {
    for (int i=n-1; i>=d; --i) {
      c[i] = (c[i-1] - c[i]) * (1./(t[i-d] - t[i]));
    }
  }
}

// TODO: move non-worm specific stuff to meshobject : entity.
template<unsigned dim>
class worm_ : public MeshObject<dim> {
public:
	class gammaprobe : entity_probe{
		gammaprobe(worm_ const& w)
			: entity_probe("gamma", w) {
		}
	public:
		double evaluate() const{
			auto& w=prechecked_cast<worm_ const&>(_e);
			return w.gamma();
		}
		friend class worm_;
	};
	class energyprobe : entity_probe{
		energyprobe(worm_ const& w)
			: entity_probe("energy", w) {
		}
	public:
		double evaluate() const{
			auto& w=prechecked_cast<worm_ const&>(_e);
			return w.energy();
		}
		friend class worm_;
	};
	class rkpstrengthprobe : entity_probe{
		rkpstrengthprobe(worm_ const& w)
			: entity_probe("rkpstrength", w) {
		}
	public:
		double evaluate() const{
			auto& w=prechecked_cast<worm_ const&>(_e);
			return w._rkpstrength;
		}
		friend class worm_;
	};
	class diststrengthprobe : entity_probe{
		diststrengthprobe(worm_ const& w)
			: entity_probe("diststrength", w) {
		}
	public:
		double evaluate() const{
			auto& w=prechecked_cast<worm_ const&>(_e);
			return w._diststrength;
		}
		friend class worm_;
	};
	class kappaprobe : entity_probe{
		kappaprobe(worm_ const& w)
			: entity_probe("curvature", w), _index(0) {
		}
	public:
		double evaluate() const {
			untested();
			auto &w = prechecked_cast<worm_ const &>(_e);
			assert(_index < howmany());
			double ret = w.curvature().at(_index);
			++_index;
			_index %= howmany();
			return ret;
		}

		unsigned howmany() const {
			untested();
			auto &w = prechecked_cast<worm_ const &>(_e);
			return w.curvature().N()*w.curvature().dim;
		}

	private:
		friend class worm_;
	  	mutable unsigned _index;
	};
  class betaprobe : entity_probe{
	betaprobe(worm_ const& w) : entity_probe("beta", w), _index(0) {}
  public:
	double evaluate() const {
		untested();
		auto &w = prechecked_cast<worm_ const &>(_e);
		assert(_index < howmany());
		double ret = w.model().beta(_index, w);
		++_index;
		_index %= howmany();
		return ret;
	}

	unsigned howmany() const {
		untested();
		auto &w = prechecked_cast<worm_ const &>(_e);
		return w.N();
	}

  private:
	friend class worm_;
	mutable unsigned _index;
  };
public:
	// it's abit weird.
	// if you know a better way, let me know.
	virtual probe* new_probe(const std::string& s){
		if(s=="energy"){
			return new energyprobe(*this);
		}else if(s=="gamma"){
			return new gammaprobe(*this);
		}else if(s=="dists"){
			return new diststrengthprobe(*this);
		}else if(s=="rkps"){
			return new rkpstrengthprobe(*this);
		}else if(s=="curvature"){
			return new kappaprobe(*this);
		}else if(s=="beta"){
			return new betaprobe(*this);
		}else{
			throw exception_invalid("no such probe, " + s);
		}
	}
public:
	using RangeVectorType = RangeVector< dim >;
	using RangeCoTangent = RangeVector< dim >;
	typedef PiecewiseLinearFunction< dim > CurvatureFunctionType;
	typedef PiecewiseLinearFunction< dim > PositionFunctionType;
	typedef PiecewiseLinearFunction< dim > SomeFunctionType;
	typedef PiecewiseConstantFunction< 1 > PressureFunctionType;
	typedef ModelInterface<dim> ModelType;
	typedef double MeshCoordinate;

	using MeshObject<dim>::model;
	using MeshObject<dim>::tr_review_check_and_convert;
	using MeshObject<dim>::mesh;
	using MeshObject<dim>::time;
	using MeshObject<dim>::_time;
	using MeshObject<dim>::scheme; //?
	using MeshObject<dim>::schemehack; //?
	using MeshObject<dim>::tr_review_trunc_error;

	using MeshObject<dim>::_newtime; //?

//	using MeshObject<dim>::new_stencil;
	using MeshObject<dim>::new_mesh_function;
public: // TODO: move up
	worm_(std::string name="worm")
		: MeshObject<dim>(name),
		  _force_data(0), _curvature_data(0), _eta(0.), _time_scale(1.)
	{
	}
	worm_(ConfigParameters const & parameters, std::string name="worm")
		: MeshObject<dim>(name),
		  _force_data(0), _curvature_data(0), _eta(0.),
		  _time_scale(1.), _time_end(1.),
		propFrom( parameters.get<double>( "model.read.propFrom" )),
		propTo( parameters.get<double>( "model.read.propTo" )),
		scalarCurvature(128), propFeedback(128), // BUG: 128>>N() but caused weird size allocation
		Kappa(NULL), feedback(NULL)

	{
		// TODO control forces in forces?
		_force_dist0=parameters.get<double>("force.dist0");
		_force_dist1=parameters.get<double>("force.dist1");
		_force_dist2=parameters.get<double>("force.dist2");
		_force_rkp0=parameters.get<double>("force.rkp0");
		_force_rkp1=parameters.get<double>("force.rkp1");
		_force_rkp2=parameters.get<double>("force.rkp2");
		_force_grav0=parameters.get<double>("force.gravity0");
		_force_grav1=parameters.get<double>("force.gravity1");
		_force_grav2=parameters.get<double>("force.gravity2");

		Kappa = new std::ofstream("Kappa.txt");
		if( !Kappa->is_open() )
		{
			std::cerr << "Error: Kappa didn't open" << std::endl;
		}
		feedback = new std::ofstream("feedback.txt");
		if( !feedback->is_open() )
		{
			std::cerr << "Error: feedback didn't open" << std::endl;
		}


// Jack
//		trace3("wormctor", _force_dist0, _force_dist1, _force_dist2);
//		trace3("wormctor", _force_rkp0, _force_rkp1, _force_rkp2);
//		trace3("wormctor", _force_grav0, _force_grav1, _force_grav2);

	}


	~worm_()
	{
		Kappa->close();
		feedback->close();
		//TODO: add pointx and pointy output files
	}

public:
	void set_param_by_name(const std::string& name, const std::string& value){ untested();
		if(name=="dist0"){ untested();
			_force_dist0 = std::stod(value);
		}else if(name=="dist1"){ untested();
			_force_dist1 = std::stod(value);
		}else if(name=="dist2"){ untested();
			_force_dist2 = std::stod(value);
		}else{ untested();
			incomplete();
		}

	}

//      void update_beta()
//  {
//	model().update_beta(*this);
//  }

	virtual void write_Kappa(double time)
	{
		*Kappa << " " << time;

		for(int j=0; j<128; j++)
		{
			*Kappa << " " << scalarCurvature[j];
		}

		*Kappa << "\n";
	}

	virtual void write_feedback(double time)
	{
		*feedback << " " << time;

		for(int j=0; j<128; j++)
		{
			*feedback << " " << scalarCurvature[j];
		}

		*feedback << "\n";
	}


	// BUG: this is needed in femscheme type
//	typedef DistanceModel< 3, 3 > model_typehack;
	 CurvatureFunctionType curvature() const{
		 return CurvatureFunctionType(mesh(),
		  SubArray< Vector >( _curvature_data ) );
	 }

	 // actually that makes sense for all meshes
	 // (not just worms)
	 // meshobject base type?
	 void computeCurvature() const {
		 CurvatureFunctionType curvature(mesh(),
				 SubArray< Vector >( _curvature_data ));
#if 0
		std::cout << "correct " << std::endl;
		 // construct mass and stiffness matrices of size worlddim*N
		 typedef InverseMassMatrix< PositionFunctionType > InverseMassMatrixType;
		 InverseMassMatrixType inverseMass( mesh(), position() );

		 typedef StiffnessMatrix< PositionFunctionType > StiffnessMatrixType;
		 StiffnessMatrixType stiff( mesh(), position() );

		 Vector rhsData( dim * mesh().N() );
		 PositionFunctionType rhs( mesh(), SubArray< Vector >( rhsData.begin(), rhsData.end() ) );
		 stiff.call(position(), rhs );
		 rhs *= -1;

		 rhs.setDirichletBoundaries( 0 );
		 inverseMass.setDirichletBoundaries();
		 inverseMass.call( rhs, curvature );
#else

		 auto const& X=position();
		 /// XtoW
		 unsigned wl = mesh().N();
		 double q[wl-1];
		 double M[wl-2];
		 RangeVectorType SX[wl-2];


		 q[0] = norm( X.eval(1) - X.eval(0) );
		 for(unsigned i=1; i<wl-1; ++i){ itested();
			 q[i] = norm( X.eval(i+1) - X.eval(i) );
			 M[i-1] = .5*(q[i] + q[i-1]);
			 SX[i-1] = (X.eval(i-1) - X.eval(i))/q[i-1] - (X.eval(i) - X.eval(i+1)) / q[i];
			 curvature.assign(i, SX[i-1]/M[i-1] );
		 }

		 curvature.assign(0,0);
		 curvature.assign(wl-1,0);
#endif
	 }

  virtual void updateScalarCurvature() // BUG: acts on internal meshpoints only!! (j != 0,N-1)
  {
//	std::cout << "curvSize: " << scalarCurvature.size() << "     N:  " << N() << std::endl;
	assert(scalarCurvature.size() == N());
//trace1("propFeedback", propFeedback[0]);

	for(unsigned int j=1; j < 128-1; j++ )
	{

		const auto xem = X().evaluate( j-1 );
		const auto xe  = X().evaluate( j  );
		const auto xep = X().evaluate( j+1 );

		RangeVectorType num = (xem - xe).perp();
		RangeVectorType nup = (xe - xep).perp();
		const auto a = num.norm();
		const auto b = nup.norm();
		num[0] = num[0]/a;
		num[1] = num[1]/a;
		nup[0] = nup[0]/b;
		nup[1] = nup[1]/b;

		RangeVectorType nubar = num + nup;
		const auto c = nubar.norm();
		nubar[0] = nubar[0]/c;
		nubar[1] = nubar[1]/c;
	//incomplete();
 	  scalarCurvature[j] = curvature().evaluate(j)[0]*nubar[0] + curvature().evaluate(j)[1]*nubar[1];
	}

	 scalarCurvature[0]   =  scalarCurvature[1];    // -beta(0)
	 scalarCurvature[N()-1] =  scalarCurvature[N()-2];  // -beta(1)
  }


	std::vector<double> getScalarCurvature() const
	{
		return scalarCurvature;
	}


   virtual void updatePropFeedback()
   {

	//updateScalarCurvature();
	double sum;
	int S;
	double a;
	double b;
	int ia;
	int ib;
	double N2 = 128.0; // BUG: change N2 to N() when N() is fixed

	for(double s = 0.0 ; s <= 1.0 ; s = s + 1/N2)
	{

		S = round(s*(N2-1));
		a = std::max(s + propFrom, 0.0);
		a = std::min(a, 1.0 - 0.0/N2);
		b = std::min(s + propTo,   1.0 + 1.0/N2); // so ia-ib != 0
		ia = a*N2;
		ib = b*N2;
		sum = 0.0;

		for(int i = ia ; i < ib ; ++i)
		{
			if( i < 1 || i > N2-1 )
			{
				sum = sum +  0;
			}
			else
			{
				sum = sum + scalarCurvature[i];
			}
		}

//incomplete
		 propFeedback[S] = sum/std::abs(double(ib-ia));
	}
    }

	virtual std::vector<double> getPropFeedback()
	{
		return propFeedback;
	}

public:
	PiecewiseLinearFunction<dim> const & X0() const{ untested();
	  return _position.solution();
	}
	PiecewiseLinearFunction<dim> const & X1() const{
	  return _position.oldsolution();
	}
private:
public: // HACK HACK HACK. vtuwriter still uses this. (WIP)
	PiecewiseLinearFunction<dim> const & Y0() const{ untested();
		incomplete(); // it's not the curvature, careful!
	  return _curvature.solution();
	}
	PiecewiseLinearFunction<dim> const & Y1() const{ untested();
	  return _curvature.oldsolution();
	}
	PiecewiseLinearFunction<dim> const & W1() const{ untested();
	  return _whatnot.oldsolution();
	}
	// don't use
	PiecewiseLinearFunction<dim> const & X() const{
	  return _position.oldsolution();
	}
	PiecewiseLinearFunction<dim> const & Y() const{
	  return _curvature.oldsolution();
	}

	PiecewiseLinearFunction<dim>&	rhsposition(){
		return _position.rhs();
	}
	PiecewiseLinearFunction<dim>&	rhscurvature(){
		return _curvature.rhs();
	}
	PiecewiseConstantFunction<1>&	rhspressure(){
		return _pressure.rhs();
	}
	double propFrom;
	double propTo;
	std::vector<double> scalarCurvature;
	std::vector<double> propFeedback;
	std::ofstream* Kappa;
	std::ofstream* feedback;
	//TODO: Add pointx and pointy output files
public:

	// a hack. scheme must be global..?
	void expand(FemScheme<dim>& s){
		MeshObject<dim>::expand(s);
		assert(mesh().N());
		_force_data.resize(dim*mesh().N());
		_curvature_data.resize(dim*mesh().N()); // ask scheme?
		for(auto& i : _extra_forces){
			i->expand(_position.oldsolution());
		}

		// good idea?
		// no. _position.new_mesh_function("X", *this) ...
		new_mesh_function(_position, "X");
		new_mesh_function(_curvature, "Y");
		new_mesh_function(_pressure, "P");

		if(_eta){ untested();assert(false);
			new_mesh_function(_whatnot, "W");
			_stiffstencil.new_stencil("W", "X", *this);
			_massstencil.new_stencil("W", "W", *this); // M/e
			_massstencil2.new_stencil("Y", "Y", *this); // M
			_massstencil3.new_stencil("Y", "W", *this, "2"); // M
			_pmassstencil.new_stencil("Y", "W", *this); // -M/e
		}else{
			_stiffstencil.new_stencil( "Y",  "X", *this);
			_massstencil.new_stencil( "Y", "Y", *this); // M/e
		}
		_pstiffstencil.new_stencil("X", "Y", *this);
		_presstencil.new_stencil("P", "X", *this);
		_dragstencil.new_stencil("X", "X", *this);

	}

// new.
	void eval(){
		//assert(_scheme);
		if(X().evaluate(0)==X().evaluate(1)){ untested();
		}else{
		}
		double t=time() / _time_end;

		assert(time()<10); // safety net.
		_diststrength=_force_dist0
			          +_force_dist1 * t
			          +_force_dist2 * t*t;
		_rkpstrength =_force_rkp0
			          +_force_rkp1 * t
			          +_force_rkp2 * t*t;
		_gravstrength=_force_grav0
			          +_force_grav1 * t
			          +_force_grav2 * t*t;

//		message(bTRACE, "WORM eval %f, %f, %f\n", _diststrength, _rkpstrength, _gravstrength);

		_force_data.clear();
		assert(_force_data.size()==dim*mesh().N());

    // project solution variable
	   _energy = 0.;
		if(_extra_forces.size()){
			for(auto& i : _extra_forces){
				i->eval();
				double e=i->energy();
		//		message(bTRACE, "WORM eval %s %f\n", i->label().c_str(), e);
				_energy += e;
			}
		}else{
		}
	} // eval
	double energy() const{
		return _energy;
	}

	double const& diststrength() const{
		return _diststrength;
	}
	double const& rkpstrength() const{
		return _rkpstrength;
	}
	double const& gravstrength() const{ untested();
		return _gravstrength;
	}
	PositionFunctionType const& position() const{ itested();
		return _position.solution(); // scheme().position();
	}
	PressureFunctionType const& pressure() const{ itested();
		return _pressure.solution(); // scheme().pressure();
	}

#if 0 // incomplete
	RangeVectorType head(){ untested();
		return scheme().position(0);
	}
	RangeVectorType tail(){ untested();
	}
#endif
private:
	// internal time is private.
	// maybe not so good, if sine waves need to use the rescaled clock
	double scaled_deltat() const{
		assert(_time_scale);
		trace1("scaled dt", _time_scale);
	  	return MeshObject<dim>::deltat() * _time_scale;
	}

private: // worm state
  // different types of energy for debugging maybe
  // TODO properly tap model to dump internals into file.
  std::vector<double> _energyVector;

  double _diststrength;
  double _rkpstrength;
  double _gravstrength;


  double _force_dist0;
  double _force_dist1;
  double _force_dist2;
  double _force_rkp0;
  double _force_rkp1;
  double _force_rkp2;
  double _force_grav0;
  double _force_grav1;
  double _force_grav2;

private: // stamping. called from matrix.hh
  bool velocitymodel() const{ untested();
	  return
		  model().constraint() == ModelType :: Constraint :: velocity;
  }
  bool measuremodel() const{
	  return
		  model().constraint() == ModelType :: Constraint :: measure;
  }
private: // baseclass?
  unsigned N() const{
	  return mesh().N();
  }
private: // overrides
  void tr_accept(){
	  computeCurvature();
	  _head[0] = _position.solution().eval(0);
	  if(mesh().N()%2){ untested();
		  _mid[0] = _position.solution().eval(N()/2);
	  }else{
		  // no middle point...
		  _mid[0] = .5*(_position.solution().eval((N()+1)/2)
					 +     _position.solution().eval(N()/2) );
	  }
	  _tail[0] = _position.solution().eval(N()-1);
  }
  void tr_regress(){ untested();
	  MeshObject<dim>::tr_regress();
  }
  void tr_advance(){
	  MeshObject<dim>::tr_advance();

	  _head[2] = _head[1];
	  _head[1] = _head[0];
	  _tail[2] = _tail[1];
	  _tail[1] = _tail[0];
  }
  void tr_load(){
// Jack	  trace2("worm tr_load", _time[0], _time[1]);
	  /* TODO
	   * each "load" method has its own mesh loop!
	   * should this be as one
	   */

	  // non symmetric terms
	  load_matrix_pres();
	  // drag, mass, Pmass and Pstiff terms
	  load_matrix_drag();
	  // stiffness only
	  load_matrix_stiff();
	  // body forcing
	  load_force_rhs();
	  // explicit terms (plus hack?) and rhs of velocity model
	  load_more_rhs();
	  // more drag?
	  load_dynamic_drag();
	  // beta and rhs of measure model
	  load_call_rhs();
  }
  double review(){
	 double timestep = tr_review_trunc_error(_head);
    _newtime = tr_review_check_and_convert(timestep);
//Jack	 trace2("worm::review", _newtime, _time[1]);

//	 message(bDEBUG, "review head %f, %f\n", _head[0], _head[1]);
//	 message(bDEBUG, "review time %f, %f\n", _time[0], timestep);
//	 message(bLOG, "DELTA %f %f %f\n", _time[0], timestep-_time[0], timestep );
	 return _newtime;
  }
public:
  RangeVectorType const& head() const{ itested();
	  return _head[0];
  }
  RangeVectorType const& mid() const{ itested();
	  return _mid[0];
  }
  RangeVectorType const& tail() const{ itested();
	  return _tail[0];
  }
  RangeVectorType const cog() const{ itested();
//	  auto const& P=scheme().firstblock(); // _position.
	  RangeVectorType c;
	  for( unsigned int e = 0; e < mesh().N(); ++e ) { itested();
		  c += _position.solution().eval(e); // * mass?
	  }
	  c /= mesh().N();
	  return c;
  }
private: // load implementation
  void load_matrix_stiff() {
	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
	  // stiffness matrix
	  // [   |   |  ]
	  // [---+---+--]
	  // [\\\|   |  ]
	  // [---+---+--]
	  // [   |   |  ]
		  const RangeVectorType xe = X().evaluate( e );
		  const RangeVectorType xep = X().evaluate( e+1 );
		  RangeVectorType tau = ( xep - xe );
		  double q = tau.norm();
		  for( unsigned int k = 0; k < X().dim; ++k ) {
			  unsigned l=k;
			  double stiff = 1.0 / q;
			  if(!q){ incomplete();
				  stiff=1.;
			  }else{
			  }

			  if( e == 0 ) {
			  } else {
				  // add( dim*(e) + l, stiffOffset + dim*(e) + k, stiff );
				  // add( dim*(e+1) + l, stiffOffset + dim*(e) + k, -stiff );
				  _stiffstencil.add(e,   e, k, l, stiff);
				  _stiffstencil.add(e, e+1, k, l, -stiff);
			  }
			  if( e == mesh().N()-2 ) {
			  }else{
				  _stiffstencil.add(e+1, e,   k, l, -stiff);
				  _stiffstencil.add(e+1, e+1, k, l, stiff);
				  //add( dim*(e) + l, stiffOffset + dim*(e+1) + k, -stiff );
				  //add( dim*(e+1) + l, stiffOffset + dim*(e+1) + k, stiff );
			  }
		  } // k
	  } // e
  }
  void load_matrix_pres(){
	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
		  // fixme: order:
		  for( unsigned int k = 0; k < X().dim; ++k ) {
			  // non symmetric terms
			  const RangeVectorType xe = X().evaluate( e );
			  const RangeVectorType xep = X().evaluate( e+1 );
			  RangeVectorType tau = ( xep - xe );
			  double q = tau.norm();
			  double pres = tau[k];
assert(pres==pres);
			  if(q){
				  pres /= q;
			  }else{ untested();
				  pres = 1.;
			  }
			  // add to matrix
//			  const unsigned int presOffset = 2 * dim * mesh().N();
			  unsigned N=mesh().N();
			  if( 1 /*implicit_*/ or velocitymodel()){

				  /*   dim*N     dim*N   N-1
					* [         |        | H  ]
					* [  ...    |....    | E  ]
					* [         |        | R  ]
					* [         |        | E  ]
					* [ --------+--------+--- ]
					* [ ...     | ...    |..  ]
					* [ --------+--------+--- ]
					* [ HERE    | ...    |..  ]
					*/
				  _presstencil.add(              e*dim + k,  pres);
				  _presstencil.add((N-1)*dim   + e*dim + k, -pres);

				  _presstencil.add((N-1)*dim*2 + e*dim + k,  pres);
				  _presstencil.add((N-1)*dim*3 + e*dim + k, -pres);
			  }else{ untested();
			  }
		  } // k
	  } // e
  }
  void load_matrix_drag(){

	  double EN=model().regularisation(mesh().u( 0 ) , *this);
	  RangeVectorType xep=X().evaluate(0);
	  RangeVectorType yep=curvature().evaluate(0);

	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
		  MeshCoordinate s=.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
		  double K=model().K(s);
		  double E=model().regularisation(s, *this);

		  double EP=EN;
		  EN = model().regularisation(mesh().u( e+1 ) , *this);

//Jack		  trace2("reg", s, E);

		  RangeVectorType xe = xep;
		  xep = X().evaluate( e+1 );

		  RangeVectorType tau = ( xep - xe );
		  double q = tau.norm();
		  assert(q==q);
		  assert(q); // for now
		  // trace4("worm X", e, xe,xep, q);
		  if(q){
			  tau /= q;
		  }else{ untested();
			//  E=.1;
		  }
		  double deltaT = scaled_deltat();

		  RangeVectorType ye = yep;
		  yep = curvature().evaluate( e+1 );
		  double energyDensity = model().energyDensity_at((xe+xep)*.5, (ye+yep)*.5, s, *this);
// Jack		  trace2("ED", s, energyDensity);

		  assert(energyDensity==energyDensity);

		  for( unsigned k=0; k<dim; ++k ){
			  double mass=.5 * q;
			  assert(E);
			  double mass_EP = mass;
			  double mass_EN = mass;
			  if(rescale_E){
				  mass_EP *= EP;
				  mass_EN *= EN;
			  }else{
			  }
			  if(!q){ untested();
				  mass = .5;
			  }else{
			  }
			  assert(mass==mass);
			  // mass matrix
			  // [   |   |  ]
			  // [---+---+--]
			  // [   | \ |  ]
			  // [---+---+--]
			  // [   |   |  ]
			  _massstencil.add_(e, e, k, k, mass_EP);
			  _massstencil.add_(e+1, e+1, k, k, mass_EN);

// Jack			  trace4("MASS", s, k, mass, mass_EN);

			  if(_eta){ itested();
				  assert(rescale_E);
				  //_massstencil2.add(e, e, k, k, mass);
				  //_massstencil2.add(e+1, e+1, k, k, mass);
				  _massstencil2.add(dim*(dim*e +    k) + k, mass);
				  _massstencil2.add(dim*(dim*(e+1) +k) + k, mass);

				  // DUP
				  //same as 2, but different matrix location...
				  //hmmm
				  // _massstencil3.add(e,e,k,k, -mass_E);
				  // _massstencil3.add(e+1, e+1, k,  k, -mass_E);
				  _massstencil3.add(dim*(dim*e +    k) + k, -mass_EP);
				  _massstencil3.add(dim*(dim*(e+1) +k) + k, -mass_EN);
			  }else{
			  }
			  for( unsigned int l = 0; l < dim; ++l ) {
				  // [\\ |   |  ]
				  // [\\\|   |  ]
				  // [ \\|   |  ]
				  // [---+---+--]
				  // [   |   |  ]
				  // [---+---+--]
				  // [   |   |  ]
				  const double wStiff = energyDensity / q;
				  if(q==0){ untested();
					  unreachable();
				  }else if(wStiff){
					  // see load_dynamic_drag
				  }else{itested();
				  }
				  // projection matrix
//				  const double Ptau = ( (double) (k == l) - tau[l]*tau[k] );

				  // drag term
				  // load_dynamic?
				  // if( k == l )
				  /* TODO: what's the logic here */
				  if(!deltaT) { untested();
					  // static analysis is incomplete
					  incomplete();
					  if(k==l){ untested();
						  _dragstencil.add_(e, e, k, l, 1.);
						  _dragstencil.add_(e+1, e+1, k, l, 1.);
					  }
				  }else if(K!=1 || k==l) {
					  const double id = (double) (k == l);
					  const double hilf = ( 1.0 - K ) * tau[l]*tau[k];
					  const double drag = 0.5 * q * ( K * id +  hilf ) / deltaT;
					  assert(drag==drag);


					  // add to matrix, dim=3
					  /* [ \       |     |    ]
						* [   3x3   |     |    ]
						* [   diag  |.... |..  ]
						* [  stamps |     |    ]
						* [        \|     |    ]
						* [ --------+-----+--- ]
						* [ ...     | ... |..  ]
						*/
					  //  1*
					  //  *1
					  //    2*
					  //    *2
					  //      2*
					  //      *2
					  //        1*
					  //        *1

//					  assert(_dragstencil);
					  // trace4("",e,k,l,drag);
#ifdef DEBUG
					  std::cout << "DRAG " << e << " "  << l << " " << k << ": " << K << " " << hilf << " " << drag << "\n";
					  std::cout << "DRAG " << tau[l] << " "  << tau[k] << "\n";
#endif
					  //_dragstencil.add(dim*(dim*e +     k) + l, drag);
					  //_dragstencil.add(dim*(dim*(e+1) + k) + l, drag);
					  _dragstencil.add_(e,e,k,l,drag); // or l,k?
					  _dragstencil.add_(e+1,e+1,k,l,drag);


				  }else{ itested();
				  }

				  {
					  double mass = 0.5 * q;
#if 0
					  if(rescale_E){ untested();
						  double mass_E = mass / E;
					  }else{ untested();
					  }
#endif

					  // something like this
					  const double id = (double) (k == l);
					  const double hilf = id - tau[l]*tau[k];

					  if(_eta){ itested();
						  double stamp = _eta * mass * hilf / deltaT;
						  _pmassstencil.add_(e, e, k, l, -stamp);
						  _pmassstencil.add_(e+1, e+1, k, l, -stamp);
					  }else{
					  }
				  }

				  //-======================================================
				  const double Ptau = ( (double) (k == l) - tau[l]*tau[k] );
#ifdef DEBUG
			  std::cout << "PTAU "  << k << l << " " << Ptau << "\n";
#endif
				  assert(Ptau==Ptau);
				  double pStiff = -Ptau;
				  if(q){
					  pStiff /= q;
				  }else{ untested();
					  pStiff = 1.;
				  }
				  double pStiff_N=pStiff * E;
				  double pStiff_P=pStiff * E;

				  if(rescale_E){
					  pStiff_P*=EP;
					  pStiff_N*=EN;
				  }else{ itested();
				  }

				  assert(pStiff==pStiff);
				  // [ |\| ]
				  // [-+-+-]
				  // [ | | ]
				  // [-+-+-]
				  // [ | | ]
				  // stamp_symmetric y <+ x ...
//				  trace2("pss", l, pStiff);
				  _pstiffstencil.add(e,   e,   k, l, pStiff_P);
				  _pstiffstencil.add(e+1, e+1, k, l, pStiff_N);

				  _pstiffstencil.add(e+1, e,   k, l, -pStiff_P);
				  _pstiffstencil.add(e,   e+1, k, l, -pStiff_N);

			  } // l loop
		  } // k loop

	  }
  }
  void load_dynamic_drag() {
	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
		  MeshCoordinate s=.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
		  double E=model().regularisation(s, *this);

//Jack		  trace2("reg", s, E);

		  const RangeVectorType xe = X().evaluate( e );
		  const RangeVectorType xep = X().evaluate( e+1 );
		  RangeVectorType tau = ( xep - xe );
		  double q = tau.norm();
		  assert(q==q);
		  assert(q); // for now
		  // trace4("worm X", e, xe,xep, q);
		  if(q){
			  tau /= q;
		  }else{ untested();
			//  E=.1;
		  }
#ifdef DEBUG
	  std::cout << "Q " << e << " " << q << "\n";
#endif

		  const RangeVectorType ye = curvature().evaluate( e );
		  const RangeVectorType yep = curvature().evaluate( e+1 );
		  double energyDensity = model().energyDensity_at( (xe + xep)*0.5,
				  (ye + yep)*0.5, s , *this);
//Jack		  trace2("ED", s, energyDensity);
#ifdef DEBUG
	  std::cout << "ED " << s << " " << energyDensity << "\n";
#endif
		  assert(energyDensity==energyDensity);
		  // trace1("worm X", energyDensity);

		  for( unsigned int k = 0; k < dim; ++k ) {

			  for( unsigned int l = 0; l < dim; ++l ) {
				  // [\\ |   |  ]
				  // [\\\|   |  ]
				  // [ \\|   |  ]
				  // [---+---+--]
				  // [   |   |  ]
				  // [---+---+--]
				  // [   |   |  ]
				  const double wStiff = energyDensity / q;
				  if(q==0){ untested();
					  unreachable();
				  }else if(wStiff){ untested();
					  //add( dim*(e) + l, dim*(e) + k, wStiff );
					  //add( dim*(e+1) + l, dim*(e+1) + k, wStiff );
					  //add( dim*(e) + l, dim*(e+1) + k, -wStiff );
					  //add( dim*(e+1) + l, dim*(e) + k, -wStiff );
					  _dragstencil.add_( e, e, l, k, wStiff );
					  _dragstencil.add_( e+1, e+1, l, k, wStiff );
					  _dragstencil.add_( e , e+1, l,  k, -wStiff );
					  _dragstencil.add_( e+1 ,e, l, k, -wStiff );

				  }else{itested();
				  }
			  } // l loop
		  } // k loop
	  } // e loop
  }

  void load_more_rhs(){
	  MeshMatrix explicit_operator( mesh(),  2*dim*mesh().N() + mesh().N()-1 );

	  double deltaT = scaled_deltat();
	  assert(deltaT); // ??

	  // walk mesh. what SystemMatrix::assemble did
	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
		  // model parameters
		  //
		  // s, the center coordinate...?
//		  const double s = 0.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
//		  const double K = model().K( s );

		  // construct area element and tangent
		  RangeVectorType xe = X().evaluate( e );
		  RangeVectorType xep = X().evaluate( e+1 );
		  if(!deltaT){ untested();
			  incomplete();
			  // HACK
			  typename ModelType :: RangeVectorType ret;
			  ret[0] = mesh().u(e);
			  ret[1] = 0.0;
			  xe = ret;
			  ret[0] = mesh().u(e+1);
			  xep = ret;
		  }else{
		  }
		  RangeVectorType tau = ( xep - xe );
		  const double q = tau.norm();
		  if(q){
			  tau /= q;
		  }else{ untested();
			  unreachable();
		  }

#ifdef SOME_OLD__
		  for( unsigned int k = 0; k < X().dim; ++k ) { untested();
			  for( unsigned int l = 0; l < X().dim; ++l ) { untested();
				  // projection matrix
//				  const double Ptau = ( (double) (k == l) - tau[l]*tau[k] );

				  // drag term
				  // load_dynamic?
				  // if( k == l )
				  if(K!=1. || k==l) { untested();
					  const double id = (double) (k == l);
					  const double hilf = ( 1.0 - K ) * tau[l]*tau[k];
					  double drag = 0.5 * q * ( K * id +  hilf );
					  if(deltaT){ itested();
						  drag /= deltaT;
					  }else{ untested();
					  }

					  // add to matrix, dim=3
					  /* [ \       |     |    ]
						* [   3x3   |     |    ]
						* [   diag  |.... |..  ]
						* [  stamps |     |    ]
						* [        \|     |    ]
						* [ --------+-----+--- ]
						* [ ...     | ... |..  ]
						*/
				  }else{ untested();
				  }
			  } // l loop

			  // non symmetric terms
			  const double pres = tau[k];
			  assert(pres==pres);
			  // add to matrix
			  const unsigned int presOffset = 2 * dim * mesh().N();
			  if( velocitymodel()){ untested();
				  incomplete(); //?

				  /* [         |     | H  ]
					* [  ...    |.... | E  ]
					* [         |     | R  ]
					* [         |     | E  ]
					* [ --------+-----+--- ]
					* [ ...     | ... |..  ]
					* [ --------+-----+--- ]
					* [ HERE    | ... |..  ]
					*/
				  // _presstencil...?
				  _matrix_hack->add( presOffset + e, dim*e + k, pres );
				  _matrix_hack->add( presOffset + e, dim*(e+1) + k, -pres );

				  _matrix_hack->add( dim*e + k, presOffset + e, pres );
				  _matrix_hack->add( dim*(e+1) + k, presOffset + e, -pres );
			  }else{ untested();
			  }
		  }
#endif
	  } // meshwalk

	  PositionCurvaturePressureTuple< dim > tmp(mesh());
	  PositionCurvaturePressureTuple< dim > hack(mesh());
	  hack.position().assign(X());
	  hack.curvature().assign(Y());
	  hack.pressure().assign(pressure());

	  /// tmp = explicit_operator * hack
	  explicit_operator.call( hack, tmp );

//	  explicit_operator.print(std::cout);

	  // (pos, pres) += EO * ( posR, presR )

//		auto& R = _position.rhs();

	  // R+= MD * X1();
	  // // BUG. only the explicit ones.
	  _dragstencil.mv(rhsposition(), X1());

	  if(_eta) { untested();
		  incomplete();
		  assert(rescale_E);
		  // RECHECK: is this divided by E correctly?
		  _pmassstencil.mv(rhscurvature(), W1());
	  }else{
	  }

#if 0
		for(unsigned i=0; i<mesh().N(); ++i){
			R.add(i, tmp.position().eval(i));
		}

		{
			auto& R = rhscurvature();
			for(unsigned i=0; i<mesh().N(); ++i){
				auto c = tmp.curvature().eval(i);
				assert(c[0]==c[0]);
				trace2("extra curv rhs", i, c);
				assert(fabs(c[i])<1e-10); // this is hacked in from another place?!
				R.add(i, c);
			}
		}

		{
		auto& R = _pressure.rhs();
		for(unsigned i=0; i<mesh().N()-1; ++i){
			R.add(i, tmp.pressure().evaluate(i));
		}
		}
#endif

	}

  void load_call_rhs(){
	  auto& rhsY = rhscurvature();
	  auto& rhsP = rhspressure();

	  // element loop
	  for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
		  // extract geometry
		  const double s = 0.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
		  const auto xe = X().evaluate( e );
		  const auto xep = X().evaluate( e+1 );
		  const double q = ( xe - xep ).norm();
		  RangeVectorType nu = ( xe - xep ).perp();
		  if(q){
			  nu /= q;
		  }else{ untested();
			  incomplete();
		  }

		  // integrate position and curvature forcing
		  for( unsigned int ie = 0; ie < 2; ++ie ) {
			  //		const auto xie = X().evaluate( e + ie );
			  const auto sie = mesh().u( e + ie );

			  RangeVectorType valueY;
			  double beta_E=model().beta( sie, *this );
//			  double E=model().regularisation( sie, *this );

			  if(rescale_E){
		//		  beta_E /= E;
			  }else{
		//		  beta_E *= E;
			  }

			  for( unsigned int d = 0; d < X().dim; ++d ) {
 				assert(beta_E == beta_E);
		  		valueY[ d ] = 0.5 * q * beta_E * nu[ d ];
			  }

			  rhsY.add( e+ie, valueY );
		  }

		  // integrate pressure forcing
		  if( measuremodel() ){
			  assert(mesh().h(1)==mesh().H(1));
			  rhsP[e] = mesh().H( e ) * -model().gamma( s );
		  }else{ untested();
		  }
	  }
     rhsY.setDirichletBoundaries( 0 ); // hmm, here?
  }

  double gamma() const{
//	  average maybe?
		return model().gamma( 0/*?*/ );
  }

	void load_force_rhs(){
		if(!_extra_forces.size()){
			// HACK, bypass force, if there's no other forc
			// (e.g. as in worm-model)
			return;
		}else{
			// assert(0); // for now.
		}
		_force_data.clear();

		PiecewiseLinearFunction<dim> tmpforce(mesh(),
			  	SubArray< Vector >( _force_data ));

		// BUG: this should be an extra force
		// this is eval. do it here, for now.
		for( unsigned int e = 0; e < mesh().N()-1; ++e ) { itested();
			// extract geometry
//			const double s = 0.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
			const auto xe = X().evaluate( e );
			const auto xep = X().evaluate( e+1 );
			const double q = ( xe - xep ).norm();
			RangeVectorType nu = ( xe - xep ).perp();
			nu /= q;


			// integrate position and curvature forcing
			for( unsigned int ie = 0; ie < 2; ++ie ) { itested();
				const auto xie = X().evaluate( e + ie );
//				const auto sie = mesh().u( e + ie );

				// the distanceforce
				RangeVectorType f = model().Force( xie );
				assert(f.size()==X().dim);

//				message(bTRACE, "size %d\n", f.size());
//				message(bTRACE, "f size %d\n", _force_data.size());
				RangeVectorType valueX;
				for( unsigned d=0; d < X().dim; ++d ) { itested();
					valueX[d] += 0.5 * q * f[ d ];
				}

				assert((ie+e)*3+2<_force_data.size());
				tmpforce.add(ie+e, valueX);
			}
		}

		// collect the extra forces
		for(auto& i : _extra_forces){
			// hmm delegate or stamp from here?
			i->add_rhs(tmpforce);
		}

		auto& R = rhsposition();
		assert(R.size()==tmpforce.size());
//		assert(scheme().N()==tmpforce.N());
		for(unsigned i=0; i<tmpforce.N(); ++i){
			R.add(i, tmpforce.evaluate(i));
		}

	} // worm_3d::load_force_rhs

public:
	template<class S>
	void dump_energies(S& s) const{ untested();
		for(auto& i : _extra_forces){ untested();
			s << "," << i->energy();
		}
	}
public: // parameters
	void set_time_end(double x){
		// This is a vile hack. used in skeleton. DONT USE.
		_time_end = x;
	}
	void set_time_scale(double x){
		assert(x);
		_time_scale = x;
	}
	void set_eta(double x){
		_eta = x;
	}
public: // HACK. cleanup later
  std::vector< ForceFunctionBase<dim>* > _extra_forces;
  virtual void initialiseXY() {
	  RangeVectorType Xu(0);
	  //PositionFunctionType HACKposition( mesh(),
		//	  SubArray< Vector >( _solution_data.begin(), _solution_data.end() ) );

	  for( unsigned int j = 0; j < mesh().N(); ++j )
	  {
		  const double uj = mesh().u(j);
		  const auto Xuj = model().X0( uj );
		  // HACK HACK
		  _position.solution().assign( j, Xuj );
	  }

  }
#if 0
  virtual RangeVectorType X0( const double s, const Entity& e) const
  { untested();
    RangeVectorType ret = distance_.middle();
    ret[ 1 ] += gamma(s, e)*(s-0.5);

    return ret;
  }
#endif
  void initialiseXY(RangeVectorType const& middle) { untested();
	  RangeVectorType Xu(0);
	  for( unsigned int j = 0; j < mesh().N(); ++j )
	  { untested();
		  const double uj = mesh().u(j);
		 RangeVectorType ret = middle;
		 ret[ 1 ] += model().gamma(uj /*, *this */)*(uj-0.5);
		  auto Xuj = ret;
//Jack		  trace2("initXY", j, Xuj);
		  _position.solution().assign( j, Xuj );
	  }
  }

  void initialiseXY( const PositionFunctionType& old ) { untested();
	  _position.solution().assign(old);
  }

#ifdef OpenCV_FOUND

  // compute triangulation thingy
  template< class D >
  void initialiseXY( const D& distance )
  {

    std::array< std::array<int, 4>, 3 > bounds;
    for( unsigned int cam = 0; cam < 3; ++cam ) {

      //really?
      bounds[cam][0] = 0;
      bounds[cam][1] = 0;
      bounds[cam][2] = 0;
      bounds[cam][3] = 0;
    }

    const auto baseDistance = distance.baseDistance();
//    auto const& projection = distance.projection();

    std::vector< std::vector< cv::Point > > knownPoints;
    std::vector< std::vector< typename D :: BaseRangeVectorType > > extremes;
    // for( auto d : baseDistance )
    for( unsigned int cam = 0; cam < 3; ++cam ) {
	const auto& d = baseDistance[cam];
	knownPoints.push_back( d.retKnown() );
	message(bLOG, "retknown %d\n", d.retKnown().size());

	for( auto p : d.retKnown() ) {
	    if( p.y < bounds[cam].at(0) ){ untested();
              bounds.at(cam).at(0) = p.y;
            }else{ itested();
            }
	    if( p.y > bounds.at(cam).at(1) ){
			 bounds.at(cam).at(1) = p.y;
		 }
	    if( p.x < bounds.at(cam).at(2) ){
			 bounds.at(cam).at(2) = p.x;
		 }
	    if( p.x > bounds.at(cam).at(3) ){
			 bounds.at(cam).at(3) = p.x;
		 }
	  }

	const cv::Point myHead = d.retKnown().front();
	const cv::Point myTail = d.retKnown().back();

	std::cout << "cam: " << cam << " head: " << myHead
		  << " tail " << myTail << std::endl;

	const typename D :: BaseRangeVectorType myHeadD
	  = {{ (double)myHead.x, (double)myHead.y }};
	const typename D :: BaseRangeVectorType myTailD
	  = {{ (double)myTail.x, (double)myTail.y }};

	extremes.push_back( { myHeadD, myTailD } );
    }

#ifdef DISPLAY_FEMSCHEME_IMAGES
SOME_DISPLAY_CODE
#endif

    struct Extreme {
      Extreme() : _fit(1e99){}
      std::array< typename D :: BaseRangeVectorType, 3 > pExtremes0;
      typename D :: RangeVectorType extreme0;
      std::array< typename D :: BaseRangeVectorType, 3 > pExtremes1;
      typename D :: RangeVectorType extreme1;

      std::array< unsigned long, 3 > npos0;
      std::array< unsigned long, 3 > npos1;

      double fitness() const{ return _fit; }
      void do_fit(D const & distance) {
	_fit = 0;
	_fit += distance.triangulate( pExtremes0, extreme0 );
	_fit += distance.triangulate( pExtremes1, extreme1 );
      }
    private:
      double _fit;
    };

    Extreme minExtreme;
//    minExtreme.fit = 1e6;

	 for(unsigned i0=0; i0 < 2; ++i0 ) {
		 unsigned j0=1-i0;
		 for( unsigned i1=0; i1 < 2; ++i1 ) {
			 unsigned j1 = 1-i1;
			 for( unsigned i2 = 0; i2 < 2; ++i2 ) {
				 unsigned j2=1-i2;
				 unsigned ii[3];
				 ii[0] = i0;
				 ii[1] = i1;
				 ii[2] = i2;

				 Extreme myExtreme;
				 myExtreme.pExtremes0[0] = extremes[0][i0];
				 myExtreme.pExtremes1[0] = extremes[0][j0];

				 myExtreme.pExtremes0[1] = extremes[1][i1];
				 myExtreme.pExtremes1[1] = extremes[1][j1];

				 myExtreme.pExtremes0[2] = extremes[2][i2];
				 myExtreme.pExtremes1[2] = extremes[2][j2];

				 myExtreme.npos0[0] = ( ii[0]  == 0 ) ? 0 : knownPoints[0].size()-1;
				 myExtreme.npos0[1] = ( ii[1]  == 0 ) ? 0 : knownPoints[1].size()-1;
				 myExtreme.npos0[2] = ( ii[2]  == 0 ) ? 0 : knownPoints[2].size()-1;

				 myExtreme.npos1[0] = ( ii[0]  == 1 ) ? 0 : knownPoints[0].size()-1;
				 myExtreme.npos1[1] = ( ii[1]  == 1 ) ? 0 : knownPoints[1].size()-1;
				 myExtreme.npos1[2] = ( ii[2]  == 1 ) ? 0 : knownPoints[2].size()-1;

				 myExtreme.do_fit(distance);

				 if( myExtreme.fitness() < minExtreme.fitness() ) {
					 minExtreme = myExtreme;
				 }else{
				 }
			 }
		 }
	 }

#ifdef DISPLAY_FEMSCHEME_IMAGES
      { untested();
	// std::cout << "one end is " << minExtreme.extreme0
	// 	  << " at " << minExtreme.npos0.at(0)
	// 	  << " " << minExtreme.npos0.at(1)
	// 	  << " " << minExtreme.npos0.at(2) << std::endl;
	// std::cout << "one end is " << minExtreme.extreme1
	// 	  << " at " << minExtreme.npos1.at(0)
	// 	  << " " << minExtreme.npos1.at(1)
	// 	  << " " << minExtreme.npos1.at(2) << std::endl;
	// std::cout << "fit: " << minExtreme.fit << std::endl;

	for( unsigned int cam = 0; cam < 3; ++cam ) { untested();
	    cv::Point pt;
	    pt.x = minExtreme.pExtremes0.at(cam).at(0);
	    pt.y = minExtreme.pExtremes0.at(cam).at(1);
	    cv::circle( ims.at(cam), pt, 1, cv::Scalar( 0, 0, 0 ), -1 );
	}
	for( unsigned int cam = 0; cam < 3; ++cam ) { untested();
	    cv::Point pt;
	    pt.x = minExtreme.pExtremes1.at(cam).at(0);
	    pt.y = minExtreme.pExtremes1.at(cam).at(1);
	    cv::circle( ims.at(cam), pt, 1, cv::Scalar( 0, 255, 0 ), -1 );
	}
      }

    for( unsigned int cam = 0; cam < 3; ++cam )
      { untested();
	cv::imshow( "initial" + std::to_string(cam),
		    ims.at(cam).rowRange( bounds.at(cam).at(0),
					  bounds.at(cam).at(1) )
		    .colRange( bounds.at(cam).at(2),
			       bounds.at(cam).at(3)) );
      }
    cv::waitKey(10);
#endif

    unsigned int N = 128;
    const double fitTol = minExtreme.fitness() / 2.0;
	 message(bLOG, "ordered triangulation done, tolerance = %f\n", minExtreme.fitness());

    struct OrderedPoint {
      typename D :: RangeVectorType point;
      std::array< unsigned long, 3 > position;
      double coordinate;
    };

    std::vector< OrderedPoint > points
      = { { minExtreme.extreme0, minExtreme.npos0, 0.0 },
	  { minExtreme.extreme1, minExtreme.npos1, 1.0 } };
    {
      std::sort( points.begin(), points.end(), []( const OrderedPoint& A, const OrderedPoint& B ) -> bool
		 {
		   return A.coordinate < B.coordinate;
		 }
		 );

      while( points.size() < N )
	{
	  bool anyAdded = false;
	  {
#ifdef DISPLAY_FEMSCHEME_IMAGES
	    for( unsigned int cam = 0; cam < 3; ++cam )
	      { untested();
		cv::imshow( "initial" + std::to_string(cam),
			    ims.at(cam).rowRange( bounds.at(cam).at(0),
						  bounds.at(cam).at(1) )
			    .colRange( bounds.at(cam).at(2),
				       bounds.at(cam).at(3)) );
	      }
	    cv::waitKey(10);
#endif
	  }

	  const unsigned int oldPointsSize = points.size();
	  for( unsigned int e = 0; e < oldPointsSize - 1 ; ++e )
	    {
		{
	      std::array< unsigned long, 3 > npos, posBegin, posEnd;
	      bool beginEqualsEnd = false;
	      for( unsigned int cam = 0; cam < 3; ++cam )
		{
		  const unsigned long posA = points.at(e).position.at(cam);
		  const unsigned long posB = points.at(e+1).position.at(cam);

		  if( posA == posB or posA+1 == posB )
		    {
		      beginEqualsEnd = true;
		      break;
		    }

		  posBegin.at(cam) = std::min( posA+1, posB );
		  posEnd.at(cam) =  std::max( posA+1, posB );

		  const unsigned int mynpos = static_cast<unsigned int>( 0.5 * ( posA+1 + posB ) );
		  npos.at(cam) = mynpos;
		}

	      if( beginEqualsEnd ){
		continue;
              }else{
              }

	      for( unsigned int iter = 0; iter < 10; ++iter )
		{
		  std::vector< typename D :: BaseRangeVectorType > projPoint;
		  for( unsigned int cam = 0; cam < 3; ++cam )
		    {
		      const auto pt = knownPoints.at(cam).at( npos.at(cam) );
		      projPoint.push_back( {{ (double)pt.x, (double)pt.y }} );
		    }

		  typename D :: RangeVectorType point3d;
		  const double fit = distance.triangulate( projPoint, point3d );
		  if( (fit < fitTol and iter > 0) or
		      (fit < fitTol * 2.0) )
		    {
		      const auto it = std::find_if( points.begin(), points.end(),
						    [&]( const OrderedPoint& A )
						    { itested();
						      return A.position.at(0) == npos.at(0) and A.position.at(1) == npos.at(1) and A.position.at(2) == npos.at(2);
						    });

		      if( it == points.end() )
			{
			  points.push_back( { point3d, npos, ((e+1.0)/static_cast<double>(oldPointsSize)) } );
			  message(bDEBUG, "added fit: %f\n", fit);
			  for( unsigned int cam = 0; cam < 3; ++cam )
			    {
			      cv::Point pt;
			      pt.x = projPoint.at(cam).at(0);
			      pt.y = projPoint.at(cam).at(1);
#ifdef DISPLAY_FEMSCHEME_IMAGES
			      cv::circle( ims.at(cam), pt, 0,
					  cv::Scalar( 0, 255*((e+1.0)/static_cast<double>(oldPointsSize)), 0), -1 );
#endif
			    }
			  anyAdded = true;
			}
		      break;
		    }

		  for( unsigned int cam = 0; cam < 3; ++cam )
		    { untested();
		      typename D :: BaseRangeVectorType projPt;
		      projPt = distance.projection().at(cam).operator()( point3d );
		      const auto it = std::min_element(
                          knownPoints.at(cam).begin() + posBegin.at(cam),
                          knownPoints.at(cam).begin() + posEnd.at(cam),
							[projPt]( const cv::Point& A,
								  const cv::Point& B ) -> bool
                            { untested();
                              const double nA = ( A.x - projPt[0] )*( A.x - projPt[0] )
                                              + ( A.y - projPt[1] )*( A.y - projPt[1] );
                              const double nB = ( B.x - projPt[0] )*( B.x - projPt[0] )
                                              + ( B.y - projPt[1] )*( B.y - projPt[1] );
                              return nA < nB;
                            });

		      npos.at(cam) = std::distance( knownPoints.at(cam).begin(), it );
		      // std::cout << "cam: " << cam << " closest: " << npos.at(cam) << std::endl;
		    }
		}
		}
	    }

	  if( not anyAdded ){ untested();
	    break;
          }else{
          }

	  std::sort( points.begin(), points.end(),
		     []( const OrderedPoint& A, const OrderedPoint& B ) -> bool
		     {
		       return A.coordinate < B.coordinate;
		     }
		     );
	}

      // for( unsigned int i = 0; i < positions.size(); ++i )
      // 	{ untested();
      // 	  std::cout << points.at(i) << "\t"
      // 		    << positions.at(i).at(0) << " "
      // 		    << positions.at(i).at(1) << " "
      // 		    << positions.at(i).at(2) << " " << std::endl;
      // 	}
    }

    N=points.size();
    assert( N > 0 );
    // throw "done";

#ifdef DISPLAY_FEMSCHEME_IMAGES
    double length = 0;
    for( unsigned int i = 0; i < N; ++i )
      { untested();
	const auto pt3d = points.at(i).point;

	for( unsigned int cam = 0; cam < 3; ++cam )
	  { untested();
	    const auto projPoint = distance.projection().at(cam).operator()( pt3d );

	    cv::Point pt;
	    pt.x = projPoint.at(0);
	    pt.y = projPoint.at(1);
	    cv::circle( ims.at(cam), pt, 0,
			cv::Scalar( 255, 255*(i/static_cast<double>(N)),  255*(1.0-i/static_cast<double>(N)) ), -1 );
	  }

	if( i > 0 )
	  { untested();
	    length += ( pt3d - points.at(i-1).point ).norm();
	  }
      }
    std::cout << "estimated length: " << length << std::endl;
    cv::waitKey(10);
#endif

    for( unsigned int j = 0; j < mesh().N(); ++j )
      {
	const double uj = mesh().u(j);
	typename D :: RangeVectorType Xuj;

	if( j == 0 ) {
	    for( unsigned int d = 0; d < 3; ++d )
	      Xuj.at(d) = points.front().point.at(d);
        } else if( j == mesh().N() - 1 ) {
	    for( unsigned int d = 0; d < 3; ++d )
	      Xuj.at(d) = points.back().point.at(d);
        } else {
#if 0
	    const unsigned int oldmyIl = std::floor( uj * (N-1) );
	    const unsigned int oldmyIr = std::min( oldmyIl + 1, N-1 );
	    const double olduhat = uj  * ( N-1 ) - oldmyIl;
#endif
	    unsigned int myIl = 0;
		 for( unsigned int k = 0; k < N; ++k ) { itested();
			 if( uj > points.at(k).coordinate ){
				 myIl = k;
			 }
		 }
	    const unsigned int myIr = std::min( myIl + 1, N-1 );
	    const double uhat = ( uj - points.at(myIl).coordinate ) /
	      ( points.at(myIr).coordinate - points.at(myIl).coordinate );


	    for( unsigned int d = 0; d < 3; ++d )
	      Xuj[d] = ( 1 - uhat ) * points.at( myIl ).point[d]
		+ uhat * points.at( myIr ).point[d];

	    message(bTRACE, "new: %f %f %f\n", uhat, myIl, myIr);
        }

//        PositionFunctionType HACKposition( mesh(),
//            SubArray< Vector >( _solution_data.begin(), _solution_data.end() ) );
	_position.solution().assign( j, Xuj ); // HACK

#ifdef DISPLAY_FEMSCHEME_IMAGES
	if( j > 0 )
	  { untested();
	    const auto XuOld = solutionTuple_.position().evaluate( j-1 );

	    for( unsigned int cam = 0; cam < 3; ++cam )
	      { untested();
		const auto pX = distance.projection().at(cam).operator()( Xuj );
		const auto pXold = distance.projection().at(cam).operator()( XuOld );

		const cv::Point pt0( pX[0], pX[1] );
		const cv::Point pt1( pXold[0], pXold[1] );

		cv::line( ims.at(cam), pt0, pt1, cv::Scalar(255*(j/static_cast<double>(mesh().N() ) ), 255*(1.0 - j/static_cast<double>(mesh().N() ) ),0) );
	      }
	  }
#endif
      }

#ifdef DISPLAY_FEMSCHEME_IMAGES
    for( unsigned int cam = 0; cam < 3; ++cam )
      { untested();
	cv::imshow( "initial" + std::to_string(cam),
		    ims.at(cam).rowRange( bounds.at(cam).at(0),
					  bounds.at(cam).at(1) )
		    .colRange( bounds.at(cam).at(2),
			       bounds.at(cam).at(3)) );
      }
    std::cout << trialAndDate << std::endl;
    cv::waitKey(10);
    // throw "done for now";
#endif

//    std::cerr << oldposition().evaluate(0) << " " << oldposition().evaluate(1) << "\n";
//    PositionFunctionType HACKposition( mesh(),
//        SubArray< Vector >( _solution_data.begin(), _solution_data.end() ) );
//    assert(HACKposition.evaluate(0)!=HACKposition.evaluate(1));
  } // initializeXY
#endif // OPENCV INTIALIZEXY HACK
private:
//  PiecewiseLinearFunction<dim> _force_data;
  Vector _force_data;
  mutable Vector _curvature_data; // BUG: altered by probe
                                  // should be in accept?
  double _energy;

  SystemMatrix<PositionFunctionType, void>* _matrix_hack;
private: // hmmm
  ModelInterface<dim> const& model() const{
	  auto mm=&MeshObject<dim>::model();
	  assert(mm);
	  ModelInterface<dim> const* m=prechecked_cast<ModelInterface<dim> const* >(mm);
	  assert(m);
	  return *m;
  }

private: // meshfunctions
	MeshFunction<PositionFunctionType> _position; // "X"
	MeshFunction<CurvatureFunctionType> _curvature; // "Y"
	MeshFunction<CurvatureFunctionType> _whatnot; // "W"
	MeshFunction<PressureFunctionType> _pressure; // "P"
private: // stencils
     // [ D | S || B ]
     // [---+---++---]
     // [ S | M ||   ]
     // [---+---++---]
     // [---+---++---]
     // [ BT|   ||   ]
  // stencil_dim_diag<dim>  _dragstencil; // D
  stencil_dim_3diag<dim>  _dragstencil; // D  required for drag forses in skel
  stencil_dim_diag<dim>  _massstencil; // M/e
  stencil_dim_diag<dim>  _massstencil2; // M   if eta
  stencil_dim_diag<dim>  _massstencil3; // M   if eta
  stencil_dim_diag<dim>  _pmassstencil; // -M + ...  if eta
  stencil_asym<dim>      _presstencil; // B
  stencil_dim_3diag<dim> _stiffstencil;
  stencil_dim_3diag<dim> _pstiffstencil;

  RangeVectorType _head[KEEP_TIMESTEPS];
  RangeVectorType _mid[KEEP_TIMESTEPS];
  RangeVectorType _tail[KEEP_TIMESTEPS];
private: // parameters
	double _eta;
	double _time_scale;
	double _time_end;
}; // worm_3d

typedef worm_<3> worm_3d;

#endif // guard
