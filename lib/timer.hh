#pragma once

#include <time.h>
#include <iostream>
#include "error.hh"

// "s" is for "stupid"
// don't need sophistication yet.
class s_timer{
public:
	s_timer(std::string const& l="that"):_start( get_time() ), _label(l) {}

	double elapsed() const{
		return get_time()-_start;
	}

	~s_timer(){
//		print(std::cout);
		message(bLOG, _label + " took " + std::to_string(elapsed()) + " seconds\n");
	}
	void print(std::ostream& o){
		o << _label << " took " << elapsed() << " seconds\n";
	}

private:
	double get_time() const{
		return static_cast<double>(clock()) / static_cast<double>(CLOCKS_PER_SEC);
	}
	volatile double _start;
	std::string _label;

};


