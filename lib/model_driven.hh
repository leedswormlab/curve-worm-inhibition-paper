#ifndef MODEL_DRIVEN_HH
#define MODEL_DRIVEN_HH

#include <memory>
#include <iostream>
#include <functional>
#include <vector>
#include "vector.hh"
#include "timeprovider.hh"
#include "parameter.hh"
#include "matrix.hh"
#include "readMat.hh"
#include <vector>
#include "worm.hh"
#ifdef OpenCV_FOUND
#include "distancefunction.hh"
#include "forcefunction.hh"
#endif

#include "femscheme.hh"

template<const unsigned int mydim>
struct DrivenMuscleModel : public ModelDefault<mydim> { //
private:
  using BaseType = ModelDefault<mydim>;
  typedef typename BaseType::MeshCoordinate MeshCoordinate;

public:
  static const unsigned int dim = BaseType::dim;
  using RangeVectorType = typename BaseType::RangeVectorType;

  DrivenMuscleModel(ConfigParameters &parameters)
  //const std::function< RangeVectorType( double ) >& X0 ,
  //const std::function< double( double, double ) > beta )   // make constructor identical to (working) lambda model
      : BaseType(parameters),
        beta0_(parameters.get<double>("model.muscle.beta0", M_PI)),
        lambda_(parameters.get<double>("model.muscle.lambda", 2.0 / 1.5)),
        omega_(parameters.get<double>("model.muscle.omega", 2.0 * M_PI)),
        _freq(parameters.get<double>("model.beta.freq",0.5)),
        _eps(parameters.get<double>("model.I2.eps", 0)),
        _amp(parameters.get<double>("model.beta.amp",0)),
        _wave(parameters.get<double>("model.beta.wave",1)) {}
  double energyDensity(const MeshCoordinate &, const Entity &) const { return 0; }

  virtual RangeVectorType X0(const double s) const {
    untested();
    RangeVectorType ret;
    ret[0] = s;
    for (unsigned int d = 1; d < dim; ++d) {
      itested();
      ret[d] = 0.0;
    }
#ifdef NOSTICK_INIT
    ret[1] = s*(1.-s);
#endif

    return ret;
  }

  // this is, what ff/ReadModel does.
  // virtual RangeVectorType X0( const double s ) const
  // {
  //   RangeVectorType ret;
  //       ret[0] = s; 		//cos(-M_PI*s)/M_PI;
  //       ret[1] = 0.0; 		//sin(M_PI*s)/M_PI;

  //   for( unsigned int d = 2; d < dim; ++d )
  //     {
  //       ret[ d ] = 0.0;
  //     }

  //   return ret;
  // }


#if 1
  virtual double regularisation(MeshCoordinate const &s, const Entity &e) const {
    untested();
    // message(bTRACE, "reg %f %f %f\n", e0_, e2_, s);
    // s = .5 is the center.
    //olde *= ( 1.-time() * et_ );
    //return ( e0_ + e2_* (s-.5)*(s-.5) ) * (1+et_*e.time());
    const double num = 2.0 * std::sqrt((_eps + s) * (_eps + 1.0 - s));
    const double den = 1.0 + 2.0 * _eps;

    return BaseType::regularisation(s, e) * (num * num * num)
        / (den * den * den); // keep in mind Ma = -E*I2*beta still <<<<<<<<<<<<<<<<

  }
#endif

#if 0 // this is happening in ff
  const double eps = eps_;
const double num = 2.0 * std::sqrt( (eps + s)*(eps + 1.0 - s) );
const double den = 1.0 + 2.0 * eps;

return BaseType::e(s) * ( num*num*num ) / ( den*den*den ); // keep in mind Ma = -E*I2*beta still <<<<<<<<<<<<<<<<
#endif

  using BaseType::gamma;
  using BaseType::K;

  // virtual?
  virtual double beta(double const &s, const Entity &e) const {
    untested();
    assert(s < 1.01);
    assert(s == s);
    // forcing is:    beta = beta0 sin( 2 pi s / lambda - omega t )
    //const double q = 2.0 * M_PI / lambda_ * s - omega_ * e.time();
    //return beta0_ * sin( q );


    const double t = e.time();
    const double q = 2.0 * M_PI * s / _wave - 2.0 * M_PI * _freq * t;

    double output = _amp * sin(q);
    trace2("BETA", s, output);
    return 10.0*sin(q);
    // TODO: analyse: OUTPUT DOESN'T SEEM TO SHOW SAME FREQ OR WAVE
    // (infact, they seem to have got mixed up)
  }

private:
  const double beta0_, lambda_, omega_;

  double _freq;
  double _eps;
  double _amp;
  double _wave;
}; // DrivenMuscleModel
#endif // MODEL_DRIVEN_HH

// TODO
// generalise by getting global:
// dt - timestep (0.001)
// n - number of neurons (12)  << probably required separate parameter file for neural network & muscles
// m - number of muscles (48)



// vim:ts=8:sw=2
