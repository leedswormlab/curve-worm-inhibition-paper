#ifndef MODEL_NEUROMUSCULAR_HH
#define MODEL_NEUROMUSCULAR_HH

#include <memory>
#include <iostream>
#include <functional>
#include <vector>
#include "vector.hh"
#include "timeprovider.hh"
#include "parameter.hh"
#include "matrix.hh"
#include "readMat.hh"
#include <vector>
#include "worm.hh"
#ifdef OpenCV_FOUND
#include "distancefunction.hh"
#include "forcefunction.hh"
#endif
template< const unsigned int mydim >
struct NeuroMuscularModel
  : public ModelDefault< mydim > { //
private:
  using BaseType = ModelDefault< mydim >;
  typedef typename BaseType::MeshCoordinate MeshCoordinate;

public:
  static const unsigned int dim = BaseType :: dim;
  using RangeVectorType = typename BaseType :: RangeVectorType;


  NeuroMuscularModel( ConfigParameters& parameters ) 
    : BaseType( parameters ),
      beta0_( parameters.get<double>( "model.muscle.beta0", M_PI ) ),
      lambda_( parameters.get<double>( "model.muscle.lambda", 2.0 /1.5 ) ),
      omega_( parameters.get<double>( "model.muscle.omega", 2.0 * M_PI ) ),
      dt(parameters.get<double>("time.deltaT")),
      V(2, std::vector<double> (12, 0) ),
      dV(2, std::vector<double> (12, 0) ),
      h(2, std::vector<double> (12, 0) ),
      dh(2, std::vector<double> (12, 0) ),
      A(2, std::vector<double> (12, 0) ),
      dA(2, std::vector<double> (12, 0) ),
      neurtime(-1.0),
      torque(12)
    {}
//      Beta(NULL)
// {
//        Beta = new std::ofstream("/usr/not-backed-up/scjde/worms-curve-worm-copy/build3/Beta.txt");
//	if( !Beta->is_open() )
//        {
//          std::cerr << "Error: Beta didn't open" << std::endl;
//        }
// }


	double energyDensity(const MeshCoordinate&, const Entity&) const {return 0;}

	typedef PiecewiseLinearFunction< dim > PositionFunctionType;

  virtual RangeVectorType X0( const double s ) const
  { untested();
    RangeVectorType ret;
    ret[ 0 ] = s;
    for( unsigned int d = 1; d < dim; ++d )
      { untested();
	ret[ d ] = 0.0;
      }

    return ret;
  }

  //using BaseType::f;     // problem here
  using BaseType::gamma;
  using BaseType::K;
  //using BaseType::e;    // problem here (also with timeProvider() )


  void update_V(Entity const&e) const
      {
//	double C = 1;
//        double G = 10;
       for(int j=0; j < 12; j++){
	      // for now, use chain of FHN oscillators (discrete CPG driven) 
	      dV[0][j] = ( V[0][j] - V[0][j]*V[0][j]*V[0][j]/3 - h[0][j] + 1)/0.1 - (V[1][j] - 0*V[0][j]); // TODO consider more relevant neuron model (e.g. RMD, Wen2013)
	      dV[1][j] = ( V[1][j] - V[1][j]*V[1][j]*V[1][j]/3 - h[1][j] + 1)/0.1 - (V[0][j] - 0*V[1][j]);

	      for(int k=0; k<12-1; k++)
	      {
		  dV[0][k] = dV[0][k] - V[0][k+1];
		  dV[1][k] = dV[1][k] - V[1][k+1];
	      }

	      dh[0][j] = 0.01*(V[0][j] + 0.7 - 0.8*h[0][j]);
	      dh[1][j] = 0.01*(V[1][j] + 0.3 - 0.8*h[1][j]);

	      V[0][j] =  V[0][j] + dt*dV[0][j];
	      V[1][j] =  V[1][j] + dt*dV[1][j];
	      h[0][j] =  h[0][j] + dt*dh[0][j];
	      h[1][j] =  h[1][j] + dt*dh[1][j];
	    }
      }

  void update_A(Entity const&e) const
      {
       for(int j=0; j < 12; j++){
	      dA[0][j] = V[0][j] - V[1][j] - A[0][j]; // Assume nothing (yet) about overlapping muscles
	      dA[1][j] = V[1][j] - V[0][j] - A[1][j];

	      A[0][j] = V[0][j] + dt*dA[0][j];
	      A[1][j] = V[1][j] + dt*dA[1][j];
	    }
      }

  void update_torque(Entity const&) const
  {
    for(int j=0; j < 12; j++)
    {
	torque[j] = 10.0*(A[0][j] - A[1][j]);
    }
  }

  void update_beta(Entity const& e) const
  { 
	NeuroMuscularModel::update_V(e);
	NeuroMuscularModel::update_A(e);
	NeuroMuscularModel::update_torque(e);
  }


  // virtual?
  virtual double beta( double const& s, const Entity& e ) const
  { untested();

    if(neurtime >= e.time()) //[CHECK]
    {
      update_beta(e);

      int S = floor(s*(12-1));
      return V[0][S];//torque[S];
    }
  neurtime += dt; // how to get step size?? from param file?? 
  return 0; // really?
  }

private:
  const double beta0_, lambda_, omega_;
        mutable double dt;
  //std::ofstream* Beta;
  mutable std::vector< std::vector<double> > V;
  mutable std::vector< std::vector<double> > dV;
  mutable std::vector< std::vector<double> > h;
  mutable std::vector< std::vector<double> > dh;
  mutable std::vector< std::vector<double> > A;
  mutable std::vector< std::vector<double> > dA;
  mutable double neurtime;
  mutable std::vector<double> torque;
  //mutable std::vector<double> tauX, tauY, nuX, nuY, nubarX, nubarY, kappa;
}; // NeuroMuscularModel

#endif
