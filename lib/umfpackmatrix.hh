#ifndef UMFPACKMATRIX_HH
#define UMFPACKMATRIX_HH

#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>

// obsolete?!
struct UmfpackMatrixHolder {
  UmfpackMatrixHolder( const int n_ = 0 )
    : n(n_), nz(0), Ap( n_+1, 0 )
  { untested();
  }

  void clear()
  { untested();
    n = 0;
    nz = 0;
    Ap.clear();
    Ai.clear();
    Ax.clear();
  }

  template< class Vector >
  void call( const Vector& in, Vector& out ) const {  untested();
    assert(false);
    assert( in.size() == n );
    assert( out.size() == n );

    // clear out
    for( int i = 0; i < n; ++i )
      out[ i ] = 0;

    for( int i = 0; i < n; ++i )
      { untested();
	for( int j = Ap.at( i ); j < Ap[i+1]; ++j )
	  { untested();
	    out[ Ai.at( j ) ] += Ax.at( j ) * in.at( i );
	  }
      }
  }


  int n;
  int nz;

  std::vector<int> Ap; //colptr
  std::vector<int> Ai; //row ind
  std::vector<double> Ax;  //value.d

  int const* Apdata(){ return Ap.data(); }
  int const* Aidata(){ return Ai.data(); }
  double const* Axdata(){ return Ax.data(); }

//   int const* _Ap;
//   int const* _Ai;
//   double const* _Ax;
}; // UmfpackMatrixHolder

class UmfpackMatrix {
  typedef std::pair< unsigned int, double > ColValPairType;
  typedef std::vector< ColValPairType > RowType;
  typedef std::vector< RowType > DataType;

public:
  UmfpackMatrix( const unsigned int n )
    : data_( n ), n_( n ), e_(0)
  {}

  // matrix_graph.hh
  enum NORMALIZED_LAPLACIAN {_NORMALIZED_LAPLACIAN};
  template<class G>
  UmfpackMatrix( const G&, NORMALIZED_LAPLACIAN, double deltat=1. );

  void add( unsigned col, unsigned row, const double val ) { itested();
    RowType& coldata = data_.at( col );
    ColValPairType p( row, val );

    for( auto& r : coldata ) { itested();
	if( r.first == p.first )
	  { untested();
	    r.second += p.second;
	    return;
	  }
      }

    // not found
    ++e_;
    coldata.push_back( p );
  }

  double get( const unsigned int rowIdx, const unsigned int colIdx ) const
  { untested();
    const RowType& row = data_.at( rowIdx );

    for( const auto& r : row )
      { untested();
	if( r.first == colIdx )
	  { untested();
	    return r.second;
	  }
      }

    return 0;
  }

  void clear()
  { untested();
    for( auto& row : data_ )
      { untested();
	row.clear();
      }
  }

  // sets one row to \delta_{i,j}
  void setDirichletRow( const unsigned int rowIdx )
  { untested();
    bool doneDiagonal = false;
    for( unsigned int i = 0; i < n_; ++i )
      { untested();
	for( auto& p : data_.at( i ) )
	  { untested();
	    if( p.first == rowIdx )
	      { untested();
		if( i == rowIdx ) { untested();
		    p.second = 1.0;
		    doneDiagonal = true;
		} else { untested();
		    p.second = 0.0;
		}
	      }
	  }
      }

    if( not doneDiagonal ) { untested();
      add( rowIdx, rowIdx, 1.0 );
    }else{ untested();
    }
  }

  template< class Vector >
  void call( const Vector& x, Vector& b ) const
  {
    // clear b
    for( unsigned int i = 0; i < n_; ++i )
      b.at( i ) = 0;

    for( unsigned int i = 0; i < n_; ++i )
      {
	const RowType& rowi = data_.at( i );
	for( const auto& p : rowi )
	  { untested();
	    b.at( p.first ) += x.at( i ) * p.second;
	  }
      }
  }

  void print( std::ostream& s ) const
  { untested();
    s << "***** WARNING: THIS IS THE TRANSPOSE *****" << std::endl;

    for( auto row : data_ )
      { untested();
	if( row.empty() )
	  std::cout << "[empty row]";

	// first sort each row
	std::sort( row.begin(), row.end() );

	for( auto p : row )
	  s << " ( " << p.first << ", " << p.second << " )";
	s << std::endl;
      }
  }

  void matrix( UmfpackMatrixHolder& m ) const
  { untested();
    m.clear();

    // set matrix size
    m.n = n_;

    int rowStartIdx = 0;
    m.Ap.push_back( rowStartIdx );
    for( auto row : data_ ) { itested();
	std::sort( row.begin(), row.end() );

	// add data to matrix holder
	for( const ColValPairType& pair : row ) { itested();
	    m.Ai.push_back( pair.first );
	    m.Ax.push_back( pair.second );
	    rowStartIdx++;
	    m.nz++;
	  }

	// add end of row counter
	m.Ap.push_back( rowStartIdx );
    }
  }

  unsigned int n() const { return n_; }
  unsigned int e() const { return e_; }
  unsigned int cols() const { return n_; }
  unsigned int rows() const { return n_; }

private:
  DataType data_;
  const unsigned int n_;
  unsigned int e_;
}; // UmfpackMatrix

#endif

// vim:ts=8
