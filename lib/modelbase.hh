#pragma once

#include "discretefunction.hh"
#include "entity.hh"

class ModelBase{
public:
  virtual ~ModelBase(){
  }
protected:
  ModelBase(){
  }

public:
  virtual void load_force_rhs(){ untested();
  }

};

// specialized model for fixed world dimension.
template< unsigned int mydim >
class ModelInterface : public ModelBase {
public:
  static const unsigned int dim = mydim;
  using RangeVectorType = RangeVector< dim >;
  typedef double MeshCoordinate; // for now
//  typedef Entity<mydim> MeshObject;
protected:
  ModelInterface( ) {
    // custom constructor for struct "ModelInterface"
  }
public:

  typedef DiscreteFunctionInterface<mydim> DF;
  double extraEnergy( DiscreteFunctionInterface<mydim> const& position ) const
  {
    return 0;
  }
  virtual void extraForcing( const DF& X, DF& rhs ) const { untested();
  }

  virtual RangeVectorType X0( const double s ) const = 0;
  virtual RangeVectorType Force( const RangeVectorType& x ) const = 0;
  virtual double gamma( const double s ) const = 0;

  virtual double K( const double s ) const = 0;
  virtual double regularisation(MeshCoordinate const&, Entity const& ) const = 0; /* TODO this is not in general regularisation - bad name */
  virtual double mu( MeshCoordinate const&, Entity const& ) const = 0;

  virtual double beta( double const& s, Entity const& e ) const = 0;
  enum Constraint { velocity, measure };
  virtual Constraint constraint() const = 0;

//   double time() const { itested();
//     return timeProvider_.time();
//   }

  virtual void extra_curvature_manipulation(DiscreteFunctionInterface<mydim>& c) const{ // = 0
    // should nop here, override this, for things like diriclet.
    c.setDirichletBoundaries(0.);
    incomplete();
  }

#if 0 // not yet.
  // BUG: wrong base type
  virtual void extra_matrix_manipulation(
      InverseMassMatrix< PiecewiseLinearFunction<mydim> >& m) const{ untested();
    // should nop here, override this, for things like diriclet.
    m.setDirichletBoundaries();
    incomplete();
  }
#endif

  // BUG: we don't know the dimension of the model.
  // assume 1. (worm midline)
  virtual double energyDensity_at(  const RangeVectorType& X, const RangeVectorType& Y,
		  double const& s, Entity const&) const = 0;
  // maybe... (how to get meshcoordinate right?!)
  virtual double energyDensity(MeshCoordinate const& s, Entity const&) const = 0;
#if 0
  { untested();
    return 0;
  }
#endif
};
