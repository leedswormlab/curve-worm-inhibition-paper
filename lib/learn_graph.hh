#ifndef GRAPHLEARN_HH
#define GRAPHLEARN_HH

#include "image/annotations.hh"
#include "image/freak.hh"
#include "image/display.hh"
#include "graph/frames.hh"

// select subset of silhouette close to a
template<class K>
static void align_silhouette(cv::Mat& s, K const& a)
{ untested();
	unsigned wd=s.cols;
	unsigned ht=s.rows;

	cv::Mat annmask=cv::Mat::zeros(s.size(), s.type());
//	display_grayscale_frame("s", s);

	for(auto i: a){ untested();
		int row=i.first.y;
		int col=i.first.x;
		annmask.at<uchar>(row, col)=255;
	}

	////display_grayscale_frame("m", annmask);

	unsigned radius=20;
   cv::Mat element = cv::getStructuringElement(cv::MORPH_ELLIPSE,
	 cv::Size(radius*2+1, radius*2+1), cv::Point(radius, radius) );

   cv::Mat dilated;
   cv::dilate(annmask, dilated, element);
   annmask = dilated;
	//display_grayscale_frame("annmask", annmask);

	cv::bitwise_and(annmask, s, s);
	//display_grayscale_frame("done", s);
}

template<class FGPF, class A>
void learn_region(FGPF& fgpf, cv::Mat const& img, cv::Mat const& sil,
	A const& a, float ppmm)
{
//	assert(fgpf.ppmm()==ppmm) ?
	auto& g=fgpf.fg();
	std::vector<cv::KeyPoint> keypoints;
	fill_keypoints(keypoints, sil);
	// visit all keypoints, create descriptor matrix...
	FREAKMAP myfreak(keypoints, img, fgpf.pattern_scale());

	ANNOTATION detectlist;
	fgpf.find_known_points_masked(img, sil, detectlist, ppmm /* BUG */);

	message(bDEBUG, "learning %i keypoints\n", keypoints.size());

	unsigned cnt=0;
	unsigned cnta=0;
	unsigned skipped=0;
	for(auto const& keypoint: keypoints){
		auto feat=myfreak.get_features(keypoint);
		int ann=0;
		bool annotated=a.contains(keypoint.pt);
		cnta+=annotated;
		bool detected=detectlist.contains(keypoint.pt);
		if (annotated == detected){ itested();
			++skipped;
			continue;
		}else{
			ann = annotated;
			ann -= detected;
			++cnt;

			g.learn_pixel(feat, ann);
		}
	}
	message(bDEBUG, "relearnt %i keypoints, %d annotated, %d skipped\n", cnt, cnta, skipped);

//	for(auto const& keypoint: keypoints){
//		// add more edges...
//	}
}

// dissect frame, learn connected components.
template<class FGPF>
void learn_frame(FGPF& fgpf, draft::annotated_frame const& a)
{
	message(bDEBUG, "learn frame with %d annotations\n", a.annotation().size());
	ANNOTATION detectlist;

//	auto& g=fgpf.fg();
	float ppmm(a.ppmm());

	// need annotated_frame::get_silhouette()?
	//FREAKMAP myfreak(a, g.pattern_scale());
	std::vector<cv::KeyPoint> keypoints;
	cv::Mat mask;
   std::vector<std::vector<cv::Point> > contours;

// FIXME: use the other pointfinder
#if 0
	silhouetteImageDilate(a.frame(), sil, fgpf.dilate_tgt_sil(), 200);
#else
	silhouetteImageContours(a.frame(), contours);

	for(auto& c : contours){ untested();
		unsigned ca(cv::contourArea(c));
		if( ca > 3500){ untested();
			message(bDANGER, "skipping monster worm %d\n", ca);
		}else{ untested();
		}

		mask=cv::Mat::zeros(a.frame().size(), a.frame().type());
		std::vector<std::vector<cv::Point> > crazy_syntax(1, c);
		cv::drawContours( mask, crazy_syntax, -1 /*sic*/, 255, CV_FILLED );
		cv::Mat img=a.frame().clone(); // need a clone, dilated masks can overlap!!

		dilate_mask(mask, 5);
		adjust_brightness_max(img, mask);

	   learn_region(fgpf, img, mask, a.annotation(), ppmm);
	}
#endif

	//display_sil_bb("siluncropped", sil);
	// align_silhouette(sil, a.annotation());
	//display_sil_bb("silcropped", sil);
	//
}

#endif
