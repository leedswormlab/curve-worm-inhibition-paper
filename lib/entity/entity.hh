/* Copyright (C) 2017 Felix Salfelder
 * Author: Albert Davis <aldavis@gnu.org>
 *
 * Inspired by "Gnucap", the Gnu Circuit Analysis Package
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 */
#pragma once

#include <algorithm>
#include <cassert>
#include <map>
#include <memory>
#include <string>
#include <vector>
#include "../trace.hh"
#include "../probe/probe.hh"
#include "../error.hh"

#ifndef prechecked_cast
#define prechecked_cast dynamic_cast
#endif

#define KEEP_TIMESTEPS 3
#define NEVER 1e99
#define DTMIN 1e-9

// preliminary
// need more of this, different
#define TRTOL 1.
#define RELTOL 1.
#define CHGTOL 1.
#define TRREJECT 100. // incomplete

class ModelBase;

/**
 *  \defgroup Entity Entity: base object for the simulator
 */

/**
 *  \addtogroup Entity
 *  @{
 */

/**
 *  \class Entity_base
 *
 *  An Entity_base is the base object of our simulations. The
 *  interface is based on ideas from Gnucap. More details are given in
 *  the manual available at
 *  http://www.gnu.org/software/gnucap/gnucap-man.pdf Especially the
 *  basic solution algorithm Sec 7.2.2.
 *
 *  The basic time loop is:
\code{.cpp}
// before doing anything
load()

// on going into the time loop
tr_begin() // get ready
tr_begin_accept() // accept initial data
for (each time step) {
  tr_advance() // precalculate and propagate
  for (each iteration) {
    tr_load() // build the matrix of equations
    tr_eval() // solve the resulting system of equations
  }
  if (converged) {
    tr_review() // how are we doing? suggest time step
  }
  if (no problems) {
    tr_accept() // postcalculate and accept data
  }
}
\endcode
 *
 *  Importantly: "The functions referred to above are actually loops
 *  that call that function for all devices...  For all
 *  of them, it is possible that they may not be called. If there is
 *  evidence that the result will not change from the last time it was
 *  called, it probably will not be called. Since this algorithm is
 *  not perfect, it is possible that any particular function may be
 *  called twice, so they are written so calling more than once is
 *  equivalent to calling once."
 */
class Entity_base {
public:
  //! \brief Deleted default constructor.
  //! An entity must have a label.
  Entity_base() = delete;

  //! \brief type of the probe_map
  using Probe_map = std::map< std::string, std::shared_ptr< const Probe_base > >;

  //! \brief Named entity constructor.
  //! \param[in] label  entity label; cannot contain ' ', ':' or '.'.
  explicit Entity_base(std::string const &label)
    : _label(label) {
    std::cout << "new entity! " << long_label() << std::endl;
    // check probe label does not contain ' ', ':' or '.'
    assert( std::all_of( label.begin(), label.end(),
			 []( const char c ){
			   return c != ' ' and
			     c != ':' and
			     c != '.';
			     }));
  }

  //! delete copy constructor
  Entity_base(const Entity_base &other) = delete;

  //! name of entity
  std::string const &long_label() const {
    // incomplete
    return _label;
  }

  //! called at the start of the simulation before any others
  virtual void load() {}
  //! called once per simulation before the time loop
  virtual void tr_begin() { untested(); }
  //! called once per simulation before the time loop after begin
  virtual void tr_begin_accept() { untested(); }
  //! called once per time step
  virtual void tr_advance() { untested(); }
  //! called once per solver iteration - does the assembly
  virtual void tr_load() { untested(); }
  //! called once per solver iteration - does the solve
  virtual void eval() { untested(); }
  //! called after the solver iteration
  virtual double tr_review() { return 1e99; }
  //! called at the end of the timestep
  virtual void tr_accept() {}

  //! \brief access probe via generic Probe_base shared_ptr
  //!
  //! This will return nullptr if name is not available from this
  //! entity.
  //!
  //! \param[in] name  name of the probe
  virtual std::shared_ptr< const Probe_base > get_probe( const std::string& name ) const {
    // find probe
    auto it = _probe_map.find( name );
    auto probe_ptr = ( it != _probe_map.end() ) ? it->second : nullptr;
    if( probe_ptr ) {
      // if found return it
      return probe_ptr;
    } else {
      // if not found return nullptr and show warning
      std::cerr << "probe " << name << " not found in " << this->long_label() << std::endl;
      std::cerr << "options are:";
      for( auto p : _probe_map ) {
	std::cerr << " " << p.first;
      }
      std::cerr << std::endl;
      return std::shared_ptr< const Probe_base >( nullptr );
    }
  }

  //! \brief connect probe via generic Probe_base shared_ptr
  //!
  //! The does nothing if a probe with the same name is already
  //! connected.
  //!
  //! \param[in] probe_ptr  the probe to connect
  virtual void connect_probe( std::shared_ptr< const Probe_base > probe_ptr ) {
    if( not probe_ptr ) {
      std::cerr << "cannot connect empty probe to " << this->long_label() << std::endl;
      return;
    }

    // check if it already exists
    const auto it = _probe_map.find( probe_ptr->name() );
    if( it != _probe_map.end() ) {
      std::cerr << "probe " << probe_ptr->name() << " already exists in " << this->long_label() << std::endl;
      return;
    }

    // add to map
    _probe_map.emplace( probe_ptr->name(), probe_ptr );
  }

  //! \brief list of essential ports that must be filled
  //!
  //! An error will be thrown if the port is not available.
  //! See also World_entity::connect_ports for more details.
  virtual std::vector< std::string > essential_ports() const {
    return std::vector< std::string >();
  }
  //! \brief list of optional ports that can be filled
  //!
  //! A warning will be displayed if the port is not available.
  //! See also World_entity::connect_ports for more details.
  virtual std::vector< std::string > optional_ports() const {
    return std::vector< std::string >();
  }

  //! const access to the probe storage
  const Probe_map& probe_map() const {
    return _probe_map;
  }

protected:
  /**
   *  \brief stores available probes to make them accessible to the outside world
   *
   *  \tparam    T      type of probe
   *  \param[in] probe  the probe which is provided
   *
   *  If the cast fails an error is thrown.
   */
  template< typename T >
  void provide_probe( std::shared_ptr< const T > probe_ptr ) {
    assert( probe_ptr );
    if( auto out = std::static_pointer_cast<const Probe_base>(probe_ptr) ) {
      connect_probe(out);
    } else {
      throw exception_invalid( std::string("unable to provide probe ")
			       + probe_ptr->name()
			       + std::string(" from ") + long_label() );
    }
  }

public:
  // TODO should this be public
  /**
   *  \brief provides internal access to probes by (possibly null) pointer
   *
   *  \tparam    T     type of the probe
   *  \param[in] name  the name of the probe wanted
   *
   *  If the probe name is not found then a warning provides all
   *  available options. If the probe name is found but the cast fails
   *  a nullptr is returned.
   */
  template< typename T >
  std::shared_ptr<const T> see_probe_ptr( const std::string& name ) const {
    for( const auto& pp : _probe_map ) {
      if( pp.first == name ) {
	return std::dynamic_pointer_cast< const T, const Probe_base >( pp.second );
      }
    }

    std::cerr << "WARNING: no probe called " << name << " connected to "
	      << long_label() << std::endl;
    std::cerr << "options are:";
    for (const auto &pp : _probe_map) {
      std::cerr << " " << pp.first;
    }
    std::cerr << std::endl;
    return nullptr;
  }

private:
  // entity label
  const std::string _label;
  // probe storage
  Probe_map _probe_map;
}; // entity_base

/**
 *  \class Entity
 */
class Entity : public Entity_base {
public:
	Entity(std::string const& label)
	  : Entity_base( label ) {
		}
public: // (almost) gnucap interface

	virtual void tr_regress() { untested(); }

public: // don't use
	virtual double review() { return tr_review(); }
protected:
public: // used in scheme... :/
	double time() const{
		return _time[0];
	}
protected:
  double _time[KEEP_TIMESTEPS];
  double _newtime;
}; // entity

/**
 * @}
 */
