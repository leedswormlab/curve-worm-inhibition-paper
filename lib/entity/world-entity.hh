#pragma once

#include <algorithm>
#include <cassert>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#include "entity.hh"

/**
 *  World_entity
 *
 *  A specialism of Entity which is used as a container for all other
 *  entities.
 */
class World_entity : public Entity_base {
public:
  using pEntity_base = std::shared_ptr< Entity_base >;

  World_entity( const std::string name = "World_entity" )
    : Entity_base( name ) {}

  /** non interface method **/
  // add subentity
  void add_sub_entity( pEntity_base e ) {
    _sub_entities.push_back( e );
  }

  // called at the start of the simulation before any others
  virtual void load() {
    connect_ports();

    std::for_each( _sub_entities.begin(), _sub_entities.end(),
		   []( pEntity_base e ){ e->load(); } );
  }
  // called once per simulation before the time loop
  virtual void tr_begin() { untested();
    std::for_each( _sub_entities.begin(), _sub_entities.end(),
		   []( pEntity_base e ){ e->tr_begin(); } );
  }
  // called once per simulation before the time loop after tr_begin
  virtual void tr_begin_accept() { untested();
    std::for_each( _sub_entities.begin(), _sub_entities.end(),
		   []( pEntity_base e ){ e->tr_begin_accept(); } );
  }
  // called once per time step
  virtual void tr_advance() { untested();
    std::for_each( _sub_entities.begin(), _sub_entities.end(),
		   []( pEntity_base e ){ e->tr_advance(); } );
  }
  // called once per solver iteration - does the assembly
  virtual void tr_load() { untested();
    std::for_each( _sub_entities.begin(), _sub_entities.end(),
		   []( pEntity_base e ){ e->tr_load(); } );
  }
  // called once per solver iteration - does the solve
  virtual void eval() { untested();
    std::for_each( _sub_entities.begin(), _sub_entities.end(),
		   []( pEntity_base e ){ e->eval(); } );
  }
  // called after the solver iteration
  // TODO should this be max/max/average?
  virtual double tr_review() {
    double min = 0;
    for( auto& e : _sub_entities ) {
      min = std::min( min, e->tr_review() );
    }
    return min;
  }
  // called at the end of the timestep
  virtual void tr_accept() {
    std::for_each(_sub_entities.begin(), _sub_entities.end(),
		  []( pEntity_base e ) { e->tr_accept(); });
  }

protected:
  void connect_port_to_entity( const std::shared_ptr< const Probe_base >& probe_ptr,
			       pEntity_base& pEntity ) {
    assert( probe_ptr );
    pEntity->connect_probe( probe_ptr );
    std::cout << "connected " << probe_ptr->name() << " to " << pEntity->long_label() << std::endl;
  }

  void connect_ports() {
    for (auto &pEntity : sub_entities()) {
      for (auto &port_name : pEntity->essential_ports()) {
	bool found = false;

	// search in current entity
	for (auto p : probe_map()) {
	  const std::string probe_name =
	      p.first;
	  if (probe_name == port_name) {
	    found = true;
	    connect_port_to_entity(p.second, pEntity);
            break;
          }
        }

	if(found) continue;

        // search other subentities
	for (auto &other_pEntity : sub_entities()) {
	  for (auto p : other_pEntity->probe_map()) {
	    const std::string probe_name =  p.first;
	    if (probe_name == port_name) {
	      found = true;
	      connect_port_to_entity( p.second, pEntity );
	      break;
	    }
          }

	  if(found) break;
        }

	if( not found ) {
	  throw exception_invalid( "probe name: " + port_name );
	}
      }

      for( auto& port_name : pEntity->optional_ports()){
	bool found = false;

	// search in current entity
	for (auto p : probe_map()) {
	  const std::string probe_name =
	      p.first;
	  if (probe_name == port_name) {
	    found = true;
	    connect_port_to_entity(p.second, pEntity);
            break;
          }
        }

	if(found) continue;

        // search other subentities
	for (auto &other_pEntity : sub_entities()) {
	  for (auto p : other_pEntity->probe_map()) {
	    const std::string probe_name =  p.first;
	    if (probe_name == port_name) {
	      found = true;
	      connect_port_to_entity( p.second, pEntity );
	      break;
	    }
          }

	  if(found) break;
        }

	if( not found ) {
	  std::cerr << "WARNING: " << "unable to find probe for " << port_name
		    << " in " << pEntity->long_label() << "\n";
	}
      }
    }

#if 0
    // for each entity
    for( auto&& entity : _sub_entities ) {
      // for each essential port
      for( auto&& probe_name : entity->essential_ports() ) {
	// split the name
	const auto split_name = split_probe_name( probe_name );

	// find entity
	auto it = std::find_if( _sub_entities.begin(), _sub_entities.end(),
				[&split_name]( const pEntity_base e ) {
				  return e->long_label() == split_name.first;
				});
	if (it == _sub_entities.end()) {
	  std::cerr << "ERROR: unable to connect " << probe_name << " to " << entity->long_label() << std::endl;
	  std::cerr << "no subentity called " << split_name.first << " in "
		    << long_label() << std::endl;
	  std::cerr << "options are:";
	  for (auto& e : _sub_entities) {
	    std::cerr << " " << e->long_label();
          }
          std::cerr << std::endl;
          abort();
        }

	// find probe
	const auto& e = *it;
	auto probe_ptr = e->get_probe( split_name.second );
	if( not probe_ptr ) {
	  std::cerr << "ERROR: unable to connect " << probe_name << " to " << entity->long_label() << std::endl;
	  abort();
	}

	// connect it
	entity->connect_probe( probe_ptr );
	std::cout << "connected " << probe_name << " to " << entity->long_label() << std::endl;
      }

      // for each optional port
      for( auto&& probe_name : entity->optional_ports() ) {
	// split the name
	const auto split_name = split_probe_name( probe_name );

	// find entity
	auto it = std::find_if( _sub_entities.begin(), _sub_entities.end(),
				[&split_name]( const pEntity_base e ) {
				  return e->long_label() == split_name.first;
				});
	if (it == _sub_entities.end()) {
	  std::cerr << "WARNING: unable to connect " << probe_name << " to " << entity->long_label() << std::endl;
	  std::cerr << "no subentity called " << split_name.first << " in "
		    << long_label() << std::endl;
	  std::cerr << "options are:";
	  for (auto& e : _sub_entities) {
	    std::cerr << " " << e->long_label();
          }
          std::cerr << std::endl;
	  continue;
        }

	// find probe
	const auto& e = *it;
	auto probe_ptr = e->get_probe( split_name.second );
	if( not probe_ptr ) {
	  std::cerr << "WARNING: unable to connect " << probe_name << " to " << entity->long_label() << std::endl;
	  continue;
	}

	// connect it
	entity->connect_probe( probe_ptr );
	std::cout << "connected " << probe_name << " to " << entity->long_label() << std::endl;
      }
    }
#endif
  }

  const std::vector< pEntity_base >& sub_entities() const {
    return _sub_entities;
  };
  std::vector< pEntity_base >& sub_entities() {
    return _sub_entities;
  };
private:
  std::vector< pEntity_base > _sub_entities;
};
