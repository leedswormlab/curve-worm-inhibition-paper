#pragma once

// main entity
#include "world-entity.hh"

// possible output entities
#include "../io/simple-io.hh"
#include "../io/stream-io.hh"
#include "../io/vtuwriter.hh"

#ifdef GAMELIBS_FOUND
#include "../io/game/online-output.hh"
#endif

// mesh management
#include "../mesh.hh"
// time management
#include "../timeprovider.hh"

// TODO and compare to what mortiz has done too

class Output_entity : public World_entity {
  using pEntity_base = World_entity :: pEntity_base;

public:
  Output_entity( const Mesh& mesh,
		 const TimeProvider& timeProvider )
    : World_entity( "output_entity" ) {

    pEntity_base output_ptr;
    for (auto &output_ent : Parameters::getStringVector("output.entity")) {
      if (output_ent == "simple") {
	output_ptr = std::make_shared<SimpleWriter>(
	    timeProvider, Parameters::getString("output.prefix"),
	    Parameters::get<double>("output.timestep"),
	    Parameters::get<double>("output.start", timeProvider.startTime()));
	this->add_sub_entity(output_ptr);
      } else if (output_ent == "vtu") {
	output_ptr = std::make_shared<VTUWriter_entity>(
	    mesh, timeProvider, Parameters::getString("output.prefix"),
	    Parameters::get<double>("output.timestep"),
	    Parameters::get<double>("output.start", timeProvider.startTime()));
	this->add_sub_entity(output_ptr);
      } else if (output_ent == "cout") {
	output_ptr = std::make_shared<StreamWriter>(
	    timeProvider, std::cout, Parameters::get<double>("output.timestep"),
	    Parameters::get<double>("output.start", timeProvider.startTime()));
	this->add_sub_entity(output_ptr);
      } else if (output_ent == "cerr") {
	output_ptr = std::make_shared<StreamWriter>(
	    timeProvider, std::cerr, Parameters::get<double>("output.timestep"),
	    Parameters::get<double>("output.start", timeProvider.startTime()));
	this->add_sub_entity(output_ptr);
      }
#ifdef GAMELIBS_FOUND
      else if (output_ent == "opengl") {
	output_ptr = std::make_shared<OpenGLWriter>( timeProvider, mesh );
	this->add_sub_entity(output_ptr);
      }
#endif
    else {
	// no output is allowed
      }
    }

    for( auto pEntity : sub_entities() ) {
      for( auto port_name : pEntity->essential_ports() ) {
	_essential_ports.push_back( port_name );
      }
      for( auto port_name : pEntity->optional_ports() ) {
	_optional_ports.push_back(port_name);
      }
      for (auto pair : pEntity->probe_map()) {
	std::cout << "forwarding probe " << pair.first << "\n";
	provide_probe(pair.second);
      }
    }
  }

  virtual std::vector< std::string > essential_ports() const {
    return _essential_ports;
  }
  virtual std::vector< std::string > optional_ports() const {
    return _optional_ports;
  }

private:
  std::vector< std::string > _essential_ports;
  std::vector< std::string > _optional_ports;
};
