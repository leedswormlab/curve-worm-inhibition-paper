#pragma once
#include <algorithm>

#include "../entity.hh"
#include "problem.hh"

#include "../../mesh.hh"
#include "../../discretefunction.hh"
#include "../../matrix.hh"
#include "../../solver.hh"
#include "../../stencil.hh"
#include "../../timeprovider.hh"
#include "../../matrix.hh"

/**
 *  Viscoelastic mechanics entity
 *
 *  TODO doc me
 */
class Viscoelastic_mechanics_entity : public Entity_base {
  static const unsigned worlddim = WORLDDIM;
  using Problem = Mechanics_problem<worlddim>;

  using SolutionTupleType = NamedFunctionTuple< worlddim >;
  using PositionFunctionType = typename SolutionTupleType::Discrete_function_type< worlddim, 1 >;
  using MomentFunctionType = typename SolutionTupleType::Discrete_function_type< worlddim, 1 >;
  using CurvatureFunctionType = typename SolutionTupleType::Discrete_function_type< worlddim, 1 >;
  using PressureFunctionType = typename SolutionTupleType::Discrete_function_type< 1, 0 >;


  using RangeVectorType = typename PositionFunctionType :: RangeVectorType;

  typedef UmfpackLU<double, int> SolverType;
  using SystemOperatorType = SystemMatrix< PositionFunctionType, SolverType >;

  using ScalarCurvatureFunctionType = PiecewiseLinearFunction<1>;
public:
  // named entity constructor
  Viscoelastic_mechanics_entity(std::string const &label, const Mesh& mesh, const TimeProvider& time_provider )
    : Entity_base(label),
      _mesh( mesh ),
      _time_provider( time_provider ),
      _problem_ptr(nullptr),
      // solution variables
      _solutionTuple( mesh ),
      _oldSolutionTuple( mesh ),
      _rhsTuple( mesh ),
      // matrix components
      // TODO what do we want here?
      _dragstencil( "drag matrix" ),
      _massstencil( "mass matrix" ),
      _mass2stencil( "mass2 matrix" ),
      _presstencil( "pressure matrix" ),
      _emassstencil( "e mass matrix" ),
      _pmassstencil( "proj mass matrix" ),
      _stiffstencil( "stiffness matrix" ),
      _pstiffstencil( "proj stiffness matrix" ),
      _constraintstencil( "constraint matrix" ) {
    // problem chooser
    const std::string problem_str = Parameters::getString( "mechanics.problem", "default" );
    if (problem_str == "dynamic_elasticity") {
      std::cout << "using dynamic_elasticity mechanics problem\n";
      _problem_ptr.reset(
	  new Mechanics_dynamic_elasticity_problem<worlddim>(time_provider));
    } else if( problem_str == "default" ) {
      std::cout << "using default mechanics problem\n";
      _problem_ptr.reset(new Mechanics_problem<worlddim>(time_provider));
    } else {
      std::cerr << "ERROR: unrecognised mechanics problem" << std::endl;
    }
    assert(_problem_ptr);

    // discrete functions
    _solutionTuple.add_function< worlddim, 1 >( "position" );
    _oldSolutionTuple.add_function< worlddim, 1 >( "position" );
    _rhsTuple.add_function< worlddim, 1 >( "position" );
    _solutionTuple.add_function< worlddim, 1 >( "moment" );
    _oldSolutionTuple.add_function< worlddim, 1 >( "moment" );
    _rhsTuple.add_function< worlddim, 1 >( "moment" );
    _solutionTuple.add_function< worlddim, 1 >( "curvature" );
    _oldSolutionTuple.add_function< worlddim, 1 >( "curvature" );
    _rhsTuple.add_function< worlddim, 1 >( "curvature" );
    _solutionTuple.add_function< 1, 0 >( "pressure" );
    _oldSolutionTuple.add_function< 1, 0 >( "pressure" );
    _rhsTuple.add_function< 1, 0 >( "pressure" );

    // probes
    provide_probe( std::make_shared< const Centre_of_mass_probe >( *this ) );
    provide_probe( std::make_shared< const Head_probe >( *this ) );
    provide_probe( std::make_shared< const Tail_probe >( *this ) );
    provide_probe( std::make_shared< const Length_probe >( *this ) );
    provide_probe( std::make_shared< const Position_probe >( "mechanics.position", &position() ) );
    provide_probe( std::make_shared< const Moment_probe >( "mechanics.moment", &moment() ) );
    provide_probe( std::make_shared< const Curvature_probe >( "mechanics.curvature", &curvature() ) );
    provide_probe( std::make_shared< const Pressure_probe >( "mechanics.pressure", &pressure() ) );
    provide_probe( std::make_shared< const Kappa_probe >( *this ) );
    provide_probe( std::make_shared< const Energy_probe >( *this ) );
    provide_probe( std::make_shared< const Elastic_force_probe >( *this ) );
    provide_probe( std::make_shared< const Viscous_force_probe >( *this ) );
  }

  // called at the start of the simulation before any others
  virtual void load() {
    // connect problem probes
    _problem_ptr->connect_probes( *this );

    // set up geometry
    for( auto&& element : mesh().elements() ) {
      _geometry_cache.push_back( ElementGeometry( oldPosition(), element ) );
    }

    // set up stencil layout
    setup_matrix_stencils();
  }
  // called once per simulation before the time loop
  virtual void tr_begin() { untested();
    set_initial_condition();
    // set old solution from new solution
    _oldSolutionTuple.assign( _solutionTuple );
    // update geometry
    for( auto& geo : _geometry_cache ) {
      geo.update();
    }
  }
  // called once per time step at the start of the time step
  virtual void tr_advance() { untested();
    // set old solution from new solution
    _oldSolutionTuple.assign( _solutionTuple );
  }
  // called once per solver iteration - does the assembly
  virtual void tr_load() {
    untested();

    implicitOperator_.clear();
    _rhsTuple.clear();

    // update geometry
    for( auto& geo : _geometry_cache ) {
      geo.update();
    }

    // drag terms
    load_matrix_drag();
    // pstiff terms
    load_matrix_pstiff();
    // non symmetric terms
    load_matrix_pres();
    // more drag?
    load_dynamic_drag();
    // mass matrices
    load_matrix_mass();
    load_matrix_mass2();
    load_matrix_emass();
    load_matrix_pmass();
    // stiffness matrix
    load_matrix_stiff();
    // body forcing
    load_force_rhs();
    // explicit terms
    load_more_rhs();
    // beta moment rhs
    load_moment_rhs();
    // curvature rhs for bcs
    load_curvature_rhs();
    // pressure rhs
    load_pressure_rhs();

#ifndef NDEBUG
    {
      // test stiffness matrix
      const unsigned int n = mesh().N();
      const Vector ones( worlddim*n, 1.0 );

      Vector out( worlddim*n );
      _stiffstencil.mv( out, ones );

      if( out.norm2() > 1.0e-15 ) {
	std::cerr << "stiffness matrix failure at timestep: " << _time_provider.iteration() << std::endl;
	std::cerr << "error: " << out.norm2() << std::endl;
	// std::cerr << out << std::endl;
	abort();
      }
    }
#ifdef CONSTANT_RADIUS
    // with non constant viscoelasticity this fails
    {
      // test stiffness matrix
      const unsigned int n = mesh().N();
      const Vector ones( worlddim * n, 1.0);

      Vector out( worlddim * n);
      _pstiffstencil.mv(out, ones);

      if (out.norm2() > 1.0e-15) {
	std::cerr << "pstiffness matrix failure at timestep: "
		  << _time_provider.iteration() << std::endl;
	std::cerr << "error: " << out.norm2() << std::endl;
	// std::cerr << out << std::endl;
	abort();
      }
    }
#endif
    {
      double comd = 0;
      // for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
      for( auto&& entity : mesh().elements() ) {
	const ElementGeometry geo( oldPosition(), entity );
	const unsigned l_i = entity.left().index();
	const unsigned r_i = entity.right().index();
	const auto com = 0.5 * geo.q() * ( position().eval( r_i )
					   + position().eval( l_i ) );
	for( unsigned int d = 0; d < worlddim; ++d )
	  comd += com.at(d);
      }

      // test mass matrix
      const unsigned int N = mesh().N();
      const Vector ones(worlddim * N, 1.0);
      Vector MX(worlddim * N);
      Vector X( worlddim * N );
      for( unsigned int n = 0; n < worlddim*N; ++n ) {
	X.at(n) = position().at(n);
      }

      _massstencil.mv(MX, X);
      double t = MX * ones;
      if( std::abs( t - comd ) > 1.0e-10 ) {
	std::cout << "mass test:" << std::abs( t - comd ) << std::endl;
	abort();
      }
    }
#ifdef FLAT_WORM
    {
      double comd = 0;
      // for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
      for( auto&& entity : mesh().elements() ) {
	const ElementGeometry geo( oldPosition(), entity );
	const unsigned l_i = entity.left().index();
	const unsigned r_i = entity.right().index();
	const auto com = 0.5 * geo.q() * ( position().eval( r_i )
					   + position().eval( l_i ) );
	for( unsigned int d = 0; d < worlddim; ++d )
	  comd += com.at(d);
      }
      comd /= deltaT();

      const unsigned int N = mesh().N();
      Vector ones( worlddim * N, 1.0 );
      Vector tau( worlddim * N );
      Vector nu( worlddim * N );
      Vector DX( worlddim * N );
      Vector X( worlddim*N );
      for( unsigned int n = 0; n < worlddim*N; ++n ) {
	X.at(n) = position().at(n);
	tau.at(n) = ( n % worlddim == 0 ) ? 1 : 0;
	nu.at(n) = ( n % worlddim == 1 ) ? 1 : 0;
      }

      _dragstencil.mv( DX, X );
      const double t1 = DX * tau;
      if( std::abs( t1 - comd ) > 1.0e-10 ) {
	std::cerr << "drag test 1: " << std::abs(t1 - comd) << std::endl;
	abort();
      }

      DX.clear();
      _dragstencil.mv( DX, nu );
      const double t2 = DX * nu;
      if( std::abs( t2 - problem().K() / deltaT() ) > 1.0e-10 ) {
	std::cerr << "drag test2: " << std::abs( t2 - problem().K() / deltaT() ) << std::endl;
	abort();
      }

      DX.clear();
      _dragstencil.mv( DX, tau );
      const double t3 = DX * tau;
      if( std::abs( t3 - 1.0 / deltaT() ) > 1.0e-10 ) {
	std::cerr << "drag test 3: " << std::abs( t3 - 1.0 ) << std::endl;
	abort();
      }

      DX.clear();
      _dragstencil.mv( DX, tau );
      const double t4 = DX * nu;
      if( std::abs( t4 ) > 1.0e-10 ) {
	std::cerr  << "drag test 4: " << std::abs( t4 ) << std::endl;
      }

      DX.clear();
      _dragstencil.mv( DX, nu );
      const double t5 = DX * tau;
      if( std::abs( t5 )> 1.0e-10 ) {
	std::cerr << "drag test 5: " << std::abs( t5 ) << std::endl;
	abort();
      }
    }
#endif
#endif
  }
  // called once per solver iteration - does the solve
  virtual void eval() { untested();
    // construct full matrix
    load_matrix_stencils();

#ifndef NDEBUG
#ifdef FLAT_WORM
    // this should be verified
    Vector tmp( _solutionTuple.size() );
    implicitOperator_.call( static_cast<Vector>(_solutionTuple), tmp );
    tmp -= _rhsTuple;
    if( tmp.norm2() > 1.0e-10 ) {
      implicitOperator_.print();
      std::cerr << "rhs test: " << tmp.norm2() << std::endl;
      implicitOperator_.call( static_cast<Vector>(_solutionTuple), tmp );
      for( unsigned int n = 0; n < _solutionTuple.size(); ++n ) {
      	std::cout << n << ": " << tmp.at(n) << " "
    		  << _rhsTuple.at(n) << " "
    		  << std::abs( tmp.at(n) - _rhsTuple.at(n) )
    		  << std::endl;
      }
      abort();
    }
#endif
#endif
    // TODO add tests for new matrices

    // do the solve
    implicitOperator_.solve( _solutionTuple, _rhsTuple );
  }
  // called after the solver iteration
  virtual double tr_review() { return 1e99; }
  // called at the end of the timestep
  virtual void tr_accept() {
#ifndef DNDEBUG
    { // check worm length
      double sum = 0;
      for( auto&& element : mesh().elements() ) {
	const ElementGeometry geo(position(), element);
	sum += geo.q();
      }
      if (sum < problem().gamma() - 1.0e-10) {
	std::cerr << "length of worm: " << sum << std::endl;
	abort();
      }
    }
    /* { this is equal to referred curvature
      const auto w0 = curvature().evaluate( 0 );
      const auto wN = curvature().evaluate( mesh().N()-1 );
      if( w0.norm() > 1.0e-10 or wN.norm() > 1.0e-10 ) {
	std::cerr << "curvature at ends: " << w0 << " " << wN << std::endl;
      }
      } */
    {
      const auto y0 = moment().evaluate( 0 );
      const auto yN = moment().evaluate( mesh().N()-1 );
      if( y0.norm() > 1.0e-10 or yN.norm() > 1.0e-10 ) {
	std::cerr << "moment at ends: " << y0 << " " << yN << std::endl;
      }
    }
#endif
  }

protected:
  const Mesh& mesh() const { return _mesh; }
  const Problem& problem() const {
    assert( _problem_ptr );
    return *_problem_ptr;
  }

  const PositionFunctionType& position() const {
    return _solutionTuple.function<PositionFunctionType>("position");
  }
  const CurvatureFunctionType& curvature() const {
    return _solutionTuple.function<CurvatureFunctionType>("curvature");
  }
  const MomentFunctionType& moment() const {
    return _solutionTuple.function<MomentFunctionType>("moment");
  }
  const PressureFunctionType& pressure() const {
    return _solutionTuple.function<PressureFunctionType>("pressure");
  }
  const PositionFunctionType& oldPosition() const {
    return _oldSolutionTuple.function<PositionFunctionType>("position");
  }
  const CurvatureFunctionType& oldCurvature() const {
    return _oldSolutionTuple.function<CurvatureFunctionType>("curvature");
  }
  PositionFunctionType& position() {
    return _solutionTuple.function<PositionFunctionType>("position");
  }
  CurvatureFunctionType& curvature() {
    return _solutionTuple.function<CurvatureFunctionType>("curvature");
  }
  MomentFunctionType& moment() {
    return _solutionTuple.function<MomentFunctionType>("moment");
  }

  double deltaT() const {
    return _time_provider.deltaT();
  }
  double time () const {
    return _time_provider.time();
  }

  /**
   *  set_initial_condition
   *
   *  Sets the initial conditions for the position and curvature.
   */
  void set_initial_condition() {
    untested();
    interpolate_initial_position();
    compute_curvature();
  }

  void interpolate_initial_position() {
    untested();
    // vertex loop
    for( auto&& vertex : mesh().vertices() ) {
      RangeVectorType Xuj = problem().X0( vertex );
      position().assign(vertex.index(), Xuj);
    }
  }

  void compute_curvature() {
    untested();
    const unsigned int N = mesh().N();

     // element loop
    double q_old = norm( position().eval(1) - position().eval(0) );
    auto tau_old = ( position().eval(1) - position().eval(0) ) / q_old;
    for( unsigned int e = 1; e < N-1; ++e ) {
      const double q = norm( position().eval(e+1) - position().eval(e) );
      const auto tau = ( position().eval(e+1) - position().eval(e) ) / q;

      const double Mi = 0.5 * ( q + q_old );
      const auto SXi = tau - tau_old;

      // TODO is there a sign error here?
      curvature().assign( e, -SXi / Mi );

      q_old = q;
      tau_old = tau;
    }
 }

  void setup_matrix_stencils() {
    // determine offsets
    const unsigned int x_offset = _solutionTuple.offset("position");
    const unsigned int y_offset = _solutionTuple.offset("moment");
    const unsigned int w_offset = _solutionTuple.offset("curvature");
    const unsigned int p_offset = _solutionTuple.offset("pressure");
    const unsigned int N = mesh().N();

    // create stencils
    _stiffstencil.init(w_offset, x_offset);
    _all_stencils.push_back( &_stiffstencil );
    _massstencil.init(y_offset, y_offset);
    _all_stencils.push_back( &_massstencil );
    _emassstencil.init(y_offset, w_offset);
    _all_stencils.push_back( &_emassstencil );
    _pmassstencil.init(y_offset, w_offset);
    _all_stencils.push_back( &_pmassstencil );
    _mass2stencil.init(w_offset, w_offset);
    _all_stencils.push_back( &_mass2stencil );
    _pstiffstencil.init(x_offset, y_offset);
    _all_stencils.push_back( &_pstiffstencil );
    _presstencil.init(x_offset, p_offset);
    _all_stencils.push_back( &_presstencil );
    _dragstencil.init(x_offset, x_offset);
    _all_stencils.push_back( &_dragstencil );
    _constraintstencil.init(p_offset, x_offset);
    _all_stencils.push_back( &_constraintstencil );

    // allocate memory
    std::for_each( _all_stencils.begin(), _all_stencils.end(),
		   [N]( stencil_base* s ){ s->alloc(N); } );

    // mark? TODO what does this do?
    SquareMatrixFootprint t( _solutionTuple.size() );
    std::for_each(_all_stencils.begin(), _all_stencils.end(),
		  [&t]( stencil_base* s ){ s->mark(t); });

    // TODO what does this do?
    implicitOperator_.init(t);

    // TODO what does this do?
    std::for_each( _all_stencils.begin(), _all_stencils.end(),
		   [this]( stencil_base* s ){ s->map( implicitOperator_ ); } );

    // TODO what does this do?
    implicitOperator_.assemble();

    return;
  }

  void load_matrix_stencils() {
    std::for_each( _all_stencils.begin(), _all_stencils.end(),
		   [this]( stencil_base* s ){ s->load( implicitOperator_ ); } );
  }

  /** load implementation **/

  struct ElementGeometry {
    using E = typename Mesh::Element;

    ElementGeometry( const PositionFunctionType& position,
		     const E& e  )
      : _position( position ), _e( e ), _q( -1.0 ) {
      update();
    }

    RangeVectorType left_position() const {
      const auto left = _e.left();
      return _position.evaluate( left.index() );
    }
    RangeVectorType right_position() const {
      const auto right = _e.right();
      return _position.evaluate( right.index() );
    }
    double q() const {
      assert( _q > 0 );
      return _q;
    }
    RangeVectorType tau() const {
      assert( std::abs(_tau*_tau - 1.0) < 1.0e-10 );
      return _tau;
    }

    void update() {
      _q = ( left_position() - right_position() ).norm();
      _tau = ( right_position() - left_position() ) / _q;
    }

  private:
    const PositionFunctionType& _position;
    const E _e;

    double _q;
    RangeVectorType _tau;
  };

  const ElementGeometry& geo( const typename Mesh::Element& e ) {
    return _geometry_cache.at( e.index() );
  }

  // stiffness matrix
  // [   |   |  ]
  // [---+---+--]
  // [\\\|   |  ]
  // [---+---+--]
  // [   |   |  ]
  void load_matrix_stiff() {
    _stiffstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      // set up geometry
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double stiff = 1.0 / geo(element).q();

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	unsigned l = k;

	if (l_ind == 0) { // first element
	} else {
	  _stiffstencil.add(l_ind, l_ind, k, l, stiff);
	  _stiffstencil.add(l_ind, r_ind, k, l, -stiff);
	}
	if (r_ind == mesh().N() - 1) { // last element
	} else {
	  _stiffstencil.add(r_ind, l_ind, k, l, -stiff);
	  _stiffstencil.add(r_ind, r_ind, k, l, stiff);
	}
      } // k
    } // e
  }

  // non symmetric terms
  void load_matrix_pres() {
    _presstencil.clear();
    _constraintstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned e_ind = element.index();
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      // set up geometry
      const auto tau = geo(element).tau();

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	double pres = tau[k];

	// add to matrix
	/*   dim*N     dim*N   N-1
	 * [         |        | H  ]
	 * [  ...    |....    | E  ]
	 * [         |        | R  ]
	 * [         |        | E  ]
	 * [ --------+--------+--- ]
	 * [ ...     | ...    |..  ]
	 * [ --------+--------+--- ]
	 * [ HERE    | ...    |..  ]
	 */
	_presstencil.add( l_ind, e_ind, k, 0, pres );
	_presstencil.add( r_ind, e_ind, k, 0, -pres );

	_constraintstencil.add( e_ind, l_ind, 0, k, pres );
	_constraintstencil.add( e_ind, r_ind, 0, k, -pres );
      } // k
    }   // e
  }

  // drag terms
  void load_matrix_drag() {
    _dragstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );

      // element geometry
      const double q = geo(element).q();
      const auto tau = geo(element).tau();

      // evaluate K
      const double K_left = problem().K( element.left() );
      const double K_right = problem().K( element.right() );

      // dim range double loop
      for( unsigned int k = 0; k < worlddim; ++k ) {
	for (unsigned int l = 0; l < worlddim; ++l) {
	  // const double D_lk = K * ( id[k==l] - tau[k]*tau[l] ) + tau[k]*tau[l];

	  const double id = (double)(k == l);
	  const double D_left = K_left * ( id - tau[k]*tau[l] ) + tau[k]*tau[l];
	  const double D_right = K_right * ( id - tau[k]*tau[l] ) + tau[k]*tau[l];

	  const double drag_left = 0.5 * q * D_left / deltaT();
          _dragstencil.add_(l_ind, l_ind, k, l, drag_left);
	  const double drag_right = 0.5 * q * D_right / deltaT();
	  _dragstencil.add_(r_ind, r_ind, k, l, drag_right);
	}
      }
    }
  }

  void load_matrix_mass() {
    _massstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double mass = 0.5 * geo(element).q();

      // dim range loop
      for( unsigned int k = 0; k < worlddim; ++k ) {
	// if( e > 0 or 1 ) {
	//   _massstencil.add_(e, e, k, k, mass);
	// }
	// if( e < mesh().N() - 2  or 1 ) {
	//   _massstencil.add_(e + 1, e + 1, k, k, mass);
	// }
	_massstencil.add_(l_ind, l_ind, k, k, mass);
	_massstencil.add_(r_ind, r_ind, k, k, mass);
      }
    }

    // // boundary conditions
    // for( unsigned int k = 0; k < worlddim; ++k ) {
    //   _massstencil.add_( 0, 0, k, k, 1.0 );
    // }
    // for( unsigned int k = 0; k < worlddim; ++k ) {
    //   _massstencil.add_( mesh().N()-1, mesh().N()-1, k, k, 1.0 );
    // } TODO this implementation will only be correct for 0 boundary data
  }

  void load_matrix_mass2() {
    _mass2stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double mass2 = 0.5 * geo(element).q();
      assert( mass2 == mass2 );
      assert( mass2 != 0 );

      // dim range loop
      for( unsigned int k = 0; k < worlddim; ++k ) {
	if( l_ind == 0 ) { // first element
	} else {
	  _mass2stencil.add_(l_ind, l_ind, k, k, mass2);
	}
	if( r_ind == mesh().N() - 1 ) { // last element
	} else {
	  _mass2stencil.add_(r_ind, r_ind, k, k, mass2);
	}
      }
    }

    // boundary conditions
    const unsigned back = mesh().N()-1;
    for( unsigned k = 0; k < worlddim; ++k ) {
      _mass2stencil.add_( 0, 0, k, k, 1.0 );
      _mass2stencil.add_( back, back, k, k, 1.0 );
    }
  }

  void load_matrix_emass() {
    _emassstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );

      // evaluate e
      const double e_left = problem().e( element.left() );
      const double e_right = problem().e( element.right() );

      const double emass_left = - e_left * 0.5 * geo(element).q();
      assert( emass_left == emass_left );
      assert( emass_left != 0 );
      const double emass_right = - e_right * 0.5 * geo(element).q();
      assert( emass_right == emass_right );
      assert( emass_right != 0 );

      // dim range loop
      for( unsigned int k = 0; k < worlddim; ++k ) {
	if( l_ind == 0 ) { // first element
	} else {
	  _emassstencil.add_(l_ind, l_ind, k, k, emass_left);
	}
	if( r_ind == mesh().N() -1 ) { // final element
	} else {
	  _emassstencil.add_(r_ind, r_ind, k, k, emass_right);
	}
      }
    }
  }

  void load_matrix_pmass() {
    _pmassstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double q = geo(element).q();
      const auto tau = geo(element).tau();

      // evaluate mu
      const double mu_left = problem().mu( element.left() );
      const double mu_right = problem().mu( element.right() );

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	for (unsigned int l = 0; l < worlddim; ++l) {
	  const double Ptau = ((double)(k == l) - tau[l] * tau[k]);
	  assert(Ptau == Ptau);

	  const double pmass_left = -mu_left * Ptau * 0.5 * q / deltaT();
	  const double pmass_right = -mu_right * Ptau * 0.5 * q / deltaT();

	  if (l_ind == 0) { // first element
	  } else {
	    _pmassstencil.add_(l_ind, l_ind, k, l, pmass_left);
	  }
	  if (r_ind == mesh().N() - 1) { // final element
	  } else {
	    _pmassstencil.add_(r_ind, r_ind, k, l, pmass_right);
	  }
	}
      }
    }
  }

  void load_matrix_pstiff() {
    _pstiffstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double q = geo(element).q();
      const auto tau = geo(element).tau();

      // dim range double loop
      for (unsigned k = 0; k < worlddim; ++k) {
	for (unsigned int l = 0; l < worlddim; ++l) {
	  const double Ptau = ((double)(k == l) - tau[l] * tau[k]);
	  assert(Ptau == Ptau);
	  const double pStiff = -Ptau/q;

	  const double pStiff_right = pStiff;
	  const double pStiff_left = pStiff;

	  // [ |\| ]
	  // [-+-+-]
	  // [ | | ]
	  // [-+-+-]
	  // [ | | ]
	  _pstiffstencil.add(l_ind, l_ind, k, l, pStiff_left);
	  _pstiffstencil.add(r_ind, r_ind, k, l, pStiff_right);

	  _pstiffstencil.add(r_ind, l_ind, k, l, -pStiff_left);
	  _pstiffstencil.add(l_ind, r_ind, k, l, -pStiff_right);
	}
      }
    }
  }

  // dynamic drag ( isn't this diagonal? )
  void load_dynamic_drag() {
    // _dragstencil.clear(); TODO should be a different stencil!

    // element loop
    for (auto &&element : mesh().elements()) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert((l_ind == element.index()) and (r_ind == element.index() + 1));
      const double q = geo(element).q();

      // evaluate curvature and energy density
      /* const RangeVectorType y_left = curvature().evaluate(l_ind); */
      /* const RangeVectorType y_right = curvature().evaluate(r_ind); */
      const double energy_density_left = 0; // TODO shouldn't be zero
      const double energy_density_right = 0; // TODO

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	for (unsigned int l = 0; l < worlddim; ++l) {
	  // [\\ |   |  ]
	  // [\\\|   |  ]
	  // [ \\|   |  ]
	  // [---+---+--]
	  // [   |   |  ]
	  // [---+---+--]
	  // [   |   |  ]
	  const double wStiff_left = energy_density_left / q;
	  const double wStiff_right = energy_density_right / q;
	    untested();
	    _dragstencil.add_(l_ind, l_ind, l, k, wStiff_left);
	    _dragstencil.add_(l_ind, r_ind, l, k, -wStiff_left);
	    _dragstencil.add_(r_ind, r_ind, l, k, wStiff_right);
	    _dragstencil.add_(r_ind, l_ind, l, k, -wStiff_right);
	} // l loop
      }   // k loop
    }     // e loop
  }

  void load_more_rhs() {
    // explicit terms
    auto& rhs_position
      = _rhsTuple.function<PositionFunctionType>("position");
    auto& rhs_moment
      = _rhsTuple.function<MomentFunctionType>("moment");
    _dragstencil.mv( rhs_position, oldPosition() );
    _pmassstencil.mv( rhs_moment, oldCurvature() );
  }

  void load_force_rhs() {
    auto& rhs_position
      = _rhsTuple.function<PositionFunctionType>("position");

    // element loop
     for( auto&& element : mesh().elements() ) {
       const double q = geo(element).q();

      // inner element loop (left or right)
      // for( auto&& vertex : element.innerVertices() ) {
       RangeVectorType f_old = problem().f( mesh().vertices().front() );
      { const auto& vertex = element.left();
	/* const auto x_ie = position().evaluate( vertex.index() ); */

	RangeVectorType value = 0.5 * q * f_old;
	rhs_position.add( vertex.index(), value );
      }
      { const auto& vertex = element.right();
	/* const auto x_ie = position().evaluate( vertex.index() ); */

	// evaluate force
	RangeVectorType f = problem().f( vertex ); // TODO: see problem::f();

	RangeVectorType value = 0.5 * q * f;
	rhs_position.add( vertex.index(), value );

	f_old = f;
      }
    }

    // TODO missing extra forces?
  }

  void load_moment_rhs() {
    auto& rhs_moment = _rhsTuple.function<MomentFunctionType>("moment");

    // vertex loop
    for( auto&& vertex : mesh().vertices() ) {
      const unsigned idx = vertex.index();
      if( idx == 0 or idx == _mesh.N()-1 ) {
	continue;
      }
      // left
      const auto p_l = oldPosition().evaluate( ( idx > 0 ) ? (idx-1) : idx );
      const auto p_c = oldPosition().evaluate( idx );
      const auto p_r = oldPosition().evaluate( ( idx < _mesh.N()-1 ) ? (idx+1) : idx  );

      const double q_l = ( p_c - p_l ).norm();
      const double q_r = ( p_r - p_c ).norm();

      RangeVectorType tau_l = ( p_c - p_l );
      // assert( tau_l.norm() > 0 ); -- not true at idx = 0
      tau_l /= std::max( tau_l.norm(), 1.0e-10 );
      RangeVectorType tau_r = ( p_r - p_c );
      // assert( tau_r.norm() > 0 ); -- not true at idx = N-1
      tau_r /= std::max( tau_r.norm(), 1.0e-10 );
      // find tau
      RangeVectorType tau = 0.5 * ( tau_l + tau_r );
      assert( tau.norm() > 0 );
      tau /= tau.norm();

      // NOTE there was an error here in old implementations which
      // used the wrong sign for nu
      // TODO decide which sign is the one to use from now on
      const RangeVectorType nu = tau.perp();

      // find beta
      const double beta = problem().beta( vertex );
      // evaluate e
      const double e = problem().e( vertex );

      RangeVectorType value = 0.5 * e * ( q_l + q_r ) * beta * nu;
      rhs_moment.add( idx, value );
    }
  }

  void load_curvature_rhs() {
    auto& rhs_curvature
      = _rhsTuple.function<CurvatureFunctionType>("curvature");

    {
      const unsigned front = 0;
      const unsigned idx = 0;

      const auto p_c = oldPosition().evaluate( front );
      const auto p_r = oldPosition().evaluate( front+1);

      // find tau
      RangeVectorType tau = ( p_r - p_c );
      assert( tau.norm() > 0 );
      tau /= tau.norm();

      // NOTE there was an error here in old implementations which
      // used the wrong sign for nu
      // TODO decide which sign is the one to use from now on
      const RangeVectorType nu = tau.perp();

      const auto beta = problem().beta( mesh().vertices().front() );
      const auto val = beta * nu;
      rhs_curvature.assign( front, val );
    }
    {
      const unsigned back = mesh().N()-1;
      const auto p_l = oldPosition().evaluate( back-1 );
      const auto p_c = oldPosition().evaluate( back );

      RangeVectorType tau = ( p_c - p_l );
      assert( tau.norm() > 0 );
      tau /= tau.norm();

      // NOTE there was an error here in old implementations which
      // used the wrong sign for nu
      // TODO decide which sign is the one to use from now on
      const RangeVectorType nu = tau.perp();

      const auto beta = problem().beta( mesh().vertices().back() );
      const auto val = beta * nu;
      rhs_curvature.assign( back, val );
    }
  }

  void load_pressure_rhs() {
    auto& rhs_pressure
      = _rhsTuple.function<PressureFunctionType>("pressure");
    const double gamma = problem().gamma();

    // element loop
     for( auto&& element : mesh().elements() ) {
      const unsigned e = element.index();
      rhs_pressure[e] = element.h() * -gamma;
    }
  }

  /** PROBES **/
  // TODO should these be public?
public:
  struct Centre_of_mass_probe : public Probe_otf<RangeVectorType> {
    using Base = Probe_otf<RangeVectorType>;
    Centre_of_mass_probe(const Viscoelastic_mechanics_entity &e)
	: Base("mechanics.centre_of_mass"), _e(e) {}

    virtual RangeVectorType evaluate() const {
      RangeVectorType ret(0.0);
      for (auto &&entity : _e.mesh().elements()) {
	const ElementGeometry geo(_e.position(), entity);
	const unsigned l_i = entity.left().index();
	const unsigned r_i = entity.right().index();
	const auto com =
	    0.5 * geo.q() * (_e.position().eval(r_i) + _e.position().eval(l_i));
	ret += com;
      }

      return ret;
    }

  private:
    const Viscoelastic_mechanics_entity &_e;
    friend class Viscoelastic_mechanics_entity;
  };

  struct Head_probe : public Probe_otf<RangeVectorType> {
    using Base = Probe_otf<RangeVectorType>;
    Head_probe(const Viscoelastic_mechanics_entity &e)
	: Base("mechanics.head"), _e(e) {}

    virtual RangeVectorType evaluate() const {
      return _e.position().eval(0);
    }

  private:
    const Viscoelastic_mechanics_entity &_e;
    friend class Viscoelastic_mechanics_entity;
  };

  struct Tail_probe : public Probe_otf<RangeVectorType> {
    using Base = Probe_otf<RangeVectorType>;
    Tail_probe(const Viscoelastic_mechanics_entity &e)
	: Base("tail"), _e(e) {}

    virtual RangeVectorType evaluate() const {
      const unsigned N = _e.mesh().N();
      return _e.position().eval(N-1);
    }

  private:
    const Viscoelastic_mechanics_entity &_e;
    friend class Viscoelastic_mechanics_entity;
  };

  struct Length_probe : public Probe_otf<double> {
    using Base = Probe_otf<double>;
    Length_probe(const Viscoelastic_mechanics_entity &e)
	: Base("mechanics.length"), _e(e) {}

    virtual double evaluate() const {
      double l = 0.0;
      for( auto && element : _e.mesh().elements() ) {
	const auto left_position = _e.position().eval( element.left().index() );
	const auto right_position = _e.position().eval( element.right().index() );

	l += ( right_position - left_position ).norm();
      }
      return l;
    }

  private:
    const Viscoelastic_mechanics_entity &_e;
    friend class Viscoelastic_mechanics_entity;
  };

  struct Kappa_probe : public Probe_otf< const ScalarCurvatureFunctionType& > {
    using T = const ScalarCurvatureFunctionType&;
    using Base = Probe_otf< T >;
    Kappa_probe( const Viscoelastic_mechanics_entity& e )
      : Base("mechanics.kappa"), _e(e),
	// scalar curature
	_kappa_storage( _e.mesh().N() ),
	_kappa( _e.mesh(), SubArray<Vector>(_kappa_storage ) ) {}

    T evaluate() const {
      _kappa_storage.clear();
      const auto& w = _e.curvature();

      for( auto&& vertex : _e.mesh().vertices() ) {
      	const unsigned idx = vertex.index();
	const auto p_l = _e.position().evaluate((idx > 0) ? (idx - 1) : idx);
	const auto p_c = _e.position().evaluate(idx);
	const auto p_r =
	  _e.position().evaluate((idx < _e.mesh().N() - 1) ? (idx + 1) : idx);

	RangeVectorType tau_l = (p_c - p_l);
	// assert( tau_l.norm() > 0 ); -- not true at idx = 0
	tau_l /= std::max(tau_l.norm(), 1.0e-10);
	RangeVectorType tau_r = (p_r - p_c);
	// assert( tau_r.norm() > 0 ); -- not true at idx = N-1
	tau_r /= std::max(tau_r.norm(), 1.0e-10);
	// find tau
	RangeVectorType tau = 0.5 * (tau_l + tau_r);
	assert(tau.norm() > 0);
	tau /= tau.norm();

	// TODO decide on sign convention
	// this matches old implementation but not paper :(
	const auto nu = - tau.perp();

	const auto w_idx = w.evaluate( idx );
	const double kappa = nu * w_idx;

	_kappa.add_scalar( idx, kappa );
      }

      return _kappa;
    }
  private:
    const Viscoelastic_mechanics_entity& _e;
    friend class Viscoelastic_mechanics_entity;
    // extra storage for scalar curvature
    mutable Vector _kappa_storage;
    mutable ScalarCurvatureFunctionType _kappa;
  };

  struct Energy_probe : public Probe_otf<double> {
    using Base = Probe_otf<double>;

    Energy_probe( const Viscoelastic_mechanics_entity& e )
      : Base("mechanics.energy"), _e(e)
      {}

    virtual double evaluate() const {
      const auto elastic_force_probe_ptr = std::dynamic_pointer_cast< const Elastic_force_probe >( _e.get_probe( "mechanics.elasticforce" ) );
      const auto viscous_force_probe_ptr = std::dynamic_pointer_cast< const Viscous_force_probe >( _e.get_probe( "mechanics.viscousforce" ) );
      return elastic_force_probe_ptr->evaluate() + viscous_force_probe_ptr->evaluate();
    }

  private:
    const Viscoelastic_mechanics_entity& _e;
    friend class Viscoelastic_mechanics_entity;
  };

  struct Elastic_force_probe : public Probe_otf<double > {
    using Base = Probe_otf< double >;
    Elastic_force_probe( const Viscoelastic_mechanics_entity& e)
      : Base("mechanics.elasticforce"), _e(e)
	 {}

    virtual double evaluate() const {
      const auto kappa_probe_ptr = std::dynamic_pointer_cast< const Kappa_probe >( _e.get_probe( "mechanics.kappa" ) );
      const auto& kappa = kappa_probe_ptr->evaluate();
      double ret = 0.0;

      for( auto&& element : _e.mesh().elements() ) {

	const auto p_l = _e.position().eval( element.left().index() );
	const auto p_r = _e.position().eval( element.right().index() );
	const auto q = (p_r - p_l).norm();

	double e_l = _e.problem().e(element.left());
	double e_r = _e.problem().e(element.right());
	double beta_l = _e.problem().beta(element.left());
	double beta_r = _e.problem().beta(element.right());
	double kappa_l = kappa.evaluate( element.left().index() );
	double kappa_r = kappa.evaluate( element.right().index() );

	ret += 0.5 * q * ( e_l*(kappa_l - beta_l)*(kappa_l - beta_l) +  e_r*(kappa_r - beta_r)*(kappa_r - beta_r) );

      }

      return ret;
    }

  private:
    const Viscoelastic_mechanics_entity& _e;
    friend class Viscoelastic_mechanics_entity;
 
  };



  struct Viscous_force_probe : public Probe_otf<double > {
    using T = const ScalarCurvatureFunctionType&;
    using Base = Probe_otf< double >;
    Viscous_force_probe( const Viscoelastic_mechanics_entity& e)
      : Base("mechanics.viscousforce"), _e(e)
         {}

    virtual double evaluate() const {
      double ret = 0.0;

      for( auto&& element : _e.mesh().elements() ) {
        const unsigned l_ind = element.left().index();
        const unsigned r_ind = element.right().index();
        assert((l_ind == element.index()) and (r_ind == element.index() + 1));

        const auto p_l = _e.position().eval( element.left().index() );
        const auto p_r = _e.position().eval( element.right().index() );
        const auto q = (p_r - p_l).norm();
        RangeVectorType tau = (p_r - p_l)/q;

        double mu_l = _e.problem().mu(  element.left()  );
        double mu_r = _e.problem().mu(  element.right()  );

        // get current and old curvatures
        const RangeVectorType w_l = _e.curvature().evaluate( l_ind );
        const RangeVectorType w_r =  _e.curvature().evaluate( r_ind );
        const RangeVectorType w_l_old = _e.oldCurvature().evaluate( l_ind );
        const RangeVectorType w_r_old = _e.oldCurvature().evaluate( r_ind );
        const RangeVectorType dwdt_l = ( w_l - w_l_old )/_e.deltaT();
        const RangeVectorType dwdt_r = ( w_r - w_r_old )/_e.deltaT();
        // Calculate Pw_t components ([2x1] vector)
        double Pwt1_l =   dwdt_l[0] * (1 - tau[0]*tau[1]) - dwdt_l[1]*tau[0]*tau[1] ;
        double Pwt2_l = - dwdt_l[0] * tau[1]*tau[0]       + dwdt_l[1]*(1 - tau[1]*tau[0]);
        double Pwt1_r =   dwdt_r[0] * (1 - tau[0]*tau[1]) - dwdt_r[1]*tau[0]*tau[1] ;
        double Pwt2_r = - dwdt_r[0] * tau[1]*tau[0]       + dwdt_r[1]*(1 - tau[1]*tau[0]);

        ret += 0.5 * q * (  mu_l*( Pwt1_l*Pwt1_l + Pwt2_l*Pwt2_l )   +   mu_r*( Pwt1_r*Pwt1_r + Pwt2_r*Pwt2_r ) );

      }

      return ret;
    }


  private:
    const Viscoelastic_mechanics_entity& _e;
    friend class Viscoelastic_mechanics_entity;

  };


  using Position_probe = Probe_ptr< const PositionFunctionType >;
  using Moment_probe = Probe_ptr< const MomentFunctionType >;
  using Curvature_probe = Probe_ptr< const CurvatureFunctionType >;
  using Pressure_probe = Probe_ptr< const PressureFunctionType >;

  virtual std::vector< std::string > optional_ports() const {
    // TODO how to add force in here too?
    return { "control.beta" };
  }

private:
  const Mesh& _mesh;
  const TimeProvider& _time_provider;
  std::shared_ptr<Problem> _problem_ptr;

  // solution variables
  SolutionTupleType _solutionTuple;
  SolutionTupleType _oldSolutionTuple;
  SolutionTupleType _rhsTuple;

  // sub matrix blocks (stencils)
  stencil_dim_3diag<worlddim> _dragstencil;
  stencil_dim_diag<worlddim> _massstencil;
  stencil_dim_diag<worlddim> _mass2stencil;
  stencil_grad< worlddim, 1 > _presstencil;
  stencil_dim_diag<worlddim> _emassstencil;
  stencil_dim_diag<worlddim> _pmassstencil;
  stencil_dim_3diag<worlddim> _stiffstencil;
  stencil_dim_3diag<worlddim> _pstiffstencil;
  stencil_div< 1, worlddim > _constraintstencil;
  std::vector< stencil_base* > _all_stencils;

  std::vector< ElementGeometry > _geometry_cache;

  // full matrix
  SystemOperatorType implicitOperator_;
};
