#pragma once

#include "../entity.hh"
#include "../../vector.hh"
#include "../../mesh.hh"
#include "../../probe/probe.hh"
#include "../../discretefunction.hh"
#include "../../timeprovider.hh"
#include "../../parameter.hh"

/**
 *  TODO doc me!
 *
 *  could be at a vertex instead of u
 *  and through time provider instead of t?
 */
template< unsigned int worlddim >
struct Mechanics_problem {
  using RangeVectorType = RangeVector<worlddim>;
  using Vertex = typename Mesh::Vertex;

  using BetaFunction = PiecewiseLinearFunction<1>;
  using BetaProbe = Probe_ptr< const BetaFunction >;

  using ForceFunction = PiecewiseLinearFunction<worlddim>;
  using ForceProbe = Probe_ptr< const ForceFunction >;

  Mechanics_problem( const TimeProvider& time_provider )
    : _time_provider( time_provider ),
      _K( Parameters::get<double>("mechanics.problem.K") ),
      _e( Parameters::get<double>("mechanics.problem.e") ),
      _e_cut( Parameters::get<double>("mechanics.problem.e_cut") ),
      _I2_eps( Parameters::get<double>("mechanics.problem.I2_eps") ),
      _mu( Parameters::get<double>("mechanics.problem.mu") ),
	// clamp
      _clamp_t0( Parameters::get<double>("control.feedback.clamp_t0" ) ),
      _clamp_t1( Parameters::get<double>("control.feedback.clamp_t1" ) ),
      _phys_clampA( Parameters::get<double>("mechanics.physical_clampA" ) ),
      _phys_clampB( Parameters::get<double>("mechanics.physical_clampB" ) ),
      _beta_grad( Parameters::get<double>("mechanics.problem.beta_grad") ),
      _gamma( Parameters::get<double>("mechanics.problem.gamma") ),
	      _beta_amplitude( Parameters::get<double>("control.beta_amplitude") ),
	      _initial_posture( Parameters::get<std::string>("mechanics.problem.initial_posture") ),
	      _constant_radius( Parameters::get<bool>("mechanics.problem.constant_radius" ) )
	    {}

	  // ratio of drag coefficients
	  virtual double K( const Vertex& v ) const {
	    // clamping
    	    if( v.u() > _phys_clampA and v.u() < ( _phys_clampA + _phys_clampB) and  _clamp_t0 < _time_provider.time() and _time_provider.time() < _clamp_t1 ){
	 	  return 1000000.0;
	    } else {
		  return _K;
	    }

	  }

	#ifdef FLAT_WORM
	  // flat worm tests require access to underlying K
	  double K() const {
	    return _K;
	  }
	#endif

	  double I2(const Vertex &v) const {
	    if (_constant_radius) {
	      return 1;
	    } else {
	      /**
	       *  ret = 2.0^3 sqrt( (eps+u) * ( eps+1-u ) )^3 / ( 1 + 2 eps )^3
	       *   \approx 2.0^2 sqrt( u * (1-u) )^3
	       */
	      const double eps = _I2_eps;
	      const double num = 2.0 * std::sqrt((eps + v.u()) * (eps + 1.0 - v.u()));
	      const double den = 1.0 + 2.0 * eps;

	      return (num * num * num) / (den * den * den);
	    }
	  }


	  // elastic coefficient
	  // term is dependent on constant_radius parameter
	   virtual double e( const Vertex& v ) const {
		 return _e * I2( v ) + _e_cut * I2( v );
	   }

	  // viscocity coefficient
	  virtual double mu( const Vertex& v ) const {
	    return _mu * I2( v );
	  }
	  // length coefficient
	  virtual double gamma() const {
	    return _gamma;
	  }

	  // initial condition
	  virtual RangeVectorType X0( const Vertex& v ) const {
	    RangeVectorType ret(0.0);	

	    if(_initial_posture == "line"){
		ret.at(0) = v.u() * gamma();
	    }else if( _initial_posture == "curve" ){
		ret.at(0) = gamma()*cos( M_PI*v.u() )/M_PI;
		ret.at(1) = gamma()*sin( M_PI*v.u() )/M_PI;
	    }

	    return ret;
	  }
	  // body forcing // TODO is this too limited?
	  virtual RangeVectorType f( const Vertex& v ) const {
	    if( _force_probe ) {
	      return force().evaluate( v.index() );
	    }
	    return RangeVectorType(0.0);
	  }


  // curvature forcing
  virtual double beta( const Vertex& v ) const {
    if( _beta_probe ) {
      const auto ret =  ( 1.0 - _beta_grad * v.u() ) * _beta_amplitude * _e * I2( v )  * beta().evaluate( v.index() ) / ( _e * I2( v ) + _e_cut * I2( v ) );
      return ret.at(0); // TODO this should be an implicit conversion
    } else {
      return 0.0;
    }
  }


  virtual void connect_probes( const Entity_base& e ) {
     // use probes
    auto beta_probe_ptr = e.see_probe_ptr< BetaProbe >( "control.beta" );
    if( beta_probe_ptr ) {
      _beta_probe = beta_probe_ptr;
    } else {
      std::cerr << "WARNING: unable to connect beta probe to problem" << std::endl;
    }

    // TODO this may have to change if more than one force e.g?
    auto force_probe_ptr = e.see_probe_ptr< ForceProbe >( "force" );
    if( force_probe_ptr ) {
      _force_probe = force_probe_ptr;
    } else {
      std::cerr << "WARNING: unable to connect force probe to problem" << std::endl;
    }
  }

protected:
  const BetaFunction& beta() const {
    assert( _beta_probe );
    return _beta_probe->evaluate();
  }
  const ForceFunction& force() const {
    assert( _force_probe );
    return _force_probe->evaluate();
  }
private:
  const TimeProvider& _time_provider;
  std::shared_ptr< const BetaProbe > _beta_probe;
  std::shared_ptr< const ForceProbe > _force_probe;

  const double _K;
  const double _e;
  const double _e_cut;
  const double _I2_eps;
  const double _mu;
  // clamping
  const double _clamp_t0;
  const double _clamp_t1;
  const double _phys_clampA;
  const double _phys_clampB; 
  const double _beta_grad;
  const double _gamma;
  const double _beta_amplitude;
  const std::string _initial_posture;
  const bool _constant_radius;
};

/**
 *  TODO doc me!
 *
 *  could be at a vertex instead of u
 *  and through time provider instead of t?
 */
template< unsigned int worlddim >
struct Mechanics_dynamic_elasticity_problem  : public Mechanics_problem< worlddim > {
private:
  using Base_type = Mechanics_problem< worlddim >;
  using Vertex = typename Base_type::Vertex;
public:
  using KappaFunction = PiecewiseLinearFunction<1>;
  using KappaProbe = Probe_otf< const KappaFunction& >;

  Mechanics_dynamic_elasticity_problem( const TimeProvider& time_provider )
    : Base_type ( time_provider ),
      _e_max( Parameters::get<double>("mechanics.problem.e_max") ),
      _muscle_tension( Parameters::get<double>("mechanics.problem.muscle_tension") ),
      _dyn_function( Parameters::get<std::string>("mechanics.problem.dyn_function") )
  {}

  // elastic coefficient
  // term is dependent on constant_radius parameter
    virtual double e( const Vertex& v ) const override {
      const double eI2 = Base_type::e( v );
      const double kappamax = 10.0; // TODO this is used?
        const double muscle_tension = _muscle_tension;

    if(_dyn_function == "4thpower"){
        const double term = muscle_tension * kappa().evaluate( v.index() ) /10.0;  // beta( v )
        return eI2 * ( 1.0 + pow(term,4) );
    }else if(_dyn_function == "4thpower_negative") {
	const double term = muscle_tension * kappa().evaluate( v.index() ) /10.0;
    	return  _e_max * I2( v ) * ( 1.0 - pow(term,4) ); // TODO why switch to _e_max here?
    }else{
        return eI2 * ( 1.0 + muscle_tension*std::abs( kappa().evaluate( v.index() ) )/10.0 ); // <<< dynamic elasticity (should be kappa, not beta) 
    }
 }

  virtual void connect_probes( const Entity_base& e ) override {
    Base_type::connect_probes( e );

    // connect kappa probe
    auto kappa_probe_ptr = e.see_probe_ptr< KappaProbe >( "mechanics.kappa" );
    assert( kappa_probe_ptr );
    _kappa_probe = kappa_probe_ptr;
  }

protected:
  using Base_type::I2;
  using Base_type::beta;
  const KappaFunction& kappa() const {
    assert( _kappa_probe );
    return _kappa_probe->evaluate();
  }
private:
  std::shared_ptr< const KappaProbe > _kappa_probe;

  const double _e_max;
  const double _muscle_tension;
  const std::string _dyn_function;
};

/**
 *  TODO doc me!
 */
template< unsigned int worlddim >
struct Mechanics_problem_3d {
  using RangeVectorType = RangeVector<worlddim>;
  using Vertex = typename Mesh::Vertex;
  using Element = typename Mesh::Element;

  using BetaFunction = PiecewiseLinearFunction<2>;
  using BetaProbe = Probe_ptr< const BetaFunction >;

  using Gamma0Function = PiecewiseConstantFunction<1>;
  using Gamma0Probe = Probe_ptr< const Gamma0Function >;

  using ForceFunction = PiecewiseLinearFunction<worlddim>;
  using ForceProbe = Probe_ptr< const ForceFunction >;

  Mechanics_problem_3d( const TimeProvider& time_provider )
    : _time_provider( time_provider ),
      _K( Parameters::get<double>("mechanics.problem.K") ),
      _K_rot( Parameters::get<double>("mechanics.problem.K_rot") ),
      _I2_eps( Parameters::get<double>("mechanics.problem.I2_eps") ),
      _e( Parameters::get<double>("mechanics.problem.e") ),
      _g( Parameters::get<double>("mechanics.problem.g") ),
      _mu( Parameters::get<double>("mechanics.problem.mu") ),
      _zeta( Parameters::get<double>("mechanics.problem.zeta") ),
      _gamma( Parameters::get<double>("mechanics.problem.gamma") ),
      _initial_posture( Parameters::get<std::string>("mechanics.problem.initial_posture") ),
      _rotation_cutoff( Parameters::get<double>("mechanics.problem.rotation_cutoff") ),
      _frame_update( Parameters::getString("mechanics.problem.frame_update") ),
      _constant_radius( Parameters::get<bool>("mechanics.problem.constant_radius" ) )
  {}

  // ratio of drag coefficients
  virtual double K( const Vertex& /*v*/ ) const {
    return _K;
  }
  virtual double K_rot( const Vertex& /*v*/) const {
    return _K_rot;
  }

#ifdef FLAT_WORM
  // flat worm tests require access to underlying K
  double K() const {
    return _K;
  }
  double g() const {
    return _g;
  }
  double zeta() const {
    return _zeta;
  }
#endif
  double I2(const Vertex &v) const {
    if (_constant_radius) {
      return 1;
    } else {
      /**
       *  ret = 2.0^3 sqrt( (eps+u) * ( eps+1-u ) )^3 / ( 1 + 2 eps )^3
       *   \approx 2.0^2 sqrt( u * (1-u) )^3
       */
      const double eps = _I2_eps;
      const double num = 2.0 * std::sqrt((eps + v.u()) * (eps + 1.0 - v.u()));
      const double den = 1.0 + 2.0 * eps;

      return (num * num * num) / (den * den * den);
    }
  }

  // elastic bending coefficient
  virtual double e( const Vertex& v ) const {
    return _e * I2( v );
  }
  // elastic twisting coefficient
  virtual double g( const Vertex& v ) const {
    return _g * I2( v );
  }
  // bending viscocity coefficient
  virtual double mu( const Vertex& v ) const {
    return _mu * I2( v );
  }
  // bending viscocity coefficient
  virtual double zeta( const Vertex& v ) const {
    return _zeta * I2( v );
  }
// length coefficient
  virtual double gamma() const {
    return _gamma;
  }

  // initial position
  virtual RangeVectorType X0( const Vertex& v ) const {
    RangeVectorType ret(0.0);
    if(_initial_posture == "line"){
    ret.at(0) = v.u() * gamma();
    }else if( _initial_posture == "curve" ){
	ret.at(0) = gamma()*cos( M_PI*v.u() )/M_PI;
	ret.at(1) = gamma()*sin( M_PI*v.u() )/M_PI;
    }

    return ret;
  }
  // initial twist
  virtual RangeVector<1> gamma_initial( const Element& /*e*/ ) const {
    RangeVector<1> ret(0.0);
    ret.at(0) = 0.0;
    return ret;
  }
  virtual RangeVector<3> initial_frame1( const Vertex& /*v*/ ) const {
    RangeVector<3> ret(0.0);
    ret.at(1) = 1.0;
    return ret;
  }

  
  // body forcing // TODO is this too limited?
  virtual RangeVectorType f( const Vertex& v ) const {
    if( _force_probe ) {
      return force().evaluate( v.index() );
    }
    return RangeVectorType(0.0);
  }


  // curvature forcing
  virtual RangeVector<2> beta( const Vertex& v ) const {
    if( _beta_probe ) {
      return beta().evaluate( v.index() );
    } else {
      return RangeVector<2>(0.0);
    }
  }
  // twist forcing
  virtual RangeVector<1> gamma_forcing( const Element& e ) const {
    if( _gamma0_probe ) {
      return gamma0().evaluate( e.index() );
    }
    RangeVector<1> ret(0.0);
    ret.at(0) = 0.0;
    return ret;
  }

  RangeVectorType frame_update( const RangeVectorType& old,
				const RangeVectorType& tau_old,
				const RangeVectorType& tau,
				const double m,
				const double deltaT ) const {

    const auto angle_axis_rotation =
      []( const RangeVectorType& old, const RangeVectorType& k, const double theta ) {
	return old * cos( theta ) + cross_product( k, old ) * sin( theta )
	  + ( 1 - cos( theta ) ) * ( old * k ) * k;
      };

    if( _frame_update == "single_rotation" ) {
      const auto tau_t = ( tau - tau_old ) / deltaT;
      const auto omega = cross_product( tau_old, tau_t ) + m * tau_old;
      const double theta = deltaT * omega.norm();
      const auto k = omega / std::max( omega.norm(), _rotation_cutoff );
      return angle_axis_rotation( old, k, theta );
    } else if ( _frame_update == "double_rotation" ) {
      auto k_tau = cross_product( tau_old, tau );
      const double cos_theta_tau = tau_old * tau;
      const auto k_m = tau;
      const double theta_m = m * deltaT;

      const auto tmp = old * cos_theta_tau + cross_product(k_tau, old) +
	      (old * k_tau) / (1 + cos_theta_tau) * k_tau;
      return angle_axis_rotation( tmp, k_m, theta_m );
    } else {
      throw exception_nomatch( _frame_update + " is unknown mechanics.entity" );
    }
  }

  void connect_probes( const Entity_base& entity ) {
    auto beta_probe_ptr = entity.see_probe_ptr< BetaProbe >( "control.beta" );
    if( beta_probe_ptr ) {
      _beta_probe =  beta_probe_ptr;
    } else {
      std::cerr << "WARNING: unable to connect beta probe to problem" << std::endl;
    }

    auto gamma0_probe_ptr = entity.see_probe_ptr< Gamma0Probe >( "control.gamma0" );
    if( gamma0_probe_ptr ) {
      _gamma0_probe = gamma0_probe_ptr;
    } else {
      std::cerr << "WARNING: unable to connect gamma0 probe to problem" << std::endl;
    }

    // TODO this may have to change if more than one force e.g?
    auto force_probe_ptr = entity.see_probe_ptr< ForceProbe >( "force" );
    if( force_probe_ptr ) {
      _force_probe = force_probe_ptr ;
    } else {
      std::cerr << "WARNING: unable to connect force probe to problem" << std::endl;
    }
  }

protected:
  const BetaFunction& beta() const {
    assert( _beta_probe );
    return _beta_probe->evaluate();
  }
  const Gamma0Function& gamma0() const {
    assert( _gamma0_probe );
    return _gamma0_probe->evaluate();
  }
  const ForceFunction& force() const {
    assert( _force_probe );
    return _force_probe->evaluate();
  }
private:
  const TimeProvider& _time_provider;
  std::shared_ptr< const BetaProbe > _beta_probe;
  std::shared_ptr< const Gamma0Probe > _gamma0_probe;
  std::shared_ptr< const ForceProbe > _force_probe;

  const double _K;
  const double _K_rot;
  const double _I2_eps;
  const double _e;
  const double _g;
  const double _mu;
  const double _zeta;
  const double _gamma;
  const bool _constant_radius;
  const std::string _initial_posture;

  const double _rotation_cutoff;
  const std::string _frame_update;
};
