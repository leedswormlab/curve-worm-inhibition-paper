#pragma once
#include <algorithm>
#include <functional>

#include "../entity.hh"
#include "problem.hh"

#include "../../mesh.hh"
#include "../../discretefunction.hh"
#include "../../matrix.hh"
#include "../../solver.hh"
#include "../../stencil.hh"
#include "../../timeprovider.hh"
#include "../../matrix.hh"
#include "../../fmatrix.hh"

#ifndef WORLDDIM
#define WORLDDIM 3
#endif

#if WORLDDIM == 3

/**
 *  Bending_twisting mechanics entity
 *
 *  TODO doc me
 *
 *  TODO make other bc's in other entities match
 *  TODO add more matrix tests
 */
class Bending_twisting_mechanics_entity : public Entity_base {
  static const unsigned worlddim = WORLDDIM;
  using Problem = Mechanics_problem_3d<worlddim>;

  using SolutionTupleType = NamedFunctionTuple< worlddim >;
  using PositionFunctionType = typename SolutionTupleType::Discrete_function_type< worlddim, 1 >;
  using BendingMomentFunctionType = typename SolutionTupleType::Discrete_function_type< worlddim, 1 >;
  using CurvatureFunctionType = typename SolutionTupleType::Discrete_function_type< worlddim, 1 >;
  using PressureFunctionType = typename SolutionTupleType::Discrete_function_type< 1, 0 >;
  using TangentAngularVelocityFunctionType = typename SolutionTupleType::Discrete_function_type<1, 1>;
  using TwistFunctionType = typename SolutionTupleType::Discrete_function_type<1, 0>;
  using TwistingMomentFunctionType = typename SolutionTupleType::Discrete_function_type<1, 0>;

  // the frame functions are piecewise linear vector valued
  using FrameFunctionType = typename SolutionTupleType :: Discrete_function_type< worlddim, 1 >;

  using RangeVectorType = typename PositionFunctionType :: RangeVectorType;

  typedef UmfpackLU<double, int> SolverType;
  using SystemOperatorType = SystemMatrix< PositionFunctionType, SolverType >;

public:
  // named entity constructor
  Bending_twisting_mechanics_entity(std::string const &label, const Mesh& mesh, const TimeProvider& time_provider )
    : Entity_base(label),
      _mesh( mesh ),
      _time_provider( time_provider ),
      _problem_ptr(nullptr),
      // solution variables
      _solutionTuple( mesh ),
      _oldSolutionTuple( mesh ),
      _rhsTuple( mesh ),
      // frame variables
      _frame_0_data( worlddim*_mesh.N() ),
      _frame_0( mesh, SubArray<Vector>( _frame_0_data ) ),
      _frame_1_data( worlddim*_mesh.N() ),
      _frame_1( mesh, SubArray<Vector>( _frame_1_data ) ),
      _frame_2_data( worlddim*_mesh.N() ),
      _frame_2( mesh, SubArray<Vector>( _frame_2_data ) ),
      // matrix components
      _dragstencil( "drag matrix" ),
      _presstencil( "pressure matrix" ),
      _pstiffstencil( "proj stiffness matrix" ),
      _frame_twist_stencil( "frame twist matrix" ),
      _massstencil( "mass matrix" ),
      _emassstencil( "e mass matrix" ),
      _pmassstencil( "visco mass matrix" ),
      _pmass2stencil( "visco2 mass matrix" ),
      _mass2stencil( "mass2 matrix" ),
      _stiffstencil( "stiffness matrix" ),
      _rot_drag_stencil( "rot drag matrix" ),
      _rot_stiff_stencil( "rot stiff matrix" ),
      _dgamma_stencil( "dgamma matrix" ),
      _alpha_dt_stencil( "alpha mass/dt matrix" ),
      _frame_twist_T_stencil( "frame twist_T matrix" ),
      _zmassstencil("z mass stencil"),
      _za_massstencil("z gamma mass stencil"),
      _zat_massstencil("z gammat mass stencil"),
      _constraint_stencil( "constraint matrix" ) {
    // problem chooser
    {
      _problem_ptr.reset( new Mechanics_problem_3d<worlddim>(time_provider) );
    }
    assert( _problem_ptr );

    // discrete functions
    // x - position
    _solutionTuple.add_function< worlddim, 1 >( "position" );
    _oldSolutionTuple.add_function< worlddim, 1 >( "position" );
    _rhsTuple.add_function< worlddim, 1 >( "position" );
    // y - bending moment
    _solutionTuple.add_function< worlddim, 1 >( "bending-moment" );
    _oldSolutionTuple.add_function< worlddim, 1 >( "bending-moment" );
    _rhsTuple.add_function< worlddim, 1 >( "bending-moment" );
    // w - curvature
    _solutionTuple.add_function< worlddim, 1 >( "curvature" );
    _oldSolutionTuple.add_function< worlddim, 1 >( "curvature" );
    _rhsTuple.add_function< worlddim, 1 >( "curvature" );
    // p - pressure/line tension
    _solutionTuple.add_function< 1, 0 >( "pressure" );
    _oldSolutionTuple.add_function< 1, 0 >( "pressure" );
    _rhsTuple.add_function< 1, 0 >( "pressure" );
    // m - tangential component of angular velocity of frame
    _solutionTuple.add_function< 1, 1 >( "tau-angular-velocity" );
    _oldSolutionTuple.add_function< 1, 1 >( "tau-angular-velocity" );
    _rhsTuple.add_function< 1, 1 >( "tau-angular-velocity" );
    // z - twisting moment
    _solutionTuple.add_function< 1, 0 >( "twisting-moment" );
    _oldSolutionTuple.add_function< 1, 0 >( "twisting-moment" );
    _rhsTuple.add_function< 1, 0 >( "twisting-moment" );
    // gamma - twist
    _solutionTuple.add_function< 1, 0 >( "twist" );
    _oldSolutionTuple.add_function< 1, 0 >( "twist" );
    _rhsTuple.add_function< 1, 0 >( "twist" );

    // probes
    provide_probe( std::make_shared< const Centre_of_mass_probe >( *this ) );
    provide_probe( std::make_shared< const Head_probe >( *this ) );
    provide_probe( std::make_shared< const Tail_probe >( *this ) );
    provide_probe( std::make_shared< const Length_probe >( *this ) );
    provide_probe( std::make_shared< const Energy_probe >( *this ) );
    provide_probe( std::make_shared< const Position_probe >( "mechanics.position", &position() ) );
    provide_probe( std::make_shared< const Bending_moment_probe >( "mechanics.bending_moment", &moment() ) );
    provide_probe( std::make_shared< const Curvature_probe >( "mechanics.curvature", &curvature() ) );
    provide_probe( std::make_shared< const Pressure_probe >( "mechanics.pressure", &pressure() ) );
    provide_probe( std::make_shared< const Tangent_angular_velocity_probe >( "mechanics.tangent_angular_velocity", &angular_velocity() ) );
    provide_probe( std::make_shared< const Twist_probe >( "mechanics.twist", &twist() ) );
    provide_probe( std::make_shared< const Twisting_moment_probe >( "mechanics.twisting_moment", &twist_moment() ) );
    provide_probe( std::make_shared< const Frame_probe >( "mechanics.frame0", &frame(0) ) );
    provide_probe( std::make_shared< const Frame_probe >( "mechanics.frame1", &frame(1) ) );
    provide_probe( std::make_shared< const Frame_probe > ( "mechanics.frame2", &frame(2) ) );
    provide_probe( std::make_shared< const Curvature_component_probe >( *this, 1 ) );
    provide_probe( std::make_shared< const Curvature_component_probe >( *this, 2 ) );
    provide_probe( std::make_shared< const Darboux_probe >( *this ) );
    provide_probe( std::make_shared< const Frame_mismatch_probe >( *this ) );
    provide_probe( std::make_shared< const Darboux_error_probe >( *this ) );
  }

  // called at the start of the simulation before any others
  virtual void load() {
    // use probes
    _problem_ptr->connect_probes( *this );

    // set up geometry
    for( auto&& element : mesh().elements() ) {
      _geometry_cache.push_back( ElementGeometry( oldPosition(), element ) );
    }

    // set up stencil layout
    setup_matrix_stencils();
  }
  // called once per simulation before the time loop
  virtual void tr_begin() { untested();
    set_initial_condition();
    // set old solution from new solution
    _oldSolutionTuple.assign( _solutionTuple );
    // update geometry
    for( auto& geo : _geometry_cache ) {
      geo.update();
    }
  }
  // called once per time step at the start of the time step
  virtual void tr_advance() { untested();
    // set old solution from new solution
    _oldSolutionTuple.assign( _solutionTuple );
  }
  // called once per solver iteration - does the assembly
  virtual void tr_load() {
    untested();

    implicitOperator_.clear();
    _rhsTuple.clear();

    // update geometry
    for( auto& geo : _geometry_cache ) {
      geo.update();
    }

    // x equation
    /**
     * D x_t + ( p tau )_u / | x_u | + ( P y_u / | x_u | )_u / | x_u |
     *   - ( z tau times w )_u / | x_u | = f
     */
    load_matrix_drag();
    load_matrix_pres(); // also does constraint equation
    load_matrix_pstiff();
    load_matrix_frame_twist();
    load_dynamic_drag();

    // y equation
    /**
     *  y = e ( w - beta ) + eta ( P w_t - m tau times w )
     */
    load_matrix_mass();
    load_matrix_emass();
    load_matrix_pmass();
    load_matrix_pmass2();

    // w equation
    /**
     *  w = tau_u / | x_u |
     */
    load_matrix_mass2();
    load_matrix_stiff();

    // m equation
    /**
     *  K_rot m + z_u / | x_u | + y dot (tau cross w) = 0
     */
    load_matrix_rot_drag();
    load_matrix_rot_stiff();

    // gamma equation
    /**
     *  m_u / | x_u | - gamma_t + x_ut dot ( tau times w ) / | x_u | = 0
     */
    load_matrix_dm();
    load_matrix_gamma_dt();
    load_matrix_frame_twist_T();

    // z equation
    /**
     *  z - g gamma - zeta gamma_t = 0
     */
    load_matrix_zmass_all();

    // body forcing
    load_force_rhs();
    // explicit terms
    load_more_rhs();
    // beta moment rhs
    load_moment_rhs();
    // curature rhs for bc's
    load_curvature_rhs();
    // gamma rhs
    load_twist_rhs();
    // pressure rhs
    load_pressure_rhs();
    // twisting-moment rhs
    load_twist_moment_rhs();

#ifndef NDEBUG
    {
      // TODO this is similar to test in other entities which use 2 instead of dim also
      // test stiffness matrix
      const unsigned int n = mesh().N();
      const Vector ones( worlddim*n, 1.0 );

      Vector out( worlddim*n );
      _stiffstencil.mv( out, ones );

      if( out.norm2() > 1.0e-15 ) {
	std::cerr << "stiffness matrix failure at timestep: " << _time_provider.iteration() << std::endl;
	std::cerr << "error: " << out.norm2() << std::endl;
	// std::cerr << out << std::endl;
	abort();
      }
    }
#ifdef CONSTANT_RADIUS
    // with non constant viscoelasticity this fails
    {
      // test stiffness matrix
      const unsigned int n = mesh().N();
      const Vector ones(worlddim * n, 1.0);

      Vector out(worlddim * n);
      _pstiffstencil.mv(out, ones);

      if (out.norm2() > 1.0e-15) {
	std::cerr << "pstiffness matrix failure at timestep: "
		  << _time_provider.iteration() << std::endl;
	std::cerr << "error: " << out.norm2() << std::endl;
	// std::cerr << out << std::endl;
	abort();
      }
    }
#endif
    {
      // sum of components of centre of mass of non boundary
      // components
      double comd = 0;
      for( auto&& entity : mesh().elements() ) {
	const ElementGeometry geo( oldPosition(), entity );
	const unsigned l_i = entity.left().index();
	const unsigned r_i = entity.right().index();
	RangeVectorType com(0.0);
	if( l_i == 0 ) { // first element
	} else {
	  com += 0.5 * geo.q() * position().eval( l_i );
	}
	if( r_i == mesh().N()-1 ) { // final element
	} else {
	  com += 0.5 * geo.q() * position().eval( r_i );
	}
	for( unsigned int d = 0; d < worlddim; ++d )
	  comd += com.at(d);
      }

      // test mass matrix
      const unsigned int N = mesh().N();
      const Vector ones(worlddim * N, 1.0);
      Vector MX(worlddim * N);
      Vector X( worlddim * N );

      // position away from boundary components
      for( unsigned n = 1; n < N-1; ++n ) {
	const auto Xx = position().evaluate(n);
	for( unsigned k = 0; k < worlddim; ++k ) {
	  X.at( n*worlddim+k ) = Xx.at(k);
	}
      }

      MX.clear();
      _massstencil.mv(MX, X);
      double t = MX * ones;
      if( std::abs( t - comd ) > 1.0e-10 ) {
	std::cout << "mass test:" << std::abs( t - comd ) << std::endl;
	abort();
      }
      MX.clear();
      _mass2stencil.mv(MX, X);
      double t2 = MX * ones;
      if( std::abs( t2 - comd ) > 1.0e-10 ) {
	std::cout << "mass2 test:" << std::abs( t2 - comd ) << std::endl;
	abort();
      }
    }
#ifdef FLAT_WORM
    {
      double comd = 0;
      // for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
      for( auto&& entity : mesh().elements() ) {
	const ElementGeometry geo( oldPosition(), entity );
	const unsigned l_i = entity.left().index();
	const unsigned r_i = entity.right().index();
	const auto com = 0.5 * geo.q() * ( position().eval( r_i )
					   + position().eval( l_i ) );
	for( unsigned int d = 0; d < worlddim; ++d )
	  comd += com.at(d);
      }
      comd /= deltaT();

      const unsigned int N = mesh().N();
      Vector ones( worlddim * N, 1.0 );
      Vector tau( worlddim * N, 0.0 );
      Vector nu( worlddim * N, 0.0 );
      Vector mu( worlddim * N, 0.0 );
      Vector DX( worlddim * N );
      Vector X( worlddim*N );
      for( unsigned int n = 0; n < worlddim*N; ++n ) {
	X.at(n) = position().at(n);
	if( n % worlddim == 0 ) {
	  tau.at(n) = 1.0;
	} else if( n % worlddim == 1 ) {
	  nu.at(n) = 1.0;
	} else if( n % worlddim == 2 ) {
	  mu.at(n) = 1.0;
	}
      }

      _dragstencil.mv( DX, X );
      const double t1 = DX * tau;
      if( std::abs( t1 - comd ) > 1.0e-10 ) {
	std::cerr << "drag test 1: " << std::abs(t1 - comd) << std::endl;
	abort();
      }

      DX.clear();
      _dragstencil.mv( DX, nu );
      const double t2 = DX * nu;
      if( std::abs( t2 - problem().K() / deltaT() ) > 1.0e-10 ) {
      	std::cerr << "drag test2: " << std::abs( t2 - problem().K() / deltaT() ) << std::endl;
      	abort();
      }

      DX.clear();
      _dragstencil.mv( DX, tau );
      const double t3 = DX * tau;
      if( std::abs( t3 - 1.0 / deltaT() ) > 1.0e-10 ) {
      	std::cerr << "drag test 3: " << std::abs( t3 - 1.0 ) << std::endl;
      	abort();
      }

      DX.clear();
      _dragstencil.mv( DX, tau );
      const double t4 = DX * nu;
      if( std::abs( t4 ) > 1.0e-10 ) {
	std::cerr  << "drag test 4: " << std::abs( t4 ) << std::endl;
	abort();
      }

      DX.clear();
      _dragstencil.mv( DX, nu );
      const double t5 = DX * tau;
      if( std::abs( t5 )> 1.0e-10 ) {
	std::cerr << "drag test 5: " << std::abs( t5 ) << std::endl;
	abort();
      }

      DX.clear();
      _dragstencil.mv(DX, mu);
      const double t6 = DX * mu;
      if (std::abs(t6 - problem().K() / deltaT()) > 1.0e-10) {
	std::cerr << "drag test 6: " << std::abs(t6 - problem().K() / deltaT())
		  << std::endl;
        abort();
      }

      DX.clear();
      _dragstencil.mv(DX, tau);
      const double t7 = DX * mu;
      if (std::abs(t7) > 1.0e-10) {
	std::cerr << "drag test 7: " << std::abs(t7) << std::endl;
	abort();
      }

      DX.clear();
      _dragstencil.mv(DX, mu);
      const double t8 = DX * tau;
      if (std::abs(t8) > 1.0e-10) {
	std::cerr << "drag test 8: " << std::abs(t8) << std::endl;
	abort();
      }
    }
#endif
    { // test z matrices
      // find length
      double length = 0;
      for( auto element : mesh().elements() ) {
	length += geo(element).q();
      }

      {
	const unsigned int N = mesh().N();
	Vector one( N-1, 1.0 );
	Vector tmp( N-1 );

	_zmassstencil.mv( tmp, one );
	const double t1 = std::abs( tmp*one - length );
	if( t1 > 1.0e-10 ) {
	  std::cerr << "z mass test:" << t1 << std::endl;
	  abort();
	}
      }
#if FLAT_WORM
      {
	const unsigned int N = mesh().N();
	Vector one( N-1, 1.0 );
	Vector tmp( N-1 );

	_za_massstencil.mv( tmp, one );
	const double t2 = std::abs( tmp*one + length * problem().g() );
	if( t2 > 1.0e-10 ) {
	  std::cerr << "za mass test:" << t2 << std::endl;
	  abort();
	}
      }

      {
	const unsigned int N = mesh().N();
	Vector one( N-1, 1.0 );
	Vector tmp( N-1 );

	_zat_massstencil.mv( tmp, one );
	const double t3 = std::abs( tmp*one + length * problem().zeta() / deltaT() );
	if( t3 > 1.0e-10 ) {
	  std::cerr << "zat mass test:" << t3 << std::endl;
	  abort();
	}
      }
#endif
    }
#endif
  }
  // called once per solver iteration - does the solve
  virtual void eval() { untested();
    // construct full matrix
    load_matrix_stencils();

#ifndef NDEBUG
#ifdef FLAT_WORM
    // this should be verified
    Vector tmp( _solutionTuple.size() );
    implicitOperator_.call( static_cast<Vector>(_solutionTuple), tmp );
    tmp -= _rhsTuple;
    if( tmp.norm2() > 1.0e-10 ) {
      implicitOperator_.print();
      std::cerr << "rhs test: " << tmp.norm2() << std::endl;
      implicitOperator_.call( static_cast<Vector>(_solutionTuple), tmp );
      for( unsigned int n = 0; n < _solutionTuple.size(); ++n ) {
      	std::cout << n << ": " << tmp.at(n) << " "
    		  << _rhsTuple.at(n) << " "
    		  << std::abs( tmp.at(n) - _rhsTuple.at(n) )
    		  << std::endl;
      }
      abort();
    }
#endif
#endif
    // TODO add tests for new matrices

    // do the solve
    implicitOperator_.solve( _solutionTuple, _rhsTuple );
  }
  // called after the solver iteration
  virtual double tr_review() { return 1e99; }
  // called at the end of the timestep
  virtual void tr_accept() {

    update_frame();
#ifndef DNDEBUG
    { // check worm length
      double sum = 0;
      for( auto&& element : mesh().elements() ) {
	const ElementGeometry geo(position(), element);
	sum += geo.q();
      }
      if (sum < problem().gamma() - 1.0e-10) {
	std::cerr << "length of worm: " << sum << std::endl;
	abort();
      }
    }
    // {
    //   const auto w0 = curvature().evaluate( 0 );
    //   const auto wN = curvature().evaluate( mesh().N()-1 );
    //   if( w0.norm() > 1.0e-10 or wN.norm() > 1.0e-10 ) {
    // 	std::cerr << "curvature at ends: " << w0 << " " << wN << std::endl;
    //   }
    // }
    {
      const auto y0 = moment().evaluate( 0 );
      const auto yN = moment().evaluate( mesh().N()-1 );
      if( y0.norm() > 1.0e-10 or yN.norm() > 1.0e-10 ) {
	std::cerr << "bending moment at ends: " << y0 << " " << yN << std::endl;
	abort();
      }
    }
    /* { // twist_moment = 0 at ends is not enforced exactly
      const auto z0 = twist_moment().evaluate( 0 );
      const auto zN = twist_moment().evaluate( mesh().N()-2 );
      if( z0.norm() > 1.0e-10 or zN.norm() > 1.0e-10 ) {
	std::cerr << "twist moment at ends: " << z0 << " " << zN << std::endl;
	}
    } */
#endif
  }

protected:
  const Mesh& mesh() const { return _mesh; }
  const Problem& problem() const {
    assert( _problem_ptr );
    return *_problem_ptr;
  }

  const PositionFunctionType& position() const {
    return _solutionTuple.function<PositionFunctionType>("position");
  }
  const CurvatureFunctionType& curvature() const {
    return _solutionTuple.function<CurvatureFunctionType>("curvature");
  }
  const BendingMomentFunctionType& moment() const {
    return _solutionTuple.function<BendingMomentFunctionType>("bending-moment");
  }
  const PressureFunctionType& pressure() const {
    return _solutionTuple.function<PressureFunctionType>("pressure");
  }
  const TangentAngularVelocityFunctionType& angular_velocity() const {
    return _solutionTuple.function<TangentAngularVelocityFunctionType>("tau-angular-velocity");
  }
  const TwistFunctionType& twist() const {
    return _solutionTuple.function<TwistFunctionType>("twist");
  }
  const TwistingMomentFunctionType& twist_moment() const {
    return _solutionTuple.function<TwistingMomentFunctionType>("twisting-moment");
  }
  const PositionFunctionType& oldPosition() const {
    return _oldSolutionTuple.function<PositionFunctionType>("position");
  }
  const CurvatureFunctionType& oldCurvature() const {
    return _oldSolutionTuple.function<CurvatureFunctionType>("curvature");
  }
  const TwistFunctionType& oldTwist() const {
    return _oldSolutionTuple.function<TwistFunctionType>("twist");
  }
  PositionFunctionType& position() {
    return _solutionTuple.function<PositionFunctionType>("position");
  }
  CurvatureFunctionType& curvature() {
    return _solutionTuple.function<CurvatureFunctionType>("curvature");
  }
  TwistFunctionType& twist() {
    return _solutionTuple.function<TwistFunctionType>("twist");
  }

  // frame function access
  const FrameFunctionType& frame( const unsigned j ) const {
    if( j == 0 ) {
      return _frame_0;
    } else if( j == 1 ) {
      return _frame_1;
    } else if( j == 2 ) {
      return _frame_2;
    } else {
      throw exception_invalid( "frame number too high" );
    }
  }
  FrameFunctionType& frame( const unsigned j ) {
    if( j == 0 ) {
      return _frame_0;
    } else if( j == 1 ) {
      return _frame_1;
    } else if( j == 2 ) {
      return _frame_2;
    } else {
      throw exception_invalid( "frame number too high" );
    }
  }


  double deltaT() const {
    return _time_provider.deltaT();
  }
  double time () const {
    return _time_provider.time();
  }

  /**
   *  set_initial_condition
   *
   *  Sets the initial conditions for the position and curvature.
   */
  void set_initial_condition() {
    untested();
    interpolate_initial_position();
    interpolate_initial_twist();
    compute_curvature();
    set_initial_frame();

    assert( _solutionTuple.is_valid() );
  }

  void interpolate_initial_position() {
    untested();
    // vertex loop
    for( auto&& vertex : mesh().vertices() ) {
      RangeVectorType Xuj = problem().X0( vertex );
      position().assign(vertex.index(), Xuj);
    }
  }

  void interpolate_initial_twist() {
    for( auto&& element : mesh().elements() ) {
      RangeVector<1> gammauj = problem().gamma_initial( element );
      twist().assign( element.index(), gammauj );
    }
  }

  void compute_curvature() {
    untested();
    const unsigned int N = mesh().N();

     // element loop
    double q_old = norm( position().eval(1) - position().eval(0) );
    auto tau_old = ( position().eval(1) - position().eval(0) ) / q_old;
    for( unsigned int e = 1; e < N-1; ++e ) {
      const double q = norm( position().eval(e+1) - position().eval(e) );
      const auto tau = ( position().eval(e+1) - position().eval(e) ) / q;

      const double Mi = 0.5 * ( q + q_old );
      const auto SXi = tau - tau_old;

      curvature().assign( e, -SXi / Mi );

      q_old = q;
      tau_old = tau;
    }
 }

  void set_initial_frame() {
    for( auto&& vertex : mesh().vertices() ) {
      const unsigned idx = vertex.index();
      const auto p_l = position().evaluate( ( idx > 0 ) ? (idx-1) : idx );
      const auto p_c = position().evaluate( idx );
      const auto p_r = position().evaluate( ( idx < _mesh.N()-1 ) ? (idx+1) : idx  );

      RangeVectorType tau_l = ( p_c - p_l );
      // assert( tau_l.norm() > 0 ); -- not true at idx = 0
      tau_l /= std::max( tau_l.norm(), 1.0e-10 );
      RangeVectorType tau_r = ( p_r - p_c );
      // assert( tau_r.norm() > 0 ); -- not true at idx = N-1
      tau_r /= std::max( tau_r.norm(), 1.0e-10 );
      // find tau
      RangeVectorType tau = 0.5 * ( tau_l + tau_r );
      assert( tau.norm() > 0 );
      tau /= tau.norm();

      // find e1 from problem
      const RangeVectorType e1 = problem().initial_frame1( vertex );

      // find e2 via cross product
      const RangeVectorType e2 = cross_product( tau, e1 );

      // store
      frame(0).assign( idx, tau );
      frame(1).assign( idx, e1 );
      frame(2).assign( idx, e2 );
    }
  }

  void setup_matrix_stencils() {
    // determine offsets
    const unsigned int x_offset = _solutionTuple.offset("position");
    const unsigned int y_offset = _solutionTuple.offset("bending-moment");
    const unsigned int w_offset = _solutionTuple.offset("curvature");
    const unsigned int p_offset = _solutionTuple.offset("pressure");
    const unsigned int m_offset = _solutionTuple.offset("tau-angular-velocity");
    const unsigned int gamma_offset = _solutionTuple.offset("twist");
    const unsigned int z_offset = _solutionTuple.offset("twisting-moment");

    // create stencils

    // x equation
    _dragstencil.init(x_offset, x_offset);
    _all_stencils.push_back(&_dragstencil);
    _presstencil.init(x_offset, p_offset);
    _all_stencils.push_back(&_presstencil);
    _pstiffstencil.init(x_offset, y_offset);
    _all_stencils.push_back(&_pstiffstencil);
    _frame_twist_stencil.init(x_offset,
			      z_offset);
    _all_stencils.push_back(&_frame_twist_stencil);

    // y equation
    _massstencil.init(y_offset, y_offset);
    _all_stencils.push_back(&_massstencil);
    _emassstencil.init(y_offset, w_offset);
    _all_stencils.push_back(&_emassstencil);
    _pmassstencil.init(y_offset, w_offset);
    _all_stencils.push_back(&_pmassstencil);
    _pmass2stencil.init(y_offset, w_offset);
    _all_stencils.push_back(&_pmass2stencil);

    // w equation
    _mass2stencil.init(w_offset, w_offset);
    _all_stencils.push_back(&_mass2stencil);
    _stiffstencil.init(w_offset, x_offset);
    _all_stencils.push_back(&_stiffstencil);

    // gamma equation
    _rot_drag_stencil.init(m_offset,
			   m_offset);
    _all_stencils.push_back(&_rot_drag_stencil);
    _rot_stiff_stencil.init(m_offset,
			    z_offset);
    _all_stencils.push_back(&_rot_stiff_stencil);

    // alpha equation
    _dgamma_stencil.init(gamma_offset,
			 m_offset);
    _all_stencils.push_back(&_dgamma_stencil);
    _alpha_dt_stencil.init(
	gamma_offset,
	gamma_offset);
    _all_stencils.push_back(&_alpha_dt_stencil);
    _frame_twist_T_stencil.init(
        gamma_offset, x_offset);
    _all_stencils.push_back(&_frame_twist_T_stencil);

    // z equation
    _zmassstencil.init( z_offset, z_offset );
    _all_stencils.push_back( &_zmassstencil );
    _za_massstencil.init( z_offset, gamma_offset );
    _all_stencils.push_back( &_za_massstencil );
    _zat_massstencil.init( z_offset, gamma_offset );
    _all_stencils.push_back( &_zat_massstencil );

    // p equation
    _constraint_stencil.init( p_offset, x_offset );
    _all_stencils.push_back( &_constraint_stencil );

    // allocate memory
    const unsigned N = mesh().N();
    std::for_each( _all_stencils.begin(), _all_stencils.end(),
		   [N]( stencil_base* s ){ s->alloc(N); } );

    // mark? TODO what does this do?
    SquareMatrixFootprint t( _solutionTuple.size() );
    std::for_each(_all_stencils.begin(), _all_stencils.end(),
		  [&t]( stencil_base* s ){ s->mark(t); });

    // TODO what does this do?
    implicitOperator_.init(t);

    // TODO what does this do?
    std::for_each( _all_stencils.begin(), _all_stencils.end(),
		   [this]( stencil_base* s ){ s->map( implicitOperator_ ); } );

    // TODO what does this do?
    implicitOperator_.assemble();

    return;
  }

  void load_matrix_stencils() {
    std::for_each( _all_stencils.begin(), _all_stencils.end(),
		   [this]( stencil_base* s ){ s->load( implicitOperator_ ); } );
  }

  /** load implementation **/

  struct ElementGeometry {
    using E = typename Mesh::Element;

    ElementGeometry( const PositionFunctionType& position,
		     const E& e  )
      : _position( position ), _e( e ), _q( -1.0 ) {
      update();
    }

    RangeVectorType left_position() const {
      const auto left = _e.left();
      return _position.evaluate( left.index() );
    }
    RangeVectorType right_position() const {
      const auto right = _e.right();
      return _position.evaluate( right.index() );
    }
    double q() const {
      assert( _q > 0 );
      return _q;
    }
    RangeVectorType tau() const {
      assert( std::abs(_tau*_tau - 1.0) < 1.0e-10 );
      return _tau;
    }

    void update() {
      _q = ( left_position() - right_position() ).norm();
      _tau = ( right_position() - left_position() ) / _q;
    }

  private:
    const PositionFunctionType& _position;
    const E _e;

    double _q;
    RangeVectorType _tau;
  };

  const ElementGeometry& geo( const typename Mesh::Element& e ) {
    return _geometry_cache.at( e.index() );
  }

  // stiffness matrix
  void load_matrix_stiff() {
    _stiffstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      // set up geometry
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double stiff = 1.0 / geo(element).q();
      assert( stiff == stiff );

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	unsigned l = k;

	if (l_ind == 0) { // first element
	} else {
	  _stiffstencil.add(l_ind, l_ind, k, l, stiff);
	  _stiffstencil.add(l_ind, r_ind, k, l, -stiff);
	}
	if (r_ind == mesh().N() - 1) { // last element
	} else {
	  _stiffstencil.add(r_ind, l_ind, k, l, -stiff);
	  _stiffstencil.add(r_ind, r_ind, k, l, stiff);
	}
      } // k
    } // e
  }

  // non symmetric terms
  void load_matrix_pres() {
    _presstencil.clear();
    _constraint_stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned e_ind = element.index();
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      // set up geometry
      const auto tau = geo(element).tau();

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	const double pres = tau[k];
	assert( pres == pres );

	// add to matrix
	_presstencil.add( l_ind, e_ind, k, 0, pres );
	_presstencil.add( r_ind, e_ind, k, 0, -pres );

	_constraint_stencil.add( e_ind, l_ind, 0, k, pres );
	_constraint_stencil.add( e_ind, r_ind, 0, k, -pres );
      } // k
    }   // e
  }

  void load_matrix_frame_twist() {
    _frame_twist_stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned e_ind = element.index();
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      // set up geometry
      const auto tau = geo(element).tau();

      // evaluate g
      const double g_left = problem().g( element.left() );
      const double g_right = problem().g( element.right() );

      // evaluate w
      const RangeVectorType w_left = curvature().evaluate( l_ind );
      const RangeVectorType w_right = curvature().evaluate( r_ind );

      // cross = tau x w
      const RangeVectorType cross_left = cross_product( tau, w_left );
      const RangeVectorType cross_right = cross_product( tau, w_right );

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	const double value = 0.5 * ( g_left * cross_left[k] + g_right * cross_right[k] );
	assert( value == value );

	_frame_twist_stencil.add( l_ind, e_ind, k, 0, value );
	_frame_twist_stencil.add( r_ind, e_ind, k, 0, -value );
      } // k
    }   // e
  }

  void load_matrix_frame_twist_T() {
    _frame_twist_T_stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned e_ind = element.index();
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      // set up geometry
      const auto tau = geo(element).tau();

      // evaluate w
      const RangeVectorType w_left = curvature().evaluate( l_ind );
      const RangeVectorType w_right = curvature().evaluate( r_ind );

      // cross = tau x w
      const RangeVectorType cross_left = cross_product( tau, w_left );
      const RangeVectorType cross_right = cross_product( tau, w_right );

      const double dt = deltaT();

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	const double value = 0.5 * ( cross_left[k] + cross_right[k] ) / dt;
	assert( value == value );

	_frame_twist_T_stencil.add( e_ind, l_ind, 0, k, value );
	_frame_twist_T_stencil.add( e_ind, r_ind, 0, k, -value );
      } // k
    }   // e
  }

  void load_matrix_rot_stiff() {
    _rot_stiff_stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned e_ind = element.index();
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();

      const double value = -1.0;

      _rot_stiff_stencil.add( l_ind, e_ind, 0, 0, value );
      _rot_stiff_stencil.add( r_ind, e_ind, 0, 0, -value );
    }   // e
  }

  void load_matrix_dm() {
    _dgamma_stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned e_ind = element.index();
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();

      const double value = 1.0;

      _dgamma_stencil.add( e_ind, l_ind, 0, 0, value );
      _dgamma_stencil.add( e_ind, r_ind, 0, 0, -value );
    }   // e
  }

  /**
   *  Drag terms
   *
   * - continuous form: D x_t = [ K ( id - tau otimes tau ) + tau otimes tau ] x_t
   */
  void load_matrix_drag() {
    _dragstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );

      // element geometry
      const double q = geo(element).q();
      const auto tau = geo(element).tau();

      // evaluate K
      const double K_left = problem().K( element.left() );
      const double K_right = problem().K( element.right() );

      // dim range double loop
      for( unsigned int k = 0; k < worlddim; ++k ) {
	for (unsigned int l = 0; l < worlddim; ++l) {
	  // const double D_lk = K * ( id[k==l] - tau[k]*tau[l] ) + tau[k]*tau[l];

	  const double id = (double)(k == l);
	  const double D_left = K_left * ( id - tau[k]*tau[l] ) + tau[k]*tau[l];
	  const double D_right = K_right * ( id - tau[k]*tau[l] ) + tau[k]*tau[l];
	  assert( D_left == D_left );
	  assert( D_right == D_right );

	  const double drag_left = 0.5 * q * D_left / deltaT();
          _dragstencil.add_(l_ind, l_ind, k, l, drag_left);
	  const double drag_right = 0.5 * q * D_right / deltaT();
	  _dragstencil.add_(r_ind, r_ind, k, l, drag_right);
	}
      }
    }
  }

  void load_matrix_rot_drag() {
    _rot_drag_stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      // element geometry
      const double q = geo(element).q();

      // evaluate K_rot
      const double K_left = problem().K_rot( element.left() );
      const double K_right = problem().K_rot( element.right() );

      const double rot_drag_left = -0.5 * q * K_left;
      assert( rot_drag_left == rot_drag_left );
      assert( rot_drag_left != 0 );
      _rot_drag_stencil.add_(l_ind, l_ind, 0, 0, rot_drag_left);
      const double rot_drag_right = -0.5 * q * K_right;
      assert( rot_drag_right == rot_drag_right );
      assert( rot_drag_right != 0 );
      _rot_drag_stencil.add_(r_ind, r_ind, 0, 0, rot_drag_right);
    }
  }

  void load_matrix_gamma_dt() {
    _alpha_dt_stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned e = element.index();

      // element geometry
      const double q = geo(element).q();

      // evaluate delta_T
      const double dt = deltaT();

      // evaluate term and add
      const double mass_dt = -q / dt;
      assert( mass_dt == mass_dt );
      assert( mass_dt != 0 );
      _alpha_dt_stencil.add_(e, e, 0, 0, mass_dt);
    }
  }


  void load_matrix_mass() {
    _massstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double mass = 0.5 * geo(element).q();
      assert( mass == mass );
      assert( mass != 0 );

      // dim range loop
      for( unsigned int k = 0; k < worlddim; ++k ) {
	if( l_ind == 0 ) { // first element
	} else {
	  _massstencil.add_( l_ind, l_ind, k, k, mass);
	}
	if( r_ind == mesh().N() - 1 ) { // last element
	} else {
	  _massstencil.add_( r_ind, r_ind, k, k, mass);
	}
      }
    }

    // boundary conditions
    for( unsigned int k = 0; k < worlddim; ++k ) {
      _massstencil.add_( 0, 0, k, k, 1.0 );
    }
    for( unsigned int k = 0; k < worlddim; ++k ) {
      _massstencil.add_( mesh().N()-1, mesh().N()-1, k, k, 1.0 );
    }
  }

  void load_matrix_mass2() {
    _mass2stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double mass2 = 0.5 * geo(element).q();
      assert( mass2 == mass2 );
      assert( mass2 != 0 );

      // dim range loop
      for( unsigned int k = 0; k < worlddim; ++k ) {
	if( l_ind == 0 ) { // first element
	} else {
	  _mass2stencil.add_(l_ind, l_ind, k, k, mass2);
	}
	if( r_ind == mesh().N() - 1 ) { // last element
	} else {
	  _mass2stencil.add_(r_ind, r_ind, k, k, mass2);
	}
      }
    }

    // boundary conditions
    const unsigned back = mesh().N()-1;
    for( unsigned k = 0; k < worlddim; ++k ) {
      _mass2stencil.add_( 0, 0, k, k, 1.0 );
      _mass2stencil.add_( back, back, k, k, 1.0 );
    }
  }

  void load_matrix_emass() {
    _emassstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );

      // evaluate e
      const double e_left = problem().e( element.left() );
      const double e_right = problem().e( element.right() );

      const double emass_left = - e_left * 0.5 * geo(element).q();
      assert( emass_left == emass_left );
      assert( emass_left != 0 );
      const double emass_right = - e_right * 0.5 * geo(element).q();
      assert( emass_right == emass_right );
      assert( emass_right != 0 );

      // dim range loop
      for( unsigned int k = 0; k < worlddim; ++k ) {
	if( l_ind == 0 ) { // first element
	} else {
	  _emassstencil.add_(l_ind, l_ind, k, k, emass_left);
	}
	if( r_ind == mesh().N() -1 ) { // final element
	} else {
	  _emassstencil.add_(r_ind, r_ind, k, k, emass_right);
	}
      }
    }
  }

  void load_matrix_pmass() {
    _pmassstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double q = geo(element).q();
      const auto tau_left = frame(0).evaluate( l_ind );
      const auto tau_right = frame(0).evaluate( r_ind );

      // evaluate mu
      const double mu_left = problem().mu( element.left() );
      const double mu_right = problem().mu( element.right() );

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	for (unsigned int l = 0; l < worlddim; ++l) {
	  const double Ptau_left = ((double)(k == l) - tau_left[l] * tau_left[k]);
	  assert(Ptau_left == Ptau_left);
	  const double Ptau_right = ((double)(k == l) - tau_right[l] * tau_right[k]);
	  assert(Ptau_right == Ptau_right);

	  const double pmass_left = -mu_left * Ptau_left * 0.5 * q / deltaT();
	  assert( pmass_left == pmass_left );
	  const double pmass_right = -mu_right * Ptau_right * 0.5 * q / deltaT();
	  assert( pmass_right == pmass_right );

	  if (l_ind == 0) { // first element
	  } else {
	    _pmassstencil.add_(l_ind, l_ind, k, l, pmass_left);
	  }
	  if (r_ind == mesh().N() - 1) { // final element
	  } else {
	    _pmassstencil.add_(r_ind, r_ind, k, l, pmass_right);
	  }
        }
      }
    }
  }

  void load_matrix_pmass2() {
    // TODO are the l and k the right way round?
    _pmass2stencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double q = geo(element).q();
      const auto tau_left = frame(0).evaluate( l_ind );
      const auto tau_right = frame(0).evaluate( r_ind );

      const Skew skew_left( tau_left );
      const Skew skew_right( tau_right );

      // evaluate m
      const double m_left = angular_velocity().evaluate( l_ind );
      const double m_right = angular_velocity().evaluate( r_ind );

      // evaluate mu
      const double mu_left = problem().mu( element.left() );
      const double mu_right = problem().mu( element.right() );

      // mu m tau times w
      for( unsigned l = 0; l < worlddim; ++l ) {
	for( unsigned k = 0; k < worlddim; ++k ) {
	  const double pmass2_left = 0.5 * mu_left * m_left * skew_left(l, k) * q;
	  assert(pmass2_left == pmass2_left);
	  const double pmass2_right = 0.5 *  mu_right * m_right * skew_right(l, k) * q;
	  assert(pmass2_right == pmass2_right);

	  if (l_ind == 0) { // first element
	  } else {
	    _pmass2stencil.add_(l_ind, l_ind, l, k, pmass2_left);
	  }
	  if (r_ind == mesh().N() - 1) { // final element
	  } else {
	    _pmass2stencil.add_(r_ind, r_ind, l, k, pmass2_right);
          }
        } // end k
      } // end l
    } // end element loop
  }

  void load_matrix_pstiff() {
    _pstiffstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert( ( l_ind == element.index() ) and
	      ( r_ind == element.index() + 1 ) );
      const double q = geo(element).q();
      const auto tau = geo(element).tau();

      // dim range double loop
      for (unsigned k = 0; k < worlddim; ++k) {
	for (unsigned int l = 0; l < worlddim; ++l) {
	  const double Ptau = ((double)(k == l) - tau[l] * tau[k]);
	  assert(Ptau == Ptau);
	  const double pStiff = -Ptau/q;
	  assert(pStiff == pStiff);

	  const double pStiff_right = pStiff;
	  const double pStiff_left = pStiff;

	  // [ |\| ]
	  // [-+-+-]
	  // [ | | ]
	  // [-+-+-]
	  // [ | | ]
	  _pstiffstencil.add(l_ind, l_ind, k, l, pStiff_left);
	  _pstiffstencil.add(r_ind, r_ind, k, l, pStiff_right);

	  _pstiffstencil.add(r_ind, l_ind, k, l, -pStiff_left);
	  _pstiffstencil.add(l_ind, r_ind, k, l, -pStiff_right);
	}
      }
    }
  }

  void load_matrix_zmass_all() {
    _zmassstencil.clear();
    _za_massstencil.clear();
    _zat_massstencil.clear();

    // element loop
    for( auto&& element : mesh().elements() ) {
      const unsigned e = element.index();
      const auto& v_left = element.left();
      const auto& v_right = element.right();

      // element geometry
      const double q = geo(element).q();

      // evaluate delta_T
      const double dt = deltaT();

      // evaluate term and add
      const double z_mass = q;
      assert( z_mass == z_mass );
      assert( z_mass != 0 );
      _zmassstencil.add_( e, e, 0, 0, z_mass );

      //
      const double g = 0.5 * ( problem().g( v_left ) + problem().g( v_right ) );
      const double ga_mass = -g* q;
      assert( ga_mass == ga_mass );
      _za_massstencil.add_( e, e, 0, 0, ga_mass );

      //
      const double zeta = 0.5 * ( problem().zeta( v_left ) + problem().zeta( v_right ) );
      const double xiat_mass = - zeta * q / dt;
      assert( xiat_mass == xiat_mass );
      _zat_massstencil.add_( e, e, 0, 0, xiat_mass );
    }
  }

  // dynamic drag ( isn't this diagonal? )
  void load_dynamic_drag() {
    return;
#if 0
    // not in current model
    // _dragstencil.clear();

    // element loop
    for (auto &&element : mesh().elements()) {
      const unsigned l_ind = element.left().index();
      const unsigned r_ind = element.right().index();
      assert((l_ind == element.index()) and (r_ind == element.index() + 1));
      const double q = geo(element).q();

      // evaluate curvature and energy density
      /* const RangeVectorType y_left = curvature().evaluate(l_ind); */
      /* const RangeVectorType y_right = curvature().evaluate(r_ind); */
      const double energy_density_left = 0;
      const double energy_density_right = 0;

      // dim range loop
      for (unsigned int k = 0; k < worlddim; ++k) {
	for (unsigned int l = 0; l < worlddim; ++l) {
	  // [\\ |   |  ]
	  // [\\\|   |  ]
	  // [ \\|   |  ]
	  // [---+---+--]
	  // [   |   |  ]
	  // [---+---+--]
	  // [   |   |  ]
	  const double wStiff_left = energy_density_left / q;
	  const double wStiff_right = energy_density_right / q;
	    untested();
	    _dragstencil.add_(l_ind, l_ind, l, k, wStiff_left);
	    _dragstencil.add_(l_ind, r_ind, l, k, -wStiff_left);
	    _dragstencil.add_(r_ind, r_ind, l, k, wStiff_right);
	    _dragstencil.add_(r_ind, l_ind, l, k, -wStiff_right);
	} // l loop
      }   // k loop
    }     // e loop
#endif
  }

  void load_more_rhs() {
    // explicit terms
    auto& rhs_position
      = _rhsTuple.function<PositionFunctionType>("position");
    auto& rhs_moment
      = _rhsTuple.function<BendingMomentFunctionType>("bending-moment");
    auto& rhs_twist
      = _rhsTuple.function<TwistFunctionType>("twist");
    auto& rhs_z
      = _rhsTuple.function<TwistingMomentFunctionType>("twisting-moment");

    _dragstencil.mv( rhs_position, oldPosition() );
    _pmassstencil.mv( rhs_moment, oldCurvature() );
    _alpha_dt_stencil.mv( rhs_twist, oldTwist() );
    _frame_twist_T_stencil.mv( rhs_twist, oldPosition() );
    _zat_massstencil.mv( rhs_z, oldTwist() );
  }

  void load_force_rhs() {
    auto& rhs_position
      = _rhsTuple.function<PositionFunctionType>("position");

    // element loop
     for( auto&& element : mesh().elements() ) {
       const double q = geo(element).q();

      // inner element loop (left or right)
      // for( auto&& vertex : element.innerVertices() ) {
       RangeVectorType f_old = problem().f( mesh().vertices().front() );
      { const auto& vertex = element.left();
	/* const auto x_ie = position().evaluate( vertex.index() ); */

	RangeVectorType value = 0.5 * q * f_old;
	rhs_position.add( vertex.index(), value );
      }
      { const auto& vertex = element.right();
	/* const auto x_ie = position().evaluate( vertex.index() ); */

	// evaluate force
	RangeVectorType f = problem().f( vertex ); // TODO: see problem::f();

	RangeVectorType value = 0.5 * q * f;
	rhs_position.add( vertex.index(), value );

	f_old = f;
      }
    }

    // TODO missing extra forces?
  }

  void load_moment_rhs() {
    auto& rhs_moment
      = _rhsTuple.function<BendingMomentFunctionType>("bending-moment");

    for( auto&& element : mesh().elements() ) {
      // find geometry
      const double q = geo(element).q();
      const auto v_left = element.left();
      const auto v_right = element.right();

      // find frame
      const auto frame1_left = frame(1).evaluate( v_left.index() );
      const auto frame2_left = frame(2).evaluate( v_left.index() );
      const auto frame1_right = frame(1).evaluate( v_right.index() );
      const auto frame2_right = frame(2).evaluate( v_right.index() );

      // evaluate beta
      // TODO sort out signs! this tried to match to erronous 2d case
      const auto beta_left = problem().beta( v_left );
      const auto beta_right = problem().beta( v_right );

      // evaluate e
      const double e_left = problem().e( v_left );
      const double e_right = problem().e( v_right );

      // compute values
      // TODO check signs
      const auto value_left = -0.5 * e_left * q * ( beta_left.at(0) * frame1_left + beta_left.at(1) * frame2_left );
      const auto value_right = -0.5 * e_right * q * ( beta_right.at(0) * frame1_right + beta_right.at(1) * frame2_right );

      // update rhs
      if( v_left.index() == 0 ) { // first element
      } else {
	rhs_moment.add( v_left.index(), value_left );
      }
      if( v_right.index() == mesh().N()-1 ) { // final element
      } else {
	rhs_moment.add( v_right.index(), value_right );
      }
    }
  }

  void load_curvature_rhs() {
    auto& rhs_curvature
      = _rhsTuple.function<CurvatureFunctionType>("curvature");

    {
      const unsigned front = 0;
      const auto frame1 = frame(1).evaluate( front );
      const auto frame2 = frame(2).evaluate( front );
      const auto beta = problem().beta( mesh().vertices().front() );
      const auto val = beta.at(0) * frame1 + beta.at(1) * frame2;
      rhs_curvature.assign( front, val );
    }
    {
      const unsigned back = mesh().N()-1;
      const auto frame1 = frame(1).evaluate( back );
      const auto frame2 = frame(2).evaluate( back );
      const auto beta = problem().beta( mesh().vertices().back() );
      const auto val = beta.at(0) * frame1 + beta.at(1) * frame2;
      rhs_curvature.assign( back, val );
    }
  }

  void load_twist_rhs() {
    auto& rhs_gamma
      = _rhsTuple.function<TangentAngularVelocityFunctionType>("tau-angular-velocity");

    const auto tau_l = frame(0).evaluate( 0 );
    const auto y_l = moment().evaluate( 0 );
    const auto w_l = curvature().evaluate( 0 );
    double tau_dot_y_times_w_l = tau_l * cross_product( y_l, w_l );

    // element loop
    for( auto&& element : mesh(). elements() ) {
      // get element geometry
      const double q = geo(element).q();

      // - y cdot ( tau times w ) phi_h
      // = tau cdot ( y times w ) phi_h

      // find tau_tilde
      RangeVectorType tau_r = frame(0).evaluate( element.right().index() );

      // find y and w
      const auto y_r = moment().evaluate( element.right().index() );
      const auto w_r = curvature().evaluate( element.right().index() );

      // evaluate product
      const double tau_dot_y_times_w_r = tau_r  * cross_product( y_r, w_r );

      // evaluate term
      const double val_left = 0.5 * q * tau_dot_y_times_w_l;
      const double val_right = 0.5 * q * tau_dot_y_times_w_r;

      // add to dof vector
      rhs_gamma.add_scalar( element.left().index(), val_left );
      rhs_gamma.add_scalar( element.right().index(), val_right );

      // update left product
      tau_dot_y_times_w_l = tau_dot_y_times_w_r;
    }
  }

  void load_twist_moment_rhs() {
    auto& rhs_z
      = _rhsTuple.function<TwistingMomentFunctionType>("twisting-moment");

    for( auto&& element : mesh().elements() ) {
      // get geometry
      const double q = geo( element ).q();

      // evaluate forcing
      const double gamma0 = problem().gamma_forcing( element );

      // evaluate g
      const double g = 0.5 * ( problem().g( element.left() )
			       + problem().g( element.right() ) );

      const double val = -gamma0 * g * q;

      rhs_z.add_scalar( element.index(), val );
    }
  }

  void load_pressure_rhs() {
    auto& rhs_pressure
      = _rhsTuple.function<PressureFunctionType>("pressure");
    const double gamma = problem().gamma();

    // element loop
     for( auto&& element : mesh().elements() ) {
      const unsigned e = element.index();
      rhs_pressure[e] = element.h() * -gamma;
    }
  }

  void update_frame() {
    for( auto&& vertex : mesh().vertices() ) {
      const unsigned idx = vertex.index();
      // find tau
      const auto p_l = position().evaluate( ( idx > 0 ) ? (idx-1) : idx );
      const auto p_c = position().evaluate( idx );
      const auto p_r = position().evaluate( ( idx < _mesh.N()-1 ) ? (idx+1) : idx  );

      RangeVectorType tau_l = ( p_c - p_l );
      // assert( tau_l.norm() > 0 ); -- not true at idx = 0
      tau_l /= std::max( tau_l.norm(), 1.0e-10 );
      RangeVectorType tau_r = ( p_r - p_c );
      // assert( tau_r.norm() > 0 ); -- not true at idx = N-1
      tau_r /= std::max( tau_r.norm(), 1.0e-10 );
      // find tau
      RangeVectorType tau = 0.5 * ( tau_l + tau_r );
      assert( tau.norm() > 0 );
      tau /= tau.norm();

      // find old tau
      const auto old_tau = frame(0).evaluate( idx );

      // find rotation vector
      const double m = angular_velocity().evaluate( idx );

      const auto frame1_old = frame(1).evaluate( idx );
      const auto frame2_old = frame(2).evaluate( idx );

      RangeVectorType f1 = problem().frame_update( frame1_old, old_tau, tau, m, deltaT() );
      RangeVectorType f2 = problem().frame_update( frame2_old, old_tau, tau, m, deltaT() );

      // test still a frame
      assert( std::abs( f1 * f2 ) < 1.0e-9 );
      assert( std::abs( f1 * f1 - 1.0 ) < 1.0e-9 );
      assert( std::abs( f2 * f2 - 1.0 ) < 1.0e-9 );
      assert( std::abs( tau * f1 ) < 1.0e-9 );
      assert( std::abs( tau * f2 ) < 1.0e-9 );

      // update frames
      frame(0).assign( idx, tau );
      frame(1).assign( idx, f1 );
      frame(2).assign( idx, f2 );
    }
  }

  /** PROBES **/
  // TODO should these be public?
public:
  struct Centre_of_mass_probe : public Probe_otf<RangeVectorType> {
    using Base = Probe_otf<RangeVectorType>;
    Centre_of_mass_probe(const Bending_twisting_mechanics_entity &e)
	: Base("mechanics.centre_of_mass"), _e(e) {}

    virtual RangeVectorType evaluate() const {
      RangeVectorType ret(0.0);
      for (auto &&entity : _e.mesh().elements()) {
	const ElementGeometry geo(_e.position(), entity);
	const unsigned l_i = entity.left().index();
	const unsigned r_i = entity.right().index();
	const auto com =
	    0.5 * geo.q() * (_e.position().eval(r_i) + _e.position().eval(l_i));
	ret += com;
      }

      return ret;
    }

  private:
    const Bending_twisting_mechanics_entity &_e;
    friend class Bending_twisting_mechanics_entity;
  };

  struct Head_probe : public Probe_otf<RangeVectorType> {
    using Base = Probe_otf<RangeVectorType>;
    Head_probe(const Bending_twisting_mechanics_entity &e)
	: Base("mechanics.head"), _e(e) {}

    virtual RangeVectorType evaluate() const {
      return _e.position().eval(0);
    }

  private:
    const Bending_twisting_mechanics_entity &_e;
    friend class Bending_twisting_mechanics_entity;
  };

  struct Tail_probe : public Probe_otf<RangeVectorType> {
    using Base = Probe_otf<RangeVectorType>;
    Tail_probe(const Bending_twisting_mechanics_entity &e)
	: Base("mechanics.tail"), _e(e) {}

    virtual RangeVectorType evaluate() const {
      const unsigned N = _e.mesh().N();
      return _e.position().eval(N-1);
    }

  private:
    const Bending_twisting_mechanics_entity &_e;
    friend class Bending_twisting_mechanics_entity;
  };

  struct Length_probe : public Probe_otf<double> {
    using Base = Probe_otf<double>;
    Length_probe(const Bending_twisting_mechanics_entity &e)
	: Base("mechanics.length"), _e(e) {}

    virtual double evaluate() const {
      double l = 0.0;
      for( auto && element : _e.mesh().elements() ) {
	const auto left_position = _e.position().eval( element.left().index() );
	const auto right_position = _e.position().eval( element.right().index() );

	l += ( right_position - left_position ).norm();
      }
      return l;
    }

  private:
    const Bending_twisting_mechanics_entity &_e;
    friend class Bending_twisting_mechanics_entity;
  };

  struct Energy_probe : public Probe_otf<double> {
    using Base = Probe_otf<double>;
    Energy_probe(const Bending_twisting_mechanics_entity &e)
      : Base("mechanics.energy"), _e(e) {}

    virtual double evaluate() const override {
      // energy is int e (kappa - kappa0)^2 + g (twist - twist0)^2
      // TODO add force term to energy
      double ret = 0.0;

      const auto& w = _e.curvature();
      const auto& gamma = _e.twist();

      for( auto&& element : _e.mesh().elements() ) {
	const ElementGeometry geo(_e.oldPosition(), element );
	const double q = geo.q();
	const auto& v_left = element.left();
	const auto& v_right = element.right();

	// get weight
	const double e_left = _e.problem().e( v_left );
	const double e_right = _e.problem().e( v_right );

	// get w
	const auto w_left = w.evaluate( v_left.index() );
	const auto w_right = w.evaluate( v_right.index() );

	// get beta TODO sort signs
	const auto b_left = _e.problem().beta( v_left );
	const auto b_right = _e.problem().beta( v_right );

	// get frame
	const auto f1_left = _e.frame(1).evaluate( v_left.index() );
	const auto f2_left = _e.frame(2).evaluate( v_left.index() );
	const auto f1_right = _e.frame(1).evaluate( v_right.index() );
	const auto f2_right = _e.frame(2).evaluate( v_right.index() );

	// find forcing
	const auto w0_left = b_left.at(0) * f1_left + b_left.at(1) * f2_left;
	const auto w0_right = b_right.at(0) * f1_right + b_right.at(1) * f2_right;

	// find difference
	const auto w_diff_left = w_left - w0_left;
	const auto w_diff_right = w_right - w0_right;

	// find integrand
	const double w_int_left = e_left * ( w_diff_left * w_diff_left );
	const double w_int_right = e_right * ( w_diff_right * w_diff_right );
	const double w_int_el = 0.5 * ( w_int_left + w_int_right );

	// get weight
	const double g_left = _e.problem().g( v_left );
	const double g_right = _e.problem().g( v_right );

	// get twist
	const double gamma_el = gamma.evaluate( element.index() );

	// get gamma0
	const double gamma0_el = _e.problem().gamma_forcing( element );

	// find difference
	const double gamma_diff = gamma_el - gamma0_el;

	// find integrand
	const double gamma_int_el = 0.5 * ( g_left + g_right ) * ( gamma_diff * gamma_diff );

	// perform integral
	ret += q * ( w_int_el + gamma_int_el );
      }

      return ret;
    }

  private:
    const Bending_twisting_mechanics_entity &_e;
    friend class Bending_twisting_mechanics_entity;
  };

  struct Curvature_component_probe : public Probe_otf< const PiecewiseLinearFunction<1>& > {
    using T = const PiecewiseLinearFunction<1>&;
    using Base = Probe_otf< T >;
    Curvature_component_probe( const Bending_twisting_mechanics_entity& e, const unsigned int j )
      : Base( "mechanics.kappa" + std::to_string(j) ), _e( e ), _j( j ),
	// curvature component
	_kappa_storage( _e.mesh().N() ),
	_kappa( _e.mesh(), SubArray<Vector>( _kappa_storage ) ) {}

    T evaluate() const {
      _kappa_storage.clear();

      const auto& w = _e.curvature();
      const auto& frame = _e.frame( _j );

      for( auto&& vertex : _e.mesh().vertices() ) {
	const auto w_v = w.evaluate( vertex.index() );
	const auto frame_v = frame.evaluate( vertex.index() );
	_kappa.assign( vertex.index(), w_v * frame_v );
      }

      return _kappa;
    }

  private:
    const Bending_twisting_mechanics_entity& _e;
    const unsigned _j;
    friend class Bending_twisting_mechanics_entity;
    // extra storage of curvature component
    mutable Vector _kappa_storage;
    mutable PiecewiseLinearFunction<1> _kappa;
  };

  struct Darboux_probe : public Probe_otf< const PiecewiseLinearFunction<3>& > {
    using T = const PiecewiseLinearFunction<3>&;
    using Base = Probe_otf< T >;
    Darboux_probe( const Bending_twisting_mechanics_entity& e )
      : Base( "mechanics.darboux" ), _e( e ),
	// curvature component
	_darboux_storage( 3*_e.mesh().N() ),
	_darboux( _e.mesh(), SubArray<Vector>( _darboux_storage ) ) {}

    T evaluate() const {
      _darboux_storage.clear();

      const auto& w = _e.curvature();
      const auto& tau = _e.frame( 0 );
      const auto& gamma = _e.twist();

      for( auto&& vertex : _e.mesh().vertices() ) {
	const auto w_v = w.evaluate( vertex.index() );
	const auto tau_v = tau.evaluate( vertex.index() );

	double gamma_v = 0.0;
	{
	  const unsigned idx = vertex.index();
	  const unsigned idx_l = ( idx > 0 ) ? ( idx - 1 ) : idx;
	  const unsigned idx_r = ( idx < _e.mesh().N()-1 ) ? idx : ( _e.mesh().N()-2 );
	  const auto gamma_l = gamma.evaluate( idx_l );
	  const auto gamma_r = gamma.evaluate( idx_r );
          gamma_v = 0.5 * (gamma_l + gamma_r).at(0);
        }
        RangeVectorType darboux_v = cross_product(tau_v, w_v) + gamma_v * tau_v;
        _darboux.assign( vertex.index(), darboux_v );
      }

      return _darboux;
    }

  private:
    const Bending_twisting_mechanics_entity& _e;
    friend class Bending_twisting_mechanics_entity;
    // extra storage
    mutable Vector _darboux_storage;
    mutable PiecewiseLinearFunction<3> _darboux;
  };

  struct Frame_mismatch_probe : public Probe_otf<RangeVector<6>> {
    using Base = Probe_otf<RangeVector<6>>;
    Frame_mismatch_probe(const Bending_twisting_mechanics_entity &e)
      : Base("mechanics.frame_mismatch"), _e(e) {}

    virtual RangeVector<6> evaluate() const override {
      // mismatch is ( int ( e_i cdot e_j - delta_ij )^2 )^1/2
      RangeVector<6> ret(0.0);

      for( auto&& element : _e.mesh().elements() ) {
	const ElementGeometry geo(_e.oldPosition(), element );
	const double q = geo.q();
	const auto& v_left = element.left();
	const auto& v_right = element.right();

	// get frame
	unsigned ret_count = 0;
	for( unsigned i = 0; i < 3; ++i ) {
	  const auto fi_left = _e.frame(i).evaluate( v_left.index() );
	  const auto fi_right = _e.frame(i).evaluate( v_right.index() );
	  for( unsigned j = 0; j <= i; ++j ) {
	    const auto fj_left = _e.frame(j).evaluate( v_left.index() );
	    const auto fj_right = _e.frame(j).evaluate( v_right.index() );

	    const double val_left = fi_left * fj_left - static_cast<double>( i == j );
	    const double val_right = fi_right * fj_right - static_cast<double>( i == j );

	    const double integral = 0.5 * q * ( val_left*val_left + val_right*val_right );
	    ret[ret_count] += integral;

	    ++ret_count;
	  }
	}
      }

      for( unsigned ret_count = 0; ret_count < 6; ++ret_count ) {
	ret[ret_count] = std::sqrt( ret[ret_count] );
      }
      return ret;
    }

  private:
    const Bending_twisting_mechanics_entity &_e;
    friend class Bending_twisting_mechanics_entity;
  };

  struct Darboux_error_probe : public Probe_otf<RangeVector<3>> {
    using Base = Probe_otf<RangeVector<3>>;
    Darboux_error_probe( const Bending_twisting_mechanics_entity& e )
      : Base("mechanics.darboux_error"), _e(e) {}


    virtual RangeVector<3> evaluate() const final {
      // mismatch is ( 1/ | x_u | e_{j,u} - Omega times e_{j} )^2
      RangeVector<3> ret(0.0);

      for( auto&& element : _e.mesh().elements() ) {
	// geometry
	const ElementGeometry geo( _e.position(), element );
	const double q = geo.q();
	const auto v_left = element.left();
	const auto v_right = element.right();

	// evaluate frames
	const auto e0_left = _e.frame(0).evaluate( v_left.index() );
	const auto e1_left = _e.frame(1).evaluate( v_left.index() );
	const auto e2_left = _e.frame(2).evaluate( v_left.index() );
	const auto e0_right = _e.frame(0).evaluate( v_right.index() );
	const auto e1_right = _e.frame(1).evaluate( v_right.index() );
	const auto e2_right = _e.frame(2).evaluate( v_right.index() );

	// evaluate curvature/twist
	const auto w_left = _e.curvature().evaluate( v_left.index() );
	const auto w_right = _e.curvature().evaluate( v_right.index() );
	const double gamma = _e.twist().evaluate( element.index() );

	// evaluate curvature components
	const double alpha_left = w_left * e1_left;
	const double beta_left = w_left * e2_left;
	const double alpha_right = w_right * e1_right;
	const double beta_right = w_right * e2_right;

	// frame 0 equation (lambda \in (0,1))
	auto error_0 = [&]( const double lambda ) -> double {
			 auto f = ( e0_right - e0_left ) / q -  ( w_left * ( 1 - lambda ) + w_right * lambda );
			 return f*f;
		       };
	// frame 1 equation
	auto error_1 = [&]( const double lambda ) -> double {
			 auto f = ( e1_right - e1_left ) / q - ( ( - alpha_left * e0_left + gamma * e2_left ) * ( 1 - lambda ) + ( - alpha_right * e0_right + gamma * e2_right) * lambda );
			 return f*f;
		       };
	// frame 2 equation
	auto error_2 = [&]( const double lambda ) -> double {
			 auto f = ( e2_right - e2_left ) / q - ( ( - beta_left * e0_left - gamma * e1_left ) * ( 1 - lambda ) + ( - beta_right * e0_right - gamma * e1_right) * lambda );
			 return f*f;
		       };

	// do integral
	ret[0] += q * integrate( error_0 );
	ret[1] += q * integrate( error_1 );
	ret[2] += q * integrate( error_2 );
      }

      return ret;
    }

  private:
    double integrate( const std::function< double(double) >& f ) const {
      const double x_q0 = 0.5 - 1/sqrt(8.0);
      const double x_q1 = 0.5;
      const double x_q2 = 0.5 + 1/sqrt(8.0);

      return 1.0/3.0 * ( f( x_q0 ) + f( x_q1 ) + f( x_q2 ) );
    }

    const Bending_twisting_mechanics_entity &_e;
    friend class Bending_twisting_mechanics_entity;
  };

  using Position_probe = Probe_ptr< const PositionFunctionType >;
  using Bending_moment_probe = Probe_ptr< const BendingMomentFunctionType >;
  using Curvature_probe = Probe_ptr< const CurvatureFunctionType >;
  using Pressure_probe = Probe_ptr< const PressureFunctionType >;
  using Tangent_angular_velocity_probe = Probe_ptr< const TangentAngularVelocityFunctionType >;
  using Twist_probe = Probe_ptr< const TwistFunctionType >;
  using Twisting_moment_probe = Probe_ptr< const TwistingMomentFunctionType >;
  using Frame_probe = Probe_ptr< const FrameFunctionType >;

  virtual std::vector< std::string > optional_ports() const {
    // TODO how to add force in here too?
    return { "control.beta", "control.gamma0" };
  }

private:
  const Mesh& _mesh;
  const TimeProvider& _time_provider;
  std::shared_ptr<Problem> _problem_ptr;

  // solution variables
  SolutionTupleType _solutionTuple;
  SolutionTupleType _oldSolutionTuple;
  SolutionTupleType _rhsTuple;

  // frame variables
  Vector _frame_0_data;
  FrameFunctionType _frame_0;
  Vector _frame_1_data;
  FrameFunctionType _frame_1;
  Vector _frame_2_data;
  FrameFunctionType _frame_2;

  // sub matrix blocks (stencils)
  stencil_dim_3diag<worlddim> _dragstencil;
  stencil_grad<worlddim,1> _presstencil;
  stencil_dim_3diag<worlddim> _pstiffstencil;
  stencil_grad<worlddim,1> _frame_twist_stencil;
  stencil_dim_diag<worlddim> _massstencil;
  stencil_dim_diag<worlddim> _emassstencil;
  stencil_dim_diag<worlddim> _pmassstencil;
  stencil_dim_diag<worlddim> _pmass2stencil;
  stencil_dim_diag<worlddim> _mass2stencil;
  stencil_dim_3diag<worlddim> _stiffstencil;
  stencil_dim_diag<1> _rot_drag_stencil;
  stencil_grad<1,1> _rot_stiff_stencil;
  stencil_div<1,1> _dgamma_stencil;
  stencil_dim_diag_Q<1> _alpha_dt_stencil;
  stencil_div<1, worlddim> _frame_twist_T_stencil;
  stencil_dim_diag_Q<1> _zmassstencil;
  stencil_dim_diag_Q<1> _za_massstencil;
  stencil_dim_diag_Q<1> _zat_massstencil;
  stencil_div< 1, worlddim > _constraint_stencil;
  std::vector< stencil_base* > _all_stencils;

  std::vector< ElementGeometry > _geometry_cache;

  // full matrix
  SystemOperatorType implicitOperator_;
};

#endif
