#pragma once

#include <cmath>

#include "../entity.hh"
#include "../../mesh.hh"
#include "../../timeprovider.hh"
#include "../../discretefunction.hh"

class Feedback_control_entity : public Entity_base {
public:
  static const unsigned int worlddim = 2;
  using BetaFunction = PiecewiseLinearFunction<1>;
  using KappaFunction = PiecewiseLinearFunction<1>;
  using KappaProbe = Probe_otf< const KappaFunction& >;
  using PositionFunction = PiecewiseLinearFunction<worlddim>;
  using PositionProbe = Probe_ptr<const PositionFunction>;
  using StateFunction = PiecewiseLinearFunction<2>;
  using FeedbackFunction = PiecewiseLinearFunction<1>;

  explicit Feedback_control_entity( const std::string name,
				      const Mesh& mesh,
				      const TimeProvider& time_provider )
    : Entity_base( name ),
      _mesh( mesh ),
      _tp( time_provider ),
      // discrete functions
      _beta_storage( _mesh.N() ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ),
      _beta_old_storage( _mesh.N() ),
      _beta_old( mesh, SubArray<Vector>( _beta_old_storage ) ),
      _state_storage( mesh.N()*2 ),
      _state( mesh, SubArray<Vector>( _state_storage ) ),
      _stretch_storage( mesh.N() ),
      _stretch( mesh, SubArray<Vector>( _stretch_storage ) ),
      _feedback_storage( mesh.N() ),
      _feedback( mesh, SubArray<Vector>( _feedback_storage ) ),
      // parameters
      _amplitude( Parameters::get<double>( "control.prescribed.beta_0" ) ),
      _frequency( Parameters::get<double>( "control.prescribed.omega" ) ),
      _wavelength( Parameters::get<double>( "control.prescribed.lambda" ) ),
      _waveform( Parameters::get<std::string>( "control.feedback.waveform" ) ),
      _network_type( Parameters::get<std::string>( "control.feedback.network_type" ) ),
      _feedback_type( Parameters::get<std::string>( "control.feedback.feedback_type" ) ),
      _prop_posterior_range( Parameters::get<double>("control.feedback.posterior_range") ),
      _prop_anterior_range( Parameters::get<double>("control.feedback.anterior_range") ),
      _prop_tau( Parameters::get<double>("control.feedback.tau")*3.3 ),
      _centre( Parameters::get<double>("control.feedback.centre") ),
      _hyst( Parameters::get<double>("control.feedback.hyst") ),
      _hyst2( Parameters::get<double>("control.feedback.hyst2") ),
      _hyst2_start( Parameters::get<double>("control.feedback.hyst2_start") ),
      _AVB_d( Parameters::get<double>("control.feedback.AVB_d") ),
      _AVB_v( Parameters::get<double>("control.feedback.AVB_v") ),
      _inh( Parameters::get<double>("control.feedback.inhibition") ),
      _Don( Parameters::get<double>("control.feedback.Don") ),
      _Doff( Parameters::get<double>("control.feedback.Doff") ),
      _Von( Parameters::get<double>("control.feedback.Von") ),
      _Voff( Parameters::get<double>("control.feedback.Voff") ),
      _VonDon( Parameters::get<double>("control.feedback.VonDon") ),
      _VoffDon( Parameters::get<double>("control.feedback.VoffDon") ),
      _alpha( Parameters::get<double>("control.driven.alpha") ),
      _a( Parameters::get<double>("control.feedback.a")*3.3 ),
      _b( Parameters::get<double>("control.feedback.b" )*3.3 ),
      _threshold_grad( Parameters::get<double>("control.feedback.threshold_grad" ) ),
      // readfile
      _colNum( Parameters::get<int>("control.feedback.readfile.colNum") ),
      _timeShift( Parameters::get<int>("control.feedback.readfile.timeShift") ),
      _timeScale( Parameters::get<int>("control.feedback.readfile.timeScale") ),
      _readfile( Parameters::get<std::string>("control.feedback.readfile") ),
      _readTime(0.0),
       Readfile(NULL)
 { 
#ifdef WORLDDIM
  assert( WORLDDIM==2 );
#endif
     if(_network_type == "readfile"){ 
           Readfile = new std::ifstream(_readfile);
           if(!Readfile->is_open()){std::cerr << "Error: Readfile didn't open" << std::endl;}
         }

    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
    provide_probe( std::make_shared< const StateProbe >( "control.state", &state() ) );
    provide_probe( std::make_shared< const FeedbackProbe >( "control.feedback", &feedback() ) );
    provide_probe( std::make_shared< const NeuralstateProbe >( *this ) );
    provide_probe( std::make_shared< const ActivationProbe >( *this ) );
    initialise_state();
  }

  Feedback_control_entity( const Feedback_control_entity& ) = delete;
 
   void initialise_state(){
	for( auto&& vertex : _mesh.vertices() )
	{
	   _state.assign( vertex.index(), {{ 0, 1 }} );
	}	
   }

  virtual void load() {
    // connect kappa probe
    auto kappa_probe_ptr = see_probe_ptr< KappaProbe >( "mechanics.kappa" );
    assert( kappa_probe_ptr );
    _kappa_probe_ptr = kappa_probe_ptr;

    // connect position probe
    auto position_probe_ptr = see_probe_ptr< PositionProbe >( "mechanics.position" );
    assert( position_probe_ptr );
    _position_probe_ptr = position_probe_ptr;
  }

  virtual void tr_begin() {
    // set initial condition
    for( auto&& v : _mesh.vertices() ) {
      _beta.assign( v.index(), 0.0 );
    }
  }

  virtual void tr_advance() {
    // update the control before solving the mechanics
    // _beta_old.assign( beta );
    for( auto v : _mesh.vertices() ) {
      _beta_old.assign( v.index(),
			_beta.evaluate( v.index() ) );
    }

    // Update network according to type
    if( _feedback_type == "space" ){
	compute_feedback_space();
    } else if( _feedback_type == "spaceTime" ){
	compute_feedback_spaceTime();
    } else if ( _feedback_type == "space_rate" ){
	compute_feedback_space_rate();
    } else if ( _feedback_type == "spaceTime_rate" ){
	compute_feedback_spaceTime_rate();
    } else if ( _feedback_type == "periodic" ){
	compute_feedback_space_periodic();
    }


    // Update network according to type
    if( _network_type == "singleThreshold" ){
	update_states_singleThreshold();
    } else if( _network_type == "reset" ){
	update_states_reset();
    } else if( _network_type == "effectiveReset" ){
	update_states_reset_effective_threshold();
    }else if(_network_type == "driven"){
        update_states_driven();
    } else if(_network_type == "readfile"){
 	update_states_readfile();
    }  
  
    compute_beta();
  }

  virtual std::vector< std::string > essential_ports() const {
    return { "mechanics.position", "mechanics.kappa" };
  }

protected:
  const Mesh& mesh() const { return _mesh; }
  
  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }
  const StateFunction& state() const { return _state; }
  const FeedbackFunction& feedback() const { return _feedback; }

  const KappaFunction& kappa() const {
    // NOTE this rebuild kappa each time it is called so try not to
    // call too often!
    assert( _kappa_probe_ptr );
    return _kappa_probe_ptr->evaluate();
  }

  const PositionFunction& position() const {
    assert( _position_probe_ptr );
    return _position_probe_ptr->evaluate();
  }

//===========================================================
//      TYPES OF PROPRIOCEPTION
//===========================================================

  // do some integration of kappa
  // feedback(u) = 1/range int_range kappa |x_u| du
  // where range = [ max( 0, u + prop_anterior_range ),
  //                 min( 1, u + prop_posterior_range ) ]

  void compute_feedback_space() {
    const auto& my_kappa = kappa();
    const auto& my_position = position();

    // compute integrals element wise
    Vector elementwise_sum( _mesh.N()-1, 0.0 );
    Vector elementwise_range( _mesh.N() - 1, 0.0 );

    for( auto&& element : _mesh.elements() ) {
      const unsigned idx = element.index();
      const unsigned l_idx = element.left().index();
      const unsigned r_idx = element.right().index();

      const double q = (my_position.evaluate( l_idx ) -
			my_position.evaluate( r_idx ))
			   .norm();

      const auto kappa_left = my_kappa.evaluate( l_idx );
      const auto kappa_right = my_kappa.evaluate( r_idx );

      elementwise_sum.at(idx) = 0.5 * q * kappa_left + 0.5 * q * kappa_right;
      elementwise_range.at(idx) = q;
    }

    // determine appropriate ranges to sum over
    // with signed contributions for anterior and posterior feedback
    for( auto&& vertex : _mesh.vertices() ) {
      const double u = vertex.u();
      const double a = u - _prop_anterior_range;
      const double b = u + _prop_posterior_range;

      RangeVector<1> sum(0.0);
      double range = 0.0;
      for( auto&& e : _mesh.elements() ) {
	if( e.left().u() < a or e.right().u() > b ) {
	  continue;
	} else if( a < e.left().u() and e.left().u() < vertex.u() ) {
	  sum -= elementwise_sum.at( e.index() ); // Negative polarity in anterior proprioceptive range
	  range += elementwise_range.at( e.index() );
        } else if( vertex.u() < e.right().u() and e.right().u() < b ) {
	  sum += elementwise_sum.at( e.index() );
	  range += elementwise_range.at( e.index() );
        }
      }
      // ensure range is not zero
      range = std::max( range, 1.0e-10 );
      _feedback.assign( vertex.index(), sum / range );   
    }

  }

  void compute_feedback_space_periodic() {
    const auto& my_kappa = kappa();
    const auto& my_position = position();

    // compute integrals element wise
    Vector elementwise_sum( _mesh.N()-1, 0.0 );
    Vector elementwise_range( _mesh.N() - 1, 0.0 );

    for( auto&& element : _mesh.elements() ) {
      const unsigned idx = element.index();
      const unsigned l_idx = element.left().index();
      const unsigned r_idx = element.right().index();

      const double q = (my_position.evaluate( l_idx ) -
			my_position.evaluate( r_idx ))
			   .norm();

      const auto kappa_left = my_kappa.evaluate( l_idx );
      const auto kappa_right = my_kappa.evaluate( r_idx );

      elementwise_sum.at(idx) = 0.5 * q * kappa_left + 0.5 * q * kappa_right;
      elementwise_range.at(idx) = q;
    }

    // determine appropriate ranges to sum over
    // with signed contributions for anterior and posterior feedback
    for( auto&& vertex : _mesh.vertices() ) {
      const double u = vertex.u();
      const double a = u - _prop_anterior_range; // NOTE: a = u - _prop_anterior_range ?? 
      const double b = u + _prop_posterior_range;
      const double aa= fmod(a, 1.0);
      const double bb= fmod(b, 1.0);

      RangeVector<1> sum(0.0);
      double range = 0.0;
      for( auto&& e : _mesh.elements() ) {
//	// Anterior range
	if( aa < vertex.u() )  { // if ant_prop doesn't extend beyond head
	  if( a < e.left().u() and e.left().u() < vertex.u() ) { 
		sum -= elementwise_sum.at( e.index() );
	  }
	}else if ( vertex.u() < aa ) { // anterior proprioception extends beyond head (periodicity required)
	  if( ( 0.0 < e.left().u()  and e.left().u() < vertex.u() ) or ( (1.0 + a) < e.left().u() and e.left().u() < 1.0 )  ) { 
		sum -= elementwise_sum.at( e.index() );
	  }
	}else{  assert(0 && "Anterior range not fully partitioned");  }
	// Posterior range
	if( vertex.u() < bb ){ // if posterior range doesn't exceed tail
	  if( vertex.u() < e.right().u() and e.right().u() < b  ){
		sum += elementwise_sum.at( e.index() );
	  }
	}else if( bb < vertex.u() ) { // if posterior range exceeds tail (periodicity required)
	  if( ( vertex.u() < e.right().u() and e.right().u() < 1.0 ) or ( 0 < e.right().u() and e.right().u() < (b - 1.0) )  ){
		sum += elementwise_sum.at( e.index() );
	  }
	}else{   assert(0 && "Anterior range not fully partitioned"); } // End posterior range loop
       }
      // define range and assign feedback
       range = _prop_posterior_range - _prop_anterior_range;
      _feedback.assign( vertex.index(), sum / range );   

      }

  }


/* 
 * We now distinguish between 'stretch' and 'proprioceptive feedback'.
 * Stretch:  the present, spatial feedback
 * Prop fb:  how the VNC neurons interpret the stretch e.g. may integrate 
 *           over time, or take spatial or time derivative.
 * In particular, in the standard case from the Signatures paper; 
 * _feedback == stretch, because we only integrated over a region of the
 *  body in the current time step.
 */

  void compute_feedback_spaceTime() {
    const auto& my_kappa = kappa();
    const auto& my_position = position();

    // compute integrals element wise
    Vector elementwise_sum( _mesh.N()-1, 0.0 );
    Vector elementwise_range( _mesh.N() - 1, 0.0 );

    for( auto&& element : _mesh.elements() ) {
      const unsigned idx = element.index();
      const unsigned l_idx = element.left().index();
      const unsigned r_idx = element.right().index();

      const double q = (my_position.evaluate( l_idx ) -
			my_position.evaluate( r_idx ))
			   .norm();

      const auto kappa_left = my_kappa.evaluate( l_idx );
      const auto kappa_right = my_kappa.evaluate( r_idx );

      elementwise_sum.at(idx) = 0.5 * q * kappa_left + 0.5 * q * kappa_right;
      elementwise_range.at(idx) = q;
    }

    // determine appropriate ranges to sum over
    for( auto&& vertex : _mesh.vertices() ) {
      const double u = vertex.u();
      const double a = u - _prop_anterior_range; 
      const double b = u + _prop_posterior_range;

      RangeVector<1> sum(0.0);
      double range = 0.0;
      for( auto&& e : _mesh.elements() ) {
	if( e.left().u() < a or e.right().u() > b ) {
	  continue;
	} else {
	  sum += elementwise_sum.at( e.index() );
	  range += elementwise_range.at( e.index() );
        }
      }
      // ensure range is not zero
      range = std::max( range, 1.0e-10 );

      // Calculate stretch
      double stretch = sum / range;

      // Add temporal decay and perform Euler integration
     double feedback_old = _feedback.evaluate( vertex.index() );
     double feedback_new =  feedback_old + _tp.deltaT()*( stretch - feedback_old ) / _prop_tau ;

      _feedback.assign( vertex.index(), feedback_new );
    }
  }

  void compute_feedback_space_rate() {
    const auto& my_kappa = kappa();
    const auto& my_position = position();

    // compute integrals element wise
    Vector elementwise_sum( _mesh.N()-1, 0.0 );
    Vector elementwise_range( _mesh.N() - 1, 0.0 );

    for( auto&& element : _mesh.elements() ) {
      const unsigned idx = element.index();
      const unsigned l_idx = element.left().index();
      const unsigned r_idx = element.right().index();

      const double q = (my_position.evaluate( l_idx ) -
			my_position.evaluate( r_idx ))
			   .norm();

      const auto kappa_left = my_kappa.evaluate( l_idx );
      const auto kappa_right = my_kappa.evaluate( r_idx );

      elementwise_sum.at(idx) = 0.5 * q * kappa_left + 0.5 * q * kappa_right;
      elementwise_range.at(idx) = q;
    }

    // determine appropriate ranges to sum over
    for( auto&& vertex : _mesh.vertices() ) {
      const double u = vertex.u();
      const double a = u - _prop_anterior_range; 
      const double b = u + _prop_posterior_range;

      RangeVector<1> sum(0.0);
      double range = 0.0;
      for( auto&& e : _mesh.elements() ) {
	if( e.left().u() < a or e.right().u() > b ) {
	  continue;
	} else {
	  sum += elementwise_sum.at( e.index() );
	  range += elementwise_range.at( e.index() );
        }
      }
      // ensure range is not zero
      range = std::max( range, 1.0e-10 );

      // Calculate stretch at it's time derivative
      double stretch = sum / range;
      double stretch_old = _stretch.evaluate( vertex.index() );
      double stretch_dt = ( stretch - stretch_old ) / _tp.deltaT();

      // Assign stretch and feedback as stretch rate
      _stretch.assign( vertex.index(), sum / range );
      _feedback.assign( vertex.index(), stretch_dt );
    }
  }


  void compute_feedback_spaceTime_rate() {
    const auto& my_kappa = kappa();
    const auto& my_position = position();

    // compute integrals element wise
    Vector elementwise_sum( _mesh.N()-1, 0.0 );
    Vector elementwise_range( _mesh.N() - 1, 0.0 );

    for( auto&& element : _mesh.elements() ) {
      const unsigned idx = element.index();
      const unsigned l_idx = element.left().index();
      const unsigned r_idx = element.right().index();

      const double q = (my_position.evaluate( l_idx ) -
			my_position.evaluate( r_idx ))
			   .norm();

      const auto kappa_left = my_kappa.evaluate( l_idx );
      const auto kappa_right = my_kappa.evaluate( r_idx );

      elementwise_sum.at(idx) = 0.5 * q * kappa_left + 0.5 * q * kappa_right;
      elementwise_range.at(idx) = q;
    }

    // determine appropriate ranges to sum over
    for( auto&& vertex : _mesh.vertices() ) {
      const double u = vertex.u();
      const double a = u - _prop_anterior_range;
      const double b = u + _prop_posterior_range;

      RangeVector<1> sum(0.0);
      double range = 0.0;
      for( auto&& e : _mesh.elements() ) {
	if( e.left().u() < a or e.right().u() > b ) {
	  continue;
	} else {
	  sum += elementwise_sum.at( e.index() );
	  range += elementwise_range.at( e.index() );
        }
      }
      // ensure range is not zero
      range = std::max( range, 1.0e-10 );

      // Calculate stretch at it's time derivative
      double stretch = sum / range;
      double stretch_old = _stretch.evaluate( vertex.index() );
      double stretch_dt = ( stretch - stretch_old ) / _tp.deltaT();

      // Add temporal decay and perform Euler integration
     double feedback_old = _feedback.evaluate( vertex.index() );
     double feedback_new =  feedback_old + _tp.deltaT()*( stretch_dt - feedback_old ) / _prop_tau ;

      _stretch.assign( vertex.index(), sum / range );
      _feedback.assign( vertex.index(), feedback_new );
    }
  }


  void update_states_singleThreshold() {

    for( auto&& vertex : _mesh.vertices() ) {
      const RangeVector<2> state = _state.evaluate( vertex.index() );
      const RangeVector<1> feedback = _feedback.evaluate( vertex.index() );
      const double I_stretch_d = -feedback.at(0);
      const double I_d = I_stretch_d;
      const double threshold_max = _centre + _hyst;
      const double linearly_decreasing_threshold =  threshold_max * ( 1.0 - _threshold_grad * vertex.u() );
      const double step_threshold = (vertex.u() < _hyst2_start) ? threshold_max : _hyst2; 
      const double adhoc_thresh = 4.0 + vertex.u() * (4.0 - 4.0);
	if(state.at(0) == 0)
	{
		if(I_d > step_threshold * (1.0 - _threshold_grad * vertex.u()) )  // _threshold_grad=0 by default, so no linear threshold gradient.
		{
  			_state.assign( vertex.index(), {{ 1, 0 }} );
		}
	}
	else
	{
		if(I_d < -step_threshold * (1.0 - _threshold_grad * vertex.u())  )
		{
	  		_state.assign( vertex.index(), {{ 0, 1 }} );
		}

	}
    }
 }


  void update_states_reset() {
    // update the states

    for( auto&& vertex : _mesh.vertices() ) {
      // get the current state
      const RangeVector<2> state = _state.evaluate( vertex.index() );
      // get feedback
      const RangeVector<1> feedback = _feedback.evaluate( vertex.index() );
      // get ventral currents
      const double I_stretch_v = feedback.at(0);
      const double I_AVB_v = _AVB_v ;
      const double I_inhib_v = -_inh * state.at(0);
      const double I_v = I_stretch_v + I_AVB_v + I_inhib_v;
      // get dorsal currents
      const double I_stretch_d = -feedback.at(0);
      const double I_AVB_d = _AVB_d ;
      const double I_inhib_d = 0.0;
      const double I_d = I_stretch_d + I_AVB_d + I_inhib_d;
      // get thresholds
      const double threshold_max = _centre + _hyst;
      const double threshold_min = _centre - _hyst;

      // ventral should have priority NOTE: this is the original
      if( state.at(0) == 0 and state.at(1) == 0 ) {
	// (0,0) -> (0,1)
//	std::cout << "state (0,0) reached "  << std::endl;
	if( I_v > threshold_max ) {
	  _state.assign( vertex.index(), {{ 0, 1 }} );
	} else if( I_d > threshold_max ) {
	  // todo: notify user if this state occurs
	  _state.assign( vertex.index(), {{ 1, 0 }} );
	}
      } else if ( state.at(0) == 0 and state.at(1) == 1 ) {
	// (1,0) -> (1,1)
//	std::cout << "state (0,1) reached  " << std::endl;
	if( I_d > threshold_max ) {
	  _state.assign( vertex.index(), {{ 1, 1 }} );
	} else if ( I_v < threshold_min ) {
	  // todo: notify user if this state occurs
	  _state.assign( vertex.index(), {{ 0, 0 }} );
	}
      } else if ( state.at(0) == 1 and state.at(1) == 1 ) {
	// (1,1) -> (0,1)
//	std::cout << "state (1,1) reached  " << std::endl;
		
	if( I_v < threshold_min ) {
	  _state.assign( vertex.index(), {{ 1, 0 }} );
	} else if( I_d < threshold_min ) {
	  // todo: notify user if this state occurs
	  _state.assign( vertex.index(), {{ 0, 1 }} );
	}
      } else if ( state.at(0) == 1 and state.at(1) == 0 ) {
	// (1,0) -> (0,0)
//	std::cout << "state (1,0) reached  " << std::endl;
	
	if( I_d < threshold_min ) {
	  _state.assign( vertex.index(), {{ 0, 0 }} );
	} else  if ( I_v > threshold_max ) {
	  _state.assign( vertex.index(), {{ 1,1 }} );
	}
      }
    }
 }




  void update_states_reset_effective_threshold() {
    // written in terms of effective thresholds -- good for understanding/debugging the neural circuit
    for( auto&& vertex : _mesh.vertices() ) {
      // get the current state
      const RangeVector<2> state = _state.evaluate( vertex.index() );
      // get feedback
      const RangeVector<1> feedback = _feedback.evaluate( vertex.index() );
      // set effective thresholds (written in ascending order)
      const double Voff = _Voff;
      const double Don  = _Don; 
      const double Von  = _Von; 
      const double Doff = _Doff;
      const double Voff_Don = _VoffDon; 
      const double Von_Don  = _VonDon;  

// Note: "continue" ensures only one state change occurs per vertex per timestep
      // ventral should have priority NOTE: this is the original
      if( state.at(0) == 0 and state.at(1) == 0 ) {
	// (0,0) -> (0,1)
	if( feedback.at(0) > Von  ) {
	  _state.assign( vertex.index(), {{ 0, 1 }} );
	continue;
	} else if( feedback.at(0) < Don ) {
	  _state.assign( vertex.index(), {{ 1, 0 }} );
	continue;
	}
      } else if ( state.at(0) == 0 and state.at(1) == 1 ) {
	// (0,1) -> (1,1)
	if( feedback.at(0) < Don ) {
 	 _state.assign( vertex.index(), {{ 1, 1 }} );
	continue;
	} else if ( feedback.at(0) < Voff ) { 
 	 _state.assign( vertex.index(), {{ 0, 0 }} );
	continue;
	}

      } else if ( state.at(0) == 1 and state.at(1) == 1 ) {
	// (1,1) -> (0,1)
	if( feedback.at(0) < Voff_Don ) {
 	 _state.assign( vertex.index(), {{ 1, 0 }} );
	continue;
	} else if( feedback.at(0) > Doff ) {
	  _state.assign( vertex.index(), {{ 0, 1 }} );
	continue;
	}
      } else if ( state.at(0) == 1 and state.at(1) == 0 ) {
	// (1,0) -> (0,0)	
	if( feedback.at(0) > Doff ) {
  	_state.assign( vertex.index(), {{ 0, 0 }} );
	continue;
	} else  if ( feedback.at(0) > Von_Don ) {
	 _state.assign( vertex.index(), {{ 1,1 }} );
	continue;
	}
      }
    }
 }


  void update_states_driven() {
      const double freq = _frequency*3.3;
      const double wave = _wavelength;
      const double amp  = _amplitude; 

      if(_waveform == "sine"){
        for( auto&& vertex : _mesh.vertices() ) {
            double drive = amp * sin( 2.0*M_PI*vertex.u()/wave - 2.0*M_PI*freq*_tp.time() ) ;
            _state.assign( vertex.index(), {{drive,0}}  );
        }
      }else if(_waveform == "sawtooth"){
        for( auto&& vertex : _mesh.vertices() ) {
	    double saw_dor = fmod( vertex.u()/wave - freq*_tp.time() ,  1  ) ;
	    double saw_ven = fmod( 0.5 + vertex.u()/wave - freq*_tp.time() ,  1  ) ;
            double drive = amp * ( saw_dor - saw_ven ) ; // dorsal - ventral
            _state.assign( vertex.index(), {{drive,0}}  );
        }
     }else if(_waveform == "revsaw"){
        for( auto&& vertex : _mesh.vertices() ) {
	    double revSaw_dor = 1.0 - fmod( vertex.u()/wave - freq*_tp.time() ,  1  ) ;
	    double revSaw_ven = 1.0 - fmod( 0.5 + vertex.u()/wave - freq*_tp.time() ,  1  );
            double drive = amp * ( revSaw_dor - revSaw_ven ) ; // dorsal - ventral
            _state.assign( vertex.index(), {{drive,0}}  );
        }
     }else if(_waveform == "triangle"){
        for( auto&& vertex : _mesh.vertices() ) {
	   double tri_dor = 2.0*std::abs( fmod( vertex.u()/wave - freq*_tp.time(),  1  ) - 1.0/2.0 );
	   double tri_ven = 2.0*std::abs( fmod( vertex.u()/wave - freq*_tp.time() + 0.5,  1  ) - 1.0/2.0 );
           double drive = amp * ( tri_dor - tri_ven ) ; // dorsal - ventral
            _state.assign( vertex.index(), {{drive,0}}  );
        }
     }else if(_waveform == "squared_sine"){ // for agar and water waveforms, this is steeper/more square than a sinewave 
        for( auto&& vertex : _mesh.vertices() ) {
             double sinedrive = amp * sin( 2.0*M_PI*vertex.u()/wave - 2.0*M_PI*freq*_tp.time() ) ;
             double drive = amp * tanh ( _alpha*sinedrive ) / tanh(_alpha) ; // dorsal - ventral
            _state.assign( vertex.index(), {{drive,0}}  );
        }
     }
   }
 
   void update_states_readfile() {
	 _nextCol = 0;
	 _currentCol = -1;      
	const double meshSizeRatio = _mesh.N()/(_colNum - 1); // -1 to discount time column

 	if(_tp.time() > _readTime){ //note: ensure model dt is smaller than data dt
 	  *Readfile >> _readTime;
	   _readTime = (_readTime - _timeShift) / (_timeScale * 3.3);    // make dataset time comparable to model time
 	   for(auto&& vertex : _mesh.vertices() ) {
	    _nextCol = std::floor( ( vertex.index() ) / meshSizeRatio );
		if( _nextCol > _currentCol ){
                  *Readfile >> _kappaValue;
                  _state.assign( vertex.index() , {{10.0*_kappaValue,0}});
		}else{
	          _state.assign( vertex.index() , {{10.0*_kappaValue,0}});
		}
	   _currentCol = _nextCol;
 	   }
 	}
   }

  void compute_beta() {
    // compute beta
    for( auto&& vertex : _mesh.vertices() ) {
      if( vertex.index() == 0 or vertex.index() == _mesh.N()-1 ) {
	_beta.assign( vertex.index(), 0 );
      } else {
	const RangeVector<1> beta_old = _beta_old.eval( vertex.index() );
	const RangeVector<2> state = _state.evaluate( vertex.index() );
	const RangeVector<1> state_diff( state.at(0) - state.at(1) );
	if( _network_type == "readfile" or _network_type == "driven" ){
		const RangeVector<1> new_beta = state_diff;
		_beta.assign( vertex.index(),  new_beta  );
	}else{
		const RangeVector<1> new_beta = beta_old + _tp.deltaT() * (_a * state_diff - _b * beta_old);
		_beta.assign( vertex.index(), new_beta ) ;
	}
      }
    }
  }

public:
  /**
   *  Probes
   * @{
   */
  using BetaProbe = Probe_ptr<const BetaFunction>;
  using FeedbackProbe = Probe_ptr<const FeedbackFunction>;
  using StateProbe = Probe_ptr<const StateFunction>;

  struct ActivationProbe : public Probe_otf< const PiecewiseLinearFunction<1>& > {
    using T = const PiecewiseLinearFunction<1>&;
    using Base = Probe_otf<T>;

    ActivationProbe( const Feedback_control_entity& e )
      : Base("control.activation"), _e( e ),
	_activation_storage( _e.mesh().N() ),
	_activation( _e.mesh(), SubArray<Vector>( _activation_storage ) ) {}

    T evaluate() const {
      _activation_storage.clear();
      const auto& state = _e.state();

      for( auto&& v : _e.mesh().vertices() ) {
	const auto my_state = state.evaluate( v.index() );
	_activation.assign( v.index(), my_state.at(0) - my_state.at(1) );
      }

      return _activation;
    }

  private:
    friend class Feedback_control_entity;
    const Feedback_control_entity& _e;

    mutable Vector _activation_storage;
    mutable PiecewiseLinearFunction<1> _activation;
  };

  struct NeuralstateProbe : public Probe_otf< const PiecewiseLinearFunction<1>& > {
    using T = const PiecewiseLinearFunction<1>&;
    using Base = Probe_otf<T>;

    NeuralstateProbe( const Feedback_control_entity& e )
      : Base("control.neuralstate"), _e( e ),
	_neuralstate_storage( _e.mesh().N() ),
	_neuralstate( _e.mesh(), SubArray<Vector>( _neuralstate_storage ) ) {}

    T evaluate() const {
      _neuralstate_storage.clear();
      const auto& state = _e.state();

      for( auto&& v : _e.mesh().vertices() ) {
	const auto my_state = state.evaluate( v.index() );

	if( my_state.at(0) == 0 and my_state.at(1) == 0 ) {
	  _neuralstate.assign( v.index(), 1.0 );
	} else if ( my_state.at(0) == 1 and my_state.at(1) == 0 ) {
	  _neuralstate.assign( v.index(), 2.0 );
	} else if ( my_state.at(0) == 1 and my_state.at(1) == 1 ) {
	  _neuralstate.assign( v.index(), 3.0 );
	} else if ( my_state.at(0) == 0 and my_state.at(1) == 1 ) {
	  _neuralstate.assign( v.index(), 4.0 );
	} else {
	  throw exception_invalid( "neural state" );
	}
      }

      return _neuralstate;
    }

  private:
    friend class Feedback_control_entity;
    const Feedback_control_entity& _e;

    mutable Vector _neuralstate_storage;
    mutable PiecewiseLinearFunction<1> _neuralstate;
  };

  /**
   *  @}
   */

private:
  const Mesh& _mesh;
  const TimeProvider& _tp;

  Vector _beta_storage;
  BetaFunction _beta;
  Vector _beta_old_storage;
  BetaFunction _beta_old;
  Vector _state_storage;
  StateFunction _state;
  Vector _stretch_storage;
  FeedbackFunction _stretch;
  Vector _feedback_storage;
  FeedbackFunction _feedback;

  // parameters
  const std::string _waveform;
  const std::string _network_type;
  const std::string _feedback_type;
  const double _prop_posterior_range;
  const double _prop_anterior_range;
  const double _prop_tau;
  const double _centre;
  const double _hyst;
  const double _hyst2;
  const double _hyst2_start;
  const double _AVB_d;
  const double _AVB_v;
  const double _inh;
  const double _a;
  const double _b;
  const double _threshold_grad;

  // effective reset parameters
  const double _Don;
  const double _Doff;
  const double _Von;
  const double _Voff;
  const double _VonDon;
  const double _VoffDon;

  // driven
  const double _amplitude;
  const double _frequency;
  const double _wavelength;
  const double _alpha;

 // readfile
  double _kappaValue;
  int _nextCol;
  int _currentCol;
  const double _colNum;
  const double _timeShift;
  const double _timeScale;
  const std::string _readfile;
  double _readTime;
  mutable std::ifstream* Readfile;

  // external probes
  std::shared_ptr<const KappaProbe> _kappa_probe_ptr;
  std::shared_ptr<const PositionProbe> _position_probe_ptr;
};
