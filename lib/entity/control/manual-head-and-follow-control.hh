#pragma once

#include <cmath>

#include "../entity.hh"
#include "../../mesh.hh"
#include "../../timeprovider.hh"
#include "../../discretefunction.hh"

#ifndef WORLDDIM
#define WORLDDIM 2
#endif

/**
 *  Manual_Head_And_Follow_control_entity
 *
 *  Prescribes a muscle torque through functions.
 */
class Manual_Head_And_Follow_control_entity : public Entity_base {
  static const unsigned worlddim = WORLDDIM;
public:
  static const unsigned betadim = worlddim-1;
  using BetaFunction = PiecewiseLinearFunction<betadim>;
  using BetaProbe = Probe_ptr<const BetaFunction>;

  using Steer_probe = Probe_ptr<const double>;

  explicit Manual_Head_And_Follow_control_entity( const std::string name,
				      const Mesh& mesh,
				      const TimeProvider& time_provider )
    : Entity_base( name ),
      _mesh( mesh ),
      _tp( time_provider ),
      _beta_storage( _mesh.N() * betadim ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ),
      _wavespeed( Parameters::get<double>("control.manual_head_and_follow.wavespeed")) {
    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
  }

  Manual_Head_And_Follow_control_entity( const Manual_Head_And_Follow_control_entity& ) = delete;

  void load() {
    // connect manual_head_and_follow probe
    auto steer_probe_ptr = see_probe_ptr< Steer_probe >( "opengl.steer");
    assert( steer_probe_ptr );
    _steer_probe_ptr = steer_probe_ptr;
  }

  void tr_begin() {
    update_beta();
  }

  void tr_advance() {
    update_beta();
  }

  virtual std::vector< std::string > essential_ports() const {
    return { "opengl.steer" };
  }

protected:
  void update_beta() {
    for( auto&& vertex : _mesh.vertices() ) {
      const unsigned idx = vertex.index();
      const double u = vertex.u();

      // do nothing at head
      if( idx == 0 ) {
	continue;
      }

      // otherwise we want beta( u - wavespeed deltaT )
      const double eval_u = u - _wavespeed * _tp.deltaT();
      for( auto&& element : _mesh.elements() ) {
	// find element with eval_u in
	if( element.left().u() < eval_u and element.right().u() > eval_u ) {
	  // interpolate beta at ends of element
	  const double u_L = element.left().u();
	  const double u_R = element.right().u();
	  const auto beta_L = beta().evaluate( element.left().index() );
	  const auto beta_R = beta().evaluate( element.right().index() );

	  const auto beta_ret = beta_L * ( eval_u - u_R ) / ( u_L - u_R )
	    + beta_R * ( eval_u - u_L ) / ( u_R - u_L );
	  // assign to beta
	  beta().assign( idx, beta_ret );
	  break;
	}
      }
    }

    // assign head from steer
    RangeVector<betadim> b;
    b.at(0) = steer();
    beta().assign( 0, b );
  }

  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }

  double steer() const {
    assert( _steer_probe_ptr );
    return _steer_probe_ptr->evaluate();
  }

private:
  const Mesh& _mesh;
  const TimeProvider& _tp;

  Vector _beta_storage;
  BetaFunction _beta;

  const double _wavespeed;

  // external probe
  std::shared_ptr< const Steer_probe > _steer_probe_ptr;
};
