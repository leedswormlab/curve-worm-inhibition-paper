#pragma once

#include <cmath>

#include "../entity.hh"
#include "../../mesh.hh"
#include "../../timeprovider.hh"
#include "../../discretefunction.hh"

/**
 *  Feedforward_control_entity
 *
 *  Todo implement me.
 */
class Feedforward_control_entity : public Entity_base {
public:
  using BetaFunction = PiecewiseLinearFunction<1>;
  using BetaProbe = Probe_ptr<const BetaFunction>;

  explicit Feedforward_control_entity( const std::string name,
				      const Mesh& mesh,
				      const TimeProvider& time_provider )
    : Entity_base( name ),
      _mesh( mesh ),
      _tp( time_provider ),
      _beta_storage( _mesh.N() ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ) {
    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
    assert(0);
  }

  Feedforward_control_entity( const Feedforward_control_entity& ) = delete;

protected:
  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }

private:
  const Mesh& _mesh;
  const TimeProvider& _tp;

  Vector _beta_storage;
  BetaFunction _beta;
};
