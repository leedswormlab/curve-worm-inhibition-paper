#pragma once

#include <cmath>

#include "../entity.hh"
#include "../../mesh.hh"
#include "../../timeprovider.hh"
#include "../../discretefunction.hh"

#ifndef WORLDDIM
#define WORLDDIM 2
#endif

/**
 *  Feedforward_head_control_entity
 *
 *  Prescribes a muscle torque in the head through additional functions,...
 *  Body torque assumed to be identical to that of prescribed control entity.
 */
class Feedforward_head_control_entity : public Entity_base {
  static const unsigned worlddim = WORLDDIM;
public:
  static const unsigned betadim = worlddim-1;
  using BetaFunction = PiecewiseLinearFunction<betadim>;
  using BetaProbe = Probe_ptr<const BetaFunction>;

  explicit Feedforward_head_control_entity( const std::string name,
				      const Mesh& mesh,
				      const TimeProvider& time_provider )
    : Entity_base( name ),
      _mesh( mesh ),
      _tp( time_provider ),
      _beta_storage( _mesh.N() * betadim ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ),
      _amplitude( Parameters::get<double>("control.feedforward.head.body.amplitude") ),
      _wavelength( Parameters::get<double>("control.feedforward.head.body.wavelength") ),
      _frequency( Parameters::get<double>("control.feedforward.head.body.frequency") ) ,
      _body_anterior( Parameters::get<double>("control.feedforward.head.body.anterior") ) ,
      _body_posterior( Parameters::get<double>("control.feedforward.head.body.posterior") ) ,
      _SMDD_amplitude( Parameters::get<double>("control.feedforward.head.SMDD.amplitude") ),
      _SMDD_anterior( Parameters::get<double>("control.feedforward.head.SMDD.anterior") ),
      _SMDD_posterior( Parameters::get<double>("control.feedforward.head.SMDD.posterior") ),
      _SMDD_phase( Parameters::get<double>("control.feedforward.head.SMDD.phase") ),
      _SMDV_amplitude( Parameters::get<double>("control.feedforward.head.SMDV.amplitude") ),
      _SMDV_anterior( Parameters::get<double>("control.feedforward.head.SMDV.anterior") ),
      _SMDV_posterior( Parameters::get<double>("control.feedforward.head.SMDV.posterior") ),
      _SMDV_phase( Parameters::get<double>("control.feedforward.head.SMDV.phase") )
  {
    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
  }

  Feedforward_head_control_entity( const Feedforward_head_control_entity& ) = delete;

  void tr_begin() {
    // interpolate beta
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b(0.0);
      b.at(0) = beta_cts( vertex, _tp.time() );
#if WORLDDIM == 3
      b.at(1) = beta_cts2( vertex, _tp.time() );
#endif
      beta().assign( vertex.index(), b );
    }
  }

/* // Old tr_advance -- before options to select neurons
  void tr_advance() {
    // interpolate beta
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b(0.0);
      b.at(0) = beta_cts( vertex, _tp.time() + _tp.deltaT())
                + 
                beta_SMDD( vertex, _tp.time() + _tp.deltaT() );
#if WORLDDIM == 3
      b.at(1) = beta_cts2( vertex, _tp.time() + _tp.deltaT() );
#endif
      beta().assign( vertex.index(), b );
    }
  }
*/

//==============================================================================
  void tr_advance() {
    // interpolate beta
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b(0.0);

	// Sum contributions from all requested head neurons
	for (auto &headNeuron : Parameters::getStringVector("includeNeurons") ) {

	    if(headNeuron == "body"){
	    b.at(0) += beta_cts( vertex, _tp.time() + _tp.deltaT());
            } else if(headNeuron == "SMDD"){
	    b.at(0) += beta_SMDD( vertex, _tp.time() + _tp.deltaT() );
	    } else if(headNeuron == "SMDV") {
	    b.at(0) += beta_SMDV( vertex, _tp.time() + _tp.deltaT() );
	    }
	    else{
		// No more neurons requested
	    }
	}
#if WORLDDIM == 3
      b.at(1) = beta_cts2( vertex, _tp.time() + _tp.deltaT() );
#endif
      beta().assign( vertex.index(), b );
    }
  }
//==============================================================================




protected:
  // NOTE when comparing to old implementations use
  // time = _tp.time() + _tp.deltaT();
 // double beta_cts(const typename Mesh::Vertex &v, const double time ) const {
 //   return -( _beta_0 * ( 1 - v.u() ) + _beta_1 * v.u() )
 //     * sin(2.0 * M_PI * v.u() / _lambda - 2.0 * M_PI * _omega * time );
 // }
  double beta_cts(const typename Mesh::Vertex &v, const double time) const {
     double output = 0.0;
     const double body_anterior  = _body_anterior;
     const double body_posterior = _body_posterior;
     if(v.u() <= body_anterior or v.u() >= body_posterior){
	output = 0.0;
     }else{
        output = _amplitude*sin(2.0 * M_PI * v.u()/_wavelength - 2.0 * M_PI * _frequency * time ); 
     }   
     return output; 
  } 
  double beta_cts2( const typename Mesh::Vertex& /* v */, const double /*time*/) const {
    return 0.0;
  }
  double beta_SMDD(const typename Mesh::Vertex &v, const double time) const {
     double output = 0.0;
     const double SMDD_amplitude = _SMDD_amplitude;
     const double SMDD_anterior  = _SMDD_anterior;
     const double SMDD_posterior = _SMDD_posterior;
     const double SMDD_phase     = _SMDD_phase;
     if(v.u() <= SMDD_anterior or v.u() >= SMDD_posterior){
	output = 0.0;
     }else{
        output =  SMDD_amplitude*positive_sin(2.0 * M_PI * v.u()/_wavelength - 2.0 * M_PI * (_frequency * time - SMDD_phase) ); // Note: could have neuron info come from a friend class? that way e.g. SMDD.V.Amplitude() 
     }										    			     // Note: Need a flatSine(double arg) function, which is zero if arg<0     
     return output; 
  }
  double beta_SMDV(const typename Mesh::Vertex &v, const double time) const {
     double output = 0.0;
     const double SMDV_amplitude = _SMDV_amplitude;
     const double SMDV_anterior  = _SMDV_anterior;
     const double SMDV_posterior = _SMDV_posterior;
     const double SMDV_phase     = _SMDV_phase;
     if(v.u() <= SMDV_anterior or v.u() >= SMDV_posterior){
	output = 0.0;
     }else{
        output =  -SMDV_amplitude*positive_sin(2.0 * M_PI * v.u()/_wavelength - 2.0 * M_PI * (_frequency * time - SMDV_phase) ); // Note: could have neuron info come from a friend class? that way e.g. SMDD.V.Amplitude() 
     }										    			     // Note: Need a flatSine(double arg) function, which is zero if arg<0     
     return output; 
  }



  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }

  double positive_sin( const double ss ) const{
     double output = 0.0;
     if(sin(ss) > 0.0 ){ output = sin(ss); }
     return output;
  }

private:
  const Mesh& _mesh;
  const TimeProvider& _tp;

  Vector _beta_storage;
  BetaFunction _beta;

  // Neuron parameters
  const double _amplitude;
  const double _wavelength;
  const double _frequency;
  const double _body_anterior;
  const double _body_posterior;
  const double _SMDD_amplitude;
  const double _SMDD_anterior;
  const double _SMDD_posterior;
  const double _SMDD_phase;
  const double _SMDV_amplitude;
  const double _SMDV_anterior;
  const double _SMDV_posterior;
  const double _SMDV_phase;


};

// Notes about sign conventions
// +ve beta is either: Excitation of dorsal  muscle
// 		       Inhibition of ventral muscle
// -ve beta is either: Excitation of ventral muscle
// 		       Inhibition of dorsal  muscle
// Todo: Should exc. dorsal and inh. ventral be identical? 
