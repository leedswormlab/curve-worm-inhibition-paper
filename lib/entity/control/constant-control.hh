#pragma once

#include <cmath>

#include "../entity.hh"
#include "../../mesh.hh"
#include "../../timeprovider.hh"
#include "../../discretefunction.hh"

/**
 *  Constant_control_entity<2>
 *
 *  Prescribes a muscle torque through constant functions.
 */
#if WORLDDIM == 2
class Constant_control_entity : public Entity_base {
  static const unsigned worlddim = 2;
public:
  static const unsigned betadim = worlddim - 1;
  using BetaFunction = PiecewiseLinearFunction<betadim>;
  using BetaProbe = Probe_ptr<const BetaFunction>;

  explicit Constant_control_entity( const std::string name,
				    const Mesh& mesh,
				    const TimeProvider& /*time_provider*/ )
    : Entity_base( name ),
      _mesh( mesh ),
      _beta_storage( _mesh.N() * betadim ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ),
      _beta_0( Parameters::get<double>("control.constant.beta_0") ) {
    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
  }

  Constant_control_entity( const Constant_control_entity& ) = delete;

  void tr_begin() {
    // interpolate beta
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b;
      b.at(0) = _beta_0;
      beta().assign( vertex.index(), b );
    }
  }

protected:
  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }

private:
  const Mesh& _mesh;

  Vector _beta_storage;
  BetaFunction _beta;
  const double _beta_0;
};
#endif


/**
 *  Constant_control_entity<3>
 *
 *  Prescribes a muscle torque through constant functions.
 */
#if WORLDDIM == 3
class Constant_control_entity : public Entity_base {
  static const unsigned worlddim = 3;
public:
  static const unsigned betadim = worlddim - 1;
  using BetaFunction = PiecewiseLinearFunction<betadim>;
  using BetaProbe = Probe_ptr<const BetaFunction>;

  using Gamma0Function = PiecewiseConstantFunction<1>;
  using Gamma0Probe = Probe_ptr<const Gamma0Function>;

  explicit Constant_control_entity( const std::string name,
				    const Mesh& mesh,
				    const TimeProvider& /*time_provider*/ )
    : Entity_base( name ),
      _mesh( mesh ),
      _beta_storage( _mesh.N() * betadim ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ),
      _gamma0_storage( _mesh.N() - 1 ),
      _gamma0( mesh, SubArray<Vector>( _gamma0_storage ) ),
      _test_case( Parameters::get<int>("control.constant.test_case") ),
      _beta_0( Parameters::get<double>("control.constant.beta_0") ),
      _beta_1( Parameters::get<double>("control.constant.beta_1") ),
      _gamma0_value( Parameters::get<double>("control.constant.gamma0") ) {
    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
    provide_probe( std::make_shared< const Gamma0Probe >( "control.gamma0", &gamma0() ) );
  }

  Constant_control_entity( const Constant_control_entity& ) = delete;

  void tr_begin() {
    // interpolate beta
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b;
      b.at(0) = _beta_0;
      if( _test_case == 1 )
	b.at(0) *= sin( 1.5 * M_PI * vertex.u() );
      b.at(1) = _beta_1;
      if( _test_case == 1 )
	b.at(1) *= cos( 1.5 * M_PI * vertex.u() );
      beta().assign( vertex.index(), b );
    }
    // interpolate gamma
    for( auto&& element : _mesh.elements() ) {
      RangeVector<1> a( _gamma0_value );
      if( _test_case == 1 ) {
	const double u = 0.5 * ( element.left().u() + element.right().u() );
	a *= cos( M_PI * u );
      }
      gamma0().assign( element.index(), a );
    }
  }

protected:
  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }
  const Gamma0Function& gamma0() const { return _gamma0; }
  Gamma0Function& gamma0() { return _gamma0; }

private:
  const Mesh& _mesh;

  Vector _beta_storage;
  BetaFunction _beta;
  Vector _gamma0_storage;
  Gamma0Function _gamma0;

  const int _test_case;
  const double _beta_0;
  const double _beta_1;
  const double _gamma0_value;
};
#endif
