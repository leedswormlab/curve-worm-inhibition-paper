#pragma once

#include <cmath>

#include "../entity.hh"
#include "../../mesh.hh"
#include "../../timeprovider.hh"
#include "../../discretefunction.hh"

/**
 *  Roller_control_entity
 *
 *  Prescribes a muscle torque through constant functions.
 */
class Roller_control_entity : public Entity_base {
  static const unsigned worlddim = 3;
public:
  static const unsigned betadim = worlddim - 1;
  using BetaFunction = PiecewiseLinearFunction<betadim>;
  using BetaProbe = Probe_ptr<const BetaFunction>;

  using Gamma0Function = PiecewiseConstantFunction<1>;
  using Gamma0Probe = Probe_ptr<const Gamma0Function>;

  explicit Roller_control_entity( const std::string name,
				    const Mesh& mesh,
				    const TimeProvider& time_provider )
    : Entity_base( name ),
      _mesh( mesh ),
      _tp( time_provider ),
      _beta_storage( _mesh.N() * betadim ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ),
      _gamma0_storage( _mesh.N() - 1 ),
      _gamma0( mesh, SubArray<Vector>( _gamma0_storage ) ),
      _beta_0( Parameters::get<double>("control.roller.beta_0") ),
      _beta_1( Parameters::get<double>("control.roller.beta_1") ),
      _lambda( Parameters::get<double>("control.roller.lambda") ),
      _omega( Parameters::get<double>("control.roller.omega") ),
      _gamma0_value( Parameters::get<double>("control.roller.gamma0") ) {
    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
    provide_probe( std::make_shared< const Gamma0Probe >( "control.gamma0", &gamma0() ) );
  }

  Roller_control_entity( const Roller_control_entity& ) = delete;

  void tr_begin() {
    // interpolate beta
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b;
      b.at(0) = beta_cts( vertex, _tp.time() );
      b.at(1) = 0.0;
      beta().assign( vertex.index(), b );
    }
    // interpolate gamma
    for( auto&& element : _mesh.elements() ) {
      const RangeVector<1> a( _gamma0_value );
      gamma0().assign( element.index(), a );
    }
  }

  void tr_advance() {
    // interpolate beta
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b;
      b.at(0) = beta_cts( vertex, _tp.time() );
      b.at(1) = 0.0;
      beta().assign( vertex.index(), b );
    }
    // interpolate gamma
    for( auto&& element : _mesh.elements() ) {
      const RangeVector<1> a( _gamma0_value );
      gamma0().assign( element.index(), a );
    }
  }

protected:
  double beta_cts(const typename Mesh::Vertex &v, const double time ) const {
    return -( _beta_0 * ( 1 - v.u() ) + _beta_1 * v.u() )
      * sin(2.0 * M_PI * v.u() / _lambda - 2.0 * M_PI * _omega * time );
  }

  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }
  const Gamma0Function& gamma0() const { return _gamma0; }
  Gamma0Function& gamma0() { return _gamma0; }

private:
  const Mesh& _mesh;
  const TimeProvider& _tp;

  Vector _beta_storage;
  BetaFunction _beta;
  Vector _gamma0_storage;
  Gamma0Function _gamma0;

  const double _beta_0;
  const double _beta_1;
  const double _lambda;
  const double _omega;
  const double _gamma0_value;
};
