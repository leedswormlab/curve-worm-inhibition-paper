#pragma once

#include <cmath>

#include "../entity.hh"
#include "../../mesh.hh"
#include "../../timeprovider.hh"
#include "../../discretefunction.hh"

#ifndef WORLDDIM
#define WORLDDIM 2
#endif

/**
 *  Prescribed_control_entity
 *
 *  Prescribes a muscle torque through functions.
 */
class Prescribed_control_entity : public Entity_base {
  static const unsigned worlddim = WORLDDIM;
public:
  static const unsigned betadim = worlddim-1;
  using BetaFunction = PiecewiseLinearFunction<betadim>;
  using BetaProbe = Probe_ptr<const BetaFunction>;

  explicit Prescribed_control_entity( const std::string name,
				      const Mesh& mesh,
				      const TimeProvider& time_provider )
    : Entity_base( name ),
      _mesh( mesh ),
      _tp( time_provider ),
      _beta_storage( _mesh.N() * betadim ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ),
      _burn_in_time( Parameters::get<double>("control.prescribed.burn_in_time",0.0 ) ),
      _beta_0( Parameters::get<double>("control.prescribed.beta_0") ),
      _beta_1( Parameters::get<double>("control.prescribed.beta_1") ),
#if WORLDDIM == 3
      _beta_perp( Parameters::get<double>("control.prescribed.beta_perp",0.0) ),
#endif
      _lambda( Parameters::get<double>("control.prescribed.lambda") ),
      _omega( Parameters::get<double>("control.prescribed.omega") ) {
    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
  }

  Prescribed_control_entity( const Prescribed_control_entity& ) = delete;

  void tr_begin() {
    // interpolate beta
    _begin_time = _tp.time();
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b(0.0);
      b.at(0) = beta_cts( vertex, _tp.time() );
#if WORLDDIM == 3
      b.at(1) = beta_cts2( vertex, _tp.time() );
#endif
      beta().assign( vertex.index(), b );
    }
  }

  void tr_advance() {
    // interpolate beta
    const double time =  std::max( _tp.time() + _tp.deltaT() - _burn_in_time, _begin_time );
    for( auto&& vertex : _mesh.vertices() ) {
      RangeVector<betadim> b(0.0);
      b.at(0) = beta_cts( vertex, time );
#if WORLDDIM == 3
      b.at(1) = beta_cts2( vertex, time );
#endif
      beta().assign( vertex.index(), b );
    }
  }

protected:
  // NOTE when comparing to old implementations use
  // time = _tp.time() + _tp.deltaT();
  double beta_cts(const typename Mesh::Vertex &v, const double time ) const {
    return ( _beta_0 * ( 1 - v.u() ) + _beta_1 * v.u() )
      * sin(2.0 * M_PI * v.u() / _lambda - 2.0 * M_PI * _omega * time );
  }
#if WORLDDIM == 3
  double beta_cts2( const typename Mesh::Vertex& v, const double time) const {
    if( v.u() < 1.0/3.0 )
      return _beta_perp;
    else
      return 0;
  }
#endif

  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }

private:
  const Mesh& _mesh;
  const TimeProvider& _tp;

  Vector _beta_storage;
  BetaFunction _beta;

  const double _burn_in_time;
  double _begin_time;

  const double _beta_0;
  const double _beta_1;
#if WORLDDIM == 3
  const double _beta_perp;
#endif
  const double _lambda;
  const double _omega;
};
