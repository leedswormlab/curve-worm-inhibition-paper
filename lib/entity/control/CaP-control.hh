#pragma once

#include <cmath>

#include "../entity.hh"
#include "../../mesh.hh"
#include "../../timeprovider.hh"
#include "../../discretefunction.hh"

/**
 *  CaP_control_entity    (CaP = "CPG and Proprioception")
 *
 *  Todo work out how to do world dim properly
 */
class CaP_control_entity : public Entity_base {
public:
  static const unsigned int worlddim = 2;
  using BetaFunction = PiecewiseLinearFunction<1>;
  using KappaFunction = PiecewiseLinearFunction<1>;
  using KappaProbe = Probe_otf< const KappaFunction& >;
  using PositionFunction = PiecewiseLinearFunction<worlddim>;
  using PositionProbe = Probe_ptr<const PositionFunction>;
  using StateFunction = PiecewiseLinearFunction<2>;
  using FeedbackFunction = PiecewiseLinearFunction<1>;
  using PhaseOscFunction = PiecewiseLinearFunction<1>;

  explicit CaP_control_entity( const std::string name,
				      const Mesh& mesh,
				      const TimeProvider& time_provider )
    : Entity_base( name ),
      _mesh( mesh ),
      _tp( time_provider ),
      // discrete functions
      _beta_storage( _mesh.N() ),
      _beta( mesh, SubArray<Vector>( _beta_storage ) ),
      _beta_old_storage( _mesh.N() ),
      _beta_old( mesh, SubArray<Vector>( _beta_old_storage ) ),
      _state_storage( mesh.N()*2 ),
      _state( mesh, SubArray<Vector>( _state_storage ) ),
      _feedback_storage( mesh.N() ),
      _feedback( mesh, SubArray<Vector>( _phaseOsc_storage ) ),
      _phaseOsc_storage( mesh.N() ),
      _phaseOsc( mesh, SubArray<Vector>( _phaseOsc_storage ) ),
      // parameters
      _network_type( Parameters::get<std::string>( "control.feedback.network_type" ) ),
      _CPG_type( Parameters::get<std::string>( "control.CaP.CPG_type" ) ),
      _prop_posterior_range( Parameters::get<double>("control.feedback.posterior_range") ),
      _prop_anterior_range( Parameters::get<double>("control.feedback.anterior_range") ),
      _prop_tau( Parameters::get<double>("control.feedback.tau") ),
      _centre( Parameters::get<double>("control.feedback.centre") ),
      _hyst( Parameters::get<double>("control.feedback.hyst") ),
      _AVB_d( Parameters::get<double>("control.feedback.AVB_d") ),
      _AVB_v( Parameters::get<double>("control.feedback.AVB_v") ),
      _inh( Parameters::get<double>("control.feedback.inhibition") ),
      _a( Parameters::get<double>("control.feedback.a")*3.3 ),
      _b( Parameters::get<double>("control.feedback.b" )*3.3 ),
      _readTime(0),
       Readfile(NULL)
 { 
#ifdef WORLDDIM
  assert( WORLDDIM==2 );
#endif
     if(_network_type == "readfile"){ 
           Readfile = new std::ifstream(_readfile);
           if(!Readfile->is_open()){std::cerr << "Error: Readfile didn't open" << std::endl;}
         }

    provide_probe( std::make_shared< const BetaProbe >( "control.beta", &beta() ) );
    provide_probe( std::make_shared< const StateProbe >( "control.state", &state() ) );
    provide_probe( std::make_shared< const FeedbackProbe >( "control.feedback", &feedback() ) );
    provide_probe( std::make_shared< const NeuralstateProbe >( *this ) );
    provide_probe( std::make_shared< const ActivationProbe >( *this ) );
    initialise_state();
  }

  CaP_control_entity( const CaP_control_entity& ) = delete;
 
   void initialise_state(){
	for( auto&& vertex : _mesh.vertices() )
	{
	   _state.assign( vertex.index(), {{ 0, 1 }} ); // { 0, 1 }
	}	
   }

  virtual void load() {
    // connect kappa probe
    auto kappa_probe_ptr = see_probe_ptr< KappaProbe >( "mechanics.kappa" );
    assert( kappa_probe_ptr );
    _kappa_probe_ptr = kappa_probe_ptr;

    // connect position probe
    auto position_probe_ptr = see_probe_ptr< PositionProbe >( "mechanics.position" );
    assert( position_probe_ptr );
    _position_probe_ptr = position_probe_ptr;
  }

  virtual void tr_begin() {
    // set initial condition
    for( auto&& v : _mesh.vertices() ) {
      _beta.assign( v.index(), 0.0 );
    }
  }

  virtual void tr_advance() {
    // update the control before solving the mechanics
    // _beta_old.assign( beta );
    for( auto v : _mesh.vertices() ) {
      _beta_old.assign( v.index(),
			_beta.evaluate( v.index() ) );
    }

    compute_feedback();
  // Update phase oscillator model once per timestep, even if it isn't used
   compute_phaseOsc();


    if(_prop_tau > 1e-10)
    {
	prop_time_integration();
    }

    // do thresholding (FF updates called from FB updates)
    if( _network_type == "singleThreshold" ){
	update_states_singleThreshold();
    } else if( _network_type == "reset" ){
	update_states_reset();
    } else if( _network_type == "effectiveReset" ){
	update_states_reset_effective_threshold();
    }
  

    compute_beta();
  }

  virtual std::vector< std::string > essential_ports() const {
    return { "mechanics.position", "mechanics.kappa" };
  }


 double advance_FF( Mesh::Vertex vertex ){ // Mesh::Vertex vertex    // double vertex_u
    double out;    
      if( _CPG_type == "phaseOsc" ){
	  out = sin(  _phaseOsc.evaluate( vertex.index() ) ); // Take sin() of the phase 
      } 
//	else if(_CPG_type == "sine")
//          out = update_states_sine( vertex.u() );
//      } 
	else if(_CPG_type == "readfile"){
 	  out = update_states_readfile(); // TODO add vertex
      } 
    return out;
  }

protected:
  const Mesh& mesh() const { return _mesh; }
  
  const BetaFunction& beta() const { return _beta; }
  BetaFunction& beta() { return _beta; }
  const StateFunction& state() const { return _state; }
  const FeedbackFunction& feedback() const { return _feedback; }
  const PhaseOscFunction& phaseOsc() const { return _phaseOsc; }

  const KappaFunction& kappa() const {
    // NOTE this rebuild kappa each time it is called so try not to
    // call too often!
    assert( _kappa_probe_ptr );
    return _kappa_probe_ptr->evaluate();
  }

  const PositionFunction& position() const {
    assert( _position_probe_ptr );
    return _position_probe_ptr->evaluate();
  }

  void compute_feedback() {
    const auto& my_kappa = kappa();
    const auto& my_position = position();

    // do some integration of kappa
    // feedback(u) = 1/range int_range kappa |x_u| du
    // where range = [ max( 0, u + prop_anterior_range ),
    //                 min( 1, u + prop_posterior_range ) ]
    for( auto&& vertex : _mesh.vertices() ) {
      const double u = vertex.u();
      const double a = u + _prop_anterior_range; // NOTE: a = u - _prop_anterior_range ?? 
      const double b = u + _prop_posterior_range;

      RangeVector<1> sum(0.0);
      double range = 0.0;
      for( auto&& e : _mesh.elements() ) {
	if( e.left().u() < a or e.right().u() > b ) {
	  continue;
	} else {
	  const double q = (my_position.evaluate(e.left().index()) -
			    my_position.evaluate(e.right().index()))
                               .norm();

	  const auto kappa_left = my_kappa.evaluate( e.left().index() );
	  const auto kappa_right = my_kappa.evaluate( e.right().index() );

	  sum += 0.5 * q * kappa_left
	    + 0.5 * q * kappa_right;
	  range += q;
        }
      }
      // ensure range is not zero
      range = std::max( range, 1.0e-10 );

      _feedback.assign( vertex.index(), sum / range );
    }
  }

  void prop_time_integration(){ // TODO: implement time integration (check if it needs to go on the rhs)

	//for(auto&& vertex : _mesh.vertices()){
		
	//}
  }

/*  void update_states_singleThreshold() {

    for( auto&& vertex : _mesh.vertices() ) {
      const RangeVector<2> state = _state.evaluate( vertex.index() );
      const RangeVector<1> feedback = _feedback.evaluate( vertex.index() );
      const double I_stretch_d = -feedback.at(0);
      const double FF_contrib  = advance_FF( vertex.u() );
      const double I_d = I_stretch_d*0.0 + FF_contrib;
      const double threshold_max = _centre + _hyst;

	if(state.at(0) == 0)
	{
		if(I_d > threshold_max)
		{
  			_state.assign( vertex.index(), {{ 1, 0 }} );
		}
	}
	else
	{
		if(I_d < -threshold_max)
		{
	  		_state.assign( vertex.index(), {{ 0, 1 }} );
		}

	}
    }
 }
*/

  void update_states_singleThreshold() {

    for( auto&& vertex : _mesh.vertices() ) {
      const RangeVector<2> state = _state.evaluate( vertex.index() );
      const RangeVector<1> feedback = _feedback.evaluate( vertex.index() );
      const double I_stretch_d = -feedback.at(0);
      const double FF_contrib  = advance_FF( vertex );
      const double I_d = I_stretch_d*0.0 + 3.0*FF_contrib; // TODO these should have 'weight' params for FF and FB to do sweeps 
      const double threshold_max = _centre + _hyst;

	if(state.at(0) == 0)
	{
		if(I_d > threshold_max)
		{
  			_state.assign( vertex.index(), {{ 1, 0 }} );
		}
	}
	else
	{
		if(I_d < -threshold_max)
		{
	  		_state.assign( vertex.index(), {{ 0, 1 }} );
		}

	}
    }
 }


  void update_states_reset() {
    // update the states

    for( auto&& vertex : _mesh.vertices() ) {
      // get the current state
      const RangeVector<2> state = _state.evaluate( vertex.index() );
      // get feedback
      const RangeVector<1> feedback = _feedback.evaluate( vertex.index() );
      // get ventral currents
      const double I_stretch_v = feedback.at(0);
      const double I_AVB_v = _AVB_v ;
      const double I_inhib_v = -_inh * state.at(0);
      const double I_v = I_stretch_v + I_AVB_v + I_inhib_v;

      // get dorsal currents
      const double I_stretch_d = -feedback.at(0);
      const double I_AVB_d = _AVB_d ;
      const double I_inhib_d = 0.0;
      const double I_d = I_stretch_d + I_AVB_d + I_inhib_d;

      // get thresholds
      const double threshold_max = _centre + _hyst;
      const double threshold_min = _centre - _hyst;

      // ventral should have priority NOTE: this is the original
      if( state.at(0) == 0 and state.at(1) == 0 ) {
	// (0,0) -> (0,1)
//	std::cout << "state (0,0) reached "  << std::endl;
	if( I_v > threshold_max ) {
	  _state.assign( vertex.index(), {{ 0, 1 }} );
	} else if( I_d > threshold_max ) {
	  // todo: should not be here? throw error?
	  _state.assign( vertex.index(), {{ 1, 0 }} );
	}
      } else if ( state.at(0) == 0 and state.at(1) == 1 ) {
	// (1,0) -> (1,1)
//	std::cout << "state (0,1) reached  " << std::endl;
	if( I_d > threshold_max ) {
	  _state.assign( vertex.index(), {{ 1, 1 }} );
	} else if ( I_v < threshold_min ) {
	  // todo: shoudn't be here
	  _state.assign( vertex.index(), {{ 0, 0 }} );
	}
      } else if ( state.at(0) == 1 and state.at(1) == 1 ) {
	// (1,1) -> (0,1)
//	std::cout << "state (1,1) reached  " << std::endl;
		
	if( I_v < threshold_min ) {
	  _state.assign( vertex.index(), {{ 1, 0 }} );
	} else if( I_d < threshold_min ) {
	  // todo: shouldn't be here
	  _state.assign( vertex.index(), {{ 0, 1 }} );
	}
      } else if ( state.at(0) == 1 and state.at(1) == 0 ) {
	// (1,0) -> (0,0)
//	std::cout << "state (1,0) reached  " << std::endl;
	
	if( I_d < threshold_min ) {
	  _state.assign( vertex.index(), {{ 0, 0 }} );
	} else  if ( I_v > threshold_max ) {
	  _state.assign( vertex.index(), {{ 1,1 }} );
	}
      }
    }
 }




  void update_states_reset_effective_threshold() {
    // written in terms of effective thresholds -- good for understanding/debugging the neural circuit
    for( auto&& vertex : _mesh.vertices() ) {
      // get the current state
      const RangeVector<2> state = _state.evaluate( vertex.index() );
      // get feedback
      const RangeVector<1> feedback = _feedback.evaluate( vertex.index() );

      // set effective thresholds (written in ascending order)
      const double Voff = -1.5;
      const double Don  = -1.4;
      const double Von  = -0.5;
      const double Doff =  1.4;
      const double Voff_Don = Voff; // 0.5; // Voff
      const double Von_Don  = Von; // 1.5; // Von


// Note: using "continue" to ensure that only one state change occurs per vertex per timestep

      // ventral should have priority NOTE: this is the original
      if( state.at(0) == 0 and state.at(1) == 0 ) {
//std::cout << "loop 1 entered" << std::endl; 
	// (0,0) -> (0,1)
	if( feedback.at(0) > Von  ) {
	  _state.assign( vertex.index(), {{ 0, 1 }} ); // {0, 1}
//	std::cout << _tp.time() <<  "  1a" << std::endl; 
	continue;
	} else if( feedback.at(0) < Don ) {
//	std::cout << _tp.time() <<  "  1b" << std::endl; 
	  _state.assign( vertex.index(), {{ 1, 0 }} );
	continue;
	}

      } else if ( state.at(0) == 0 and state.at(1) == 1 ) {
//std::cout << "loop 2 entered" << std::endl; 
	// (0,1) -> (1,1)
	if( feedback.at(0) < Don ) {
//	std::cout << _tp.time() <<  "  2a" << std::endl; 
 	 _state.assign( vertex.index(), {{ 1, 1 }} );
	continue;
	} else if ( feedback.at(0) < Voff ) {
//	std::cout << _tp.time() <<  "  2b" << std::endl; 
 	 _state.assign( vertex.index(), {{ 0, 0 }} );
	continue;
	}

      } else if ( state.at(0) == 1 and state.at(1) == 1 ) {
//std::cout << "loop 3 entered" << std::endl; 
	// (1,1) -> (0,1)
	if( feedback.at(0) < Voff_Don ) {
//	std::cout << _tp.time() <<  "  3a" << std::endl; 
 	 _state.assign( vertex.index(), {{ 1, 0 }} );
	continue;
	} else if( feedback.at(0) > Doff ) {
//	std::cout << _tp.time() << "  3b" << std::endl;
	  _state.assign( vertex.index(), {{ 0, 1 }} ); //{0, 1}a
	continue;
	}

      } else if ( state.at(0) == 1 and state.at(1) == 0 ) {
//std::cout << "loop 4 entered" << std::endl; 
	// (1,0) -> (0,0)	
	if( feedback.at(0) > Doff ) {
//	std::cout << _tp.time() <<  "  4a" << std::endl; 
  	_state.assign( vertex.index(), {{ 0, 0 }} );
	continue;
	} else  if ( feedback.at(0) > Von_Don ) {
//	 std::cout << _tp.time() <<  "  4b" << std::endl; 
	 _state.assign( vertex.index(), {{ 1,1 }} );
	continue;
	}
      }
    }
 }

//======================================================
//        FORMS OF CPG: SINE, READ AND PHASE 
//======================================================

  double  update_states_sine( const double vertex_u ) {
	//TODO get fwa from param file
      const double freq = 0.5*3.3;
      const double wave = 0.6; 
      const double amp = 1.0; 

      double drive = amp * sin( 2.0*M_PI*vertex_u/wave - 2.0*M_PI*freq*_tp.time() ) ;
         // _state.assign( vertex.index(), {{drive,0}}  );
      return drive;
   }
 

   double update_states_readfile() {
 	std::string line;
 	double kappaValue;
      
 	if(_tp.time() > _readTime){
 	  // Assign kappa value to each mesh vertex (todo: at the appropriate time)
 	  *Readfile >> _readTime;
 	   std::cout << "readTime: " << _readTime << std::endl;
 	   for(auto&& vertex : _mesh.vertices() ) {
 	      *Readfile >> kappaValue;
 	      _state.assign( vertex.index() , {{kappaValue,0}});
 	   }
 	}
	return 0.0;
   }

  void compute_phaseOsc() {
//    const auto& my_kappa = kappa();
//    const auto& my_position = position();
// TODO do we need feedback to the phase oscillators here?
    const double N = _mesh.N();
    const double freq = 0.5*3.3;//*2*M_PI;
    const double weight = 1.0;
    const double lag = 0.0 ;//2.0*M_PI/N;

    for( auto&& vertex : _mesh.vertices() ) {

      const double u = vertex.u();
      const double theta  = _phaseOsc.evaluate( vertex.index() );
      const double theta_right = _phaseOsc.evaluate( vertex.index() + 1 );
      const double dtheta_dt   = freq + weight*sin( theta - theta_right + lag );

      // Euler -- TODO assert correct time somehow?
      const double new_theta = theta + _tp.deltaT() * dtheta_dt;
      _phaseOsc.assign( vertex.index(), new_theta ); 
    }
  }



  double update_states_phase_osc( const double u ) {
	double output = 0.0;
      	output = _phaseOsc.evaluate( 1.0 ); // TODO 1.0 should be the current vertex (not 'u') :/

	return 0.0;
   }
 





  void compute_beta() {
    // compute beta
    // beta = beta_old + dt * ( -a ( state(0) - state(1) ) - b * beta )
    for( auto&& vertex : _mesh.vertices() ) {
      const RangeVector<1> beta_old = _beta_old.eval( vertex.index() );
      const RangeVector<2> state = _state.evaluate( vertex.index() );
      const RangeVector<1> state_diff( state.at(0) - state.at(1) );
   // const RangeVector<1> new_beta = state_diff; // NOTE: use line below (unless using driven network type)
      const RangeVector<1> new_beta = beta_old + _tp.deltaT() * (_a * state_diff - _b * beta_old);

      _beta.assign( vertex.index(), new_beta );
    }
  }

public:
  /**
   *  Probes
   * @{
   */
  using BetaProbe = Probe_ptr<const BetaFunction>;
  using FeedbackProbe = Probe_ptr<const FeedbackFunction>;
  using StateProbe = Probe_ptr<const StateFunction>;

  struct ActivationProbe : public Probe_otf< const PiecewiseLinearFunction<1>& > {
    using T = const PiecewiseLinearFunction<1>&;
    using Base = Probe_otf<T>;

    ActivationProbe( const CaP_control_entity& e )
      : Base("control.activation"), _e( e ),
	_activation_storage( _e.mesh().N() ),
	_activation( _e.mesh(), SubArray<Vector>( _activation_storage ) ) {}

    T evaluate() const {
      _activation_storage.clear();
      const auto& state = _e.state();

      for( auto&& v : _e.mesh().vertices() ) {
	const auto my_state = state.evaluate( v.index() );
	_activation.assign( v.index(), my_state.at(0) - my_state.at(1) );
      }

      return _activation;
    }

  private:
    friend class CaP_control_entity;
    const CaP_control_entity& _e;

    mutable Vector _activation_storage;
    mutable PiecewiseLinearFunction<1> _activation;
  };

  struct NeuralstateProbe : public Probe_otf< const PiecewiseLinearFunction<1>& > {
    using T = const PiecewiseLinearFunction<1>&;
    using Base = Probe_otf<T>;

    NeuralstateProbe( const CaP_control_entity& e )
      : Base("control.neuralstate"), _e( e ),
	_neuralstate_storage( _e.mesh().N() ),
	_neuralstate( _e.mesh(), SubArray<Vector>( _neuralstate_storage ) ) {}

    T evaluate() const {
      _neuralstate_storage.clear();
      const auto& state = _e.state();

      for( auto&& v : _e.mesh().vertices() ) {
	const auto my_state = state.evaluate( v.index() );

	if( my_state.at(0) == 0 and my_state.at(1) == 0 ) {
	  _neuralstate.assign( v.index(), 1.0 );
	} else if ( my_state.at(0) == 1 and my_state.at(1) == 0 ) {
	  _neuralstate.assign( v.index(), 2.0 );
	} else if ( my_state.at(0) == 1 and my_state.at(1) == 1 ) {
	  _neuralstate.assign( v.index(), 3.0 );
	} else if ( my_state.at(0) == 0 and my_state.at(1) == 1 ) {
	  _neuralstate.assign( v.index(), 4.0 );
	} else {
	  throw exception_invalid( "neural state" );
	}
      }

      return _neuralstate;
    }

  private:
    friend class CaP_control_entity;
    const CaP_control_entity& _e;

    mutable Vector _neuralstate_storage;
    mutable PiecewiseLinearFunction<1> _neuralstate;
  };

  /**
   *  @}
   */

private:
  const Mesh& _mesh;
  const TimeProvider& _tp;

  Vector _beta_storage;
  BetaFunction _beta;
  Vector _beta_old_storage;
  BetaFunction _beta_old;
  Vector _state_storage;
  StateFunction _state;
  Vector _feedback_storage;
  FeedbackFunction _feedback;
  Vector _phaseOsc_storage;
  PhaseOscFunction _phaseOsc;


  // parameters
  const std::string _network_type;
  const std::string _CPG_type;
  const double _prop_posterior_range;
  const double _prop_anterior_range;
  const double _prop_tau;
  const double _centre;
  const double _hyst;
  const double _AVB_d;
  const double _AVB_v;
  const double _inh;
  const double _a;
  const double _b;
 	
  const std::string _readfile;
  double _readTime;
  mutable std::ifstream* Readfile;

  // external probes
  std::shared_ptr<const KappaProbe> _kappa_probe_ptr;
  std::shared_ptr<const PositionProbe> _position_probe_ptr;
};
