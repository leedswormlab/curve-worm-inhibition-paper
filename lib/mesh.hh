#pragma once

// BUG: library here.
// #include "config.hh"

#include <cassert>
#include <vector>
#include "../include/misc.hh"

/**
 *  Mesh
1D mesh.
interval [a, b] divided into N-1 segments.
u: the control points.
h: the segment length
 *
 *  The mesh elements and vertices should be accessed via their contains in loops such as:
\code{.cpp}
for( auto&& vertex : mesh.vertices() ) {
  // do stuff with vertex
}

for( auto&& element : mesh.elements() ) {
  // do stuff with element
}
\end{code}
 */
class Mesh {
public:
  struct Vertex;
  struct Element;
protected:
  struct VertexIterator;
  struct Vertices;
  struct ElementIterator;
  struct Elements;

public:
  Mesh( const double a, const double b, unsigned int N )
    : _a( a ), _b( b ), _N( N ),
      _vertices( *this ),
      _elements( *this )
  {
    assert( a <= b );
  }

  // delete copy constructor
  Mesh( const Mesh& other ) = default;

  // left end point
  double a() const {
    return _a;
  }
  // right end point
  double b() const {
    return _b;
  }
  // number of vertices
  unsigned int N() const {
    return _N;
  }

  // position of vertex v
  double u( const Vertex& v ) const
  {
    return v.u();
  }
  // size of element e
  double h( const Element& e ) const
  {
    return e.h();
  }

  // iterable container of vertices
  const Vertices& vertices() const {
    return _vertices;
  }
  // iterable container of elements
  const Elements& elements() const {
    return _elements;
  }

  DEPRECATED
  double h( const unsigned int ) const {
    return 1./double(N()-1);
  }
  DEPRECATED
  double H( const unsigned int j ) const {
    return h(j);
  }
  DEPRECATED
  double u( const unsigned int j ) const {
    return vertices()[j].u();
  }

  /**
   *  Vertex
   *
   *  A single vertex within a mesh.
   */
    struct Vertex {
      Vertex( const Mesh& mesh, unsigned int index = 0 )
	: mesh_( mesh ), index_( index ), h_( comp_h() ), u_( comp_u() )
      {}
      Vertex( const Vertex& other ) = default;
      Vertex( Vertex&& other ) = default;

      // position of vertex
      double u() const {
	return u_;
      }
      // index of vertex
      unsigned int index() const {
	return index_;
      }
    protected:
      // compute mesh size of a fixed width mesh
      double comp_h() const {
	return ( mesh_.b() - mesh_.a() ) / ( mesh_.N() - 1.0 );
      }
      // compute location for a fixed width mesh
      double comp_u() const {
	return index_ * h_;
      }

    private:
      // increment operator
      Vertex& operator++() {
	++index_;
	u_ += h_;
	return *this;
      }

      const Mesh& mesh_;
      unsigned int index_;
      const double h_;
      double u_;

      friend VertexIterator;
    };



  /**
   *  Element
   *
   *  A single element in a mesh
   */
  struct Element {
  public:
    Element( const Mesh& mesh, unsigned int index )
      : mesh_( mesh ), index_( index )
    {}
    Element( const Element& other ) = default;
    Element( Element&& other ) = default;

    // element width
    double h() const {
      return right().u() - left().u();
    }

    // left vertex
    const Vertex left() const { return mesh_.vertices()[ index_ ]; }
    // right vertex
    const Vertex right() const { return mesh_.vertices()[ index_+1 ]; }

    const Mesh& mesh() const { return mesh_; }
    unsigned int index() const { return index_; }

  private:
    // increment operator
    Element &operator++() {
      ++index_;
      return *this;
    }

    const Mesh& mesh_;
    unsigned int index_;

    friend ElementIterator;
  };

protected:
  /**
   *  VertexIterator
   */
    struct VertexIterator {
      VertexIterator( const Mesh& mesh, const unsigned int index = 0 )
	: value( mesh, index )
      {}

      // increment operator
      VertexIterator& operator++() {
	++value;
	return (*this);
      }

      // dereference
      const Vertex& operator* () const {
	return value;
      }

      // equality comparison
      bool operator==( const VertexIterator& other ) const {
	return value.index() == other.value.index();
      }
      // inequality comparison
      bool operator!=( const VertexIterator& other ) const {
	return value.index() != other.value.index();
      }

    private:
      Vertex value;
    };

  /**
   *  Vertices
   *
   *  Iterable container of vertices(iterators)
   */
  struct Vertices {
    Vertices( const Mesh& mesh )
      : mesh_( mesh )
    {}

    VertexIterator begin() const {
      return VertexIterator( mesh_ );
    }

    VertexIterator end() const {
      return VertexIterator( mesh_, mesh_.N() );
    }

    Vertex front() const {
      return Vertex( mesh_, 0 );
    }
    Vertex back() const {
      return Vertex( mesh_, mesh_.N()-1 );
    }

    // random access
    Vertex operator[] ( const unsigned int index ) const {
      return Vertex( mesh_, index );
    }

    unsigned size() const {
      return mesh_.N();
    }

  private:
    const Mesh& mesh_;
  };

  /**
   *  ElementIterator
   *
   *  Iterator for Elements
   */
  struct ElementIterator {
    ElementIterator( const Mesh& mesh, const unsigned int index = 0 )
      : value( mesh, index )
    {}

    // increment operator
    ElementIterator& operator++() {
      ++value;
      return (*this);
    }

    // dereferencer
    const Element& operator* () const {
      return value;
    }

    // equality comparison
    bool operator==( const ElementIterator& other ) const {
      return value.index() == other.value.index();
    }
    // inequality comparison
    bool operator!=( const ElementIterator& other ) const {
      return value.index() != other.value.index();
    }

  private:
    Element value;
  };

  /**
   *  Elements
   *
   *  Container for elements.
   */
  struct Elements {
    Elements( const Mesh& mesh )
      : mesh_( mesh )
    {}

    ElementIterator begin() const {
      return ElementIterator( mesh_, 0 );
    }

    ElementIterator end() const {
      return ElementIterator( mesh_, mesh_.N()-1 );
    }

    unsigned size() const {
      return mesh_.N()-1;
    }

  private:
    const Mesh& mesh_;
  };

private:
  const double _a;
  const double _b;
  const unsigned int _N;
  const Vertices _vertices;
  const Elements _elements;
}; // mesh
