#ifndef MODEL_HH
#define MODEL_HH

#include <memory>
#include <iostream>
#include <functional>
#include <vector>
#include "vector.hh"
#include "timeprovider.hh"
#include "parameter.hh"
#include "matrix.hh"
#include "readMat.hh"
#include "x0.hh"
#include <vector>
#include "worm.hh"
#ifdef OpenCV_FOUND
#include "distancefunction.hh"
#include "forcefunction.hh"
#endif

#include "femscheme.hh" // BUG?

ConfigParameters emptyParm;

  // TODO: use reasonable defaults
  const double default_gamma = 1.0;
  const double default_K = 1.0;
  const double default_e0 = 1.0;
  const double default_et = 0;
  const double default_e2 = 0;
  const double default_mu = 0;


// a model for the worm and the environment.
/* TODO
 * this isn't the correct defaults but it is used(?)
 */
template<const unsigned int mydim>
struct ModelDefault : public ModelInterface<mydim> {
private:
  using BaseType = ModelInterface<mydim>;
public: // types
  static const unsigned int dim = BaseType::dim; // not mydim..?
  using RangeVectorType = typename BaseType::RangeVectorType;
  using WorldCoordinate = typename BaseType::RangeVectorType;
  using WorldCoTangent = typename BaseType::RangeVectorType;
  using Constraint = typename BaseType::Constraint;
  using MeshCoordinate = typename BaseType::MeshCoordinate;
public: // construct
  ModelDefault(ConfigParameters &parameters)
      : gamma_(default_gamma), K_(default_K), e0_(default_e0), et_(default_et), e2_(default_e2), mu_(default_mu) {
    gamma_ = parameters.get<double>("model.gamma", default_gamma);
    K_ = parameters.get<double>("model.K", default_K);
    e0_ = parameters.get<double>("model.e0", default_e0);
    et_ = parameters.get<double>("model.et", default_et);
    e2_ = parameters.get<double>("model.e2", default_e2);
    mu_ = parameters.get<double>("model.mu", default_mu);

    _diriclet_curv = 1;

    message(bTRACE, "initialize model %f %f\n", e0_, e2_);
  }
public:
  // initial values. mesh coordinate to world
  virtual WorldCoordinate X0(const double s) const {
    untested();
    WorldCoordinate ret;
    ret[0] = s;
    for (unsigned int d = 1; d < dim; ++d) {
      untested();
      ret[d] = 0.0;
    }

    return ret;
  }

  virtual WorldCoTangent Force(const WorldCoordinate & /*x*/ ) const {
    untested();
    RangeVectorType ret(0);
    return ret;
  }

  virtual double gamma(double /*s*/) const {
    return gamma_;
  }

  virtual double K(const double /*s*/) const {
    return K_;
  }

protected:
public:
  virtual double regularisation( MeshCoordinate const& s, const Entity& e) const { untested();
//    message(bTRACE, "reg %f %f %f\n", e0_, e2_, s);
    // s = .5 is the center.
    //olde *= ( 1.-time() * et_ );
//Jack    trace5("base reg", et_, e0_, e2_, s, e.time());
    return (e0_ + e2_ * (s - .5) * (s - .5)) * (1. + et_ * e.time());
    /* TODO some funny time scaling was in here shouldn't be for default! */
  }
#if 0
  /* TODO this should be the default
   * but be careful of side effects of changing this */
  virtual double regularisation( MeshCoordinate const& s, Entity const& e ) const {
    return e0_;
  }
#endif
  virtual double mu(MeshCoordinate const &s, Entity const &e) const {
    return mu_;
  }

  /// hmm, proper interface?!
  // only use continuous model, still efficient
  virtual double energyDensity_at(
      WorldCoordinate const& X,
      RangeVectorType const& Y, double const& s,
      Entity const& e )const
  {
    return 0;
  }

  virtual double energyDensity(
      WorldCoordinate const &X,
      RangeVectorType const &Y) const {
    untested();
    incomplete();
    return 0;
  }

  virtual double beta(double const &s, const Entity &e) const {
    untested();
    return 0;
  }

  virtual Constraint constraint() const {
    return Constraint::measure;
  }

  // obsolete.
  template<class DF>
  void extraForcing_template(const DF &X, DF &rhs) const {
    untested();
    unreachable();
  }

  template<class DF>
  double extraEnergy_template(const DF & /*X*/ ) const {
    untested();
    unreachable();
    return 0;
  }

  // this is likely obsolete.
  void extra_curvature_manipulation(DiscreteFunctionInterface<mydim> &c) const {
    untested();
    if (_diriclet_curv) {
      untested();
      c.setDirichletBoundaries(0);
    } else {
      untested();
    }
  }

  virtual void extra_matrix_manipulation(
      InverseMassMatrix<PiecewiseLinearFunction<mydim> > &m) const {
    untested();
    if (_diriclet_curv) {
      untested();
      m.setDirichletBoundaries();
    } else {
      untested();
    }
  }

private:
  double gamma_;
  double K_;
  double e0_;
  double et_;
  double e2_;
  double mu_;
  bool _diriclet_curv;
}; // ModelDefault
#endif // MODEL_HH

// TODO
// generalise by getting global:
// dt - timestep (0.001)
// n - number of neurons (12)  << probably required separate parameter file for neural network & muscles
// m - number of muscles (48)

// vim:ts=8:sw=2
