#ifndef PARAMETER_HH
#define PARAMETER_HH

#include <algorithm>
#include <iostream>
#include <fstream>
#include <map>
#include <sstream>
#include <utility>
#include <vector>

#include "trace.hh"
#include "error.hh"
#include "../include/misc.hh"

/**
 *  \class ConfigParameters
 *
 *  \brief class for accessing parameters from files and command line
 *
 *  This class contains method to help with the use of parameters in
 *  the algorithms we want to use. The parameters are specified in a
 *  file with one key-value pair per line for example:
 \code{.unparsed}

  key1:value1
  key2:value2
  key3:value3

 \endcode
 *  Note that there are no spaces either side of the colon. In
 *  addition to a parameter file, extra key-value pairs can be added

 *  using the add method.
 */
class ConfigParameters { //
private: // types
  typedef std::map< std::string, std::string > DataType;
public: // types
  typedef std::map< std::string, ConfigParameters > NestType;
public:
  explicit ConfigParameters() {}
  /**
   * \brief constructor with parameter filename input
   */
  explicit ConfigParameters( const std::string& filename)
  { untested();
    merge_file(filename);
  }

  void merge_file(const std::string& filename){ 
    std::ifstream file;
    file.open( filename.c_str() );
    if( !file.is_open() ){ untested();
      throw exception_cantfind(filename);
    }else{
       merge(file);
    }
    file.close();
  }

  // non-protected merge
  // (why is readConfigFile protected?)
  void merge(std::ifstream& file)
  {
    readConfigFile( file );
  }

  /**
   * \brief set parameter with key
   */
  void set( const std::string& key, const std::string& value){ itested();
     auto p=key.find('.');
     if(p!=std::string::npos){ itested();
	auto cat=key.substr(0,p);
	auto subkey=key.substr(p+1);
	_nest[cat].set(subkey, value);

	data_[key] = value; // legacy
     }else{
	data_[key] = value;
     }
  }

  /**
   * \brief update parameter at key. complain if it's not there.
   */
  void update( const std::string& key, const std::string& value){ itested();
     // incomplete. handle dots...
     auto pos=data_.find(key);
     if(pos==data_.end()){ untested();
	throw exception_cantfind(key);
     }else{
	data_[key]=value;
     }
  }

  /**
   * \brief get parameter with key
   */
  template< class T >
  T get( const std::string& key ) const
  {
    // get value from data
    const auto valueIt = data_.find( key );

    if( valueIt == data_.end() ) {
//       std::cerr << key << "\n";
       throw exception_cantfind(key);
    }

    // extract data
    const std::string value = valueIt->second;

    // assign to string stream
    std::stringstream ss;
    ss << value;

    // extract from string stream
    T retValue;
    ss >> retValue;

    return retValue;
  }

  template< class T >
  T& dump( T& s, std::string prefix="", const std::string separator="=") const {
     for(auto const& i : data_){
	s << prefix << i.first << separator << i.second << "\n";
     }
     return s;
  }

  /**
   * \brief get parameter with key
   */
  template< class T >
  T get( const std::string& key, const T& def ) const
  {
    // get value from data
    const auto valueIt = data_.find( key );

    if( valueIt == data_.end() )
      { untested();
	std::cout << "unable to find key: " + key << "\n"
		  << "using default " << key << " = " << def << std::endl;
	return def;
      }

    // extract data
    const std::string value = valueIt->second;

    // assign to string stream
    std::stringstream ss;
    ss << value;

    // extract from sting stream
    T retValue;
    ss >> retValue;

    return retValue;
  }

  std::string getString( const std::string& key ) const
  {
    // get value from data
    const auto valueIt = data_.find( key );

    if( valueIt == data_.end() )
      { untested();
       throw exception_cantfind(key);
      }

    // extract data
    const std::string value = valueIt->second;
    return value;
  }

  std::string getString(const std::string &key, const std::string &def) const
  {
    try {
      return getString(key);
    } catch (std::exception e) {
      std::cout << "unable to find key: " + key << "\n"
                << "using default " << key << " = " << def << std::endl;
      return def;
    }
  }

  std::vector<std::string> getStringVector( const std::string& key ) const
  { untested();
    const auto valueIt = data_.find( key );

    if( valueIt == data_.end() )
    { untested();
      throw exception_cantfind(key);
    }

    std::stringstream ss(valueIt->second);
    std::string values;
    std::getline(ss, values, '#');

    std::vector< std::string > ret;
    values.erase(std::remove (values.begin(), values.end(), ' '), values.end());
    std::stringstream params(values);
    std::string item;
    while (std::getline(params, item, ',')) {
      ret.push_back(item);
    }
    return ret;
  }
  std::vector<std::string> getStringVector( const std::string& key, const std::string& def ) const {
    try {
      return getStringVector( key );
    } catch ( std::exception e ) {
      std::cerr << "unable to find key: " + key << "\n"
		<< "using default for " << key << " = " << def << std::endl;

      std::stringstream ss( def );
      std::string values;
      std::getline(ss, values, '#');

      std::vector<std::string> ret;
      values.erase(std::remove(values.begin(), values.end(), ' '),
		   values.end());
      std::stringstream params(values);
      std::string item;
      while (std::getline(params, item, ',')) {
	ret.push_back(item);
      }
      return ret;
    }
  }

  /**
   * \brief get parameter with key plus camera number
   * the format is original_key.camN
   */
  template< class T >
  T getCam( const std::string& key, const unsigned int cam )
  { untested();
    std::stringstream ss;
    ss << key << ".cam" << cam;
    return get< T >( ss.str() );
  }

  std::string getCamString( const std::string& key, const unsigned int cam ) const
  {
    std::stringstream ss;
    ss << key << ".cam" << cam;
    return getString( ss.str() );
  }

  /**
   * \brief get vector with key base
   */
  template< class T >
  std::vector< T > getCamVector( const std::string& key, const unsigned int nCams )
  { untested();
    std::vector< T > ret;
    for( unsigned int cam = 0; cam < nCams; ++cam )
      ret.push_back( getCam<T>( key, cam ) );

    return ret;
  }

  std::vector< std::string > getCamStringVector( const std::string& key, const unsigned int nCams )
  { untested();
    std::vector< std::string > ret;
    for( unsigned int cam = 0; cam < nCams; ++cam )
      ret.push_back( getCamString( key, cam ) );

    return ret;
  }

  int getFromList( const std::string& key, const std::vector< std::string >& names )
  {
    const std::string value = getString( key );
    const auto it = std::find( names.begin(), names.end(), value );

    if( it != names.end() )
      {
	return std::distance( names.begin(), it );
      }
    else
      { untested();
	std::stringstream ss;
	ss << "unable to find value \"" << value << "\" in names:";
	for( const std::string& n : names )
	  ss << " " << n;
	throw MyException( ss.str() );
      }
  }

  /**
   * \brief add parameter (from command line arguments)
   */
  void add( const char* in )
  { untested();
    std::istringstream is_line( in );
    std::string key;
    if( std::getline( is_line, key, ':' ) ) // hmm, also need key=value in --key=value
      { untested();
	std::string value;
	if( std::getline( is_line, value ) )
	  store_line( key, value );
      }
  }


protected:
  /**
   * \brief extract key-value pairs from stream
	* for store_line to read
   */
  void readConfigFile( std::ifstream& stream )
  {
    std::string line;
    while( std::getline( stream, line) )
      {
	std::istringstream is_line(line);
	std::string key;
	if( std::getline(is_line, key, ':') )
	  {
	    std::string value;
	    if( std::getline(is_line, value) )
	      store_line(key, value);
	  }
      }
  }

  /**
   * \brief stores key-value pairs, over writes where necessary
   */
  void store_line( const std::string &key, const std::string& value )
  {
    auto it = data_.insert( std::pair< std::string, std::string >( key, value ) );

    if( it.second == true )
      {
	return;
      }

    // otherwise over writing
    it.first->second = value;
  }

private:
  DataType data_;
  NestType _nest;
public:
  friend void parse(ConfigParameters& c, const char* in );
};

template<typename >
DEPRECATED
void set( ConfigParameters& c, const std::string& /*key*/, const bool& value )
{ untested();
	std::string vs=value?"true":"false";
	c.set(vs, vs);
}

template<class T>
DEPRECATED
T get( ConfigParameters const& c, const std::string& key )
{
	return c.get<T>(key);
}

template<>
DEPRECATED
inline bool get( ConfigParameters const& c, const std::string& key )
{
	std::string s=c.get<std::string>(key);

	if(s=="true"){
		return true;
	}else if(s=="1"){ untested();
		return true;
	}else if(s=="yes"){ untested();
		return true;
	} // ....
	else{
		return false;
	}
}

template<class T>
DEPRECATED
T get( ConfigParameters const& c, const std::string& key, T /*def*/ )
{ untested();
	incomplete();
	return c.get<T>(key);
}

template<>
DEPRECATED
inline bool get( ConfigParameters const& c, const std::string& key, bool def )
{
	bool ret;
	try{
		ret = get<bool>(c, key);
	}catch( exception_cantfind& ){
		ret = def;
	}catch( std::string ){ untested();
	  unreachable(); // should not throw non-exceptions
		ret = def;
	}

	return ret;
}

// bug: wrong file.
DEPRECATED
inline void parse(ConfigParameters& c, const char* in )
{
   std::string arg( in );
   std::istringstream is_line( in );
   std::string key;
   if( arg.find(":")!=std::string::npos
	 &&  std::getline( is_line, key, ':' ) ) {
      std::string value;
      if( std::getline( is_line, value ) ){
	 c.update( key, value );
	 return;
      }else{ untested();
      }
   }else if( arg.find("=")!=std::string::npos
	 && std::getline( is_line, key, '=' ) ) {
      std::string value;
      if( std::getline( is_line, value ) ){
	 c.update( key, value );
	 return;
      }else{ untested();
      }
   }else{
      trace1("parse, did not work", in);
      throw exception_nomatch();
   }
}


/**
 *  Parameters
 *
 *  Implemented as a static singleton. Parameters are used
 *  everywhere so we shouldn't worry about passing it around.
 *
 *  TODO improve constness
 */
class Parameters {
public:

  // deleted copy constructor
  Parameters( const Parameters& ) = delete;
  // deleted assignment operator
  void operator=( const Parameters& ) = delete;

  // merge file by filename
  static void merge_file( const std::string& filename ) {
    instance().merge_file(filename);
  }

  // merge file from input stream
  static void merge( std::ifstream& file ) {
    instance().merge( file );
  }

  // add parameter (often used for command line arguments)
  static void add( const char* in ) {
    instance().add( in );
  }

  // set parameters by string value
  static void set( const std::string& key, const std::string& value ) {
    instance().set( key, value );
  }

  // update parameters by string value
  static void update( const std::string& key, const std::string& value ) {
    instance().update( key, value );
  }

  // templated getter. complains if key not found
  template< class T >
  static T get( const std::string& key ) /*const*/ {
    return instance().get<T>( key );
  }

  // templated getter. uses default ~def~ if not found
  template< class T >
  static T get( const std::string& key, const T& def ) /*const*/ {
    return instance().get( key, def );
  }

  // specialised getter for strings. complains if key not found
  static std::string getString( const std::string& key ) /*const*/ {
    return instance().getString( key );
  }

  // specialised getter for strings. uses default ~def~ if key not found
  static std::string getString( const std::string& key, const std::string& def ) /*const*/ {
    return instance().getString( key, def );
  }

  // specialised getter for string lists. complains if key not found
  static std::vector<std::string> getStringVector( const std::string& key ) /* const */ {
    return instance().getStringVector(key );
  }

  // specialised getter for string lists. uses default ~def~ if key not found
  static std::vector<std::string> getStringVector( const std::string& key, const std::string& def ) /* const */ {
    return instance().getStringVector(key, def);
  }

  // dump parameters to stream s
  // the format is
  // {prefix}{key}{separator}{value}
  template< class T >
  static T& dump( T& s, std::string prefix="", const std::string separator="=") /*const*/ {
    return instance().dump( s, prefix, separator );
  }
private:
  // private constructor
  Parameters() {}

  // access to instance
  // TODO can we have const access also?
  static ConfigParameters& instance() {
    static ConfigParameters _instance;
    return _instance;
  }
};

#endif // #ifndef PARAMETER_HH
// vim:ts=8:
