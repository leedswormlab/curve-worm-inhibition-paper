#ifndef FGPF_HPP
#define FGPF_HPP

#include "graph/frames.hh"
#include "graph/he.hh"
#include "bits/map.hh"
#include "image/known_points.hh"

namespace draft{
/*--------------------------------------------------------------------------*/
// transition from arbitrary pxmm to per-frame ppmm
static const double some_pixel_scaling=180.;
/*--------------------------------------------------------------------------*/
template<class G, class W=void>
class framesgraph_pointfinder : public draft::MidlineFinderA<ANNOTATION> {
	using basefinder=draft::MidlineFinderA<ANNOTATION>;
public:
	typedef framesgraph<G, W> fg_type;
	using fgstats_type=typename fg_type::fgstats_type;
public: // construct
	framesgraph_pointfinder(std::string filename, double deltat,
			double ik, double iu,
			unsigned tc, unsigned te, double ds,
			unsigned dsr, bool rs, double eb=0)
		: _filename(filename), _deltat(deltat), _ik(ik), _iu(iu),
		  _thresh_collapse(tc), _thresh_edge(te),
		  _dilate_tgt_sil(dsr),
		  _restrict_tgt_sil(rs),
		  _eucl_base(eb)
	{
		message(bDEBUG, "crreating fgpf, dilate_tgt_sil %d restrict_tgt_sil %d\n", dsr, rs);
		_fg.clear();
		_fg.set_distance_scale(ds);
		std::ifstream myfile;
		myfile.open (filename);
		_fg.load(myfile);
	}
	void set_eucl_base(double x){
		_eucl_base=x;
	}
    //kpf = new fgpf_type(loadfile, deltat, initialguess_known, initialguess_unknown,
	 // thresh_collapse, thresh_edge,
	 // similarity_sigma, dilate_sil_radius, restrict_sil);
	framesgraph_pointfinder(fg_type&& f, double deltat,
			double ik, double iu,
			unsigned tc, unsigned te, double ds,
			unsigned dsr, bool rs, double eb)
		: _filename("none"), _deltat(deltat), _ik(ik), _iu(iu),
		  _thresh_collapse(tc), _thresh_edge(te),
		  _dilate_tgt_sil(dsr),
		  //_distance_scale(ds),
		  _restrict_tgt_sil(rs),
		  _eucl_base(eb)
	{ untested();
		message(bDEBUG, "fgpf from graph, dilate_tgt_sil %d\n", dsr);
		_fg = std::move(f);
		//_fg.set_distance_scale(ds); //??
	}

	// just find the points in ROI.
	void find_known_points_masked(cv::Mat const& worm, cv::Mat const& mask, ANNOTATION& known,
			float ppmm /*BUG*/) const
	{
		// display_grayscale_frame("a worm", worm);
		// display_grayscale_frame("a mask", mask);
		_fg.import_target(worm, mask,
				_thresh_collapse, _thresh_edge, _similarity_sigma,
				ppmm, true /*internal edges*/, _stats, _eucl_base);

		std::string label="dontknowyet";
		{
			s_timer ht("hesolve");
#if 1
		try{
			draft::HEsolver<fg_type> he(_fg, _deltat, _ik, _iu, label);
			he.do_it(mask);
			he.append_result(known);
		}catch(UmfpackError e){ untested();
			message(bDANGER, "didn't work with %f\n", _deltat);
			throw e;
		}
#else
		draft::FEsolver<fg_type> he(fg, _deltat, _ik, _iu, label);
		he.do_it(silfrm);
		he.get_result(known);
#endif
		}
	}

	void find_known_points(cv::Mat const& sil,
	                       cv::Mat& worm,
								  ANNOTATION& known,
								  float ppmm) const
	{
		// HACK, used from within worm-skeleton?
		adjust_brightness_max(worm, sil);
		auto const& cworm=worm;
		find_known_points(sil, cworm, known, ppmm);
	}

	void find_known_points(
		cv::Mat const& sil, cv::Mat const& worm, ANNOTATION& known, float ppmm) const
	{
		message(bDEBUG, "graph::fkp\n");
		s_timer tmr("midline finder");
		// what is srcs_?

		cv::Mat tgtsil=worm.clone();
		// =============
		cv::Mat silfrm=worm.clone();
		cv::Mat tmpworm=worm.clone();
		// if(_one_by_one){ untested();
		// 	find_known_points_dissected(worm, known, ppmm);
		// }else
		if(_restrict_tgt_sil){
			message(bLOG, "restrict silhouette, dilate %d\n", _dilate_tgt_sil);
//			silhouetteImageDilate(worm, silfrm, _dilate_tgt_sil);
			basefinder::find_known_points(sil, tmpworm, known, 0.f);
			kpframe(tmpworm, known); // sets known white, rest black
			known.clear();
			// display_grayscale_frame("wormnodilate", tmpworm);
			// display_sil_bb("wormnodilate", tmpworm);
			dilate_circular(tmpworm, _dilate_tgt_sil);
			silfrm = tmpworm;
			// display_sil_bb("wormtodetect", tmpworm);
			find_known_points_masked(worm, silfrm, known, ppmm);
		}else{ untested();
			 incomplete(); //does not make sense.
			silhouetteImageDilate(worm, silfrm, _dilate_tgt_sil, 200); // FIXME
			find_known_points_masked(worm, silfrm, known, ppmm);
		}
		//	==============

		_stats._elapsed=tmr.elapsed();
		message(bLOG, "mf took %f\n", _stats._elapsed);
	}
	float pattern_scale() const{ untested();
		return _fg.pattern_scale();
	}
	fg_type& fg(){ untested();
		return _fg;
	}

	unsigned dilate_tgt_sil()const{ return _dilate_tgt_sil; }
private:
	std::string _filename;
	double _deltat;
	double _ik, _iu; // initially known/unknown
	mutable /*bug/incomplete*/ fg_type _fg;

	unsigned _thresh_collapse;
	unsigned _thresh_edge;
	double _similarity_sigma;
	unsigned _dilate_tgt_sil;
	bool _restrict_tgt_sil;

private: // stats, override virtual
	void dump_stats(std::ofstream& o) const{
		o << _stats._max_radius_sq << " " << _stats._elapsed << "";

		assert(_stats._points_left.size());
		auto l=_stats._points_left.begin();
		o << " " << *l;
		++l;
		while(_stats._points_left.end()!=l){
			o << "," << *l;
			++l;
		}
	}
	mutable fgstats_type _stats;
	const double _eucl_base ; // exp(-15)?
}; // framesgraph_pointfinder

template<class G, class W=void>
class framesgraph_pointfinder_dissect {
public:
	typedef framesgraph<G, W> fg_type;
	using fgstats_type=typename fg_type::fgstats_type;
public: // construct
	framesgraph_pointfinder_dissect(std::string filename, double deltat,
			double ik, double iu, unsigned tc, unsigned te, double ds,
			unsigned dsr, bool rs, double eb)
		: _filename(filename), _deltat(deltat), _ik(ik), _iu(iu),
		  _thresh_collapse(tc), _thresh_edge(te),
		  _dilate_tgt_sil(dsr),
		  _restrict_tgt_sil(rs),
		  _eucl_base(eb)
	{ untested();
		message(bDEBUG, "crreating fgpf, dilate_tgt_sil %d\n", dsr);
		_fg.clear();
		_fg.set_distance_scale(ds);
		std::ifstream myfile;
		myfile.open (filename);
		_fg.load(myfile);
	}
	framesgraph_pointfinder_dissect(fg_type&& f, double deltat,
			double ik, double iu, unsigned tc, unsigned te,
			unsigned dsr, bool rs, double eb)
		: _filename("none"), _deltat(deltat), _ik(ik), _iu(iu),
		  _thresh_collapse(tc), _thresh_edge(te),
		  _dilate_tgt_sil(dsr),
		  _restrict_tgt_sil(rs),
		  _eucl_base(eb)
	{ untested();
		message(bDEBUG, "fgpf from graph, dilate_tgt_sil %d\n", dsr);
		_fg = std::move(f);
	}

	// just find the points in ROI.
	// static, share...?!
	void find_known_points_masked(cv::Mat const& worm, cv::Mat const& mask, ANNOTATION& known,
			float ppmm /*BUG*/) const
	{ untested();
		// display_grayscale_frame("a worm", worm);
		// display_grayscale_frame("a mask", mask);
		_fg.import_target(worm, mask,
				_thresh_collapse, _thresh_edge, _similarity_sigma,
				ppmm, true /*internal edges*/, _stats, _eucl_base);

		std::string label="dontknowyet";
		{ untested();
			s_timer ht("hesolve");
#if 1
		try{ untested();
			draft::HEsolver<fg_type> he(_fg, _deltat, _ik, _iu, label);
			he.do_it(mask);
			he.get_result(known);
		}catch(UmfpackError e){ untested();
			message(bDANGER, "didn't work with %f\n", _deltat);
			throw e;
		}
#else
		draft::FEsolver<fg_type> he(fg, _deltat, _ik, _iu, label);
		he.do_it(silfrm);
		he.get_result(known);
#endif
		}
	}

	// BUG: contours should be external...
	void find_known_points(
		cv::Mat const& sil, cv::Mat const& worm, ANNOTATION& known, float ppmm) const
	{ untested();
		s_timer tmr("dissect midline finder");
		assert(!known.size());

		std::vector<std::vector<cv::Point> > contours;
		silhouetteImageContours(worm, contours);

		cv::Mat mask;

		for(auto& c : contours){ untested();
//			assert(cv::iscorrect(c));
			unsigned ca(cv::contourArea(c));
			if( ca > 3500){ untested();
				message(bDANGER, "skipping monster worm %d\n", ca);
			}else{ untested();
			}

			mask=cv::Mat::zeros(worm.size(), worm.type());
			std::vector<std::vector<cv::Point> > crazy_syntax(1, c);
			cv::drawContours( mask, crazy_syntax, -1 /*sic*/, 255, CV_FILLED );
			// display_grayscale_frame("mask", mask);
			cv::Mat img=worm.clone(); // need a clone, dilated masks can overlap!!

			dilate_mask(mask, 5);

			// display_grayscale_frame("before", img);
			adjust_brightness_max(img, mask);
			// display_grayscale_frame("after", img);

			// incomplete();
			find_known_points_masked(img, mask, known, ppmm /* BUG */);
		}

		_stats._elapsed=tmr.elapsed();
	}
	float pattern_scale() const{ untested();
		return _fg.pattern_scale();
	}
	fg_type& fg(){ untested();
		return _fg;
	}

	unsigned dilate_tgt_sil()const{ return _dilate_tgt_sil; }
private:
	std::string _filename;
	double _deltat;
	double _ik, _iu; // initially known/unknown
	mutable /*bug/incomplete*/ fg_type _fg;

	unsigned _thresh_collapse;
	unsigned _thresh_edge;
	double _similarity_sigma;
	unsigned _dilate_tgt_sil;
	bool _restrict_tgt_sil;

private: // stats, override virtual
	void dump_stats(std::ofstream& o) const{ untested();
		o << _stats._max_radius_sq << " " << _stats._elapsed << "";

		assert(_stats._points_left.size());
		auto l=_stats._points_left.begin();
		o << " " << *l;
		++l;
		while(_stats._points_left.end()!=l){ untested();
			o << "," << *l;
			++l;
		}
	}
	mutable fgstats_type _stats;
	double _eucl_base;
}; // framesgraph_pointfinder



/*--------------------------------------------------------------------------*/
template<class G, class W>
template<class K, class F>
unsigned framesgraph<G, W>::find_nn(const K& keypoints, F& myfreak,
		unsigned thresh_edge)
{ untested();
	unsigned nv=::boost::num_vertices(_g);
#ifdef USE_NESTED_MAP
	double totalwt=0.;
#endif
	unsigned keycnt=0;

	unsigned found=0;

	for(auto const& k : keypoints){ itested();
		int row = k.pt.y;
		int col=k.pt.x;

		coord_type curpix(col, row);

#ifdef USE_NESTED_MAP
		_p2g.push_back(nv); // ?? fixlater..

		{ itested();
			auto f=myfreak.get_features(k);
			//vertex_descriptor nv=add_vertex(_g);
			features_type a;
			memcpy(a.data(), f, 64);
			unsigned cnt=0;
			auto p=nest::make_cn_range(_feat_stash, a, thresh_edge);
			bool foundone=false;
			for(; p.first!=p.second; ++p.first){ itested();
				foundone=true;
				auto existing_vertex = (*p.first).second;
				double distance_square = (*p.first).first;
				assert(distance_square<thresh_edge);
				++cnt;

				// auto newedge=boost::add_edge(nv, existing_vertex, _g).first;
				// auto EWM=boost::get(boost::edge_weight, _g);
				double wt=distance_square_to_weight(distance_square);
//				boost::get(EWM, newedge) = wt;
				_target_edgs.push_back(target_edg_t(nv, existing_vertex, wt));
				totalwt += wt;
			}
			found += foundone;

			// good idea?
			// what if a is already there (unlikely).
//			if(internal_edges){ itested();
//			// incomplete();
////				_feat_stash[a] = nv; // need this!
//			}else{ itested();
//			}

		}
#endif

		if(keycnt==keypoints.size()/25){ itested();
			message(bLOG, ".");
			std::cout.flush(); // hmm.
			keycnt=0;
		}else{ itested();
			keycnt++;
		}

		++nv;
	}
	return found;
}
/*--------------------------------------------------------------------------*/
#ifdef USE_NESTED_MAP
// slightly connect nearby pixels
// in an attempt to make resulting pointset more contiguous
template<class E, class K, class F, class G>
static void connect_internally_eucl(E& target_edgs, K& keypoints,
	  F const& myfreak, G const& g, double base)
{
//	typedef typename G::vertex_descriptor vertex_descriptor;
	typedef typename G::nm_type nm_type;
	typedef typename G::target_edg_t target_edg_t;

	double intwt=0;
	unsigned intcnt=0;
	nm_type internal_featmap;
	for(unsigned k=0; k<keypoints.size(); ++k){ itested();
		for(unsigned l=0; l<k; ++l){ itested();

			double dx=keypoints[k].pt.x - keypoints[l].pt.x;
			double dy=keypoints[k].pt.y - keypoints[l].pt.y;
			double sqsum=dx*dx+dy*dy;

			if(sqsum<5){ itested();
				double wt=pow(base, sqrt(sqsum));
				// message(bTRACE, "eucl %E\n", wt);
				target_edgs.push_back(target_edg_t(g.p2g(k), g.p2g(l), wt));
			}
		}
	}
	message(bLOG, "internal eucl edges/weight %d/%f\n", intcnt, intwt);
}
/*--------------------------------------------------------------------------*/
template<class E, class K, class F, class G>
static void connect_internally(E& target_edgs, K& keypoints,
	  F const& myfreak, G const& g)
{
	typedef typename G::vertex_descriptor vertex_descriptor;
	typedef typename G::nm_type nm_type;
	typedef typename G::target_edg_t target_edg_t;

	double intwt=0;
	unsigned intcnt=0;
	nm_type internal_featmap;
	for(unsigned k=0; k<keypoints.size(); ++k){ itested();
		auto f=myfreak.get_features(k);
		features_type a;
		memcpy(a.data(), f, 64);
		auto p=nest::make_cn_range(internal_featmap, a, 30000);

		for(; p.first!=p.second; ++p.first){ itested();

				vertex_descriptor existing_vertex = (*p.first).second;
				double distance_square = (*p.first).first;
//				assert(distance_square<thresh_edge);
				double wt=g.distance_square_to_weight(distance_square);
				target_edgs.push_back(target_edg_t(g.p2g(k), g.p2g(existing_vertex), wt));

				++intcnt;
				intwt+=wt;
		}
		internal_featmap[a] = k;
	}
	message(bDEBUG, "internal ps edges/weight %d/%f\n", intcnt, intwt);
}
/*--------------------------------------------------------------------------*/
template<class G, class W>
template<class K, class F>
void framesgraph<G, W>::find_te(const K& keypoints, F& myfreak,
		unsigned thresh_edge_in, bool internal_edges, fgstats_type& t, double eucl_base)
{
	size_t totalsize = _feat_stash.recount();
	message(bTRACE, "find_te in nested map of size %d\n", totalsize);
	t._points_left.clear();
	t._max_radius_sq = 0;

	unsigned long thresh_edge=thresh_edge_in;

	unsigned nv=::boost::num_vertices(_g);
   unsigned extcnt=0.;
	double extwt=0.;
//	unsigned keycnt=0;

	std::vector<unsigned> q(keypoints.size());
	std::vector<unsigned> q2;
	_p2g.resize(keypoints.size());

	for(unsigned i=0; i<keypoints.size(); ++i){ itested();
		q[i] = i;
	}

	t._points_left.push_back(keypoints.size());
	while(q.size()==keypoints.size()
		|| ( q.size() * 4 > keypoints.size() && thresh_edge<130000 )){
		t._max_radius_sq = thresh_edge; // logging.
	  	// 66%
		message(bDEBUG, "%ld/%ld left, trying %d\n", q.size(), keypoints.size(), thresh_edge);
		q2.clear();
		unsigned unknown=0;
		for(auto const& k : q) { itested();
			assert(k<keypoints.size());

			auto f=myfreak.get_features(k);
			//vertex_descriptor nv=add_vertex(_g);
			features_type a;
			memcpy(a.data(), f, 64);
			auto p=nest::make_cn_range(_feat_stash, a, thresh_edge);

			unsigned found=0;
			size_t tes=_target_edgs.size();

			double trywt=0.;
			unsigned trycnt=0;
			for(; p.first!=p.second; ++p.first){ itested();
				++found;
				vertex_descriptor existing_vertex = (*p.first).second;
				double distance_square = (*p.first).first;
				assert(distance_square<thresh_edge);
				assert(existing_vertex<::boost::num_vertices(_g));

				double wt=distance_square_to_weight(distance_square);
				assert(wt>=0);
				_target_edgs.push_back(target_edg_t(nv, existing_vertex, wt));
				trywt += wt;
				++trycnt;
			}
//			trace2("scan done", found, q.size());

			if(found==0){
				++unknown;
			}else{
			}

			if(found>5){ itested();
				// found enough neighbours accept.
				_p2g[k] = nv;
				++nv;
				extwt += trywt;
				extcnt += trycnt;
			}else if(found==totalsize){ itested();
				// found all. accept.
				_p2g[k] = nv;
				++nv;
				extwt += trywt;
				extcnt += trycnt;
			}else{ itested();
				// reset try larger radius.
				_target_edgs.resize(tes);
				q2.push_back(k);
			}

		}
		message(bTRACE, "unknowns %d\n", unknown);
		trace2("connected a pixel", extcnt, q2.size());
		message(bTRACE, ".\n");
		std::cout.flush(); // hmm.

		thresh_edge*=2;
		q = q2; // retry q2 with twice the radius.
		t._points_left.push_back(q.size());
	}// findmore
	message(bLOG, "%d/%d. done added %d new edges\n", q.size(), keypoints.size(), _target_edgs.size());
	message(bDEBUG, "external edges/weight %d/%f\n", extcnt, extwt);

	// map the remaining
	for(auto k : q){
		_p2g[k] = nv++;
	}

//	if(internal_edges)... INCOMPLETE
#if 1
	connect_internally(_target_edgs, keypoints, myfreak, *this);
	connect_internally_eucl(_target_edgs, keypoints, myfreak, *this, eucl_base);
#else
#endif
} // framesgraph::find_te
#endif
/*--------------------------------------------------------------------------*/
template<class G, class W>
void framesgraph<G, W>::import_target(frame_type const & frm, frame_type const& silfrm,
		unsigned thresh_collapse, unsigned thresh_edge, double similarity_sigma,
	  	float target_px_mm, bool internal_edges, fgstats_type& stats, double eucl_base)
{

#ifndef USE_NESTED_MAP
	message(bDEBUG, "import target w/o feat_map\n");
	return import_frame(frm, silfrm,
		 thresh_collapse, thresh_edge, "nolabelyet", NULL);
#endif
	message(bDEBUG, "feat_map\n");
	std::vector<cv::KeyPoint> keypoints;

	unsigned start=::boost::num_vertices(_g);

	assert(silfrm.rows == frm.rows);
	assert(silfrm.cols == frm.cols);
	unsigned ht=silfrm.rows;
	unsigned wd=silfrm.cols;
	message(bLOG, "fg import nv %d ht %d wd %d\n", start, ht, wd);

	//all cvs: 40/200 is too much (without clique)
	//unsigned thresh_collapse = 80; // pixels as close as this will be collapsed
	                               // (not the target image pixels)

#ifndef USE_FREAK
	W myweight(frm);
#endif

	keypoints.clear();
	fill_keypoints(keypoints, silfrm);

	unsigned size=keypoints.size();
	message(bDEBUG, "found %d kp, creating freakmap\n", size, _pattern_scale);
	_nvt=size;

	// visit all keypoints, create descriptor matrix...
	FREAKMAP myfreak(keypoints, frm, _pattern_scale * (target_px_mm/some_pixel_scaling));

	message(bDEBUG, "done freakmap\n");

	if(size!=keypoints.size()){ untested();
#ifdef DEBUG_FREAK
		for(auto x: keypoints){ untested();
				trace2("fillkp now", x.pt.x, x.pt.y);
		}
#endif
		// FREAK removes keypoints too close to the border
		// need to extend the scene more?!
		std::cerr << "freakframe?" << size << " " << keypoints.size() << " ps " << _pattern_scale << "\n";
		unreachable(); incomplete();
		// currently, we lose a few annotated pixels...
	}

	// go through pixels in that frame
	// TODO:linearize
	// display_grayscale_frame("silhouette", silfrm);
	// display_grayscale_frame("worm", frm);
	unsigned wormpixels=0;
	wormpixels = keypoints.size();

			{
#ifndef NDEBUG
#ifdef USE_NESTED_MAP
				auto i=_feat_stash.recount();
				unsigned nv=::boost::num_vertices(_g);
				std::cout << "found " << nv << " verts in _g\n";
				std::cout << "found " << i << " verts in feat stash\n";
				FGassert(i==nv);
#endif
#endif
			}

#ifdef USE_NESTED_MAP
	unsigned nv=::boost::num_vertices(_g);
	_p2g.resize(0);
	--nv;
	_target_edgs.clear();
#endif
	// keypoints are exactly the silhouette points.
//	unsigned ne_before=::boost::num_edges(_g);

	if(1){
		message(bDEBUG, "calling find_te\n");
		find_te(keypoints, myfreak, thresh_edge, internal_edges, stats, eucl_base);
	}else{ untested();
		unsigned found=0;
		found = find_nn(keypoints, myfreak, thresh_edge);
		while(found<keypoints.size()/2){ untested();
			message(bLOG, ",");
			message(bWARNING, "find_nn %d unsuccessful, retry\n", thresh_edge);
			thresh_edge *= 2;
			found = find_nn(keypoints, myfreak, thresh_edge);
		}
	}

#ifdef USE_NESTED_MAP
	message(bDEBUG, "%d target edges (may contain dups)\n", _target_edgs.size());
#endif

//	display_sil_bb("learnsil", silfrm);
//	std::cout << "learn silouette nonzero " << countNonZero(silfrm) << "\n";

	assert(wormpixels);
	message(bDEBUG, "import_target worm with %d px\n", wormpixels);
	message(bLOG, std::to_string(_target_edgs.size()) + " new edges\n");
} // import_target
/*--------------------------------------------------------------------------*/
template<class FG>
void import_target(FG& fg, frame_type const & frm, frame_type const& silfrm,
	  	ConfigParameters const& parameters)
{ untested();
	unsigned thresh_edge = parameters.get<unsigned>("thresh-edge");
	unsigned thresh_collapse = parameters.get<unsigned>("thresh-collapse");
	double similarity_sigma = parameters.get<double>("similarity-sigma");

	fg.import_target(frm, silfrm, thresh_edge, thresh_collapse, similarity_sigma);
}
/*--------------------------------------------------------------------------*/

} // draft
#endif
