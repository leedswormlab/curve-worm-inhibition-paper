#ifndef MATRIX_HH
#define MATRIX_HH

#include <cassert>
#include <iostream>
#include <utility>
#include <vector>
#include <deque>

#include "mesh.hh"
// #include "entity.hh"
#include "modelbase.hh"
#include "parameter.hh"
#include "timeprovider.hh"
//#include "model.hh"
#include "umfpackmatrix.hh"
//#include <boost/graph/graph_traits.hpp>
//
#ifdef HAVE_ARPACK_PP
#include <ardsmat.h>
#include <ardssym.h>
#endif


class UmfpackMatrixHolder;

class SquareMatrixFootprint {
  // for each column, store an array of nonzero indices
  typedef unsigned ColumnIdType;
  typedef std::deque<ColumnIdType> RowType;
  typedef std::vector< RowType > DataType;

public:
  SquareMatrixFootprint( unsigned int n )
    : _data(n), _n(n), _nz(0)
  {}

  void clear() {
    for( auto& col : _data ) {
	col.clear();
    }
  }

  void print( std::ostream& s ) const
  {
    s << "***** WARNING: THIS IS THE TRANSPOSE *****" << std::endl;

    for( auto row : _data )
      {
	if( row.empty() )
	  std::cout << "[empty row]";

	// first sort each row
	std::sort( row.begin(), row.end() );

	for( auto p : row )
	  s << " " << p << ", " ;
	s << std::endl;
      }
  }

  void mark( unsigned row, unsigned col) {
//    trace3("mark", row, col, _n);
    assert(row<_n);
    assert(col<_n);
    RowType& coldata = _data[col];

    for( auto& r : coldata ) {
      if( r == row ) {
	return;
      }else{
      }
    }

    ++_nz;
    coldata.push_back(row);
  }


  unsigned int n() const { return _n; }
  unsigned int nz() const { return _nz; }
  unsigned int cols() const { return _n; }
  unsigned int rows() const { return _n; }

  DataType const& col_unordered(unsigned);

  // HACK
  DataType const& data() const{ return _data; }

private:
  DataType _data;
  const unsigned int _n;
  unsigned _nz; // num nonzeroes?
}; // SquareMatrixFootprint

// the binary layout used in UMFPACK
template<class D, class I>
class CompressedColumnMatrix{
public:
  CompressedColumnMatrix( SquareMatrixFootprint const& m )
    : _cols(m.cols()), _nz(0)
  {
    int r=0;
    _Ap.push_back( r );
    for( auto row : m.data() ) {
      std::sort( row.begin(), row.end() );

      for( auto const& ind : row ) {
	_Ai.push_back( ind );
	++r;
	++_nz;
      }

      // add end of row index
      _Ap.push_back(r);
    }
    _Ax.resize(_Ai.size());
  }

  void add(unsigned row, unsigned col, double v, unsigned hint){
    assert(pos(row, col) == int(hint));
    // trace3("", _Ax.data(), hint, v);
    assert(hint<_Ax.size());
    _Ax[hint]+=v;
  }
// find the position of row/col pair in compressed blob
  I pos(unsigned row, unsigned col) const{
    assert(col+1<_Ap.size());
    I b=_Ap[col];
    I e=_Ap[col+1];

    auto l=std::lower_bound(_Ai.begin()+b, _Ai.begin()+e, row);
    assert(*l==int(row));
    I p = &*l - _Ai.data();
//    trace3("pos", row, col, p);
    return p;
  }
  unsigned cols()const{
    return _cols;
  }
  void clear(){
    std::fill(_Ax.begin(), _Ax.end(), 0.);
  }

  // HACK
  template<class M>
  void matrix(M& m) const{
    m._Ap=(int*)_Ap.data();
    m._Ai=(int*)_Ai.data();
    m._Ax=_Ax.data();
    m.nz=_nz;
    m.n=_cols;
    assert(_Ap.size()==1+_cols);

  }
  inline void print( std::ostream& s ) const
  {
    s << "***** WARNING: THIS IS THE TRANSPOSE *****" << std::endl;

    auto row = _Ap.begin();
    auto next=row;
    ++next;
    int count = 0;
    for( ; next!=_Ap.end(); row=next, ++next) {

      s << count++ << ": ( ";
      for( int i=*row; i!=*next; ++i ){
	if(_Ax[i]){
	  s<< _Ai[i] << " " << _Ax[i] << ", ";
	}else{
	}
      }
      s<< ")\n";
    }
  }


  // res+=A*x
  template<class V>
  V& call(std::vector<D> const& x, V& res) const{
    assert(res.size()==x.size());
    for(int col=0; col<_cols; col++){
      for(int j=_Ap[col];j<_Ap[col+1]; j++) {
	res[_Ai[j]] += _Ax[j]*x[col];
      }
    }
    return res;
  }


  unsigned size() const{return _Ap.size()-1;}
  I const* Ap() const{return _Ap.data();}
  I const* Ai() const{return _Ai.data();}
  D const* Ax() const{return _Ax.data();}
private:
  std::vector<I> _Ap; //colptr
  std::vector<I> _Ai; //row ind
  std::vector<D> _Ax;  //value.d
  int _cols;
  int _nz;
};

inline std::ostream& operator<<(std::ostream& o, CompressedColumnMatrix<double, int> const& m)
{
  m.print(o);
  return o;
}

template<class FLOAT>
class COLROW_DENSE_MATRIX{
public:
   COLROW_DENSE_MATRIX(unsigned rows, unsigned cols=0)
      : _data(rows*(cols?cols:rows)), _rows(rows)
   {
   }

public:
   FLOAT& operator[](unsigned i){
      return _data[i];
   }
private:
   std::vector<FLOAT> _data;
   unsigned _rows;
};


class MeshMatrix : public UmfpackMatrix {
  typedef UmfpackMatrix BaseType;

public:
  MeshMatrix( const Mesh& mesh )
    : BaseType( mesh.N() ), mesh_( mesh )
  {}

  MeshMatrix( const Mesh& mesh, const unsigned int n )
    : BaseType( n ), mesh_( mesh )
  {}

  // this seems to operate on the left upper block.
  void setDirichletBoundaries() {
    const int dim = n() / mesh().N();

    for( int d = 0; d < dim; ++d )
      {
	setDirichletRow( 0*dim + d );
	setDirichletRow( (mesh().N()-1)*dim + d );
      }
  }

  virtual void assemble() { incomplete(); }
  void assemble( const bool ) { std::cerr << "not me!" << std::endl; }

protected:
  const Mesh& mesh() const
  {
    return mesh_;
  }

private:
  const Mesh& mesh_;
};

template< class PositionMeshFunction >
class MassMatrix : public MeshMatrix {
private:
  typedef typename PositionMeshFunction :: RangeVectorType RangeVectorType;

public:
  MassMatrix( const Mesh& mesh, const PositionMeshFunction& X )
    : MeshMatrix( mesh, X.dim * mesh.N() ), X_( X )
  {
    assemble();
  }

  // MassMatrix::
  void assemble() {
    // clear first
    clear();

    // element loop
    for( unsigned int e = 0; e < mesh().N()-1; ++e )
      {
	// construct area element
	const RangeVectorType xe = X_.evaluate( e );
	const RangeVectorType xep = X_.evaluate( e+1 );
	const double q = ( xe - xep ).norm();

	// find value
	const double value = 0.5 * q;

	// add to vertex in this element
	for( unsigned int d = 0; d < X_.dim; ++d )
	  {
	    for( unsigned int ie = 0; ie < 2; ++ie )
	      {
		add( X_.dim*(ie+e) + d, X_.dim*(ie+e) + d, value );
	      }
	  }
      }
  }

  void setDirichletBoundaries() {
    for( unsigned int d = 0; d < X_.dim; ++d ) {
	setDirichletRow( d );
	setDirichletRow( mesh().N()-1+d );
    }
  }


private:
  const PositionMeshFunction& X_;
};

template< class PositionMeshFunction >
class InverseMassMatrix : public MeshMatrix{
private:
  typedef typename PositionMeshFunction :: RangeVectorType RangeVectorType;
public:
  InverseMassMatrix( const Mesh& mesh, const PositionMeshFunction& X )
    : MeshMatrix( mesh, X.dim * mesh.N() ), X_( X )
  {
    assemble();
  }

  void assemble() {
    clear();

    // element loop
    for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
	// construct area element
	const RangeVectorType xe = X_.evaluate( e );
	const RangeVectorType xep = X_.evaluate( e+1 );
	const double q = ( xe - xep ).norm();

	// find value
	double value = 0.5 * q;

	if( e > 0 ) {
	    // construct previous area element
	    const RangeVectorType xem = X_.evaluate( e-1 );
	    const double qm = ( xe - xem ).norm();

	    // find value
	    value += 0.5 * qm;
	}

	// add to vertex in this element
	for( unsigned int d = 0; d < X_.dim; ++d ) {
	    add( X_.dim*e + d, X_.dim*e + d, 1.0/ value );
	}
    }
  }

private:
  const PositionMeshFunction& X_;
}; // InverseMassMatrix

// the matrix that turns positions into curvature.
template< class PositionMeshFunction >
class StiffnessMatrix : public MeshMatrix{
private:
  typedef typename PositionMeshFunction :: RangeVectorType RangeVectorType;

public:
  StiffnessMatrix( const Mesh& mesh, const PositionMeshFunction& X )
    : MeshMatrix( mesh, X.dim * mesh.N() ), X_( X )
  {
    assemble();
  }

  // tmp HACK, double arg
  // why assemble?
  void assemble()
  {
    // clear first
    clear();

    // element loop
    for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
	// construct area element
	const RangeVectorType xe = X_.evaluate( e );
	const RangeVectorType xep = X_.evaluate( e+1 );
	const double q = ( xe - xep ).norm();

	// find value
	const double value = 1.0 / q;

	// add to vertex in this element
	for( unsigned int d = 0; d < X_.dim; ++d )
	  {
	    // diagonal terms
	    for( unsigned int ie = 0; ie < 2; ++ie )
	      {
		add( X_.dim*(ie+e) + d, X_.dim*(ie+e) + d, value );
	      }

	    // off diagonal terms
	    add( X_.dim*(e) + d, X_.dim*(e+1) + d, -value );
	    add( X_.dim*(e+1) + d, X_.dim*(e) + d, -value );
	  }
      }
  }

private:
  const PositionMeshFunction& X_;
}; // StiffnessMatrix

template < class M >
class SubMatrix {
public:
  SubMatrix( unsigned r, unsigned c, M& m)
    : _r(r), _c(c), _m(m) {
    }
public:

  void allocate_load(unsigned r, unsigned c, double v){
    _m.allocate_load(r, c, v);
  }
private:
  unsigned _r;
  unsigned _c;
  M& _m;
}; // SubMatrix


// hmm systemMatrix should be a block matrix.
// then allocate/stamp the blocks...
template < class PositionMeshFunction, class SolverType >
// template < class MatrixBackend, class MatrixSolver >
class SystemMatrix { //  : public MeshMatrix
public:
  typedef CompressedColumnMatrix<double, int> BackendType;
  typedef typename PositionMeshFunction :: RangeVectorType RangeVectorType;
  static constexpr unsigned dim=RangeVectorType::dim;
  using ModelType = ModelInterface<dim>;
public: // construct
  SystemMatrix()
    : implicit_( true ),
      assembled_( false ),
      _extraforcing(true), //??
      _matrix_backend(NULL),
      _matrix_solver(NULL)
  {
  }

  void add( unsigned row, unsigned col, const double val, unsigned hint ) {
    assert(_matrix_backend);
    return _matrix_backend->add(row, col, val, hint);
  }
#if 0
  // swap row/col
  void allocate_load(unsigned row, unsigned col, double val){
    assert(_matrix_backend);
    return _matrix_backend->add(row, col, val);
  }
#endif

  // matrix-vector multiplication, then add more
  template< class Vector >
  void call( const Vector& x, Vector& b ) const {
    // incomplete();
    _matrix_backend->call( x, b );
#if 0
    assert(assembled_);
    if( not assembled_ ){
      throw "system matrix not assembled";
    }else{
    }

    // calls UmfpackMatrix::call. seems to refresh curvature.
    // x is always oldsolutiontuple here?
    assert(_matrix_backend);
    _matrix_backend->call( x, b );

    if(implicit_ ) {
       unreachable();
       // how does it make sense?
    }else{
	// extract sub-vectors
	const auto& X = x.position(); // actually oldposition
	auto& rhsY = b.curvature();
	auto& rhsP = b.pressure();

	// element loop
	for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
	    // extract geometry
	    const double s = 0.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
	    const auto xe = X.evaluate( e );
	    const auto xep = X.evaluate( e+1 );
	    const double q = ( xe - xep ).norm();
	    RangeVectorType nu = ( xe - xep ).perp();
	    nu /= q;

	    // integrate position and curvature forcing
	    for( unsigned int ie = 0; ie < 2; ++ie ) {
//		const auto xie = X.evaluate( e + ie );
		const auto sie = mesh().u( e + ie );

		RangeVectorType valueY;
		for( unsigned int d = 0; d < X.dim; ++d ) {
		  valueY[ d ] = 0.5 * q * model().beta( sie ) * nu[ d ];
		}

		/// TODO: stamp from worm.
		rhsY.add( e+ie, valueY );
	    }

	    // integrate pressure forcing
	    if( model().constraint() == ModelType :: Constraint :: measure ){
	      rhsP[e] = mesh().h( e+1 ) * -model().gamma( s );
	    }else{
	    }
	  }
      }
#endif
  } // call

private:
//  bool velocitymodel() const{
//    return model().constraint() == ModelType :: Constraint :: velocity ;
//  }
public:
  unsigned int n() const {
    assert(_matrix_backend);
    return _matrix_backend->cols();
  }
  unsigned int cols() const { return n(); }
  unsigned int rows() const { return n(); }

  void setDirichletBoundaries( /* blockaddress? */) {
    incomplete();
#if 0
    const int dim = n() / mesh().N();

    for( int d = 0; d < dim; ++d )
      {
	_matrix_backend->setDirichletRow( 0*dim + d );
	_matrix_backend->setDirichletRow( (mesh().N()-1)*dim + d );
      }
#endif
  }

  void clear(){
    assert(_matrix_backend);
    _matrix_backend->clear();
  }

  // SystemMatrix::
  template<class F>
  void init(F const& footprint) {
    if(_matrix_backend){
      _matrix_backend->clear();
    }else{
      _matrix_backend = new BackendType(footprint);
    }
  }

  void assemble() {
    // too early for symbolic_solve...
//    double deltaT = model().timeProvider().deltaT(); // FIXME/ 20.;

#if 0
    // walk mesh
    for( unsigned int e = 0; e < mesh().N()-1; ++e ) {
	// model parameters
	//
	// s, the center coordinate...?
	const double s = 0.5 * ( mesh().u( e ) + mesh().u( e+1 ) );
//	const double K = model().K( s );

	// construct area element and tangent
	const RangeVectorType xe = X_.evaluate( e );
	const RangeVectorType xep = X_.evaluate( e+1 );
	RangeVectorType tau = ( xep - xe );
	const double q = tau.norm();
trace4("matrix X", e, xe,xep, q);	
	tau /= q;
	const RangeVectorType ye = Y_.evaluate( e );
	const RangeVectorType yep = Y_.evaluate( e+1 );

#if 0
	const double energyDensity = model().energyDensity_at( (xe + xep)*0.5,
							    (ye + yep)*0.5, s );

	// add to vertex in this element
	for( unsigned int k = 0; k < X_.dim; ++k ) {
	    for( unsigned int l = 0; l < X_.dim; ++l ) {
		// projection matrix

		if( implicit_ ){
		   // double Ptau = ( (double) (k == l) - tau[l]*tau[k] );
		   // load_implicit(e, k, l, Ptau, q, energyDensity, s);
		}else{
		}
	      } // l loop
	  }
#endif
      } // meshwalk
#endif

    // set flag
    assembled_ = true;
  }

  double* ptr(unsigned c, unsigned r){
    assert(assembled_);

    incomplete();
    return NULL;
  }
  unsigned pos(unsigned row, unsigned col) const{
    assert(_matrix_backend);
    return _matrix_backend->pos(row, col);
  }

  template<class Vector >
  void solve(Vector&x, const Vector& b ){
    assert(_matrix_backend);
    if(_matrix_solver){
    }else{
      _matrix_solver = new SolverType(*_matrix_backend);
    }
    _matrix_solver->solve(*_matrix_backend, x, b);
  }

protected:
#if 0
  ModelInterface<dim> const& model() const {
    auto mm=&_entity.model();
    ModelInterface<dim> const* m=prechecked_cast<ModelInterface<dim> const*>(mm);
    assert(m);

    return *m;
  }
#endif

private:
//  const PositionMeshFunction& X_;
//  const PositionMeshFunction& Y_;
  const bool implicit_;

  bool assembled_;
  bool _extraforcing;

public: // free?!
  void print() const{untested();
    return print(std::cout);
  }
  template<class O>
  void print(O& o) const{untested();
    if(_matrix_backend){ untested();
      // BUG. use free access
      return _matrix_backend->print(o);
    }else{ untested();
    }
  }

private:
  BackendType* _matrix_backend;
  SolverType* _matrix_solver;
public:

  template<class Matrix, class Vector >
  friend void solve( const Matrix& A, Vector&x, const Vector& b );
}; // SystemMatrix

template<class Matrix, class Vector >
void solve( const Matrix& A, Vector&x, const Vector& b )
{
  umfpackSolve(*A._matrix_backend, x, b);
}

template<class FLOAT>
class DIAGONAL_MATRIX{
public: //types
	enum SQRTDEG {_SQRTDEG};
	enum SOMETHING {_SOMETHING};
private: // don't construct
	DIAGONAL_MATRIX(){unreachable();}
	DIAGONAL_MATRIX(const DIAGONAL_MATRIX&){incomplete();}
public: // construct
	template<class G>
	DIAGONAL_MATRIX(const G& g, SQRTDEG);


	~DIAGONAL_MATRIX(){
	}
public:
	unsigned rows() const{return _data.size();}
	unsigned cols() const{return _data.size();}
	const FLOAT& operator[](unsigned i) const{
		return _data[i];
	}
	FLOAT& operator[](unsigned i){
		return _data[i];
	}
	FLOAT const & get(unsigned i, unsigned j) const{
		if(i==j){
			return _data[i];
		}else{
			assert(!_zero);
			return _zero;
		}
	}
private:
	std::vector<FLOAT> _data;
	FLOAT _zero;
};

#ifdef HAVE_ARPACK_PP

template<class FLOAT>
class SYM_DENSE_MATRIX : public ARdsSymMatrix<FLOAT>{
public: // types
	typedef ARdsSymMatrix<FLOAT> base;
private: // don't construct
	SYM_DENSE_MATRIX(){unreachable();}
	SYM_DENSE_MATRIX(const SYM_DENSE_MATRIX&){incomplete();}
public: // construct
	SYM_DENSE_MATRIX(unsigned n)
	    : base(n, new FLOAT[n*(n+1)/2])
	{
	}
	SYM_DENSE_MATRIX(FLOAT d, unsigned n)
	    : base(n, new FLOAT[n*(n+1)/2])
	{
		unsigned skip=dim();
		unsigned k=0;
		for(unsigned i=0; i<dim(); ++i){
			base::A[k] = d;
			k+=skip;
			--skip;
		}
	}
	template<class G>
	SYM_DENSE_MATRIX(G const& g);

	~SYM_DENSE_MATRIX(){
		delete[] base::A;
	}

	// hack. don't use
	FLOAT* data() { return base::A; }

	void conjugate(DIAGONAL_MATRIX<FLOAT> const& d){
		unsigned skip=0;
		unsigned k=0;
		for(unsigned i=0; i<unsigned(base::n); ++i){
			for(unsigned j=skip; j<dim(); ++j){
				base::A[k++]*=d[i] * d[j];
			}
			++skip;
		}
	}
public:
#if 0
	SYM_DENSE_MATRIX& operator-=(DIAGONAL_MATRIX const& o){
	}
#endif
	SYM_DENSE_MATRIX& operator-=(SYM_DENSE_MATRIX const& o){
		assert(dim()==o.dim());
		for(unsigned i=0; i<datasize(); ++i){
			base::A[i]-=o.A[i];
		}
		return *this;
	}
public:
	unsigned rows() const{return base::n;}
	unsigned cols() const{return base::n;}
	FLOAT get(unsigned r, unsigned c) const{
		if(r<c){
			std::swap(r,c);
		}else{
		}
		unsigned i=dim()*c - c*(c-1)/2 + r-c;
		return base::A[i];
	}
private:
	unsigned dim() const{return base::n;}
	unsigned datasize() const{return dim()*(dim()+1)/2;}
	void set(unsigned r, unsigned c, FLOAT f){
		if(r<c){
			std::swap(r,c);
		}else{
		}
		unsigned i=dim()*c - c*(c-1)/2 + r-c;
		trace4("set", r, c, i, f);
		base::A[i]=f;
	}


private:
	template<class G>
	unsigned area(G const& g);
};
#endif // ARPACK_PP

#endif // #ifndef MATRIX_H
// vim:ts=8:sw=2:
