#ifndef MODEL_DISTANCE_H
#define MODEL_DISTANCE_H

#include "model.hh"

// is this worm or generic?!
template< const unsigned int mydim, const unsigned int nc >
struct DistanceModel : public ModelDefault< mydim > {
private:
  using BaseType = ModelDefault< mydim >;
public:
  static const unsigned int dim = BaseType :: dim;
  static const unsigned int nCam = nc;
  using RangeVectorType = typename BaseType :: RangeVectorType;
  using WorldCoTangent = typename BaseType::WorldCoTangent;
  using WorldCoordinate = typename BaseType::WorldCoordinate;

  using DistanceFunctionType = DistanceFunction<dim>;

  DistanceModel( ConfigParameters const& parameters, DistanceFunctionType& distanceFunction,
		 const TimeProvider& timeProvider, const TimeProvider& imageTimeProvider,
		 const double& gamma )
    : BaseType( const_cast<ConfigParameters&>(parameters) // BUG
			 ),
      distance_( distanceFunction ),
      imageTimeProvider_( imageTimeProvider ),
      lastFrameWithRelaxation_( parameters.get<double>( "model.e.firststep.lastframewithrelaxation") ),
      firstStepMaxPenality_( parameters.get<double>( "model.e.firststep.maxpenality") ),
      firstStepRelaxationTime_( parameters.get<double>( "model.e.firststep.relaxationtime") ),
      myGamma_( gamma ),
     _contract_tau  /*= 1e-2*/ ( parameters.get<double>( "gamma.contract-tau") ),
     _contract_scale ( parameters.get<double>( "gamma.contract-scale") )
  {
    message(bDEBUG, "running model with gamma = %f\n", myGamma_);
  }

  // initial values. TODO: does not work
  virtual RangeVectorType X0( const double s, const Entity& e) const
  { untested();
    RangeVectorType ret = distance_.middle();
    ret[ 1 ] += gamma(s, e)*(s-0.5);

    return ret;
  }

  // environmental force at X
  virtual WorldCoTangent Force( const WorldCoordinate& x ) const
  {

    // incomplete/obsolete?
    // normaldistance is pointing from closest keypoint to x
    RangeVectorType ret = distance_.normalDistance( x );
    ret *= -1.0;

#if 0
    RangeVectorType rhs
    for(auto const& f : _forces){ untested();
      f->extraForcing( X, rhs );
    }
#endif

   // _energy=
   // const double d = distance_.distance( X );
   // const double fid = 0.5 * d*d; ...?

    return ret;
  }

  // could be
  // virtual WorldCoTangent {internal/external}Force( const& thing, internalCoordinate ) const
  // that will be inefficient: the internalCoordinate is float...
  // ... need to rethink

  // this is a HACK, mixing internal and external stuff.
  // s is the internal coordinate....
  // hmm, is Y a WorldTangent?
  // DistanceModel::
  double energyDensity( //  const WorldCoordinate& X, const RangeVectorType& Y,
      double const& s, Entity const& e) const
  { untested();

    MeshObject<dim> const* m = prechecked_cast<MeshObject<dim> const*>(&e);
    assert(m);

// !! similar to Force, but
//   * raw distance (== | normaldistance | ?)
//   * adds regularisation.

    // fidelity term
    unsigned ee = s*(m->N()-1)+1e-12; // HACK.
    const double d = distance_.distance( m->position().evaluate(ee) );
    const double fid = 0.5 * d*d;

//     for i in forces:
//       addenergy

    // curvature term
    const double y = m->curvature().evaluate(ee).norm();
    const double curvature = 0.5 * regularisation(s, e) * y*y;

    return fid + curvature;
  }

  double energyDensity_at( const WorldCoordinate& X, const RangeVectorType& Y,
      double const& s, Entity const& e) const
  { untested();

//    MeshObject<dim> const* m = prechecked_cast<MeshObject<dim> const*>(&e);
//    assert(m);

// !! similar to Force, but
//   * raw distance (== | normaldistance | ?)
//   * adds regularisation.

    // fidelity term
//    unsigned ee = s*(m->N()-1)+1e-12; // HACK.
    const double d = distance_.distance( X );
    const double fid = 0.5 * d*d;

//     for i in forces:
//       addenergy

    // curvature term
//    const double y = m->curvature().evaluate(ee).norm();
    const double curvature = 0.5 * regularisation(s, e) * (Y*Y);

    return fid + curvature;
  }

#if 0 // oldcode.
  double energyDensity( const RangeVectorType& X, const RangeVectorType& Y ) const
  {
    // fidelity term
    const double fid = 0.5 * distance_.distance( X );

    // curvature term
    const double curvature = 0.5 * this->e(0) * Y.norm();

    return fid + curvature;
  }
#endif

  template< class DF >
  // typedef DiscreteFunctionInterface<2> DF;
  double extraEnergy( const DF& X ) const
  { incomplete(); // need sim of forces, then enrgy?
#ifdef OLDFORCES
    return distance_.extraEnergy( X );
#else
    double ret=0.;
    for(auto const& f : _forces){ untested();
      ret += f->extraEnergy( X );
    }
    return ret;
#endif
  }

#if 0
  template< class DF >
  void extraForcing_template( const DF& X, DF& rhs ) const
  { untested();
    // for each redpoint, stamp a force.
#ifdef OLDFORCES
    distance_.extraForcing( X, rhs );
#else
    for(auto const& f : _forces){ untested();
      f->extraForcing( X, rhs );
    }
#endif
  }
#endif

  typedef DiscreteFunctionInterface<mydim> DFf;
  void extraForcing( const DFf& X, DFf& rhs ) const
  { untested();
    for(auto const& f : _forces){ untested();
      f->extraForcing( X, rhs );
    }
  }

  virtual double beta( double const& s, const Entity& e ) const {
    return 0;
  }

 //  double reg_scale_radial(double r) const{ untested();
 //    return 1. - r*r/.5;
 //  }
  typedef double MeshCoordinate;

  // DistanceModel::
  virtual double regularisation( MeshCoordinate const& s, Entity const& e ) const {
    double basereg = BaseType::regularisation(gammascale(s, e) * (s-.5) + .5, e);

    if( imageTimeProvider_.iteration() > lastFrameWithRelaxation_ ){ itested();
      basereg *= ( 1.0 + firstStepMaxPenality_ * exp( - e.time() / firstStepRelaxationTime_ ) );
    }else{ itested();
    }
	 return basereg;
  }

  virtual double gammascale( const double /*s*/, Entity const& e) const {
    double t = e.time()/_contract_tau;

    // this is an insane hack.
    // only works because it breaks convergence,
    // the iteration does not break.
    return 1. - _contract_scale * t * exp( -t );
  }

  virtual double gamma( const double s, Entity const& e) const { untested();
    return myGamma_ * gammascale(s, e);
  }

#if 0 // not model
  std::vector<double> const& energyVector() const{ untested();
    return _energyVector;
  }
#endif
public:
  void attach_force(ForceFunctionBase<3/* worlddim? */> const& f){ untested();
    _forces.push_back(&f);
    message(bLOG, "now have %d forces\n", _forces.size());
  }
private:
  DistanceFunctionType& distance_;
  std::vector<ForceFunctionBase<3> const*> _forces;
  const TimeProvider& imageTimeProvider_;

  const unsigned lastFrameWithRelaxation_;
  const double firstStepMaxPenality_;
  const double firstStepRelaxationTime_;

  const double &myGamma_;
  const double _contract_tau;
  const double _contract_scale;

}; // DistanceModel

#endif // guard
