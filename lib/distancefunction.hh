#ifndef DISTANCEFUNCTION_HH
#define DISTANCEFUNCTION_HH

#include <functional>
#include <array>

#include "vector.hh"
#include "projectionoperator.hh"
#include "discretefunction.hh"

// bug. don't use cv here.
#include "image/util.hh"

#include <opencv2/calib3d.hpp>

#if 0
template<class X>
void get_point(X& x)
{
  static_assert(!sizeof(X));
}
#endif



template< const unsigned int mydim >
struct DistanceFunction
{
  typedef DiscreteFunctionInterface<2> DF;
  static const unsigned int dim = mydim;
  using RangeVectorType = RangeVector< dim >;

  virtual RangeVectorType middle() const = 0;
  virtual double distance( const RangeVectorType& /*x*/ ) const = 0;
  virtual double distance( const RangeVectorType& /*x*/, std::vector< double >& /*list*/ ) const{
    unreachable(); // should be pure.
    return 0.;
  }
  virtual RangeVectorType normal( const RangeVectorType& /*x*/ ) const = 0;
  virtual RangeVectorType normalDistance( const RangeVectorType& /*x*/ ) const = 0;

  // does not work. need one for dim, and one for dim+1?
  // virtual double extraEnergy( const DF& /*X*/ ) const = 0;
  virtual double extraEnergy( const PiecewiseLinearFunction<dim>& /*X*/ ) const
  {
    unreachable();
    return 0;
  }

  virtual void extraForcing( const PiecewiseLinearFunction<dim>& /*X*/, PiecewiseLinearFunction<dim>& /*f*/ ) const
#if 0 // not yet
    = 0;
#else
  { incomplete(); // BUG?
    // throw "distance function should be specialised";
   }
#endif

};

template< class Distance, class Projection, const unsigned int myncams = 1 >
struct ProjectedDistanceFunction
: public DistanceFunction< Projection::worlddim >
{
  static const unsigned int nCams = myncams;
  static_assert( Projection::projecteddim == Distance::dim,
		 "dimension mismatch between projection operator and distance function");
  static const unsigned int worlddim = Projection::worlddim;
  static const unsigned int projecteddim = Projection::projecteddim;

  using BaseType = DistanceFunction< worlddim >;
  using BaseDistanceFunctionType = Distance;
  using ProjectionOperatorType = Projection;

  using RangeVectorType = typename BaseType :: RangeVectorType;
  using BaseRangeVectorType = typename BaseDistanceFunctionType :: RangeVectorType;
  using WorldRangeVectorType = typename ProjectionOperatorType :: WorldRangeVectorType;

  ProjectedDistanceFunction(
      std::vector<BaseDistanceFunctionType>& baseDistance,
      const std::vector<ProjectionOperatorType>& projection )
    : baseDistance_( baseDistance ), projection_( projection )
  {}

  virtual RangeVectorType middle() const
  {
    // find middle of projected images
    std::vector< BaseRangeVectorType > middles;
    for( auto const& d : baseDistance_ ) {
	middles.push_back( d.middle() );
    }

    RangeVectorType ret;
    const double me = triangulate( middles, ret );
    (void) me;
    return ret;
  }

  template< class V >
  double triangulate( const V &middles,
		      RangeVectorType &middle3d ) const
  {
    return free_triangulate(middles, middle3d, projection_);
  }


  virtual double distance( const RangeVectorType& x ) const { itested();
    double ret = 0;
    for( unsigned int cam = 0; cam < nCams; ++cam ) { itested();
	const BaseRangeVectorType px = projection_.at( cam )( x );
	ret += baseDistance_.at( cam ).distance( px );
    }
    return ret;
  }

  virtual double distance( const RangeVectorType& x, std::vector< double >& list ) const
  {
    list.clear();

    double ret = 0;
    for( unsigned int cam = 0; cam < nCams; ++cam )
      {
	const BaseRangeVectorType px = projection_.at( cam )( x );
	const double d = baseDistance_.at( cam ).distance( px );
	ret += d;
	list.push_back( d );
      }
    return ret;
  }

  virtual RangeVectorType normal( const RangeVectorType& x ) const
  {
    RangeVectorType ret(0);

    for( unsigned int cam = 0; cam < nCams; ++cam )
      {
	const auto px = projection_.at( cam )( x );
	const auto n = baseDistance_.at( cam ).normal( px );
	const auto r = projection_.at( cam ).jacobian( x, n );

	ret += r;
      }

    return ret;
  }

  // this is turned into a force, called once per midline sample point.
  // ProjectedDistanceFunction::
  virtual RangeVectorType normalDistance( const RangeVectorType& x ) const {
    RangeVectorType ret(0);

    for( unsigned int cam = 0; cam < nCams; ++cam ) { itested();
	auto px=projection_[cam]( x );
	typename BaseDistanceFunctionType::RangeVectorType n(0);
#if 1
	baseDistance_[cam].normal_distance( px, n);
#else
	// done? move to different class.
	baseDistance_[cam].gravity_force( px, n);
#endif

	WorldRangeVectorType r=projection_[cam].jacobian( x, n );
	ret += r;
    }

    return ret;
  }

  virtual void extraForcing( PiecewiseLinearFunction<worlddim> const& X,
      PiecewiseLinearFunction<worlddim>& f ) const
  {
    incomplete();
    // assign data
    Vector pXdata( X.size() );
    Vector pFdata( f.size() );

    for( unsigned int cam = 0; cam < nCams; ++cam )
      {
	// clear data
	pXdata.clear();
	pFdata.clear();

	// project solution variable
	PiecewiseLinearFunction< projecteddim > pX( X.mesh(), SubArray< Vector >( pXdata.begin(), pXdata.end() ) );
	projection_.at( cam ).projectSolution( X, pX );

	// compute extra forcing in 2d
	PiecewiseLinearFunction< projecteddim > pF( X.mesh(), SubArray< Vector >( pFdata.begin(), pFdata.end() ) );
	baseDistance_.at( cam ).extraForcing( pX, pF );

	for( unsigned int j = 0; j < f.N(); ++j )
	  {
	    const auto pFj = pF.evaluate( j );
	    const auto Xj = X.evaluate( j );
	    auto fj = projection_.at( cam ).jacobian( Xj, pFj );

	    f.add( j, fj );
	  }
      }
  }

  // typedef DiscreteFunctionInterface<2> DF;
  // template<class DF>
  // double extraEnergy( const DF& X ) const
  virtual double extraEnergy( const PiecewiseLinearFunction<worlddim>& X ) const
  { untested();
    double ret = 0.0;

    // assign data
    Vector pXdata( X.size() );

    for( unsigned int cam = 0; cam < nCams; ++cam )
      {
	// clear data
	pXdata.clear();

	// project solution variable
	PiecewiseLinearFunction< projecteddim > pX( X.mesh(), SubArray< Vector >( pXdata.begin(), pXdata.end() ) );
	projection_.at( cam ).projectSolution( X, pX );

	ret += baseDistance_.at( cam ).extraEnergy( pX );
      }

    return ret;
  }

  const std::vector<BaseDistanceFunctionType>& baseDistance() const {
    return baseDistance_;
  }

  const std::vector<ProjectionOperatorType>& projection() const
  {
    return projection_;
  }

private:
  std::vector<BaseDistanceFunctionType>& baseDistance_;
  const std::vector<ProjectionOperatorType>& projection_;
}; // ProjectedDistanceFunction

template< class Distance, class Projection >
struct ProjectedDistanceFunction< Distance, Projection, 1 >
  : public DistanceFunction< Projection::worlddim >
{
  static const unsigned int nCams = 1;
  static_assert( Projection::projecteddim == Distance::dim,
		 "dimension mismatch between projection operator and distance function");
  static const unsigned int worlddim = Projection::worlddim;
  static const unsigned int projecteddim = Projection::projecteddim;

  using BaseType = DistanceFunction< worlddim >;
  using BaseDistanceFunctionType = Distance;
  using ProjectionOperatorType = Projection;

  using RangeVectorType = typename BaseType :: RangeVectorType;
  using BaseRangeVectorType = typename BaseDistanceFunctionType :: RangeVectorType;

  ProjectedDistanceFunction( std::vector<BaseDistanceFunctionType>& baseDistance, const std::vector<ProjectionOperatorType>& projection )
    : baseDistance_( baseDistance.at(0) ), projection_( projection.at(0) )
  {}

  ProjectedDistanceFunction( BaseDistanceFunctionType& baseDistance, const ProjectionOperatorType& projection )
    : baseDistance_( baseDistance ), projection_( projection )
  {}

  virtual RangeVectorType middle() const
  {
    return projection_.inverse( baseDistance_.middle() );
  }
  virtual double distance( const RangeVectorType& x ) const
  {
    const BaseRangeVectorType px = projection_( x );
    return baseDistance_.distance( px );
  }
  template< class V >
  double triangulate( const V &middles,
		      RangeVectorType &middle3d ) const
  {
    incomplete();
    return 0;
  }

  virtual double distance( const RangeVectorType& x, std::vector< double >& list ) const
  {
    const double d = distance(x);
    list.push_back( d );
    return d;
  }
  virtual RangeVectorType normal( const RangeVectorType& x ) const
  {
    const auto n = baseDistance_.normal( projection_( x ) );
    const auto r = projection_.jacobian( x, n );
    return r;
  }

  virtual RangeVectorType normalDistance( const RangeVectorType& x ) const
  {
    const auto& r = this->normal( x );
    const auto& d = this->distance( x );
    return r * d;
  }

  // typedef DiscreteFunctionInterface<2> DF;
  // template<class DF>
  // double extraEnergy( const DF& X ) const
  virtual double extraEnergy( const PiecewiseLinearFunction<worlddim>& X ) const
  { untested();
    // assign data
    Vector pXdata( X.size() );
    // clear data
    pXdata.clear();

    // project solution variable
    PiecewiseLinearFunction< projecteddim > pX( X.mesh(), SubArray< Vector >( pXdata.begin(), pXdata.end() ) );
    projection_.projectSolution( X, pX );

    return baseDistance_.extraEnergy( pX );
  }

  virtual void extraForcing(PiecewiseLinearFunction<worlddim> const& X,
      PiecewiseLinearFunction<worlddim>& f ) const
  { untested();
    // assign data
    Vector pXdata( X.size() );
    Vector pFdata( f.size() );

    // clear data
    pXdata.clear();
    pFdata.clear();

    // project solution variable
    PiecewiseLinearFunction< projecteddim > pX( X.mesh(), SubArray< Vector >( pXdata.begin(), pXdata.end() ) );
    projection_.projectSolution( X, pX );

    // compute extra forcing in 2d
    PiecewiseLinearFunction< projecteddim > pF( X.mesh(), SubArray< Vector >( pFdata.begin(), pFdata.end() ) );
    baseDistance_.extraForcing( pX, pF );

    for( unsigned int j = 0; j < f.N(); ++j )
      {
	const auto pFj = pF.evaluate( j );
	const auto Xj = X.evaluate( j );
	auto fj = projection_.jacobian( Xj, pFj );

	f.add( j, fj );
      }
  }

private:
  BaseDistanceFunctionType& baseDistance_;
  const ProjectionOperatorType& projection_;
};

template< const unsigned int mydim, const unsigned int nCams >
struct TestDistanceFunctionS;

template< const unsigned int nCams >
struct TestDistanceFunctionS<3,nCams>
  : public DistanceFunction<3>
{
  using BaseType = DistanceFunction<3>;
  static const unsigned int dim = BaseType::dim;
  using RangeVectorType = typename BaseType::RangeVectorType;
  using ProjectedRangeVectorType = RangeVector<2>;

  TestDistanceFunctionS( const std::function< RangeVectorType(double) >& f )
    : f_( f )
  {}

  virtual RangeVectorType middle() const
  {
    RangeVectorType p(0);
    return p;
  }

  virtual double distance( const RangeVectorType& x ) const
  {
    double sum = 0;
    for( int cam = 0; cam < nCams; ++cam )
      {
	const auto px = proj( x, cam );
	sum += px.dist( closestPoint( x, cam ) );
      }

    return sum;
  }

  virtual double distance( const RangeVectorType& x, std::vector< double >& list ) const
  { untested();
    list.clear();

    double sum = 0;
    for( int cam = 0; cam < nCams; ++cam )
      {
	const auto px = proj( x, cam );
	const double d = px.dist( closestPoint( x, cam ) );
	sum += d;
	list.push_back( d );
      }

    return sum;
  }

#if 0
  virtual RangeVectorType normal( const RangeVectorType& x )
  {
	throw "not implemented"
  }
#endif

  virtual RangeVectorType normalDistance( const RangeVectorType& x ) const
  {
    RangeVectorType sum(0);

    for( int cam = 0; cam < nCams; ++cam )
      {
	ProjectedRangeVectorType px = proj( x, cam );

	ProjectedRangeVectorType p = closestPoint( x, cam );
	p -= px;
	p /= -p.norm();

	const double d = px.dist( p );
	p *= d;

	sum += dproj( p, cam );
      }

    return sum;
  }

  typedef DiscreteFunctionInterface<2> DF;
  virtual double extraEnergy( const DF& X ) const
  { untested();
    const auto& mesh = X.mesh();

    double ret = 0.0;
    double size = 0.0;

    for( int cam = 0; cam < nCams; ++cam )
      {
	for( unsigned int j = 0; j < mesh.N(); ++j )
	  {
	    const auto& pj = f( mesh.u(j), cam );

	    double minDist = 1e10;
	    for( unsigned int i = 0; i < X.N(); ++i )
	      {
		const auto& Xi = X.evaluate( i );
		const auto& Xpi = proj( Xi, cam );

		const double d = ( pj - Xpi ).norm();
		minDist = std::min( d, minDist );
	      }

	    ret += minDist * minDist;
	    size += 1.0;
	  }
      }

    double out = 0.5 * ret / size;
    return out;
  }

  virtual void extraForcing( const PiecewiseLinearFunction<dim>& X,
      PiecewiseLinearFunction<dim>& rhs ) const
  { untested();
    const auto& mesh = X.mesh();

    for( int cam = 0; cam < nCams; ++cam )
      {
	for( unsigned int j = 0; j < mesh.N(); ++j )
	  {
	    const auto &pj = f( mesh.u(j), cam );

	    double minDist = 1e10;
	    ProjectedRangeVectorType minXp;
	    int minI = -1;

	    for( unsigned int i = 0; i < mesh.N(); ++i )
	      {
		const auto Xi = X.evaluate( i );
		const auto Xpi = proj( Xi, cam );

		const double d = ( pj - Xpi ).norm();
		if( d < minDist )
		  {
		    minDist = d;
		    minXp = Xpi;
		    minI = i;
		  }
	      }

	    ProjectedRangeVectorType fp = pj - minXp;
	    fp /= fp.norm();
	    fp *= (minDist < 0.1) ? minDist : minDist * 1.0e-2;

	    RangeVectorType f = dproj( fp, cam );
	    rhs.add( minI, f );
	  }
      }

    return;
  }

protected:
  ProjectedRangeVectorType closestPoint( const RangeVectorType& x, const int cam ) const
  {
    const ProjectedRangeVectorType px = proj( x, cam );

    const double gr = ( std::sqrt(5.0) - 1.0 ) / 2.0;
    const double tol = 1.0e-10;

    double a = 0.0;
    double b = 1.0;

    double c = b - gr * ( b - a );
    double d = a + gr * ( b - a );

    while( std::abs( c - d ) > tol )
      {
	const double fc = px.dist( f( c, cam ) );
	const double fd = px.dist( f( d, cam ) );

	if( fc < fd )
	  {
	    b = d;
	    d = c;
	    c = b - gr * ( b - a);
	  }
	else
	  {
	    a = c;
	    c = d;
	    d = a + gr * ( b - a );
	  }
      }

    return f( (b+a)/2.0, cam );
  }

  ProjectedRangeVectorType proj( RangeVectorType const& x, unsigned cam_sel ) const
  {
    ProjectedRangeVectorType ret;
    switch( cam_sel ) {
      case 0:
	ret[0]=x[0]; ret[1]=x[1];
	break;
      case 1:
	ret[0]=x[1]; ret[1]=x[2];
	break;
      case 2:
	ret[0]=x[0]; ret[1]=x[2];
	break;
      default:
	unreachable();
    }
    return ret;
  }

  RangeVectorType dproj( const ProjectedRangeVectorType& dir, const int cam ) const
  {
    RangeVectorType ret;
    switch( cam ) {
      case 0:
	ret[0]=dir[0]; ret[1]=dir[1]; ret[2]=0;
	break;
      case 1:
	ret[0]=0; ret[1]=dir[0]; ret[2]=dir[1];
	break;
      case 2:
	ret[0]=dir[0]; ret[1]=0; ret[2]=dir[1];
	break;
      default:
	unreachable();
    }
    return ret;

  }

private:
  const std::function< RangeVectorType(double) >& f_;

  ProjectedRangeVectorType f( const double u, const int cam ) const
  {
    RangeVectorType fu = f_( u );
    return proj( fu, cam );
  }
};

struct VectorDistanceFunction
  : DistanceFunction<2> {
public: // types
  typedef std::vector< cv::Point > points_type;
private:
   VectorDistanceFunction() = delete;
public:
   virtual ~VectorDistanceFunction(){ itested();
   }
public: // construct
//  template<class Iter>
//  explicit VectorDistanceFunction(Iter b, Iter e)
//  {
//    set_points(b,e);
//  }

  explicit VectorDistanceFunction(const std::vector<cv::Point>& points)
    : points_( points )
  {
  }

  VectorDistanceFunction( const VectorDistanceFunction& other ) = default;
protected:
//   void set_points(points_type const&p)
//   {
//     points_ = p;
//   }
//   template<class Iter>
//   void set_points(Iter b, Iter e)
//   {
//     points_.resize(0);
//     for(;b!=e;++b){
//       points_.push_back(get_point(*b)); // BUG. need free access function.
//     }
//   }

public:
  // compute the median
  virtual RangeVectorType middle() const
  {
    std::vector< cv::Point > myPoints = points_;
    std::sort( myPoints.begin(), myPoints.end(),
	       []( const cv::Point& A, const cv::Point& B ) -> bool
	       {
		 return A.x < B.x;
	       });
    const int midx = myPoints.at( myPoints.size() / 2 ).x;

    std::sort( myPoints.begin(), myPoints.end(),
	       []( const cv::Point& A, const cv::Point& B ) -> bool
	       {
		 return A.y < B.y;
	       });
    const int midy = myPoints.at( myPoints.size() / 2 ).y;

    RangeVectorType ret;
    ret[0]=midx;
    ret[1]=midy;
    return ret;
  }

  // sort of 'forcing' = distance*normal
  // compute distance between a midline point x and the points_
  virtual double distance( const RangeVectorType& x ) const {
    // is this used?
    double m = 1e12;
    for( const auto& pt : points_ ) {
	const double d = ( (x[0] - pt.x)*(x[0] - pt.x) + (x[1] - pt.y)*(x[1] - pt.y) );
	m = std::min( m, d );
    }
    return sqrt(m);
  }
  // the distance times the normal
  // find a point p in points_ closest to x, comppute dn=x-p
  // return the distance squared
  // maybe "normalDistance"?
  virtual double normal_distance( const RangeVectorType& x, RangeVectorType& dn) const
  { itested();
    double square=1e99;
    cv::Point closestPt(-1,-1);
    for( const auto& pt : points_ ) { itested();
        double d = (x[0] - pt.x)*(x[0] - pt.x) + (x[1] - pt.y)*(x[1] - pt.y);
	if( d < square ) { itested();
	    square = d;
	    closestPt = pt;
	}else{
	}
    }

    dn[0] = x[0] - closestPt.x;
    dn[1] = x[1] - closestPt.y;

    return square;
  }

  // the normalized normal
  virtual RangeVectorType normal( const RangeVectorType& x ) const
  { untested();
    double m = 1e6;
    cv::Point closestPt(-1,-1);
    for( const auto& pt : points_ ) {
	const double d = std::sqrt( (x[0] - pt.x)*(x[0] - pt.x) + (x[1] - pt.y)*(x[1] - pt.y) );
	if( d < m ) {
	    m = d;
	    closestPt = pt;
	}else{
	}
    }

    RangeVectorType nu;
    nu[0] =  closestPt.x - x[0];
    nu[1] = closestPt.y - x[1];
    nu /= -nu.norm();

    return nu;
  }

  // not even used?
  virtual RangeVectorType normalDistance( const RangeVectorType& x ) const { untested();
    double minimum = 1e6;
    cv::Point closestPt(-1,-1);
    assert(points_.size());

    for( const auto& pt : points_ ) {
	const double ds = (x[0] - pt.x)*(x[0] - pt.x) + (x[1] - pt.y)*(x[1] - pt.y);
	if( ds < minimum ) {
	    minimum = ds;
	    closestPt = pt;
	}else{
	}
    }

    RangeVectorType nu;
    nu[0] = closestPt.x - x[0];
    nu[1] = closestPt.y - x[1];

    message(bDEBUG, "huh %f %f\n", nu.norm(), sqrt(minimum));
    if( nu.norm() > 1.0e-10 ){ untested();
      nu *= -std::sqrt(minimum) / nu.norm();
    }else{ untested();
      nu *= -std::sqrt(minimum) * 1.0e10;
    }

    return nu;
  }

  typedef DiscreteFunctionInterface<2> DF;
  double extraEnergy( const DF& X ) const
  { untested();
    double ret = 0;
    double size = 0;

    for( const auto& p : points_ ) { itested();
	double minDist = 1e10 * 1e10;
	for( unsigned int i = 0; i < X.N(); ++i ) {
	    const auto Xi = X.evaluate(i);
	    const double d = (p.x - Xi[0])*(p.x - Xi[0]) +
					(p.y - Xi[1])*(p.y - Xi[1]);
	    minDist = std::min( d, minDist );
	}

	ret += minDist;
	size += 1.0;
    }

    if( size > 0 ){ itested();
      ret *= 0.5 / size;
    }else{ untested();
    }

    return ret;
  }

  // for each keypoint, find nearby midline point.
  // pull on that.
  template< class DF >
  void extraForcing( const DF& X, DF& rhs ) const
  { untested();
    incomplete();
    for( auto p : points_ )
      {
	double minDist = 1e10;
	RangeVectorType minX;
	int minI = -1;
	for( unsigned int i = 0; i < X.N(); ++i )
	  {
	    const auto Xi = X.evaluate(i);
	    const double d = std::sqrt( (p.x - Xi[0])*(p.x - Xi[0]) +
					(p.y - Xi[1])*(p.y - Xi[1]) );
	    if( d < minDist )
	      {
		minDist = d;
		minX = Xi;
		minI = i;
	      }
	  }

	RangeVectorType f;
	f[0] = p.x - minX[0];
	f[1] = p.y - minX[1];
	f /= std::max( f.norm(), 1.0e-10 );
	// f *= std::min( minDist, 0.1 );
	// f *= ( minDist < 0.1 ) ? minDist : minDist * 1.0e-2;
	f *= minDist;
	f /= static_cast<double>(points_.size());

//	f/=5;

	rhs.add( minI, f );
      }
  }

  void printKnown( std::ostream& s ) const
  {
    s << "x,y" << std::endl;

    for( auto pt : points_ )
      {
	s << pt.x << ","
	  << pt.y << std::endl;
      }
  }

  const std::vector< cv::Point >& retKnown() const
  {
    return points_;
  }

  const size_t numKnown() const
  {
    return points_.size();
  }

protected:
  points_type const& points_;
};

// weird hack. abuse distance for force.
// does that even make sense?
class GravityDistanceFunction : protected VectorDistanceFunction
{
public: // types
  typedef std::vector< cv::Point > points_type;
  using DF=DiscreteFunctionInterface<2>;
public: // construct
  template<class Iter>
  explicit GravityDistanceFunction(Iter b, Iter e, double gs, double gr)
  :  VectorDistanceFunction(b,e),
    _gravity_scale(gs),
    _gravity_radius(gr)
  { untested();
  }

  GravityDistanceFunction( const GravityDistanceFunction& other ) = default;
public:
  // should be center of gravity?
  // virtual RangeVectorType middle() const
  // median, for now.

  virtual double distance( const RangeVectorType& x ) const
  { incomplete();
    return 0;
  }

  // abuse for gravity forces of all keypoints in x.
  virtual double normal_distance( const RangeVectorType& x, RangeVectorType& dn) const
  { itested();
    double scale = _gravity_scale / static_cast<double>(points_.size());
    double pointradius_squared=_gravity_radius * _gravity_radius;
    for( const auto& pt : points_ ) { itested();
      double square_distance = (x[0] - pt.x)*(x[0] - pt.x) + (x[1] - pt.y)*(x[1] - pt.y);

      if(square_distance>pointradius_squared){
	scale *= pow( (pointradius_squared/square_distance), 4./2.);
	// scale *= (pointradius_squared/square_distance);
      }else{
      }
      dn[0] += scale * (x[0] - pt.x);
      dn[1] += scale * (x[1] - pt.y);
    }

    return dn[0]*dn[0] + dn[1]*dn[1];
  }

  // the normalized normal
  virtual RangeVectorType normal( const RangeVectorType& x ) const
  {
    incomplete();
    RangeVectorType nu;
    return nu;
  }

  // not even used?
  virtual RangeVectorType normalDistance( const RangeVectorType& x ) const { untested();
    incomplete();
    RangeVectorType nu;

    return nu;
  }

   double extraEnergy( const DF& X ) const
   { untested();
     return VectorDistanceFunction::extraEnergy(X);
   }

  template< class DF >
  void extraForcing( const DF& X, DF& rhs ) const
  { untested();
     return VectorDistanceFunction::extraForcing(X, rhs);
  }
  

private:
  double _gravity_scale;
  double _gravity_radius;
};

#endif // #ifndef DISTANCEFUNCTION_HH

// vim:ts=8:sw=2
