#ifndef LIB_FORCEFUNCTION_DISTANCE_HH
#define LIB_FORCEFUNCTION_DISTANCE_HH

// (c) Felix Salfelder
// leeds university

// force functions for use in models. inspired by Toms DistanceFunction
//
#include <functional>
#include <array>
#include "forcefunction.hh"
#include "vector.hh"
#include "function.hh"
#include "mesh.hh"
#include "projectionoperator.hh"
#include "discretefunction.hh"
#include "forcefunction_distance.hh"

// the midline normal distance force.
// the closest point pulls on the midline.
class DistanceForceFunction : public VectorForceFunction {
  // map a numbered meshpoint on the midline to a force.
  typedef std::vector<RangeCoTangent> FORCE_CONTAINER;
  static constexpr unsigned dim=2;
public:
  DistanceForceFunction(const std::vector<cv::Point>& p, double const& s)
    : VectorForceFunction(p,s)
  {
  }

public:
  void expand(DFI const& x){
    VectorForceFunction::expand(x);
    _fm.resize(mesh().N());
  }
public: // misc

  RangeCoTangent const& directional_energy() const{
    return _directional_energy;
  }
  RangeCoTangent const& total_force() const{
    return _total_force;
  }
private: // overrides
  // DistanceForceFunction::
  void eval(){ itested();
    auto const& X=VectorForceFunction::X();
    _energy = 0.;
//    message(bTRACE, "rff eval %d %d\n", X.N(), _fm.size());
    assert(X.N() == _fm.size());
    for( unsigned i=0; i<X.N(); ++i ) { itested();
      _fm[i].clear();
    }
    _total_force.clear();
	 assert(!_total_force[0]);

	 // foreach midline point, find closest keypoint.
	 // add a force.
    for( unsigned i=0; i<X.N(); ++i ) { itested();
      double minDist_sq = 1e20;
      auto Xi = X.evaluate(i);
      cv::Point2d minp;
	   minDist_sq = 1e20;
      for( auto p : _points ) { itested();
        RangeVectorType minX;
        const double d = (p.x - Xi[0])*(p.x - Xi[0]) + (p.y - Xi[1])*(p.y - Xi[1]);
        if( d < minDist_sq ) { itested();
          minDist_sq = d;
          minp = p;
        }else{ itested();
        }
      }

      _energy += minDist_sq;

      RangeVectorType f;
//		message(bTRACE, "DF %d X %d %E %E nearest %E %E\n",
//				_points.size(), i, Xi[0], Xi[1], minp.x, minp.y);

      f[0] = minp.x - Xi[0];
      f[1] = minp.y - Xi[1];
      // f /= std::max( f.norm(), 1.0e-10 );
      // f *= std::min( minDist, 0.1 );
      // f *= ( minDist < 0.1 ) ? minDist : minDist * 1.0e-2;
      // f *= sqrt(minDist_sq);
      f /= static_cast<double>(_points.size());

      _fm[i] += _strength*f;
      _total_force += f;
//      _total_force += f;
      f[0] *= f[0];
      f[1] *= f[1];
      _directional_energy = f;
    }
  }
private:
//  FORCE_CONTAINER _fm;
}; // distanceForceFunction

#endif
