#include "config.hh"

#include <iostream>
#include <cassert>
#include <cmath>
#include <memory>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>
#include <limits>
#include <getopt.h>

// input parameters
#include "lib/parameter.hh"

// mesh and time helpers
#include "lib/mesh.hh"
#include "lib/timeprovider.hh"

// main entity
#include "lib/entity/entity.hh"
#include "lib/entity/world-entity.hh"

// control entity
#include "lib/entity/control/prescribed-control.hh"
#include "lib/entity/control/constant-control.hh"
#include "lib/entity/control/roller-control.hh"
#include "lib/entity/control/feedback-control.hh"
#include "lib/entity/control/feedforward-control.hh"
#include "lib/entity/control/feedforward-head-control.hh"
#include "lib/entity/control/CaP-control.hh"
#include "lib/entity/control/steering-control.hh"
#include "lib/entity/control/manual-head-and-follow-control.hh"

// mechanics entity
#include "lib/entity/mechanics/elastic-mechanics-entity.hh"
#include "lib/entity/mechanics/viscoelastic-mechanics-entity.hh"
#include "lib/entity/mechanics/bending-twisting-mechanics-entity.hh"
#include "lib/entity/mechanics/problem.hh"

// output entity
#include "lib/entity/output-entity.hh"

// output helpers
#include "lib/options.hh"

void algorithm() {
  // time provider
  TimeProvider timeProvider( Parameters::get<double>( "time.start" ),
			     Parameters::get<double>( "time.end" ) );
  timeProvider.setDeltaT( Parameters::get<double>( "time.deltaT" ) );

  // spatial discretisation
  const Mesh mesh( Parameters::get<double>("mesh.a"),
		   Parameters::get<double>("mesh.b"),
		   Parameters::get<unsigned int>("mesh.N") );

  // world entity - the great container
  // why not entitylist? that should be the same as in compound entities.
  // i.e. worldentity == entitylist::root (static).
  World_entity world;
  using pEntity_base = typename World_entity::pEntity_base;

  // control chooser
  pEntity_base control_ptr = nullptr;

  const std::string control_ent = Parameters::getString( "control.entity" );
  if( control_ent == "prescribed" ) {
    control_ptr.reset(  new Prescribed_control_entity( "control",
						       mesh,
						       timeProvider ) );
  } else if ( control_ent == "constant" ) {
    control_ptr.reset( new Constant_control_entity( "control",
						    mesh,
						    timeProvider ) );
  } else if ( control_ent == "roller" ) {
    control_ptr.reset( new Roller_control_entity( "control",
						  mesh,
						  timeProvider ) );
  } else if( control_ent == "feedforward" ) {
    control_ptr.reset(  new Feedforward_control_entity( "control",
  						       mesh,
  						       timeProvider ) );
  } else if( control_ent == "feedback" ) {
    control_ptr.reset(  new Feedback_control_entity( "control",
  						       mesh,
  						       timeProvider ) );
  } else if( control_ent == "feedforward_head" ) {
    control_ptr.reset( new Feedforward_head_control_entity("control",
                                                            mesh, 
                                                            timeProvider ) );
  } else if( control_ent == "CaP" ) {
    control_ptr.reset( new CaP_control_entity("control",
                                                            mesh, 
                                                            timeProvider ) );
  }
#ifdef GAMELIBS_FOUND
  else if( control_ent == "opengl.steering" ) {
    control_ptr.reset( new Steering_control_entity("control",
						   mesh, 
						   timeProvider ) );
} else if( control_ent == "opengl.manual_head_and_follow") {
    control_ptr.reset( new Manual_Head_And_Follow_control_entity("control",
						   mesh,
						   timeProvider ) );
 }
#endif
 else {
    throw exception_nomatch(control_ent + " is unknown control.entity");
  }
  assert( control_ptr );
  world.add_sub_entity( control_ptr );

  // mechanics chooser
  pEntity_base mechanics_ptr = nullptr;

  const std::string mechanics_ent = Parameters::getString( "mechanics.entity" );
  if( mechanics_ent == "elastic" ) {
    mechanics_ptr.reset( new Elastic_mechanics_entity( "mechanics",
						       mesh,
						       timeProvider ) );
  } else if ( mechanics_ent == "viscoelastic" ) {
    mechanics_ptr.reset( new Viscoelastic_mechanics_entity( "mechanics",
							    mesh,
							    timeProvider ) );
  } else if ( mechanics_ent == "bending-twisting" ) {
#if WORLDDIM == 3
    mechanics_ptr.reset( new Bending_twisting_mechanics_entity( "mechanics",
								mesh,
								timeProvider ) );
#else
    throw exception_nomatch( mechanics_ent + " is not available for WORLDDIM = "
			     + std::to_string( WORLDDIM ) );
#endif
  } else {
    throw exception_nomatch( mechanics_ent + " is unknown mechanics.entity" );
  }
  assert( mechanics_ptr );
  world.add_sub_entity( mechanics_ptr );


  // output chooser
  world.add_sub_entity( std::make_shared<Output_entity>( mesh, timeProvider ) );

  // set up world
  world.load();

  // begin time loop
  world.tr_begin();

  // accept initial conditions
  world.tr_begin_accept();

  for (; timeProvider.is_end(); timeProvider.next()) {
    // within time loop
    world.tr_advance();

    { // within solver loop
      world.tr_load();
      world.eval();
    }

    world.tr_review();
    world.tr_accept();
  }
}

static void printParameters() {
  std::cout << "compile time variables:" << std::endl;
  std::cout << " - WORLD_DIM: " << WORLDDIM << std::endl;
  std::cout << "other parameters" << std::endl;
  Parameters::dump(std::cout, " - ", " = ");
  std::cout << std::endl;
}

static void getParameters(int argc, char **argv, int i) {

  try {
    Parameters::merge_file(argv[i]);
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    std::exit(1);
  }

  ++i;
  for (; i < argc; ++i) {
    untested();
    try {
      // parse(parameters, &argv[i][2]);
      Parameters::add( argv[i] );
    } catch (std::exception &e) {
      std::cerr << "exception caught: " << e.what() << std::endl;
      std::exit(1);
    }
  }
}

static int parseArgumentsToOptions(int argc, char **argv) {

  int option = 0;
  while (option != -1) {
    option = getopt(argc, argv, "DTL");
    switch (option) {
    case 'D':options::set_errorlevel(bDEBUG);
      break;
    case 'T':options::set_errorlevel(bTRACE);
      break;
    case 'L':options::set_errorlevel(bLOG);
      break;
    case '?': {
      std::cerr << "usage: " << argv[0] << " {parameter file}" << std::endl;
      std::exit(1);
    }
    default:break;
    }
  }

  return optind;
}

int main(int argc, char **argv) {
  int i = parseArgumentsToOptions(argc, argv);
  getParameters(argc, argv, i);
  printParameters();

  try {
    algorithm();
  } catch (std::exception &e) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }

  return 0;
}
//  vim: set cinoptions=>4,n-2,{2,^-2,:2,=2,g0,h2,p5,t0,+2,(0,u0,w1,m1
//  vim: set autoindent expandtab shiftwidth=2 softtabstop=2 tabstop=8:
