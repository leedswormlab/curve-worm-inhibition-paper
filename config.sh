#!/bin/bash

BUILDDIR="build"

#CXXFLAGS="-O3 -Wall -Wfatal-errors"
CXXFLAGS="-std=c++11 -Wall -Wfatal-errors -DNDEBUG -O3"
# CXXFLAGS="-O0 -g -Wall -Wfatal-errors"

# echo "removing old build dir: ${BUILDDIR}"
# rm -rf ${BUILDDIR}

# echo "making new build directory ${BUILDDIR}"
# mkdir ${BUILDDIR}

echo "entering build directory ${BUILDDIR}"
cd ${BUILDDIR}

# find umfpack fun
UMFPACK_BUILDDIR=/home/cserv1_a/soc_staff/scstr/Software/DUNE/modules/SuiteSparse/build
# UMFPACK_BUILDDIR=/usr/lib/SuiteSparse/bin
OPENCV_BUILDDIR=/nobackup/scsrih/opencv/build
DOESNTWORK=-DSuiteSparse_ROOT="${UMFPACK_BUILDDIR}"

echo "calling cmake"
cmake .. \
    -DCMAKE_CXX_FLAGS="${CXXFLAGS}" \
    -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
    -DCMAKE_PREFIX_PATH="$OPENCV_BUILDDIR"


# tell git to ignore this directory
# echo "*" > .gitignore
