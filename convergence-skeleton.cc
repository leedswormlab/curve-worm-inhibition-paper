#include "config.hh"

#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>

#include <opencv2/opencv.hpp> // c++ headers

// input parameters
#include "lib/parameter.hh"

// mesh and time helpers
#include "lib/mesh.hh"
#include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"

void algorithm( ConfigParameters& parameters, const Mesh &mesh )
{
  static const unsigned int dim = WORLD_DIM;
  static const unsigned int nCams = NCAMS;

  // set energy difference tolerance
  const double energyDiffTol = parameters.get< double >( "term.energydiff" );

  // time holder
  TimeProvider timeProvider;
  const double endTime = parameters.get< double >( "time.end" );
  double deltaT = parameters.get< double >( "time.deltaT" );
  timeProvider.setDeltaT( deltaT );


  // time the loop
  std::clock_t start;
  start = std::clock();

  // find distance function
#if 0
  using BaseDistanceFunctionType = DistanceFunctionImage<2>;
  std::vector< BaseDistanceFunctionType > d;

  for( unsigned int cam = 0; cam < nCams; ++cam )
    {
      BaseDistanceFunctionType dd( srcs.at(cam ), frames.at(cam) );
      d.push_back( dd );
    }

  using DistanceFunctionType = ProjectedDistanceFunction< BaseDistanceFunctionType, BaseProjectionOperatorType, nCams >;
  DistanceFunctionType distanceFunction( d, p );
#endif

  using DistanceFunctionType = TestDistanceFunctionS<dim,nCams>;

  const int testCase = parameters.getFromList( "test.problem", { "helix", "lasso" } );

  std::function< typename DistanceFunctionType :: RangeVectorType(double) > f;

  switch( testCase ) {
  case 0: // helix
    f = []( const double u ) -> typename DistanceFunctionType :: RangeVectorType
      {
	typename DistanceFunctionType :: RangeVectorType ret;
	ret[ 0 ] = 2.0 * M_PI * (u-0.5);
	ret[ 1 ] = cos( 2.0 * M_PI * u );
	ret[ 2 ] = sin( 2.0 * M_PI * u );
	return ret;
      };
    break;
  case 1: // lasso
    f = []( const double u ) -> typename DistanceFunctionType :: RangeVectorType
      {
	typename DistanceFunctionType :: RangeVectorType ret;
	if( u < 0.5 )
	  {
	    ret[ 0 ] = cos( 2.0 * M_PI * u );
	    ret[ 1 ] = sin( 2.0 * M_PI * u );
	    ret[ 2 ] = 1;
	  }
	else
	  {
	    ret[ 0 ] = -1;
	    ret[ 1 ] = 0;
	    ret[ 2 ] = 1.5 - u;
	  }

	return ret;
      };
    break;
  }

  DistanceFunctionType distanceFunction( f );

  // find model
  using ModelType = DistanceModel< dim, nCams >;
  const ModelType model( parameters, distanceFunction,
			 timeProvider, imageTimeProvider );

  // initialise scheme
  using FemSchemeType = FemScheme< ModelType, WORLD_DIM >;
  FemSchemeType scheme( mesh, model );

  // initialse scheme
  scheme.initialiseXY( distanceFunction.middle() );

  // io time helpers
  double nextWriteTime = 0;
  const double nextWriteTimeStep = parameters.get< double >( "io.timestep" );
  const bool doInnerWrite = parameters.get< bool >( "io.innerwrite" );

  std::stringstream writerSs;
  writerSs << "convergence_skeletons_frame_";
  VTUWriter writer( parameters, timeProvider, writerSs.str() );
  std::vector< VTUWriter > projectedWriter;
  for( unsigned int cam = 0; cam < nCams; ++cam )
    {
      std::stringstream ss;
      ss << "convergence_projected_skeleton_frame_" << "cam" << cam << "_";
      projectedWriter.push_back( VTUWriter( parameters, timeProvider, ss.str() ) );
    }

#if 0
  {
    for( unsigned int cam = 0; cam < nCams; ++cam )
      {
	std::stringstream ss2;
	ss2 << parameters.getString( "io.prefix" )
	    << "/distance_cam" << cam << "_frame.csv";
	std::ofstream file;
	file.open( ss2.str().c_str() );
	d.at(cam).printKnown( file );
	file.close();

	std::stringstream ss3;
	ss3 << parameters.getString( "io.prefix" )
	    << "/cam" << cam << "_" << "frame.png";
	cv::imwrite( ss3.str(), frames.at(cam) );
      }
  }
#endif

  std::ofstream energyFile;
  {
    const std::string prefix = parameters.get< std::string >( "io.prefix" );
    std::stringstream energy_ss;
    energy_ss << prefix << "/energy.txt";
    energyFile.open( energy_ss.str().c_str() );
    if( energyFile.is_open() )
      std::cout << "energy file opened: " << energy_ss.str() << std::endl;
    else
      throw "unable to open file " + energy_ss.str();
  }

  double oldEnergy = -1;

  for( ; timeProvider.time() < endTime; timeProvider.next( deltaT ) )
    {
      // prepare
      scheme.prepare();

      // solve
      scheme.solve();

      // compute energy
      const double energy = scheme.energy( energyFile );

      // compute energy and output important data
      if( timeProvider.time() >= nextWriteTime and doInnerWrite )
	{
	  writer.writeVtu( scheme.position(), scheme.curvature(), scheme.pressure() );
#if 0
	  for( unsigned int cam = 0; cam < nCams; ++cam )
	    {
	      Vector pXdata( dim * mesh.N() );
	      PiecewiseLinearFunction< dim-1 > pX( mesh, SubArray< Vector >( pXdata.begin(), pXdata.end() ) );

	      p.at( cam ).projectSolution( scheme.position(), pX );
	      projectedWriter.at( cam ).writeVtu( pX, scheme.curvature(), scheme.pressure() );
	    }
#else
#warning to do: projections
#endif

	  std::cout << "time: " << timeProvider.time() << std::endl;
	  // std::cout << "extra energy: " << extraEnergy << std::endl;
	  std::cout << "energy: " << energy
		    << " (change: " << std::abs( oldEnergy - energy )
		    << ")"<< std::endl;

	  nextWriteTime += nextWriteTimeStep;
	}

      // check stopping criteria
      const double criteria = scheme.updateSize();
      if( criteria < energyDiffTol )
	{
	  std::cout << "breaking: change = " << criteria << "/ " << energyDiffTol << std::endl;
	  break;
	}

      // update old energy
      oldEnergy = energy;
    }

  // output converged solution
  {
    writer.writeVtu( scheme.position(), scheme.curvature(), scheme.pressure() );
#if 0
    for( unsigned int cam = 0; cam < nCams; ++cam )
      {
	Vector pXdata( dim * mesh.N() );
	PiecewiseLinearFunction< dim-1 > pX( mesh, SubArray< Vector >( pXdata.begin(), pXdata.end() ) );

	p.at( cam ).projectSolution( scheme.position(), pX );
	projectedWriter.at( cam ).writeVtu( pX, scheme.curvature(), scheme.pressure() );
      }
#else
#warning to do: projections
#endif
  }

  double finalEnergy = scheme.energy( energyFile, true );
  // const double extraEnergy = distanceFunction.extraEnergy( X );
  // finalEnergy += extraEnergy;

  energyFile.close();

  const double totalTime = (std::clock() - start) / (double)(CLOCKS_PER_SEC);

  std::cout << "total time " << totalTime << " sec." << std::endl;
  std::cout << "final energy: " << finalEnergy << std::endl;
  // std::cout << "extra energy: " << extraEnergy << std::endl;
  std::cout << "simulation time: " << timeProvider.time() << std::endl;
  std::cout << "simulation iter: " << timeProvider.iteration() << std::endl;
}

int main( int argc, char **argv )
{
  try{
    ConfigParameters parameters( "../data/parameters" );
    for( int i = 1; i < argc; ++i )
      parameters.add( argv[ i ] );

    std::cout << "compile time variables:" << std::endl;
    std::cout << " - WORLD_DIM: " << WORLD_DIM << std::endl;
    std::cout << " - NCAMS: " << NCAMS << std::endl;
    std::cout << " - USE_TIME_ADAPTIVE: " << USE_TIME_ADAPTIVE << std::endl;
    std::cout << " - USE_INTERSECTION_FORCE: " << USE_INTERSECTION_FORCE << std::endl;

    // make mesh
    const unsigned int N = parameters.get< unsigned int >( "mesh.N" );
    const double a = parameters.get< double >( "mesh.a" );
    const double b = parameters.get< double >( "mesh.b" );
    Mesh mesh( a, b, N );

    // run algorithm
    algorithm( parameters, mesh );

    return 0;
  }
  catch( const std::string& e ) {
    std::cout << "Exception: " << e << std::endl;
    return 1;
  }
  catch( const char* e ) {
    std::cout << "Exception: " << e << std::endl;
    return 1;
  }
  catch( std::exception& e ) {
    std::cerr << "exception caught: " << e.what() << std::endl;
    return 1;
  }
}
