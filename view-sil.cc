//
// (c) 2017 scs.leeds.ac.uk
// Author: Felix Salfelder 2017
//
// a worm-graph thing

#include "config.hh"
#include <boost/graph/properties.hpp>
#include <boost/functional/hash.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <opencv2/features2d.hpp>

#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>

// #include <opencv2/opencv.hpp> // c++ headers
#include "lib/trace.hh"
#include "lib/io_misc.hh"

///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
///// OBSOLETE
/// OBSOLETE
#include "lib/parameter.hh"
#include "lib/matrix_graph.hh"
#include "lib/error.hh"
#include "lib/options.hh"

#include "lib/image/util.hh"
#include "lib/image/freak.hh"
#include "lib/mesh.hh"
// #include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

#include "lib/error.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"
#include "lib/image/io.hh"
#include "lib/image/display.hh"
#include "lib/image/annotations.hh"

#ifndef WORLD_DIM
#define WORLD_DIM 3
#endif
#ifndef NCAMS
#define NCAMS 3
#endif

#define USE_FREAK


// these should be parameters/cmdline args
// (some are)
double initialguess_known = 1.;
double initialguess_unknown = -1.;
double deltat=1e-4;
unsigned numberofsteps=5;
/*------------------------------------------------------*/

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

typedef cv::Mat frame_type;
typedef std::array< cv::Mat, NCAMS > frames_type;
typedef std::deque< cv::Mat > frame_deque_type;
typedef std::deque< frames_type > frames_deque_type;

void run_wormgraph(ConfigParameters const& );
//typedef std::pair<unsigned, unsigned> coord_type;
typedef cv::Point coord_type;

#if 0
namespace std{
static bool operator<(coord_type const& a, coord_type const& b){
	return make_pair(a.x, a.y) < make_pair(b.x, b.y);
}
} // std
#endif


// represent a pixel on a frame
// this is expensive...
struct vertex_repr_type{
	vertex_repr_type() : _frame(NULL) {
	}
	vertex_repr_type(coord_type const& c, frame_type const& f)
		: _frame(&f), _which(c){ itested();
	}
	vertex_repr_type(vertex_repr_type const& o)
		: _frame(o._frame), _which(o._which){ itested();
	}

public:
	frame_type const& frame()const{
		assert(_frame);
		return *_frame;
	}
	coord_type const& coord()const{
		assert(_which.x<99999);
		assert(_which.y<99999);
		return _which;
	}
private:
#ifndef NDEBUG
public:
#endif
	frame_type const* _frame;
	coord_type _which;
}; // vertex_repr_type

typedef vertex_repr_type pixel_address;
typedef std::array<uchar, 64> pixel_features;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
		  boost::property<boost::vertex_color_t, long int,
#ifdef USE_FREAK
		  boost::property<boost::vertex_owner_t, pixel_features> >,
#else
		  boost::property<boost::vertex_owner_t, vertex_repr_type> >,
#endif
		  boost::property<boost::edge_weight_t, double>
			  > edgeweighted_graph_type1;

namespace framestuff{


std::string bgimgname(std::string const& aviname)
{
	auto p=aviname.find("cam");
	std::string ret="BGimg_" + aviname.substr(0, p+4) + ".png";
	return ret;
}


} // framestuff

frame_type const& get_frame(framestuff::annotated_frame const& f)
{
	return f.frame();
}
typedef framestuff::annotated_frame::annotation_type annotation_type;
annotation_type const& get_annotation(framestuff::annotated_frame const& f)
{
	return f.annotation();
}

template<class A>
float count_incidences(cv::Mat const& m, A const& p)
{
	unsigned hit=0;
	unsigned miss=0;
	for(auto i: p){
		int X=i.first.x; // BUG,vector?
		int Y=i.first.y;
		if(m.at<uchar>(Y, X)){
			++hit;
		}else{
			++miss;
		}
	}

	return float(hit) / float(hit+miss);
}

int main( int argc, char **argv )
{

	std::string aviname="";
	std::string frameno="";
	std::string bgname="";

	ConfigParameters parameters;
	parameters.set("bgname", "bg.png");
	parameters.set("aviname", "worm.avi");
	parameters.set("frameno", "0");
	parameters.set("dilate-sil", "3");

	if(argc<2){
		std::cerr << "need headclip csv filename\n";
	}

	float printhit=0;
	int i=1;
	// poor mans getopt. yes it's limited, but it works.
	for(; i<argc; ++i){
		trace1("main", argv[i][0]);
		if(argv[i][0]=='-'){
			switch(argv[i][1]){
				case 'L': untested();
					options::set_errorlevel(bLOG);
					message(bTRACE, "bTRACE\n");
					break;
				case 'T': untested();
					options::set_errorlevel(bTRACE);
					message(bTRACE, "bTRACE\n");
					break;
				case 'H': itested();
					printhit = .95;
					break;
				case 'B': untested();
					 ++i;
					parameters.set("bgname", argv[i]);
					break;
				case 'D': untested();
					options::set_errorlevel(bDEBUG);
					break;
				//case 't':
				//			 ++i;
				//			 deltat=atof(argv[i]);
				//			 std::cout << "deltat override" <<deltat<< "\n";
				//			 ///  skip frames...
				//			 break;
			}
		}else{
			break;
		}
	}

	bool display=!printhit;
	unsigned dilate_sil=parameters.get<unsigned>("dilate-sil");

	std::string csvfilename=argv[i];

	auto p=csvfilename.find_last_of("/");

	std::string dir(csvfilename.substr(0,p));
	std::string file(csvfilename.substr(p));

	message(bLOG, "ann %s, %s\n", dir.c_str(), file.c_str());

	typedef framestuff::annotated_frame annotated_frame;

	annotated_frame a;
	
	bgname=parameters.get<std::string>("bgname");
	if(bgname==""){ untested();
		a=annotated_frame(dir + "/" + file, annotated_frame::_CSV);
	}else{
		cv::Mat BG;
		get_grayscale_image(bgname, BG);
		a=annotated_frame(dir + "/" + file, BG);
	}

	cv::Mat S=a.frame().clone();
	if(display)
		display_grayscale_frame("all", S);

	silhouetteImageDilate(a.frame(), S, dilate_sil, 200.);

	if(display)
		display_grayscale_frame("sil", S);
	
	float x=count_incidences(S, a.annotation());
	if(x>printhit){
		std::cout << csvfilename << " " << "hitproportion " << x << "\n";
	}else{
		std::cerr << csvfilename << " " << "hitproportion " << x << "\n";
	}

	if(display){
		// auto bb=bbox(S);
		display_pointset_on_frame("?", S, a.annotation(), 128);
	}
}
