% Jack Denham
% Amplitude defined by maximum distance between straight line joining head and tail
% and the body at half the body length: u = 0.5.
% input - x and y coordinates of worm simulation size:[N_timesteps, N_body]

function y = get_waveAmp(pointx, pointy) % size = [time x body]
    checks = 0;
    
    [Nt,Nx] = size(pointx)
    u = 0.5;
    U = round(u*Nx);
   
    % for each time point find straight line y=mx+c connecting first and
    % last mesh point
    for ii = 1:Nt
        m = (pointy(ii,end) - pointy(ii,1))/(pointx(ii,end) - pointx(ii,1));
        c = pointy(ii, 1) - m*pointx(ii,1);
        X(ii,:) = linspace(pointx(ii, end), pointx(ii,1),Nx);
        line(ii,:) = m*X(ii,:) + c;
        % distance from line to body
   	    amp_measure(ii) = ( ( X(ii,U) - pointx(ii,U) )^2 + ( line(ii,U) - pointy(ii,U) )^2  )^0.5;
    end
      
    % find peak amplitude
    [loc, amp] = pickpeak(amp_measure,100);
     loc(isnan(amp)) = [];
     amp(isnan(amp)) = [];
     
    if(checks)
        figure; plot(amp_measure, '.'); hold on; plot(loc, amp, 'r.', 'MarkerSize', 15); hold off;
        Timepoint = 1100;
        figure; plot(X(Timepoint,:), line(Timepoint, :), '.');hold on; plot(pointx(Timepoint,:), pointy(Timepoint,:), '.')
    end
    
    % average over simulation
    y = mean(amp);
    
end
