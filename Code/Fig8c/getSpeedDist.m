% Jack Denham
% calculate distance travelled by worm and average speed
% input - x and y coordinates of worm simulation size:[N_timesteps, N_body]
%         T - corresponding time vector

function [aveSpeed, DistinTime, aveDist] = getSpeedDist(pointx, pointy, T) % dist (mm/sec)

    [lenTx, lenX] = size(pointx); 
    [lenTy, lenY] = size(pointy);
    lenT = length(T);
    
    midpt = round(lenX/2);
    tspan = T(end) - T(1);
    
    % Check input matrices are consistent
    if(lenX ~= lenY)
        Error = 'Spacial dimension mismatch'
        return
    end
    if(lenTx ~= lenT || lenTy ~= lenT)
        Error = 'Time dimension mismatch'
        return
    end

    % Fit line to body midpoint trajectory over full simulation
    % Note: after transient, model worm travels in a straight line
    p1 = pointx(:,midpt);
    p2 = pointy(:,midpt);
    P = polyfit(p1, p2, 1);
    curveFit = P(1)*pointx(:,midpt) + P(2);
    
    dist    = nan(1,lenT-1);
    avedist = nan(1,lenT-1);
    % Get distance the midbody travels per time point
    for(k = 1:lenT-1)
        dist(k) = (pointx(k, midpt) - pointx(k+1, midpt))^2 + (pointy(k, midpt) - pointy(k+1, midpt))^2;
        dist(k) =  sqrt(dist(k));   
    
        avedist(k) = (pointx(k, midpt) - pointx(k+1, midpt))^2 + (curveFit(k) - curveFit(k+1))^2;
        avedist(k) = sign(pointx(k, midpt) - pointx(k+1, midpt))*sqrt(avedist(k)); % sign accounts for backwards travel in x-direction
    end 
    Sign = sign(p1(1) - p1(end)); % speed positive (negative) if Sign positive (negative)
    
    % Output
    DistinTime = cumsum(dist);
    aveDist    = sum(dist);
    aveSpeed   = Sign*sqrt((p1(end) - p1(1))*(p1(end) - p1(1)) + (curveFit(end) - curveFit(1))*(curveFit(end) - curveFit(1)))/(tspan); %abs(aveDist/(tspan)); 
    
end

