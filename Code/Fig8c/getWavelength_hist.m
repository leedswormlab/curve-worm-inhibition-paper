% Thomas Ranner, Jack Denham
% Time - time vector
% curveMat - scalar curvature matrix size [N_timesteps x N_meshsize]
% Period -   period of undulation (1/frequency)
% meshN  - mesh size (default value = 128)
function y = getWavelength_hist(Time, curveMat, Period, meshN )

    if nargin == 3
        meshN = 128;
    elseif nargin == 4
        meshN = meshN;
    end

    showGradKymos = 0;
    showHistogram = 0;

    % find size of curvature matrix
    [Nt, Nx] = size( curveMat )

    disp( ['Nx = ',int2str(Nx), ', Nt = ', int2str(Nt) ] );
      L = Nx/meshN;
     dx = L/(Nx-1.0);
     dt = mean(diff(Time));

    % smoothing
    filter_kernel = ones( 10, 10 );
    smooth_curvatures = filter2( filter_kernel, curveMat );
    [smooth_gradX, smooth_gradY] = gradient( smooth_curvatures, dx, dt );
    smooth_wavelength = abs( Period * smooth_gradY ./ smooth_gradX );
    smooth_wavelength = smooth_wavelength( 2 : Nt-1, floor(Nx/10) : ceil(Nx/2) );
    
    % remove nans
    smooth_wavelength_nans = find( isnan(smooth_wavelength) );
    smooth_wavelength( smooth_wavelength_nans ) = -1;

    % remove infs
    smooth_wavelength_infs = find( isinf( smooth_wavelength ) );
    smooth_wavelength( smooth_wavelength_infs ) = -1;

    if(showGradKymos)
        figure(1); pcolor(smooth_curvatures'); shading flat; colorbar;
        figure(2); pcolor(smooth_wavelength'); shading flat; colorbar;
    end

    % remove nans and infs and convert to vec
    A = smooth_wavelength(:);
    Aneg = find( A < 0 );
    A( Aneg ) = [];

    % convert to smoothed histogram
    bins = logspace( -2, 2, Nt );
    Ac = histcounts( A, bins );
    Ac_smooth = Ac; %smooth( Ac, 5 ); jd change back
    
    % find max
    [m, idx] = max( Ac_smooth );
    y1 = ( bins(idx)+bins(idx+1) )/2;
    
    M = 0.75*m;
    topAC = bins(find(Ac_smooth>M));
    AveragingWavelength = mean(topAC);
    y = AveragingWavelength;
    
    if( showHistogram )
        figure;
        hold on;
        set(gca,'xscale','log');
        set(gca,'yscale','log')
        plot( bins(1:end-1), Ac, 'm.-' );
        plot( bins(1:end-1), Ac_smooth, 'b.-' );
        plot( y, m, 'ro' );
        plot(bins, linspace(M, M, length(bins)),'k:')
        legend('wavelengths','smoothed', 'peak')
    end

    if( showGradKymos )
        figure; pcolor( log(abs(smooth_wavelength' - y)) );
        shading flat; colorbar
    end
end
