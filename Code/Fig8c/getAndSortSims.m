% Jack Denham
% Find file names beginning with 'r_' in a chosen directory and order them
% by the number that follows
% Return file names and full paths to files. 

function [fullPaths, fileNames] = getAndSortSims(pathToSims)

    originalPath = pwd;
    cd(pathToSims);
    
    Files = dir('r_*');
    
    for kk = 1:length(Files)
        fileName(kk) = cellstr(Files(kk).name);
    end

    fileNames = sort(fileName);
    fullPaths = strcat(pathToSims,'/', fileNames, '/');
    cd(originalPath);
end
