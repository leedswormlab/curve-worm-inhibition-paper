% This file calculates frequency, wavelength, wave amplitude and speed
% from pre-existing data from the model described in 
% Denham, Ranner and Cohen (2018) doi:10.1098/rstb.2018.0208
% Additional functions: analyseSims(dir, transient)

%% Calculate metrics from model data
% Directories containing simulation data for each hypothesis
dir = '../hyp_1/';
% dir = '../hyp_2/';
% dir = '../hyp_3/';
% dir = '../hyp_1_2/';

% Get metrics
 transient = 1;
 [f,w,a,s] = analyseSims(dir, transient);
 len = length(f);

% arrange data for bar plot
f_ordered = [f(1), f(3); f(2), f(4); f(5), f(7); f(6), f(8) ];
w_ordered = [w(1), w(3); w(2), w(4); w(5), w(7); w(6), w(8) ];
a_ordered = [a(1), a(3); a(2), a(4); a(5), a(7); a(6), a(8) ];
s_ordered = [s(1), s(3); s(2), s(4); s(5), s(7); s(6), s(8) ];
order = {'FF water';'FF agar';'FB water';'FB agar'};

 %% plots
 X = categorical(order);
 
 figure(1);
 bar(X, f_ordered )
 ylabel('frequency')
 legend('WT','mutant')

 figure(2);
 bar(X, w_ordered )
 ylabel('wavelength')

 figure(3);
 bar(X, a_ordered )
 ylabel('amplitude')
 
 figure(4);
 bar( X, s_ordered )
 ylabel('speed (mm/s)')
 
%% save as .dat, .png and .fig
% data
% save('f_ordered', 'f_ordered', '-ascii');
% save('w_ordered', 'w_ordered', '-ascii');
% save('a_ordered', 'a_ordered', '-ascii');
% save('s_ordered', 's_ordered', '-ascii')

% save figures
% figure(1); print('hyp1_freq','-dpng');  savefig('hyp1_freq');
% figure(2); print('hyp1_wave','-dpng');  savefig('hyp1_wave');
% figure(3); print('hyp1_amp','-dpng');   savefig('hyp1_amp');
% figure(4); print('hyp1_speed','-dpng'); savefig('hyp1_speed');





