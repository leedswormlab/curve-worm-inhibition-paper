% Jack Denham
% This function returns metrics from one or more folders containing
% position and curvature data of a slender body over time. 
% 'kappa.dat' and 'position.dat' have dimensions [N_time, 1+N_bodypoints],
% where the first column is time.
% dir - directory containing simulation folders named r_*
% transient - initial number of rows to be ignored

function [frequency, wavelength, amplitude, speed] = analyseSims(dir, transient)

% Check number of inputs
if (nargin<1), display ('error, too few inputs');
    elseif (nargin>2), display ('error, too many inputs');
    elseif (nargin==1), 
            display ('using default transient = 1'); 
            transient = 1;
end;

% get paths to files
  [fullPaths, fileNames] = getAndSortSims(dir);
  chosenSims = fullPaths;             

% time rescaling from model
tc = 3.3;

% prepare vectors
frequency = [];
wavelength = [];
amplitude = [];
speed = [];
count = 0;
for k = 1:length(fullPaths);
     
	if(length(transient) > 1)
		trans = transient(k);
	else
		trans = transient;
    end
    
            kappa = load(strcat(chosenSims{k}, 'kappa.dat'));
            kappaTimes = kappa(trans:end,1)*tc;
            kappa = kappa(trans:end, 2:end);            
	 if( exist( strcat(fullPaths{k}, 'position.dat') ) )
            position = load(strcat(fullPaths{k}, 'position.dat'));
            ptx = position(trans:end, 2:2:end);
            pty = position(trans:end, 3:2:end);
            [aveSpeed, DistinTime, aveDist] = getSpeedDist(ptx, pty, kappaTimes);
	    speed = [speed aveSpeed];
 	    Amp = get_waveAmp(ptx, pty);
	    amplitude = [amplitude Amp];
	 else
	    speed = [];  
	    amplitude = [];         
	 end
            currentFreq = get_frequency(kappa(:, 10), kappaTimes);
            frequency   = [frequency currentFreq]; 
            wavelength  = [wavelength getWavelength_hist(kappaTimes, kappa(:,13:85), 1/currentFreq)]; %1:80  13:85
            
        count = count + 1
end
