% Jack Denham
% Calculate undulation frequency through zero crossings of curvature
% withing a time frame

function frequency = get_frequency(curveMat, curveTimes)

[Nt, Nx] = size(curveMat);
bodyCoordinate =  round(0.2*Nx);

% Find average difference between time points where curvature increases 
% through zero
temp     = curveMat(1  :end-1, bodyCoordinate);
tempnext = curveMat(2  :end  , bodyCoordinate);
ind      = find( ( (temp.*tempnext-0.0) < 0.0) & ( (tempnext-0.0) > 0.0 ) );  
diffs = diff(curveTimes(ind));
diffs(find(isnan(diffs))) = [];
period   = mean(diff(curveTimes(ind)));

% check that undulations occurred
if(isnan(period))
    freq = 0;
else
    freq = 1/period;
end

frequency = freq;

end
