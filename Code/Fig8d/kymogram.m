% Jack Denham
% Plot curvature kymograms
%% directory 
   dir = '..\hyp_4\';

%% find chosen simulations
 [fullPaths, fileNames] = getAndSortSims(dir);
 index = 1:4
sims  =  fullPaths(index); 

%% construct kymograms
fignum = index;

% Choose colormap
newMap01 = redBluecolormap();
 
count = 1;
for sim = sims
        
    sim = cell2mat(sim);
    disp(sim)
    cd(sim);

tc = 3.3;

    kappa = load('kappa.dat');
    kappaTime = kappa(:,1)*tc;
    kappa = kappa(:, 2:end);
    sizeKappa = size(kappa);

    t1 = 1; 
    t2 = length(kappa(:,1));  
    xx = (1:1:length(kappa(1,:)))/length(kappa(1,:));
    [T X] = meshgrid(kappaTime(t1:t2), xx );
    
     figure( fignum(count) );
     pcolor(T(:,t1:t2), X(:,t1:t2), kappa(t1:t2, 1:end)'); cc = colorbar; colormap(newMap01);
     hold on;
     cc.Label.String = '\kappa (mm^{-1})';
     set(gca, 'YTick', [0.03 1]);
     set(gca, 'YTickLabel', {'H','T'});
     rectangle('position',[43.7 0.1 1 0.03], 'FaceColor', 'w')
     shading flat;
     set(gca,'fontsize', 15);
     set(gca, 'fontname', 'Times New Roman')
     axis([30 45 0.02 1]);
    %caxis([-10 10]);
    
 count = count + 1;
end % for sim in sims


%% extra functions

function newMap = redBluecolormap()

% colormaps are 64 x 3 typically
 redVec   = [ zeros(1, 28) ,linspace(0.0, 1.0, 36)];
 greenVec = linspace(0.0, 0.0, 64);
 blueVec  = [linspace(1.0, 0.0, 36),  zeros(1, 28)];
 
newMap = [redVec', greenVec', blueVec'];

end