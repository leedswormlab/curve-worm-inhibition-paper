% Jack Denham
% Plot model data from hypothesis 2 sweeps

dir = '..\hyp_2\sweeps\FB\';
dirFF = '..\hyp_2\sweeps\FF\';

% params
a    = 1.0:1.0:15.0 ;
b = a*10;
taum = 1./a;
lenbeta0 = length(taum);

alpha = 0:0.5:5.0;
lenalpha = length(alpha);

% Get metrics
% transient = 1;
% [f,w,a,s] = analyseSims(dir, transient); s = abs(s);
% len = length(f);
% F_FF = reshape(f, [lenbeta0, 2]);
% W_FF = reshape(w, [lenbeta0, 2]);
% A = reshape(a, [lenbeta0, 2]);
% S_FF = reshape(s, [lenbeta0, 2]);
 
  F = load(strcat(dir, 'F')); F_FF = load(strcat(dirFF, 'F_FF'));
  W = load(strcat(dir, 'W')); W_FF = load(strcat(dirFF, 'W_FF'));
  A = load(strcat(dir, 'A')); A_FF = load(strcat(dirFF, 'A_FF'));
  S = load(strcat(dir, 'S')); S_FF = load(strcat(dirFF, 'S_FF'));
  thrust = S./F;              thrust_FF = S_FF./F_FF;

f_wt_water = F(10,1);  f_FF_wt_water = F_FF(7,1);
f_wt_agar  = F(10,2);  f_FF_wt_agar  = F_FF(7,2);
w_wt_water = W(10,1);  w_FF_wt_water = W_FF(7,1);
w_wt_agar  = W(10,2);  w_FF_wt_agar  = W_FF(7,2);
a_wt_water = A(10,1);  a_FF_wt_water = A_FF(7,1);
a_wt_agar  = A(10,2);  a_FF_wt_agar  = A_FF(7,2);
s_wt_water = S(10,1);  s_FF_wt_water = S_FF(7,1);
s_wt_agar  = S(10,2);  s_FF_wt_agar  = S_FF(7,2);
thr_wt_water = thrust(10, 1); thr_FF_wt_water = thrust_FF(1, 1);
thr_wt_agar  = thrust(10, 2); thr_FF_wt_agar  = thrust_FF(1, 2);

% Lan experiment - calc percentage frequency decrease from WT to mutant and
% compare with hyp1 sweeps (beta_0) 
% freq crawl
wt_fag = 0.38;
unc25_f_ag = (wt_fag - 0.35)/wt_fag % 0.0789
unc46_f_ag = (wt_fag - 0.28)/wt_fag % 0.2632
unc49_f_ag = (wt_fag - 0.21)/wt_fag % 0.4474

%speed crawl
wt_sag = 150;
unc25_s_ag = (wt_sag - 100)/wt_sag % 0.3333
unc46_s_ag = (wt_sag - 80)/wt_sag  % 0.4667
unc49_s_ag = (wt_sag - 50)/wt_sag  % 0.6667

%freq swim
wt_fw = 1.65;
unc25_f_wa = (wt_fw - 0.8)/wt_fw % 0.5152
unc46_f_wa = (wt_fw - 1.0)/wt_fw % 0.3939
unc49_f_wa = (wt_fw - 0.3)/wt_fw % 0.8182

%speed swim
wt_sw = 400;
unc25_s_wa = (wt_sw - 200)/wt_sw % 0.5000
unc46_s_wa = (wt_sw - 180)/wt_sw % 0.5500
unc49_s_wa = (wt_sw - 80)/wt_sw  % 0.8000


 %% plots

figure(1); % frequency
subplot(1,2,1)
hold on;
semilogx(taum, F(:,1)./f_wt_water, 'b.-', taum, F(:,2)./f_wt_agar, 'r.-', 'Markersize', 20, 'Linewidth', 1.5 )
semilogx(taum, repmat(1-unc25_f_wa, [1 length(taum)]), ':c', taum, repmat(1-unc25_f_ag, [1 length(taum)]), ':m', 'Markersize', 20, 'Linewidth', 1.5)
plot(linspace(.1,.1,100), linspace(0, 1.5, 100), 'k--')
hold off;
xlim([0.06 1.1])
xlabel('\tau_{m} (sec)')
ylabel('Frequency (Hz)')
legend('feedback water','feedback agar', 'location', 'northeast')
subplot(1,2,2)
hold on;
plot(alpha, F_FF(:,1)./f_FF_wt_water, 'bs-', alpha, F_FF(:,2)./f_FF_wt_agar, 'rd-','Markersize', 8 , 'Linewidth', 1.5)
plot(alpha, repmat(1-unc25_f_wa, [1 length(alpha)]), ':c', alpha, repmat(1-unc25_f_ag, [1 length(alpha)]), ':m', 'Markersize', 20, 'Linewidth', 1.5)
plot(linspace(3,3,100), linspace(0, 1.5, 100), 'k--')
text(0.9,0.9,'Exp. crawl speed', 'FontSize', 12, 'Color', 'm')
text(0.9,0.4,'Exp. swim speed', 'FontSize', 12, 'Color', 'c')
hold off;
ylim([0.0 1.5])
xlabel('\alpha')
title('hypothesis 2')

figure(2); % speed
subplot(1,2,1)
hold on;
semilogx( taum, S(:,1)./s_wt_water, 'b.-', taum, S(:,2)./s_wt_agar, 'r.-','Markersize', 20, 'Linewidth', 1.5 )
semilogx(taum, repmat(1-unc25_s_wa, [1 length(taum)]), ':c', taum, repmat(1-unc25_s_ag, [1 length(taum)]), ':m', 'Markersize', 20, 'Linewidth', 1.5)
semilogx(linspace(.1,.1,100), linspace(0, 1.5, 100), 'k--')
hold off;
xlim([0.06 1.1])
xlabel('\tau_{m} (sec)')
ylabel('Speed (mm/s)')

subplot(1,2,2)
hold on;
plot( alpha, S_FF(:,1)./s_FF_wt_water, 'bs:', alpha, S_FF(:,2)./s_FF_wt_agar, 'rd:','Markersize', 8 , 'Linewidth', 1.5)
plot(alpha, repmat(1-unc25_s_wa, [1 length(alpha)]), ':c', alpha, repmat(1-unc25_s_ag, [1 length(alpha)]), ':m', 'Markersize', 20, 'Linewidth', 1.5)
plot(linspace(3,3,100), linspace(0, 1.5, 100), 'k--')
text(0.9,0.7,'Exp. crawl speed', 'FontSize', 12, 'Color', 'm')
text(0.9,0.4,'Exp. swim speed', 'FontSize', 12, 'Color', 'c')
hold off;
ylim([0.0 1.6])
xlabel('\alpha')
legend('feedback water','feedback agar', 'location', 'northwest')
title('hypothesis 2')

%% save stuff
% data
% save('F_FF', 'F_FF', '-ascii');
% save('W_FF', 'W_FF', '-ascii');
% save('A', 'A', '-ascii');
% save('S_FF', 'S_FF', '-ascii');

% figures save
% figure(1); print('hyp2_sweep_freq_norm','-dpng');   savefig('hyp2_sweep_freq_norm');
% figure(2); print('hyp2_sweep_speed_norm','-dpng');  savefig('hyp2_sweep_speed_norm');

