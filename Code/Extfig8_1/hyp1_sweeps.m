% Jack Denham
% Plot model data from hypothesis 1 sweeps

dir = '..\sweeps\hyp1_sweeps\FB\';
dirFF = '..\hyp1_sweeps\FF\';

% params
beta0 = 0.1:0.1:1.3 ;
lenbeta0 = length(beta0);

% Get metrics
% transient = 1;
% [f,w,a,s] = analyseSims(dirFF, transient);
% len = length(f);
% F = reshape(f, [lenbeta0, 2]);
% W = reshape(w, [lenbeta0, 2]);
% A = reshape(a, [lenbeta0, 2]);
% S = reshape(s, [lenbeta0, 2]);

F = load(strcat(dir, 'F')); F_FF = load(strcat(dirFF, 'F'));
W = load(strcat(dir, 'W')); W_FF = load(strcat(dirFF, 'W'));
A = load(strcat(dir, 'A')); A_FF = load(strcat(dirFF, 'A'));
A_kappa = load(strcat(dir, 'A_kappa')); A_kappa_FF = load(strcat(dirFF, 'A_kappa_FF'));
S = load(strcat(dir, 'S')); S_FF = load(strcat(dirFF, 'S'));
thrust = S./F;   thrust_FF = S_FF./F_FF;

f_wt_water = F(10,1); f_ff_wt_water = F_FF(10,1);
f_wt_agar  = F(10,2); f_ff_wt_agar  = F_FF(10,2);
w_wt_water = W(10,1); w_ff_wt_water = W_FF(10,1);
w_wt_agar  = W(10,2); w_ff_wt_agar  = W_FF(10,2);
a_wt_water = A(10,1); a_ff_wt_water = A_FF(10,1);
a_wt_agar  = A(10,2); a_ff_wt_agar  = A_FF(10,2);
s_wt_water = S(10,1); s_ff_wt_water = S_FF(10,1);
s_wt_agar  = S(10,2); s_ff_wt_agar  = S_FF(10,2);
thr_wt_water = thrust(10, 1); thr_ff_wt_water = thrust_FF(10, 1);
thr_wt_agar  = thrust(10, 2); thr_ff_wt_agar  = thrust_FF(10, 2);

% Lan experiment - calc percentage frequency decrease from WT to mutant and
% compare with hyp1 sweeps (beta_0) 
% freq crawl
wt_fag = 0.38;
unc25_f_ag = (wt_fag - 0.35)/wt_fag % 0.0789
unc46_f_ag = (wt_fag - 0.28)/wt_fag % 0.2632
unc49_f_ag = (wt_fag - 0.21)/wt_fag % 0.4474

%speed crawl
wt_sag = 150;
unc25_s_ag = (wt_sag - 100)/wt_sag % 0.3333
unc46_s_ag = (wt_sag - 80)/wt_sag  % 0.4667
unc49_s_ag = (wt_sag - 50)/wt_sag  % 0.6667

%freq swim
wt_fw = 1.65;
unc25_f_wa = (wt_fw - 0.8)/wt_fw % 0.5152
unc46_f_wa = (wt_fw - 1.0)/wt_fw % 0.3939
unc49_f_wa = (wt_fw - 0.3)/wt_fw % 0.8182

%speed swim
wt_sw = 400;
unc25_s_wa = (wt_sw - 200)/wt_sw % 0.5000
unc46_s_wa = (wt_sw - 180)/wt_sw % 0.5500
unc49_s_wa = (wt_sw - 80)/wt_sw  % 0.8000


 %% plots
figure(1) % frequency
hold on;
plot(beta0(7:end), F(7:end,1)./f_wt_water, 'b.-', beta0(6:end), F(6:end,2)./f_wt_agar, 'r.-', 'Markersize', 20, 'Linewidth', 1.5)
plot(beta0, F_FF(:,1)./f_ff_wt_water, 'bs:', beta0, F_FF(:,2)./f_ff_wt_agar, 'rd:', 'Markersize', 8, 'Linewidth', 1.5)
plot(beta0, repmat(1-unc25_f_wa, [1 length(beta0)]), ':c', beta0, repmat(1-unc25_f_ag, [1 length(beta0)]), ':m', 'Markersize', 20, 'Linewidth', 1.5)
plot(linspace(1,1,100), linspace(0, 1.5, 100), 'k--')
text(0.9,0.9,'Exp. crawl speed', 'FontSize', 12, 'Color', 'm')
text(0.9,0.4,'Exp. swim speed', 'FontSize', 12, 'Color', 'c')
hold off;
xlim([0.7, 1.3])
xlabel('Muscle activation amplitude (C)', 'FontSize', 12)
ylabel('Normalized frequency', 'FontSize', 12)
legend('feedback water','feedback agar','feedforward water','feedforward agar', 'location', 'northwest')
title('hyp 1')

figure(2) % speed
hold on;
plot(beta0(7:end), S(7:end,1)./s_wt_water, 'b.-', beta0(7:end), S(7:end,2)./s_wt_agar, 'r.-','Markersize', 20, 'Linewidth', 1.5 )
plot(beta0, S_FF(:,1)./s_ff_wt_water, 'bs:', beta0, S_FF(:,2)./s_ff_wt_agar, 'rd:','Markersize', 8, 'Linewidth', 1.5 )
%plot(beta0, S_mean_water, ':m', beta0, S_mean_agar, ':c','Markersize', 20, 'Linewidth', 1.5 )
plot(beta0, repmat(1-unc25_s_wa, [1 length(beta0)]), ':c', beta0, repmat(1-unc25_s_ag, [1 length(beta0)]), ':m', 'Markersize', 20, 'Linewidth', 1.5)
plot(linspace(1,1,100), linspace(0, 1.5, 100), 'k--')
text(0.9,0.7,'Exp. crawl speed', 'FontSize', 12, 'Color', 'm')
text(0.9,0.4,'Exp. swim speed', 'FontSize', 12, 'Color', 'c')
hold off;
xlabel('Muscle activation amplitude (C)', 'FontSize', 12)
ylabel('Normalized Speed', 'FontSize', 12)
%legend('feedback water','feedback agar','feedforward water','feedforward agar', 'location', 'northwest')
title('hyp 1')
xlim([0.7 1.3])


%% save stuff
% data
% save('F', 'F', '-ascii');
% save('W', 'W', '-ascii');
% save('A_kappa_FF', 'A_kappa_FF', '-ascii');
% save('S', 'S', '-ascii')

% figuressave
% figure(1); print('hyp1_sweep_freq_norm','-dpng');   savefig('hyp1_sweep_freq_norm');
% figure(2); print('hyp1_sweep_speed_norm','-dpng');  savefig('hyp1_sweep_speed_norm');




