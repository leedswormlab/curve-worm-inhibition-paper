//
// (c) 2017 scs.leeds.ac.uk
// Author: Felix Salfelder 2017
//
// a worm-graph thing

#include "config.hh"
#include <boost/graph/properties.hpp>
#include <boost/functional/hash.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <opencv2/features2d.hpp>

#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>

// #include <opencv2/opencv.hpp> // c++ headers
#include "lib/trace.hh"
#include "lib/io_misc.hh"

#include "lib/parameter.hh"
#include "lib/matrix_graph.hh"
#include "lib/error.hh"
#include "lib/options.hh"

#include "lib/image/util.hh"
#include "lib/image/freak.hh"
#include "lib/image/thinning.hh"
#include "lib/mesh.hh"
// #include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

#include "lib/error.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"
#include "lib/image/annotations.hh"

#ifndef WORLD_DIM
#define WORLD_DIM 3
#endif
#ifndef NCAMS
#define NCAMS 3
#endif

#define USE_FREAK

bool raw=0;

// these should be parameters/cmdline args
// (some are)
double initialguess_known = 1.;
double initialguess_unknown = -1.;
double deltat=1e-4;
unsigned numberofsteps=5;
unsigned do_thinning = 0;
/*------------------------------------------------------*/

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

typedef cv::Mat frame_type;
typedef std::array< cv::Mat, NCAMS > frames_type;
typedef std::deque< cv::Mat > frame_deque_type;
typedef std::deque< frames_type > frames_deque_type;

void run_wormgraph(ConfigParameters const& );
//typedef std::pair<unsigned, unsigned> coord_type;
typedef cv::Point coord_type;

#if 0
namespace std{
static bool operator<(coord_type const& a, coord_type const& b){
	return make_pair(a.x, a.y) < make_pair(b.x, b.y);
}
} // std
#endif


// represent a pixel on a frame
// this is expensive...
struct vertex_repr_type{
	vertex_repr_type() : _frame(NULL) {
	}
	vertex_repr_type(coord_type const& c, frame_type const& f)
		: _frame(&f), _which(c){ itested();
	}
	vertex_repr_type(vertex_repr_type const& o)
		: _frame(o._frame), _which(o._which){ itested();
	}

public:
	frame_type const& frame()const{
		assert(_frame);
		return *_frame;
	}
	coord_type const& coord()const{
		assert(_which.x<99999);
		assert(_which.y<99999);
		return _which;
	}
private:
#ifndef NDEBUG
public:
#endif
	frame_type const* _frame;
	coord_type _which;
}; // vertex_repr_type

typedef vertex_repr_type pixel_address;
typedef std::array<uchar, 64> pixel_features;

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
		  boost::property<boost::vertex_color_t, long int,
#ifdef USE_FREAK
		  boost::property<boost::vertex_owner_t, pixel_features> >,
#else
		  boost::property<boost::vertex_owner_t, vertex_repr_type> >,
#endif
		  boost::property<boost::edge_weight_t, double>
			  > edgeweighted_graph_type1;

namespace framestuff{


std::string bgimgname(std::string const& aviname)
{
	auto p=aviname.find("cam");
	std::string ret="BGimg_" + aviname.substr(0, p+4) + ".png";
	return ret;
}


} // framestuff

frame_type const& get_frame(framestuff::annotated_frame const& f)
{
	return f.frame();
}
typedef framestuff::annotated_frame::annotation_type annotation_type;
annotation_type const& get_annotation(framestuff::annotated_frame const& f)
{
	return f.annotation();
}

template<class A>
float count_incidences(cv::Mat const& m, A const& p)
{
	unsigned hit=0;
	unsigned miss=0;
	for(auto i: p){
		int X=i.first.x; // BUG,vector?
		int Y=i.first.y;
		if(m.at<uchar>(Y, X)){
			++hit;
		}else{
			++miss;
		}
	}

	return float(hit) / float(hit+miss);
}


int main( int argc, char **argv )
{

	std::string aviname="";
	std::string frameno="";
	std::string dumpfile="";
	std::string bgname="";
	unsigned dilate=3;
	bool display=true;
	float printhit=0;
	bool sil=false;
	bool postp_steps=false;

	ConfigParameters parameters;
	parameters.set("bgname", "");
	parameters.set("dilate-sil", "3");
	parameters.set("aviname", "worm.avi");
	parameters.set("frameno", "0");

	if(argc<2){
		std::cerr << "need headclip csv filename\n";
	}
	bool bgs=false;

	int i=1;
	// poor mans getopt. yes it's limited, but it works.
	for(; i<argc; ++i){
		trace1("main", argv[i][0]);
		if(argv[i][0]=='-'){
			switch(argv[i][1]){ untested();
				case 'T':
					options::errorlevel = bTRACE;
					break;
				case 's': untested();
					sil = true;
					break;
				case 'N': itested();
					display = false;
					break;
				case 'H': itested();
					printhit = .95;
					break;
				case 'd': untested();
					 ++i;
					dilate = atoi(argv[i]);
					break;
				case 'o': untested();
					 ++i;
					dumpfile = argv[i];
					break;
				case 'p': untested();
					 ++i;
					postp_steps = atoi(argv[i]);
					break;
				case 't':
					 ++i;
					do_thinning = atoi(argv[i]);
					break;
				case 'B': untested();
					 ++i;
					parameters.set("bgname", argv[i]);
				case 'b':
					bgs = true;
					break;
				case 'D':
					options::errorlevel = bDEBUG;
					break;
				//case 't':
				//			 ++i;
				//			 deltat=atof(argv[i]);
				//			 std::cout << "deltat override" <<deltat<< "\n";
				//			 ///  skip frames...
				//			 break;
			}
		}else{
			break;
		}
	}

	std::string csvfilename=argv[i];

	unsigned dilate_sil=parameters.get<unsigned>("dilate-sil");

	auto p=csvfilename.find_last_of("/");

	std::string dir="";
	std::string file=csvfilename;
	
	if(p!=std::string::npos){
		file=csvfilename.substr(p);
		dir=csvfilename.substr(0,p);
	}

	trace2("ann", dir, csvfilename);

	typedef framestuff::annotated_frame annotated_frame;
	// annotated_frame a(dir + "/" + csvfilename, annotated_frame::_CSV);
	annotated_frame a;

	bgname=parameters.get<std::string>("bgname");
	if(bgname==""){ untested();
		a=annotated_frame(dir + "/" + file, annotated_frame::_CSV, bgs);
	}else{ untested();
		cv::Mat BG;
		if(bgs){untested();
			get_grayscale_image(bgname, BG);
		}else{ untested();
		}
		a=annotated_frame(dir + "/" + file, BG);
		auto bb = a.bounding_box();

		display_grayscale_frame("bg", BG, bb);
	}

	cv::Mat M;
	bounding_box_type bb;

	if(sil){ untested();
		bb = a.show_me(M);
		cv::Mat S=a.frame().clone();
		silhouetteImageDilate(a.frame(), S, dilate, 200.);
		M = S;
	}else{ untested();
		a.postprocess(postp_steps, do_thinning);
		M.release();
		if(raw){
			bb = a.rawimg(M);
		}else{
			bb = a.show_me_colour(M);
		}
	}

	message(bDEBUG, "bb %d %d\n", bb.first.x, bb.first.y);

	if(display){
		display_grayscale_frame("ann", M, bb);
	}else{
	}

	if(dumpfile!=""){ untested();
		message(bLOG, "dumping %s\n", dumpfile.c_str());
		dump_grayscale_frame(dumpfile, M, bb);
	}else{
	}
}
