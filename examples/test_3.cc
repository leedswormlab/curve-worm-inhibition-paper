
#include <boost/graph/adjacency_list.hpp>
#include "lib/graph/spectral.hh"
#include "lib/matrix.hh"
#include "lib/io_misc.hh"
#include "lsymsol.h"
#include <boost/numeric/ublas/lu.hpp>

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
//		  boost::property<boost::vertex_owner_t, vertex_repr_type>,
		  boost::no_property,
		  boost::property<boost::edge_weight_t, double>
			  > edgeweighted_graph_type_simple;

using spectral::SYM_DENSE_MATRIX;
using spectral::DIAGONAL_MATRIX;
using namespace boost::numeric::ublas; // yuck

template<class U, class M, class LAMBDA>
void iter(U& b, M const& evb, M const& Ainv, LAMBDA const& factors);

template<class M, class LAMBDA>
class HE_discrete{
public:
	HE_discrete(M const& evb, M const& Ainv, LAMBDA const& factors)
		: _evb(evb), _ainv(Ainv), _factors(factors)
	{
	}
	template<class U>
	void iter(U& u) {
		unsigned DIM=_factors.size();
		boost::numeric::ublas::vector<double> b(DIM);
		b = prod(_ainv, u);

		for(unsigned i=0; i<DIM; ++i){
			b(i) *= _factors(i);
		}
		u = prod(_evb, b);
	}
private:
	M _evb;
	M _ainv;
	LAMBDA _factors;
};

template<class M, class V>
HE_discrete<M,V> make_he_discrete(M const& m, M const& inv, V const& fac)
{
	return HE_discrete<M, V>(m, inv, fac);
}
int main(){

	unsigned DIM=6;
	edgeweighted_graph_type_simple g(DIM);

	// o       o
	// |\     /|
	// | o---o |
	// |/     \|
	// o       o
	//
	double EDGWT = 1.;
	auto e=boost::add_edge(0,1,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(0,2,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(1,2,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(2,3,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(3,4,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(3,5,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(4,5,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;

	SYM_DENSE_MATRIX<double> L(1, DIM);
	DIAGONAL_MATRIX<double> sD(g, DIAGONAL_MATRIX<double>::_SQRTDEG);
	SYM_DENSE_MATRIX<double> M(g);

	M.conjugate(sD);

	L -= M;
	std::cout << "LAGRANGIAN\n";
	std::cout << L;

	ARluSymStdEig<double> dprob(DIM-1, L, "LM");

	// Finding eigenvalues and eigenvectors.
	dprob.FindEigenvectors();
	// Printing solution.
	Solution(L, dprob);

	// trying to flow.
	boost::numeric::ublas::vector<double> u(DIM);
	boost::numeric::ublas::vector<double> lambda(DIM);
	boost::numeric::ublas::vector<double> factors(DIM);

	matrix<double> evb (DIM, DIM);

	for (unsigned i=0; i<DIM; i++) {
		evb(0, i) = sqrt(1./DIM);
		u(i)=0;
	}
	u(0) = 1.;
	std::cout << "u\n" << u << "\n";

	lambda(0) = 0.;

	for (unsigned i=1; i<DIM; i++) { untested();
		auto column = dprob.RawEigenvector(i-1);
		lambda(i) = dprob.Eigenvalue(i-1);
		for (unsigned j=0; j<DIM; j++) { untested();
			evb(i,j)=column[j];
		}
	}

#if 1
	matrix<double> tmp(evb);
	auto id=identity_matrix<double>(evb.size1());
	matrix<double> Ainv(id);
	permutation_matrix<size_t> pm(evb.size1());
	lu_factorize(tmp, pm);
	lu_substitute(tmp, pm, Ainv);

	std::cout << "Ainv " << Ainv.size1() << " " << Ainv.size2() << "\n";
	std::cout << Ainv << "\n";
#else
	untested();
	permutation_matrix<size_t> pm(evb.size1());
  	lu_factorize(evb, pm);
	lu_substitute(evb, pm, u);

	untested();
#endif


	double deltat=1e-4;

	for(unsigned i=0; i<DIM; ++i){
		//factors(i) = .7 * deltat / (1+lambda(i)*deltat);
		factors(i) = 1. / (1.+lambda(i)*deltat);
		// factors(i) = 1.;
	}

	auto he=make_he_discrete(evb, Ainv, factors);

	std::cout << "initial u\n" << u << "\n";
	for(unsigned i=0; i<1000; ++i){
		he.iter(u);
		std::cout << u << "\n";
	}

	
	return 0;
}
