/*
 *
 * found this in /usr/share/doc/arpack.../examples

   1) Problem description:

      In this example we try to solve A*x = x*lambda in regular 
      mode, where A is derived from the standard central difference
      discretization of the 2-dimensional Laplacian on the unit 
      square with zero Dirichlet boundary conditions.

   2) Data structure used to represent matrix A:

      Although A is very sparse in this example, it is stored
      here as a dense symmetric matrix. The lower triangular part
      of A is stored, by columns, in the vector A.

*/

#include "dsmatrxa.h"
#include <arpack++/ardsmat.h>
#include <arpack++/ardssym.h>
#include "lsymsol.h"
#include "lib/io_misc.hh"
#include "lib/matrix.hh"
#include "lib/matrix_graph.hh"



int main()
{

  // Defining variables;

  int     n;   // Dimension of the problem.
  double* A;   // Pointer to an array that stores the lower triangular
               // elements of A.

  // Creating a 100x100 matrix.

  n = 6;
  unsigned datasize=(n*n+n)/2;
  // "ds" means "dense" and "symmetric", probably
  SYM_DENSE_MATRIX<double> matrix(0, n);
  A = matrix.data();

  std::cout << matrix << "\n";
  // DenseMatrixA(nx, n, A);
  // we want this instead
  //  2 -1 -1
  // -1  2 -1
  // -1 -1  3 -1
  //  0  0 -1  3 -1 -1
  //  0  0  0 -1  2 -1
  //  0  0  0 -1 -1  2


  A[0] =  2; A[1] = -1; A[2] = -1;
             A[6] =  2; A[7] = -1;
                        A[11]=  3; A[12]=-1;
								           A[15]= 3; A[16]=-1; A[17]=-1;
											            A[18]= 2; A[19]=-1;
															          A[20]= 2;
#if 1
for(unsigned i=0; i<datasize; ++i){
	A[i]=0;
}
  A[0] =  1;
             A[6] =  1;
                        A[11]=  1;
								           A[15]= 0;
											            A[18]= 0;
															          A[20]= 6;
#endif

  // Defining what we need: the four eigenvectors of A with largest magnitude.

  ARluSymStdEig<double> dprob(6, matrix, "LM");

  // Finding eigenvalues and eigenvectors.

  dprob.FindEigenvectors();

  // Printing solution.

  Solution(matrix, dprob);

} // main.
