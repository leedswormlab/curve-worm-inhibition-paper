
#include <boost/graph/adjacency_list.hpp>
#include "lib/graph/spectral.hh"
#include "lsymsol.h"

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
//		  boost::property<boost::vertex_owner_t, vertex_repr_type>,
		  boost::no_property,
		  boost::property<boost::edge_weight_t, double>
			  > edgeweighted_graph_type_simple;

using spectral::SYM_DENSE_MATRIX;
using spectral::DIAGONAL_MATRIX;

int main(){

	edgeweighted_graph_type_simple g(6);

	// o       o
	// |\     /|
	// | o---o |
	// |/     \|
	// o       o
	//
	double EDGWT=.1;
	auto e=boost::add_edge(0,1,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(0,2,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(1,2,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(2,3,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(3,4,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(3,5,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;
	e = boost::add_edge(4,5,g).first;
	boost::get(boost::edge_weight, g, e) = EDGWT;

	SYM_DENSE_MATRIX<double> L(1, 6);
	DIAGONAL_MATRIX<double> sD(g, DIAGONAL_MATRIX<double>::_SQRTDEG);

	std::cout << "D\n";
	std::cout << sD;


	std::cout << "diag 1\n";
	std::cout << L;
	SYM_DENSE_MATRIX<double> M(g);

	std::cout << "weight matrix\n";
	std::cout << M;
	M.conjugate(sD);
	std::cout << "conj weight matrix\n";
	std::cout << M;

	L -= M;
	std::cout << "LAGRANGIAN\n";
	std::cout << L;

	ARluSymStdEig<double> dprob(5, L, "LM");

	// Finding eigenvalues and eigenvectors.
	dprob.FindEigenvectors();
	// Printing solution.
	Solution(L, dprob);

	return 0;
}
