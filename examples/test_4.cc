
#include <boost/graph/adjacency_list.hpp>
#include "lib/graph/spectral.hh"
#include "lib/matrix_graph.hh"
#include "lib/vector.hh"
#include "lib/solver.hh"
#include "lib/io_misc.hh"
#include "lsymsol.h"
#include <boost/numeric/ublas/lu.hpp>

typedef boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS,
//		  boost::property<boost::vertex_owner_t, vertex_repr_type>,
		  boost::no_property,
		  boost::property<boost::edge_weight_t, double>
			  > edgeweighted_graph_type_simple;

template<class U, class M, class LAMBDA>
void iter(U& b, M const& evb, M const& Ainv, LAMBDA const& factors);

template<class M, class LAMBDA>
class HE_discrete{
public:
	HE_discrete(M const& evb, M const& Ainv, LAMBDA const& factors)
		: _evb(evb), _ainv(Ainv), _factors(factors)
	{
	}
	template<class U>
	void iter(U& u) {
		unsigned DIM=_factors.size();
		boost::numeric::ublas::vector<double> b(DIM);
		b = prod(_ainv, u);

		for(unsigned i=0; i<DIM; ++i){
			b(i) *= _factors(i);
		}
		u = prod(_evb, b);
	}
private:
	M _evb;
	M _ainv;
	LAMBDA _factors;
};

template<class M, class V>
HE_discrete<M,V> make_he_discrete(M const& m, M const& inv, V const& fac)
{
	return HE_discrete<M, V>(m, inv, fac);
}
int main(){

	unsigned DIM=8;
	edgeweighted_graph_type_simple g(DIM);

	//  o---o - --- o ---o
	double EDGWT = 1.;
	for(unsigned x=1; x<DIM; ++x){
		trace2("", x-1, x);
		auto e=boost::add_edge(x-1,x,g).first;
		boost::get(boost::edge_weight, g, e) = EDGWT;
	}

	double deltat=.01;
	UmfpackMatrix M(g, UmfpackMatrix::_NORMALIZED_LAPLACIAN, deltat);

	std::cout << M;

	Vector u(DIM);

	u[1]=2;
	u[4]=9;
	Vector x(DIM);

//	solving A*x=b;...
	
	// umfpackSolve(M, x, u);
	impl::UMF_LU lu(M);
	lu.backsubst(x,u);

	std::cout << "initial u\n" << u << "\n";
	for(unsigned i=0; i<100; ++i){

	//	umfpackSolve(M, x, u);
		lu.backsubst(x,u);
		x*=.95;
		std::cout << i << " " <<  x << "\n";
		u=x;
	}
}

