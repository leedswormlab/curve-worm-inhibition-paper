//
// (c) 2017 scs.leeds.ac.uk
// Author: Felix Salfelder 2017
//
// a worm-graph thing

#include "config.hh"
#include <boost/graph/properties.hpp>
#include <boost/functional/hash.hpp>
#include <boost/graph/adjacency_matrix.hpp>
#include <boost/graph/adjacency_list.hpp>

#include <opencv2/features2d.hpp>

#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>

// #include <opencv2/opencv.hpp> // c++ headers
#include "lib/trace.hh"
#include "lib/io_misc.hh"

#include "lib/parameter.hh"
#include "lib/matrix_graph.hh"
#include "lib/error.hh"
#include "lib/image/util.hh"

// mesh and time helpers
#include "lib/mesh.hh"
#include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"
#include "lib/image/freak.hh" // FIXME: use FREAKMAP

#include "lib/options.hh"

using cv::xfeatures2d::FREAK;

int main( int argc, char **argv )
{ untested();
	ConfigParameters parameters;
  parameters.set("bg", "bg.png");

	int i=1;
	// poor mans getopt. yes it's limited, but it works.
	unsigned framenumber=0;
	float patternScale=22.0f;
	for(; i<argc; ++i){ untested();
		trace1("main", argv[i][0]);
		if(argv[i][0]=='-'){ untested();
			switch(argv[i][1]){
				case 'n':
					++i;
					framenumber = atoi(argv[i]);
					break;
				case 's': untested();
					++i;
					patternScale = atof(argv[i]);
					break;
				case 'T': untested();
					options::errorlevel = bTRACE;
					break;
			}
		}else if(argv[i][0]=='-' && argv[i][1]=='-'){
			try{
				parse(parameters, &argv[i][2]); // only works with "="
			}catch(exception_nomatch){
				incomplete();
			  std::cerr << "something wrong with '" << argv[i] << "'\n";
			  exit(1);
			}

		}else{ untested();
			break;
		}
	}
	if(i!=argc-1){
		std::cerr << "usage: \n";
		std::cerr << argv[0] << "[options] <image_filename>\n";
		exit(1);
	}else{ untested();
	}
	std::string imagefile(argv[i]);
	std::string bgfile=parameters.get<std::string>("bg");

	cv::Mat M;
	cv::Mat B;
	// get_grayscale_image(imagefile, M);
	//
	message(bTRACE, "image %s frame %d\n", imagefile.c_str(), framenumber);
	message(bTRACE, "bg %s\n", bgfile.c_str());

	getframefromfile(imagefile, M, framenumber);
	getframefromfile(bgfile, B, 0);

	display_grayscale_frame("1", M);

	assert(M.rows = B.rows);
	assert(M.cols = B.cols);

	std::vector<cv::KeyPoint> keypoints;
	B = B-M;

	// need to find initial midline.
	cv::Mat sil=B.clone();
	silhouetteImage(B, sil, 0);

	auto p=bbox(sil);

	//		cv::imwrite( "result"+infix+".png", bb );
	//

	bool orientationNormalized=true;// Enable orientation normalization.
	bool scaleNormalized=false; // Enable scale normalization.
	unsigned nOctaves=1; // Number of octaves covered by the detected keypoints.

	auto _backend=FREAK::create( orientationNormalized, scaleNormalized,
			patternScale, nOctaves);

	{
		// broken _backend->detect( B, keypoints );
	}

	for( int i = 0; i < sil.rows; ++i ){ itested();
		for( int j = 0; j < sil.cols; ++j ) { itested();
			if( sil.at<uchar>(j,i) == (uchar)255 ) {
				keypoints.push_back(cv::KeyPoint(float(i), float(j), 1));
			}else{
			}
		}
	}
	std::cout << "have " << keypoints.size() << " keypoints\n";
	size_t bak=keypoints.size();

	adjust_brightness_max(B);
	//display_grayscale_frame("1", B, p);

	cv::Mat descriptors;
	_backend->compute(B, keypoints, descriptors);
	assert( bak==keypoints.size());
	(void)bak;

	std::cout << descriptors.rows << "x" << descriptors.cols << "\n";
	cv::Mat out=B.clone();
	std::cout << "feat mat " << misc::type2str(descriptors.type()) << "\n";

	for(unsigned cn=0; cn< unsigned(descriptors.cols); ++cn)
	{
		unsigned seek=0;
		for( int i = 0; i < sil.rows; ++i ){ itested();
			for( int j = 0; j < sil.cols; ++j ) { itested();
				if( sil.at<uchar>(j,i) == (uchar)255 ) {
					out.at<uchar>(j,i) = descriptors.at<uchar>(seek, cn);
					++seek;
				}else{
				}
			}
		}
		auto infix=std::to_string(cn);
		dump_grayscale_frame("freak_"+infix+".png", out, p);
	}

	// display_grayscale_frame("2", out, p);



#if 0
	dumpmatrix()
	for( int i = 0; i < descriptors.rows; ++i ){ itested();
		std::cout <<  i;
		for( int j = 0; j < descriptors.cols; ++j ) { itested();
			std::cout << " " << unsigned(descriptors.at<uchar>(j,i));
		}
		std::cout <<  "\n";
	}
#endif

}


