#include <globals.h>
#include <e_compon.h>
#include <s__.h>
#include <e_node.h>
#include <l_denoise.h>
#include "femsim.h"

#include "worm.hh"
#include "vtuwriter.hh"
#include <io_trace.h>

#include "model_lambda.hh"
/*--------------------------------------------------------------------------*/
inline unsigned iteration(SIM_DATA const& s)
{
	return s.iteration_tag();
}
/*--------------------------------------------------------------------------*/
inline double time(SIM_DATA const& s)
{
	std::cerr<< "st0 " << s._time0 << "\n";
	return s._time0;
}
/*--------------------------------------------------------------------------*/
#define NUM_MUSCLES 12 // BUG worm parameter
#define NUM_CURVATURES 3 // BUG worm parameter

typedef int uint_t;

/*--------------------------------------------------------------------------*/
TimeProvider timeProvider;
Mesh mesh( 0., 1., MESHSIZE ); // here?
/*--------------------------------------------------------------------------*/
//TODO
static const double beta0=10.;
static const double beta1=6.;
static const double lambda=0.65;
static const double omega=1.0;
/*--------------------------------------------------------------------------*/
void	SIM::outdata(double, int){ untested();
	unreachable();
}
SIM::~SIM(){ }
/*--------------------------------------------------------------------------*/
// not used.
void SIM::head(double,double,const std::string&) {unreachable();}
void SIM::print_results(double){unreachable();}
void SIM::alarm(){unreachable();}
void SIM::store_results(double){unreachable();}
void SIM::solve_equations(){unreachable();}
void SIM::clear_arrays(){unreachable();}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
namespace{
/*--------------------------------------------------------------------------*/
template<unsigned dim>
class WORM;
// turn currents into beta used in model
class MuscleControl{
private:
	MuscleControl() = delete;
public:
	MuscleControl(const MuscleControl& m)
		: _n(m._n)
	{
			// need (?) this to act as a functor. YIKES
	}

	MuscleControl(node_t const* n) : _n(n){ }

	// provide beta.
	double operator()(double u, double t ) const { itested();

#if 0
		 double ref = ( beta0 * (1-u) + beta1 * u )
		 	* sin( 2.0 * M_PI * ( u / lambda -  omega * t ));
		 return ref;
#endif
		
		unsigned pos = ((NUM_MUSCLES)*u-1e-5);
		

		assert(pos<NUM_MUSCLES);
		return _n[pos].v0();
	};

private:
	node_t const* _n;
};

template<unsigned dim>
class WORM : public COMPONENT{
   typedef LambdaMuscleModel< dim > ModelType; // TODO: why lambdaMuscle?
	typedef PiecewiseLinearFunction< dim > CurvatureFunctionType;
public:
	WORM()
		: _musclecontrol(_nodes),
		  _model(_musclecontrol),
		  _vw(*_sim), //?!
		  _e(_w)
	{
		assert(_sim);
		_n = _nodes;
		// _vw->setBeta(*_sim);
//		_model.setBeta(&_musclecontrol); TODO
	}
	WORM(const WORM& w)
		: _musclecontrol(_nodes), _model(_musclecontrol), _e(_w),
		  _vw(*_sim)
	{
		_n = _nodes;
	}
	virtual CARD* clone()const{
		return new WORM(*this);
	}
private: // overrides
	int param_count()const {untested();
		return (1 + COMPONENT::param_count());
	}
	std::string param_name(uint_t i) const{ untested();
		uint_t j = param_count()-1-i;
		switch(j) {
			case 0: return "eta";
			case 1: return "K";
			default: return COMPONENT::param_name(i);
		}
	}
	bool param_is_printable(uint_t i)const
	{ untested();
		trace1("pip", i);
		switch(param_count()-1-i) {
			case 0: return true;
			case 1: return true;
			default: COMPONENT::param_is_printable(i);
		}
	}
	std::string param_value(uint_t i)const{ untested();
		switch(param_count()-1-i) {
			case 0: return _eta.string();
			case 1: return _K.string();
			default: return COMPONENT::param_value(i);
		}
	}
	void set_param_by_name(std::string Name, std::string Value) { untested();
		if(Name=="eta"){ untested();
			_eta = Value;
		}else if(Name=="K"){ untested();
			_K = Value;
		}else{ untested();
			incomplete();
		}
	}

   std::string dev_type()const{ return "worm1"; }
   std::string port_name(int i)const{ untested();
		if(i<NUM_MUSCLES){ untested();
			return "m"+std::to_string(i);
		}else if(1){ untested();
			return "c"+std::to_string(i-NUM_MUSCLES);
		}else{ unreachable();
			return "-";
		}
	}
   std::string value_name()const{ return "none"; }
   bool print_type_in_spice()const{ untested();
		unreachable();
		return false;
	}
   int min_nodes()const	{
		return NUM_MUSCLES;
	}
   int max_nodes()const	{
		return NUM_MUSCLES+NUM_CURVATURES;
	}
	int net_nodes()const	{
		return _net_nodes;
	}
	void tr_load_source() {
#if !defined(NDEBUG)
// not yet.
//		assert(_loaditer != _sim->iteration_tag()); // double load
//		_loaditer = _sim->iteration_tag();
#endif

		node_t* out=_n+NUM_MUSCLES;

		for(unsigned i=0; i<_net_nodes - NUM_MUSCLES; ++i){
			double d = dampdiff(&_c[i], _c1[i]);
			if (d != 0.) {
				if (_c[i] != 0) {
					out[i].i() -= d;
				}else{ untested();
				}
			}else{
			}
			_c1[i] = _c[i];
		}
	}
	void precalc_first() {
		const CARD_LIST* par_scope = scope();
		assert(par_scope);
		COMPONENT::precalc_first();
		e_val(&_eta, 0., par_scope);
		_w.set_eta(_eta);

		_have_eta = _eta.has_hard_value();
	}

	void expand(){
		_w.setN(MESHSIZE);
		_w.expand(scheme);
		_w.setModel(&_model);
		scheme.init(); // BUG

	   auto X0 = []( const double u ) -> typename ModelType :: RangeVectorType
		{ untested();
			typename ModelType :: RangeVectorType ret;
			ret[1] = u;
			ret[0] = 0.0;
			return ret;
		};
		_w.initialiseXY();
		scheme.advance_time(); // yikes!
	}
	bool do_tr(){
		trace1("do_tr", _sim->_time0);
		_w.eval();
		eval_curv();
		q_load();
		return	true;
	}
	void tr_load(){
		_e.tr_load();
		tr_load_source();
	}
	void tr_begin(){
		_w.tr_begin();
		std::fill(_c, _c+NUM_CURVATURES, 0);
		std::fill(_c1, _c1+NUM_CURVATURES, 0);
		q_eval(); // needed?
		tr_accept(); // wrong?
		q_load();
		trace2("worm::tr_begin", _sim->_time0, _w.time());
	}
	void tr_restore(){ untested();
		_w.tr_restore();
	}
   bool tr_needs_eval()const	{return true;}

	void tr_regress(){ untested();
		trace2("worm::tr_regress0", _sim->_time0, _w.time());
		_e.tr_regress();
	}

	void tr_advance(){
		double deltat = _sim->_time0 - _w.time();
		trace2("worm::tr_advance", _w.time(), _w.time1());
		_e.tr_advance();
		trace1("tr_advance", _w.time());
	}
	TIME_PAIR tr_review(){
		double t = _e.review();
		q_accept();
		return TIME_PAIR(t, NEVER);
	}
	void tr_accept(){
		_e.tr_accept();
		trace2("acc, wr ", _sim->_time0, _sim->iteration_tag() );
		_vw.writeVtu(_w.position(), _w.curvature(), _w.pressure());
	}
	// better way? yes. use vtk
	double tr_probe_num(std::string const& s) const{ itested();
		if(s=="headx"){
			return _w.head()[0];
		}else if(s=="heady"){
			return _w.head()[1];
		}else if(s=="midx"){
			return _w.mid()[0];
		}else if(s=="midy"){
			return _w.mid()[1];
		}else if(s=="tailx"){
			return _w.tail()[0];
		}else if(s=="taily"){
			return _w.tail()[1];
		}else if(s=="cogx"){ untested();
			return _w.cog()[0];
		}else if(s=="cogy"){ untested();
			return _w.cog()[1];
		}else if(s=="time"){ untested();
			return _w.time();
		}else if(s=="newtime"){
			return _w.newtime();
		}else{ untested();
		}
	}
private: // internals
	unsigned N() const{
		return MESHSIZE; // incomplete
	}
	// meshobject function?
	double curv(unsigned m, CurvatureFunctionType const& c) const{
		auto const& X=_w.position();
		assert(m<N());
		if(m==0){
			return 0;
		}else if(m==N()-1){
			return 0;
		}else{
			auto tau=(X.eval(m+1)-X.eval(m-1));
			tau/=norm(tau);
			tau.i();
			return tau * c.eval(m);
		}
	}
	void eval_curv(){
		auto c=_w.curvature();
		unsigned num_curvs=_net_nodes - NUM_MUSCLES;
		unsigned m=0;
		unsigned end=0;
		trace2("eval_curv", num_curvs, N());

		for(unsigned i=0; i<num_curvs; ++i){
			_c[i] = 0;
			end += N()/num_curvs;
			for(; m<end; ++m){
				_c[i] += curv(m, c);
			}
		}
	}
	double dampdiff(double* v0, const double& v1)
	{
		//double diff = v0 - v1;
		assert(v0);
		assert(*v0 == *v0);
		assert(v1 == v1);
		double diff = dn_diff(*v0, v1);
		assert(diff == diff);
		if (!_sim->is_advance_or_first_iteration()) {
			diff *= _sim->_damp;
			*v0 = v1 + diff;
		}else{
		}
		return mfactor() * ((_sim->is_inc_mode()) ? diff : *v0);
	}
private:
	bool _have_eta;
	PARAMETER<double> _eta;
	PARAMETER<double> _K;
private:
	node_t _nodes[NUM_MUSCLES+NUM_CURVATURES];
   MuscleControl _musclecontrol;
   LambdaMuscleModel< dim > _model;
	worm_<dim> _w;
	Entity& _e;
	double _c[NUM_CURVATURES];
	double _c1[NUM_CURVATURES];
private: // tmp hack
	VTUWriter_<SIM_DATA> _vw;
};

WORM<2> p3;
DISPATCHER<CARD>::INSTALL d3(&device_dispatcher, "worm1", &p3);
}
/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/
