#ifdef HAVE_CONFIG_H
#include "config.hh"
#endif

// HACK
#define OpenCV_FOUND

#include <iostream>
#include <projectionoperator.hh>

int main( int argc, char **argv )
{

	if(argc<2){
		std::cerr << "need more args\n";
		return(1);
	}else{
	}
	
	std::string calibfn(argv[1]);
	unsigned select=atoi(argv[2]);
	cv::FileStorage fs;
	fs.open( calibfn, cv::FileStorage::READ );

	typedef CameraProjectionOperator<3,2> CPO;

	typedef CPO::WorldRangeVectorType WRVT;

	if(!select){
		std::cerr << "need select\n";
		return(1);
	}

	if(select==1){
		select=0;
	}else if(select==2){
		select=1;
	}else if(select==4){
		select=2;
	}else{
		incomplete(); // only one for now.
	}

	CameraProjectionOperator<3,2> P=makeProjectionOperator(fs, select);

	WRVT X;

	while(1){
		std::cin >> X[0];
		std::cin >> X[1];
		std::cin >> X[2];
		if(std::cin.eof()){
			break;
		}
		auto PX=P(X);
//		std::cout << X << "\n";
		std::cout << PX[0] << " " << PX[1] << "\n";
	}

}
