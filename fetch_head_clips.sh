#!/bin/sh

rm head_clips.local/*.csv

# for now.
[ -e head_clips ] || mkdir -p head_clips.local/BG_imgs

RSYNC_ARGS=--max-size=250m

echo fetching csv

rsync ${RSYNC_ARGS} --exclude '*.bmp' --exclude '*.xml' \
  	-av comp-pc3135:/usr/not-backed-up/for-tom/head_clips/ \
	--exclude 'original/*.png' \
	head_clips.local/

( cd head_clips; ln -sf ../head_clips.local/*.csv . )
echo fetching BG

rsync -av comp-pc3135:/usr/not-backed-up/for-tom/head_clips/BG_imgs \
	--exclude '*.bmp' --exclude '*.xml' --exclude 'original/*.png' \
	head_clips.local/BG_imgs/

( cd head_clips; ln -sf ../head_clips.local/BG_imgs/*.png . )

dos2unix head_clips.local/*.csv

cd head_clips.local
ls *.csv > index

ln -s BG_imgs/* .

# now ./worm-graph -d filename.fg0 should create a graph dump.
