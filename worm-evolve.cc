//
// (c) 2017 scs.leeds.ac.uk
// Author: Felix Salfelder 2017
//
// a worm evolver
//
#define USE_FREAK
#define USE_NESTED_MAP

#include "config.hh"
#include "lib/options.hh"

#include <opencv2/features2d.hpp>
#include "opencv2/imgproc/imgproc.hpp" // morph?


#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>

// #include <opencv2/opencv.hpp> // c++ headers
#include "lib/trace.hh"
#include "lib/io_misc.hh"

#include "lib/parameter.hh"
#include "lib/error.hh"

#include "lib/image/util.hh"
#include "lib/image/freak.hh"
#include "lib/image/display.hh"
#include "lib/mesh.hh"
#include "lib/timer.hh"
// #include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"
#include "lib/image/annotations.hh"

#include "lib/bits/map.hh"
#include "lib/graph_pointfinder.hh"
#include "lib/graph/he.hh"
#include "lib/learn_graph.hh"

#ifndef WORLD_DIM
#define WORLD_DIM 3
#endif
#ifndef NCAMS
#define NCAMS 3
#endif

#define WGassert(x)

// these should be parameters/cmdline args
// (some are)
double initialguess_unknown = -1.;
double deltat=1e-4;
unsigned numberofsteps=5;

static const std::string restrict_sil_default="0";
/*------------------------------------------------------*/

typedef draft::neigh_similarity1 weights_type;
typedef draft::edgeweighted_graph_type1 G;
typedef draft::framesgraph<G> fg_type;

typedef cv::Point coord_type;

typedef std::array< cv::Mat, NCAMS > frames_type;
typedef std::deque< frames_type > frames_deque_type;

using draft::frame_type;
using draft::annotation_type;
//using draft::features_type;

void worm_evolve(ConfigParameters const& );
//typedef std::pair<unsigned, unsigned> coord_type;
#if 0
namespace std{
static bool operator<(coord_type const& a, coord_type const& b){ itested();
	return make_pair(a.x, a.y) < make_pair(b.x, b.y);
}
} // std
#endif

int main( int argc, char **argv )
{ itested();

	std::string aviname="";
	std::string bgname="";
	std::string loadfile="";
	std::string dilate_sil_radius="";

	ConfigParameters parameters;
	parameters.set("display", "0");
	parameters.set("print-summary", "0");

	parameters.set("bgname", "bg.png");
	parameters.set("aviname", "worm.avi");
	parameters.set("frameno", "0");

	parameters.set("eval-csv", "");
	parameters.set("eval-index", "");
	parameters.set("learn-csv", ""); // learn a particular annotation.
	parameters.set("learn-index", "");

	parameters.set("dumpfile", "");
	parameters.set("loadfile", "");

	parameters.set("initial-known", "0");
	parameters.set("initial-unknown", "0");


#include "worm_defaults.hh"

	parameters.set("headclips-index", "../head_clips/index");

	parameters.set("verbose", "0");

#ifdef USE_FREAK
	parameters.set("pattern-scale", "15");
	// pixels as close as this will be collapsed
	parameters.set("thresh-collapse", "80");

 	// no edge between vertices further than this.
	parameters.set("thresh-edge", "30000");
#else
	parameters.set("thresh-collapse", "40");
	parameters.set("thresh-edge", "250");
#endif

#include "worm_defaults.hh"

	int i=1;
	for(; i<argc; ++i){
		trace2("getopt", i, argv[i]);
		if(argv[i][0]=='-' && argv[i][1]!='-'){
			// one letter aliases.
			switch(argv[i][1]){ untested();
				case 'T':
					options::set_errorlevel( bTRACE);
					break;
				case 'Q':
					options::set_errorlevel( bDANGER);
					break;
				case 'L':
					options::set_errorlevel( bLOG);
					break;
				case 'D':
					options::set_errorlevel( bDEBUG);
					break;
				case 'E': untested();
					++i;
					 options::set_errorlevel( atoi(argv[i]));
					break;
				case 'l':
					++i;
					parameters.set("loadfile", argv[i]);
					break;
				case 'd':
				case 'o':
					++i;
					parameters.set("dumpfile", argv[i]);
					break;
				case 'k':
					++i;
					parameters.set("initial-known", argv[i]);
					break;
				case 'u':
					++i;
					parameters.set("initial-unknown", argv[i]);
					break;
				case 'v': untested();
					parameters.set("verbose", "yes");
					break;
				case 'b': untested();
					++i;
					parameters.set("bgname", argv[i]);
					break;
				case 'a': untested();
					++i;
					parameters.set("aviname", argv[i]);
					break;
				case 's': untested();
					parameters.set("print-summary", "1");
					break;
				case 'S': untested();
					++i;
					parameters.set("pattern-scale", argv[i]);
					break;
////				case 'D': untested();
////					++i;
////					parameters.set("dilate-tgt-sil", argv[i]);
////					parameters.set("dilate-sil", argv[i]);
////					break;
				case 'f': untested();
					++i;
					parameters.set("frameno", argv[i]);
					break;
				case 'm':
					++i;
					parameters.merge_file(argv[i]);
					break;
				case 'n':
					++i;
					numberofsteps = atoi(argv[i]);
					break;
				case 't':
					++i;
					parameters.set("deltat", argv[i]);
					break;
				case 'h':
					parameters.dump(std::cout);
					exit(1);
			}
		}else if(argv[i][0]=='-' && argv[i][1]=='-'){
			// pass to ConfigParameters
			trace1("got", &argv[i][2]);
			try{
				parse(parameters, &argv[i][2]);
				// ok, must have been : or =
				trace1("parsed", argv[i]);
			}catch(exception_nomatch){ untested();
				if(i+1<argc){ untested();
					parameters.update( &argv[i][2], argv[i+1]);
					++i;
				}else{ untested();
					std::cerr << "something wrong with '" << argv[i] << "'\n";
					exit(1);
				}
			}
		}else{ untested();
			// this is unnecessary, -m
			trace1("nodash", argv[i]);
			try{ untested();
				parameters.merge_file(argv[i]);
			}catch( ...) { untested();
				incomplete();
				std::cout << "something wrong w/params\n";
				return 1;
			}
		}
	}

	bool verbose=parameters.get<bool>("verbose");
	if(verbose){
	  // print variables
	  std::cout << "parameters" << std::endl;
	  parameters.dump( std::cout, " - ", " = " );
	  std::cout << std::endl;
	}else{
	}

	try{
		worm_evolve(parameters);
	}catch(exception_cantfind const& e){ untested();
		std::cerr << "paramerror in wormgraph: " << e.what() << "\n";
	}
}

namespace draft{

// this is a stub.
class pixel_sim_base{
protected:
	pixel_sim_base(){ untested();
	}
	pixel_sim_base(frame_type const& f)
		: _f1(&f) { untested();
	}

public:
	unsigned distance_square( vertex_repr_type a, vertex_repr_type b) const{ untested();
		_f1 = &a.frame();
		_f2 = &b.frame();
		return distance_square(a.coord(), b.coord());
	}

	unsigned distance_square( coord_type x, vertex_repr_type b) const{ untested();
		_f2 = &b.frame();
		return distance_square(x, b.coord());
	}

	virtual unsigned distance_square( coord_type x, coord_type y ) const { unreachable(); return -1u; }

public:
#if 0
	template<class T, class S>
	double similarity(T x, S b) const{ untested();
		unsigned edgeweight=distance_square(x, b);
		double variance=1.;
		return exp(-double(edgeweight)/variance);
	}
#endif
protected:
	uchar get(cv::Mat const& t, int x, int y) const{ untested();
		int imgsz=2048;
		if(x<0){ untested();
			return 0;
		}else if(y<0){ itested();
			return 0;
		}else if(x+1>imgsz){ untested();
			return 0;
		}else if(y+1>imgsz){ untested();
			return 0;
		}else{ untested();
			return t.at<unsigned char>(y, x);
		}
	}

protected: // BUG
	mutable frame_type const* _f1;
	mutable frame_type const* _f2; // yuck.
};

class pixel_similarity1 : public pixel_sim_base {
public:
	pixel_similarity1() : pixel_sim_base(){ untested();
	}
	pixel_similarity1(frame_type const& f)
		: pixel_sim_base(f) { untested();
	}
	pixel_similarity1(const pixel_similarity1& o)
		: pixel_sim_base(o) { untested();
	}
public:
	unsigned distance_square( coord_type x, coord_type y ) const{ itested();
		WGassert(_f1);
		WGassert(_f2);

		unsigned g1 = _f1->at<unsigned char>(x.x, x.y);
		unsigned g2 = _f2->at<unsigned char>(y.x, y.y);

		int delta = g1-g2;
		return delta*delta;
	}
};

} // draft

// need better backend. move to class...
void worm_evolve(ConfigParameters const& parameters)
{
	typedef framestuff::annotated_frame annotated_frame;
	bool display=parameters.get<bool>("display");
	bool print_summary=parameters.get<bool>("print-summary");

	std::string bgname=parameters.get<std::string>("bgname");
	std::string dumpfile=parameters.get<std::string>("dumpfile");
	std::string loadfile=parameters.get<std::string>("loadfile");
	std::string avifilename=parameters.get<std::string>("aviname");
//	unsigned frameno=parameters.get<unsigned>("frameno");

	std::string eval_csv=parameters.get<std::string>("eval-csv");
	std::string eval_index=parameters.get<std::string>("eval-index");

	// these only apply to /target/ frame
	unsigned dilate_tgt_sil=parameters.get<unsigned>("dilate-tgt-sil");
	// unsigned dilate_sil=parameters.get<unsigned>("dilate-sil");

	bool restrict_tgt_sil=parameters.get<bool>("restrict-tgt-sil");
//	bool restrict_sil=parameters.get<bool>("restrict-sil");

	unsigned thresh_edge=parameters.get<unsigned>("thresh-edge");
	unsigned thresh_collapse=parameters.get<unsigned>("thresh-collapse");
	int initialguess_known=parameters.get<int>("initial-known");
	int initialguess_unknown=parameters.get<int>("initial-unknown");

	double distance_scale=parameters.get<double>("distance-scale");
	std::string learn_csv=parameters.get<std::string>("learn-csv");
	std::string learn_index=parameters.get<std::string>("learn-index");

	double eucl_base=parameters.get<double>("graph.eucl-base");

	fg_type FG;

	if(loadfile!=""){
		message(bLOG, "loading graph from %s\n", loadfile.c_str());
		s_timer tmr("file load");
		std::ifstream myfile;
		myfile.open (loadfile);
		fg_type a(myfile, distance_scale);

		FG = std::move(a);
	}else{ untested();
		s_timer tmr("fg from index");
		make_framesgraph_from_indexfile(FG, parameters);
	}

	unsigned nv=::boost::num_vertices(FG);
	unsigned ne=::boost::num_edges(FG);

	message(bLOG, "read framesgraph. %d vertices %d edges\n", nv, ne);

	if(learn_csv!="" || learn_index!=""){
		message(bLOG, "learning " + learn_csv + "\n");
		typedef draft::framesgraph_pointfinder<G> fgpf_type;
		fgpf_type kpf(std::move(FG), deltat, initialguess_known, initialguess_unknown,
				thresh_collapse, thresh_edge,
				dilate_tgt_sil, restrict_tgt_sil, eucl_base);
		// hmm hide keypoint_finder in FG?!

		if(learn_index!=""){ untested();
			auto afd=make_annotated_frames_deque(learn_index);
			for(auto const& af: afd){ untested();
				message(bDEBUG, "learning " + af.label() + "\n");
				learn_frame(kpf, af);
				message(bDEBUG, "now have v/e %d/%d\n",
						boost::num_vertices(kpf.fg()),
						boost::num_edges(kpf.fg()));
			}
		}else if(learn_csv!=""){
			auto csv_af=annotated_frame(learn_csv, annotated_frame::_CSV);
			learn_frame(kpf, csv_af);
		}else{
		}

		FG=std::move(kpf.fg());
	}else{ untested();
	}

	if(dumpfile!=""){
		message(bLOG, "dumping into " + dumpfile + "\n");
		std::ofstream myfile;
		myfile.open (dumpfile);
		parameters.dump(myfile, "# ");
		FG.dump(myfile);
		return;
	}else{
	}

	framesindex index;
	if(eval_csv!=""){ untested();
		index = framesindex(eval_csv, framesindex::_SINGLE);
	}else if(eval_index!=""){ untested();
		index = misc::make_framesindex(eval_index);
	}else{
		std::cerr << "no eval index\n";
		exit(1);
	}

	{

		typedef draft::edgeweighted_graph_type1 G;
#if 0
		typedef draft::framesgraph_pointfinder<G> fgpf_type;
#else
		typedef draft::framesgraph_pointfinder_dissect<G> fgpf_type;
#endif

		fgpf_type* kpf = new fgpf_type(loadfile, deltat,
				initialguess_known, initialguess_unknown,
				thresh_collapse, thresh_edge, distance_scale,
				dilate_tgt_sil, restrict_tgt_sil, eucl_base);

		cv::Mat worm, tgtsil;
		annotation_type annotation;

		double distance_sum=0.;
		unsigned number=0.;

		ANNOTATION known;
		unsigned ii=0;
		for(auto fi=index.begin(); fi!=index.end(); ++fi){
			auto ef=framestuff::annotated_frame(index.dir() + "/" + *fi,
					framestuff::annotated_frame::_CSV);

			message(bDEBUG, "computing distance for " + ef.label() + "\n");

			cv::Mat worm=draft::get_frame(ef);
			tgtsil=worm.clone();
			// display_grayscale_frame("wormtodetect", worm);
			silhouetteImageDilate(worm, tgtsil, dilate_tgt_sil, 200); // FIXME
			// display_grayscale_frame("wormtodetect", tgtsil);
			known.clear();
			kpf->find_known_points(tgtsil, worm, known, ef.ppmm() );
			if(display){
				display_pointset_on_frame("result", worm, known);
			}else{untested();
			}
			dump_pointset_on_frame("evalresult"+std::to_string(ii)+".png", worm, known);
			++ii;

			annotation = draft::get_annotation(ef);
			double anndist=framestuff::annotated_frame::distance(known, annotation);
			std::cout << number << " " << anndist << " " << *fi << "\n";

			if(anndist==anndist){
				distance_sum += anndist;
			}else{ untested();
				message(bDANGER, "problem computing distance in " + ef.label() + "\n");
			}
			++number;
		}

		unsigned lvl=bDEBUG;
		if(print_summary){
			lvl=7;
		}else{
		}
			
		message(lvl, "distance sum %f\n", distance_sum);
	}

} // wormgraph
