//
// (c) 2017 scs.leeds.ac.uk
// Author: Felix Salfelder 2017
//
// a worm-graph thing
//
// currently not working without FREAK
#define USE_FREAK

#define USE_NESTED_MAP // under construction?
#define OpenCV_FOUND


#include "config.hh"

#include <opencv2/features2d.hpp>
#include "opencv2/imgproc/imgproc.hpp" // morph?


#include <iostream>
#include <cassert>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
#include <exception>

// #include <opencv2/opencv.hpp> // c++ headers
#include "lib/options.hh"
#include "lib/trace.hh"
#include "lib/io_misc.hh"

#include "lib/parameter.hh"
#include "lib/error.hh"

#include "lib/image/util.hh"
#include "lib/image/freak.hh"
#include "lib/image/display.hh"
#include "lib/mesh.hh"
#include "lib/timer.hh"
// #include "lib/timeprovider.hh"

// problem definitions
#include "lib/model.hh"
#include "lib/femscheme.hh"

// output helpers
#include "lib/vtuwriter.hh"

// helpers with images and projections
#include "lib/distancefunction.hh"
#include "lib/projectionoperator.hh"
#include "lib/image/silhouette.hh"
#include "lib/image/imagereader.hh"
#include "lib/image/annotations.hh"

#include "lib/bits/map.hh"
#include "lib/graph/frames.hh"
#include "lib/graph_pointfinder.hh"

#ifndef WORLD_DIM
#define WORLD_DIM 3
#endif
#ifndef NCAMS
#define NCAMS 3
#endif

#define WGassert(x)

// these should be parameters/cmdline args
// (some are)
double initialguess_unknown = -1.;
double deltat=1e-4;
unsigned numberofsteps=5;

static const std::string restrict_sil_default="0";
/*------------------------------------------------------*/

typedef draft::neigh_similarity1 weights_type;
typedef draft::edgeweighted_graph_type1 G;
typedef draft::framesgraph<G, weights_type> fg_type;

typedef cv::Point coord_type;

typedef std::array< cv::Mat, NCAMS > frames_type;
typedef std::deque< frames_type > frames_deque_type;

using draft::frame_type;
using draft::annotation_type;
//using draft::features_type;

void run_wormgraph(ConfigParameters const& );
//typedef std::pair<unsigned, unsigned> coord_type;
#if 0
namespace std{
static bool operator<(coord_type const& a, coord_type const& b){ untested();
	return make_pair(a.x, a.y) < make_pair(b.x, b.y);
}
} // std
#endif

int main( int argc, char **argv )
{ itested();

	std::string aviname="";
	std::string bgname="";
	std::string loadfile="";
	std::string dilate_sil_radius="";

	ConfigParameters parameters;
	parameters.set("display", "0");
	parameters.set("verbose", "1");

	parameters.set("bgname", "bg.png");
	parameters.set("aviname", "worm.avi");
	parameters.set("frameno", "0");

	// evaluate frames ("fitness")
	parameters.set("eval-csv", "");
	parameters.set("eval-index", "");

	parameters.set("dumpfile", "");
	parameters.set("loadfile", "");

	parameters.set("initial-known", "0");
	parameters.set("initial-unknown", "0");

	parameters.set("restrict-sil", "0");
	parameters.set("restrict-tgt-sil", "1");
	parameters.set("dilate-sil", "0");
	parameters.set("dilate-tgt-sil", "5");

	parameters.set("headclips-index", "../head_clips/index");

  #include "worm_defaults.hh"


	int i=1;
	// poor mans getopt. yes it's limited, but it works.
	for(; i<argc; ++i){
		trace2("getopt", i, argv[i]);
		if(argv[i][0]=='-' && argv[i][1]!='-'){
			switch(argv[i][1]){ untested();
				case 'T':
					options::errorlevel = bTRACE;
					break;
				case 'D':
					options::errorlevel = bDEBUG;
					break;
				case 'E': untested();
					++i;
					options::errorlevel = atoi(argv[i]);
					break;
				case 'v':
					parameters.set("verbose", argv[i]);
					break;
				case 'l':
					++i;
					parameters.set("loadfile", argv[i]);
					break;
				case 'd':
					++i;
					parameters.set("dumpfile", argv[i]);
					break;
				case 'k':
					++i;
					parameters.set("initial-known", argv[i]);
					break;
				case 'u':
					++i;
					parameters.set("initial-unknown", argv[i]);
					break;
				case 'b': untested();
					++i;
					parameters.set("bgname", argv[i]);
					break;
				case 'a': untested();
					++i;
					parameters.set("aviname", argv[i]);
					break;
				case 'S': untested();
					++i;
					parameters.set("pattern-scale", argv[i]);
					break;
				//case 'D': untested();
				//	++i;
				//	parameters.set("dilate-tgt-sil", argv[i]);
				//	parameters.set("dilate-sil", argv[i]);
				//	break;
				case 'f': untested();
					++i;
					parameters.set("frameno", argv[i]);
					break;
				case 'm':
					++i;
					parameters.merge_file(argv[i]);
					break;
				case 'n':
					++i;
					numberofsteps = atoi(argv[i]);
					break;
				case 't':
					++i;
					parameters.set("deltat", argv[i]);
					break;
				case 'h':
					parameters.dump(std::cout);
					exit(1);
			}
		}else if(argv[i][0]=='-' && argv[i][1]=='-'){
			trace1("got", &argv[i][2]);
			try{
				parse(parameters, &argv[i][2]);
				// ok, must have been : or =
				trace1("parsed", argv[i]);
			}catch(exception_nomatch){
				incomplete(); // BUG, no feedback
				              // should only allow override existing params.
				if(i+1<argc){
					parameters.update( &argv[i][2], argv[i+1]);
					++i;
				}else{ untested();
					std::cerr << "something wrong with '" << argv[i] << "'\n";
					exit(1);
				}
			}
		}else{ untested();
			trace1("nodash", argv[i]);
			try{ untested();
				parameters.merge_file(argv[i]);
			}catch( ...) { untested();
				incomplete();
				std::cout << "something wrong w/params\n";
				return 1;
			}
		}
	}

	try{
	  // print variables
	  std::cout << "parameters" << std::endl;
	  parameters.dump( std::cout, " - ", " = " );
	  std::cout << std::endl;

		run_wormgraph(parameters);
	}catch(exception_cantfind const& e){ untested();
		std::cerr << "paramerror in wormgraph: " << e.what() << "\n";
	}
}

namespace draft{

// this is a stub.
class pixel_sim_base{
protected:
	pixel_sim_base(){ untested();
	}
	pixel_sim_base(frame_type const& f)
		: _f1(&f) { untested();
	}

public:
	unsigned distance_square( vertex_repr_type a, vertex_repr_type b) const{ untested();
		_f1 = &a.frame();
		_f2 = &b.frame();
		return distance_square(a.coord(), b.coord());
	}

	unsigned distance_square( coord_type x, vertex_repr_type b) const{ untested();
		_f2 = &b.frame();
		return distance_square(x, b.coord());
	}

	virtual unsigned distance_square( coord_type x, coord_type y ) const { unreachable(); return -1u; }

public:
#if 0
	template<class T, class S>
	double similarity(T x, S b) const{ untested();
		unsigned edgeweight=distance_square(x, b);
		double variance=1.;
		return exp(-double(edgeweight)/variance);
	}
#endif
protected:
	uchar get(cv::Mat const& t, int x, int y) const{ untested();
		int imgsz=2048;
		if(x<0){ untested();
			return 0;
		}else if(y<0){ itested();
			return 0;
		}else if(x+1>imgsz){ untested();
			return 0;
		}else if(y+1>imgsz){ untested();
			return 0;
		}else{ untested();
			return t.at<unsigned char>(y, x);
		}
	}

protected: // BUG
	mutable frame_type const* _f1;
	mutable frame_type const* _f2; // yuck.
};

class pixel_similarity1 : public pixel_sim_base {
public:
	pixel_similarity1() : pixel_sim_base(){ untested();
	}
	pixel_similarity1(frame_type const& f)
		: pixel_sim_base(f) { untested();
	}
	pixel_similarity1(const pixel_similarity1& o)
		: pixel_sim_base(o) { untested();
	}
public:
	unsigned distance_square( coord_type x, coord_type y ) const{ itested();
		WGassert(_f1);
		WGassert(_f2);

		unsigned g1 = _f1->at<unsigned char>(x.x, x.y);
		unsigned g2 = _f2->at<unsigned char>(y.x, y.y);

		int delta = g1-g2;
		return delta*delta;
	}
};

} // draft

void run_wormgraph(ConfigParameters const& parameters)
{

	bool display=parameters.get<bool>("display");

	std::string bgname=parameters.get<std::string>("bgname");
	std::string dumpfile=parameters.get<std::string>("dumpfile");
	std::string loadfile=parameters.get<std::string>("loadfile");
	std::string avifilename=parameters.get<std::string>("aviname");
	unsigned frameno=parameters.get<unsigned>("frameno");

	std::string eval_csv=parameters.get<std::string>("eval-csv");
	std::string eval_index=parameters.get<std::string>("eval-index");

	// these only apply to /target/ frame
	unsigned dilate_tgt_sil_radius=parameters.get<unsigned>("dilate-tgt-sil");
	bool restrict_tgt_sil=parameters.get<bool>("restrict-tgt-sil");

	unsigned thresh_edge=parameters.get<unsigned>("thresh-edge");
	unsigned thresh_collapse=parameters.get<unsigned>("thresh-collapse");
	int initialguess_known=parameters.get<int>("initial-known");
	int initialguess_unknown=parameters.get<int>("initial-unknown");

	double distance_scale=parameters.get<double>("distance-scale");

	fg_type FG;

	if(loadfile!=""){
		s_timer tmr("file load");
		std::ifstream myfile;
		myfile.open (loadfile);
		fg_type a(myfile, distance_scale);

		FG = std::move(a);
	}else{ untested();
		s_timer tmr("fg from index");
		make_framesgraph_from_indexfile(FG, parameters);
	}

	unsigned nv=boost::num_vertices(FG);
	unsigned ne=boost::num_edges(FG);

	message(bLOG, "framesgraph w/o target, v/e %d/%d\n", nv, ne);

	if(dumpfile!=""){ untested();
		std::ofstream myfile;
		myfile.open (dumpfile);
		parameters.dump(myfile, "# ");
		FG.dump(myfile);
		return;
	}else{
	}

	typedef draft::neigh_similarity1 weights_type;
	typedef draft::edgeweighted_graph_type1 G;
	typedef draft::framesgraph_pointfinder<G, weights_type> fgpf_type;

	double eucl_base=parameters.get<double>("graph.eucl-base");

	fgpf_type* kpf = new fgpf_type(loadfile, deltat,
			 initialguess_known, initialguess_unknown,
			 thresh_collapse, thresh_edge, distance_scale,
			 dilate_tgt_sil_radius, restrict_tgt_sil, eucl_base);

	cv::Mat worm;
	annotation_type annotation;
	double ppmm;
	if(eval_csv!=""){
		annotated_frame af(eval_csv, annotated_frame::_CSV);
		worm = draft::get_frame(af);
		annotation = af.annotation();
		ppmm=af.ppmm();
		untested();
	}else{ untested();
		cv::Mat background, wormfull;
		getBackgroundImage(bgname, background);
		getframefromfile(avifilename, wormfull, frameno);
		worm = background - wormfull;

		incomplete();
		ppmm=150;
	}

	// adjust_brightness_max(worm);
	cv::Mat tgtsil=worm.clone();
	// display_grayscale_frame("wormtodetect", worm);

//	display_sil_bb("detectsil", tgtsil);

	ANNOTATION known;

	cv::Mat worm_FKP=worm.clone();
	silhouetteImageDilate(worm, tgtsil, dilate_tgt_sil_radius);
	std::cout << "target tgtsil nonzero " << countNonZero(tgtsil) << "\n";
	kpf->find_known_points(tgtsil, worm_FKP, known, ppmm);

	 if(display){ untested();
		 display_sil_bb("search area", tgtsil);
	 }else{
	 }

	 std::cout << "found " << known.size() << " known points\n";

	 if(eval_csv!=""){
		 double anndist=framestuff::annotated_frame::distance(known, annotation);
		 std::cout << "distance " << anndist << "\n";
	 }

	 if(display){ untested();
		 display_pointset_on_frame("result", worm, known);
	 }
	 exit(0);

	//display_sil_bb("he area", tgtsil);

	nv=boost::num_vertices(FG);
	ne=boost::num_edges(FG);

	std::cout << "graph finally " <<  ne << " " << nv << "\n";

//void find_known_points(cv::Mat const& src_, cv::Mat& raw, K& known)

	double deltat_save=deltat;
//	double initialguess_unknown_save = initialguess_unkonwn;
   
#if 0
	typedef draft::HEsolver<fg_type> hesolver_type;
#else
	typedef draft::FEsolver<fg_type> hesolver_type;
#endif

	for(unsigned i=0; i<1; ++i){ untested();
		deltat = deltat_save;
		std::cout << "init" << initialguess_known << "\n";
		for(unsigned j=0; j<3; ++j){ untested();
			hesolver_type he(FG, deltat,
					initialguess_known, initialguess_unknown,
					+ "_" + std::to_string(initialguess_known)
					+ "_" + std::to_string(initialguess_unknown)
					+ "_" + std::to_string(deltat));
			he.do_it(tgtsil, &known);
			he.dump_result();
			// he.display();
			deltat*=.5;
		}
		initialguess_known*=.9;
		initialguess_unknown*=.9;
	}

	// silhouetteImages( p, frames, backgrounds, srcs, 10 );
	// time holder
	// io time helpers
	// double nextWriteTime = 0;
	// const double nextWriteTimeStep = parameters.get< double >( "io.timestep" );
	// const bool doInnerWrite = parameters.get< bool >( "io.innerwrite" );

} // wormgraph
