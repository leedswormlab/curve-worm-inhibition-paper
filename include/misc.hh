#pragma once

#if __cplusplus > 201400L
#define DEPRECATED [[deprecated]]
#else
// gcc or clang only
#define DEPRECATED __attribute__((deprecated))
#endif
